<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use DB;

class Blog extends Model
{
    protected $table = 'blog_post';

    protected $primaryKey = 'post_id';

    public $fillable = ['post_title','post_content'];

    public function getPosts($lang, $categoryId = null, $postId = null)
    {
    	$blog = array();

    	$blogModel = new Blog;
    	$blogPosts = $blogModel
    		//->select()
            ->where('lang_code',$lang)
            ->where('post_status',1)
            ->orderBy('created_at','desc');

        if($postId)
        	$blogPosts = $blogPosts->where('post_id',$postId);

        $blogPosts = $blogPosts->get();

        if(sizeof($blogPosts))
        {
        	foreach($blogPosts as $blog_post)
        	{
        		$blogData = array();
        		$blogData['post_id'] = $blog_post->post_id;
        		$blogData['post_title'] = $blog_post->post_title;
        		$blogData['post_content'] = $blog_post->post_content;
        		$blogData['post_tags'] = $blog_post->post_tags;
        		$blogData['blog_banner'] = $blog_post->blog_banner;
        		$blogData['short_desc'] = $blog_post->short_desc;
        		$blogData['post_slug'] = $blog_post->post_slug;
        		$blogData['featured_article'] = $blog_post->featured_article;
        		$blogData['publish_date'] = $blog_post->publish_date;
        		$blogData['meta_title'] = $blog_post->meta_title;
        		$blogData['meta_desc'] = $blog_post->meta_desc;
        		$blogData['meta_keyword'] = $blog_post->meta_keyword;

        		$cateIds = array();

        		if($blog_post->category_id)
        		{
	        		$cateIds = json_decode($blog_post->category_id);

	        		if(sizeof($cateIds))
	        		{	
	        			$temp = array();
	        			foreach($cateIds as $category_id)
	        			{
	        				$categoryModel = new Category;
        					$category = $categoryModel->where('blog_categories_id',$category_id)->first();
        					$temp[$category_id]['category_name'] = $category->category_name;
        					$temp[$category_id]['category_slug'] = $category->category_slug;
	        			}
	        			ksort($temp);
        				$blogData['category_data'] = $temp;
	        		}
        		}

        		if($categoryId)
        		{
        			if(isset($blogData['category_data']) && is_array($blogData['category_data']) && array_key_exists($categoryId, $blogData['category_data']))
        				$blog[] = (object) $blogData;
        		}
        		else
        			$blog[] = (object) $blogData;
        	}
        }
    	return ($blog);
    }

    public function getBlogSettings($lang)
    {
    	$blog_setting =  DB::table('blog_setting')
    		->where('status',1)
    		->whereRaw('( (lang_code = "'.$lang.'") or (lang_code = "en") )') //have to check this query working or not
    		->first();
    	return $blog_setting;
    }

    public function getRelatedPosts($lang,$post_id)
    {
    	$blog_post =  DB::table('blog_post')
            ->join('related_blogs','related_blogs.bpost_id','=','blog_post.post_id')
        ->where('blog_post.post_status',1)->where('blog_post.post_id',$post_id)->get();

        $blog_realtedpost = array();
        foreach($blog_post as $key => $blogpost){
        	
            $record = $this->getPosts($lang,null,$blogpost->rposts_id);
            if($record)
            	$blog_realtedpost[] = $record;
        }

        return ($blog_realtedpost);
    }

    public function getBlogCategoriesMenu()
    {
    	$categories_menu =  DB::select("select bp.category_name as parent, 
        bp.blog_categories_id as parent_id,
        bp.category_slug as parent_slug, 
        bc.blog_categories_id as child_id,
        bc.category_name as child, 
        bc.category_slug as child_slug
        from blog_categories bp
        left join blog_categories bc on bc.parent_id = bp.blog_categories_id
        where bp.parent_id = 0 and bp.`status`= 1
        order by bp.category_name");

        $categoriesMenu = array();
        if(sizeof($categories_menu))
        {
            foreach ($categories_menu as $key => $cm) {
                $categoriesMenu[ $cm->parent_id ]['id'] = $cm->parent_id;
                $categoriesMenu[ $cm->parent_id ]['name'] = $cm->parent;
                $categoriesMenu[ $cm->parent_id ]['slug'] = $cm->parent_slug;

                if($cm->child_id)
                {
                    $child = array();
                    $child[ $cm->child_id ]['id'] = $cm->child_id;
                    $child[ $cm->child_id ]['name'] = $cm->child;
                    $child[ $cm->child_id ]['slug'] = $cm->child_slug;
                    if(!isset( $categoriesMenu[ $cm->parent_id ]['child'] ))
                        $categoriesMenu[ $cm->parent_id ]['child'] = $child;
                    else
                        $categoriesMenu[ $cm->parent_id ]['child'][ $cm->child_id ] = $child[ $cm->child_id ];
                }
            }
        }
        return $categoriesMenu;
    }
}