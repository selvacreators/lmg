<?php

namespace App;

use DB;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Common extends Model
{
    /**
     * Menu Type 1-Category, 2-Collection, 3-Pages, 4-Custom Menu
    */
    public function checkPageType($url)
    {
        $menuData = DB::table('menu_main as m')
            ->select('menu_link','menu_name','menu_type')
            ->where('m.menu_status', 1)
            ->where('m.menu_link', $url)
            ->first();
        return $menuData;
    }

    public static function getBrandSetting()
    {
        $lang = Session::get('locale');
        return DB::table('brands_setting')
            ->where('lang_code',$lang)
            ->where('status',1)
            ->first();
    }

    public static function getBrands()
    {
        $lang = Session::get('locale');
        
        return DB::table('brands as b')
        ->join('brands_description as bd','b.brand_id','=','bd.brand_id')
            ->where('bd.lang_code',$lang)
            ->where('b.brand_status',1)
            ->orderBy('b.sort_order')
            ->get();
    }

    public static function getMenuContent()
    {
        $lang = Session::get('locale');
        $subMenu = DB::table('menu_sub')
            ->where('submenu_status',1)
            ->get();

        $utilityMenu = DB::table('menu_main')
            ->where('menu_status',1)
            ->where('menu_position','U')
            ->where('lang_code',$lang)
            ->orderBy('sort_order')
            ->get();

        $topMenu = DB::table('menu_main')
            ->where('menu_status',1)
            ->where('menu_position','M')
            ->where('lang_code',$lang)
            ->orderBy('sort_order')
            ->get();

        $getfooterMenu = DB::table('menu_main')
            ->where('menu_status',1)
            ->where('menu_position','F')
            ->where('lang_code',$lang)
            ->orderby('sort_order')
            ->get();

    	return array('topMenu' => $topMenu, 'subMenu' => $subMenu, 'utilityMenu' => $utilityMenu, 'footer_menu' => $getfooterMenu);
    }

    public static function checkLang($lang)
    {
        $langData = DB::table('language')
            ->where('lang_code',$lang)
            ->where('status',1)
            ->first();
        if(isset($langData->lang_id))
            return TRUE;
        return FALSE;
    }

    public static function getAllActiveLanguage()
    {
        $langData = DB::table('language')
            ->where('status',1)
            ->get();
        return $langData;
    }

    public static function getChatInfo()
    {
        $chatData = DB::table('chat_info')
            ->where('status',1)
            ->get();
        return $chatData;
    }

    public static function getCommonSetting()
    {
        $lang = Session::get('locale');
        $csetting = DB::table('common_setting')
            ->where('lang_code',$lang)
            // ->whereRaw('( (lang_code = "'.$lang.'") or (lang_code = "en") )')
            ->where('status',1)
            ->first();
        return $csetting;
    }

    public static function getSocialInfo()
    {
        $social = DB::table('social_plugin')
            ->where('social_id',1)
            ->first();
        return $social;
    }

    public static function getPoupExistIntent()
    {
        $exit_intent = DB::table('tbl_exit_intent')
            ->where('status',1)
            ->get();
        return $exit_intent;
    }

    public static function getPoupNewsletter()
    {
        $popup = DB::table('newsletter_popup')
            ->where('popup_status',1)
            ->get();
        return $popup;
    }
}