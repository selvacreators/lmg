<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reviews extends Model
{
    protected $table = 'product_review';
    protected $primaryKey = 'review_id';

    public function getProductReviews($product_id)
    {
    	$ratingData[1] = $ratingData[2] = $ratingData[3] = $ratingData[4] = $ratingData[5] = $ratingData['averageRating']= 0;
    	$ratings = DB::table( $this->table )
    				->select('rating')
                    ->where('status',1)
                    ->where('product_id',$product_id)
                    ->get();
        if(sizeof($ratings))
        {
        	$max = $totalReview = 0;
	        foreach($ratings as $rating)
	        {
	        	@$ratingData[ $rating->rating ] += 1;
	        	$max += $rating->rating;
	        	$totalReview++;
	        }
	        $ratingData['averageRating'] = round(($max / $totalReview),2);
        }
        return $ratingData;
    }
}