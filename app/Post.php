<?php 
namespace App;

use DB;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Post extends Eloquent {


    protected $table = 'blog_post';

    protected $fillable = ['post_status', 'post_title', 'post_slug', 'post_content', 'updated_at', 'category_id'];
    
    function getUrlAttribute($value) {
        return '/blog/'.$this->slug.'/';
    }
    function getPubDateAttribute($value) {
        return $this->created_at->format('D, d M Y H:i:s O');
    }
    function scopeIsPublished($query) {
        return $query->where('updated_at','!=','0000-00-00 00:00:00')->where('updated_at', '<', \DB::raw('now()'));
    }
    function is_published() {
        return ($this->published_at !== null);
    }
    function Category() {
        return $this->hasOne('App', 'id', 'category_id');
    }
    function getImageAttribute($value) {
        if ($value == '') {
            return 'http://lorempixel.com/400/300/';
        }
        return '/blog/posts/'.$value;
    }
}