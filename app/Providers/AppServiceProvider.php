<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       // if (\App::environment('local')) { URL::forceSchema('https'); }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
