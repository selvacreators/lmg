<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function filterProduct($slug, $request)
    {
        $baseQuery = 'select p.product_id
        from categories_description cd 
        join product_to_category ptc on ptc.category_id = cd.categories_id
        join product p on p.product_id = ptc.product_id
        where cd.category_slug = "'.$slug.'"';

        $baseProductSelect = DB::select($baseQuery);

        $getProductIds = array_column($baseProductSelect, 'product_id');
        if(!sizeof($getProductIds))
        {
            $getProductIds = array_map(function($e) {
                return is_object($e) ? $e->product_id : $e['product_id'];
            }, $baseProductSelect);
        }

        if(sizeof($getProductIds))
        {
            $productIds = implode(',', $getProductIds);
            $products = array();

            $sql = '';
            if(isset($request->attribute_value) && sizeof($request->attribute_value))
            {
                $commonSearchArray = $brandSearchArry = array();
                foreach ($request->attribute_value as $column => $values) {
                    foreach($values as $value)
                    {
                        if($column == 'brands')
                            $brandSearchArry[] = $value;
                        else
                            $commonSearchArray[$column][] = $value;
                    }
                }

                if(sizeof( $commonSearchArray ))
                {
                    $commonSearchKey = ' and (';
                    foreach($commonSearchArray as $column => $values)
                    {
                        if(sizeof( $values ))
                        {
                            foreach($values as $value)
                                $commonSearchKey .= '(ad.name = "'.$column.'" and pa.text like "%'.$value.'%") or';
                        }
                    }

                    $commonSearchKey = rtrim($commonSearchKey,'or');                    
                    $sql .= $commonSearchKey.')';

                }

                if(sizeof($brandSearchArry))
                {
                    $brandSearchKey = '';
                    foreach ($brandSearchArry as $value) {
                        $brandSearchKey .= '"'.$value.'",';
                    }
                    $brandSearchKey = rtrim($brandSearchKey, ',');
                    $sql .= 'and b.brand_name in ('.$brandSearchKey.')';
                }
            }

            $productQuery = "select 
            pa.text as product_text, 
            ad.name as attribute_name, 
            b.brand_name, 
            cd.category_name,
            pd.name,
            product_slug,
            short_desc,
            prod_slider_image,
            comfort
            from product p 
            left join product_attibute pa on p.product_id = pa.product_id 
            left join product_description pd on pd.product_id = p.product_id 
            left join attribute a on a.attribute_id = pa.attribute_id 
            left join attribute_description ad on ad.attribute_id = pa.attribute_id 
            left join attribute_group_description agd on agd.attribute_group_id = a.attribute_group_id 
            left join brands_description b on b.brand_id = p.brand_id 
            left join product_to_category ptc on ptc.product_id = p.product_id 
            left join categories_description cd on cd.categories_id = ptc.category_id 
            where 
            p.product_id in (".$productIds.") ".$sql." group by p.product_id";
            // return $productQuery;
            $filterProduct = DB::select($productQuery);

            if(sizeof($filterProduct))
            {
                foreach ($filterProduct as $pro) {
                    $products[] = array(
                        'product_slug' => url($request->path,[$pro->product_slug]).'.html',
                        'prod_slider_image' => $pro->prod_slider_image,
                        'comfort' => $pro->comfort,
                        'desc' => html_entity_decode($pro->short_desc),
                        'name' => $pro->name,
                    );
                }
            }

            return $products;
        }
        return FALSE;
    }

    public function makeFilterOption(array $productIds)
    {
        //$currentPath = basename($data->url());

       // $path = $data->path();
        if(sizeof($productIds))
        {
            $productIds = implode(',', $productIds);
            $filterData = array();
            // $filterQuery = '
            // select 
            // pa.text as product_text, ad.name as attribute_name, b.brand_name, cd.category_name
            // from product_attibute pa
            // join product p on p.product_id = pa.product_id
            // join product_description pd on pd.product_id = pa.product_id
            // join attribute a on a.attribute_id = pa.attribute_id
            // join attribute_description ad on ad.attribute_id = pa.attribute_id
            // join attribute_group_description agd on agd.attribute_group_id = a.attribute_group_id
            // join brands b on b.brand_id = p.brand_id
            // join product_to_category ptc on ptc.product_id = p.product_id
            // join categories_description cd on cd.categories_id = ptc.category_id
            // where pa.product_id in ('.$productIds.')';
            $filterQuery = '
            select 
            pa.text as product_text, ad.name as attribute_name, bd.brand_name, cd.category_name
            from product_attibute pa
            join product p on p.product_id = pa.product_id
            join product_description pd on pd.product_id = pa.product_id
            join attribute a on a.attribute_id = pa.attribute_id
            join attribute_description ad on ad.attribute_id = pa.attribute_id
            join attribute_group_description agd on agd.attribute_group_id = a.attribute_group_id
            join brands b on b.brand_id = p.brand_id
            join brands_description bd on bd.brand_id = b.brand_id
            join product_to_category ptc on ptc.product_id = p.product_id
            join categories_description cd on cd.categories_id = ptc.category_id
            where pa.product_id in ('.$productIds.')';

            //order by attribute_name
            //group by attribute_name
            $filterResult = DB::select($filterQuery);
            // dd($productIds);
            if(sizeof($filterResult))
            {
                foreach ($filterResult as $filter) {
                    if(array_key_exists($filter->attribute_name, $filterData))
                    {
                        foreach (explode(PHP_EOL, $filter->product_text ) as $attr_val) {
                            $key = str_slug($attr_val, '_');
                            $filterData[ $filter->attribute_name ][$key] = $attr_val;
                            ksort($filterData[ $filter->attribute_name ]);
                        }
                    }
                    else
                    {
                        $temp = array();
                        foreach (explode(PHP_EOL, $filter->product_text ) as $attr_val) {
                            $key = str_slug($attr_val, '_');
                            $temp[$key] = $attr_val;
                        }
                        $filterData[ $filter->attribute_name ] = $temp;
                    }

                    /* Brands Filter Add*/
                    $brandKey = str_slug($filter->brand_name, '_');
                    $filterData['Brands'][$brandKey] = $filter->brand_name;

                    /*Category Filter Add*/
                    $cateforyKey = str_slug($filter->category_name, '_');
                    $filterData['Type'][$cateforyKey] = $filter->category_name;
                }
            }
            ksort($filterData);
            // dd($filterData);
            //dd($filterData,$currentPath,$path);
            return $filterData;
            
        }
        return FALSE;
    }

    //Product Page Frontend function
	public static function getProducts($slug, $lang)
    {
    	$productDetails = DB::table('product as p')
                    ->select('p.*','d.*','bd.brand_name','b.brand_logo')
                    ->join('product_description as d','p.product_id','=','d.product_id')
                    ->join('brands as b','b.brand_id','=','p.brand_id')
    				->join('brands_description as bd','bd.brand_id','=','b.brand_id')
    				->where('d.lang_code',$lang)
    				->where('product_slug',$slug)
    				->where('status',1)
    				->first();

                    // dd($lang);
                    
        if(! $productDetails ) abort('404');

    	$product['product'] = $productDetails;

    	$product['product_collections'] = DB::table('product_to_collection as pc')
                    ->join('collection as c','c.collection_id','=','pc.collection_id')
                    ->join('collection_description as cd','c.collection_id','=','cd.collection_id')
                    ->where('cd.lang_code',$lang)
                    ->where('pc.product_id',$productDetails->product_id)
                    ->where('col_status',1)
                    ->first();
        // dd($product['product_collections']);

        $product['product_category'] = DB::table('product_to_category as pc')
                    ->join('categories as c','c.categories_id','=','pc.category_id')
                    ->join('categories_description as cd','cd.categories_id','=','c.categories_id')
    				->where('cd.lang_code',$lang)
    				->where('pc.product_id',$productDetails->product_id)
    				->where('c.status',1)
    				->get();

    	$product['product_images_gallery'] = DB::table('product_image')
    				->where('product_id',$productDetails->product_id)
    				->get();

		$product['product_features'] = DB::table('product_features')
    				->where('product_id',$productDetails->product_id)
    				->where('status',1)
    				->get();

		$product['product_description'] = DB::table('product_description')
    				->where('product_id',$productDetails->product_id)
    				->first();

        // $product['product_attibute'] = DB::table('product_attibute')
        // ->where('product_id',$productDetails->product_id)
        // ->get();

        $product['product_attibute'] = DB::table('product_attibute')
                    ->join('attribute_description','product_attibute.attribute_id','=','attribute_description.attribute_id')
                    ->where('product_id',$productDetails->product_id)
                    ->get();

        $product['product_to_store'] = DB::table('product_to_store as ps')
                    ->join('store_locator as sl','sl.store_id','=','ps.store_id')
                    ->where('ps.product_id',$productDetails->product_id)
                    ->where('sl.store_status',1)
                    ->get();

        $product['other_products'] = DB::table('other_products')
                    ->join('product','product.product_id','=','other_products.oproduct_id')
                    ->join('product_description','product.product_id','=','product_description.product_id')
                ->where('other_products.product_id', $productDetails->product_id)
                ->where('product.status', 1)
                ->get();

        $product['related_products'] = DB::table('related_products')        
                    ->join('product','product.product_id','=','related_products.rproduct_id')
                    ->join('product_description','product.product_id','=','product_description.product_id')
                ->where('related_products.product_id', $productDetails->product_id)
                ->where('product.status', 1)
                ->get();
                /*
                DB::table('product as p')
                    ->select('p.*','d.*','b.brand_name','b.brand_logo')
                    ->join('product_description as d','p.product_id','=','d.product_id')
                    ->join('brands as b','b.brand_id','=','p.brand_id')
                    ->where('d.lang_code',$lang)
                    ->where('product_slug',$slug)*/
    	// echo "<pre>";
    	// print_r($product);
    	// die();
    	return $product;
    }

    //Edit Page backend function
    public static function getEditProducts($id,$lang)
    {
        $productDetails = DB::table('product as p')
            ->join('product_description as d','p.product_id','=','d.product_id')
            ->where('p.product_id',$id)
            ->where('d.lang_code',$lang)
            ->first();

        $product['product'] = $productDetails;

        $product['product_collections'] = DB::table('product_to_collection as pc')
            ->join('collection as c','c.collection_id','=','pc.collection_id')
            ->join('collection_description as cd','c.collection_id','=','cd.collection_id')
            ->where('product_id',$productDetails->product_id)
            ->where('cd.lang_code',$lang)
            ->first();

            // dd($product['product_collections']);

        $product['product_category'] = DB::table('product_to_category as pc')
            ->join('categories as c','c.categories_id','=','pc.category_id')
            ->join('categories_description as cd','c.categories_id','=','cd.categories_id')
            ->where('pc.product_id',$productDetails->product_id)
            ->where('cd.lang_code',$lang)
            ->where('c.status',1)
            ->get();

        $product['product_to_store'] = DB::table('product_to_store as ps')
            ->join('store_locator as sl','sl.store_id','=','ps.store_id')
            ->where('ps.product_id',$productDetails->product_id)
            ->where('sl.store_status',1)
            ->where('sl.lang_code',$lang)
            ->get();

        $product['product_images_gallery'] = DB::table('product_image')
                    ->where('product_id',$productDetails->product_id)
                    ->orderBy('sort_order')
                    ->get();

        $product['product_features'] = DB::table('product_features')
                    ->where('product_id',$productDetails->product_id)
                    ->orderBy('sort_order')
                    //->where('status',1)
                    // ->first();
                    ->get();

        $product['product_description'] = DB::table('product_description')
                    ->where('product_id',$productDetails->product_id)
                    ->where('lang_code',$lang)
                    ->first();

        $product['product_attibute'] = DB::table('product_attibute')
                    ->where('product_id',$productDetails->product_id)
                    ->get();

        $product['other_products'] = DB::table('other_products')
                    ->where('product_id', $productDetails->product_id)
                    ->get();

        $product['related_products'] = DB::table('related_products')
                    ->where('product_id', $productDetails->product_id)
                    ->get();

                    // dd($product['product_description']);
                    
         // echo "<pre>";
         // print_r($product);
         // die();
         //dd($product);
        return $product;
    }

    public function getProductDescription()
    {
        $productDetails = DB::table('product_description')
            ->where('product_id',$productDetails->product_id)
            ->first();
        $product['description'] = $productDetails;
        return new HtmlString($this->description);
    }

    public static function getLanguageProductInfo($lang)
    {
        $stock_status = DB::table('stock_status')
            ->where('lang_code',$lang)
            ->where('st_status',1)
            ->get();

        $product_type = DB::table('product_type')
            ->join('product_type_desc','product_type.id','=','product_type_desc.product_type_id')
            ->where('lang_code',$lang)
            ->where('status',1)
            ->get();

        $getweight = DB::table('weight_class')
            ->join('weight_class_description','weight_class.weight_class_id','=','weight_class_description.weight_class_id')
            ->Where('weight_class_description.lang_code',$lang)
            ->orderBy('weight_class.weight_class_id')
            ->get();

        $getlength = DB::table('length_class')
            ->join('length_class_description','length_class.length_class_id','=','length_class_description.length_class_id')
            ->Where('length_class_description.lang_code',$lang)
            ->orderBy('length_class.length_class_id')
            ->get();

        $store_location = DB::table('store_locator')
            ->where('lang_code',$lang)
            ->where('store_status',1)
            ->get();

        $brands = DB::table('brand_description')
            ->join('brands','brand_description.brand_id','=','brands.brand_id')
            ->where('lang_code',$lang)
            ->where('brand_status',1)
            ->get();

        $collection = DB::table('collection')
            ->join('collection_description','collection_description.collection_id','=','collection.collection_id')
            ->where('lang_code',$lang)
            ->where('col_status',1)
            ->get();

        // dd($collection);

        $categories = DB::table('categories')
            ->join('categories_description','categories_description.categories_id','=','categories.categories_id')
            ->Where('categories_description.lang_code',$lang)
            ->Where('categories.status',1)
            ->get();

        $attribute = DB::table('attribute')
            ->join('attribute_description','attribute_description.attribute_id','=','attribute.attribute_id')
            ->join('attribute_group','attribute_group.attribute_group_id','=','attribute.attribute_group_id')
            ->Where('attribute_description.lang_code',$lang)
            ->orderBy('attribute_group.sort_order')
            ->get();

        $ProductDesc = DB::table('product')
            ->join('product_description','product_description.product_id','=','product.product_id')
            ->where('lang_code',$lang)
            ->where('product.status',1)
            ->orderBy('product.product_id')
            ->get();

        $allData = array(
            'stock_status' => $stock_status,
            'product_type' => $product_type,
            'getweight' => $getweight,
            'getlength' => $getlength,
            'store_location' => $store_location,
            'categories' => $categories,
            'brands' => $brands,
            'collection' => $collection,
            'attribute' => $attribute,
            'ProductDesc' => $ProductDesc
        );

        // echo "<pre>";
        // print_r($allData);
        // die();

        // return response()->json(['data' => $allData]);
        // return response()->json(['data' => $allData]);
        return json_encode($allData);
        // return $allData;
    }

    /*$product = DB::select(' select * from product p
    INNER JOIN product_to_collection pc on p.product_id =  pc.product_id
    INNER JOIN product_description pd on p.product_id =  pd.product_id
    INNER JOIN product_attibute pa on p.product_id =  pa.product_id
    LEFT OUTER JOIN product_image pi on pi.product_id =  p.product_id
    INNER JOIN product_features pf on pf.product_id =  p.product_id
    INNER JOIN collection c on pc.collection_id = c.collection_id
    where p.status = 1 and c.col_status = 1 and pd.name = "'.$currentPath.'" ');*/
}