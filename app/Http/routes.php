<?php
include __DIR__.'/../Modules/LmgWorldAdmin/routes.php';
include __DIR__.'/../Modules/User/routes.php';
include __DIR__.'/../Modules/Product/routes.php';
include __DIR__.'/../Modules/Blog/routes.php';
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['module' => 'Admin', 'namespace' => 'Modules\Admin\Controllers'], function () {
    Route::get('/Admins','AdminController@login');
    //Route::get('/admin','AdminController@login');
    Route::get('/register', 'AdminController@register');
});


Route::group(['namespace' => 'Http\Controllers'], function () {

    Route::get('manageMailChimp', 'NewsletterController@manageMailChimp');
    Route::post('subscribe-newsletter',['as'=>'subscribe','uses'=>'NewsletterController@subscribe']);
    Route::post('sendCompaign',['as'=>'sendCompaign','uses'=>'NewsletterController@sendCompaign']);
    //Route::post('subscribe-newsletter',['as'=>'subscribeInsert','uses'=>'NewsletterController@subscribeInsert']);
    //Route::any('subscribe-newsletter', 'HomeController@subscribeInsert');

    Route::post('/advance_ajaxfilter/getproducts','ProductController@filterProduct');
    Route::post('/post-review','ProductController@saveReview')->name('review');
    Route::post('/post-question','ProductController@saveQuestion')->name('question');
    
    Route::any('/mattress_mail','MatressController@sendMattress_mail');

    Route::any('/test','MatressController@test');
    Route::any('/test1','ProductController@test1');
        
    Route::get('/langSwitch/{lang}', 'HomeController@postChangeLanguage');

    //Blog
    Route::any('/{lang}/feed', [
        'uses' => 'BlogController@rss',
        'as' => 'rss'
    ]);

    Route::any('/{lang}/blog', [
        'uses' => 'BlogController@index',
        'as' => 'index'
    ]);

    Route::get('/{lang}/blog', [
        'uses' => 'BlogController@index',
        'as' => 'ajax-pagination'
    ]);

    


    Route::any('/{lang}/blog'.'c-{slug}', [
        'uses' => 'BlogController@showCategory',
        'as' => 'showCategory'
    ]);

    Route::any('/{lang}/blog'.'{slug}', [
        'uses' => 'BlogController@showPost',
        'as' => 'showPost'
    ]);

    Route::any('/ajax/getblog', [
        'uses' => 'BlogController@getblog',
        'as' => 'getblog'
    ]);

    //Category Submenu
    Route::any('/{lang}/blog/{slug}', [
        'uses' => 'BlogController@showCategoryPosts',
        'as' => 'showPageCategory'
    ]);

    Route::any('/{lang}/blog/{slug}/{child}', [
        'uses' => 'BlogController@showSinglePostPage',
        'as' => 'showPostPage'
    ]);
    
    //E-catalogue
    Route::any('/{lang}/e-catalogue', 'MatressController@vieweCatalogue');
    Route::any('/{lang}/e-catalogue/{newpage}', 'MatressController@showflipcatalog');

    Route::post('/search/product', 'ProductController@searchProduct');

    Route::get('/getproduct-size', 'ProductController@getproductSize');

    //Mattress Selector
    Route::get('/{lang}/mattress-selector', 'MatressController@index');
    Route::any('/add_mattressselect', 'MatressController@add_mattress');
    Route::any('/{lang}/mattress-selector-form/{id}', 'MatressController@viewform');

    //E-warranty
    Route::get('/{lang}/e-warranty', 'HomeController@viewWarranty');
    Route::any('/e-warranty-info', 'HomeController@ewarrantyInsert');
 
    //Clear Cache facade value:
    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');
        //return '<h1>Cache facade value cleared</h1>';
        $exitCode = Artisan::call('view:clear');
        $exitCode = Artisan::call('route:clear');
        $exitCode = Artisan::call('config:cache');
        return '<h1>Cache facade value cleared</h1>';
    });

    //Reoptimized class loader:
    Route::get('/optimize', function() {
        $exitCode = Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';
    });

    //Route cache:
    Route::get('/route-cache', function() {
        $exitCode = Artisan::call('route:cache');
        return '<h1>Routes cached</h1>';
    });

    //Clear Route cache:
    Route::get('/route-clear', function() {
        $exitCode = Artisan::call('route:clear');
        return '<h1>Route cache cleared</h1>';
    });

    //Clear View cache:
    Route::get('/view-clear', function() {
        $exitCode = Artisan::call('view:clear');
        return '<h1>View cache cleared</h1>';
    });

    //Clear Config cache:
    Route::get('/config-cache', function() {
        $exitCode = Artisan::call('config:cache');
        return '<h1>Clear Config cleared</h1>';
    });

    Route::any('/user/register', ['uses' => 'Auth\AuthController@register']);
    Route::any('/auth/login', 'Auth\AuthController@postLogin');

    Route::get("/Admin/auth/logout", ["uses"=>"Auth\AuthController@getLogout"]);
    Route::get("/logout", ["uses"=>"Auth\AuthController@logout"]);

    Route::group(['middleware' => 'headerPromo'], function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::any('/enquiry', 'HomeController@enquiryInsert');

        //Refer A Friend
        Route::any('/{lang}/refer-a-friend/', 'HomeController@viewReferFriend');
        Route::get('/{lang}/stores', 'HomeController@viewStores'); 
        Route::any('/store-locator','HomeController@storeLocator');
        Route::any('/{lang}/contact','HomeController@contactshow');

        Route::any('/{lang}/{page}', 'HomeController@show');
        Route::any('/{lang}/{page}/{subPage}', 'HomeController@show');
        Route::any('/{lang}/{page}/{subPage}/{childPage}', 'HomeController@show');
        Route::any('/{lang}/{page}/{subPage}/{childPage}/{grandChildPage}', 'HomeController@show');
    });
});
// File Upload Manager //

Route::group(['middleware' => 'auth'], function () {

    Route::get('/laravel-filemanager/demo', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DemoController@index',
        'as' => 'index',
    ]);

    Route::get('/laravel-filemanager', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\LfmController@show',
        'as' => 'show',
    ]);

    // Show integration error messages
    Route::get('/laravel-filemanager/errors', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\LfmController@getErrors',
        'as' => 'getErrors',
    ]);

    // upload
    Route::any('/laravel-filemanager/upload', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\UploadController@upload',
        'as' => 'upload',
    ]);

    // list images & files
    Route::get('/laravel-filemanager/jsonitems', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ItemsController@getItems',
        'as' => 'getItems',
    ]);

    // folders
    Route::get('/laravel-filemanager/newfolder', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getAddfolder',
        'as' => 'getAddfolder',
    ]);
    Route::get('/laravel-filemanager/deletefolder', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getDeletefolder',
        'as' => 'getDeletefolder',
    ]);
    Route::get('/laravel-filemanager/folders', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getFolders',
        'as' => 'getFolders',
    ]);

    // crop
    Route::get('/laravel-filemanager/crop', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getCrop',
        'as' => 'getCrop',
    ]);
    Route::get('/laravel-filemanager/cropimage', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getCropimage',
        'as' => 'getCropimage',
    ]);
    Route::get('/laravel-filemanager/cropnewimage', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getNewCropimage',
        'as' => 'getCropimage',
    ]);

    // rename
    Route::get('/laravel-filemanager/rename', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\RenameController@getRename',
        'as' => 'getRename',
    ]);

    // scale/resize
    Route::get('/laravel-filemanager/resize', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ResizeController@getResize',
        'as' => 'getResize',
    ]);
    Route::get('/laravel-filemanager/doresize', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ResizeController@performResize',
        'as' => 'performResize',
    ]);

    // download
    Route::get('/laravel-filemanager/download', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DownloadController@getDownload',
        'as' => 'getDownload',
    ]);

    // delete
    Route::get('/laravel-filemanager/delete', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DeleteController@getDelete',
        'as' => 'getDelete',
    ]);
});
//End File Upload Manager