<?php

namespace App\Http\Controllers;

use App;
use App\Common;
use App\Http\Controllers\Controller;
use App\Register;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Mail;
use Auth;
use DB;
use Redirect;
use Response;
use Session;
use Validator;
use url;

class NewsletterController extends Controller
{


    public $mailchimp;
    public $listId = '95c09effc6';


    public function __construct(\Mailchimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    public function manageMailChimp()
    {
        return view('includes.mailchimp');
    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
	    	'email' => 'required|email'
        ]);
        try
        {
            $check_email = DB::table('newsletter_subscribe_list')->where('email',$request->input('email'))->get();

            if(empty($check_email))
            {
                $this->mailchimp
                ->lists
                ->subscribe(
                    $this->listId,
                    ['email' => $request->input('email')],
                    ['FNAME' => $request->input('fname')],
                    ['LNAME' => $request->input('lname')]
                );

                $insert_data['lang_code']= Session::get('locale');
                $insert_data['first_name']= $request['fname'];
                $insert_data['last_name']= $request['lname'];
                $insert_data['email']= $request['email'];
                $insert_data['type']= $request['submit'];
                $insert_data['created_ip']= $request->ip();
                DB::table('newsletter_subscribe_list')->insert($insert_data);
                
                return redirect()->back()->with('success','Email Subscribed Successfully');
            }
            else
            {
                return redirect()->back()->with('success','Email already exits! Please check your email and confirm your subscription.');
            }
        } catch (\Mailchimp_List_AlreadySubscribed $e) {
            return redirect()->back()->with('error','Email is Already Subscribed');
        } catch (\Mailchimp_Error $e) {
            return redirect()->back()->with('error','Error from MailChimp');
        }
    }

    public function sendCompaign(Request $request)
    {
    	$this->validate($request, [
	    	'subject' => 'required',
	    	'to_email' => 'required',
	    	'from_email' => 'required',
	    	'message' => 'required',
        ]);


        try {


	        $options = [
	        'list_id'   => $this->listId,
	        'subject' => $request->input('subject'),
	        'from_name' => $request->input('from_email'),
	        'from_email' => 'selva@creatorswebindia.com',
	        'to_name' => $request->input('to_email')
	        ];


	        $content = [
	        'html' => $request->input('message'),
	        'text' => strip_tags($request->input('message'))
	        ];


	        $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
	        $this->mailchimp->campaigns->send($campaign['id']);


        	return redirect()->back()->with('success','send campaign successfully');

        	
        } catch (Exception $e) {
        	return redirect()->back()->with('error','Error from MailChimp');
        }
    }


}

?>