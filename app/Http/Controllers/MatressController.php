<?php

namespace App\Http\Controllers;

use App\User;
use App\Register;
use App\Common;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use Redirect;
//use App\Http\Requests;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Response;
use Session;
use Cache;
use Auth;
use DB;
use Page;
//use Request;
use url;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

class MatressController extends Controller
{
    public function __construct()
    {
        $lang = Session::get('locale');
        if(!$lang)
            Session::put('locale','en');

        $langData = Common::getAllActiveLanguage();
        View::share('langData', $langData);
        
        $chatData = Common::getChatInfo();
        View::share('chat_details', $chatData);

        $csettingData = Common::getCommonSetting();
        View::share('csetting', $csettingData);

        $socialData = Common::getSocialInfo();
        View::share('social', $socialData);
    }
    
    public function index(Request $data)
    {
        $currentPath = basename($data->url());

        if(! sizeof($currentPath))
            abort(404, 'The resource you are looking for could not be found');

        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath');
        return view('mattress.view', $passData);        
    }

    public function add_mattress(Request $data)
    {
        try
        {
            unset($data['_token']);
            if($data['gender'] == 'Just for the spare bedroom')
            {
                $insert_mat['gender'] = $data['gender'];
                $insert_mat['sleep_guest'] = $data['2people'];
                $insert_mat['comfor_table'] = $data['comfor_table'];
                $insert_mat['comfortablesleep'] = $data['comfortablesleep'];
            }
            else
            {
                $insert_mat['gender'] = $data['gender'];
                $insert_mat['lookingmattress'] = $data['lookingmattress'];
                $insert_mat['weightrange'] = $data['weightrange'];

                if($data['partner_weightrange']){
                $insert_mat['partner_weightrange'] = $data['partner_weightrange'];
                }

                $insert_mat['agerange'] = $data['agerange'];

                if($data['partner_agerange']){

                $insert_mat['partner_agerange'] = $data['partner_agerange'];
                }

                $insert_mat['yourposition'] = $data['yourposition'];
                if($data['partnerposition']){
                $insert_mat['partnerposition'] = $data['partnerposition'];
                }
                $insert_mat['issues'] = $data['issues'];
                $insert_mat['hotcold'] = $data['hotcold'];
                $insert_mat['comfor_table'] = $data['comfor_table'];
                $insert_mat['comfortablesleep'] = $data['comfortablesleep'];
            }

            $insert_mat['lang_code'] = Session::get('locale');
            $insert_mat['created_by'] = $data->ip();
            $insert_mat['status'] = 1;

            $get_insert_id = DB::table('mattress-selector')->insertGetId($insert_mat);

            if(!empty($get_insert_id))
            {

                if($data['gender'] == 'Just for the spare bedroom')
                {
                    $admin_mattess = DB::select('select * from admin_matt_selector where buying_mattress like "'.$data['gender'].'" and sleep_guest like "'.$data['2people'].'" and comfortable_sleep like "'.$data['comfortablesleep'].'" and status = 1');
                }
                elseif($data['gender'] == 'Myself')
                {
                    $admin_mattess = DB::select('select * from admin_matt_selector where buying_mattress like "'.$data['gender'].'" and mattress_new like "'.$data['lookingmattress'].'" and weight_range like "'.$data['weightrange'].'" and age_range like "'.$data['agerange'].'" and your_sleep_position like "'.$data['yourposition'].'" and issues like "'.$data['issues'].'" and sleep_too like "'.$data['hotcold'].'" and comfortable_sleep like "'.$data['comfortablesleep'].'" and status = 1');
                }
                elseif($data['gender'] == 'My Partner & I' || $data['gender'] == 'My partner, I & My kids')
                {
                    $admin_mattess = DB::select('select * from admin_matt_selector where buying_mattress like "'.$data['gender'].'" and mattress_new like "'.$data['lookingmattress'].'" and weight_range like "'.$data['weightrange'].'" and partner_weight_range like "'.$data['partner_weightrange'].'" and age_range like "'.$data['agerange'].'" and partner_age_range like "'.$data['partner_agerange'].'" and your_sleep_position like "'.$data['yourposition'].'" and partner_sleep_position like "'.$data['partnerposition'].'" and issues like "'.$data['issues'].'" and sleep_too like "'.$data['hotcold'].'" and comfortable_sleep like "'.$data['comfortablesleep'].'" and status = 1');
                }

                $getdata = DB::table('mattress-selector')->Where('id',$get_insert_id)->first();

                if(sizeof($admin_mattess))
                {
                    $admin_matt_product = DB::table('admin_matt_selector_product')->Where('admin_matt_id',$admin_mattess[0]->admin_matt_id)->get();

                    foreach ($admin_matt_product as $key => $row)
                    {
                        $recommandProduct[$key] = DB::table('product')
                        ->join('product_description','product_description.product_id','=','product.product_id')
                        ->Where('product.product_id',$row->recommandation_product)
                        ->Where('product.status',1)->get();

                        $relateProduct[$key] = DB::table('product')
                        ->join('product_description','product_description.product_id','=','product.product_id')
                        ->Where('product.product_id',$row->related_product)
                        ->Where('product.status',1)->get();
                    }
                    return view('mattress.show')->with('mat_val',$getdata)->with('admin_mattess',$admin_mattess)->with('recommandProduct',$recommandProduct)->with('relateProduct',$relateProduct);
                }
                else
                {
                    return view('mattress.show')->with('mat_val',$getdata)->with('admin_mattess','')->with('recommandProduct','')->with('relateProduct','');
                }
            }
            // return Redirect('/'.$lang.'/mattress-selector-form/'.$get_insert_id);
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function vieweCatalogue(Request $data)
    {
        $currentPath = basename($data->url());
        $path = $data->path();

        if(! sizeof($currentPath))
        abort(404, 'The resource you are looking for could not be found');

        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $metaData['meta_title'] = 'Mattress Catalogue';

        $ecatalogue_desc = DB::table('ecatalogue')
        ->join('ecatalogue_description','ecatalogue_description.ecatalogue_id','=','ecatalogue.id')
        ->join('language','ecatalogue.lang_code','=','language.lang_code')
        ->Where('ecatalogue.lang_code', Session::get('locale') )
        ->Where('language.status',1)
        ->groupBy('ecatalogue.id')
        ->get();

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','ecatalogue_desc','metaData','path');
        return view('ecatalogue.view', $passData);
    }

    public function showflipcatalog(Request $data)
    {
        $currentPath = basename($data->url());
        $path = $data->path();

       if(! sizeof($currentPath))
         abort(404, 'The resource you are looking for could not be found');

        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $ecatalogue_desc = DB::table('ecatalogue')
        ->join('ecatalogue_description','ecatalogue_description.ecatalogue_id','=','ecatalogue.id')
        ->join('language','ecatalogue.lang_code','=','language.lang_code')
        ->Where('language.status',1)
        ->Where('ecatalogue.ec_slug',$currentPath)
        ->get();

        if(! sizeof($ecatalogue_desc))
        abort(404, 'The resource you are looking for could not be found');

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','ecatalogue_desc','path');
        return view('ecatalogue.flip', $passData);
    }

   //Mattress_mail

    public function sendMattress_mail(Request $data)
    {
        try
        {
            $lang = Session::get('locale');
            if(!$lang)
                Session::put('locale','en');

            $csetting = Common::getCommonSetting();

            $getdata['mattressInfo'] = DB::table('mattress-selector')->Where('id',$data['txtmattress_selector_id'])->first();

            $getdata['userInfo'] = array('name'=>ucfirst($data['name']),'email'=>$data['email'],'phone'=>$data['phone'],'message'=>$data['message']);

            $message = view('mail.mattress_mail')
            ->with('lang',$lang)
            ->with('getdata',$getdata)
            ->with('csetting',$csetting);

            // $to = $data['email'];
            $to = "support@creatorswebindia.com";

            $subject = ucfirst($data['name'])." Send Enquiry From Mattress Selector";

             // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <lmgworld@sgp54.siteground.asia>' . "\r\n";

            $getmail = mail($to,$subject,$message,$headers);

            // if($getmail){ echo '1'; }

            //return Redirect('/'.$lang.'/mattress-selector');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function viewform(Request $data,$id,$mat_id)
    {
        $currentPath = basename($data->url());
        $path = $data->path();

        $mattress_form = explode('/', $data->path());

        $mattress_name = $mattress_form[1];
        $currentPath = $mattress_name ;

        if(!empty($mat_id))
        {
            $mattress_sel = DB::table('mattress-selector')->where('id',$mat_id)->get();
        }

        if(! sizeof($currentPath))
        abort(404, 'The resource you are looking for could not be found');


        $getMenu = DB::table('menu_main')->where('menu_status',1)->where('menu_type','M')->get();
        $getSubMenu = DB::table('menu_sub')->where('submenu_status',1)->get();

        $getutl = DB::table('menu_main')->where('menu_status',1)->where('menu_type','U')->orderby('sort_order')->get();
        $getfooterMenu = DB::table('menu_main')->where('menu_status',1)->where('menu_position','F')->orderby('sort_order')->get();

        return view('mattress.show')->with('res_menu',$getMenu)->with('sub_menu',$getSubMenu)->with('getutl',$getutl)->with('footer_menu', $getfooterMenu)->with('currentPath',$currentPath)->with('mat_val',$mattress_sel);
    }
}