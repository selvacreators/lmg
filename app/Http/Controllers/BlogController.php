<?php

namespace App\Http\Controllers;

use App;
use App\Common;
use App\Http\Controllers\Controller;
use App\Register;
use App\User;
use App\Post;
use App\Category;
use App\Option;
use App\Blog as BlogModel;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Readers\dd;
use Redirect;
use Illuminate\Session\Store;
use Page;
use Response;
use Session;
use Cache;
use Auth;
use DB;
class BlogController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    private $_lang;
    
    public function __construct()
    {
        $lang = Session::get('locale');
        if(!$lang)
            Session::put('locale','en');
        
        $this->_lang = Session::get('locale');

        $langData = Common::getAllActiveLanguage();
        View::share('langData', $langData);
        
        $chatData = Common::getChatInfo();
        View::share('chat_details', $chatData);
        
        $csettingData = Common::getCommonSetting();
        View::share('csetting', $csettingData);

        $socialData = Common::getSocialInfo();
        View::share('social', $socialData);
    }    

    protected function index(Request $data)
    {
        $currentPath = basename($data->url());
        $path = $data->path();
        $lang = $this->_lang;
        $metaData = $breadcrumbs = $feature_posts = array();

        $blogModel = new BlogModel;
        $commonModel = new Common;

        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $blog_setting = $blogModel->getBlogSettings($lang);
        $categories_menu = $blogModel->getBlogCategoriesMenu($lang);

        $category_path = '';

        if($blog_setting)
        {
            $breadcrumbs[] = array( 'name' => 'Blog', 'url' => url($path), 'last' => 1 );

            $metaData['meta_title'] = $blog_setting->meta_title;
            $metaData['meta_desc'] = $blog_setting->meta_desc;
            $metaData['meta_keyword'] = $blog_setting->meta_keyword;

            $post_ids = json_decode($blog_setting->featu_article_perpage);
            $feature_posts = $blogModel
                ->whereIn('post_id',$post_ids)
                ->where('post_status',1)
                ->orderBy('sort_order')
                ->get();
        }

        /*Fetch Blog Posts*/
        $blog_post_paginate = $blogModel->getPosts($lang);
        
        $blogList = $blogModel->latest('lang_code', 'created_at')->paginate(2);

        if ($data->ajax()) {
            return view('blog.viewblog', compact('blogList'));
        
        }

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs','metaData', 'blog_setting', 'categories_menu', 'category_path','blog_post_paginate','feature_posts','blogList');
        return view('blog.viewblog', $passData);
    }

    /*public function blogList(Request $request){

        $blogList = BlogModel::paginate(2);

        if ($request->ajax()) {
            return view('blog.viewblog', compact('blogList'));
        }
        return view('blog.viewblog',compact('blogList'));
    }*/

    public function showCategoryPosts(Request $data)
    {
        try
        {
            $currentPath = basename($data->url());
            $path = $data->path();
            $lang = $this->_lang;
            $category_path = '';

            $categoryModel = new Category;
            $blogModel = new BlogModel;
            $commonModel = new Common;

            $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility
            $res_menu = $getMenuContent['topMenu'];
            $sub_menu = $getMenuContent['subMenu'];
            $getutl = $getMenuContent['utilityMenu'];
            $footer_menu = $getMenuContent['footer_menu'];

            $blog_setting = $blogModel->getBlogSettings($lang);
            $categories_menu = $blogModel->getBlogCategoriesMenu($lang);
            $category_path = '';

            if($blog_setting)
            {
                $exp = explode('/', $path);
                $breadcrumbs[] = array( 'name' => 'Blog', 'url' => url($exp[0],[$exp[1]]));
                $breadcrumbs[] = array( 'name' => ucwords( str_replace('-', ' ', $currentPath)), 'url' => url($path), 'last' => 1 );

                $metaData['meta_title'] = $blog_setting->meta_title;
                $metaData['meta_desc'] = $blog_setting->meta_desc;
                $metaData['meta_keyword'] = $blog_setting->meta_keyword;

                $post_ids = json_decode($blog_setting->featu_article_perpage);
                $feature_posts = $blogModel
                    ->whereIn('post_id',$post_ids)
                    ->where('post_status',1)
                    ->orderBy('sort_order')
                    ->get();
            }

            $checkCategory = $categoryModel->where('category_slug',$currentPath)->firstOrFail();
            
            $categoryId = $checkCategory->blog_categories_id;

            $blog_post_paginate = $blogModel->getPosts($lang, $categoryId);

            $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs','metaData', 'blog_setting', 'categories_menu', 'category_path','blog_post_paginate','feature_posts');
            return view('blog.viewblog', $passData);

        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function showSinglePostPage(Request $data)
    {
        try
        {
            $currentPath = basename( str_replace('.html', '', $data->url()) );
            $path = $data->path();
            $lang = $this->_lang;
            $category_path = '';

            $categoryModel = new Category;
            $blogModel = new BlogModel;
            $commonModel = new Common;

            $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility
            $res_menu = $getMenuContent['topMenu'];
            $sub_menu = $getMenuContent['subMenu'];
            $getutl = $getMenuContent['utilityMenu'];
            $footer_menu = $getMenuContent['footer_menu'];

            $blog_setting = $blogModel->getBlogSettings($lang);
            $categories_menu = $blogModel->getBlogCategoriesMenu($lang);
            $category_path = '';

            if($blog_setting)
            {
                $exp = explode('/', $path);
                $breadcrumbs[] = array( 'name' => 'Blog', 'url' => url($exp[0],[$exp[1]]));
                $breadcrumbs[] = array( 'name' => ucwords( str_replace('-', ' ', $exp[2])), 'url' => url($exp[0],[$exp[1],$exp[2]]));
                $breadcrumbs[] = array( 'name' => ucwords( str_replace('-', ' ', $currentPath)), 'url' => url($path), 'last' => 1 );

                $metaData['meta_title'] = $blog_setting->meta_title;
                $metaData['meta_desc'] = $blog_setting->meta_desc;
                $metaData['meta_keyword'] = $blog_setting->meta_keyword;
            }

            $blog_post  = $blogModel->where('post_slug',$currentPath)->firstOrFail();

            $blog_realtedpost = $blogModel->getRelatedPosts($lang,$blog_post->post_id);

            $blog_realtedpost = $this->array_flatten($blog_realtedpost);

            $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs','metaData', 'blog_setting', 'categories_menu', 'category_path','blog_post','feature_posts','blog_realtedpost');
            return view('blog.detailblog', $passData);
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    function array_flatten($array) { 
      if (!is_array($array)) { 
        return FALSE; 
    } 
    $result = array(); 
    foreach ($array as $key => $value) { 
        if (is_array($value)) { 
          $result = array_merge($result, array_flatten($value)); 
      } 
      else { 
          $result[$key] = $value; 
      } 
  } 
  return $result; 
}
}