<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Register;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use Redirect;
use App\Http\Requests;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Response;
use Session;
use Cache;
use Auth;
use DB;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    // public function enquiryInsert(Request $data)
    // {
    //     dd($_POST);
    //     // $store =  Register::register($data);
    //     // return Redirect('/admin')->with('message',$store);
    // }

    public function register(Request $data)
    {
        $store =  Register::register($data);
        return Redirect('/admin')->with('message',$store);
    }

    public function getLogout(Request $data)
    {
        Auth::logout();// log the user out of our application
        Session::flush();
        return Redirect::to('/admin')->with('message','You logged out!');
    }

    public function logout(Request $data)
    {
        Auth::logout();// log the user out of our application
        Session::flush();
        return Redirect::to('/admin');
    }

    public function postLogin(Request $data)
    {
        try
        {
            $email = $data['email'];
            $password = $data['password'];
            if (Auth::attempt(['email' => $email, 'password' => $password]))
            {
                $udata = User::Where('email',$email)->Where('status',1)->get();
                if(sizeof($udata))
                {
                    $utype = $udata[0]->user_type;

                    if($utype == 5)
                    {
                        Session::set('id', $udata[0]->id);
                        Session::set('reg_id', $udata[0]->profile_id);
                        Session::set('user_type', $udata[0]->user_type);
                        Session::set('email', $udata[0]->email);
                        Session::set('name', $udata[0]->name);

                        return Redirect('Admin/dashboard');
                    }
                    elseif($utype == 1)
                    {
                        Session::set('id', $udata[0]->id);
                        Session::set('reg_id', $udata[0]->profile_id);
                        Session::set('user_type', $udata[0]->user_type);
                        Session::set('email', $udata[0]->email);
                        Session::set('name', $udata[0]->name);

                        return Redirect('Admin/dashboard');
                    }
                    else
                    {
                        return Redirect('/admin')->with('message','Something Wrong!');
                    }
                }
                else
                {
                    return Redirect('/admin')->with('message','Invalid Username and Password');
                }
            }
            else
            {
                return Redirect('/admin')->with('message','Invalid Username and Password');
            }
        }
        catch (Exception $Exception)
        {
            return 'Something went wrong';
        }
    }
}
