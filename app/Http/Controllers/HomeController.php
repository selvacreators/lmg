<?php

namespace App\Http\Controllers;

use App;
use App\Common;
use App\Http\Controllers\Controller;
use App\Register;
use App\User;
use App\Product;
use Auth;
use Cache;
use DB;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Readers\dd;
use Mail;
use Page;
use Redirect;
use Response;
use Session;
use Validator;
use url;

class HomeController extends Controller
{
    private $_lang;
    public function __construct()
    {
        $lang = Session::get('locale');
        if(!$lang)
            Session::put('locale','en');
        
        $this->_lang = Session::get('locale');

        $langData = Common::getAllActiveLanguage();
        View::share('langData', $langData);

        $chatData = Common::getChatInfo();
        View::share('chat_details', $chatData);
        
        $csettingData = Common::getCommonSetting();
        View::share('csetting', $csettingData);

        $socialData = Common::getSocialInfo();
        View::share('social', $socialData);

        $ExistIntent = Common::getPoupExistIntent();
        View::share('Exist_Intent', $ExistIntent);

        $popup = Common::getPoupNewsletter();
        View::share('popup', $popup);
    }
    public function index(Request $data)
    {
        $videostatsus = DB::table('video')
        ->where('video_status',1)
        ->where('home_show_status',1)
        ->get();
        $currentPath = basename($_SERVER['PHP_SELF'],'.php');
        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        if(!empty($videostatsus))
        {
            if($videostatsus[0]->home_show_status == '1')
            {
                $video = DB::table('video')
                ->where('video_status',1)
                ->where('home_show_status',1)
                ->get();

                $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','video');
                return view('home.view', $passData);
            }
            else
            {
                $slider = DB::table('slider')
                    ->where('slider_status',1)
                    ->get();

                $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','slider');
                return view('home.slider', $passData);                
            }
        }
        else
        {
            $slider = DB::table('slider')
                ->where('slider_status',1)
                ->get();

            $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','slider');
            return view('home.slider', $passData);
        }
    }

    public function postChangeLanguage($language)
    {
        $chkLang = Common::checkLang($language);

        if($chkLang) 
        {
            Session::put('locale',$language);
            App::setLocale($language);
        }
        $redirect = $url = redirect()->back()->getTargetUrl();
        $url = str_replace( url() , '', $url);
        $url = explode('/', $url);

        // dd(redirect()->back()->getTargetUrl());
        if(isset($url[1]))
        {
            if($url[1] != '')
            {
                $redirect = str_replace('/'.$url[1].'/', '/'.$language.'/', $redirect);
            }
        }
        // dd($redirect);
        return redirect($redirect);
    }

    public function show($lang, Request $data)
    {
        $chkLang = Common::checkLang($lang);
        if(!$chkLang) abort(404);

        $urlString = trim( str_replace($this->_lang.'/', '', $data->path()) , '/');
        $urls = explode('/', $urlString);
        $common = new Common;

        if(isset($urls[0]))
        {
            if( ! $checkParms = $common->checkPageType($urls[0]) )
                abort(404);

            // dd($checkParms);
            if($checkParms->menu_type == 2) //Call Brand Collection Method
            {
                if(sizeof($urls))
                {
                    // dd(sizeof($urls));
                    if(sizeof($urls) == 1)
                        return app('App\Http\Controllers\HomeController')->viewbrands( $data );
                    elseif(sizeof($urls) == 2)
                        return app('App\Http\Controllers\HomeController')->viewcollection( $data );
                    elseif(sizeof($urls) == 3)
                        return app('App\Http\Controllers\HomeController')->viewsubcollection( $data );
                    elseif(sizeof($urls) == 4)
                        return app('App\Http\Controllers\ProductController')->viewproduct( $data );
                }
            }

            if($checkParms->menu_type == 1) //Call Brand Category Method
            {
                // dd(sizeof($urls));
                if(sizeof($urls) == 1)
                    
                    return app('App\Http\Controllers\HomeController')->categoryHandler( $data );
            
                elseif(sizeof($urls) == 2)
                {
                    $endPage = end($urls);
                    //dd($endPage);
                    if(strpos($endPage, '.html'))
                        return app('App\Http\Controllers\ProductController')->viewproduct( $data , 'category');
                    else
                        return app('App\Http\Controllers\HomeController')->categoryHandler( $data );
                }
                elseif(sizeof($urls) == 3)
                    return app('App\Http\Controllers\ProductController')->viewproduct( $data , 'category');
            }

            if($checkParms->menu_type == 3) //Call Pages Handler Method
                return app('App\Http\Controllers\HomeController')->pagesHandler( $data );
        }
        abort(404);
        exit;
        
        $buildMenuArray = array();
        foreach ($urls as $key => $url) {
            $checkParms = $common->checkPageType($url);
            $buildMenuArray[] = $checkParms;
        }
    }

    private function pagesHandler(Request $data)
    {
        $currentPath = basename($data->url());
        $path = $data->path();
        $lang = $this->_lang;

        $getdata = DB::table('pages')
            ->where('slug',$currentPath)
            ->where('pages.lang_code',$lang)
            ->where('status',1)
            ->first();

            // dd($getdata);

        // if(! sizeof($getdata))
        //     abort(404, 'The resource you are looking for could not be found');

        // dd($getdata);

        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        // $metaData = $breadcrumbs = $brandsetting = array();
        
        $breadcrumbs[] = array( 'name' => $getdata->page_title, 'url' => url($path), 'last' => 1 );

        $metaData['meta_title'] = $getdata->meta_title;
        $metaData['meta_desc'] = $getdata->meta_desc;
        $metaData['meta_keyword'] = $getdata->meta_keyword;

        // return view('home.page')->with('getdata',$getdata)->with('res_menu',$getMenu)->with('sub_menu',$getSubMenu)->with('getutl',$getutl)->with('breadcrumbs',$breadcrumbs)->with('footer_menu', $getfooterMenu)
        // ->with('metaData', $metaData)
        // ->with('currentPath',$currentPath);

        $passData = compact('getdata','res_menu','sub_menu','getutl','footer_menu','currentPath','breadcrumbs','metaData');
        // dd($passData);
        return view('home.page', $passData);
    }

    public function viewbrands(Request $data)
    {
        $currentPath = basename($data->url());
        $path = $data->path();
        $lang = $this->_lang;
        $metaData = $breadcrumbs = $brandsetting = array();

        $commonModel = new Common;

        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility
        $brandsetting = $commonModel->getBrandSetting();
        // dd($brandsetting);

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $brand = $commonModel->getBrands();
        // dd($brand);

        if(! sizeof($brand))
            abort(404, 'The resource you are looking for could not be found');

        if($brandsetting)
        {
            $breadcrumbs[] = array( 'name' => $brandsetting->page_tittle, 'url' => url($path), 'last' => 1 );

            $metaData['meta_title'] = $brandsetting->meta_title;
            $metaData['meta_desc'] = $brandsetting->meta_desc;
            $metaData['meta_keyword'] = $brandsetting->meta_keywords;
        }

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs','metaData','brand','brandsetting');

        return view('brand.view', $passData);
    }

    public function viewcollection(Request $data)
    {
        try
        {
            $currentPath = basename($data->url());
            $path = $data->path();
            $lang = $this->_lang;
            
            // $collection = DB::select('select * from brands b INNER JOIN collection c on c.brand_id = b.brand_id where b.brand_status = 1 and c.col_status = 1 and b.btn_url = "'.$currentPath.'" and b.lang_code = "'.$lang.'"');

            // $collection = DB::select('select * from brands b INNER JOIN brand_description bd on b.brand_id = bd.brand_id INNER JOIN collection c on c.brand_id = b.brand_id where b.brand_status = 1 and c.col_status = 1 and bd.btn_url = "'.$currentPath.'" and bd.lang_code = "'.$lang.'"');

            $collection = DB::select('select * from brands b INNER JOIN brands_description bd on b.brand_id = bd.brand_id INNER JOIN collection c on c.brand_id = b.brand_id INNER JOIN collection_description cd on c.collection_id = cd.collection_id where b.brand_status = 1 and c.col_status = 1 and bd.btn_url = "'.$currentPath.'" and bd.lang_code = "'.$lang.'" and cd.lang_code = "'.$lang.'"');

            // $collection = DB::select('select * from brands b INNER JOIN brand_description bd on b.brand_id = bd.brand_id INNER JOIN collection c on c.brand_id = b.brand_id where b.brand_status = 1 and c.col_status = 1 and bd.btn_url = "americanstar" and bd.lang_code = "en"');

            // dd($collection);//americanstar

            // $collection = DB::select('select * from brands b INNER JOIN brand_description bd on b.brand_id = bd.brand_id INNER JOIN collection c on c.brand_id = b.brand_id INNER JOIN collection_description cd on cd.collection_id = c.collection_id where b.brand_status = 1 and c.col_status = 1 and bd.btn_url = "'.$currentPath.'" and bd.lang_code = "'.$lang.'"');

            // dd($collection);

            //INNER JOIN collection_description cd on cd.collection_id = b.collection_id

            if(! sizeof($collection))
            abort(404, 'The resource you are looking for could not be found');

            $commonModel = new Common;
            $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

            $res_menu = $getMenuContent['topMenu'];
            $sub_menu = $getMenuContent['subMenu'];
            $getutl = $getMenuContent['utilityMenu'];
            $footer_menu = $getMenuContent['footer_menu'];

            $brandsetting = $commonModel->getBrandSetting();
            
            $exp = explode('/', $path);
            $breadcrumbs[] = array( 'name' => $brandsetting->page_tittle, 'url' => url($exp[0],[$exp[1]]));
            $breadcrumbs[] = array( 'name' => $collection[0]->brand_name, 'url' => url($path), 'last' => 1 );

            $metaData['meta_title'] = $collection[0]->brand_meta_title;
            $metaData['meta_desc'] = $collection[0]->brand_meta_desc;
            $metaData['meta_keyword'] = $collection[0]->brand_meta_keyword;

            $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs','metaData','collection');

            // dd($passData);
            return view('collection.view', $passData);
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function categoryHandler(Request $data)
    {
        try
        {
            $currentPath = basename($data->url());
            $path = $data->path();
            $brand__CollectionName = explode('/', $data->path());
            $currentPath1 = $brand__CollectionName[1];
            $lang = $this->_lang;

            $colection_path = $brand__CollectionName[0].'/'.$brand__CollectionName[1];

            $collection = DB::select('select c.categories_id,cd.* from categories_description cd 
                join categories c on c.categories_id = cd.categories_id
                where c.status = 1 and cd.category_slug = "'.$currentPath.'" and cd.lang_code = "'.$lang.'"');

            if(! sizeof($collection))
                abort(404, 'The resource you are looking for could not be found');

            $categories_id = $collection[0]->categories_id;

            $getproductImage = DB::select('select * from product p
            INNER JOIN product_description pd on p.product_id =  pd.product_id INNER JOIN product_to_category ptc on ptc.product_id = p.product_id where p.status = 1 and ptc.category_id = "'.$categories_id.'" and pd.lang_code = "'.$lang.'"');

            $getAllProductIds = array_column( $getproductImage, 'product_id');
            if(!sizeof($getAllProductIds))
            {
                $getAllProductIds = array_map(function($e) {
                    return is_object($e) ? $e->product_id : $e['product_id'];
                }, $getproductImage);
            }

            $productObj = new Product;
            $filters = $productObj->makeFilterOption($getAllProductIds);

            //get product category Azhar 15-5-2018
            $getcategory = DB::select(' select * from product p
            INNER JOIN product_to_category pc on p.product_id =  pc.product_id
            INNER JOIN product_description pd on p.product_id =  pd.product_id
            INNER JOIN categories c on pc.category_id = c.categories_id
            INNER JOIN categories_description cd on pc.category_id = cd.categories_id
            where p.status = 1 and c.status = 1 and cd.category_slug = "'.$currentPath.'" and pd.lang_code = "'.$lang.'" and cd.lang_code = "'.$lang.'" ');

            $menu_list = DB::select(' select * from menu_main m INNER JOIN menu_sub ms on m.menu_id = ms.menu_id where m.menu_status = 1 and ms.submenu_status = 1 and m.menu_link = "'.$currentPath1.'" and m.lang_code = "'.$lang.'" ');

            //---------------------------------------------------
            $commonModel = new Common;
            $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

            $res_menu = $getMenuContent['topMenu'];
            $sub_menu = $getMenuContent['subMenu'];
            $getutl = $getMenuContent['utilityMenu'];
            $footer_menu = $getMenuContent['footer_menu'];

            $promo_banner = DB::table('promo_banner')->Where('promo_banner.lang_code',$lang)->Where('banner_status',1)->get();
            
            /*BREADCRUMB CODE*/
            $exp = explode('/', $path);
            if(isset($exp[2]))
            {
                $breadcrumbs[] = array( 'name' => ucwords( str_replace('-', ' ', $exp[1])), 'url' => url( $exp[0], [$exp[1]] ));
                $breadcrumbs[] = array( 'name' => ucwords( str_replace('-', ' ', $exp[2])), 'url' => url( $exp[0], [$exp[1],$exp[2]] ), 'last' => 1);
            }
            else
                $breadcrumbs[] = array( 'name' => ucwords( str_replace('-', ' ', $exp[1])), 'url' => url( $exp[0], [$exp[1]] ), 'last' => 1);

            $metaData['meta_title'] = $collection[0]->meta_title;
            $metaData['meta_desc'] = $collection[0]->meta_description;
            $metaData['meta_keyword'] = $collection[0]->meta_keywords;

            $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs','metaData','collection','col_name','brands_Name','getproductImage','getcategory','colection_path','promo_banner','filters','menu_list','currentPath1');
            // dd($passdata);
            return view('collection.subcollection',$passData);

            // return view('collection.subcollection')->with('collection',$getcollection)->with('res_menu',$getMenu)->with('sub_menu',$getSubMenu)->with('getutl',$getutl)->with('footer_menu', $getfooterMenu)->with('currentPath',$currentPath)->with('getproductImage',$getproductImage)->with('path',$path)->with('breadcrumbs',$breadcrumbs)->with('getcategory',$getcategory)->with('filters',$getFilters)->with('promo_banner',$promo_banner)->with('menu_list',$menu_list)->with('currentPath1',$currentPath1)
            // ->with('metaData', $metaData)
            // ->with('colection_path',$colection_path);

        } catch (\Illuminate\Database\QueryException $exception) {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function viewsubcollection(Request $data)
    {
        try
        {
            $currentPath = basename($data->url());
            $path = $data->path();
            $brand__CollectionName = explode('/', $data->path());
            $lang = $this->_lang;

            $brands_Name = $brand__CollectionName[2];
            $colection_path = $brand__CollectionName[0].'/'.$brand__CollectionName[1].'/'.$brand__CollectionName[2];

            // $collection = DB::select('select * from brands b INNER JOIN collection c on c.brand_id = b.brand_id where b.brand_status = 1 and c.col_status = 1 and c.collection_btn_url = "'.$currentPath.'" and b.lang_code = "'.$lang.'"');

            $collection = DB::select('select * from brands b INNER JOIN brands_description bd on b.brand_id = bd.brand_id INNER JOIN collection c on c.brand_id = b.brand_id INNER JOIN collection_description cd on c.collection_id = cd.collection_id where b.brand_status = 1 and c.col_status = 1 and cd.collection_btn_url = "'.$currentPath.'" and cd.lang_code = "'.$lang.'"');

            // dd($collection);

            // $collection = DB::select('select * from brands b INNER JOIN brand_description bd on b.brand_id = bd.brand_id INNER JOIN collection c on c.brand_id = b.brand_id where b.brand_status = 1 and c.col_status = 1 and cd.collection_btn_url = "'.$currentPath.'" and bd.lang_code = "'.$lang.'"');

            // $collection = DB::select('select * from brands b INNER JOIN brand_description bd on b.brand_id = bd.brand_id INNER JOIN collection c on c.brand_id = b.brand_id INNER JOIN collection_description cd on c.collection_id = cd.collection_id where b.brand_status = 1 and c.col_status = 1 and bd.btn_url = "'.$currentPath.'" and bd.lang_code = "'.$lang.'" and cd.lang_code = "'.$lang.'"');

            // dd($collection);

            if(! sizeof($collection))
            abort(404, 'The resource you are looking for could not be found');

            $getproductImage = DB::select('select * from product p INNER JOIN product_to_collection pc on p.product_id =  pc.product_id INNER JOIN product_description pd on p.product_id =  pd.product_id INNER JOIN collection c on pc.collection_id = c.collection_id INNER JOIN collection_description cd on c.collection_id = cd.collection_id where p.status = 1 and c.col_status = 1 and cd.collection_btn_url = "'.$currentPath.'" and pd.lang_code = "'.$lang.'" and cd.lang_code = "'.$lang.'"');

            // dd($getproductImage); //"americanstar-latexco"

            //get product category Azhar 15-5-2018
            $getcategory = DB::select('select * from product p
                INNER JOIN product_to_category pc on p.product_id =  pc.product_id
                INNER JOIN product_description pd on p.product_id =  pd.product_id
                INNER JOIN categories c on pc.category_id = c.categories_id
                INNER JOIN categories_description cd on pc.category_id = cd.categories_id
                where p.status = 1 and c.status = 1 and cd.category_slug = "'.$currentPath.'" and pd.lang_code = "'.$lang.'" and cd.lang_code = "'.$lang.'"');

            $col_name = DB::table('collection')
                ->join('collection_description','collection.collection_id','=','collection_description.collection_id')
                ->join('brands','brands.brand_id','=','collection.brand_id')
                ->join('brands_description as bd','brands.brand_id','=','bd.brand_id')
                ->Where('brands.brand_status',1)
                ->Where('collection.col_status',1)
                ->Where('bd.btn_url',$brands_Name)
                ->Where('bd.lang_code',$lang)
                ->Where('collection_description.lang_code',$lang)
                ->get();

                // dd($col_name);

            //Show Promo Banner

            $promo_banner = DB::table('promo_banner')->Where('promo_banner.lang_code',$lang)->Where('banner_status',1)->get();

            $commonModel = new Common;

            $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

            $res_menu = $getMenuContent['topMenu'];
            $sub_menu = $getMenuContent['subMenu'];
            $getutl = $getMenuContent['utilityMenu'];
            $footer_menu = $getMenuContent['footer_menu'];
            
            /*BREADCRUMB CODE*/
            $brandsetting = $commonModel->getBrandSetting();

            $exp = explode('/', $path);
            $breadcrumbs[] = array( 'name' => $brandsetting->page_tittle, 'url' => url( $exp[0], [$exp[1]] ));
            $breadcrumbs[] = array( 'name' => $collection[0]->brand_name, 'url' => url( $exp[0], [$exp[1],$exp[2]] ) );
            $breadcrumbs[] = array( 'name' => $collection[0]->collection_name, 'url' => url($path), 'last' => 1 );

            $metaData['meta_title'] = $collection[0]->meta_title;
            $metaData['meta_desc'] = $collection[0]->meta_desc;
            $metaData['meta_keyword'] = $collection[0]->meta_keyword;

            $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs','metaData','collection','col_name','brands_Name','getproductImage','getcategory','colection_path','promo_banner');
            return view('collection.subcollection',$passData);
            
        } catch (\Illuminate\Database\QueryException $exception) {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function viewStores(Request $data)
    {
        $currentPath = basename($data->url());
        $lang = $this->_lang;
        $StoreCollection = DB::table('store_locator')->where('lang_code',$lang)->where('store_status',1)->get();

        if(! sizeof($StoreCollection))
            abort(404, 'The resource you are looking for could not be found');

        foreach($StoreCollection as $key => $row)
        {
            $nama_kabkot = $row->store_name;
            $longitude = $row->store_longitude;
            $latitudem = $row->store_latitude;

            /* Each row is added as a new array */
            // $locations[]=array( 'name'=>$nama_kabkot, 'lat'=>$latitudem, 'lng'=>$longitude );
        }
        /* Convert data to json */
        // $markers = json_encode( $locations );

        $commonModel = new Common;

        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $apikey = $commonModel->getCommonSetting();

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','StoreCollection','apikey');
        return view('store.view',$passData);

        // return view('store.view')->with('res_menu',$getMenu)->with('sub_menu',$getSubMenu)->with('getutl',$getutl)->with('footer_menu', $getfooterMenu)->with('currentPath',$currentPath)->with('StoreCollection',$store_locator)->with('apikey',$googleApi);
    }

    // public function storeLocator(Request $data)
    // {
    //     $currentPath = basename($data->url());
    //     $path = $data->path();

    //     $commonModel = new Common;

    //     $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

    //     $res_menu = $getMenuContent['topMenu'];
    //     $sub_menu = $getMenuContent['subMenu'];
    //     $getutl = $getMenuContent['utilityMenu'];
    //     $footer_menu = $getMenuContent['footer_menu'];

    //     $breadcrumbs[] = array( 'name' => 'Contact Us', 'url' => url($path), 'last' => 1 );

    //     $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','breadcrumbs');
    //     return view('home.storeLocator', $passData);
    // }

    public function contactshow(Request $data)
    {
        $currentPath = basename($data->url());
        $path = $data->path();
            
        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $csettings = $commonModel->getCommonSetting();
        // dd($csettings);

        if(! sizeof($csettings))
            abort(404, 'The resource you are looking for could not be found');

        $breadcrumbs[] = array( 'name' => 'Contact Us', 'url' => url($path), 'last' => 1 );

        // $csettings = array('phone'=>$csettingData->phone,'chat'=>$csettingData->chat,'open_time'=>$csettingData->open_time,'email'=>$csettingData->email,'address'=>$csettingData->address);

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','csettings','path','breadcrumbs');
        return view('home.contact',$passData);
    }

    public function enquiryInsert(Request $data)
    {
        try
        {
            $insert_data['lang_code']= Session::get('locale');
            $insert_data['name']= $data['name'];
            $insert_data['email']= $data['email'];
            $insert_data['mobile']= $data['telephone'];
            $insert_data['comment']= $data['content'];
            $insert_data['created_ip']= $data->ip();
            $insert_data['type']= $data['submit'];
            
            DB::table('enquiry_info')->insert($insert_data);

            $to = "support@creatorswebindia.com";
            // $to = "Sales@lmgworld.com, Contact@lmgworld.com";
            $subject = ucfirst($data['name'])." send you a Enquiry Form";

            $message = "
            <p>Hi Admin, ".$data['name']." send message from LMG World enquiry form and details as follow</p>
            <table>
            <tr>
            <th>Name</th>
            <td>".$data['name']."</td>
            </tr>
            <tr>
            <th>Email</th>
            <td>".$data['email']."</td>
            </tr>
            <tr>
            <th>Mobile</th>
            <td>".$data['telephone']."</td>
            </tr>
            <tr>
            <th>Content</th>
            <td>".$data['content']."</td>
            </tr>
            </table>";

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <lmgworld@sgp54.siteground.asia>' . "\r\n";

            // $getmail = mail($to,$subject,$message,$headers);

            Session::flash('enq_msg','Thanks for this submission we will contact you shortly');
            return Redirect($insert_data['lang_code'].'/contact');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function ewarrantyInsert(Request $data)
    {
        try
        {
            unset($data['_token']);

            $product_type = explode("$$",$data['product_type']);
            $product_size = explode("$$",$data['product_size']);

            $insert_data['salutation']= $data['salutation'];
            $insert_data['name']= $data['name'];
            $insert_data['email']= $data['email'];
            $insert_data['phone']= $data['telephone'];
            $insert_data['dob']= $data['date_of_birth'];
            $insert_data['address']= $data['address'];
            $insert_data['postal_code']= $data['postal'];
            $insert_data['purchase_invoice_num']= $data['purchase_invoice_number'];
            $insert_data['place_of_purchase']= $data['place_of_purchase'];
            $insert_data['date_of_purchase']= $data['date_of_purchase'];
            $insert_data['date_of_delivery']= $data['date_of_delivery'];
            $insert_data['model_name']= $data['model_name'];
            $insert_data['serial_number']= $data['serial_number'];
            $insert_data['product_type']= $product_type[1];
            $insert_data['product_size']= $data['product_size'];
            $insert_data['purchase_experience']= $data['purchase_experience'];
            $insert_data['purchase_delivery']= $data['purchase_delivery'];
            $insert_data['message']= $data['message'];
            $insert_data['created_at']= date("Y-m-d H:i:s");
            $insert_data['lang_code']= Session::get('locale');
            $insert_data['updated_at']= date("Y-m-d H:i:s");
            $insert_data['created_ip']= $data->ip();

            // dd($insert_data);

            DB::table('e_warranty')->insert($insert_data);

            $to = "support@creatorswebindia.com";
            $subject = ucfirst($data['name'])." send you a Enquiry Form";

            $message = "
            <p>Hi Admin, ".$data['name']." send message from LMG World E-Warranty form and details as follow</p>
            <table>
            <tr>
            <th>Name</th>
            <td>".$data['salutation'].' '.$data['name']."</td>
            </tr>
            <tr>
            <th>Email</th>
            <td>".$data['email']."</td>
            </tr>
            <tr>
            <th>Phone</th>
            <td>".$data['telephone']."</td>
            </tr>
            <tr>
            <th>Date Of Birth</th>
            <td>".$data['date_of_birth']."</td>
            </tr>
            <tr>
            <th>Address</th>
            <td>".$data['address']."</td>
            </tr>
            <tr>
            <th>Postal Code</th>
            <td>".$data['postal']."</td>
            </tr>
            <tr>
            <th>Purchase Invoice Number</th>
            <td>".$data['purchase_invoice_number']."</td>
            </tr>
            <tr>
            <th>Place Of Purchase</th>
            <td>".$data['place_of_purchase']."</td>
            </tr>
            <tr>
            <th>Date Of Purchase</th>
            <td>".$data['date_of_purchase']."</td>
            </tr>
            <tr>
            <th>Date Of Delivery</th>
            <td>".$data['date_of_delivery']."</td>
            </tr>
            <tr>
            <th>Model Name</th>
            <td>".$data['model_name']."</td>
            </tr>
            <tr>
            <th>Serial Number</th>
            <td>".$data['serial_number']."</td>
            </tr>
            <tr>
            <th>Product Type</th>
            <td>".$product_type[1]."</td>
            </tr>
            <tr>
            <th>Product Size</th>
            <td>".$product_size[1]."</td>
            </tr>
            <tr>
            <th>Purchase Experience</th>
            <td>".$data['purchase_experience']."</td>
            </tr>
            <tr>
            <th>Purchase Delivery</th>
            <td>".$data['purchase_delivery']."</td>
            </tr>
            <tr>
            <th>Message</th>
            <td>".$data['message']."</td>
            </tr>
            <tr>
            <th>Language Code</th>
            <td>".$insert_data['lang_code']."</td>
            </tr>
            <tr>
            <th>IP Address</th>
            <td>".$insert_data['created_ip']."</td>
            </tr>
            </table>";

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <lmgworld@sgp54.siteground.asia>' . "\r\n";

            $getmail = mail($to,$subject,$message,$headers);
            Session::flash('ewarranty_msg','Thanks for this submission we will contact you shortly');
            return Redirect($insert_data['lang_code'].'/e-warranty');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function viewWarranty(Request $data)
    {
        $currentPath = basename($data->url());
        $lang = $this->_lang;

        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $productType = DB::table('product_type as pt')
            ->select('pt.*','ptd.*')
          ->join('product_type_desc as ptd','pt.id','=','ptd.product_type_id')
          ->join('product as p','pt.id','=','p.ptype_id')
          ->Where('pt.status',1)
          ->Where('ptd.lang_code',$lang)
          ->orderBy('pt.sort_order')
          ->groupBy('ptype_name')
          // ->groupBy('ptd.ptype_name')
          ->get();

        // dd($productType);

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','productType');
        return view('e-warranty.view',$passData);
    }

    public function viewReferFriend(Request $data)
    {
        $currentPath = basename($data->url());

        //dd($currentPath);
        // $brand = DB::table('brands')->where('brand_status',1)->orderBy('sort_order')->get();
        // if(! sizeof($brand))
        // abort(404, 'The resource you are looking for could not be found');

        $commonModel = new Common;

        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath');
        return view('referral.view_refer_friend',$passData);
    }
}