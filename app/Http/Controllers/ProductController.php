<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use App\Register;
use App\Common;
use App\Reviews;
use App\Questions;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use Redirect;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Response;
use Session;
use Cache;
use Auth;
use Mail;
use DB;
use Page;
use url;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

class ProductController extends Controller
{
    public function __construct()
    {
        $lang = Session::get('locale');
        if(!$lang)
            Session::put('locale','en');

        $langData = Common::getAllActiveLanguage();
        View::share('langData', $langData);
        
        $chatData = Common::getChatInfo();
        View::share('chat_details', $chatData);

        // $csettingData = Common::getCommonSetting();
        // View::share('csetting', $csettingData);

        // $socialData = Common::getSocialInfo();
        // dd($socialData);
        // View::share('social', $socialData);
        
        // $currentPath = basename($_SERVER['PHP_SELF'],'.php');

        // $metaData = Common::getPageMetaDetails($currentPath);
        // View::share('metaData', $metaData);
    }

    // public function old_test1()
    // {
    //     $slug = 'mattresses';

    //     $baseQuery = 'select p.product_id
    //     from categories_description cd 
    //     join product_to_category ptc on ptc.category_id = cd.categories_id
    //     join product p on p.product_id = ptc.product_id
    //     where cd.category_slug = "'.$slug.'"';

    //     $baseProductSelect = DB::select($baseQuery);
    //     $getProductIds = array_column($baseProductSelect, 'product_id');
    //     if(sizeof($getProductIds))
    //     {
    //         $productIds = implode(',', $getProductIds);

    //         $products = array();

    //         $sql = '';
    //         if(isset($request->attribute_value) && sizeof($request->attribute_value))
    //         {
    //             $commonSearchArray = $brandSearchArry = array();
    //             foreach ($request->attribute_value as $column => $values) {
    //                 foreach($values as $value)
    //                 {
    //                     if($column == 'brands')
    //                         $brandSearchArry[] = $value;
    //                     else
    //                         $commonSearchArray[$column][] = $value;
    //                 }
    //             }

    //             if(sizeof( $commonSearchArray ))
    //             {
    //                 $commonSearchKey = ' and (';
    //                 foreach($commonSearchArray as $column => $values)
    //                 {
    //                     if(sizeof( $values ))
    //                     {
    //                         foreach($values as $value)
    //                             $commonSearchKey .= '(ad.name = "'.$column.'" and pa.text like "%'.$value.'%") or';
    //                     }
    //                 }

    //                 $commonSearchKey = rtrim($commonSearchKey,'or');
    //                 $sql .= $commonSearchKey.')';

    //             }

    //             if(sizeof($brandSearchArry))
    //             {
    //                 $brandSearchKey = '';
    //                 foreach ($brandSearchArry as $value) {
    //                     $brandSearchKey .= '"'.$value.'",';
    //                 }
    //                 $brandSearchKey = rtrim($brandSearchKey, ',');
    //                 $sql .= 'and b.brand_name in ('.$brandSearchKey.')';
    //             }
    //         }

    //         $productQuery = "select 
    //         pa.text as product_text, 
    //         ad.name as attribute_name, 
    //         b.brand_name, 
    //         cd.category_name,
    //         pd.name,
    //         product_slug,
    //         short_desc,
    //         prod_slider_image,
    //         comfort
    //         from product p 
    //         left join product_attibute pa on p.product_id = pa.product_id 
    //         left join product_description pd on pd.product_id = p.product_id 
    //         left join attribute a on a.attribute_id = pa.attribute_id 
    //         left join attribute_description ad on ad.attribute_id = pa.attribute_id 
    //         left join attribute_group_description agd on agd.attribute_group_id = a.attribute_group_id 
    //         left join brands b on b.brand_id = p.brand_id 
    //         left join product_to_category ptc on ptc.product_id = p.product_id 
    //         left join categories_description cd on cd.categories_id = ptc.category_id 
    //         where 
    //         p.product_id in (".$productIds.") ".$sql." group by p.product_id";

    //         $filterProduct = DB::select($productQuery);

    //         // dd($filterProduct);

    //         // $request->path='';

    //         $request = 'en/mattresses';

    //         if(sizeof($filterProduct))
    //         {
    //             foreach ($filterProduct as $pro) {
    //                 $products[] = array(
    //                     'product_slug' => url($request,[$pro->product_slug]).'.html',
    //                     'prod_slider_image' => $pro->prod_slider_image,
    //                     'comfort' => $pro->comfort,
    //                     'desc' => html_entity_decode($pro->short_desc),
    //                     'name' => $pro->name,
    //                 );
    //             }
    //         }

    //         dd($products);

    //         return $products;
    //     }
    //     return FALSE;
    // }

    public function test1(Request $data)
    {
        try
        {
            $data['txtmattress_selector_id'] = 48;

            $lang = Session::get('locale');
            if(!$lang)
                Session::put('locale','en');

            $csetting = Common::getCommonSetting();
            $id = $data['txtmattress_selector_id'];

            $getdata['mattressInfo'] = DB::table('mattress-selector')->Where('id',$id)->first();

            if(sizeof($getdata['mattressInfo'])){

                $data['name'] = 'bharathi';
                $data['email'] = 'bharathiking94@gmail.com';
                $data['phone'] = '8220205237';
                $data['message'] = 'message message message message message message';


                $getdata['userInfo'] = array('name'=>$data['name'],'email'=>$data['email'],'phone'=>$data['phone'],'message'=>$data['message']);

                // $updateData['lang_code'] = $data['language'];
                // $updateData['name'] = $data['name'];
                // $updateData['email'] = $data['email'];
                // $updateData['mobile'] = $data['mobile'];
                // $updateData['comment'] = $data['comment'];
                // $updateData['status'] = $data['status'];

                DB::table('mattress-selector')
                ->where('id',$id)
                ->update($getdata['userInfo']);

                // $insert_data['menu_position']=$position;
                // $insert_data['sort_order']=$data['sortOrder'];
                // $insert_data['button_text']=$data['txtbuttontext'];

                // DB::table('menu_main')->insert($getdata['userInfo']);

                // $getdata['userInfo'] = $data['email'];
                // $getdata['userInfo'] = $data['phone'];
                // $getdata['userInfo'] = $data['message'];

                // dd($getdata);

                return view('mail.mattress_mail')
                ->with('lang',$lang)
                ->with('getdata',$getdata)
                ->with('csetting',$csetting);
            }

            // $message = view('mail.mattress_mail')
            // ->with('lang',$lang)
            // ->with('getdata',$getdata)
            // ->with('csetting',$csetting);

            // echo "<pre>";
            // print_r($message);
            die();

            $to = "support@creatorswebindia.com";
            // $to = "Sales@lmgworld.com, Contact@lmgworld.com";
            $subject = "Raja send you a Enquiry Form";

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <lmgworld@sgp54.siteground.asia>' . "\r\n";

            $getmail = mail('support@creatorswebindia.com',$subject,$message,$headers);

            // dd($getmail);

            Session::flash('enq_msg','Thanks for this submission we will contact you shortly');
            return Redirect($insert_data['lang_code'].'/contact');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function filterProduct(Request $request)
    {
        $productObj = new Product;
        $products = $productObj->filterProduct($request->currPath, $request);
        return response()->json(['record' => $products ]);
    }
    
    public function viewproduct(Request $data, $pageType = null)
    {
        $currentPath = basename( str_replace('.html', '', $data->url()) );
        $path = $data->path();

        $lang = Session::get('locale');

        $path_exp = explode('/', $path);
        $familer_path = "";

        // dd($path_exp);

        if(!empty($path_exp[3])){
            if(sizeof($path_exp) == 4){
                $endPage = end($path_exp);
                if(strpos($endPage, '.html')){

                $familer_path = $path_exp[0].'/'.$path_exp[1].'/'.$path_exp[2];
                }
                else{
                    
                $familer_path = $path_exp[0].'/'.$path_exp[1].'/'.$path_exp[2].'/'.$path_exp[3];
                }
            }
        }
        else{
            if(sizeof($path_exp) == 1){

              $familer_path = $path_exp[0].'/'.$path_exp[1].'/'.str_replace('.html', '',$path_exp[2]);
            }
            else{
                $familer_path = $path_exp[0].'/'.$path_exp[1];
            }
        }
        $product = Product::getProducts($currentPath, $lang);

        $product_id = $product['product']->product_id;

        if(! sizeof($product))
            abort(404, 'The resource you are looking for could not be found');

        $commonModel = new Common;
        $getMenuContent = $commonModel->getMenuContent(); // Get Common Menu for Header, Footer, Utility

        $res_menu = $getMenuContent['topMenu'];
        $sub_menu = $getMenuContent['subMenu'];
        $getutl = $getMenuContent['utilityMenu'];
        $footer_menu = $getMenuContent['footer_menu'];

        $review = new Reviews;
        $reviews = Reviews::where(array('status' => 1, 'product_id' => $product_id ))->get();
        $ratings = $review->getProductReviews($product_id);

        $questionObj = new Questions;
        $question = $questionObj->getProductQuestionAnswer($product_id);

        /*BREADCRUMB CODE*/
        if($pageType)
        {
            $exp = explode('/', $path);
            $breadcrumbs[] = array( 'name' => title_case($exp[1]), 'url' => url( $exp[0], [$exp[1]] ));

            if( ! strpos($exp[2], '.html') )
                $breadcrumbs[] = array( 'name' => title_case($exp[2]), 'url' => url( $exp[0], [$exp[1],$exp[2]] ) );
            $breadcrumbs[] = array( 'name' => $product['product']->name, 'url' => url($path), 'last' => 1 );
        }
        else
        {
            $brandsetting = $commonModel->getBrandSetting();
            $exp = explode('/', $path);

            $breadcrumbs[] = array( 'name' => $brandsetting->page_tittle, 'url' => url( $exp[0], [$exp[1]] ));
            $breadcrumbs[] = array( 'name' => $product['product']->brand_name, 'url' => url( $exp[0], [$exp[1],$exp[2]] ) );
            $breadcrumbs[] = array( 'name' => $product['product_collections']->collection_name, 'url' => url( $exp[0], [$exp[1],$exp[2],$exp[3]] ));
            $breadcrumbs[] = array( 'name' => $product['product']->name, 'url' => url($path), 'last' => 1 );
        }
        
        $metaData['meta_title'] = $product['product']->meta_title;
        $metaData['meta_desc'] = $product['product']->meta_desc;
        $metaData['meta_keyword'] = $product['product']->meta_keyword;

        $passData = compact('res_menu','sub_menu','getutl','footer_menu','currentPath','path','product','breadcrumbs','metaData','ratings','question','reviews','familer_path');
        return view('product.view', $passData);
    }

    public function getproductSize(Request $data)
    {
        // $ptypeId = 6;
        // $product_type = DB::table('product_dimension')
        //     ->join('length_class_description', 'product_dimension.length_class_id', '=', 'length_class_description.length_class_id')
        //     ->where('product_dimension.ptype_id',$ptypeId)
        //     ->get();
        // dd($product_type);
        // $product_type = DB::table('product_dimension')
        //     ->join('length_class_description', 'product_dimension.length_class_id', '=', 'length_class_description.length_class_id')
        //     ->get();
        // $product_type = DB::table('product_dimension')->where('ptype_id',$ptypeId)->get();

        $ptypeId = Input::get('type');
        try
        {
            $product_type = DB::table('product_dimension')
            ->join('length_class_description', 'product_dimension.length_class_id', '=', 'length_class_description.length_class_id')
            ->where('product_dimension.ptype_id',$ptypeId)
            ->get();

            echo json_encode($product_type);
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function saveReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'review_product' => 'required',
            // 'review_title' => 'required|max:120',
            // 'name' => 'required|max:120',
            // 'email' => 'required|email',
            // 'summary' => 'required|min:3|max:600',
            // 'rating' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator,'product_errors')
            ->withInput();
        }

        $review = new Reviews;

        $review->product_id = $request->review_product;
        $review->lang_code = $request->lang;
        $review->title = $request->review_title;
        $review->name = $request->name;
        $review->emailid = $request->email;
        $review->text = $request->summary;
        $review->rating = $request->rating;
        $review->created_at = date("Y-m-d H:i:s");
        $review->created_by = $request->ip();

        $review->save();

        return back()->with('successReview','Review Added Successfully...');
    }

    public function saveQuestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question_product' => 'required',
            'name' => 'required|max:120',
            'email' => 'required|email',
            'summary' => 'required|min:3|max:600',
        ]);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator,'question_errors')
            ->withInput();
        }

        $question = new Questions;

        $question->product_id = $request->question_product;
        // $question->user_id = 4;
        $question->lang_code = $request->lang;
        $question->name = $request->name;
        $question->emailid = $request->email;
        $question->question = $request->summary;
        $question->created_at = date("Y-m-d H:i:s");
        $question->created_by = $request->ip();

        $question->save();

        return back()->with('successQuestion','We receive your question and we will replay shortly...');
    }

    public function searchProduct()
    {
        $keyword = Input::get('keyword');
        $lang = Session::get('locale');

        /*Get Pages*/
        $pagesSearch = DB::table('pages')->where('status',1)
                    ->select('page_name as pageName','slug as pageUrl')
                    ->where('page_name', 'like', '%'.$keyword.'%')
                    ->get();

        /*Get Brands*/
        $brandSearch = DB::table('brands as b')->where('brand_status',1)
                    ->select('brand_name as pageName','btn_url as pageUrl')
                    ->join('brands_description as bd', 'bd.brand_id', '=', 'b.brand_id')
                    ->where('brand_name', 'like', '%'.$keyword.'%')
                    ->where('bd.lang_code',$lang)
                    ->get();

        /*Get Collection*/
        $collectionSearch = DB::table('collection as c')->where('col_status',1)
                    ->select('collection_name as pageName',
                        DB::raw('CONCAT(btn_url,"/",collection_slug) as pageUrl')
                        )
                    ->join('collection_description as cd', 'c.collection_id', '=', 'cd.collection_id')
                    ->join('brands as b', 'c.brand_id', '=', 'b.brand_id')
                    ->join('brands_description as bd', 'b.brand_id', '=', 'bd.brand_id')
                    ->where('bd.lang_code',$lang)
                    ->where('cd.lang_code',$lang)
                    ->where('brand_status',1)
                    ->where('collection_name', 'like', $keyword.'%')
                    ->get();

        /*Get Products*/
        $productSearch = DB::table('product_description as pd')
                            ->select( DB::raw(' CONCAT(category_slug, "/", product_slug) as pageUrl ') ,'prod_slider_image','pd.name as pageName', 'short_desc')
                            ->join('product as p', 'p.product_id', '=', 'pd.product_id')
                            ->join('product_to_category as ptc', 'p.product_id', '=', 'ptc.product_id')
                            ->join('categories as c', 'c.categories_id', '=', 'ptc.category_id')
                            ->join('categories_description as cd', 'cd.categories_id', '=', 'c.categories_id')
                            ->where('pd.name', 'like', '%'.$keyword.'%')
                            ->where('c.parent_id', '0')
                            ->where('cd.lang_code',$lang)
                            ->get();

        return response()->json([
            'keyword' => $keyword, 
            'pages' => $pagesSearch,
            'brands' => $brandSearch,
            'collections' => $collectionSearch,
            'products' => $productSearch,
            'langCode' => $lang
        ]);
    }
}
