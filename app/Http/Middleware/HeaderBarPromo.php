<?php

namespace App\Http\Middleware;

use Closure;
use View;
use App\HeaderPromo;

class HeaderBarPromo
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headerPromoObj = new HeaderPromo;
        $promoContent = $headerPromoObj->getPromoDetail();
        View::share('headerBarPromo', $promoContent);
        return $next($request);
    }

}