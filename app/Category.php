<?php 
namespace App;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent {
    
    protected $table = 'blog_categories';
    
    protected $fillable = ['category_name', 'category_slug'];

    public $timestamps = FALSE;
    
    function getUrlAttribute() {
        return '/blog/'.'c-'.$this->slug.'/';
    }
}