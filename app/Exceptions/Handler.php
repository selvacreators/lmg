<?php

namespace App\Exceptions;

use \App\Common;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if($e instanceof NotFoundHttpException) {

            $menuData = Common::getMenuContent();
            $langData = Common::getAllActiveLanguage();
            $csettingData = Common::getCommonSetting();
            $socialData = Common::getSocialInfo();
            return response()->view('errors.pageNotFound', 
                [
                    'res_menu' => $menuData['topMenu'],
                    'sub_menu' => $menuData['subMenu'],
                    'getutl' => $menuData['utilityMenu'],
                    'footer_menu' => $menuData['footer_menu'],
                    'langData' => $langData,
                    'csetting' => $csettingData,
                    'social' => $socialData,
                    'currentPath' => basename($request->url()),
                ],404);
        }
		
        if ($e instanceof \Illuminate\Session\TokenMismatchException) {
            return Redirect('/admin')->with('message','The login form session expired, please try again.');
        }
		
        return parent::render($request, $e);
    }
}
