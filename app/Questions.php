<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Questions extends Model
{
    protected $table = 'product_question';
    protected $primaryKey = 'question_id';

    public function getProductQuestionAnswer($product_id)
    {
        $question = $answer = 0;
        $ratingData = array();
        $ratingData['questions'] = $question;
        $ratingData['answers'] = $answer;
        $ratingData['datas'] = array();

        $ratings = DB::table( $this->table )
                    ->where('status',1)
                    ->where('product_id',$product_id)
                    ->get();

        if($ratings)
        {
            foreach($ratings as $rating)
            {
                $data['username'] = $rating->name;
                $data['question'] = $rating->question;
                $data['answer'] = $rating->answer;
                $data['created_on'] = $rating->created_at;
                array_push($ratingData['datas'], $data);
                if($rating->question) $question++;
                if($rating->answer) $answer++;
            }
            $ratingData['questions'] = $question;
            $ratingData['answers'] = $answer;
        }
        return $ratingData;
    }
}