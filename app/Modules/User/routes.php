<?php 
/*
|---------------------------------------------------------------------------
| ModuleOne Module Routes
|---------------------------------------------------------------------------
|
| All the routes related to the ModuleOne module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/
Route::group(['module' => 'User', 'namespace' => 'App\Modules\User\Controllers'], function (){

  Route::any('/Admin/users','UserController@adminUsersList');
  Route::any('/Admin/add/user','UserController@createUser');
  Route::any('/Admin/insert/admin-user','UserController@insertUser');
  Route::any('/Admin/edit/{id}/user','UserController@editUser');
  Route::any('/Admin/update/admin-user','UserController@updateUser');
  Route::any('/Admin/delete/{id}/user','UserController@deleteUser');
  Route::any('/Admin/check/user','UserController@checkUser');

  Route::any('/Admin/edit/admin-profile','UserController@editAdminProfile');

});
?>