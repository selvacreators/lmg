<?php
namespace App\Modules\User\Controllers;

use Image;
use App;
use File;
use App\HeaderPromo;
use App\Http\Controllers\Controller;
use App\Http\Requests;
//use App\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Session\Store;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Session;
use Cache;
use Excel;
use PDF;
use DB;
//use Request;

class UserController extends Controller
{
	// public function __construct(Store $session)
	// {
	// 	$this->session = $session;
	// 	// $this->middleware('auth', ['except' => []]);
	// }

	public function adminUsersList()
	{
		$users = DB::table('users')->get();
		return view('users.viewAdminUser')->with('users',$users);
	}
	public function createUser()
	{
		return view('users.addAdminUser');
	}
	public function insertUser(Request $data)
	{
		$insert_data['name']=$data['name'];
		$insert_data['email']=$data['username'];
		$insert_data['user_type']=1;
		$insert_data['password']=password_hash($data['password'], PASSWORD_DEFAULT);
		$insert_data['created_at']= date("Y-m-d H:i:s");
		$insert_data['status']=$data['status'];

		DB::table('users')->insert($insert_data);
		return Redirect('/Admin/users/')->with('message','New User Added Successfully...');
	}
	public function editUser($id)
	{
		$users = DB::table('users')->where('id',$id)->first();
		return view('users.editAdminUser')->with('users',$users);
	}
	public function editAdminProfile()
	{
		// dd($this->session);
		$adminId = Session::get('id');
		// dd($adminId);
		$users = DB::table('users')->where('id',$adminId)->where('user_type',5)->first();
		// if(!$users)
		// 	return Redirect('/Admins');
		return view('users.editAdminProfile')->with('users',$users);
	}
	public function updateUser(Request $data)
	{
		$id = $data['uid'];
		$users = DB::table('users')->where('id',$id)->first();

		if(!empty($data['password']))
		{
			$update_data['password']=password_hash($data['password'], PASSWORD_DEFAULT);
		}
		if(!empty($data['status']))
		{
			$update_data['status']=$data['status'];
		}

		$update_data['name']=$data['name'];
		$update_data['email']=$data['username'];
		$update_data['updated_at']= date("Y-m-d H:i:s");

		DB::table('users')->where('id',$users->id)->update($update_data);
		return Redirect('/Admin/users/')->with('message','User Updated Successfully...');
	}
	public function deleteUser($id)
	{
		$users = DB::table('users')->where('id',$id)->delete();
		return Redirect('/Admin/users/')->with('message','User Deleted Successfully...');
	}
	public function checkUser(Request $data)
    {
        try
        {
            $userData = DB::table('users')->where('email',strtolower($data['getuser']))->first();
            if(!empty($userData))
            {
                if($userData->email != '')
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
          $a = $exception->errorInfo;
          return view('errors.custom')->with('message',end($a));
        }
    }
}
?>