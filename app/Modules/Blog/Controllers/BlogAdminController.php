<?php 

namespace App\Modules\Blog\Controllers;

use Image;
use App;
use File;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Post;
use App\Category;
use App\Option;
//use App\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Session\Store;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Session;
use Cache;
use DB;

//use Request;
class BlogAdminController extends Controller {

    public function __construct(Store $session)
    {
         $this->session = $session;
         $this->middleware('auth', ['except' => []]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
    */
    public function index()
    {
        $language = DB::table('language')->Where('status',1)->get();
        $blogs = DB::table('blog_post')->orderBy('sort_order')->get();
        return view('Adminblog.blogpost')->with('language',$language)->with('blogs',$blogs);
    }
    public function createBlog()
    {
        $blogs = DB::table('blog_post')->get();
        $blogCategories = DB::table('blog_categories')->Where('status',1)->get();
        $language = DB::table('language')->Where('status',1)->get();
        return view('Adminblog.addpost')->with('language',$language)->with('blogCategories',$blogCategories)->with('blogs',$blogs);
    }
    public function check_postSlug(Request $data)
    {
        try
        {
            $postSlug = DB::table('blog_post')->where('post_slug',strtolower(str_replace(' ', '-', $data['getpost'])))->get();
            if(!empty($postSlug))
            {
                if($postSlug[0]->post_slug != '')
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
          $a = $exception->errorInfo;
          return view('errors.custom')->with('message',end($a));
        }
    }
    public function blog_insert(Request $data)
    {
        try
        {
            $rand = substr(md5(microtime()),rand(0,26),5);
            $post_slug = strtolower(str_replace(' ', '-', $data['post_slug']));
            $categoryData = NULL;

            if(!empty(Input::file('blog_banner')))
            {
                $blog_banner = Input::file('blog_banner');
                $extension = pathinfo($blog_banner->getClientOriginalName(), PATHINFO_EXTENSION);

                $fileName = $post_slug.'-blogfeature-'.$rand.'.'.$extension;

                $uploadDir  = public_path() . "/images/blog/";

                if (!file_exists($uploadDir))
                File::makeDirectory($uploadDir);

                if (Input::file('blog_banner')->move($uploadDir, $fileName))
                {
                    $blog_banner = $fileName;
                }
            }
            else
            {
                $blog_banner = "";
            }

            unset($data['_token']);

            if(sizeof($data['category']))
            {
                foreach ($data['category'] as $key => $row)
                {
                    $categoryData[$key] = $row;
                }
                $categoryData = json_encode($categoryData);
            }

            $insert_data['lang_code']=$data['lang_code'];
            $insert_data['post_title']=$data['post_title'];
            $insert_data['post_slug']=$post_slug;
            $insert_data['post_content']=$data['content'];
            $insert_data['post_tags']=$data['tags'];
            $insert_data['blog_banner']=$blog_banner;
            $insert_data['category_id']=$categoryData;
            $insert_data['short_desc']=$data['short_desc'];
            $insert_data['post_content']=$data['content'];
            $insert_data['meta_title']=$data['meta_title'];
            $insert_data['meta_keyword']=$data['meta_keyword'];
            $insert_data['meta_desc']=$data['meta_desc'];
            $insert_data['featured_article']=$data['featured_article'];
            $insert_data['post_status']=$data['status'];
            $insert_data['sort_order']=$data['sort_order'];
            $insert_data['created_ip']=$data->ip();
            $insert_data['publish_date']=$data['published_at'];
            $insert_data['created_at']=date("Y-m-d H:i:s");

            $binsertId = DB::table('blog_post')->insertGetId($insert_data);

            if(sizeof($data['rposts']))
            {
                foreach ($data['rposts'] as $key => $row)
                {
                    $insert_rpost_data['bpost_id']=$binsertId;
                    $insert_rpost_data['rposts_id']=$row;
                    DB::table('related_blogs')->insert($insert_rpost_data);
                }
            }

            if(sizeof($data['category']))
            {
                foreach ($data['category'] as $key => $row)
                {
                    $insert_cate_data['post_id'] = $binsertId;
                    $insert_cate_data['categories_id'] = $row;
                    DB::table('post_category')->insert($insert_cate_data);
                }
                //$categoryData = json_encode($insert_cate_data);
            }

            return Redirect('/admin/blog')->with('message','New Blog Created Successfully...');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }
    public function edit_blog(Request $data,$id)
    {
        $blogs = DB::table('blog_post')->get();
        $blogPosts = DB::table('blog_post')->where('post_id',$id)->first();

        $post_category = DB::table('post_category')->where('post_id',$id)->get();

        if(!$blogPosts)
        return Redirect('/admin/blog');

        $rblogs = DB::table('related_blogs')->Where('bpost_id',$id)->get();
        $blogCategories = DB::table('blog_categories')->Where('status',1)->get();
        $language = DB::table('language')->Where('status',1)->get();

        return view('Adminblog.editpost')->with('language',$language)->with('blogCategories',$blogCategories)->with('blogPosts',$blogPosts)->with('blogs',$blogs)->with('rblogs',$rblogs)->with('post_category',$post_category);
    }
    public function blog_update($id,Request $data)
    {
        try
        {
            $rand = substr(md5(microtime()),rand(0,26),5);
            $post_slug = strtolower(str_replace(' ', '-', $data['post_slug']));
            $categoryData = '';

            if(!empty(Input::file('blog_banner')))
            {
                $blog_banner = Input::file('blog_banner');
                $extension = pathinfo($blog_banner->getClientOriginalName(), PATHINFO_EXTENSION);

                $fileName = $post_slug.'-blogfeature-'.$rand.'.'.$extension;

                $uploadDir  = public_path() . "/images/blog/";

                @File::delete($uploadDir.$data['Insert_blog_banner']);

                if (!file_exists($uploadDir))
                File::makeDirectory($uploadDir);

                if (Input::file('blog_banner')->move($uploadDir, $fileName))
                {
                    $blog_banner = $fileName;
                }
            }
            else
            {
                $blog_banner = $data['Insert_blog_banner'];
            }
            unset($data['_token']);

            // if(sizeof($data['category']))
            // {
            //     foreach ($data['category'] as $key => $row)
            //     {
            //         $insert_cate_data[$key]=$row;
            //     }
            //     $categoryData = json_encode($insert_cate_data);
            // }

            if(sizeof($data['category']))
            {
                foreach ($data['category'] as $key => $row)
                {
                    $categoryData[$key] = $row;
                }
                $categoryData = json_encode($categoryData);
            }
            
            $update_data['lang_code']=$data['lang_code'];
            $update_data['post_title']=$data['post_title'];
            $update_data['post_slug']=$post_slug;
            $update_data['post_content']=$data['content'];
            $update_data['post_tags']=$data['tags'];
            $update_data['blog_banner']=$blog_banner;
            $update_data['category_id']=$categoryData;
            $update_data['short_desc']=$data['short_desc'];
            $update_data['post_content']=$data['content'];
            $update_data['meta_title']=$data['meta_title'];
            $update_data['meta_keyword']=$data['meta_keyword'];
            $update_data['meta_desc']=$data['meta_desc'];
            $update_data['featured_article']=$data['featured_article'];
            $update_data['post_status']=$data['status'];
            $update_data['sort_order']=$data['sort_order'];
            $update_data['created_ip']=$data->ip();
            $update_data['publish_date']=$data['published_at'];
            $update_data['updated_at']=date("Y-m-d H:i:s");

            DB::table('blog_post')->Where('post_id',$id)->update($update_data);

            DB::table('related_blogs')->Where('bpost_id',$id)->delete();
            if(sizeof($data['rposts']))
            {
                foreach ($data['rposts'] as $key => $row)
                {
                    $insert_rpost_data['bpost_id']=$id;
                    $insert_rpost_data['rposts_id']=$row;
                    DB::table('related_blogs')->insert($insert_rpost_data);
                }
            }

            DB::table('post_category')->Where('post_id',$id)->delete();

            if(sizeof($data['category']))
            {
                foreach ($data['category'] as $key => $row)
                {
                    $insert_cate_data['post_id'] = $id;
                    $insert_cate_data['categories_id'] = $row;
                    
                    DB::table('post_category')->insert($insert_cate_data);
                }
                //$categoryData = json_encode($insert_cate_data);
            }

            return Redirect('/admin/blog')->with('message','Blog Updated Successfully...');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }
    public function blog_delete(Request $data,$id)
    {
        $blog = DB::table('blog_post')->where('post_id',$id)->get();
        
        if(sizeof($blog))
        {
            $uploadDir  = public_path() . "/images/blog/";
            DB::table('related_blogs')->Where('bpost_id',$id)->delete();
            DB::table('blog_post')->Where('post_id',$id)->delete();
            @File::delete($uploadDir.$blog[0]->blog_banner);
        }
        return Redirect('/admin/blog')->with('message','Blog Deleted Successfully...');
    }
    public function viewcategory()
    {
        $language = DB::table('language')->Where('status',1)->get();
        $blogCategories = DB::table('blog_categories')->Where('status',1)->get();
        return view('Adminblog.category')->with('language',$language)->with('blogCategories',$blogCategories);
    }
    public function category_create()
    {
        $language = DB::table('language')->Where('status',1)->get();
        $blogCategories = DB::table('blog_categories')->Where('status',1)->get();
        return view('Adminblog.addcategory')->with('language',$language)->with('blogCategories',$blogCategories);
    }
    public function category_insert(Request $data)
    {
        try
        {
            unset($data['_token']);
            $insert_data['lang_code']=$data['lang_code'];
            $insert_data['category_name']=$data['category_name'];
            $insert_data['category_slug']=strtolower(str_replace(' ', '-', $data['category_slug']));

            $insert_data['parent_id']=$data['parentid'];
            $insert_data['meta_title']=$data['meta_title'];
            $insert_data['meta_keyword']=$data['meta_keyword'];
            $insert_data['meta_desc']=$data['meta_desc'];
            $insert_data['status']=$data['status'];

            DB::table('blog_categories')->insert($insert_data);

            return Redirect('/admin/blog/category/')->with('message','New Blog Category Created Successfully...');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }
    public function check_categorySlug(Request $data)
    {
        try
        {
            $categoryslug = DB::table('blog_categories')->where('category_slug',strtolower(str_slug($data['getcategory'])))->get();
            if(!empty($categoryslug))
            {
                if($categoryslug[0]->category_slug != '')
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
            // return $categoryslug;
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
          $a = $exception->errorInfo;
          return view('errors.custom')->with('message',end($a));
        }
    }
    public function category_update(Request $data)
    {
        try
        {
            unset($data['_token']);
            $update_data['lang_code']=$data['lang_code'];
            $update_data['category_name']=$data['category_name'];
            $update_data['category_slug']=strtolower(str_replace(' ', '-', $data['category_slug']));

            $update_data['parent_id']=$data['parentid'];
            $update_data['meta_title']=$data['meta_title'];
            $update_data['meta_keyword']=$data['meta_keyword'];
            $update_data['meta_desc']=$data['meta_desc'];
            $update_data['status']=$data['status'];

            DB::table('blog_categories')
            ->Where('blog_categories_id',$data['bcategoryid'])
            ->update($update_data);

            return Redirect('/admin/blog/category/')->with('message','Blog Category Updated Successfully...');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }
    public function edit_category(Request $data,$id)
    {
        $language = DB::table('language')->Where('status',1)->get();
        $editBlogCategories = DB::table('blog_categories')->where('blog_categories_id',$id)->Where('status',1)->get();
        $blogCategories = DB::table('blog_categories')->Where('status',1)->get();
        return view('Adminblog.editcategory')->with('language',$language)->with('blogCategories',$blogCategories)->with('editBlogCategories',$editBlogCategories);
    }
    public function deleteCategory(Request $data,$id)
    {
        DB::table('blog_categories')->where('blog_categories_id',$id)->delete();
        return Redirect('/admin/blog/category')->with('message','Mattress E-warranty Delete Successfully...');
    }

    // public function FeatureSlider_create()
    // {
    //     $language = DB::table('language')->where('status',1)->get();
    //     return view('Adminblog.addfeatureslider')->with('language',$language);
    // }
    // public function viewFeatureSlider(Request $data)
    // {
    //     $sliders = DB::table('blog_feature_slider')->get();
    //     $language = DB::table('language')->Where('status',1)->get();
    //     return view('Adminblog.featureslider')->with('sliders',$sliders)->with('language',$language);
    // }

    /*********OLD START***********/
    // public function createPost() {
    //     $categories = Category::lists('category_name', 'blog_categories_id');
        
    //     return view('Adminblog.addEdit_blog')
    //         ->withCategories($categories)
    //         ->withPostId('');
    // }
    // public function viewRss()
    // {
    //     return view('Adminblog.rss')
    //         ->withCategories(Category::all())
    //         ->withOptionRssName(Option::get('rss_name'))
    //         ->withOptionRssNumber(Option::get('rss_number'));
    // }
    // public function editPost($id) {
    //     $categories = Category::lists('category_name', 'blog_categories_id');
    //     $post= Post::find($id);
    //     return view('Adminblog.addEdit_blog')
    //         ->withCategories($categories)
    //         ->withPost($post)
    //         ->withPostId($id);
    // }

    

    public function show($slug) {
        $post = Post::whereCategorySlug($slug)->first();
        return view('blog.post.show')
            ->withPost($post);
    }

    public function showPost($slug) {
        $post = Post::whereCategorySlug($slug)->first();
        return view('blog.post.show')
            ->withPost($post);
    }

    public function formAddImage($post_id) {
        $post = Post::find($post_id);
        return view('Adminblog.image')
            ->withPost($post);
    }

    public function addImage($post_id) {
        @mkdir(public_path().'/blog/');
        @mkdir(public_path().'/blog/posts/');
        @mkdir(public_path().'/blog/posts/thumbs');
        $post = Post::find($post_id);
        $file = Input::file('image');
        $img = \Image::make($file)->fit(1000, 500);
        $filename = '/blog/posts/'.$file->getClientOriginalName();
        $img->save( public_path().$filename );
        $post->image = $file->getClientOriginalName();
        $post->save();
        return redirect('/admin/blog/');
    }

    public function ajax_post_save() {
        $fields = array_except(Input::all(), ['_token', 'post_id' ]);
        $fields['slug'] = (strlen($fields['slug']) === 0) ? Str::slug($fields['title']) : Str::slug($fields['slug']);
        if (Input::get('post_id') > 0) {
            $post = Post::find(Input::get('post_id'));
            $post->update($fields);
        } else {
            $post = Post::create($fields);
        }
        return response()->json($post);
    }
    public function ajax_post_load() {
        $post_id = Input::get('post_id');
        $post = Post::find($post_id);
        return response()->json($post);
    }
    public function ajax_post_publish() {
        $post_id = Input::get('post_id');
        $post = Post::find($post_id);
        if ($post->published_at === '0000-00-00 00:00:00') {
            $post->published_at = DB::raw('now()');
            $post->save();
        }
        return response()->json($post);
    }
    public function ajax_options_save() {
        $options = array_except(Input::all(), '_token' );
        foreach ($options as $key=>$val) {
            $option = Option::firstOrCreate( ['blog_option_name' => $key]);
            $option->value = $val;
            $option->save();
            $ret[] = $option;
        }
        return response()->json($ret);
    }


    //azhar Created Blog Setting
    public function viewsetting(Request $data){
        try{
            $blog_setting = DB::table('blog_setting')->get();
            $language = DB::table('language')->Where('status',1)->get();
            return view('Adminblog.setting.view')->with('blog_setting',$blog_setting)->with('language',$language);
            
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function addSetting(Request $data){
        try{
            // $blog_setting = DB::table('blog_setting')->get();

        $blogs = DB::table('blog_post')->get();

        $language = DB::table('language')->Where('status',1)->get();
        return view('Adminblog.setting.addblog_setting')->with('language',$language)->with('blogPost',$blogs);

        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function addblog_setting(Request $data){
         try{

            // dd($data->all());

        unset($data['_token']);

      $blog_title = $data['blog_title'];
      $rand = substr(md5(microtime()),rand(0,26),5);
      $insert_featu_article = '';

      if(!empty(Input::file('blog_banner')))
      {
        $blog_banner = Input::file('blog_banner');
        $extension = pathinfo($blog_banner->getClientOriginalName(), PATHINFO_EXTENSION);

        $fileName = strtolower(str_slug($blog_title));
        $fileName = $fileName.'-'.$rand.'.'.$extension;
        $uploadDir = public_path() . "/images/blog/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('blog_banner')->move($uploadDir, $fileName))
        {
          $can['blog_banner'] = $fileName;
        }
    }
    else
    {
        $can['blog_banner'] = "";
    }

    if(sizeof($data['featureBlogPost']))
    {
        foreach ($data['featureBlogPost'] as $key => $row)
        {
            $insert_featu_article[$key]=$row;
        }
    }
    $feArtiData = json_encode($insert_featu_article);

      $insert_data['lang_code']= $data['lang_code'];
      $insert_data['blog_tittle']=  $blog_title;
      $insert_data['no_slider_show']= $data['no_slider_show'];
      $insert_data['post_perpage']= $data['post_perpage'];
      
      $insert_data['featu_article_perpage']= $feArtiData;

      $insert_data['meta_title']=$data['meta_title'];
      $insert_data['meta_keyword']= $data['meta_keywords'];
      $insert_data['meta_desc']= $data['meta_desc'];
      $insert_data['blog_desc']= $data['blog_desc'];
      $insert_data['blog_banner']=$can['blog_banner'];
      $insert_data['status']= $data['status'];
      $insert_data['created_at']= date("Y-m-d H:i:s");

      DB::table('blog_setting')->insert($insert_data);

      return Redirect('/admin/blog/setting')->with('message','New Blog Setting Added Successfully...');
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function edit_setting(Request $data,$id){
        try{
            $blog_setting =  DB::table('blog_setting')->where('blog_setting_id',$id)->get();
            $language = DB::table('language')->Where('status',1)->get();
            $blogs = DB::table('blog_post')->Where('post_status',1)->get();

            // dd($blog_setting);

        return view('Adminblog.setting.editblog_setting')->with('blog',$blog_setting)->with('language',$language)->with('blogPost',$blogs);             
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }


    public function updated_setting(Request $data){
        try {
            unset($data['_token']);

      $blog_title = $data['blog_title'];
      $rand = substr(md5(microtime()),rand(0,26),5);
      $feArtiData = NULL;

      if(!empty(Input::file('blog_banner')))
      {
        $blog_banner = Input::file('blog_banner');
        $extension = pathinfo($blog_banner->getClientOriginalName(), PATHINFO_EXTENSION);

        $fileName = strtolower(str_slug($blog_title));
        $fileName = $fileName.'-'.$rand.'.'.$extension;
        $uploadDir = public_path() . "/images/blog/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('blog_banner')->move($uploadDir, $fileName))
        {
          $can['blog_banner'] = $fileName;
        }
      }
      else
      {
        $can['blog_banner'] = $data['insert_blog_banner'];
      }

    if(sizeof($data['featureBlogPost']))
    {
        foreach ($data['featureBlogPost'] as $key => $row)
        {
            $insert_featu_article[$key]=$row;
        }
        $feArtiData = json_encode($insert_featu_article);
    }

      $insert_data['lang_code']= $data['lang_code'];
      $insert_data['blog_tittle']=  $blog_title;
      $insert_data['no_slider_show']= $data['no_slider_show'];
      $insert_data['post_perpage']= $data['post_perpage'];
      $insert_data['meta_title']=$data['meta_title'];

      $insert_data['featu_article_perpage']= $feArtiData;
      
      $insert_data['meta_keyword']= $data['meta_keywords'];
      $insert_data['meta_desc']= $data['meta_desc'];
      $insert_data['blog_desc']= $data['blog_desc'];
      $insert_data['blog_banner']=$can['blog_banner'];
      $insert_data['status']= $data['status'];
      $insert_data['created_at']= date("Y-m-d H:i:s");

        DB::table('blog_setting')->Where('blog_setting_id',$data['blog_id'])->update($insert_data);

      return Redirect('/admin/blog/setting')->with('message','Blog Setting Updated Successfully...');
        } 
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

    public function deleteblog_setting(Request $data,$id){
        try{
        DB::table('blog_setting')->Where('blog_setting_id',$id)->delete();

        return Redirect('/admin/blog/setting')->with('message','Blog Setting Deleted Successfully...');
        } 
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
    }

}