  <?php 
/*
|---------------------------------------------------------------------------
| ModuleOne Module Routes
|---------------------------------------------------------------------------
|
| All the routes related to the ModuleOne module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/
Route::group(['prefix' => 'admin', 'module' => 'Blog', 'namespace' => 'App\Modules\Blog\Controllers'], function () {

    Route::any('/blog', [
        'uses' => 'BlogAdminController@index',
        'as' => 'index'
    ]);

    Route::any('/blog/feature-slider', [
        'uses' => 'BlogAdminController@viewFeatureSlider',
        'as' => 'viewFeatureSlider'
    ]);

    // Route::any('/blog/add/feature-slider', [
    //     'uses' => 'BlogAdminController@FeatureSlider_create',
    //     'as' => 'viewcategory'
    // ]);

    Route::any('/blog/category', [
        'uses' => 'BlogAdminController@viewcategory',
        'as' => 'viewcategory'
    ]);

    Route::any('/blog/add/category', [
        'uses' => 'BlogAdminController@category_create',
        'as' => 'category_create'
    ]);

    Route::any('/blog/check/postslug', [
        'uses' => 'BlogAdminController@check_postSlug',
        'as' => 'check_postSlug'
    ]);

    Route::any('/blog/insert/post', [
        'uses' => 'BlogAdminController@blog_insert',
        'as' => 'blog_insert'
    ]);

    Route::any('/blog/delete/{id}/post', [
        'uses' => 'BlogAdminController@blog_delete',
        'as' => 'blog_delete'
    ]);

    Route::any('/blog/update/{id}/post', [
        'uses' => 'BlogAdminController@blog_update',
        'as' => 'blog_update'
    ]);

    Route::any('/blog/{id}/edit/post', [
        'uses' => 'BlogAdminController@edit_blog',
        'as' => 'edit_blog'
    ]);

    Route::any('/blog/update/category', [
        'uses' => 'BlogAdminController@category_update',
        'as' => 'category_update'
    ]);

    Route::any('/blog/insert/category', [
        'uses' => 'BlogAdminController@category_insert',
        'as' => 'category_insert'
    ]);

    Route::any('/blog/check/categoryslug', [
        'uses' => 'BlogAdminController@check_categorySlug',
        'as' => 'check_categorySlug'
    ]);

    Route::any('/blog/{id}/edit/category', [
        'uses' => 'BlogAdminController@edit_category',
        'as' => 'edit_category'
    ]);

    // Route::any('/blog/category', [
    //     'uses' => 'BlogAdminController@viewpost',
    //     'as' => 'viewcategory'
    // ]);

    // Route::any('/blog/add/post', [
    //     'uses' => 'BlogAdminController@post_create',
    //     'as' => 'category_create'
    // ]);

    Route::any('/blog/rss', [
        'uses' => 'BlogAdminController@viewRss',
        'as' => 'viewRss'
    ]);

    Route::any('/post/create', [
        'uses' => 'BlogAdminController@createBlog',
        'as' => 'createBlog'
    ]);

    Route::any('/post/{id}/edit', [
        'uses' => 'BlogAdminController@editPost',
        'as' => 'editPost'
    ]);

    Route::any('/blog/category/{id}/edit', [
        'uses' => 'BlogAdminController@editPost',
        'as' => 'editCategory'
    ]);

    Route::any('/blog/category/{id}/delete', [
        'uses' => 'BlogAdminController@deleteCategory',
        'as' => 'deleteCategory'
    ]);

    Route::any('/post/{id}/image', [
        'uses' => 'BlogAdminController@addImage',
        'as' => 'addImage'
    ]);

    Route::any('/post/{id}/image', [
        'uses' => 'BlogAdminController@formAddImage',
        'as' => 'formAddImage'
    ]);

    Route::any('/blog/save_post', [
        'uses' => 'BlogAdminController@ajax_post_save',
        'as' => 'ajax_post_save'
    ]);

    Route::any('/blog/load_post', [
        'uses' => 'BlogAdminController@ajax_post_load',
        'as' => 'ajax_post_load'
    ]);

    Route::any('/blog/publish_post', [
        'uses' => 'BlogAdminController@ajax_post_publish',
        'as' => 'ajax_post_publish'
    ]);

    // Route::any('/blog/create_category', [
    //     'uses' => 'BlogAdminController@ajax_category_create'
    // ]);

    Route::any('/blog/save_options', [
        'uses' => 'BlogAdminController@ajax_options_save',
        'as' => 'ajax_options_save'
    ]);

    //Azhar Created Route :
     Route::any('/blog/setting', [
        'uses' => 'BlogAdminController@viewsetting',
        'as' => 'viewsetting'
    ]); 

     Route::any('/blog/add/blog-setting', [
        'uses' => 'BlogAdminController@addSetting',
        'as' => 'addSetting'
    ]);

     Route::any('/insert/blog-setting', [
        'uses' => 'BlogAdminController@addblog_setting',
        'as' => 'addblog_setting'
    ]);

     Route::any('blog/edit/{id}', [
        'uses' => 'BlogAdminController@edit_setting',
        'as' => 'edit_setting'
    ]);

     Route::any('blog/delete/{id}', [
        'uses' => 'BlogAdminController@deleteblog_setting',
        'as' => 'deleteblog_setting'
    ]);

     Route::any('update/blog-setting', [
        'uses' => 'BlogAdminController@updated_setting',
        'as' => 'updated_setting'
    ]);

    //Route::any('blog', ['uses' => 'BlogAdminController@view']);
    //Route::any('blog/category', ['uses' => 'BlogAdminController@viewcategory']);
    //Route::any('blog/rss', ['uses' => 'BlogAdminController@viewRss']);
    //Route::any('post/create', ['uses' => 'BlogAdminController@createPost']);
    //Route::any('post/{id}/edit', ['uses' => 'BlogAdminController@editPost']);
    //Route::any('post/{id}/image', ['uses' => 'BlogAdminController@addImage']);
    //Route::any('post/{id}/image', ['uses' => 'BlogAdminController@formAddImage']);
    //Route::any('blog/save_post', ['uses' => 'BlogAdminController@ajax_post_save']);
    //Route::any('blog/load_post', ['uses' => 'BlogAdminController@ajax_post_load']);
    //Route::any('blog/publish_post', ['uses' => 'BlogAdminController@ajax_post_publish']);
    //Route::any('blog/create_category', ['uses' => 'BlogAdminController@ajax_category_create']);
    //Route::any('blog/save_options', ['uses' => 'BlogAdminController@ajax_options_save']);
    

    //Route::resource('post', 'didcode\Blog\BlogPostController');

});

?>