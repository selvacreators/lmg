<?php
namespace App\Modules\LmgWorldAdmin\Controllers;

use Image;
use App;
use File;
use App\HeaderPromo;
use App\Http\Controllers\Controller;
use App\Http\Requests;
//use App\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Session\Store;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Response;
use Validator;
// use Illuminate\Support\Facades\Validator;
use Session;
use Cache;
use Excel;
use PDF;
use DB;
//use Request;

class LmgWorldAdminController extends Controller
{
	public function __construct(Store $session)
	{
		 $this->session = $session;
		 $this->middleware('auth', ['except' => []]);
	}

	public function index()
	{
		return 'index';
	}

	public function dashboard()
	{
		return view('lmgAdmin.dashboard');
	}

  public function catalog(Request $data)
  {
    return view('lmgAdmin.catalog');
  }

//Azhar created Menu type 25-5-2018
  public function menutype()
  {
    $menus = DB::table('menu_main')->get();
    return view('lmgAdmin.menu.menutype')->with('menus',$menus);
  }

  // public function menu(Request $data)
  // {
  //   $menus = DB::table('menu_main')->get();
  //   return view('lmgAdmin.menu')->with('menus',$menus);
  // }
  public function editMenutype($type)
  {
    $language = DB::table('language')->where('status',1)->get();
    $menus = DB::table('menu_main')->Where('menu_position',$type)->get();
    return view('lmgAdmin.menu.menu')->with('menus',$menus)->with('language',$language);
  }

  public function Pages(Request $data)
  {
    return view('lmgAdmin.catalog');
  }

  public function refer(Request $data)
  {
    return view('lmgAdmin.catalog');
  }

  /* E-WARRANTY INFO */

  public function viewEwarranty(Request $data)
  {
    
    $language = DB::table('language')->where('status',1)->get();

    $warranty = DB::table('e_warranty as e')
    ->select('e.ewarranty_id','e.lang_code','e.salutation','e.name','e.email','e.phone','e.purchase_invoice_num','e.place_of_purchase','e.model_name','e.status')
    ->get();

    // dd($warranty);

    //->join('product as p','p.product_id','=','q.product_id')
    // ->join('product_description as d','p.product_id','=','d.product_id')    
    //->Where('product_review.status',1)
   // ->get();
    return view('lmgAdmin.e-warranty.adminview')->with('warranty',$warranty)->with('language',$language);
  }

  public function delete_warranty(Request $data,$id)
  {
    $insert = DB::table('e_warranty')->where('ewarranty_id',$id)->delete();
    return Redirect('/Admin/e-warranty')->with('message','Mattress E-warranty Delete Successfully...');
  }

  public function editWarranty(Request $data,$id)
  {
    $language = DB::table('language')->where('status',1)->get();
    $pre_warranty = DB::table('e_warranty')->where('ewarranty_id',$id)->first();
    $productType = DB::table('product_type')->get();
    
    return view('lmgAdmin.e-warranty.adminedit')->with('language',$language)->with('warranty',$pre_warranty)->with('productType',$productType);
  }

  public function edit_warranty(Request $data)
  {
    try
    {
      unset($data['_token']);
      $user_id = Session::get('id');
      $warranty['salutation'] = $data['salutation'];
      $warranty['user_id'] = $user_id;
      $warranty['name'] = $data['name'];
      $warranty['email'] = $data['email'];
      $warranty['phone'] = $data['phone'];
      $warranty['dob'] = $data['dob'];
      $warranty['address'] = $data['address'];
      $warranty['postal_code'] = $data['postal_code'];
      $warranty['purchase_invoice_num'] = $data['purchase_invoice_num'];
      $warranty['place_of_purchase'] = $data['place_of_purchase'];
      $warranty['date_of_purchase'] = $data['date_of_purchase'];
      $warranty['date_of_delivery'] = $data['date_of_delivery'];
      $warranty['model_name'] = $data['model_name'];
      $warranty['serial_number'] = $data['serial_number'];
      $warranty['product_type'] = $data['product_type'];
      $warranty['product_size'] = $data['product_size'];
      $warranty['purchase_experience'] = $data['purchase_experience'];
      $warranty['purchase_delivery'] = $data['purchase_delivery'];
      $warranty['message'] = $data['message'];
      $warranty['lang_code'] = $data['language'];
      $warranty['purchase_delivery'] = $data['purchase_delivery'];
      $warranty['purchase_delivery'] = $data['purchase_delivery'];
      $warranty['created_at'] = date("Y-m-d H:i:s");
      $warranty['created_ip'] =  $data->ip();
      $warranty['updated_at'] = date("Y-m-d H:i:s");
      $warranty['updated_by'] =  $data->ip();
      $warranty['status'] = $data['status'];

      // dd($warranty);

      DB::table('e_warranty')
      ->Where('ewarranty_id',$data['ewarrantyId'])
      ->update($warranty);

      return Redirect('/Admin/e-warranty')->with('message','E-Warranty Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function AddMainMenu()
  {
    $pages = DB::table('pages')->where('status',1)->get();
    $language = DB::table('language')->where('status',1)->get();
    //$collection = DB::table('collection')->where('col_status',1)->get();

    $categories = DB::table('categories')
    ->join('categories_description','categories.categories_id','=','categories_description.categories_id')
    ->where('categories.status',1)
    ->get();
    //dd( $categories);
    //->with('collection',$collection)
    return view('lmgAdmin.menu.Addmenu')->with('pages',$pages)->with('categories',$categories)->with('language',$language);
  }

  public function addmenus(Request $data)
  {
    try
    {
      // dd($data->all());
      unset($data['_token']);

      $insert_data['menu_name']=$data['txtmenu'];
      //$insert_data['page_name']=$data['pagename'];
      if(!empty($data['category_slug']))
      {
        $insert_data['menu_link'] = $data['category_slug'];
      }
      else if(!empty($data['pagename']))
      {
        $insert_data['menu_link'] = $data['pagename'];
      }
      // else if(!empty($data['collection_slug'])){
      //   $insert_data['menu_link'] = $data['collection_slug'];
      // }
      else
      {
         $insert_data['menu_link']=$data['txtmenulink'];
      }

      $position = $data['menuPosition'];

      $insert_data['lang_code']=$data['lang_code'];
      $insert_data['menu_type']=$data['menutype'];
      $insert_data['menu_status']=$data['menustatus'];
      $insert_data['mega_menu']=$data['megamenu'];
      $insert_data['megamenu_content']=$data['editor1'];
      $insert_data['menu_position']=$position;
      $insert_data['sort_order']=$data['sortOrder'];
      $insert_data['button_text']=$data['txtbuttontext'];

      // dd($insert_data);

      DB::table('menu_main')->insert($insert_data);
      return Redirect('/Admin/editMenutype/'.$position)->with('message','Menu Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function editMenu(Request $data,$id)
  {
    try
    {
      $pages = DB::table('pages')->where('status',1)->get();
      
      $categories = DB::table('categories')
      ->join('categories_description','categories.categories_id','=','categories_description.categories_id')
      ->where('categories.status',1)
      ->get();

      // $collection = DB::table('collection')->where('col_status',1)->get();

      $getmenus = DB::table('menu_main')->where('menu_id',$id)->get();

      $language = DB::table('language')->where('status',1)->get();
      //dd($getmenus);
      //->with('collection',$collection)
      return view('lmgAdmin.menu.editmenu')->with('pages',$pages)->with('categories',$categories)->with('results',$getmenus)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function editmenus(Request $data)
  {
    try
    {
      unset($data['_token']);

      if(!empty($data['category_slug']))
      {
        $insert_data['menu_link'] = $data['category_slug'];
      }
      else if(!empty($data['pagename']))
      {
        $insert_data['menu_link'] = $data['pagename'];
      }
      // else if(!empty($data['collection_slug'])){
      //   $insert_data['menu_link'] = $data['collection_slug'];
      // }
      else
      {
        $insert_data['menu_link']=$data['txtmenulink'];
      }

      $list_part=DB::table('menu_main')->Where('menu_id',$data['txtmenu_id'])
      ->update(['menu_name'=>$data['txtmenu'],
        'lang_code'=>$data['lang_code'],
        'menu_type'=>$data['menutype'],
        'menu_link'=>$insert_data['menu_link'],
        'mega_menu'=>$data['megamenu'],
        'megamenu_content'=>$data['editor1'],
        'menu_status'=>$data['menustatus'],
        'menu_position'=>$data['menuPosition'],
        'sort_order'=>$data['sortOrder'],
        'button_text'=>$data['txtbuttontext']
        //'page_name'=>$data['pagename'],
      ]);

      //return Redirect('/Admin/menu')->with('message','Menu Updated Successfully...');
      return Redirect('/Admin/editMenutype/'.$data['menuPosition'])->with('message','Menu Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function deleteMenu(Request $data,$id)
  {
    try
    {
      $menuData = DB::table('menu_main')->where('menu_id', $id)->get();
      DB::table('menu_main')->where('menu_id', $id)->delete();
      return Redirect('/Admin/editMenutype/'.$menuData[0]->menu_position)->with('message','Menu Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function addSubMenu(Request $data)
  {
    try
    {
      // $getmenus = DB::table('menu_sub')->paginate(5);
      // $getmenus = \DB::connection('mysql')->select('select * from menu_main m 
      //   INNER JOIN menu_sub s on m.menu_id = s.menu_id
      //   where m.menu_status = 1 ')->paginate(5);
      // $getmenus =  \DB::connection('mysql')->table('menu_sub AS m')
      //                 //>select('c.company', 'c.id')
      //                  ->join('menu_sub AS s', 'm.menu_id ', '=', 's.menu_id ')
      //                  //->where('rc.title', urldecode($city))
      //                  //->where('UPPER(SUBSTR(company,1, 1))',  urldecode($alphabet))
      //                  ->Where('m.menu_status', '1')
      //                  ->paginate(5);

      $language = DB::table('language')->where('status',1)->get();
      $getmenus = DB::table('menu_sub')
        ->select('menu_sub.*', 'menu_main.menu_name','menu_main.lang_code')
        ->join('menu_main','menu_main.menu_id','=','menu_sub.menu_id')
        ->Where('menu_main.menu_status',1)
        ->get();

      // dd($getmenus);

      //dd( $categories);
      // return view('lmgAdmin.Addmenu')->with('pages',$pages)->with('categories',$categories)->with('collection',$collection);                         
       
      // dd($getmenus);
      return view('lmgAdmin.menu.submenu')->with('smenus',$getmenus)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function add_SubMenu(Request $data)
  {
    try
    {
      $getmenus = DB::table('menu_main')->get();
      $pages = DB::table('pages')->where('status',1)->get();
      //$collection = DB::table('collection')->where('col_status',1)->get();

      $categories = DB::table('categories')
      ->join('categories_description','categories.categories_id','=','categories_description.categories_id')
      ->where('categories.status',1)
      ->get();
      //->with('collection',$collection)
      return view('lmgAdmin.menu.AddSubmenu')->with('getmenu',$getmenus)->with('pages',$pages)->with('categories',$categories);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function addsubmenus(Request $data)
  {
    try
    {
      unset($data['_token']);
      if(!empty($data['category_slug']))
      {
        $insert_data['sub_link'] = $data['category_slug'];
      }
      else if(!empty($data['pagename']))
      {
        $insert_data['sub_link'] = $data['pagename'];
      }
      // else if(!empty($data['collection_slug'])){
      //   $insert_data['sub_link'] = $data['collection_slug'];
      // }
      else
      {
        $insert_data['sub_link']=$data['txtsubmenulink'];
      }
      $insert_data['menu_type']=$data['menutype'];
      $insert_data['menu_id']=$data['menuName'];
      $insert_data['sub_name']=$data['txtsubmenuname'];
      //$insert_data['sub_link']=$data['txtsubmenulink'];
      $insert_data['submenu_status']=$data['submenustatus'];
      DB::table('menu_sub')->insert($insert_data);
      return Redirect('/Admin/AddSubMenu')->with('message','SubMenu Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

//editsubmenu
public function editsubMenu(Request $data,$id){
try {
        $getsubmenus = DB::table('menu_sub')->where('sub_id',$id)->get();

        $getmenus = DB::table('menu_main')->where('menu_status',1)->get();  



        //$getmenus = DB::table('menu_main')->get();
$pages = DB::table('pages')->where('status',1)->get();
//$collection = DB::table('collection')->where('col_status',1)->get();

$categories = DB::table('categories')
->join('categories_description','categories.categories_id','=','categories_description.categories_id')
->where('categories.status',1)
->get();     
     //->with('collection',$collection)
        return view('lmgAdmin.menu.editsubmenu')->with('results',$getsubmenus)->with('getmenus',$getmenus)->with('pages',$pages)->with('categories',$categories);
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
}


public function edit_submenus(Request $data){
  try {
        // $getsubmenus = DB::table('menu_sub')->where('sub_id',$id)->get();

        // $getmenus = DB::table('menu_main')->get();
       // dd($data->all());

        unset($data['_token']);


                if(!empty($data['category_slug'])){

          $data['sub_link'] = $data['category_slug'];
        }
        else if(!empty($data['pagename'])){

          $data['sub_link'] = $data['pagename'];
        }
        // else if(!empty($data['collection_slug'])){

        //   $data['sub_link'] = $data['collection_slug'];
        // }
        else{

           $data['sub_link']=$data['txtsubmenulink'];
        }

        $list_part=DB::table('menu_sub')->Where('sub_id',$data['submenu_id'])
        ->update(['menu_id'=>$data['menuName'],'menu_type'=>$data['menutype'],'sub_name'=>$data['txtsubmenuname'],'sub_link'=>$data['sub_link'],'submenu_status'=>$data['submenustatus'],'updated_as'=> date("Y-m-d H:i:s")]);

        return Redirect('/Admin/AddSubMenu')->with('message','SubMenu Updated Successfully...');
     
        return view('lmgAdmin.menu.editsubmenu')->with('results',$getsubmenus)->with('getmenus',$getmenus);
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
}


public function delete_subMenu(Request $data,$id){
     try{
    $res = DB::table('menu_sub')->where('sub_id', $id)->delete();
    return Redirect('/Admin/AddSubMenu')->with('message','SubMenu Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
    }
}

  //SLIDER

  public function slider(Request $data)
  {
    $sliders = DB::table('slider')->get();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.slider.slider')->with('sliders',$sliders)->with('language',$language);
  }

  public function AddSlider(Request $data)
  {
    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.slider.addslider')->with('language',$language);
  }

  public function addslides(Request $data)
  {
    try
    {
      unset($data['_token']);
      $language = $data['language'];
      $slider_name = $data['txtslidername'];
      if(!empty(Input::file('txtslimage')))
      {
        $txtslimage = Input::file('txtslimage');
        $extension = pathinfo($txtslimage->getClientOriginalName(), PATHINFO_EXTENSION);

        $fileName = strtolower(str_slug($slider_name.'-'.$language));
        $fileName = $fileName.'.'.$extension;
        $uploadDir  = public_path() . "/slider/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('txtslimage')->move($uploadDir, $fileName))
        {
          $slider_image = $fileName;
        }
      }
      else
      {
        $slider_image = "";
      }

      $insert_data['lang_code']= $language;
      $insert_data['slider_name']= $slider_name;
      $insert_data['slider_image']=$slider_image;
      $insert_data['slider_description']= $data['txtsliderdesc'];
      $insert_data['slider_status']= $data['sliderstatus'];
      $insert_data['created_at']= date("Y-m-d H:i:s");
      DB::table('slider')->insert($insert_data);
      return Redirect('/Admin/slider')->with('message','New Slider Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //Edit Slider

  public function edit_slider(Request $data,$id)
  {
    $language = DB::table('language')->where('status',1)->get();
    $slider = DB::table('slider')->where('slider_id',$id)->get();
    return view('lmgAdmin.slider.editslider')->with('sliders',$slider)->with('language',$language);
  }

  public function editslides(Request $data)
  {
    try
    {
      unset($data['_token']);
      $language = $data['language'];
      $slider_name = $data['txtslidername'];
      if(!empty(Input::file('txtslimage')))
      {
        $txtslimage = Input::file('txtslimage');
        $extension = pathinfo($txtslimage->getClientOriginalName(), PATHINFO_EXTENSION);

        $fileName = strtolower(str_slug($slider_name.'-'.$language));
        $fileName = $fileName.'.'.$extension;
        $uploadDir  = public_path() . "/slider/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        @File::delete($uploadDir.$data['insertimage']);
        if (Input::file('txtslimage')->move($uploadDir, $fileName))
        {
          $slider_image = $fileName;
        }
      }
      else
      {
        $slider_image = $data['insertimage'];
      }

      DB::table('slider')->Where('slider_id',$data['sliderid'])
      ->update(['lang_code'=>$language,'slider_name'=>$slider_name,'slider_image'=>$slider_image,'slider_description'=>$data['txtsliderdesc'],'slider_status'=>$data['sliderstatus'],'updated_at'=> date("Y-m-d H:i:s")]);
      return Redirect('/Admin/slider')->with('message','Slider Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function deleteslider(Request $data,$id)
  {
    try
    {
      $slider = DB::table('slider')->where('slider_id',$id)->get();
      if(sizeof($slider))
      {
        $uploadDir  = public_path() . "/slider/";
        @File::delete($uploadDir.$slider[0]->slider_image);
        DB::table('slider')->where('slider_id', $id)->delete();
      }
      return Redirect('/Admin/slider')->with('message','Slider Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //VIDEO INFO

  public function viewvideo(Request $data)
  {
    $video = DB::table('video')->get();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.video.video')->with('videos',$video)->with('language',$language);
  }

  public function AddVideo(Request $data)
  {
    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.video.addvideo')->with('language',$language);
  }

  public function add_video(Request $data)
  {
    try
    {
      unset($data['_token']);
      //ini_set('upload_max_filesize', '2048M');
      $language = $data['language'];

      if(!empty(Input::file('txtvideo')))
      {
        $validator = Validator::make(Input::all(),array('file'=> 'mimes:mp4,mov,ogg | max:20000'));
        $rand = substr(md5(microtime()),rand(0,26),5);

        $image = Input::file('txtvideo');
        $extension = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
        $fileName = strtolower('lmgworld-'.$rand.'-'.$language).'.'.$extension;
        $uploadDir  = public_path() . "/slidervideo/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('txtvideo')->move($uploadDir, $fileName))
        {
          $slider_videos = $fileName;
        }
      }
      else
      {
        $slider_videos = "";
      }

      $insert_data['lang_code']= $data['language'];
      $insert_data['video_name']= $data['txtvideoname'];
      $insert_data['upload_video']=$slider_videos;
      $insert_data['home_show_status']=$data['txtshowpage'];
      $insert_data['video_status']= $data['videostatus'];
      $insert_data['created_at']= date("Y-m-d H:i:s");
      DB::table('video')->insert($insert_data);
      return Redirect('/Admin/videos')->with('message','Video Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function delete_video(Request $data,$id)
  {
    $res = DB::table('video')->where('video_id',$id)->get();

    if(sizeof($res))
    {
      $uploadDir  = public_path() . "/slidervideo/";
      DB::table('video')->where('video_id',$id)->delete();
      @File::delete($uploadDir.$res[0]->upload_video);
    }
    return Redirect('/Admin/videos')->with('message','Video Deleted Successfully...');
  }

  public function edit_video(Request $data,$id)
  {
    $language = DB::table('language')->where('status',1)->get();
    $video = DB::table('video')->where('video_id',$id)->get();
    return view('lmgAdmin.video.editvideo')->with('video',$video)->with('language',$language);
  }

  public function editvideos(Request $data)
  {
    try
    {
      unset($data['_token']);
      //ini_set('upload_max_filesize', '2048M');
      $language = $data['language'];
      $video_name = $data['txtvideoname'];

      if(!empty(Input::file('txtvideo')))
      {        
        $validator = Validator::make(Input::all(),array('file'=> 'mimes:mp4,mov,ogg | max:20000'));
        $rand = substr(md5(microtime()),rand(0,26),5);

        $image = Input::file('txtvideo');
        $extension = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
        $fileName = strtolower('lmgworld-'.$rand.'-'.$language).'.'.$extension;
        $uploadDir  = public_path() . "/slidervideo/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        @File::delete($uploadDir.$data['insertvideo']);
        if (Input::file('txtvideo')->move($uploadDir, $fileName))
        {
          $slider_videos = $fileName;
        }
      }
      else
      {
        $slider_videos = $data['insertvideo'];
      }

      DB::table('video')->Where('video_id',$data['video_id'])
      ->update(['video_name'=>$data['txtvideoname'],'lang_code'=>$language,'upload_video'=>$slider_videos,'video_status'=>$data['videostatus'],'home_show_status'=>$data['txtshowpage'],'updated_at'=> date("Y-m-d H:i:s")]);

      return Redirect('/Admin/videos')->with('message','Video Updated Successfully...');
    } 
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

 //viewUtillity
  public function viewUtillity(Request $data)
  {
    $Utillity = DB::table('menu_Utillity')->paginate(5);
    return view('lmgAdmin.viewUtillity')->with('Utillity',$Utillity);
  }
  public function add_Utillity(Request $data){

    return view('lmgAdmin.add_Utillity');

  }

  public function add_Utillitys(Request $data)
  {
    try
    {
        unset($data['_token']);
        $insert_data['Utillity_name']=$data['txtUtillity'];
        $insert_data['Utillity_link']=$data['txtUtillitylink'];
        $insert_data['Utillity_status']=$data['Utillitystatus'];

        DB::table('menu_Utillity')->insert($insert_data);

        return Redirect('/Admin/AddUtillity')->with('message','Utillity Added Successfully...');

        }
        catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
  }

  //Edit editUtillity 
  public function editUtillity(Request $data,$id){

     $Utillity = DB::table('menu_Utillity')->where('ut_id',$id)->get();
    return view('lmgAdmin.editUtillity')->with('utres',$Utillity);
  }


  public function Utillityedited(Request $data){
      try {

      unset($data['_token']);

      $list_part=DB::table('menu_utillity')->Where('ut_id',$data['txtut_id'])
      ->update(['Utillity_name'=>$data['txtUtillity'],
      'Utillity_link'=>$data['txtUtillitylink'],
      'Utillity_status'=>$data['Utillitystatus']]);

        return Redirect('/Admin/AddUtillity')->with('message','Utillity Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
    }
  }

  public function deleteUtillity(Request $data,$id){
    try{
      $res = DB::table('menu_utillity')->where('ut_id', $id)->delete();
      return Redirect('/Admin/AddUtillity')->with('message','Utillity Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //Expert Enquiry

  public function enquiry_export(Request $data)
  {
    $validator = Validator::make($data->all(), [
      'fromdate' => 'required',
    ]);

    if ($validator->fails()) {
      return back()
      ->withErrors($validator,'enquiry_errors')
      ->withInput();
    }

    unset($data['_token']);

    try
    {
      $fromdate = Input::get('fromdate');
      $todate = Input::get('todate');
      $lang_code = Input::get('lang_code');
      $type = Input::get('type');

      if($todate == '')
      {
        $todate = date('Y-m-d');
      }

      switch ($type) {
        case 'contact':
          $type = 'Contact Enquiry';
          $table = 'enquiry_info';
          $url = '/Admin/contact-enquiry';
          $message = 'Contact Enquiry Not Found';
          break;
        case 'newsletter':
          $type = 'Newsletter Subscribe';
          $table = 'newsletter_subscribe_list';
          $url = '/Admin/newsletter-subscribers';
          $message = 'Newsletter Subscribers Not Found';
          break;
        default:
          $type = 'Contact Enquiry';
          $table = 'enquiry_info';
          $url = '/Admin/contact-enquiry';
          $message = 'Contact Enquiry Not Found';
          break;
      }

      $enquiry_info = DB::table($table)
      ->where('type', $type)
      ->where('lang_code', $lang_code)
      ->where('created_at', '>=', $fromdate.' '.'00:00:00')
      ->where('created_at', '<=', $todate.' '.'23:59:59')
      ->get();

      $values_in1 = [];
      $i=1;
      if(sizeof($enquiry_info))
      {
        foreach ($enquiry_info as $iteration => $value)
        {
          switch ($value->status)
          {
            case '1':
              $status = 'Pending';
              break;
            case '2':
              $status = 'Done';
              break;            
            default:
              $status = 'Reject';
              break;
          }
          $values_in1[$iteration]['S.No'] = $i++;
          $values_in1[$iteration]['Store Lang'] = $value->lang_code;

          if($value->type == 'Contact Enquiry')
          {
            $values_in1[$iteration]['Name'] = $value->name;
            $values_in1[$iteration]['Email'] = $value->email;
            $values_in1[$iteration]['Mobile'] = $value->mobile;
            $values_in1[$iteration]['Comment'] = $value->comment;
          }
          else
          {
            $values_in1[$iteration]['First Name'] = $value->first_name;
            $values_in1[$iteration]['Last Name'] = $value->last_name;
            $values_in1[$iteration]['Email'] = $value->email;
          }

          $values_in1[$iteration]['Created At'] = $value->created_at;
          $values_in1[$iteration]['Status'] = $status;
        }
        ob_end_clean();
        ob_start();
        $path = Excel::create($type, function($excel) use($values_in1)
        {
          $excel->sheet('enquiry_info', function($sheet) use($values_in1){
          $sheet->setOrientation('landscape');
          $sheet->freezeFirstRow(); 
          $sheet->fromArray($values_in1);
          });
        })->download('xls');
      }
      else
      {
        return Redirect($url)->with('message',$message);
      }
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function viewContactenquiry()
  {
    $cenquiry = DB::table('enquiry_info')
      ->where('type','Contact Enquiry')
      ->get();
    $language = DB::table('language')->Where('status',1)->get();  
    return view('lmgAdmin.enquiry.viewContactenquiry')->with('cenquiry',$cenquiry)->with('language',$language);
  }

  public function editContactenquiry($id)
  {
    $cenquiry = DB::table('enquiry_info')
      ->where('type','Contact Enquiry')
      ->where('enq_id',$id)
      ->first();
    $language = DB::table('language')
      ->where('status',1)
      ->get();
    return view('lmgAdmin.enquiry.editContactenquiry')
      ->with('cenquiry',$cenquiry)
      ->with('language',$language);
  }

  public function updateContactenquiry(Request $data)
  {
    try
    {
      $updateData['lang_code'] = $data['lang_code'];
      $updateData['name'] = $data['name'];
      $updateData['email'] = $data['email'];
      $updateData['mobile'] = $data['mobile'];
      $updateData['comment'] = $data['comment'];
      $updateData['status'] = $data['status'];
      DB::table('enquiry_info')
        ->where('enq_id',$data['eid'])
        ->update($updateData);

      return Redirect('/Admin/contact-enquiry')->with('message','Contact Enquiry Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function contactDeleteEnquiry($id)
  {
    try
    {
      DB::table('enquiry_info')
      ->where('enq_id',$id)
      ->delete();
      return Redirect('/Admin/contact-enquiry')->with('message','Contact Enquiry Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function viewMemberSubscribers()
  {
    try
    {
      $mssubscribe = DB::table('enquiry_info')
      ->where('type','subscribe') //membersubscribe
      ->get();
      return view('lmgAdmin.enquiry.viewMemberSubscribers')->with('mssubscribe',$mssubscribe);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function viewNewsletterSubscribe()
  {
    try
    {
      $nlenquiry = DB::table('newsletter_subscribe_list')
      ->where('type','Newsletter Subscribe')
      ->get();
      $language = DB::table('language')->Where('status',1)->get();

      return view('lmgAdmin.enquiry.viewNewsletterSubscribe')->with('nlenquiry',$nlenquiry)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function editNewsletterSubscribe($id)
  {
    try
    {
      $nlenquiry = DB::table('newsletter_subscribe_list')
      ->where('type','Newsletter Subscribe')
      ->where('id',$id)
      ->first();
      $language = DB::table('language')
      ->where('status',1)
      ->get();
      return view('lmgAdmin.enquiry.editNewsletterSubscribe')->with('nlenquiry',$nlenquiry)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function updateNewsletterSubscribe(Request $data)
  {
    try
    {
      $updateData['lang_code'] = $data['lang_code'];
      $updateData['first_name'] = $data['fname'];
      $updateData['last_name'] = $data['lname'];
      $updateData['email'] = $data['email'];
      $updateData['status'] = $data['status'];
      DB::table('newsletter_subscribe_list')
      ->where('id',$data['eid'])
      ->update($updateData);

      return Redirect('/Admin/newsletter-subscribers')->with('message','Newsletter Subscribers Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function newsletterDeleteEnquiry($id)
  {
    try
    {
      DB::table('newsletter_subscribe_list')
      ->where('id',$id)
      ->delete();
      return Redirect('/Admin/newsletter-subscribers')->with('message','Newsletter Subscribers Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //PAGES INFO

  public function viewpages(Request $data)
  {
    $page = DB::table('pages')->get();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.page.viewpage')->with('page',$page)->with('language',$language);
  }

  public function addpages(Request $data)
  {
    $language = DB::table('language')->where('status',1)->get();
    $getmenus = DB::table('menu_main')->where('menu_status',1)->get(); 
    return view('lmgAdmin.page.addpage')->with('menus',$getmenus)->with('language',$language);
  }

  public function add_pages(Request $data)
  {
    try
    {
      unset($data['_token']);
      $insert_data['lang_code']=$data['language'];
      $insert_data['page_name']=$data['page_name'];
      $insert_data['page_title']=$data['page_title'];
      $insert_data['slug']= strtolower(str_slug($data['page_name']));
      $insert_data['content_heading']=$data['content_heading'];
      $insert_data['content']=$data['content'];
      $insert_data['meta_title']=$data['meta_title'];
      $insert_data['meta_desc']=$data['meta_tag_desc'];
      $insert_data['meta_keyword']=$data['meta_tag_keywords'];
      $insert_data['status']=$data['status'];
      $insert_data['created_at']=date("Y-m-d H:i:s");
      
      DB::table('pages')->insert($insert_data);

      return Redirect('/Admin/pages')->with('message','New Page Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function edit_page(Request $data,$id)
  {
    try
    {
      // bharathi comment unwanted query->
      // $getmenus = DB::table('menu_main')->where('menu_status',1)->get();
      // ->with('menus',$getmenus)
      $getpage = DB::table('pages')->where('page_id',$id)->get();
      $language = DB::table('language')->where('status',1)->get();
      return view('lmgAdmin.page.editpage')->with('language',$language)->with('getpage',$getpage);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function editedpages(Request $data)
  {
    try
    {
      unset($data['_token']);
      $update_data['lang_code']=$data['language'];
      $update_data['page_name']=$data['page_name'];
      $update_data['slug']=$data['page_slug'];
      $update_data['page_title']=$data['page_title'];
      $update_data['content_heading']=$data['content_heading'];
      $update_data['content']=$data['content'];
      $update_data['meta_title']=$data['meta_title'];
      $update_data['meta_desc']=$data['meta_tag_desc'];
      $update_data['meta_keyword']=$data['meta_tag_keywords'];
      $update_data['status']=$data['status'];
      $update_data['updated_at']=date("Y-m-d H:i:s");

      DB::table('pages')
      ->Where('page_id',$data['page_id'])
      ->update($update_data);

      return Redirect('/Admin/pages')->with('message','Page Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function deletepage(Request $data,$id)
  {
    try
    {
      $res = DB::table('pages')
      ->where('page_id',$id)
      ->delete();
      return Redirect('/Admin/pages')->with('message','Page Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    } 
  }

  ///*VIEW BRANDS*/

  public function viewbrands()
  {
    $brands = DB::table('brands')
      ->join('brand_description','brand_description.brand_id','=','brands.brand_id')
      ->get();
    // dd($brands);
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.brands.brand')->with('brands',$brands)->with('language',$language);
  }

  public function addbrands()
  {
    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.brands.addbrand')->with('storeLang',$language);
  }

  public function checkBrandslug(Request $data)
  {
    try
    {
      $brandData = DB::table('brand_description')
        ->where('btn_url',$data['getslug'])
        ->where('lang_code',$data['lang'])
        ->first();
      if(!empty($brandData))
      {
        if($brandData->btn_url != '')
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }
      else
      {
        return 0;
      }
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function getBrandByLang(Request $data)
  {
    $lang = $data->lang;
    $id = $data->b;

    $brands = DB::table('brand_description')
      ->join('brands','brand_description.brand_id','=','brands.brand_id')
      ->where('brands.brand_id',$id)
      ->where('lang_code',$lang)
      ->where('brand_status',1)
      ->first();

    return response()->json(['data' => $brands]);
  }

  public function add_brands(Request $data)
  {
    try
    {
      unset($data['_token']);
      //brand collection image upload

      $lang = $data->lang;
      $id = $data->b;

      if(!empty(Input::file('feature_image')))
      {
        $image = Input::file('feature_image');
        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        
        $uploadPath  = public_path() . "/brand/brand_collection/";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('feature_image')->move($uploadPath, $fileName))
        {
          $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
          $imgLarge = Image::make($thumbPath);
          $imgLarge->resize(1260,900);
          $imgLarge->save($thumbPath);

          $brand_feature = $fileName;
        }
      }
      else
      {
        $brand_feature = "";
      }

      //Brand Banner image upload

      if(!empty(Input::file('brand_banner')))
      {
        $image = Input::file('brand_banner');
        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        
        $uploadPath  = public_path() . "/brand/brand_banner/";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('brand_banner')->move($uploadPath, $fileName))
        {
          $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
          $imgLarge = Image::make($thumbPath);
          $imgLarge->resize(1600,400);
          $imgLarge->save($thumbPath);

          $brand_banner = $fileName;
        }
      }
      else
      {
        $brand_banner = "";
      }

      //Brand Logo image upload

      if(!empty(Input::file('txtbrandlog')))
      {
        $image = Input::file('txtbrandlog');
        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        
        $uploadPath  = public_path() . "/brand/brand_logo/";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('txtbrandlog')->move($uploadPath, $fileName))
        {
          $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
          $imgLarge = Image::make($thumbPath);
          $imgLarge->resize(300,90);
          $imgLarge->save($thumbPath);

          $brand_logo = $fileName;
        }
      }
      else
      {
        $brand_logo = "";
      }

      // Insert brands table

      $insertBrand['brand_status']= $data['col_status'];
      $getInsert_id = DB::table('brands')->InsertGetID($insertBrand);

      $insert_data['brand_id']= $getInsert_id;
      $insert_data['lang_code']= $data['lang_code'];
      $insert_data['brand_name']= $data['brand_name'];
      $insert_data['breadcumb_title']= $data['breadcumb_title'];
      $insert_data['brand_description']= $data['brand_des'];
      $insert_data['brand_heading']= $data['brand_caption'];
      $insert_data['brand_sort_desc']= $data['brand_sort_desc'];
      $insert_data['brand_logo']=$brand_logo;
      $insert_data['brand_featureimg']=$brand_feature;
      $insert_data['brands_banner']=$brand_banner;
      $insert_data['imge_position']=$data['position'];
      $insert_data['sort_order']= $data['sort_order'];
      $insert_data['brand_meta_title']= $data['meta_title'];
      $insert_data['brand_meta_keyword']= $data['meta_keyword'];
      $insert_data['brand_meta_desc']= $data['meta_desc'];
      $insert_data['btn_text']= $data['btn_text'];
      $insert_data['btn_url']= $data['btn_url'];
      $insert_data['created_at']= date("Y-m-d H:i:s");

      // dd($insert_data);

      DB::table('brand_description')->insert($insert_data);

      return Redirect('/Admin/brands')->with('message','New Brand Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function deletebrand($lang,$id)
  {
    try
    {
      $resultBrand = DB::table('brand_description')
        ->join('brands','brand_description.brand_id','=','brands.brand_id')
        ->where('brand_description.brand_id',$id)
        ->where('brand_description.lang_code',$lang)
        ->first();

      // dd($resultBrand);
    
      if(sizeof($resultBrand))
      {
        $brand_description = DB::table('brand_description')
          ->where('brand_description.brand_id',$id)
          ->where('brand_description.lang_code',$lang)
          ->delete();

        $brandDescData = DB::table('brand_description')
          ->Where('brand_id',$id)
          ->get();

        if(empty($brandDescData))
        {
          $dirPath = public_path().'/brand';

          @File::delete($dirPath.'/brand_collection/'.$brandDescData->brand_logo);
          @File::delete($dirPath.'/brand_banner/'.$brandDescData->brand_featureimg);
          @File::delete($dirPath.'/brand_logo/'.$brandDescData->brands_banner);
          DB::table('brand_description')->Where('brand_id',$id)->delete();
        }
        $message = strtoupper($lang).' Brand Deleted Successfully...';
      }
      return Redirect('/Admin/brands')->with('message',$message);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  // public function deletebrand($lang,$id)
  // {
  //   $resultBrand = DB::table('brand_description')
  //     ->join('brands','brand_description.brand_id','=','brands.brand_id')
  //     ->where('brand_description.brand_id',$id)
  //     ->where('brand_description.lang_code',$lang)
  //     ->first();

  //   if(!empty($resultBrand))
  //   {
  //     $uploadPath  = public_path() . "/brand/brand_collection";
  //     @File::delete($uploadDir.'/'.$data['insertbrandfeatureimg']);
  //   }

  //   if(!empty($resultBrand)){

  //   }


  //     dd($result);

  //   $res = DB::table('brands')
  //     ->where('brand_id',$id)
  //     ->delete();
  //   return Redirect('/Admin/brands')->with('message','Brands Deleted Successfully...');
  // }

  public function editbrand($lang,$id)
  {
    $brands = DB::table('brands')
      ->join('brand_description','brand_description.brand_id','=','brands.brand_id')
      ->where('lang_code',$lang)
      ->where('brands.brand_id',$id)
      ->first();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.brands.editbrand')->with('brands',$brands)->with('language',$language);
  }

  public function edit_brands(Request $data)
  {
    try
    {
      unset($data['_token']);

      // dd($data->all());

      $checkBrandsSlug = DB::table('brand_description')
        ->where('brand_id',"!=",$data['brandid'])
        ->where('btn_url',$data['btn_url'])
        ->where('lang_code',$data['lang_code'])
        ->first();

        // dd($checkBrandsSlug);

      if(empty($checkBrandsSlug))
      {
        //brand collection image upload

        if(!empty(Input::file('feature_image')))
        {
          $image = Input::file('feature_image');
          $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
          $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
          
          $uploadPath  = public_path() . "/brand/brand_collection";

          if (!file_exists($uploadPath))
            File::makeDirectory($uploadPath);

          @File::delete($uploadPath.'/'.$data['insertbrandfeatureimg']);
          if (Input::file('feature_image')->move($uploadPath, $fileName))
          {
            $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
            $imgLarge = Image::make($thumbPath);
            $imgLarge->resize(1260,900);
            $imgLarge->save($thumbPath);

            $brand_feature = $fileName;
          }
        }
        else
        {
          $brand_feature = $data['insertbrandfeatureimg'];
        }

        //Brand Banner image upload

        if(!empty(Input::file('brand_banner')))
        {
          $image = Input::file('brand_banner');
          $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
          $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
          
          $uploadPath  = public_path() . "/brand/brand_banner";

          if (!file_exists($uploadPath))
            File::makeDirectory($uploadPath);

          @File::delete($uploadPath.'/'.$data['insertbrands_banner']);
          if (Input::file('brand_banner')->move($uploadPath, $fileName))
          {
            $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
            $imgLarge = Image::make($thumbPath);
            $imgLarge->resize(1600,400);
            $imgLarge->save($thumbPath);

            $brand_banner = $fileName;
          }
        }
        else
        {
          $brand_banner = $data['insertbrands_banner'];
        }

        //Brand Logo image upload

        if(!empty(Input::file('txtbrandlog')))
        {
          $image = Input::file('txtbrandlog');
          $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
          $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
          
          $uploadPath  = public_path() . "/brand/brand_logo";

          if (!file_exists($uploadPath))
            File::makeDirectory($uploadPath);

          @File::delete($uploadPath.'/'.$data['insertbrands_logo']);
          if (Input::file('txtbrandlog')->move($uploadPath, $fileName))
          {
            $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
            $imgLarge = Image::make($thumbPath);
            $imgLarge->resize(300,90);
            $imgLarge->save($thumbPath);

            $brand_logo = $fileName;
          }
        }
        else
        {
          $brand_logo = $data['insertbrands_logo'];
        }

        $updateBrand_data['brand_status']= $data['col_status'];
          DB::table('brands')
            ->Where('brand_id',$data['brandid'])
            ->update($updateBrand_data);

        $checkBrandDesc = DB::table('brand_description')
          ->where('brand_id',$data['brandid'])
          ->where('lang_code',$data['lang_code'])
          ->first();

        if($checkBrandDesc)
        {
          $update_data['brand_name']= $data['brand_name'];
          $update_data['breadcumb_title']= $data['breadcumb_title'];
          $update_data['btn_url']= $data['btn_url'];
          $update_data['brand_description']= $data['brand_des'];
          $update_data['brand_heading']= $data['brand_caption'];
          $update_data['brand_sort_desc']= $data['brand_sort_desc'];
          $update_data['imge_position']=$data['position'];
          $update_data['sort_order']= $data['sort_order'];
          $update_data['brand_meta_title']= $data['meta_title'];
          $update_data['brand_meta_keyword']= $data['meta_keyword'];
          $update_data['brand_meta_desc']= $data['meta_desc'];
          $update_data['btn_text']= $data['btn_text'];
          $update_data['updated_at']= date("Y-m-d H:i:s");

          DB::table('brand_description')
            ->Where('brand_id',$data['brandid'])
            ->where('lang_code',$data['lang_code'])
            ->update($update_data);

          $update_Imgdata['brand_logo']=$brand_logo;
          $update_Imgdata['brand_featureimg']=$brand_feature;
          $update_Imgdata['brands_banner']=$brand_banner;

          DB::table('brand_description')
            ->Where('brand_id',$data['brandid'])
            ->update($update_Imgdata);

          $messageInfo = 'Brand Updated Successfully...';
        }
        else
        {
          $insert_data['brand_id']= $data['brandid'];
          $insert_data['lang_code']= $data['lang_code'];
          $insert_data['brand_name']= $data['brand_name'];
          $insert_data['breadcumb_title']= $data['breadcumb_title'];
          $insert_data['brand_description']= $data['brand_des'];
          $insert_data['brand_heading']= $data['brand_caption'];
          $insert_data['brand_sort_desc']= $data['brand_sort_desc'];
          $insert_data['brand_logo']=$brand_logo;
          $insert_data['brand_featureimg']=$brand_feature;
          $insert_data['brands_banner']=$brand_banner;
          $insert_data['imge_position']=$data['position'];
          $insert_data['sort_order']= $data['sort_order'];
          $insert_data['brand_meta_title']= $data['meta_title'];
          $insert_data['brand_meta_keyword']= $data['meta_keyword'];
          $insert_data['brand_meta_desc']= $data['meta_desc'];
          $insert_data['btn_text']= $data['btn_text'];
          $insert_data['btn_url']= $data['btn_url'];
          $insert_data['created_at']= date("Y-m-d H:i:s");

          DB::table('brand_description')->insert($insert_data);
          $messageInfo = 'New Brand Added Successfully...';
        }
      }
      else
      {
        // return Redirect('/Admin/brands')->with('message',$messageInfo);
        return Redirect('/Admin/'.$data['lang_code'].'/edit/brand/'.$data['brandid'])
          ->with('message','This brand slug is already set another brand');
      }
      return Redirect('/Admin/brands')->with('message',$messageInfo);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  /*VIEW COLLECTION*/

  public function viewcollection()
  {
    $collection = DB::table('collection')
      ->join('collection_description','collection_description.collection_id','=','collection.collection_id')
      ->orderBy('sort_order')
      ->get();

    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.collection.collection')
      ->with('collection',$collection)
      ->with('language',$language);
  }

  public function getCollectionByLang(Request $data)
  {
    $lang = $data->lang;
    $collection = '';

    if(isset($data->c)){

      $id = $data->c;

      $collection = DB::table('collection')
        ->join('collection_description','collection.collection_id','=','collection_description.collection_id')
        ->where('collection.collection_id',$id)
        ->where('lang_code',$lang)
        ->first();
    }

    $brands = DB::table('brands')
      ->join('brand_description','brands.brand_id','=','brand_description.brand_id')
      // ->where('brand_id',$id)
      ->where('lang_code',$lang)
      ->get();

    return response()->json(['bdata' => $brands,'cdata' => $collection]);
  }

  public function addcollection()
  {
    $lang = 'en';
    $brand = DB::table('brands')
      ->join('brand_description','brand_description.brand_id','=','brands.brand_id')
      ->Where('lang_code',$lang)
      ->get();

      // dd($brand);

    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.collection.addcollection')->with('brand',$brand)->with('language',$language);
  }

  public function add_collection(Request $data)
  {
    // dd($data->all());
    try
    {
      unset($data['_token']);

      if(!empty(Input::file('feature_image')))
      {
        $image = Input::file('feature_image');
        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        
        $uploadPath  = public_path() . "/collection/images";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('feature_image')->move($uploadPath, $fileName))
        {
          $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
          $imgLarge = Image::make($thumbPath);
          $imgLarge->resize(1260,900);
          $imgLarge->save($thumbPath);

          $feature_image = $fileName;
        }
      }
      else
      {
        $feature_image = "";
      }

      if(!empty(Input::file('collection_banner')))
      {
        $image = Input::file('collection_banner');
        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        
        $uploadPath  = public_path() . "/collection/banner";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('collection_banner')->move($uploadPath, $fileName))
        {
          $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
          $imgLarge = Image::make($thumbPath);
          $imgLarge->resize(1600,400);
          $imgLarge->save($thumbPath);

          $collection_banner = $fileName;
        }
      }
      else
      {
        $collection_banner = "";
      }


      $insertCollect['brand_id']= $data['brand_id'];
      $insertCollect['col_status']= $data['col_status'];
      $getInsert_id = DB::table('collection')->InsertGetID($insertCollect);

      $insert_data['collection_id']= $getInsert_id;
      $insert_data['lang_code']= $data['lang_code'];
      $insert_data['collection_name']= $data['collection_name'];
      $insert_data['collection_slug']= strtolower(str_slug($data['collection_name']));
      $insert_data['col_caption']= $data['col_caption'];
      $insert_data['col_des']= $data['col_des'];
      $insert_data['sort_order']= $data['sort_order'];
      $insert_data['feature_image']=$feature_image;
      $insert_data['collection_banner']=$collection_banner;
      $insert_data['position']=$data['position'];
      $insert_data['collection_btn_text']= $data['btn_text'];
      $insert_data['collection_btn_url']= $data['btn_url'];
      $insert_data['meta_title']= $data['meta_title'];
      $insert_data['meta_keyword']= $data['meta_keyword'];
      $insert_data['meta_desc']= $data['meta_desc'];
      $insert_data['created_by']= $data->ip();
      $insert_data['created_at']= date("Y-m-d H:i:s");

      DB::table('collection_description')->insert($insert_data);

      return Redirect('/Admin/collection')->with('message','New Collection Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function edit_collection(Request $data)
  {
    try
    {
      unset($data['_token']);
      $id = $data['col_id'];
      $lang = $data['lang_code'];
      if(!empty(Input::file('feature_image')))
      {
        $image = Input::file('feature_image');
        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        
        $uploadPath  = public_path() . "/collection/images";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('feature_image')->move($uploadPath, $fileName))
        {
          $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
          $imgLarge = Image::make($thumbPath);
          $imgLarge->resize(1260,900);
          $imgLarge->save($thumbPath);

          $feature_image = $fileName;
          @File::delete($uploadPath.'/'.$data['insertfeature_image']);
        }
      }
      else
      {
        $feature_image = $data['insertfeature_image'];
      }
      if(!empty(Input::file('collection_banner')))
      {
        $image = Input::file('collection_banner');
        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        
        $uploadPath  = public_path() . "/collection/banner";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('collection_banner')->move($uploadPath, $fileName))
        {
          $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
          $imgLarge = Image::make($thumbPath);
          $imgLarge->resize(1600,400);
          $imgLarge->save($thumbPath);

          $collection_banner = $fileName;
          @File::delete($uploadPath.'/'.$data['insert_collection_banner']);
        }
      }
      else
      {
        $collection_banner = $data['insert_collection_banner'];
      }

      $updateCollect['brand_id']= $data['brand_id'];
      $updateCollect['col_status']= $data['col_status'];
      
      DB::table('collection')->Where('collection_id',$id)
        ->update($updateCollect);

      $checkBrandDesc = DB::table('collection_description')
        ->where('collection_id',$id)
        ->where('lang_code',$lang)
        ->first();

      $updateBdesc['collection_name']= $data['collection_name'];
      $updateBdesc['collection_slug']= strtolower(str_slug($data['collection_name']));
      $updateBdesc['col_caption']= $data['col_caption'];
      $updateBdesc['col_des']= $data['col_des'];
      $updateBdesc['feature_image']=$feature_image;
      $updateBdesc['sort_order']= $data['sort_order'];
      $updateBdesc['collection_banner']=$collection_banner;
      $updateBdesc['position']=$data['position'];
      $updateBdesc['collection_btn_text']= $data['btn_text'];
      $updateBdesc['collection_btn_url']= $data['btn_url'];
      $updateBdesc['meta_title']= $data['meta_title'];
      $updateBdesc['meta_keyword']= $data['meta_keyword'];
      $updateBdesc['meta_desc']= $data['meta_desc'];

      if($checkBrandDesc)
      {
        $updateBdesc['updated_by']= $data->ip();
        $updateBdesc['updated_at']= date("Y-m-d H:i:s");

        DB::table('collection_description')->Where('collection_id',$id)
        ->where('lang_code',$lang)
        ->update($updateBdesc);

        $updateBimg['feature_image']=$feature_image;
        $updateBimg['collection_banner']=$collection_banner;

        DB::table('collection_description')->Where('collection_id',$id)
        ->update($updateBimg);

        $messageInfo = 'Collection Updated Successfully...';
      }
      else
      {
        $updateBdesc['collection_id']= $id;
        $updateBdesc['lang_code']= $lang;
        $updateBdesc['created_by']= $data->ip();
        $updateBdesc['created_at']= date("Y-m-d H:i:s");

        DB::table('collection_description')->insert($updateBdesc);
        $messageInfo = 'New Collection Added Successfully...';
      }
      return Redirect('/Admin/collection')->with('message',$messageInfo);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function editcollection($lang,$id)
  {
    $collection = DB::table('collection')
      ->join('collection_description','collection.collection_id','=','collection_description.collection_id')
      ->where('collection.collection_id',$id)
      ->where('collection_description.lang_code',$lang)
      ->first();

    if(!$collection)
      return Redirect('/Admin/collection');

    $brand = DB::table('brands')
      ->join('brand_description','brand_description.brand_id','=','brands.brand_id')
      ->where('brand_status',1)
      ->where('brand_description.lang_code',$lang)
      ->get();

    $language = DB::table('language')->where('status',1)->get();

    return view('lmgAdmin.collection.editcollection')->with('collection',$collection)->with('brand',$brand)->with('language',$language);
  }

  public function deletecollection($lang,$id)
  {
    DB::table('collection_description')
      ->where('collection_id',$id)
      ->where('lang_code',$lang)
      ->delete();

    // $collectionData = DB::table('collection')
    //   ->join('collection_description','collection.collection_id','=','collection_description.collection_id')
    //   ->where('collection.collection_id',$id)
    //   ->first();
    $collectionData = DB::table('collection_description')
      ->where('collection_id',$id)
      ->where('lang_code',$lang)
      ->first();

    if(empty($collectionData))
    {
      $uploadDir  = public_path() . "/collection/images/";
      // dd($uploadDir.$collectionData->feature_image);
      DB::table('collection')->where('collection_id',$id)->delete();

      $uploadDir  = public_path() . "/collection/images/";
      @File::delete($uploadDir.$collectionData->feature_image);

      $uploadDir  = public_path() . "/collection/banner/";
      @File::delete($uploadDir.$collectionData->collection_banner);
    }

    // if(empty($collectionData))
    // {
    //   DB::table('collection')->where('collection_id',$id)->delete();

    //   $uploadDir  = public_path() . "/collection/images/";
    //   @File::delete($uploadDir.$collectionData->feature_image);

    //   $uploadDir  = public_path() . "/collection/banner/";
    //   @File::delete($uploadDir.$collectionData->collection_banner);
    // }
    return Redirect('/Admin/collection')->with('message','Collection Deleted Successfully...');
  }

  //VIEW STORE LOCATOR

  public function manageStore(Request $data)
  {
    $manage_store = DB::table('store_locator')->get();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.storelocator.manage_store')->with('manage_store',$manage_store)->with('language',$language);
  }

  public function AddstoreLocator()
  {
    $country = DB::table('country')->get();
    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.storelocator.addstorelocator')->with('country',$country)->with('language',$language);
  }

  public function EditstoreLocator(Request $data,$id)
  {
    $store_locator = DB::table('store_locator')->where('store_id',$id)->get();
    $country = DB::table('country')->get();
    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.storelocator.editstorelocator')->with('country',$country)->with('store_locator',$store_locator)->with('language',$language);
  }

  public function DeletestoreLocator(Request $data,$id)
  {
    DB::table('store_locator')->where('store_id',$id)->delete();
    return Redirect('/Admin/manage-store-locators')->with('message','Store Locator Deleted Successfully...');
  }

  public function InsertstoreLocator(Request $data)
  {
    try
    {
      $insert_data['lang_code']=$data['lang_code'];
      $insert_data['store_name']=$data['name'];
      $insert_data['store_address']=$data['address'];
      $insert_data['store_city']=$data['city_province'];
      $insert_data['store_state']=$data['state_region'];
      $insert_data['store_pincode']=$data['postalcode'];
      $insert_data['store_country']=$data['country'];
      $insert_data['store_latitude']=$data['latitude'];
      $insert_data['store_longitude']=$data['longitude'];
      $insert_data['zoom']=$data['zoom'];
      $insert_data['radius']=$data['radius'];
      $insert_data['store_email']=$data['email'];
      $insert_data['store_phone']=$data['phone'];
      $insert_data['store_fax']= $data['fax'];
      $insert_data['created_at']= date("Y-m-d H:i:s");
      $insert_data['created_by']= $data->ip();
      $insert_data['store_status']= $data['status'];

      DB::table('store_locator')->insert($insert_data);
      return Redirect('/Admin/manage-store-locators')->with('message','New Store Locator Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function UpdatestoreLocator(Request $data)
  {
    try
    {
      unset($data['_token']);

      $update_data['lang_code']=$data['lang_code'];
      $update_data['store_name']=$data['name'];
      $update_data['store_address']=$data['address'];
      $update_data['store_city']=$data['city_province'];
      $update_data['store_state']=$data['state_region'];
      $update_data['store_pincode']=$data['postalcode'];
      $update_data['store_country']=$data['country'];
      $update_data['store_latitude']=$data['latitude'];
      $update_data['store_longitude']=$data['longitude'];
      $update_data['zoom']=$data['zoom'];
      $update_data['radius']=$data['radius'];
      $update_data['store_email']=$data['email'];
      $update_data['store_phone']=$data['phone'];
      $update_data['store_fax']= $data['fax'];
      $update_data['updated_at']= date("Y-m-d H:i:s");
      $update_data['updated_by']= $data->ip();
      $update_data['store_status']= $data['status'];

      DB::table('store_locator')
      ->Where('store_id',$data['storeid'])
      ->update($update_data);
      
      return Redirect('/Admin/manage-store-locators')->with('message','Store Locator Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function commonsettingList()
  {
    $common_setting = DB::table('common_setting')->get();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.settings.setting')->with('common_setting',$common_setting)->with('language',$language);
  }

  public function Addcommonsetting()
  {
    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.settings.addcommonsetting')->with('language',$language);
  }

  public function add_commonsetting(Request $data)
  {
    try
    {
      unset($data['_token']);
      $language = $data['language'];
      if(!empty(Input::file('store_logo')))
      {
        $logo = Input::file('store_logo');
        $extension = pathinfo($logo->getClientOriginalName(), PATHINFO_EXTENSION);
        $fileName = 'lmgworld-logo'.'-'.strtolower($language).'.'.$extension;
        $uploadDir  = public_path() . "/logo/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('store_logo')->move($uploadDir, $fileName))
        {
          $logo = $fileName;
        }
      }
      else
      {
        $logo = "";
      }
      if(!empty(Input::file('fav_icon')))
      {
        $fav_icon = Input::file('fav_icon');
        $extension = pathinfo($fav_icon->getClientOriginalName(), PATHINFO_EXTENSION);
        $fileName = 'favicon'.'-'.strtolower($language).'.'.$extension;
        $uploadDir  = public_path() . "/favicon/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('fav_icon')->move($uploadDir, $fileName))
        {
          $fav_icon = $fileName;
        }
      }
      else
      {
        $fav_icon = "";
      }

      $insert_data['lang_code']=$language;
      $insert_data['store_name']=$data['store_name'];
      $insert_data['store_logo']=$logo;
      $insert_data['fav_icon']=$fav_icon;
      $insert_data['phone']=$data['phone'];
      $insert_data['email']=$data['email'];
      $insert_data['address']=$data['address'];
      $insert_data['open_time']=$data['opening_timings'];
      $insert_data['store_api']=$data['store_apikey'];
      $insert_data['show_feedback']=$data['show_feedback'];
      $insert_data['footer_copyright']=$data['footer_copyright'];
      $insert_data['footer_link']=$data['footer_link'];
      $insert_data['meta_title']=$data['meta_title'];
      $insert_data['meta_desc']=$data['meta_tag_desc'];
      $insert_data['meta_keyw']=$data['meta_tag_keywords'];
      $insert_data['chat']= $data['chat_url'];
      $insert_data['status']= $data['status'];
      $insert_data['created_at']= date("Y-m-d H:i:s");

      DB::table('common_setting')->insert($insert_data);

      // Insert Social Plugin
      $social_plugin['facebook_app_id'] = $data['facebook_api'];
      $social_plugin['facebook_user'] = $data['facebook_username'];
      $social_plugin['twitter_app_id'] = $data['twitter_api'];
      $social_plugin['twitter_user'] = $data['twitter_username'];
      $social_plugin['google_plus_page'] = $data['google_api'];
      $social_plugin['youtube_url'] = $data['youtube_api'];
      $social_plugin['linkedin_id'] = $data['linkedin_api'];
      $social_plugin['status'] = 1;
      $social_plugin['created_at'] = $data->ip();

      DB::table('social_plugin')->insert($social_plugin);


      return Redirect('/Admin/common-setting')->with('message','New Common Setting Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function edit_commonSetting(Request $data,$id)
  {
    $language = DB::table('language')->where('status',1)->get();
    $csettings = DB::table('common_setting')->where('id',$id)->first();
    $social_plugin = DB::table('social_plugin')->where('social_id',1)->first();
    return view('lmgAdmin.settings.editsetting')->with('csettings',$csettings)->with('language',$language)->with('social',$social_plugin);
  }

  public function updateCommonSetting(Request $data)
  {
    try
    {
      unset($data['_token']);

      $language = $data['language'];
      $logo = $data['old_store_logo'];
      $fav_icon = $data['old_fav_icon'];

      if(!empty(Input::file('store_logo')))
      {
        $store_logo = Input::file('store_logo');
        $extension = pathinfo($store_logo->getClientOriginalName(), PATHINFO_EXTENSION);
        $fileName = 'lmgworld-logo'.'-'.strtolower($language).'.'.$extension;
        $uploadDir  = public_path() . "/logo/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('store_logo')->move($uploadDir, $fileName))
        {
          $logo = $fileName;
        }
      }
      if(!empty(Input::file('fav_icon')))
      {
        $favicon = Input::file('fav_icon');
        $extension = pathinfo($favicon->getClientOriginalName(), PATHINFO_EXTENSION);
        $fileName = 'favicon'.'-'.strtolower($language).'.'.$extension;
        $uploadDir  = public_path() . "/favicon/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('fav_icon')->move($uploadDir, $fileName))
        {
          $fav_icon = $fileName;
        }
      }

      $update_data['lang_code']=$language;
      $update_data['store_name']=$data['store_name'];
      $update_data['store_logo']=$logo;
      $update_data['fav_icon']=$fav_icon;
      $update_data['phone']=$data['phone'];
      $update_data['email']=$data['email'];
      $update_data['address']=$data['address'];
      $update_data['open_time']=$data['opening_timings'];
      $update_data['store_api']=$data['store_apikey'];
      $update_data['show_feedback']=$data['show_feedback'];
      $update_data['footer_copyright']=$data['footer_copyright'];
      $update_data['footer_link']=$data['footer_link'];
      $update_data['meta_title']=$data['meta_title'];
      $update_data['meta_desc']=$data['meta_tag_desc'];
      $update_data['meta_keyw']=$data['meta_tag_keywords'];
      $update_data['chat']= $data['chat_url'];
      $update_data['status']= $data['status'];
      $update_data['updated_at']= date("Y-m-d H:i:s");

      DB::table('common_setting')
      ->Where('id',$data['csettingsid'])
      ->update($update_data);

       // Update Social Plugin
      $social_plugin['facebook_app_id'] = $data['facebook_api'];
      $social_plugin['facebook_user'] = $data['facebook_username'];
      $social_plugin['twitter_app_id'] = $data['twitter_api'];
      $social_plugin['twitter_user'] = $data['twitter_username'];
      $social_plugin['google_plus_page'] = $data['google_api'];
      $social_plugin['youtube_url'] = $data['youtube_api'];
      $social_plugin['linkedin_id'] = $data['linkedin_api'];
      $social_plugin['status'] = 1;
      $social_plugin['created_at'] = $data->ip();

      DB::table('social_plugin')->where('social_id',$data['social_id'])->update($social_plugin);


      return Redirect('/Admin/common-setting')->with('message','Common Setting Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function deletecommonset(Request $data,$id)
  {
    try
    {
      $csettings = DB::table('common_setting')->where('id',$id)->get();

      if(sizeof($csettings))
      {
        $status = array('status'=>true);
        $dirLogoPath = public_path().'/logo/';
        DB::table('common_setting')->where('id',$id)->delete();
        @File::delete($dirLogoPath.'/'.$csettings[0]->store_logo);
        $dirFavPath = public_path().'/favicon/';
        @File::delete($dirFavPath.'/'.$csettings[0]->fav_icon);
      }
      return Redirect('/Admin/common-setting')->with('message','Common Setting Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  /*BRAND SETTING INFO*/
  
  public function viewbrandsetting()
  {
    $brands_setting = DB::table('brands_setting')->get();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.brands.brands_setting')->with('brands_setting',$brands_setting)->with('language',$language);
  }
  public function Addbrandssetting()
  {
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.brands.addbrands_setting')->with('language',$language);
  }
  public function insertBrands_setting(Request $data)
  {
    try
    {
      // dd($data->all());
      unset($data['_token']);

      $page_title = $data['page_title'];
      $rand = substr(md5(microtime()),rand(0,26),5);

      if(!empty(Input::file('brand_banner')))
      {
        $brand_banner = Input::file('brand_banner');
        $extension = pathinfo($brand_banner->getClientOriginalName(), PATHINFO_EXTENSION);

        $fileName = strtolower(str_slug($page_title));
        $fileName = $fileName.'-'.$rand.'.'.$extension;
        $uploadDir = public_path() . "/brand/brand_banner/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);


        if (Input::file('brand_banner')->move($uploadDir, $fileName))
        {
          $can['brand_banner'] = $fileName;
        }
      }
      else
      {
        $can['brand_banner'] = "";
      }

      $insert_data['lang_code']=$data['lang_code'];
      $insert_data['page_tittle']=$page_title;
      $insert_data['brand_header']= $data['brand_header'];
      $insert_data['meta_title']=$data['meta_title'];
      $insert_data['meta_keywords']= $data['meta_keywords'];
      $insert_data['brands_desc']= $data['brands_desc'];
      $insert_data['meta_desc']= $data['meta_desc'];
      $insert_data['status']= $data['status'];
      $insert_data['brand_banner']=$can['brand_banner'];
      $insert_data['created_at']= date("Y-m-d H:i:s");

      DB::table('brands_setting')->insert($insert_data);

      return Redirect('/Admin/brandsetting')->with('message','New Brand Setting Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function editbrandssetting(Request $data,$id)
  {
    $get_brand = DB::table('brands_setting')->where('brands_setting_id',$id)->first();
    // dd($get_brand);
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.brands.editbrands_setting')->with('brand_set',$get_brand)->with('language',$language);
  }
  public function editbrands_setting(Request $data)
  {
    try
    {
      unset($data['_token']);

      $page_title = $data['page_title'];
      $rand = substr(md5(microtime()),rand(0,26),5);

      if(!empty(Input::file('brand_banner')))
      {
        $brand_banner = Input::file('brand_banner');
        $extension = pathinfo($brand_banner->getClientOriginalName(), PATHINFO_EXTENSION);

        $fileName = strtolower(str_slug($page_title));
        $fileName = $fileName.'-'.$rand.'.'.$extension;
        $uploadDir = public_path() . "/brand/brand_banner/";

        if (!file_exists($uploadDir))
          File::makeDirectory($uploadDir);

        if (Input::file('brand_banner')->move($uploadDir, $fileName))
        {
          $brand_banner = $fileName;
          @File::delete($uploadDir.$data['insertbrand_banner']);
        }
      }
      else
      {
        $brand_banner = $data['insertbrand_banner'];
      }
      $list_part=DB::table('brands_setting')->Where('brands_setting_id',$data['brand_setid'])
      ->update(['lang_code'=>$data['lang_code'],
        'page_tittle'=>$page_title,
        'meta_title'=>$data['meta_title'],
        'meta_keywords'=>$data['meta_keywords'],
        'brand_banner'=>$brand_banner,
        'meta_desc'=>$data['meta_desc'],
        'brand_header'=> $data['brand_header'],
        'brands_desc'=> $data['brands_desc'],
        'status'=>$data['status']
      ]);
      return Redirect('/Admin/brandsetting')->with('message','Brands Setting Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function deletebrandssetting(Request $data,$id)
  {
    $brands_setting = DB::table('brands_setting')
    ->where('brands_setting_id',$id)->get();
    if(sizeof($brands_setting))
    {
      $dirPath = public_path().'/brand/brand_banner'; //. $brands_setting[0]->brand_banner;
      @File::delete($dirPath.'/'.$brands_setting[0]->brand_banner);
      DB::table('brands_setting')->where('brands_setting_id',$id)->delete();
    }
    return Redirect('/Admin/brandsetting')->with('message','Brand Setting Deleted Successfully...');
  }

  /*CHATS INFO*/

  public function viewchats(Request $data)
  {
    $chats = DB::table('chat_info')->get();
    $language = DB::table('language')->Where('status',1)->get();

    return view('lmgAdmin.chat.viewchats')->with('chats',$chats)->with('language',$language);
  }
  public function Addchat()
  {
    $lang = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.chat.addchat')->with('language',$lang);
  }
  public function add_chat(Request $data)
  {
    try
    {
      $insert_data['lang_code']= $data['language'];
      $insert_data['chat_name']= $data['chatname'];
      $insert_data['status']= $data['status'];
      $insert_data['chat_script']= $data['script_code'];
      DB::table('chat_info')->insert($insert_data);
      return Redirect('/Admin/chats')->with('message','Chat Script Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function editchat(Request $data,$id)
  {
    $chat = DB::table('chat_info')->where('chat_id', $id)->get();
    $language = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.chat.editchat')->with('chat',$chat)->with('language',$language);
  }
  public function edit_chat(Request $data)
  {
    try
    {
      unset($data['_token']);
      $list_part=DB::table('chat_info')->Where('chat_id',$data['chatid'])
        ->update(['chat_name'=>$data['chatname'],
        'lang_code'=>$data['language'],
        'chat_script'=>$data['script_code'],
        'status'=>$data['status']
      ]);
      return Redirect('/Admin/chats')->with('message','Chat Script Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function deletechat(Request $data,$id)
  {
    $res = DB::table('chat_info')->where('chat_id', $id)->delete();
    return Redirect('/Admin/chats')->with('message','Chat Script Deleted Successfully...');
  }

  /*STOCKS INFO*/

  public function viewstocks(Request $data)
  {
    $stocks = DB::table('stock_status')->get();
    $language = DB::table('language')->Where('status',1)->get();
    return view('lmgAdmin.stocks.viewstocks')->with('stocks',$stocks)->with('language',$language);
  }
  public function Addstock()
  {
    $lang = DB::table('language')->where('status',1)->get();
    return view('lmgAdmin.stocks.addstock')->with('lang',$lang);
  }
  public function add_stock(Request $data)
  {
    try
    {
      $insert_data['lang_code']= $data['lang'];
      $insert_data['name']= $data['stock_name'];
      $insert_data['st_status']= $data['status'];
      DB::table('stock_status')->insert($insert_data);
      return Redirect('/Admin/stocks')->with('message','New Stock Status Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function editstock(Request $data,$id)
  {
    try
    {
      $stock = DB::table('stock_status')->where('stock_status_id', $id)->get();
      $language = DB::table('language')->where('status',1)->get();
      return view('lmgAdmin.stocks.editstock')->with('stock',$stock)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function edit_stock(Request $data)
  {
    try
    {
      unset($data['_token']);
      DB::table('stock_status')->Where('stock_status_id',$data['txtstock_id'])
        ->update(['name'=>$data['stock_name'],
        'lang_code'=>$data['lang'],
        'st_status'=>$data['status']]);
      return Redirect('/Admin/stocks')->with('message','Stock Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function deletestock(Request $data,$id)
  {
   try
    {
      $res = DB::table('stock_status')->where('stock_status_id', $id)->delete();
      return Redirect('/Admin/stocks')->with('message','Stock Deleted Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

    /*LANGUAGE INFO*/

    public function viewlanguage(Request $data)
    {
      $language = DB::table('language')
      ->orderBy('sort_order')
      ->get();
      return view('lmgAdmin.lang.viewlanguage')->with('language',$language);
    }
    public function Addlanguage()
    {
      return view('lmgAdmin.lang.addlanguage');
    }
    public function check_language(Request $data)
    {
      $getres = DB::table('language')->where('lang_code',$data['getval'])->get();
      if(!empty($getres))
      {
        return 1;
      }
      else
      {
        return 0;
      }
    }
    public function add_language(Request $data)
    {
      try
      {
        $insert_data['lang_code']= $data['lang_code'];
        $insert_data['lang_name']= $data['lang_name'];
        $insert_data['sort_order']= $data['sort_order'];
        $insert_data['status']= $data['status'];
        DB::table('language')->insert($insert_data);
        return Redirect('/Admin/language')->with('message','Language Added Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function editlanguage(Request $data,$id)
    {
      $getlan = DB::table('language')->where('lang_id',$id)->get();
      return view('lmgAdmin.lang.editlanguage')->with('lang',$getlan);
    }
    public function edit_language(Request $data)
    {
      try
      {
        $list_part=DB::table('language')->Where('lang_id',$data['lang_id'])
        ->update(['lang_code'=>$data['lang_code'],
          'lang_name'=>$data['lang_name'],
          'sort_order'=>$data['sort_order'],
          'status'=>$data['status']
          ]);
        return Redirect('/Admin/language')->with('message','Language Updated Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function deletelanguage(Request $data,$id)
    {
      $delete = DB::table('language')->where('lang_id',$id)->delete();
      return Redirect('/Admin/language')->with('message','Language Delete Successfully...');
    }

    /*CATEGORIES INFO*/

    public function viewcategories(Request $data)
    {
      $categories = DB::table('categories_description')
        ->join('categories','categories.categories_id','=','categories_description.categories_id')
        ->orderBy('categories.categories_id')
        ->get();

        // dd($categories);

      $language = DB::table('language')->Where('status',1)->get();

      return view('lmgAdmin.categories.viewcategories')->with('categories',$categories)->with('language',$language);
    }
    public function addCategories()
    {
      $lang = 'en';
      $categories_name = DB::table('categories_description')
      ->join('categories','categories.categories_id','=','categories_description.categories_id')
      ->Where('categories.status',1)
      ->where('categories_description.lang_code',$lang)
      ->get();

      $language = DB::table('language')->where('status',1)->get();

      return view('lmgAdmin.categories.addcategories')->with('language',$language)->with('categories_name',$categories_name);
    }
    public function add_categories(Request $data)
    {
      try
      {
        // dd($data->all());
        unset($data['_token']);
        if(!empty(Input::file('category_image')))
        {
          $image = Input::file('category_image');
          $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
          $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));

          $uploadPath = public_path()."/category";

          if (!file_exists($uploadPath))
            File::makeDirectory($uploadPath);

          if (Input::file('category_image')->move($uploadPath, $fileName))
          {
            $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
            $imgLarge = Image::make($thumbPath);
            $imgLarge->resize(650,270);
            $imgLarge->save($thumbPath);

            $cate_image = $fileName;
          }
        }
        else
        {
          $cate_image ="";
        }
        $cate['parent_id'] = $data['parent_id'];
        $cate['status'] = $data['col_status'];

        $getInsert_id = DB::table('categories')->InsertGetID($cate);

        $cate_desc['categories_id'] = $getInsert_id;
        $cate_desc['lang_code'] = $data['lang_code'];
        $cate_desc['category_name'] = $data['category_name'];
        $cate_desc['category_slug'] = strtolower(str_replace(' ','-',$data['category_slug']));
        $cate_desc['meta_title'] = $data['meta_title'];
        $cate_desc['meta_keywords'] = $data['meta_keyword'];
        $cate_desc['meta_description'] = $data['meta_desc'];
        $cate_desc['cate_image'] = $cate_image;
        $cate_desc['cate_desc'] = $data['cate_desc'];
        $cate_desc['created_at'] = date("Y-m-d H:i:s");
        $cate_desc['created_by'] = $data->ip();

        DB::table('categories_description')->Insert($cate_desc);

        return Redirect('/Admin/categories')->with('message','New '.strtoupper($data['lang_code']).' Category Added Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function editCategories($lang,$id)
    {
      try
      {
        // dd($lang);
        $categories = DB::table('categories_description')
          ->join('categories','categories.categories_id','=','categories_description.categories_id')
          ->Where('categories.status',1)
          ->Where('categories_description.categories_id',$id)
          ->Where('categories_description.lang_code',$lang)
          ->first();

        $allCategories = DB::table('categories_description')
          ->join('categories','categories_description.categories_id','=','categories.categories_id')
          ->Where('categories_description.lang_code',$categories->lang_code)
          ->Where('categories.status',1)
          ->get();

        $language = DB::table('language')
          ->where('status',1)
          ->get();
        
        return view('lmgAdmin.categories.editcategories')->with('language',$language)->with('categories',$categories)->with('allCategories',$allCategories);
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function checkCategoryslug(Request $data)
    {
      try
      {
        $categoryData = DB::table('categories_description')
          ->where('category_slug',$data['getslug'])
          ->where('lang_code',$data['lang'])
          ->first();
        if(!empty($categoryData))
        {
          if($categoryData->category_slug != '')
          {
            return 1;
          }
          else
          {
            return 0;
          }
        }
        else
        {
          return 0;
        }
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function edit_categories(Request $data)
    {
      try
      {
        // dd($data->all());

        $checkCategorySlug = DB::table('categories_description')
          ->where('categories_id',"!=",$data['categories_id'])
          ->where('category_slug',$data['category_slug'])
          ->where('lang_code',$data['lang_code'])->first();

        // dd($checkCategorySlug);

        // if($checkCategorySlug->category_slug == $data['category_slug'])

        if(empty($checkCategorySlug))
        {
          // dd('test1');
          if(!empty(Input::file('category_image')))
          {
            $image = Input::file('category_image');
            $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
            $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));

            $uploadPath = public_path()."/category";

            if (!file_exists($uploadPath))
              File::makeDirectory($uploadPath);

            @File::delete($uploadPath.'/'.$data['InsertCategory_image']);
            if (Input::file('category_image')->move($uploadPath, $fileName))
            {
              $thumbPath = $uploadPath . DIRECTORY_SEPARATOR . $fileName;
              $imgLarge = Image::make($thumbPath);
              $imgLarge->resize(650,270);
              $imgLarge->save($thumbPath);

              $cate_image = $fileName;
            }
          }
          else
          {
            $cate_image = $data['InsertCategory_image'];
          }

          $cate['parent_id'] = $data['parent_id'];
          $cate['status'] = $data['col_status'];
          
          DB::table('categories')
            ->where('categories_id',$data['categories_id'])
            ->update($cate);

          $checkCategoryDesc = DB::table('categories_description')->where('categories_id',$data['categories_id'])->where('lang_code',$data['lang_code'])->first();

          if($checkCategoryDesc)
          {
            $cate_desc['category_name'] = $data['category_name'];
            $cate_desc['category_slug'] = $data['category_slug'];
            
            $cate_desc['cate_desc'] = $data['cate_desc'];
            $cate_desc['updated_at'] = date("Y-m-d H:i:s");
            $cate_desc['meta_title'] = $data['meta_title'];
            $cate_desc['meta_keywords'] = $data['meta_keyword'];
            $cate_desc['meta_description'] = $data['meta_desc'];
            $cate_desc['updated_by'] = $data->ip();

            DB::table('categories_description')
              ->where('categories_id',$data['categories_id'])
              ->where('lang_code',$data['lang_code'])
              ->update($cate_desc);

            $cate_desc_img['cate_image'] = $cate_image;

            DB::table('categories_description')
              ->where('categories_id',$data['categories_id'])
              ->update($cate_desc_img);
          }
          else
          {
            $insert_Categorydata['categories_id']= $data['categories_id'];
            $insert_Categorydata['lang_code'] = $data['lang_code'];
            $insert_Categorydata['category_name'] = $data['category_name'];
            $insert_Categorydata['category_slug'] = $data['category_slug'];
            $insert_Categorydata['meta_title'] = $data['meta_title'];
            $insert_Categorydata['meta_keywords'] = $data['meta_keyword'];
            $insert_Categorydata['meta_description'] = $data['meta_desc'];
            $insert_Categorydata['cate_image'] = $cate_image;
            $insert_Categorydata['cate_desc'] = $data['cate_desc'];
            $insert_Categorydata['created_at'] = date("Y-m-d H:i:s");
            $insert_Categorydata['created_by'] = $data->ip();
            
            DB::table('categories_description')
              ->insert($insert_Categorydata);
          }
        }
        else
        {
          // dd('/Admin/'.$data['lang_code'].'/editCategories/'.$data['categories_id']);
          return Redirect('/Admin/'.$data['lang_code'].'/editCategories/'.$data['categories_id'])
          ->with('message','This category slug is already set another category');

        }
        return Redirect('/Admin/categories')->with('message','Categories Updated Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception){
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function getCategoryByLang(Request $data)
    {
      $lang = $data->lang;
      $checkCategoryDesc = '';

      if(!empty($data->c)){
        $categories_id = $data->c;

        $checkCategoryDesc = DB::table('categories_description')
        ->join('categories','categories_description.categories_id','=','categories.categories_id')
        ->where('categories.categories_id', $categories_id)
        ->where('categories_description.lang_code', $lang)
        ->first();
      }

      $allCategories = DB::table('categories_description')
        ->join('categories','categories_description.categories_id','=','categories.categories_id')
        ->Where('categories_description.lang_code',$lang)
        ->Where('categories.status',1)
        ->get();

      return response()->json(['data' => $checkCategoryDesc,'allCategories' => $allCategories]);
    }
    public function deleteCategories($lang,$id)
    {
      try
      {
        $prod_categories = DB::table('product_to_category')
          ->Where('category_id',$id)
          ->get();

        if(sizeof($prod_categories))
        {
          $message = 'This category link to '.sizeof($prod_categories).' products. Please remove after delete this category';
        }
        else
        {
          $categories = DB::table('categories_description')
            ->join('categories','categories_description.categories_id','=','categories.categories_id')
            ->Where('categories.categories_id',$id)
            ->Where('categories_description.lang_code',$lang)
            ->first();

          // dd($categories);

          if(sizeof($categories))
          {
            $categories_description = DB::table('categories_description')
              ->Where('categories_id',$id)
              ->Where('lang_code',$lang)
              ->delete();
            $categoriesdescData = DB::table('categories_description')
              ->Where('categories_id',$id)
              // ->Where('lang_code',$lang)
              ->get();
            if(empty($categoriesdescData)){
              $dirPath = public_path().'/category/';
              @File::delete($dirPath.'/'.$categories->cate_image);
              $categories = DB::table('categories')->Where('categories_id',$id)->delete();
            }
            $message = strtoupper($lang).' Categories Deleted Successfully...';
          }
        }
        return Redirect('/Admin/categories')->with('message',$message);
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    /*E-CATALOGUE INFO*/

    public function eCatalogue(Request $data)
    {
      $ecatalogue = DB::table('ecatalogue')->where('status',1)->get();
      $language = DB::table('language')->where('status',1)->get();
      return view('lmgAdmin.catalogue.vieweCatalogue')->with('language',$language)->with('ecatalogue',$ecatalogue);
    }
    public function addECatalogue(Request $data)
    {
      $language = DB::table('language')->where('status',1)->get();
      return view('lmgAdmin.catalogue.add_Catalogue')->with('language',$language);
    }
    public function add_eCatalogue(Request $data)
    {
      try
      {
        unset($data['_token']);

        $ecatalogue_slug = strtolower(str_replace(' ','-',$data['ecatalogue_slug']));

        if(!empty(Input::file('ecatalogue_image')))
        {
          $ecatalogue_image = Input::file('ecatalogue_image');
          if(sizeof($ecatalogue_image))
          {
            $uploadDir = public_path().'/product/ecatalogue/' . $ecatalogue_slug;

            if (!file_exists($uploadDir))
              File::makeDirectory($uploadDir);

            foreach ($ecatalogue_image as $key => $value)
            {
              $fileName = $value->getClientOriginalName();
              if ($ecatalogue_image[$key]->move($uploadDir, $fileName))
              {
                $ecatalogue_image['ecatalogue_image'][$key] = $fileName;
              }
            }
          }
        }        
        $catalog['ec_name'] = $data['ecatalogue_name'];
        $catalog['ec_slug'] = $ecatalogue_slug;
        $catalog['lang_code'] = $data['lang_code'];
        $catalog['ewidth'] = $data['ewidth'];
        $catalog['eheight'] = $data['eheight'];
        $catalog['sort_order'] = $data['sort_order'];
        $catalog['status'] = $data['col_status'];
        $catalog['created_by'] = $data->ip();

        $getInsert_id = DB::table('ecatalogue')
          ->insertGetId($catalog);

        if(sizeof($data['eci_sort_order']))
        {
          foreach ($data['eci_sort_order'] as $key => $value)
          {
            $insert_ecatalog_img['ecatalogue_id']=$getInsert_id;
            $insert_ecatalog_img['ec_image'] = $_FILES['ecatalogue_image']['name'][$key];
            $insert_ecatalog_img['ec_desc_sort_order']=$value;
            $insert_ecatalog_desc  = DB::table('ecatalogue_description')->insert($insert_ecatalog_img);
          }
        }
        return Redirect('/Admin/ecatalogue')->with('message','E-Catalogue Added Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception){
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function editECatalogue(Request $data,$id)
    {
      $ecatalogue = DB::table('ecatalogue')
        ->join('language','ecatalogue.lang_code','=','language.lang_code')
        ->Where('language.status',1)
        ->Where('ecatalogue.id',$id)
        ->first();

        // dd($ecatalogue);

      $ecatalogue_desc = DB::table('ecatalogue')
        ->join('ecatalogue_description','ecatalogue_description.ecatalogue_id','=','ecatalogue.id')
        ->join('language','ecatalogue.lang_code','=','language.lang_code')
        ->Where('language.status',1)
        ->orderBy('ec_desc_sort_order')
        ->Where('ecatalogue.id',$id)
        ->get();
      
      // dd($ecatalogue_desc);

      $language = DB::table('language')->where('status',1)->get();
      return view('lmgAdmin.catalogue.editecatalogue')->with('language',$language)->with('ecatalogue',$ecatalogue)->with('ecatalogue_desc',$ecatalogue_desc);
    }
    public function edit_ecatalogue(Request $data) 
    {
      try
      {
        unset($data['_token']);

        $ecatalogue_slug = strtolower(str_replace(' ','-',$data['ecatalogue_slug']));

        if(!empty(Input::file('InsertEcatalogue_image')))
        {
          $ecatalogue_image = Input::file('InsertEcatalogue_image');
          if(sizeof($ecatalogue_image))
          {
            $uploadDir = public_path().'/product/ecatalogue/' . $ecatalogue_slug;

            if(!file_exists($uploadDir))
              File::makeDirectory($uploadDir);

            foreach ($ecatalogue_image as $key => $value)
            {
              $fileName = $value->getClientOriginalName();
              if($ecatalogue_image[$key]->move($uploadDir, $fileName))
                  $ecatalogue_image1[$key] = $fileName;
            }
          }
        }

        $ecatalogue['ec_name'] = $data['ecatalogue_name'];
        $ecatalogue['ec_slug'] = $ecatalogue_slug;
        $ecatalogue['lang_code'] = $data['lang_code'];
        $ecatalogue['status'] = $data['col_status'];
        $ecatalogue['ewidth'] = $data['ewidth'];
        $ecatalogue['sort_order'] = $data['sort_order'];
        $ecatalogue['eheight'] = $data['eheight'];

        /*UPDATE ECATALOG ROW*/
        DB::table('ecatalogue')
          ->where('id',$data['ecatalogueid'])
          ->update($ecatalogue);

        /*DROP ALL ECTALOG DESCRIPTION*/
        DB::table('ecatalogue_description')
          ->where('ecatalogue_id',$data['ecatalogueid'])
          ->delete();

        if(sizeof($data['eci_sort_order']))
        {
          foreach ($data['eci_sort_order'] as $key => $value)
          {
            $ec_image = '';
            $insert_ecatalog_img['ecatalogue_id'] = $data['ecatalogueid'];
            if(isset( $data['old_ecatalogue_image'][$key] ))
              $ec_image = $data['old_ecatalogue_image'][$key];

            if(isset( $ecatalogue_image1[$key] ))
              $ec_image = $ecatalogue_image1[$key];

            $insert_ecatalog_img['ec_image'] = $ec_image;
            $insert_ecatalog_img['ec_desc_sort_order'] = $value;
            
            DB::table('ecatalogue_description')->insert($insert_ecatalog_img);
          }
        }
        return Redirect('/Admin/ecatalogue')->with('message','E-Catalogue Updated Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception) {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function delete_Ecatalogue_images()
    {
      $status = array('status'=>false);
      $id = Input::get('id');
      $dirname = Input::get('name');

      $ecatalogue_desc = DB::table('ecatalogue_description')
      ->where('ecdesc_id',$id)->get();
      if(sizeof($ecatalogue_desc))
      {
        $status = array('status'=>true);
        $dirPath = public_path().'/product/ecatalogue/' . $dirname;
        $ecatalogue = DB::table('ecatalogue_description')
        ->Where('ecdesc_id',$id)->delete();
        @File::delete($dirPath.'/'.$ecatalogue_desc[0]->ec_image);
      }
      echo json_encode($status);
    }
    public function deleteECatalogue(Request $data,$id)
    {
      try {
        $ecatalogue = DB::table('ecatalogue')->where('id',$id)->get();
        $ecatalogue_desc = DB::table('ecatalogue')
        ->join('ecatalogue_description','ecatalogue_description.ecatalogue_id','=','ecatalogue.id')
        ->join('language','ecatalogue.lang_code','=','language.lang_code')
        ->Where('language.status',1)
        ->Where('ecatalogue.id',$id)
        ->get();

        // $ecatalogue_name = preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $ecatalogue[0]->ec_name);
        $ecatalogue_slug = $ecatalogue[0]->ec_slug;
        $uploadDir = public_path().'/product/ecatalogue/' . $ecatalogue_slug .'/';

        if(sizeof($ecatalogue_desc)) {
          foreach ($ecatalogue_desc as $key => $value) {
            $ecatalogue = DB::table('ecatalogue_description')->Where('ecdesc_id',$value->ecdesc_id)->Where('ecatalogue_id',$id)->delete();
            $fileName = $value->ec_image;

            File::delete($uploadDir.$fileName);
          }
          File::deleteDirectory($uploadDir);
        }
        $ecatalogue = DB::table('ecatalogue')->Where('id',$id)->delete();        
        return Redirect('/Admin/ecatalogue')->with('message','E-Catalogue Deleted Successfully...');
      } catch(\Illuminate\Database\QueryException $exception) {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function check_ecatalogueSlug(Request $data)
    {
        try
        {
            $ecatalogueData = DB::table('ecatalogue')->where('ec_slug',strtolower($data['getecatalogue']))->first();
            if(!empty($ecatalogueData))
            {
                if($ecatalogueData->ec_slug != '')
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
            // return $categoryslug;
        }
        catch(\Illuminate\Database\QueryException $exception)
        {
          $a = $exception->errorInfo;
          return view('errors.custom')->with('message',end($a));
        }
    }
    
    //Exit Intent Popup
    public function exit_intent(Request $data)
    {
      $exit_intent = DB::table('tbl_exit_intent')->get();
      $language = DB::table('language')->Where('status',1)->get();
      return view('lmgAdmin.promotion.exit-intent.view')->with('exit_intent',$exit_intent)->with('language',$language);
    }
    public function AddExitst_intent(Request $data){
      try{
        $language = DB::table('language')->Where('status',1)->get();
        return view('lmgAdmin.promotion.exit-intent.add')->with('language',$language);
      }
      catch(\Illuminate\Database\QueryException $exception){
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
    public function add_exit_intent(Request $data){
      try{
         //dd($data->all());
      if(!empty(Input::file('intent_image'))){

            $image = Input::file('intent_image');
            $fileName   = $image->getClientOriginalName();
            $uploadDir  = public_path() . "/promotion/";    

            //dd($fileName);
            if (Input::file('intent_image')->move($uploadDir, $fileName)) {

                $can['intent_image'] = $fileName;
            }
      }
      else{

         $can['intent_image'] = "";
      }

         $promo_insert['lang_code'] = $data['lang_code'];
         $promo_insert['popup_title'] = $data['popup_title'];
         $promo_insert['intent_image'] = $can['intent_image'];
         $promo_insert['coupon_code'] = $data['coupon_code'];
         $promo_insert['popup_content'] = $data['popup_content'];
         $promo_insert['disable_form'] = $data['disable_form'];
         $promo_insert['button_text'] = $data['button_text'];
         $promo_insert['social_login'] = $data['social_login'];
         $promo_insert['status'] = $data['status'];
         $promo_insert['created_at'] = date("Y-m-d H:i:s");
         $promo_insert['created_by'] =  $data->ip();

         DB::table('tbl_exit_intent')->Insert($promo_insert);


        return Redirect('/Admin/exit_intent')->with('message','Exit Intent Added Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception){
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }


    public function deleteExist_Intent(Request $data,$id){
      try{
        $delete = DB::table('tbl_exit_intent')->where('exit_intent_id',$id)->delete();

        return Redirect('/Admin/exit_intent')->with('message','Exit Intent deleted Successfully...');

      }
      catch(\Illuminate\Database\QueryException $exception){
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function editExist_Intent(Request $data,$id){
      try{
        $get = DB::table('tbl_exit_intent')->where('exit_intent_id',$id)->get();
        $language = DB::table('language')->Where('status',1)->get();

       
      return view('lmgAdmin.promotion.exit-intent.edit')->with('intent',$get)->with('language',$language);

      }
      catch(\Illuminate\Database\QueryException $exception){
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function edit_exit_intent(Request $data){
     
      try{
         if(!empty(Input::file('intent_image'))){

            $image = Input::file('intent_image');
            $fileName   = $image->getClientOriginalName();
            $uploadDir  = public_path() . "/promotion/";    

            //dd($fileName);
            if (Input::file('intent_image')->move($uploadDir, $fileName)) {

                $can['intent_image'] = $fileName;
            }
      }
      else{

         $can['intent_image'] = $data['Upload_images'];
      }
         $promo_insert['lang_code'] = $data['lang_code'];
         $promo_insert['popup_title'] = $data['popup_title'];
         $promo_insert['intent_image'] = $can['intent_image'];
         $promo_insert['coupon_code'] = $data['coupon_code'];
         $promo_insert['popup_content'] = $data['popup_content'];
         $promo_insert['disable_form'] = $data['disable_form'];
         $promo_insert['button_text'] = $data['button_text'];
         $promo_insert['social_login'] = $data['social_login'];
         $promo_insert['status'] = $data['status'];
         $promo_insert['created_at'] = date("Y-m-d H:i:s");
         $promo_insert['created_by'] =  $data->ip();

         DB::table('tbl_exit_intent')->where('exit_intent_id',$data['exit_intent_id'])->update($promo_insert);


        return Redirect('/Admin/exit_intent')->with('message','Exit Intent Upload Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception){
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    //Header Promo
    public function headerPromotion()
    {
      $promotions = HeaderPromo::all();
      return view('lmgAdmin.headerPromotion.list')->withPromotions($promotions);
    }

    public function CreateHeaderPromotion()
    {
      // $language = App\Language::all()->where('status',1);
      $language = DB::table('language')->Where('status',1)->get();
      // dd($language);
      $langData = array('default' => 'All Language');
      foreach ($language as $key => $lang) {
        $langData[$lang->lang_code] = $lang->lang_name;
      }
      $langData = (object) $langData;
      return view('lmgAdmin.headerPromotion.create')->withLanguage($langData);
    }

    public function UpdateHeaderPromotion($id, Request $request)
    {
      $message = [
        'code.required' => 'Promo code field is required',
        'msg' => 'Promo Message field is required'
      ];

      $validator = Validator::make($request->all(), [
        'msg' => 'required',
        'code' => 'required',
        'lang' => 'required',
        'user_type' => 'required',
        'status' => 'required'
      ], $message);

      if ($validator->fails()) {
        return redirect()->route('admin.editHeaderPromo',[$id])
          ->withErrors($validator)
          ->withInput();
      }

      $headerPromo = App\HeaderPromo::find($id);

      $headerPromo->msg = $request->msg;
      $headerPromo->code = $request->code;
      $headerPromo->lang = $request->lang;
      $headerPromo->d_rule = $request->d_rule;
      $headerPromo->user_type = $request->user_type;
      $headerPromo->status = $request->status;
      $headerPromo->btn_txt = $request->btn_txt;
      $headerPromo->url = $request->url;
      $headerPromo->pages = $request->pages;
      $headerPromo->date_from = $request->date_from;
      $headerPromo->date_to = $request->date_to;
      $headerPromo->save();

      return redirect()->route('admin.headerPromo')->withSuccess('Header Promotion Updated');
    }

    public function SaveHeaderPromotion(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'msg' => 'required',
        'code' => 'required',
        'lang' => 'required',
        'user_type' => 'required',
        'status' => 'required'
      ]);

      if ($validator->fails()) {
        return redirect()->route('admin.addHeaderPromo')
          ->withErrors($validator)
          ->withInput();
      }

      $headerPromoObj = new HeaderPromo;

      $headerPromoObj->msg = $request->msg;
      $headerPromoObj->code = $request->code;
      $headerPromoObj->lang = $request->lang;
      $headerPromoObj->d_rule = $request->d_rule;
      $headerPromoObj->user_type = $request->user_type;
      $headerPromoObj->status = $request->status;
      $headerPromoObj->btn_txt = $request->btn_txt;
      $headerPromoObj->url = $request->url;
      $headerPromoObj->pages = $request->pages;
      $headerPromoObj->date_from = $request->date_from;
      $headerPromoObj->date_to = $request->date_to;

      $headerPromoObj->save();
      return redirect()->route('admin.headerPromo')->withSuccess('Header Promotion Added');
    }

    public function EditHeaderPromotion($id, Request $request)
    {
      $headerPromo = HeaderPromo::findOrFail($id);
     // dd($headerPromo);

      $language = App\Language::all()->where('status',1);
      $langData = array('default' => 'All Language');
      foreach ($language as $key => $lang) {
        $langData[$lang->lang_name] = $lang->lang_name;
      }
      $langData = (object) $langData;
      return view('lmgAdmin.headerPromotion.create')->with('promo', $headerPromo)->withLanguage($langData);
    }

    public function deleteHeaderPromotion(int $id)
    {
      $headerPromoObj = HeaderPromo::findOrFail($id);
      $headerPromoObj->delete();
      return redirect()->route('admin.headerPromo')->withSuccess('Header Promotion Removed');
    }

    //Banner Promo
    public function promo_banner(Request $data)
    {
      $language = DB::table('language')->Where('status',1)->get();
      $promo_banner = DB::table('promo_banner')->get();
      return view('lmgAdmin.promotion.promo_banner/view')->with('language',$language)->with('promo_banner',$promo_banner);
    }

    public function addBanner(Request $data){

      try{
      $lang = DB::table('language')->Where('status',1)->get();

      
      return view('lmgAdmin.promotion.promo_banner/add')->with('language',$lang);
       }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
       
    }

    public function add_banner(Request $data){
    try{
      //dd($data->all());
        $language = $data['language'];
        $banner_name = $data['txtslidername'];
      if(!empty(Input::file('txtslimage')))
      {
        $txtslimage = Input::file('txtslimage');

        $imageName = pathinfo($txtslimage->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));

        $uploadDir  = public_path() . "/promo-banner/";

        // if (!file_exists($uploadDir))
        //   File::makeDirectory($uploadDir);

        if (Input::file('txtslimage')->move($uploadDir, $fileName))
        {
          $slider_image = $fileName;
        }
      }
      else
      {
        $slider_image = "";
      }

      $insert_data['lang_code']= $language;
      $insert_data['banner_title']= $banner_name;
      $insert_data['banner_image']=$slider_image;
      $insert_data['banner_desc']= $data['desc'];
      $insert_data['banner_show_page']= $data['pageShow'];
      $insert_data['banner_btn_txt']= $data['banner_btn_txt'];
      $insert_data['banner_btn_url']= $data['banner_btn_url'];
      $insert_data['banner_status']= $data['sliderstatus'];
      $insert_data['created_at']= date("Y-m-d H:i:s");
      DB::table('promo_banner')->insert($insert_data);
      return Redirect('/Admin/promo_banner')->with('message','Promo Banner Added Successfully...');
       }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
       
    }

    public function delete_banner(Request $data,$id){
      try{
       $promo_banner =  DB::table('promo_banner')->Where('promo_banner_id',$id)->delete();

    return Redirect('/Admin/promo_banner')->with('message','Promo Banner Delete Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function edit_banner(Request $data,$id){
      try{
      
        $lang = DB::table('language')->Where('status',1)->get();
        $promo_banner = DB::table('promo_banner')->Where('promo_banner_id',$id)->first();
     
      return view('lmgAdmin.promotion.promo_banner/edit')->with('language',$lang)->with('promo_banner',$promo_banner);
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }


    public function editbanner(Request $data){
      try{

        // /dd($data->all());

      $language = $data['language'];
      $banner_name = $data['txtslidername'];

      if(!empty(Input::file('txtslimage')))
      {
        $txtslimage = Input::file('txtslimage');
        $imageName = pathinfo($txtslimage->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));

        $uploadDir  = public_path() . "/promo-banner/";

        // if (!file_exists($uploadDir))
        //   File::makeDirectory($uploadDir);

          if (Input::file('txtslimage')->move($uploadDir, $fileName))
          {
            $slider_image = $fileName;
          }
      }
      else
      {
        $slider_image =  $data['insert_images'];
      }

      $insert_data['lang_code']= $language;
      $insert_data['banner_title']= $banner_name;
      $insert_data['banner_image']=$slider_image;
      $insert_data['banner_desc']= $data['desc'];
      $insert_data['banner_show_page']= $data['pageShow'];
      $insert_data['banner_btn_txt']= $data['banner_btn_txt'];
      $insert_data['banner_btn_url']= $data['banner_btn_url'];
      $insert_data['banner_status']= $data['sliderstatus'];
      $insert_data['created_at']= date("Y-m-d H:i:s");

      DB::table('promo_banner')->where('promo_banner_id',$data['promo_id'])->update($insert_data);
      return Redirect('/Admin/promo_banner')->with('message','Promo Banner Updated Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function viewPopup(Request $data)
    {
        $language = DB::table('language')->Where('status',1)->get();
        $popup = DB::table('newsletter_popup')->get();
        return view('lmgAdmin.promotion.newsletter/view')->with('language',$language)->with('popup',$popup);
    }

    public function addPopup(Request $data){

    try{
      $lang = DB::table('language')->Where('status',1)->get();
     
      return view('lmgAdmin.promotion.newsletter/add')->with('language',$lang);
       }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
       
    }

    public function add_popup(Request $data){
    try{
      //dd($data->all());
        $language = $data['language'];
        $popup_name = $data['popup_title'];
      if(!empty(Input::file('popup_image')))
      {
        $popup_image = Input::file('popup_image');
        $extension = pathinfo($popup_image->getClientOriginalName(), PATHINFO_EXTENSION);

        $fileName = strtolower(str_slug($popup_name.'-'.$language));
        $fileName = $fileName.'.'.$extension;
        $uploadDir  = public_path() . "/promotion/";

        // if (!file_exists($uploadDir))
        //   File::makeDirectory($uploadDir);

        if (Input::file('popup_image')->move($uploadDir, $fileName))
        {
          $popup_image = $fileName;
        }
      }
      else
      {
        $popup_image = "";
      }

      $insert_data['lang_code']= $language;
      $insert_data['popup_title']= $popup_name;
      $insert_data['popup_image']=$popup_image;
      $insert_data['popup_desc']= $data['popup_desc'];
      $insert_data['popup_btn_txt']= $data['popup_btn_txt'];
      $insert_data['popup_btn_url']= $data['popup_btn_url'];
      $insert_data['popup_status']= $data['popup_status'];
      $insert_data['created_at']= date("Y-m-d H:i:s");
      DB::table('newsletter_popup')->insert($insert_data);
      return Redirect('/Admin/newsletter_popup')->with('message','Popup Added Successfully...');
       }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
       
    }

    public function delete_popup(Request $data,$id){
      try{
       $popup =  DB::table('newsletter_popup')->Where('popup_id',$id)->delete();

    return Redirect('/Admin/newsletter_popup')->with('message','Popup Delete Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function edit_popup(Request $data,$id){
      try{
      
        $lang = DB::table('language')->Where('status',1)->get();
        $popup = DB::table('newsletter_popup')->Where('popup_id',$id)->get();
     
      return view('lmgAdmin.promotion.newsletter/edit')->with('language',$lang)->with('popup',$popup);
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }


    public function editPopup(Request $data)
    {
      try
      {
        // dd($data->all());

        $language = $data['language'];
        $popup_name = $data['popup_title'];

        if(!empty(Input::file('popup_image')))
        {
          $popup_image = Input::file('popup_image');
          $extension = pathinfo($popup_image->getClientOriginalName(), PATHINFO_EXTENSION);

          $fileName = strtolower(str_slug($popup_name.'-'.$language));
          $fileName = $fileName.'.'.$extension;
          $uploadDir  = public_path() . "/promotion/";

          // if (!file_exists($uploadDir))
          //   File::makeDirectory($uploadDir);

          if (Input::file('popup_image')->move($uploadDir, $fileName))
          {
            $popup_image = $fileName;
          }
        }
        else
        {
          $popup_image =  $data['insert_images'];
        }

        $insert_data['lang_code']= $language;
        $insert_data['popup_title']= $popup_name;
        $insert_data['popup_image']=$popup_image;
        $insert_data['popup_desc']= $data['popup_desc'];
        $insert_data['popup_btn_txt']= $data['popup_btn_txt'];
        $insert_data['popup_btn_url']= $data['popup_btn_url'];
        $insert_data['popup_status']= $data['popup_status'];
        $insert_data['created_at']= date("Y-m-d H:i:s");

        DB::table('newsletter_popup')->where('popup_id',$data['popup_id'])->update($insert_data);
        return Redirect('/Admin/newsletter_popup')->with('message','Popup Updated Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
}
?>