<?php 
/*
|---------------------------------------------------------------------------
| ModuleOne Module Routes
|---------------------------------------------------------------------------
|
| All the routes related to the ModuleOne module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/
Route::group(['module' => 'LmgWorldAdmin', 'namespace' => 'App\Modules\LmgWorldAdmin\Controllers'], function () {

 Route::get('/Admin/header-promotion', ['uses' => 'LmgWorldAdminController@headerPromotion'])->name('admin.headerPromo');

 Route::get('/Admin/create-header-promotion', ['uses' => 'LmgWorldAdminController@CreateHeaderPromotion'])->name('admin.addHeaderPromo');

 Route::post('/Admin/save-header-promotion', ['uses' => 'LmgWorldAdminController@SaveHeaderPromotion'])->name('admin.saveHeaderPromo');

 Route::get('/Admin/delete-header-promotion/{id}', ['uses' => 'LmgWorldAdminController@deleteHeaderPromotion'])->name('admin.deleteHeaderPromo');

 Route::get('/Admin/edit-header-promotion/{id}', ['uses' => 'LmgWorldAdminController@EditHeaderPromotion'])->name('admin.editHeaderPromo');
 
 // patch
 Route::any('/Admin/update-header-promotion/{id}', ['uses' => 'LmgWorldAdminController@UpdateHeaderPromotion'])->name('admin.updateHeaderPromo');

//Add Promobanner
 Route::any('/Admin/promo_banner', ['uses' => 'LmgWorldAdminController@promo_banner']);
 Route::any('/Admin/AddBanner', ['uses' => 'LmgWorldAdminController@addBanner']);
 Route::any('/Admin/add_banner', ['uses' => 'LmgWorldAdminController@add_banner']);
  Route::any('/Admin/delete_banner/{id}', ['uses' => 'LmgWorldAdminController@delete_banner']);
 Route::any('/Admin/edit_banner/{id}', ['uses' => 'LmgWorldAdminController@edit_banner']);
 Route::any('/Admin/edit_banner', ['uses' => 'LmgWorldAdminController@editbanner']);

 //Add Popup
 Route::any('/Admin/newsletter_popup', ['uses' => 'LmgWorldAdminController@viewPopup']);
 Route::any('/Admin/AddPopup', ['uses' => 'LmgWorldAdminController@addPopup']);
 Route::any('/Admin/add_popup', ['uses' => 'LmgWorldAdminController@add_popup']);
  Route::any('/Admin/delete_popup/{id}', ['uses' => 'LmgWorldAdminController@delete_popup']);
 Route::any('/Admin/edit_popup/{id}', ['uses' => 'LmgWorldAdminController@edit_popup']);
 Route::any('/Admin/edit_popup', ['uses' => 'LmgWorldAdminController@editPopup']);



 Route::any('/Admin/dashboard', ['uses' => 'LmgWorldAdminController@dashboard']);




 Route::any('/Admin/catalog', ['uses' => 'LmgWorldAdminController@catalog']);

 Route::any('/Admin/menu', ['uses' => 'LmgWorldAdminController@menu']);
 Route::any('/Admin/menutype', ['uses' => 'LmgWorldAdminController@menutype']);

 Route::any('/Admin/refer', ['uses' => 'LmgWorldAdminController@refer']);

 //selva Created
 Route::any('/Admin/e-warranty', ['uses' => 'LmgWorldAdminController@viewEwarranty']);
 Route::any('/Admin/delete/ewarranty/{id}', ['uses' => 'LmgWorldAdminController@delete_warranty']);
 Route::any('/Admin/edit/ewarranty/{id}', ['uses' => 'LmgWorldAdminController@editWarranty']);
 Route::any('/Admin/update/ewarranty', ['uses' => 'LmgWorldAdminController@edit_warranty']);

 Route::any('/Admin/AddMainMenu', ['uses' => 'LmgWorldAdminController@AddMainMenu']);
 Route::any('/Admin/addmenus', ['uses' => 'LmgWorldAdminController@addmenus']);

//submenu
 Route::any('/Admin/AddSubMenu', ['uses' => 'LmgWorldAdminController@addSubMenu']);
 Route::any('/Admin/add_SubMenu', ['uses' => 'LmgWorldAdminController@add_SubMenu']);
 Route::any('/Admin/addsubmenus', ['uses' => 'LmgWorldAdminController@addsubmenus']);
 Route::any('/Admin/editsubMenu/{id}', ['uses' => 'LmgWorldAdminController@editsubMenu']);
 Route::any('/Admin/editsubmenus', ['uses' => 'LmgWorldAdminController@edit_submenus']);
 Route::any('/Admin/deletesubMenu/{id}', ['uses' => 'LmgWorldAdminController@delete_subMenu']);


 Route::any('/Admin/editMenu/{id}', ['uses' => 'LmgWorldAdminController@editMenu']);
 Route::any('/Admin/editMenutype/{type}', ['uses' => 'LmgWorldAdminController@editMenutype']);
 Route::any('/Admin/editmenus', ['uses' => 'LmgWorldAdminController@editmenus']);
 Route::any('/Admin/deleteMenu/{id}', ['uses' => 'LmgWorldAdminController@deleteMenu']);

 //Azhar 19-04

 //sliders
 Route::any('/Admin/slider', ['uses' => 'LmgWorldAdminController@slider']);
 Route::any('/Admin/add/slider', ['uses' => 'LmgWorldAdminController@AddSlider']);
 Route::any('/Admin/addslides', ['uses' => 'LmgWorldAdminController@addslides']);
 Route::any('/Admin/editslider/{id}', ['uses' => 'LmgWorldAdminController@edit_slider']);
 Route::any('/Admin/editslidespage', ['uses' => 'LmgWorldAdminController@editslides']);
 Route::any('/Admin/deleteslider/{id}', ['uses' => 'LmgWorldAdminController@deleteslider']);

  //video
  Route::any('/Admin/videos', ['uses' => 'LmgWorldAdminController@viewvideo']);
  Route::any('/Admin/add/video', ['uses' => 'LmgWorldAdminController@AddVideo']);
  Route::any('/Admin/insert/video', ['uses' => 'LmgWorldAdminController@add_video']);
  Route::any('/Admin/delete/video/{id}', ['uses' => 'LmgWorldAdminController@delete_video']);
  Route::any('/Admin/edit/video/{id}', ['uses' => 'LmgWorldAdminController@edit_video']);
  Route::any('/Admin/update/video', ['uses' => 'LmgWorldAdminController@editvideos']);

  //AddUtillity
  Route::any('/Admin/AddUtillity', ['uses' => 'LmgWorldAdminController@viewUtillity']);
  Route::any('/Admin/addUtillity', ['uses' => 'LmgWorldAdminController@add_Utillity']);
  Route::any('/Admin/add_Utillitys', ['uses' => 'LmgWorldAdminController@add_Utillitys']);
  Route::any('/Admin/deleteUtillity/{id}', ['uses' => 'LmgWorldAdminController@deleteUtillity']);
  Route::any('/Admin/editUtillity/{id}', ['uses' => 'LmgWorldAdminController@editUtillity']);
  Route::any('/Admin/edit_Utillitys', ['uses' => 'LmgWorldAdminController@Utillityedited']);

  //Page

  Route::any('/Admin/pages', ['uses' => 'LmgWorldAdminController@viewpages']);
  Route::any('/Admin/add/page', ['uses' => 'LmgWorldAdminController@addpages']);
  Route::any('/Admin/insert/page', ['uses' => 'LmgWorldAdminController@add_pages']);
  Route::any('/Admin/edit/page/{id}', ['uses' => 'LmgWorldAdminController@edit_page']);
  Route::any('/Admin/update/page', ['uses' => 'LmgWorldAdminController@editedpages']);
  Route::any('/Admin/delete/page/{id}', ['uses' => 'LmgWorldAdminController@deletepage']);

  //Brands
  Route::any('/Admin/brands', ['uses' =>'LmgWorldAdminController@viewbrands']);
  Route::any('/Admin/add/brand', ['uses' =>'LmgWorldAdminController@addbrands']);
  Route::any('/Admin/insert/brand', ['uses' =>'LmgWorldAdminController@add_brands']);
  Route::any('/Admin/{any}/delete/brand/{id}', ['uses' =>'LmgWorldAdminController@deletebrand']);
  Route::any('/Admin/{any}/edit/brand/{id}', ['uses' =>'LmgWorldAdminController@editbrand']);
  Route::any('/Admin/update/brand', ['uses' =>'LmgWorldAdminController@edit_brands']);

  Route::any('/Admin/checkBrandslug', ['uses' =>'LmgWorldAdminController@checkBrandslug']);
  Route::any('/Admin/getBrandByLang', ['uses' =>'LmgWorldAdminController@getBrandByLang']);

  //Collection
  Route::any('/Admin/collection', ['uses' =>'LmgWorldAdminController@viewcollection']);
  Route::any('/Admin/add/collection', ['uses' =>'LmgWorldAdminController@addcollection']);
  Route::any('/Admin/insert/collection', ['uses' =>'LmgWorldAdminController@add_collection']);
  Route::any('/Admin/{any}/deletecollection/{id}', ['uses' =>'LmgWorldAdminController@deletecollection']);
  Route::any('/Admin/{any}/editcollection/{id}', ['uses' =>'LmgWorldAdminController@editcollection']);
  Route::any('/Admin/update/collection', ['uses' =>'LmgWorldAdminController@edit_collection']);
  Route::any('/Admin/getCollectionByLang', ['uses' =>'LmgWorldAdminController@getCollectionByLang']);
   
  //Common Setting
  Route::any('/Admin/common-setting', ['uses' =>'LmgWorldAdminController@commonsettingList']);
  Route::any('/Admin/add/common-setting', ['uses' =>'LmgWorldAdminController@Addcommonsetting']);
  Route::any('/Admin/insert/common-setting', ['uses' =>'LmgWorldAdminController@add_commonsetting']);
  Route::any('/Admin/edit/common-setting/{id}', ['uses' =>'LmgWorldAdminController@edit_commonSetting']);
  Route::any('/Admin/update/common-setting', ['uses' =>'LmgWorldAdminController@updateCommonSetting']);
  Route::any('/Admin/delete/common-setting/{id}', ['uses' =>'LmgWorldAdminController@deletecommonset']);

  //Brand Setting
  Route::any('/Admin/brandsetting', ['uses' =>'LmgWorldAdminController@viewbrandsetting']);
  Route::any('/Admin/add/brands-setting', ['uses' =>'LmgWorldAdminController@Addbrandssetting']);
  Route::any('/Admin/insert/brands-setting', ['uses' =>'LmgWorldAdminController@insertBrands_setting']);
  Route::any('/Admin/delete/brands-setting/{id}', ['uses' =>'LmgWorldAdminController@deletebrandssetting']);
  Route::any('/Admin/edit/brands-setting/{id}', ['uses' =>'LmgWorldAdminController@editbrandssetting']);
  Route::any('/Admin/update/brands-setting', ['uses' =>'LmgWorldAdminController@editbrands_setting']);

//language - modified by bharathi
  Route::any('/Admin/language', ['uses' =>'LmgWorldAdminController@viewlanguage'])->name('languageList');
  Route::any('/Admin/add/language', ['uses' =>'LmgWorldAdminController@Addlanguage']);
  Route::any('/Admin/check_language', ['uses' =>'LmgWorldAdminController@check_language']);
  Route::any('/Admin/add_language', ['uses' =>'LmgWorldAdminController@add_language']);
  Route::any('/Admin/delete/language/{id}', ['uses' =>'LmgWorldAdminController@deletelanguage']);
  Route::any('/Admin/edit/language/{id}', ['uses' =>'LmgWorldAdminController@editlanguage']);
  Route::any('/Admin/update/language', ['uses' =>'LmgWorldAdminController@edit_language']);


  //Raja Created Admin page 02-05-2018


  Route::any('/Admin/stocks', ['uses' =>'LmgWorldAdminController@viewstocks']);
  Route::any('/Admin/addstock', ['uses' =>'LmgWorldAdminController@Addstock']);
  Route::any('/Admin/add_stock', ['uses' =>'LmgWorldAdminController@add_stock']);

  Route::any('/Admin/deletestock/{id}', ['uses' =>'LmgWorldAdminController@deletestock']);
  Route::any('/Admin/editstock/{id}', ['uses' =>'LmgWorldAdminController@editstock']);
  Route::any('/Admin/edit_stock', ['uses' =>'LmgWorldAdminController@edit_stock']);

  Route::any('/Admin/ecatalogue', ['uses' => 'LmgWorldAdminController@eCatalogue']);

  Route::any('/Admin/deleteecatalogueimg', ['uses' => 'LmgWorldAdminController@delete_Ecatalogue_images']);

  Route::any('/Admin/addecatalogue', ['uses' => 'LmgWorldAdminController@addECatalogue']);
  Route::any('/Admin/add_ecatalogue', ['uses' => 'LmgWorldAdminController@add_ecatalogue']);
  Route::any('/Admin/editecatalogue/{id}', ['uses' => 'LmgWorldAdminController@editECatalogue']);
  Route::any('/Admin/edit_ecatalogue', ['uses' => 'LmgWorldAdminController@edit_ecatalogue']);
  Route::any('/Admin/deleteecatalogue/{id}', ['uses' =>'LmgWorldAdminController@deleteECatalogue']);
  
  Route::any('/Admin/check/ecatalogueslug', ['uses' => 'LmgWorldAdminController@check_ecatalogueSlug']);

  //chat
  Route::any('/Admin/chats', ['uses' =>'LmgWorldAdminController@viewchats']);
  Route::any('/Admin/addchat', ['uses' =>'LmgWorldAdminController@Addchat']);
  Route::any('/Admin/add_chat', ['uses' =>'LmgWorldAdminController@add_chat']);
  Route::any('/Admin/editchat/{id}', ['uses' =>'LmgWorldAdminController@editchat']);
  Route::any('/Admin/edit_chat', ['uses' =>'LmgWorldAdminController@edit_chat']);
  Route::any('/Admin/deletechat/{id}', ['uses' =>'LmgWorldAdminController@deletechat']);

  //Azhar categories 08-05-2018
  Route::any('/Admin/categories', ['uses' => 'LmgWorldAdminController@viewcategories']);
  Route::any('/Admin/AddCategories', ['uses' => 'LmgWorldAdminController@addCategories']);
  Route::any('/Admin/add_categories', ['uses' => 'LmgWorldAdminController@add_categories']);
  Route::any('/Admin/{any}/deleteCategories/{id}', ['uses' => 'LmgWorldAdminController@deleteCategories']);
  Route::any('/Admin/{any}/editCategories/{id}', ['uses' => 'LmgWorldAdminController@editCategories']);
  Route::any('/Admin/edit_categories', ['uses' => 'LmgWorldAdminController@edit_categories']);
  Route::get('/Admin/getCategoryByLang', 'LmgWorldAdminController@getCategoryByLang');
  Route::get('/Admin/checkCategoryslug', 'LmgWorldAdminController@checkCategoryslug');

  

  // Route::any('/Admin/test', ['uses' =>'LmgWorldAdminController@test']);

  //Azhar Exit Intent 13-05-2018
  Route::any('/Admin/exit_intent', ['uses' => 'LmgWorldAdminController@exit_intent']);
  Route::any('/Admin/AddExitst_intent', ['uses' => 'LmgWorldAdminController@AddExitst_intent']);
  Route::any('/Admin/add_exit_intent', ['uses' => 'LmgWorldAdminController@add_exit_intent']);
  Route::any('/Admin/deleteExist_Intent/{id}', ['uses' => 'LmgWorldAdminController@deleteExist_Intent']);
  Route::any('/Admin/editExist_Intent/{id}', ['uses' => 'LmgWorldAdminController@editExist_Intent']);
  Route::any('/Admin/edit_exit_intent', ['uses' => 'LmgWorldAdminController@edit_exit_intent']);

  //Enquiry -- bharathi - 17-5-18
  Route::any('/Admin/contact-enquiry', ['uses' => 'LmgWorldAdminController@viewContactenquiry']);
  Route::any('/Admin/edit/contact-enquiry/{id}', ['uses' => 'LmgWorldAdminController@editContactenquiry']);
  Route::any('/Admin/update/contact-enquiry', ['uses' => 'LmgWorldAdminController@updateContactenquiry']);
  Route::any('/Admin/delete/contact-enquiry/{id}', ['uses' =>'LmgWorldAdminController@contactDeleteEnquiry']);


  Route::any('/Admin/newsletter-subscribers', ['uses' => 'LmgWorldAdminController@viewNewsletterSubscribe']);
  Route::any('/Admin/edit/newsletter-subscribers/{id}', ['uses' => 'LmgWorldAdminController@editNewsletterSubscribe']);
  Route::any('/Admin/update/newsletter-subscribers', ['uses' => 'LmgWorldAdminController@updateNewsletterSubscribe']);
  Route::any('/Admin/delete/newsletter-subscribers/{id}', ['uses' =>'LmgWorldAdminController@newsletterDeleteEnquiry']);


  Route::any('/Admin/member-subscribers', ['uses' => 'LmgWorldAdminController@viewMemberSubscribers']);

  //Manage Store Locator - developed bharahti 22-05-2018
  Route::any('/Admin/manage-store-locators', ['uses' =>'LmgWorldAdminController@manageStore']);
  Route::any('/Admin/add/store-locator', ['uses' =>'LmgWorldAdminController@AddstoreLocator']);
  Route::any('/Admin/insert/store-locator', ['uses' =>'LmgWorldAdminController@InsertstoreLocator']);
  Route::any('/Admin/edit/store-locator/{id}', ['uses' =>'LmgWorldAdminController@EditstoreLocator']);
  Route::any('/Admin/update/store-locator', ['uses' =>'LmgWorldAdminController@UpdatestoreLocator']);
  Route::any('/Admin/delete/store-locator/{id}', ['uses' =>'LmgWorldAdminController@DeletestoreLocator']);


//Export Enquiry Azhar
  Route::any('/Admin/enquiry_export', ['uses' => 'LmgWorldAdminController@enquiry_export']);
  // Route::any('/Admin/subscribeList', ['uses' => 'LmgWorldAdminController@newsletterList_export']);

/*Route::post('/upload_image', function() {
    $CKEditor = Input::get('CKEditor');
    $funcNum = Input::get('CKEditorFuncNum');
    $message = $url = '';
    if (Input::hasFile('upload')) {
        $file = Input::file('upload');
        if ($file->isValid()) {
            $filename = $file->getClientOriginalName();
            $file->move(storage_path().'/images/', $filename);
            $url = public_path() .'/images/' . $filename;
        } else {
            $message = 'An error occured while uploading the file.';
        }
    } else {
        $message = 'No file uploaded.';
    }
    return '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
});
*/
});

// File Upload Manager //
Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/laravel-filemanager/demo', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DemoController@index',
        'as' => 'index',
    ]);

    Route::get('/laravel-filemanager', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\LfmController@show',
        'as' => 'show',
    ]);

    // Show integration error messages
    Route::get('/laravel-filemanager/errors', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\LfmController@getErrors',
        'as' => 'getErrors',
    ]);

    // upload
    Route::any('/laravel-filemanager/upload', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\UploadController@upload',
        'as' => 'upload',
    ]);

    // list images & files
    Route::get('/laravel-filemanager/jsonitems', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ItemsController@getItems',
        'as' => 'getItems',
    ]);

    // folders
    Route::get('/laravel-filemanager/newfolder', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getAddfolder',
        'as' => 'getAddfolder',
    ]);
    Route::get('/laravel-filemanager/deletefolder', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getDeletefolder',
        'as' => 'getDeletefolder',
    ]);
    Route::get('/laravel-filemanager/folders', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getFolders',
        'as' => 'getFolders',
    ]);

    // crop
    Route::get('/laravel-filemanager/crop', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getCrop',
        'as' => 'getCrop',
    ]);
    Route::get('/laravel-filemanager/cropimage', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getCropimage',
        'as' => 'getCropimage',
    ]);
    Route::get('/laravel-filemanager/cropnewimage', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getNewCropimage',
        'as' => 'getCropimage',
    ]);

    // rename
    Route::get('/laravel-filemanager/rename', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\RenameController@getRename',
        'as' => 'getRename',
    ]);

    // scale/resize
    Route::get('/laravel-filemanager/resize', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ResizeController@getResize',
        'as' => 'getResize',
    ]);
    Route::get('/laravel-filemanager/doresize', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ResizeController@performResize',
        'as' => 'performResize',
    ]);

    // download
    Route::get('/laravel-filemanager/download', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DownloadController@getDownload',
        'as' => 'getDownload',
    ]);

    // delete
    Route::get('/laravel-filemanager/delete', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DeleteController@getDelete',
        'as' => 'getDelete',
    ]);
});

//End File Upload Manager

?>