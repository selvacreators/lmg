<?php 
namespace App\Modules\Product\Controllers;

use Image;
use App;
use File;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Product;
//use App\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Session\Store;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Response;
use Validator;
// use Illuminate\Support\Facades\Validator;
use Session;
use Cache;
use Excel;
use PDF;
use DB;
//use Request;

class ProductAdminController extends Controller
{
  public function __construct(Store $session)
  {
    $this->session = $session;
    $this->middleware('auth', ['except' => []]);
  }
  public function checkslug(Request $data)
  {
    try
    {
      $productslug = strtolower(str_replace(' ','-',$data['getproduct']));
      $product_slug = DB::table('product_description')->where('product_slug',$productslug)->get();
      if(!empty($product_slug))
      {
        if($product_slug[0]->product_slug != '')
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }
      else
      {
        return 0;
      }
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function checksku(Request $data)
  {
    try
    {
      $getsku = $data['getsku'];
      $check_sku = DB::table('product')->where('sku',str_slug($getsku))->get();
      if(!empty($check_sku))
      {
        if($check_sku[0]->sku != '')
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }
      else
      {
        return 0;
      }
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function view(Request $data)
  {
    $product = DB::table('product')
      ->select('product.*','product_description.*','product.status') //'language.*',
      ->join('product_description','product_description.product_id','=','product.product_id')
      // ->join('language','language.lang_code','=','product_description.lang_code')
      ->orderBy('product.product_id')
      ->get();
    $language = DB::table('language')->Where('status',1)->get();

    return view('Adminproduct.view')->with('product',$product)->with('language',$language);
  }
  // public function get_related_product(Request $Request)
  // {
  //   $lang = $Request->lang;

  //   $checkProductDesc = DB::table('product')
  //     ->join('product_description','product_description.product_id','=','product.product_id')
  //     ->where('lang_code', $lang)
  //     ->where('product.status',1)
  //     ->orderBy('product.product_id')
  //     ->get();
      
  //   return response()->json(['data' => $checkProductDesc]);
  // }

  public function getProductByLang(Request $data)
  {
    $lang = $data->lang;
    $product_id = $data->p;

    $product = DB::table('product_description')
      ->where('product_id', $product_id)
      ->where('lang_code', $lang)
      ->first();

    $allproduct = Product::getLanguageProductInfo($lang);

    // $checkRelatedProductDesc = DB::table('product')
    //   ->join('product_description','product_description.product_id','=','product.product_id')
    //   ->where('lang_code', $lang)
    //   ->where('product.status',1)
    //   ->orderBy('product.product_id')
    //   ->get();
      
    return response()->json(['data' => $product,'allData' => $allproduct]);
  }

  public function getProductInfo(Request $data)
  {
    $lang = $data->lang;
    $product = Product::getLanguageProductInfo($lang);
    return $product;
  }
  public function addProduct()
  {
    try
    {
      $lang = 'en';
      $product = Product::getLanguageProductInfo($lang);
      // dd($product);

      $storeLang = DB::table('language')
        ->where('status',1)
        ->get();

      $productInfo = json_decode($product);
      $passData = compact('storeLang','productInfo');
      return view('Adminproduct.addProduct', $passData);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  /*Add products*/
  public function add_Product(Request $data)
  {
    try
    {
      // dd($data->all());
      /*Form Validation*/
      $validator = Validator::make($data->all(), [
        'language' => 'required',
        'product_name' => 'required',
        'product_slug' => 'required',
        'meta_title' => 'required',
        'sku' => 'required',
        // 'stock_status' => 'required',
        'product_type' => 'required',
        'status' => 'required',
        'brand' => 'required',
      ]);

      if ($validator->fails()) {
        return back()
        ->withErrors($validator,'product_errors')
        ->withInput();
      }

      unset($data['_token']);
      $getservice = '';

      if(!empty(Input::file('Product_image')))
      {
        $image = Input::file('Product_image');

        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        $path  = public_path() . "/product";
        $uploadPath  = public_path() . "/product/large";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        if (Input::file('Product_image')->move($uploadPath, $fileName))
        {
          $largePath = $path . DIRECTORY_SEPARATOR .'/large/'. $fileName;
          $imgLarge = Image::make($largePath);
          $imgLarge->resize(1260,900);
          $imgLarge->save($largePath);

          $product_image = $fileName;
          $diretorio = 'small';
        }
        if($diretorio == 'small')
        {
          $smallFolder = $path . DIRECTORY_SEPARATOR .'/small/'. $fileName;
          $img = Image::make($largePath);
          $img->resize(560,400);
          $img->save($smallFolder);
          $diretorio = 'thumb';
        }
        if($diretorio == 'thumb')
        {
          $thumbFolder = $path . DIRECTORY_SEPARATOR .'/thumb/'. $fileName;
          $img = Image::make($largePath);
          $img->resize(84,83);
          $img->save($thumbFolder);
        }
      }
      else
      {
        $product_image ="";
      }

      //Insert product

      $insert_data['sku']= $data['sku'];
      $insert_data['ptype_id']=$data['product_type'];
      $insert_data['quantity']=$data['quantity'];
      $insert_data['stock_status_id']=1;//$data['stock_status'];
      $insert_data['prod_slider_image']=$product_image;
      $insert_data['comfort']=$data['comfort'];
      $insert_data['brand_id']=$data['brand'];
      $insert_data['price']=$data['price'];
      $insert_data['date_available']= date('Y-m-d',strtotime($data['date_available']));
      $insert_data['weight_class_id']= $data['weight_class_id'];
      $insert_data['weight']=$data['weight'];
      $insert_data['length_class_id']= $data['length_class_id'];
      $insert_data['length']=$data['length'];
      $insert_data['width']=$data['width'];
      $insert_data['height']=$data['height'];
      $insert_data['subtract']=$data['subtract'];
      $insert_data['sort_order']=$data['sort_order'];
      $insert_data['created_at']= date("Y-m-d H:i:s");
      $insert_data['created_by']= $data->ip();
      $insert_data['status']= $data['status'];

      // dd($insert_data);

      $get_prod_id = DB::table('product')->insertGetId($insert_data);


      //Shipping service image is upload


      /*Bharathi working inprogress Start*/

      $service_text = $data['service_text'];
      $service_image = Input::file('service_image');      

      $service_textCount = count($service_text);
      $service_imageCount = count($service_image);

      // $counts = $service_textCount;
      $select = 'serviceText';

      if($service_textCount > $service_imageCount)
      {
        // $counts = $service_textCount;
        $select = 'serviceText';
      }
      elseif($service_textCount < $service_imageCount)
      {
        // $counts = $service_imageCount;
        $select = 'serviceImage';
      }
      // dd($select);

      // dd($data->all());

      /*Bharathi working inprogress end*/

      if($select == 'serviceImage')
      {
        if(!empty(Input::file('service_image')))
        {
          if(sizeof($service_image))
          {
            foreach ($service_image as $key => $value)
            {
              if($value)
              {
                $imageName = pathinfo($value->getClientOriginalName(),PATHINFO_BASENAME);
                $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
                $uploadPath  = public_path() . "/product/service_img";

                if (!file_exists($uploadPath))
                  File::makeDirectory($uploadPath);

                if ($service_image[$key]->move($uploadPath, $fileName))
                {
                  // $imagePath = $uploadPath . DIRECTORY_SEPARATOR .'/'. $fileName;
                  // dd($imagePath);
                  // $imgLarge = Image::make($imagePath);
                  // $imgLarge->resize(1260,900);
                  // $imgLarge->save($imagePath);
                }
                $getservice[$key] = array("text" => $service_text[$key], "image" =>$fileName);
              }
            }
            // dd($getservice);
          }
          else
          {
            $getservice[] = "";
          }
        }
      }
      if($select == 'serviceText')
      {
        $fileName = '';
        $getservice = "";
        foreach ($service_text as $key => $value)
        {
          if(!empty(Input::file('service_image')))
          {
            $service_image = Input::file('service_image');
            if($service_image[$key] != null){
              $imageName = pathinfo($service_image[$key]->getClientOriginalName(),PATHINFO_BASENAME);
              $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
              $uploadPath  = public_path() . "/product/service_img";

              if (!file_exists($uploadPath))
                File::makeDirectory($uploadPath);

              if (@$service_image[$key]->move($uploadPath, $fileName))
              {
                // $imagePath = $uploadPath . DIRECTORY_SEPARATOR .'/'. $fileName;
                // dd($imagePath);
                // $imgLarge = Image::make($imagePath);
                // $imgLarge->resize(1260,900);
                // $imgLarge->save($imagePath);
              }
            }
          }
          // print_r($_FILES['service_image']['name'][$key]);
          if(!empty($_FILES['service_image']['name'][$key]))
            $fileName = $fileName;
          else
            $fileName = '';

          $getservice[$key] = array("text" => $service_text[$key], "image" =>$fileName);

          // dd($getservice);
        }
        // if(empty($getservice)){
        //   dd('step1');
        // }
        // dd('step2');
      }
      // dd($getservice);

      //Insert Prodcut Description

      $insert_Prdouctdata['product_id']=  $get_prod_id;
      $insert_Prdouctdata['lang_code']=$data['language'];
      $insert_Prdouctdata['name']=$data['product_name'];
      $insert_Prdouctdata['product_slug'] = $data['product_slug'];
      $insert_Prdouctdata['desc']=$data['desc'];
      $insert_Prdouctdata['short_desc']=$data['short_desc'];
      $insert_Prdouctdata['meta_title']= $data['meta_title'];
      $insert_Prdouctdata['meta_desc']= $data['meta_desc'];
      $insert_Prdouctdata['meta_keyword']= $data['meta_keyword'];
      $insert_Prdouctdata['tag']= $data['tag'];
      $insert_Prdouctdata['service']= json_encode($getservice);
      
      DB::table('product_description')->insert($insert_Prdouctdata);

      //Insert collection

      if(!empty($data['collection_id']))
      {
        $products['product_id'] = $get_prod_id;
        $products['collection_id'] = $data['collection_id'];

        DB::table('product_to_collection')->insert($products);
      }

      //Insert product_to_category

      if(!empty($data['categories_id']))
      {
        if(sizeof($data['categories_id']))
        {
          foreach ($data['categories_id'] as $key => $val)
          {
            $categ['product_id'] = $get_prod_id;
            $categ['category_id'] = $val;
            
            DB::table('product_to_category')->insert($categ);
          }
        }
      }

      //Insert store_location

      if(!empty($data['store_id']))
      {
        if(sizeof($data['store_id']))
        {
          foreach ($data['store_id'] as $key => $stor_id)
          {
            $store['product_id'] = $get_prod_id;
            $store['store_id'] = $stor_id;
            
            DB::table('product_to_store')->insert($store);
          }
        }
      }

      //Insert Product Attribute

      if(!empty($data['attribute_id']))
      {
        if(sizeof($data['attribute_id']))
        {
          $attribute_id = $data['attribute_id'];
          foreach ($attribute_id as $key => $value)
          {
            $insert_proattr['product_id']= $get_prod_id;
            $insert_proattr['attribute_id']=$value;
            $insert_proattr['text']=$data['text'][$key];
            DB::table('product_attibute')->insert($insert_proattr);
          }
        }
      }

      //Product Zoom slider Image Insert

      if(sizeof($data['ImageList']))
      {
        $ImageList = $data['ImageList'];
        foreach ($ImageList as $key => $val)
        {
          $insert_image['product_id']= $get_prod_id;
          $insert_image['image']=$val;
          $insert_image['sort_order']=$data['Imagesort_order'][$key];
          DB::table('product_image')->insert($insert_image);
        }
      }

      //Product Feature Image is upload
      if(!empty(Input::file('Feat_image_or_video')))
      {
        $featImage = Input::file('Feat_image_or_video');

        if(sizeof($featImage))
        {
          foreach ($featImage as $key => $value)
          {
            $imageName = pathinfo($value->getClientOriginalName(),PATHINFO_BASENAME);
            $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
            $uploadPath  = public_path() . "/product/features";

            if (!file_exists($uploadPath))
              File::makeDirectory($uploadPath);

            if ($featImage[$key]->move($uploadPath, $fileName))
            {
              $can3['features_img'][$key] = $fileName;
              $imagePath = $uploadPath . DIRECTORY_SEPARATOR .'/'. $fileName;
              $imgLarge = Image::make($imagePath);
              $imgLarge->resize(1260,900);
              $imgLarge->save($imagePath);
            }
          }
        }
        else
        {
          $can3['features_img'] = "";
        }
      }

      if(!empty(Input::file('Feat_image_or_video')))
      {
        if(sizeof($data['fsort_order']))
        {
          $fsort_order = $data['fsort_order'];
          foreach ($fsort_order as $key => $value)
          {
            $insert_feat_image['product_id']= $get_prod_id;
            $insert_feat_image['name']=$data['heading'][$key];
            $insert_feat_image['prod_des']=$data['Feat_desc'][$key];

            $featImage = $_FILES['Feat_image_or_video']['name'][$key];
            $image = Input::file('Feat_image_or_video');

            $imageName = pathinfo($image[$key]->getClientOriginalName(),PATHINFO_BASENAME);
            $features_img = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
            // $uploadPath  = public_path() . "/product/features";


            // $prodFeadimage = pathinfo($featImage, PATHINFO_FILENAME);
            // $extension = pathinfo($featImage, PATHINFO_EXTENSION);
            // $can3['features_img'] = strtolower('product-'.$prodFeadimage.'.'.$extension);

            $insert_feat_image['prod_img_or_vdo']=$features_img;
            $insert_feat_image['sort_order']=$value;
            $insert_feat_image['status']=1;
            DB::table('product_features')->insert($insert_feat_image);
          }
        }
      }

      //Insert related products

      if(!empty($data['rproduct']))
      {
        if(sizeof($data['rproduct']))
        {
          foreach ($data['rproduct'] as $key => $val)
          {
            $rproduct['product_id'] = $get_prod_id;
            $rproduct['rproduct_id'] = $val;
            
            DB::table('related_products')->insert($rproduct);
          }
        }
      }

      //Insert other products

      if(!empty($data['oproduct']))
      {
        if(sizeof($data['oproduct']))
        {
          foreach ($data['oproduct'] as $key => $val)
          {
            $oproduct['product_id'] = $get_prod_id;
            $oproduct['oproduct_id'] = $val;
            
            DB::table('other_products')->insert($oproduct);
          }
        }
      }
      return Redirect('/Admin/product/')->with('message','New Product Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function deleteprodFeatimg()
  {
    $status = array('status'=>false);
    $id = Input::get('id');

    $prod_feat = DB::table('product_features')
    ->where('product_feature_id',$id)->get();
    if(sizeof($prod_feat)) {
      $status = array('status'=>true);
      $dirPath = public_path().'/product/features/';
      DB::table('product_features')
      ->Where('product_feature_id',$id)->delete();
      @File::delete($dirPath.$prod_feat[0]->prod_img_or_vdo);
    }
    echo json_encode($status);
  }
  public function deleteprodSubimg()
  {
    $status = array('status'=>false);
    $id = Input::get('id');

    $prod_Img = DB::table('product_image')
    ->where('product_image_id',$id)->get();
    if(sizeof($prod_Img)) {
      $status = array('status'=>true);
      // $dirPath = public_path().'/product/features/';
      @File::delete(public_path()."/product/large/".$prod_Img[0]->prod_img_or_vdo);
      @File::delete(public_path()."/product/thumb/".$prod_Img[0]->prod_img_or_vdo);
      @File::delete(public_path()."/product/small/".$prod_Img[0]->prod_img_or_vdo);
      DB::table('product_image')
      ->Where('product_image_id',$id)->delete();
      // @File::delete($dirPath.$prod_Img[0]->prod_img_or_vdo);
    }
    echo json_encode($status);
  }
  public function deleteprodAttrimg()
  {
    $status = array('status'=>false);
    $id = Input::get('id');

    $prod_attr = DB::table('product_attibute')
    ->where('attribute_id',$id)->get();
    if(sizeof($prod_attr)) {
      $status = array('status'=>true);
      DB::table('product_attibute')
      ->Where('attribute_id',$id)->delete();
    }
    echo json_encode($id);
  }
  //Edit Product
  public function edit_Product(Request $data)
  {
    try
    {
      // dd($data->all());
      /*Form Validation*/
      $validator = Validator::make($data->all(), [
        'language' => 'required',
        'product_name' => 'required',
        'product_slug' => 'required',
        'meta_title' => 'required',
        'sku' => 'required',
        // 'stock_status' => 'required',
        'product_type' => 'required',
        'status' => 'required',
        'brand' => 'required',
      ]);

      if ($validator->fails()) {
        return back()
        ->withErrors($validator,'product_errors')
        ->withInput();
      }

      unset($data['_token']);
      $getservice = '';
      
      $ptype_id = $data['product_type'];

      $service_text = $data['service_text'];
      $insert_service_image = $data['insert_service_image'];

      if(!empty(Input::file('Product_image')))
      {
        $image = Input::file('Product_image');

        $imageName = pathinfo($image->getClientOriginalName(),PATHINFO_BASENAME);
        $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
        $path  = public_path() . "/product";
        $uploadPath  = public_path() . "/product/large";

        if (!file_exists($uploadPath))
          File::makeDirectory($uploadPath);

        @File::delete($uploadPath.'/'.$data['InsertProduct_image']);
        @File::delete($path.'/small/'.$data['InsertProduct_image']);
        @File::delete($path.'/thumb/'.$data['InsertProduct_image']);

        if (Input::file('Product_image')->move($uploadPath, $fileName))
        {
          $largePath = $path . DIRECTORY_SEPARATOR .'/large/'. $fileName;
          $imgLarge = Image::make($largePath);
          $imgLarge->resize(1260,900);
          $imgLarge->save($largePath);

          $product_image = $fileName;
          $diretorio = 'small';
        }
        if($diretorio == 'small')
        {
          $smallFolder = $path . DIRECTORY_SEPARATOR .'/small/'. $fileName;
          $img = Image::make($largePath);
          $img->resize(560,400);
          $img->save($smallFolder);
          $diretorio = 'thumb';
        }
        if($diretorio == 'thumb')
        {
          $thumbFolder = $path . DIRECTORY_SEPARATOR .'/thumb/'. $fileName;
          $img = Image::make($largePath);
          $img->resize(84,83);
          $img->save($thumbFolder);
        }
      }
      else
      {
        $product_image = $data['InsertProduct_image'];
      }

      $insert_data['sku']= $data['sku'];
      $insert_data['ptype_id']=$ptype_id;
      $insert_data['quantity']=$data['quantity'];
      $insert_data['stock_status_id']=1;//$data['stock_status'];
      $insert_data['prod_slider_image']=$product_image;
      $insert_data['comfort']=$data['comfort'];
      $insert_data['brand_id']=$data['brand'];
      $insert_data['price']=$data['price'];
      $insert_data['date_available']= date('Y-m-d',strtotime($data['date_available']));
      $insert_data['weight_class_id']= $data['weight_class_id'];
      $insert_data['weight']=$data['weight'];
      $insert_data['length_class_id']= $data['length_class_id'];
      $insert_data['length']=$data['length'];
      $insert_data['width']=$data['width'];
      $insert_data['height']=$data['height'];
      $insert_data['subtract']=$data['subtract'];
      $insert_data['sort_order']=$data['sort_order'];
      $insert_data['updated_at']= date("Y-m-d H:i:s");
      $insert_data['updated_by']= $data->ip();
      $insert_data['status']= $data['status'];

      DB::table('product')->Where('product_id',$data['txt_product_id'])->update($insert_data);

      if(!empty($insert_service_image))
      {
        foreach ($insert_service_image as $key1 => $ins_bvalue)
        {
          if($ins_bvalue)
          {
            $getservice[$key1] = array("text" => $service_text[$key1], "image" =>$ins_bvalue); 
          }
        }
      }

      //INSERT SERVICE
      if(!empty(Input::file('service_image')))
      {
        $service_image = Input::file('service_image');
        if(sizeof($service_image))
        {
          foreach ($service_image as $key => $value)
          {
            if($value)
            {
              $imageName = pathinfo($value->getClientOriginalName(),PATHINFO_BASENAME);
              $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
              $uploadPath  = public_path() . "/product/service_img/";

              if (!file_exists($uploadPath))
                File::makeDirectory($uploadPath);

              if ($service_image[$key]->move($uploadPath, $fileName))
              {
                $serice_name['service_image'][$key] = $fileName;
              }
              $getservice[$key] = array("text" => $service_text[$key], "image" =>$fileName);
            }
          }
        }
      }

      //Insert Prodcut data

      $insert_Prdouctdata['lang_code']=$data['language'];
      $insert_Prdouctdata['product_slug']=$data['product_slug'];
      $insert_Prdouctdata['name']=$data['product_name'];
      $insert_Prdouctdata['desc']=$data['desc'];
      $insert_Prdouctdata['short_desc']=$data['short_desc'];
      $insert_Prdouctdata['meta_title']= $data['meta_title'];
      $insert_Prdouctdata['meta_desc']= $data['meta_desc'];
      $insert_Prdouctdata['meta_keyword']= $data['meta_keyword'];
      $insert_Prdouctdata['tag']= $data['tag'];
      $insert_Prdouctdata['service']= json_encode($getservice);

      // dd($insert_Prdouctdata);
      $checkProductDesc = DB::table('product_description')->where('product_id',$data['txt_product_id'])->where('lang_code',$insert_Prdouctdata['lang_code'])->first();

      if($checkProductDesc)
      {
        DB::table('product_description')
        ->Where('product_id',$data['txt_product_id'])
        ->where('lang_code',$insert_Prdouctdata['lang_code'])
        ->update($insert_Prdouctdata);
      }
      else
      {
        $insert_Prdouctdata['product_id']= $data['txt_product_id'];
        DB::table('product_description')->insert($insert_Prdouctdata);
      }
      
      $Delete_pro_collection = DB::table('product_to_collection')->where('product_id',$data['txt_product_id'])->delete();

      if(!empty($data['collection_id']))
      {
        //Insert collection
        $products['product_id'] = $data['txt_product_id'];
        $products['collection_id'] = $data['collection_id'];
        
        DB::table('product_to_collection')->insert($products);
      }

      //Update categories

      if(!empty($data['categories_id']))
      {
        if(sizeof($data['categories_id']))
        {
          $Delete_pro_collection = DB::table('product_to_category')->where('product_id',$data['txt_product_id'])->delete();
          foreach ($data['categories_id'] as $key => $val)
          {
            $categ['product_id'] = $data['txt_product_id'];
            $categ['category_id'] = $val;
            
            DB::table('product_to_category')->insert($categ);
          }
        }
      }

      //Update store_id

      $Delete_pro_store = DB::table('product_to_store')->where('product_id',$data['txt_product_id'])->delete();

      if(!empty($data['store_id']))
      {
        if(sizeof($data['store_id']))
        {
          foreach ($data['store_id'] as $key => $store_val)
          {
            $store['product_id'] = $data['txt_product_id'];
            $store['store_id'] = $store_val;
            
            DB::table('product_to_store')->insert($store);
          }
        }
      }

      //Insert Product Dimension
      // DB::table('product_dimension')->where('product_id',$data['txt_product_id'])->delete();
      
      // if(!empty($data['width']))
      // {
      //   if(sizeof($data['width'])){
      //     foreach ($data['width'] as $key => $val)
      //     {
      //       $dimension['product_id'] = $data['txt_product_id'];
      //       $dimension['ptype_id'] = $ptype_id;
      //       $dimension['length_class_id'] = $data['length_class_id'];
      //       $dimension['dwidth'] = $val;
      //       $dimension['dheight'] = $data['height'][$key];

      //       DB::table('product_dimension')->insert($dimension);
      //     }
      //   }
      // }

      //Update Product Attribute
      DB::table('product_attibute')->where('product_id',$data['txt_product_id'])->delete();

      if(!empty($data['attribute_id']))
      {
        if(sizeof($data['attribute_id']))
        {
          $attribute_id = $data['attribute_id'];
          foreach ($attribute_id as $key => $value)
          {
            $insert_proattr['product_id']= $data['txt_product_id'];
            $insert_proattr['attribute_id']=$value;
            $insert_proattr['text']=$data['text'][$key];
            DB::table('product_attibute')->insert($insert_proattr);
          }
        }
      }

      if(sizeof($data['ImageList']))
      {
        $ImageList = $data['ImageList'];

        foreach ($ImageList as $key => $val)
        {
          $insert_image['product_id'] = $data['txt_product_id'];
          $insert_image['image'] = $val;
          $insert_image['sort_order'] = $data['Imagesort_order'][$key];
          
          DB::table('product_image')->insert($insert_image);
        }
      }

      //INSERT FEAT_IMAGE_OR_VIDEO
      if(!empty(Input::file('Feat_image_or_video')))
      {
        $prod_image = Input::file('Feat_image_or_video');

        if(sizeof($prod_image))
        {
          foreach ($prod_image as $key => $value)
          {
            $imageName = pathinfo($value->getClientOriginalName(),PATHINFO_BASENAME);
            $fileName = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $imageName));
            $uploadPath  = public_path() . "/product/features";

            if (!file_exists($uploadPath))
              File::makeDirectory($uploadPath);

            if ($prod_image[$key]->move($uploadPath, $fileName))
            {
              $can3['features_img'][$key] = $fileName;

              $imagePath = $uploadPath . DIRECTORY_SEPARATOR .'/'. $fileName;
              $imgLarge = Image::make($imagePath);
              $imgLarge->resize(1260,900);
              $imgLarge->save($imagePath);
            }
          }
        }
      }

      /*DELETE PRODUCT FEATURE IMAGES*/

      DB::table('product_features')
      ->Where('product_id',$data['txt_product_id'])
      ->delete();

      if(sizeof($data['fsort_order']))
      {
        foreach ($data['fsort_order'] as $key => $value)
        {
          $feaImg = '';
          $insert_feat_image['product_id']= $data['txt_product_id'];
          $insert_feat_image['name']=$data['heading'][$key];
          $insert_feat_image['prod_des']=$data['Feat_desc'][$key];

          if(isset($data['oldFeat_image_video'][$key]))
            $feaImg = $data['oldFeat_image_video'][$key];

          if(isset($data['Feat_image_or_video'][$key]))
          {
            if(isset( Input::file('Feat_image_or_video')[$key] ))
            {
              /*IF IMAGE IS REPLACE THEN DELETE BELOW FILE*/
              // $delImageName = Input::file('Feat_image_or_video')[$key];
              @File::delete($uploadPath.'/'.$data['oldFeat_image_video'][$key]);
              //$ecatalogue_desc[0]->ec_image);
            }
            /*Do upload codes here*/
            $feaImg = strtolower(preg_replace("^[\\\\/:\*\?\"'<>\|]^", "_", $_FILES['Feat_image_or_video']['name'][$key]));
            // $feaImg = strtolower(str_slug($_FILES['Feat_image_or_video']['name'][$key]));
          }

          $insert_feat_image['prod_img_or_vdo']=$feaImg;
          $insert_feat_image['sort_order']=$value;
          $insert_feat_image['status']=1;
          DB::table('product_features')->insert($insert_feat_image);
        }
      }

      //Insert related product

      if(!empty($data['rproduct']))
      {
        if(sizeof($data['rproduct']))
        {
           DB::table('related_products')->where('product_id',$data['txt_product_id'])->delete();
          foreach ($data['rproduct'] as $key => $val)
          {
            $rproduct['product_id'] = $data['txt_product_id'];
            $rproduct['rproduct_id'] = $val;
            
            DB::table('related_products')->insert($rproduct);
          }
        }
      }

      //Insert other product

      if(!empty($data['oproduct']))
      {
        if(sizeof($data['oproduct']))
        {
           DB::table('other_products')->where('product_id',$data['txt_product_id'])->delete();
          foreach ($data['oproduct'] as $key => $val)
          {
            $oproduct['product_id'] = $data['txt_product_id'];
            $oproduct['oproduct_id'] = $val;
            
            DB::table('other_products')->insert($oproduct);
          }
        }
      }

      //-------------------------------------------------------

      return Redirect('/Admin/product/')->with('message','Product Updated successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception){
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function editProduct($lang,$id)
  {
    try
    {
      $allProduct = Product::getLanguageProductInfo($lang);

      $storeLang = DB::table('language')
        ->where('status',1)
        ->get();
      $productInfo = json_decode($allProduct);
      // foreach ($productInfo->collection as $key => $value) {
      //   echo "<pre>";
      //   print_r($value);
      // }
      // die();
      $product = Product::getEditProducts($id,$lang);
      // dd($product);

      $passData = compact('storeLang','productInfo','product');
      return view('Adminproductedit.editProduct', $passData);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //Delete Product
  public function deleteProduct(Request $data,$id)
  {
    try
    {
      // dd($id);
      $productImageData = DB::table('product_image')->where('product_id',$id)->get();
      $productData = DB::table('product')->where('product_id',$id)->first();
      $productImage = DB::table('product_image')->where('product_id',$id)->get();
      $productFeatures = DB::table('product_features')->where('product_id',$id)->get();

      if(sizeof($productData))
      {
        @File::delete(public_path() . "/product/images/".$productData->prod_slider_image);

        if(sizeof($productImage))
        {
          foreach ($productImage as $key => $value)
          {
            @File::delete(public_path().'/product/large/'.$value->image);
            @File::delete(public_path().'/product/thumb/'.$value->image);
            @File::delete(public_path().'/product/small/'.$value->image);
          }
        }

        if(sizeof($productFeatures))
        {
          foreach ($productFeatures as $key => $value)
          {
            @File::delete(public_path().'/product/features/'.$value->prod_img_or_vdo);
          }
        }

        DB::table('product')->where('product_id',$id)->delete();
        DB::table('product_to_collection')->where('product_id',$id)->delete();
        DB::table('product_to_category')->where('product_id',$id)->delete();
        DB::table('related_products')->where('product_id',$id)->delete();
        DB::table('other_products')->where('product_id',$id)->delete();
        DB::table('product_to_store')->where('product_id',$id)->delete();
        DB::table('product_description')->where('product_id',$id)->delete();
        DB::table('product_attibute')->where('product_id',$id)->delete();
        DB::table('product_image')->where('product_id',$id)->delete();
        DB::table('product_features')->where('product_id',$id)->delete();
      }
      return Redirect('/Admin/product/')->with('message','Product Delete Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //WEIGHT INFO

  public function viewWeight(Request $data)
  {
    try
    {
      $viewWeight = DB::table('weight_class')
      ->join('weight_class_description','weight_class_description.weight_class_id','=','weight_class.weight_class_id')          
      ->join('language','language.lang_code','=','weight_class_description.lang_code')
      ->Where('language.status',1)
      ->get();

      $language = DB::table('language')->where('status',1)->get();

      return view('Adminproduct.weight')->with('weight',$viewWeight)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function addweight(Request $data)
  {
    $language = DB::table('language')->where('status',1)->get();
    return view('Adminproduct.addweight')->with('language',$language);
  }
  public function add_weight(Request $data)
  {
    try
    {
      $insert_we_class['value'] = $data['value'];
      $insert_we_class['default'] = $data['default'];

      $get_insert_id = DB::table('weight_class')->insertGetId($insert_we_class);

      $weight_class_des['weight_class_id'] = $get_insert_id;
      $weight_class_des['lang_code'] = $data['lang_code'];
      $weight_class_des['title'] = $data['title'];
      $weight_class_des['unit'] = $data['unit'];
      $insert = DB::table('weight_class_description')->Insert($weight_class_des);
      return Redirect('/Admin/weight')->with('message','Weight Added successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function deleteweight(Request $data,$id)
  {
    try
    {
      $delete = DB::table('weight_class_description')->where('weight_class_id',$id)->delete();
      $delete_we_class = DB::table('weight_class')->where('weight_class_id',$id)->delete();
      return Redirect('/Admin/weight')->with('message','Weight Delete successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function editweight(Request $data,$id)
  {
    try
    {
      $viewWeight = DB::table('weight_class')
      ->join('weight_class_description','weight_class_description.weight_class_id','=','weight_class.weight_class_id')          
      ->join('language','language.lang_code','=','weight_class_description.lang_code')
      ->Where('language.status',1) 
      ->Where('weight_class.weight_class_id',$id) 
      ->get();

      $language = DB::table('language')->where('status',1)->get();

      return view('Adminproduct.editweight')->with('weight',$viewWeight)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }
  public function edit_weight(Request $data)
  {
    try
    {
      $insert_we_class['value'] = $data['value'];
      $insert_we_class['default'] = $data['default'];

      // $weightData = DB::table('weight_class')
      //   ->where('weight_class_id',$data['weightclass_id'])
      //   ->first();

      $weightData = DB::table('weight_class_description')
        ->where('weight_class_id',$data['weightclass_id'])
        ->where('lang_code',$data['lang_code'])
        ->first();

      $updatData['title'] = $data['title'];
      $updatData['unit'] = $data['unit'];
      $updatData['lang_code'] = $data['lang_code'];

      $updatWeightData['unit'] = $data['unit'];

      if($weightData)
      {
        DB::table('weight_class')
          ->where('weight_class_id',$data['weightclass_id'])
          ->update($updatWeightData);

        DB::table('weight_class_description')
          ->where('weight_class_id',$data['weightclass_id'])
          ->update($updatData);
      }

        dd($weightData);

      DB::table('weight_class')->where('weight_class_id',$data['weightclass_id'])->update($insert_we_class);

      $weight_class_des['lang_code'] = $data['lang_code'];
      $weight_class_des['title'] = $data['title'];
      $weight_class_des['unit'] = $data['unit'];

      $insert = DB::table('weight_class_description')->where('weight_class_id',$data['weightclass_id'])->update($weight_class_des);

      return Redirect('/Admin/weight')->with('message','Weight Updated successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //LENGTH INFO

public function viewlength(Request $data){
  try{
    $viewlength = DB::table('length_class')
          ->join('length_class_description','length_class_description.length_class_id','=','length_class.length_class_id')          
          ->join('language','language.lang_code','=','length_class_description.lang_code')
          ->Where('language.status',1) 
          ->get();
    $language = DB::table('language')->Where('status',1)->get();
    //dd($viewWeight);

    return view('Adminproduct.length')->with('length',$viewlength)->with('language',$language);
  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

public function addlength(Request $data){

    $language = DB::table('language')->where('status',1)->get();
   return view('Adminproduct.addlength')->with('language',$language);
}

public function add_length(Request $data){
  try{
    //dd($data->all());

      $insert_le_class['value'] = $data['value'];
      $insert_le_class['default'] = $data['default'];

      $get_insert_id = DB::table('length_class')->insertGetId($insert_le_class);

      $length_class_des['length_class_id'] = $get_insert_id;
      $length_class_des['lang_code'] = $data['lang_code'];
      $length_class_des['title'] = $data['title'];
      $length_class_des['unit'] = $data['unit'];

      $insert = DB::table('length_class_description')->Insert($length_class_des);

       return Redirect('/Admin/length')->with('message','Length Added successfully...');

    }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}


public function deletelength(Request $data,$id){
  try{

    $delete = DB::table('length_class_description')->where('length_class_id',$id)->delete();
    $delete_lee_class = DB::table('length_class')->where('length_class_id',$id)->delete();

  return Redirect('/Admin/length')->with('message','Length Delete successfully...');
  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

public function editlength(Request $data,$id){
  try{

      $viewlength = DB::table('length_class')
          ->join('length_class_description','length_class_description.length_class_id','=','length_class.length_class_id')          
          ->join('language','language.lang_code','=','length_class_description.lang_code')
          ->Where('language.status',1) 
          ->Where('length_class.length_class_id',$id) 
          ->get();
    

    $language = DB::table('language')->where('status',1)->get();

    return view('Adminproduct.editlength')->with('length',$viewlength)->with('language',$language);
  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}


public function edit_length(Request $data){
  try{

       $insert_le_class['value'] = $data['value'];
      $insert_le_class['default'] = $data['default'];

      $get_insert_id = DB::table('length_class')->where('length_class_id',$data['lengthclass_id'])->update($insert_le_class);

      $length_class_des['lang_code'] = $data['lang_code'];
      $length_class_des['title'] = $data['title'];
      $length_class_des['unit'] = $data['unit'];

      $insert = DB::table('length_class_description')->where('length_class_id',$data['lengthclass_id'])->update($length_class_des);

       return Redirect('/Admin/length')->with('message','Length Updated successfully...');

  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}



//Review

public function viewreview(Request $data)
{
  $product_review = DB::table('product_review as r')
  ->select('r.review_id','r.lang_code','r.name','r.text','rating','r.created_at as review_date','d.name as p_name')
  ->join('product as p','p.product_id','=','r.product_id')
  ->join('product_description as d','p.product_id','=','d.product_id')    
  //->Where('product_review.status',1)
  ->get();
  /*$product_review = DB::table('product_review')
  ->select(DB::raw('product_review.review_id,product_review.name,product_review.text,product_review.rating,product_review.review_date,product_description.name as p_name'))
  ->join('product','product.product_id','=','product_review.product_id')
  ->join('product_description','product.product_id','=','product_description.product_id')    
  //->Where('product_review.status',1)
  ->get();*/
  $language = DB::table('language')->Where('status',1)->get();
  return view('Adminproduct.review')->with('review',$product_review)->with('language',$language);
}

public function addreview(Request $data){

  $product = DB::table('product')
      ->join('product_description','product.product_id','=','product_description.product_id')
      ->Where('product.status',1) 
      ->get();
  return view('Adminproduct.addreview')->with('product',$product);
}

public function add_review(Request $data){
  try{
    //dd($data->all());
    $user_id = Session::get('id');

    $review['product_id'] = $data['product_id'];
    $review['user_id'] = $user_id;;
    $review['title'] = $data['title'];
    $review['name'] = $data['name'];
    $review['emailid'] = $data['emailid'];
    $review['rating'] = $data['rating'];
    $review['text'] = $data['text'];
    $review['review_date'] = date("Y-m-d H:i:s");
    $review['created_at'] = date("Y-m-d H:i:s");
    $review['created_by'] =  $data->ip();
    $review['status'] = $data['status'];

    $insert = DB::table('product_review')->insert($review);

    return Redirect('/Admin/review')->with('message','Review Added successfully...');

  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

public function deletereview(Request $data,$id){
  try
  {

    $insert = DB::table('product_review')->where('review_id',$id)->delete();
    return Redirect('/Admin/review')->with('message','Review Delete successfully...');
  }  
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

public function editreview(Request $data,$id){
  try
  {

      $product = DB::table('product')
      ->join('product_description','product.product_id','=','product_description.product_id')
      ->Where('product.status',1) 
      ->get();

      $pre_review = DB::table('product_review')->where('review_id',$id)->get();

      return view('Adminproduct.editreview')->with('product',$product)->with('review',$pre_review);

  }  
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

public function edit_review(Request $data){
  try {
    
    $user_id = Session::get('id');

    $review['product_id'] = $data['product_id'];
    $review['user_id'] = $user_id;;
    $review['title'] = $data['title'];
    $review['name'] = $data['name'];
    $review['emailid'] = $data['emailid'];
    $review['rating'] = $data['rating'];
    $review['text'] = $data['text'];
    $review['review_date'] = date("Y-m-d H:i:s");
    $review['updated_at'] = date("Y-m-d H:i:s");
    $review['updated_by'] =  $data->ip();
    $review['status'] = $data['status'];

    $insert = DB::table('product_review')->Where('review_id',$data['pre_review_id'])->update($review);

    return Redirect('/Admin/review')->with('message','Review Updated successfully...');
  }  
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

  //PRODUCT ASK A QUESTION & ANSWERS

  public function productTypes()
  {
    $productData = DB::table('product_type')
    ->join('product_type_desc','product_type_desc.product_type_id','=','product_type.id')
    ->get();

    // dd($productData);

    $language = DB::table('language')->Where('status',1)->get();

    return view('Adminproduct.product_types')->with('productData',$productData)->with('language',$language);
  }

  public function addProductType()
  {
    $language = DB::table('language')->Where('status',1)->get();
    return view('Adminproduct.add_product_type')->with('language',$language);
  }

  public function insertProductType(Request $data)
  {
    try
    {
      $insertProduct['status'] = $data['status'];
      $insertProduct['sort_order'] = $data['sort_order'];
      $get_id = DB::table('product_type')->insertGetId($insertProduct);

      $insertData['product_type_id'] = $get_id;
      $insertData['lang_code'] = $data['lang_code'];
      $insertData['ptype_name'] = $data['name'];

      DB::table('product_type_desc')->insert($insertData);
      return Redirect('/Admin/product-type')->with('message','New Product Type Added Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function editProductType($lang,$id)
  {
    $product_type = DB::table('product_type')
    ->join('product_type_desc','product_type.id','=','product_type_desc.product_type_id')
    ->Where('id',$id)
    ->Where('lang_code',$lang)
    ->first();
    // dd($product_type);
    $language = DB::table('language')->Where('status',1)->get();
    return view('Adminproduct.edit_product_type')->with('productType',$product_type)->with('language',$language);
  }

  public function updateProductType(Request $data)
  {
    try
    {
      $id = $data['ptypeid'];
      $lang = $data['lang_code'];

      $checkProductType = DB::table('product_type_desc')
        ->where('product_type_id',$id)
        ->where('lang_code',$lang)
        ->first();

        // dd($checkProductType);

      $product_type_desc['lang_code'] = $data['lang_code'];
      $product_type_desc['ptype_name'] = $data['name'];

      if($checkProductType)
      {
        DB::table('product_type_desc')
        ->Where('product_type_id',$id)
        ->Where('lang_code',$lang)
        ->update($product_type_desc);

        $product_type['sort_order'] = $data['sort_order'];
        $product_type['status'] = $data['status'];

        DB::table('product_type')
        ->Where('id',$id)
        ->update($product_type);

        $msgData = 'Product Type Updated Successfully...';
      }
      else
      {
        $product_type_desc['product_type_id'] = $id;
        $product_type_desc['lang_code'] = $data['lang_code'];
        $product_type_desc['ptype_name'] = $data['name'];

        DB::table('product_type_desc')->insert($product_type_desc);

        $msgData = 'New Product Type Inserted Successfully...';
      }      
      return Redirect('/Admin/product-type')->with('message','Product Type Updated Successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  // public function checkProductType(Request $data)
  // {
  //   try
  //   {
  //     $productslug = strtolower(str_replace(' ','-',$data['getslug']));
  //     $product_slug = DB::table('product_type')->where('ptype_slug',$productslug)->first();
  //     if(!empty($product_slug))
  //     {
  //       if($product_slug->ptype_slug != '')
  //       {
  //         return 1;
  //       }
  //       else
  //       {
  //         return 0;
  //       }
  //     }
  //     else
  //     {
  //       return 0;
  //     }
  //   }
  //   catch(\Illuminate\Database\QueryException $exception)
  //   {
  //     $a = $exception->errorInfo;
  //     return view('errors.custom')->with('message',end($a));
  //   }
  // }

  public function deleteProductType($id)
  {
    DB::table('product_type')->Where('id',$id)->delete();
    return Redirect('/Admin/product-type')->with('message','Product Type Deleted Successfully...');
  }

  //PRODUCT ASK A QUESTION & ANSWERS

  public function viewQuestion(Request $data)
  {
    $product_questions = DB::table('product_question as q')
    ->select('q.question_id','q.name','q.emailid','q.lang_code','q.question','q.answer','q.created_at','d.name as q_name','q.status')
    ->join('product as p','p.product_id','=','q.product_id')
    ->join('product_description as d','p.product_id','=','d.product_id')
    ->get();

    $language = DB::table('language')->Where('status',1)->get();

    /*$product_review = DB::table('product_review')
    ->select(DB::raw('product_review.review_id,product_review.name,product_review.text,product_review.rating,product_review.review_date,product_description.name as p_name'))
    ->join('product','product.product_id','=','product_review.product_id')
    ->join('product_description','product.product_id','=','product_description.product_id')    
    //->Where('product_review.status',1)
    ->get();*/

    return view('Adminproduct.questions')->with('question',$product_questions)->with('language',$language);
  }

  public function addQuestion(Request $data)
  {
    $product = DB::table('product')
    ->join('product_description','product.product_id','=','product_description.product_id')
    ->Where('product.status',1) 
    ->get();

    $language = DB::table('language')->Where('status',1)->get();

    return view('Adminproduct.addquestion')->with('product',$product)->with('language',$language);
  }

  public function add_question(Request $data)
  {
    try
    {
      $user_id = Session::get('id');

      $question['lang_code'] = $data['lang_code'];
      $question['name'] = $data['name'];
      $question['emailid'] = $data['emailid'];
      $question['product_id'] = $data['product_id'];
      $question['question'] = $data['question'];
      $question['answer'] = $data['answer'];
      $question['user_id'] = $user_id;
      $question['created_at'] = date("Y-m-d H:i:s");
      $question['created_by'] =  $data->ip();
      $question['status'] = $data['status'];
      DB::table('product_question')->insert($question);
      return Redirect('/Admin/question')->with('message','Questions Added successfully...');
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function deleteQuestion(Request $data,$id)
  {
    try
    {
      $insert = DB::table('product_question')->where('question_id',$id)->delete();
      return Redirect('/Admin/question')->with('message','Question Delete successfully...');
    }  
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function editQuestion(Request $data,$id)
  {
    try
    {
      $product = DB::table('product')
      ->join('product_description','product.product_id','=','product_description.product_id')
      ->Where('product.status',1) 
      ->get();
      
      $language = DB::table('language')->Where('status',1)->get();

      $pre_question = DB::table('product_question')->where('question_id',$id)->get();
      return view('Adminproduct.editquestion')->with('product',$product)->with('question',$pre_question)->with('language',$language);
    }
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  public function edit_question(Request $data)
  {
    try
    {
      $user_id = Session::get('id');
      $question['lang_code'] = $data['lang_code'];
      $question['product_id'] = $data['product_id'];
      $question['user_id'] = $user_id;
      $question['name'] = $data['name'];
      $question['emailid'] = $data['emailid'];
      $question['question'] = $data['question'];
      $question['answer'] = $data['answer'];
      $question['updated_at'] = date("Y-m-d H:i:s");
      $question['updated_by'] =  $data->ip();
      $question['status'] = $data['status'];

      DB::table('product_question')->Where('question_id',$data['questionid'])->update($question);

      return Redirect('/Admin/question')->with('message','Question Updated Successfully...');
    }  
    catch(\Illuminate\Database\QueryException $exception)
    {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
    }
  }

  //ATTRUBUTE INFO

public function viewattribute(Request $data)
{
   $viewattr = DB::table('attribute')
          ->join('attribute_description','attribute_description.attribute_id','=','attribute.attribute_id')          
          ->join('language','language.lang_code','=','attribute_description.lang_code')
          ->Where('language.status',1) 
          ->get();
    $language = DB::table('language')->Where('status',1)->get();

return view('Adminproduct.attributeview')->with('attr',$viewattr)->with('language',$language);
}

public function addattribute(Request $data){
  try{
    $language = DB::table('language')->where('status',1)->get();

    $att_group = DB::table('attribute_group')
          ->join('attribute_group_description','attribute_group_description.attribute_group_id','=','attribute_group.attribute_group_id')          
          //->join('language','language.lang_code','=','attribute_description.lang_code')
          //->Where('language.status',1) 
          ->get();
          
//dd($att_group);
    return view('Adminproduct.addattribute')->with('attr_group',$att_group)->with('language',$language);
  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}


public function add_attribute(Request $data){
  try {
    //dd($data->all());

     
    $attribute['attribute_group_id'] = $data['group_id'];
    $attribute['sort_order'] = $data['sort_order'];

    $get_id = DB::table('attribute')->insertGetId($attribute);

    $attribute_dec['attribute_id'] = $get_id;
    $attribute_dec['lang_code'] = $data['lang_code'];
    $attribute_dec['name'] = $data['name'];

    $insert = DB::table('attribute_description')->insert($attribute_dec);

    return Redirect('/Admin/attribute')->with('message','Attribute Added successfully...');
  } 
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}


public function deleteattribute(Request $data,$id){
  try{
     $delete = DB::table('attribute_description')->where('attribute_id',$id)->delete();
    $delete_attr_dec = DB::table('attribute')->where('attribute_id',$id)->delete();

     return Redirect('/Admin/attribute')->with('message','Attribute Delete successfully...');
  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }

}

public function editattribute(Request $data,$id){
  try {

      $language = DB::table('language')->where('status',1)->get();

      $att_group = DB::table('attribute_group')
      ->join('attribute_group_description','attribute_group_description.attribute_group_id','=','attribute_group.attribute_group_id') 
      ->get();

      $attribute = DB::table('attribute')
      ->join('attribute_description','attribute_description.attribute_id','=','attribute.attribute_id') 
      ->where('attribute.attribute_id',$id) 
      ->get();
      //dd($attribute);

    return view('Adminproduct.editattribute')->with('attr_group',$att_group)->with('language',$language)->with('attribute',$attribute);
  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}


public function edit_attribute(Request $data){
  try {

      $attribute['attribute_group_id'] = $data['group_id'];
      $attribute['sort_order'] = $data['sort_order']; 

    $get_id = DB::table('attribute')->where('attribute_id',$data['attribute_id'])->update($attribute);

   // $attribute_dec['attribute_id'] = $get_id; 
    $attribute_dec['lang_code'] = $data['lang_code']; 
    $attribute_dec['name'] = $data['name']; 

    $insert = DB::table('attribute_description')
    ->where('attribute_id',$data['attribute_id'])->update($attribute_dec);

    return Redirect('/Admin/attribute')->with('message','Attribute Updated successfully...');
  } 
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}



//Attribute Group
public function viewattribute_group(Request $data){

  $viewattr = DB::table('attribute_group')
          ->join('attribute_group_description','attribute_group_description.attribute_group_id','=','attribute_group.attribute_group_id')          
          ->join('language','language.lang_code','=','attribute_group_description.lang_code')
          ->Where('language.status',1) 
          ->get();
  $language = DB::table('language')->Where('status',1)->get();
  return view('Adminproduct.attributeview_group')->with('attr_group',$viewattr)->with('language',$language);
}


public function AddattributeGroup(Request $data){
  try {
    $language = DB::table('language')->where('status',1)->get();
    return view('Adminproduct.addattribute_group')->with('language',$language);
  } 
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

public function add_attribute_group(Request $data){
  try {

   // dd($data->all());
    //$language = DB::table('language')->where('status',1)->get();
   /// return view('Adminproduct.addattribute_group')->with('language',$language);
    $attr_group['sort_order'] = $data['sort_order'];
    $get_attr_group = DB::table('attribute_group')->insertGetId($attr_group);

    $attr_group_desc['attribute_group_id'] = $get_attr_group;
    $attr_group_desc['lang_code'] = $data['lang_code'];
    $attr_group_desc['name'] = $data['name'];
    $insert = DB::table('attribute_group_description')->insert($attr_group_desc);


    return Redirect('/Admin/attribute_group')->with('message','Attribute Added successfully...');

  } 
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}


public function deleteattribute_group(Request $data,$id){
  try {
     $delete = DB::table('attribute_group_description')->where('attribute_group_id',$id)->delete();
    $delete_attr_dec = DB::table('attribute_group')->where('attribute_group_id',$id)->delete();

     return Redirect('/Admin/attribute_group')->with('message','Attribute Delete successfully...');
  
  } 
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}

public function editattribute_group(Request $data,$id){
  try {
    
  $viewattr = DB::table('attribute_group')
          ->join('attribute_group_description','attribute_group_description.attribute_group_id','=','attribute_group.attribute_group_id')          
          //->join('language','language.lang_code','=','attribute_group_description.lang_code')
          ->Where('attribute_group.attribute_group_id',$id) 
          ->get();
      $language = DB::table('language')->where('status',1)->get();
  //$language = DB::table('language')->where('status',1)->get();
    return view('Adminproduct.editattribute_group')->with('language',$language)->with('attrgroup',$viewattr);
  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}


public function edit_attribute_group(Request $data){
  try{
    //dd($data->all());
    
     $attr_group['sort_order'] = $data['sort_order'];
    $attr_group = DB::table('attribute_group')->where('attribute_group_id',$data['attribute_group_id'])->update($attr_group);

    //$attr_group_desc['attribute_group_id'] = $get_attr_group;
    $attr_group_desc['lang_code'] = $data['lang_code'];
    $attr_group_desc['name'] = $data['name'];
    $insert = DB::table('attribute_group_description')->where('attribute_group_id',$data['attribute_group_id'])->update($attr_group_desc);


    return Redirect('/Admin/attribute_group')->with('message','Attribute Updated successfully...');

  }
  catch(\Illuminate\Database\QueryException $exception)
  {
      $a = $exception->errorInfo;
      return view('errors.custom')->with('message',end($a));
  }
}
//Option

  public function viewOption(Request $data)
  {
    $viewopt = DB::table('option')
    ->join('option_description','option_description.option_id','=','option.option_id')          
    ->join('language','language.lang_code','=','option_description.lang_code')
    ->Where('language.status',1) 
    ->get();
    $language = DB::table('language')->where('status',1)->get();
    return view('Adminproduct.option')->with('option',$viewopt)->with('language',$language);
  }

    public function uploadImage_ajax(Request $data)
    {
      header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
      header("Cache-Control: no-store, no-cache, must-revalidate");
      header("Cache-Control: post-check=0, pre-check=0", false);
      header("Pragma: no-cache");

      @set_time_limit(5 * 60);

      $diretorio = $_GET["diretorio"];
      $newName = $_GET["newName"];
      $path = public_path();
      $targetLarge = $path."/product/large";
      $targetDir = $path."/product/".$diretorio;
      $cleanupTargetDir = true;
      $maxFileAge = 5 * 3600;

      if (!file_exists($targetLarge)) {
          @mkdir($targetLarge);
      }

      if (!file_exists($targetDir)) {
          @mkdir($targetDir);
      }

      if($data->file('file')->isValid())
        $fileName = $newName.'.'.$data->file('file')->getClientOriginalExtension();
      else
        $fileName = uniqid("file_");

      $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

      $chunk = isset($data->chunk) ? intval($data->chunk) : 0;
      $chunks = isset($data->chunks) ? intval($data->chunks) : 0;

      if ($cleanupTargetDir) {
          if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
              die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
          }

          while (($file = readdir($dir)) !== false) {
              $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

              // If temp file is current file proceed to the next
              if ($tmpfilePath == "{$filePath}.part") {
                  continue;
              }

              // Remove temp file if it is older than the max age and is not the current file
              if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                  @unlink($tmpfilePath);
              }
          }
          closedir($dir);
      }

      if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
          die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
      }

      if (!empty($data->file('file'))) {
          if (!$data->file('file')->isValid() || ! $data->hasFile('file') ) {
              die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
          }

          // Read binary input stream and append it to temp file
          if (!$in = @fopen($data->file('file')->getPathName(), "rb")) {
              die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
          }
      } else {    
          if (!$in = @fopen("php://input", "rb")) {
              die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
          }
      }

      while ($buff = fread($in, 4096)) {
          fwrite($out, $buff);
      }

      @fclose($out);
      @fclose($in);

      if (!$chunks || $chunk == $chunks - 1) {
          // Strip the temp .part suffix off 
          rename("{$filePath}.part", $filePath);
      }

      if($diretorio == 'thumb')
      {
        $imgLarge = Image::make($filePath);
        $largeFolder = $targetLarge . DIRECTORY_SEPARATOR . $fileName;
        $imgLarge->save($largeFolder);

        $img = Image::make($filePath);
        $img->resize(210, 140);
        $img->save($filePath);
      }
      else if($diretorio == 'small')
      {
        $img = Image::make($filePath);
        $img->resize(600, 400);
        $img->save($filePath);
      }
      die('{"jsonrpc" : "2.0", "result" : "'.$fileName.'", "id" : "id"}');
    }

    public function MattressSelect(Request $data)
    {
      $language = DB::table('language')->where('status',1)->get();
      $view_mattress = DB::table('admin_matt_selector')
      ->join('language','language.lang_code','=','admin_matt_selector.lang_code')
      // ->Where('language.status',1)
      // ->Where('admin_matt_selector.status',1)
      ->get();
      return view('lmgAdmin.mattress.view')->with('view_mattress',$view_mattress)->with('language',$language);
    }

    public function AddMattress(Request $data)
    {
      try
      {
        $product = DB::table('product')
        ->join('product_description','product_description.product_id','=','product.product_id')
        ->Where('status',1)
        ->get();
        $lang = DB::table('language')->Where('status',1)->get();

        return view('lmgAdmin.mattress.AddMattress')->with('language',$lang)->with('product',$product);
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function add_mattress(Request $data)
    {
      try
      {
        // dd($data->all());
        $mattress['lang_code'] = $data['lang_code'];

        if ( $data['buying'] == 'Myself') {

          $mattress['buying_mattress'] = $data['buying'];
          $mattress['mattress_new'] = $data['new_mattress'];
          $mattress['weight_range'] = $data['weight_range'];
          $mattress['age_range'] = $data['age_range'];
          $mattress['your_sleep_position'] = $data['your_sleep_position'];
          $mattress['issues'] = $data['issues'];
          $mattress['sleep_too'] = $data['sleep_too'];          
          $mattress['comfortable_sleep'] = $data['comfortable_sleep'];

          $mattress['status'] = $data['Status'];
          $mattress['created_at'] = date("Y-m-d H:i:s");
          $mattress['created_by'] = $data->ip();
          
        }
       
        if ($data['buying'] == 'My partner & I' || $data['buying'] == 'My partner, I & My kids') {
          
          if ($data['buying'] == 'My partner & I') {
            $mattress['buying_mattress'] = $data['buying'];
          }

          if ($data['buying'] == 'My partner, I & My kids') {
            $mattress['buying_mattress'] = $data['buying'];
          }
          
          $mattress['mattress_new'] = $data['new_mattress'];
          $mattress['weight_range'] = $data['weight_range'];
          $mattress['partner_weight_range'] = $data['Partner_weight_range'];
          $mattress['age_range'] = $data['age_range'];
          $mattress['partner_age_range'] = $data['Partner_age_range'];
          $mattress['your_sleep_position'] = $data['your_sleep_position'];
          $mattress['partner_sleep_position'] = $data['partner_sleep_position'];
          $mattress['issues'] = $data['issues'];          
          $mattress['sleep_too'] = $data['sleep_too'];
          $mattress['comfortable_sleep'] = $data['comfortable_sleep'];
          
          $mattress['status'] = $data['Status'];
          $mattress['created_at'] = date("Y-m-d H:i:s");
          $mattress['created_by'] = $data->ip();
        }

        if ($data['buying'] == 'Just for the spare bedroom') {

          $mattress['buying_mattress'] = $data['buying'];
          $mattress['sleep_guest'] = $data['sleep_guest'];
          $mattress['comfortable_sleep'] = $data['comfortable_sleep'];

          $mattress['status'] = $data['Status'];
          $mattress['created_at'] = date("Y-m-d H:i:s");
          $mattress['created_by'] = $data->ip();
        }

        // $insert = DB::table('admin_matt_selector')->insert($mattress);
        $get_insert_id = DB::table('admin_matt_selector')->insertGetId($mattress);

        // $count = count($data['recommandation_product']).'---'.count($data['related_product']);
        // dd($count);

        $a = count($data['recommandation_product']);
        $b = count($data['related_product']);

        if($a > $b){ $length = $a; }
        else { $length = $b; }

        for ($i=0; $i < $length; $i++) {

          $matt_selector_product['admin_matt_id'] = $get_insert_id;
          $matt_selector_product['recommandation_product'] = @$data['recommandation_product'][$i];
          $matt_selector_product['related_product'] = @$data['related_product'][$i];

          DB::table('admin_matt_selector_product')->insert($matt_selector_product);

          // echo "<pre>";
          // print_r(@$data['recommandation_product'][$i]);echo "<br>";
          // print_r(@$data['related_product'][$i]);echo "<br>";
        }
        // dd($matt_selector_product);
        return Redirect('/Admin/mattress-select')->with('message','Mattress Added Successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function deleteMattress_selector(Request $data,$id)
    {
      try
      {
        DB::table('admin_matt_selector')->Where('admin_matt_id',$id)->delete();
        DB::table('admin_matt_selector_product')->Where('admin_matt_id',$id)->delete();
        return Redirect('/Admin/mattress-select')->with('message','Mattress Deleted Successfully...');
      }    
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function editMattress_selector(Request $data,$id)
    {
      try
      {
        $matt_selector = DB::table('admin_matt_selector')
        ->Where('admin_matt_id',$id)->get();

        $product = DB::table('product')
        //$product_result
        ->join('product_description','product_description.product_id','=','product.product_id')
        ->Where('status',1)
        ->get();

        //Azhar code here 17-5-2018
        $get_mattress = DB::table('admin_matt_selector')
        ->join('admin_matt_selector_product','admin_matt_selector_product.admin_matt_id','=','admin_matt_selector.admin_matt_id')
        ->where('admin_matt_selector.status',1)
        ->where('admin_matt_selector.admin_matt_id',$id)
        ->get();

        foreach ($get_mattress as $key => $value) {

          $value->product_recommandation = DB::table('product')
          ->select('product_description.name','product.product_id')
          ->join('product_description','product_description.product_id','=','product.product_id')
          ->where('product.product_id',$value->recommandation_product)
          ->get();

          $value->product_related_product = DB::table('product')
          ->select('product_description.name','product.product_id')
          ->join('product_description','product_description.product_id','=','product.product_id')
          ->where('product.product_id',$value->related_product)
          ->get();
        }

        //dd($get_mattress);

        //Azhar code End here 17-5-2018

        $lang = DB::table('language')->Where('status',1)->get();

        // return view('mattress.editMattress')->with('mattress',$matt_selector)->with('matt_selector_product',$matt_selector_product)->with('product',$product)->with('language',$lang);

        return view('lmgAdmin.mattress.editMattress')->with('mattress',$matt_selector)->with('matt_selector_product',$get_mattress)->with('product',$product)->with('language',$lang);
      }    
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }

    public function edit_mattress(Request $data, $value = null)
    {
      // dd($data->all());
      try
      {
        /*$mattress['lang_code'] = $data['lang_code'];
        $mattress['buying_mattress'] = $data['buying'];
        $mattress['mattress_new'] = $data['new_mattress'];
        $mattress['weight_range'] = $data['weight_range'];
        $mattress['partner_weight_range'] = $data['partner_weight_range'];
        $mattress['age_range'] = $data['age_range'];
        $mattress['partner_age_range'] = $data['partner_age_range'];
        $mattress['your_sleep_position'] = $data['your_sleep_position'];
        $mattress['partner_sleep_position'] = $data['partner_sleep_position'];
        $mattress['issues'] = $data['issues'];
        $mattress['sleep_too'] = $data['sleep_too'];
        $mattress['comfortable_sleep'] = $data['comfortable_sleep'];
        $mattress['status'] = $data['Status'];
        $mattress['updated_at'] = date("Y-m-d H:i:s");
        $mattress['updated_by'] = $data->ip();*/

        $mattress['lang_code'] = $data['lang_code'];
        
        if ( $data['buying'] == 'Myself') {

          $mattress['buying_mattress'] = $data['buying'];
          $mattress['mattress_new'] = $data['new_mattress'];
          $mattress['weight_range'] = $data['weight_range'];
          $mattress['age_range'] = $data['age_range'];
          $mattress['your_sleep_position'] = $data['your_sleep_position'];
          $mattress['issues'] = $data['issues'];
          $mattress['sleep_too'] = $data['sleep_too'];          
          $mattress['comfortable_sleep'] = $data['comfortable_sleep'];

          $mattress['status'] = $data['Status'];
          $mattress['created_at'] = date("Y-m-d H:i:s");
          $mattress['created_by'] = $data->ip();
          
        }
       
        if ($data['buying'] == 'My partner & I' || $data['buying'] == 'My partner, I & My kids') {
          
          if ($data['buying'] == 'My partner & I'){
            $mattress['buying_mattress'] = $data['buying'];
          }

          if ($data['buying'] == 'My partner, I & My kids'){
            $mattress['buying_mattress'] = $data['buying'];
          }

          $mattress['mattress_new'] = $data['new_mattress'];
          $mattress['weight_range'] = $data['weight_range'];
          $mattress['partner_weight_range'] = $data['Partner_weight_range'];
          $mattress['age_range'] = $data['age_range'];
          $mattress['partner_age_range'] = $data['Partner_age_range'];
          $mattress['your_sleep_position'] = $data['your_sleep_position'];
          $mattress['partner_sleep_position'] = $data['partner_sleep_position'];
          $mattress['issues'] = $data['issues'];          
          $mattress['sleep_too'] = $data['sleep_too'];
          $mattress['comfortable_sleep'] = $data['comfortable_sleep'];
          
          $mattress['status'] = $data['Status'];
          $mattress['created_at'] = date("Y-m-d H:i:s");
          $mattress['created_by'] = $data->ip();
        }

        if ($data['buying'] == 'Just for the spare bedroom') {

          $mattress['buying_mattress'] = $data['buying'];
          $mattress['sleep_guest'] = $data['sleep_guest'];
          $mattress['comfortable_sleep'] = $data['comfortable_sleep'];

          $mattress['status'] = $data['Status'];
          $mattress['created_at'] = date("Y-m-d H:i:s");
          $mattress['created_by'] = $data->ip();
        }

        DB::table('admin_matt_selector')->where('admin_matt_id',$data['mattress_id'])->update($mattress);

        $a = count($data['recommandation_product']);
        $b = count($data['related_product']);

        if($a > $b){ $length = $a; }
        else { $length = $b; }

        DB::table('admin_matt_selector_product')
        ->Where('admin_matt_id',$data['mattress_id'])->delete();

        for ($i=0; $i < $length; $i++) {

          $matt_selector_product['admin_matt_id'] = $data['mattress_id'];
          $matt_selector_product['recommandation_product'] = @$data['recommandation_product'][$i];
          $matt_selector_product['related_product'] = @$data['related_product'][$i];

          DB::table('admin_matt_selector_product')->insert($matt_selector_product);
        }
        return Redirect('/Admin/mattress-select')->with('message','Mattress Updated successfully...');
      }
      catch(\Illuminate\Database\QueryException $exception)
      {
        $a = $exception->errorInfo;
        return view('errors.custom')->with('message',end($a));
      }
    }
}
?>