<?php 
/*
|---------------------------------------------------------------------------
| ModuleOne Module Routes
|---------------------------------------------------------------------------
|
| All the routes related to the ModuleOne module have to go in here. Make sure
| to change the namespace in case you decide to change the 
| namespace/structure of controllers.
|
*/

Route::group(['module' => 'Product', 'namespace' => 'App\Modules\Product\Controllers'], function () {

Route::any('/Admin/product', ['uses' => 'ProductAdminController@view']);
Route::any('/Admin/addProduct', ['uses' => 'ProductAdminController@addProduct']);
Route::any('/Admin/add_Product', ['uses' => 'ProductAdminController@add_Product']);
Route::any('/Admin/products/uploadImage_ajax', ['uses' => 'ProductAdminController@uploadImage_ajax']);
Route::any('/Admin/deleteProduct/{id}', ['uses' => 'ProductAdminController@deleteProduct']);

Route::any('/Admin/deleteprodFeatimg', ['uses' => 'ProductAdminController@deleteprodFeatimg']);
Route::any('/Admin/deleteprodAttrimg', ['uses' => 'ProductAdminController@deleteprodAttrimg']);
Route::any('/Admin/deleteprodsubimg', ['uses' => 'ProductAdminController@deleteprodSubimg']);

Route::any('/Admin/{any}/editProduct/{id}', ['uses' => 'ProductAdminController@editProduct']);
Route::any('/Admin/edit_Product', ['uses' => 'ProductAdminController@edit_Product']);

Route::get('/Admin/product-type', 'ProductAdminController@productTypes');
Route::get('/Admin/add/product-type', 'ProductAdminController@addProductType');
Route::any('/Admin/insert/product-type', 'ProductAdminController@insertProductType');

Route::any('/Admin/{any}/edit/{id}/product-type', 'ProductAdminController@editProductType');
Route::any('/Admin/update/product-type', 'ProductAdminController@updateProductType');
Route::any('/Admin/delete/{id}/product-type', 'ProductAdminController@deleteProductType');

// Route::any('/Admin/check-type-slug', 'ProductAdminController@checkProductType');

Route::get('/Admin/getProductByLang', 'ProductAdminController@getProductByLang');

Route::get('/Admin/get-product-info', 'ProductAdminController@getProductInfo');
// Route::get('/Admin/test', 'ProductAdminController@test');

 //weight
 Route::any('/Admin/weight', ['uses' => 'ProductAdminController@viewWeight']);
 Route::any('/Admin/Addweight', ['uses' => 'ProductAdminController@addweight']);
 Route::any('/Admin/add_weight', ['uses' => 'ProductAdminController@add_weight']);
 Route::any('/Admin/deleteweight/{id}', ['uses' => 'ProductAdminController@deleteweight']);
 Route::any('/Admin/editweight/{id}', ['uses' => 'ProductAdminController@editweight']);
 Route::any('/Admin/edit_weight', ['uses' => 'ProductAdminController@edit_weight']);

 //length
 Route::any('/Admin/length', ['uses' => 'ProductAdminController@viewlength']);
 Route::any('/Admin/Addlength', ['uses' => 'ProductAdminController@addlength']);
 Route::any('/Admin/add_length', ['uses' => 'ProductAdminController@add_length']);
 Route::any('/Admin/deletlength/{id}', ['uses' => 'ProductAdminController@deletelength']);
 Route::any('/Admin/editlength/{id}', ['uses' => 'ProductAdminController@editlength']);
 Route::any('/Admin/edit_length', ['uses' => 'ProductAdminController@edit_length']);

//Review 04-05-2018

 Route::any('/Admin/review', ['uses' => 'ProductAdminController@viewreview']);
 Route::any('/Admin/Addreview', ['uses' => 'ProductAdminController@addreview']);
 Route::any('/Admin/add_review', ['uses' => 'ProductAdminController@add_review']);
 Route::any('/Admin/deletereview/{id}', ['uses' => 'ProductAdminController@deletereview']);
 Route::any('/Admin/editreview/{id}', ['uses' => 'ProductAdminController@editreview']);
 Route::any('/Admin/edit_review', ['uses' => 'ProductAdminController@edit_review']);

//Selva Created
 //Product Ask a Question & Answers
 Route::any('/Admin/question', ['uses' => 'ProductAdminController@viewQuestion']);
 Route::any('/Admin/AddQuestion', ['uses' => 'ProductAdminController@addQuestion']);
 Route::any('/Admin/add_question', ['uses' => 'ProductAdminController@add_question']);
 Route::any('/Admin/deletequestion/{id}', ['uses' => 'ProductAdminController@deleteQuestion']);
 Route::any('/Admin/editquestion/{id}', ['uses' => 'ProductAdminController@editQuestion']);
 Route::any('/Admin/edit_question', ['uses' => 'ProductAdminController@edit_question']);

//attribute
 Route::any('/Admin/attribute', ['uses' => 'ProductAdminController@viewattribute']);
 Route::any('/Admin/Addattribute', ['uses' => 'ProductAdminController@addattribute']);
 Route::any('/Admin/add_attribute', ['uses' => 'ProductAdminController@add_attribute']);
 Route::any('/Admin/deleteattribute/{id}', ['uses' => 'ProductAdminController@deleteattribute']);
 Route::any('/Admin/editattribute/{id}', ['uses' => 'ProductAdminController@editattribute']);
 Route::any('/Admin/edit_attribute', ['uses' => 'ProductAdminController@edit_attribute']);

 //attribute Group
 Route::any('/Admin/attribute_group', ['uses' => 'ProductAdminController@viewattribute_group']);
 Route::any('/Admin/AddattributeGroup', ['uses' => 'ProductAdminController@AddattributeGroup']);
 Route::any('/Admin/add_attribute_group', ['uses' => 'ProductAdminController@add_attribute_group']);
 Route::any('/Admin/deleteattribute_group/{id}', ['uses' => 'ProductAdminController@deleteattribute_group']);

 Route::any('/Admin/editattribute_group/{id}', ['uses' => 'ProductAdminController@editattribute_group']);

 Route::any('/Admin/edit_attribute_group', ['uses' => 'ProductAdminController@edit_attribute_group']);

//option
Route::any('/Admin/option', ['uses' => 'ProductAdminController@viewOption']);

Route::any('/Admin/checksku', ['uses' => 'ProductAdminController@checksku']);
Route::any('/Admin/checkslug', ['uses' => 'ProductAdminController@checkslug']);
Route::any('/Admin/addproductdata/{id}', ['uses' => 'ProductAdminController@addproductdata']);
Route::any('/Admin/add_ProductData', ['uses' => 'ProductAdminController@add_ProductData']);
Route::any('/Admin/product_attribute/{id}', ['uses' => 'ProductAdminController@addproduct_attribute']);
Route::any('/Admin/add_ProductAttribute', ['uses' => 'ProductAdminController@add_ProductAttribute']);
Route::any('/Admin/product_image/{id}', ['uses' => 'ProductAdminController@addproduct_image']);
Route::any('/Admin/add_Productimages', ['uses' => 'ProductAdminController@add_Productimages']);

Route::any('/Admin/product_review/{id}', ['uses' => 'ProductAdminController@product_review']);
Route::any('/Admin/add_ProductReview', ['uses' => 'ProductAdminController@add_ProductReview']);

Route::any('/Admin/product_features/{id}', ['uses' => 'ProductAdminController@addproduct_features']);
Route::any('/Admin/add_ProductFeature', ['uses' => 'ProductAdminController@add_ProductFeature']);
Route::any('/Admin/product_option/{id}', ['uses' => 'ProductAdminController@addproduct_option']);

//cke editor upload and browser
Route::any('/Admin/product_option/{id}', ['uses' => 'ProductAdminController@addproduct_option']);

// //Azhar categories 08-05-2018
// Route::any('/Admin/categories', ['uses' => 'ProductAdminController@viewcategories']);

//Mattress-selected Admin page Azhar created 12-05-2018
//modified by bharathi 17-5-18

Route::any('/Admin/mattress-select', ['uses' => 'ProductAdminController@MattressSelect']);
Route::any('/Admin/add/mattress', ['uses' => 'ProductAdminController@AddMattress']);
Route::any('/Admin/insert/mattress', ['uses' => 'ProductAdminController@add_mattress']);
Route::any('/Admin/deleteMattress_selector/{id}', ['uses' => 'ProductAdminController@deleteMattress_selector']);
Route::any('/Admin/editMattress_selector/{id}', ['uses' => 'ProductAdminController@editMattress_selector']);
Route::any('/Admin/edit_mattress', ['uses' => 'ProductAdminController@edit_mattress']);



});

?>