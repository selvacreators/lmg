<?php namespace App\Modules\Admin\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Session\Store;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Registrar;

use App\Modules\Admin\Models\TestModel;


class AdminController extends Controller
{
	public function __construct(Store $session)
	{
		 $this->session = $session;
	}
	public function help()
	{

		return view('admin.help');

	}


	public function index()
	{
		return 'hi';
	}

	public function login()
	{
		return view('admin.login');
	}

	public function register()
	{
		return view('admin.register');
	}

	public function subgroup_name(Request $data)
	{
		$subgroup = \DB::table('master_subgroup')->Where('group_id',$data['group_id'])->Where('status',1)->lists('subgroup_name');

		return $subgroup;
	}

	public function activities(Request $data)
	{
		$activities = \DB::table('list_activities')->Where('model_id',$data['model_id'])->Where('status',1)->lists('activities_description','activities_id');

		return $activities;
	}
	
	public function linkbookactivities(Request $data)
	{
		$linkactivities = \DB::table('book_list_activities')->Where('market_id',$data['book_id'])->Where('status',1)->lists('book_name','book_id');

		return $linkactivities;
		//dd($linkactivities);
	}
  public function linkmarketactivities(Request $data)
	{
		$linkactivities = \DB::table('warrenty_period')->Where('market_id',$data['book_id'])->Where('status',1)->lists('model_name','model_id');

		return $linkactivities;
		dd($linkactivities);
	}
	public function bookactivities(Request $data)
	{
		$activities = \DB::table('book_list_activities')->Where('book_id',$data['book_id'])->Where('status',1)->lists('activities_description','booklist_id');

		return $activities;
		//dd($activities);
	}

	public function parts_activities(Request $data)
	{
		$activities = \DB::table('list_activities')
                                        ->where(function($query) {
                                              $query->orwhere('type','=','Various')
                                              ->orwhere('type','=','Replacement');
                                               }) 
                                              ->Where('model_id',$data['model_id'])->Where('status',1)->lists('activities_description','activities_id');

		return $activities;
	}

	public function parts_subactivities(Request $data)
	{
		$activities = \DB::table('sub_list_activities') 
                                              ->Where('activities_id',$data['activities_id'])
                                              ->Where('type','Replacement')
                                              ->Where('status',1)
                                              ->lists('subactivities_description','subactivities_id');

		return $activities;
	}

	public function labour_subactivities(Request $data)
	{
		$activities = \DB::table('sub_list_activities') 
                                              ->Where('activities_id',$data['activities_id'])
                                              ->Where('status',1)
                                              ->lists('subactivities_description','subactivities_id');

		return $activities;
	}
	
	public function bookparts_activities(Request $data)
	{
		$activities = \DB::table('book_list_activities')
                                        //->where(function($query) {
                                              //$query->orwhere('type','=','Various')
                                              //->orwhere('type','=','Replacement');
                                              // }) 
                                              ->Where('book_id',$data['book_id'])->Where('status',1)->lists('activities_description','booklist_id');

		return $activities;
	}
	public function bookparts_subactivities(Request $data)
	{
		$activities = \DB::table('book_sublist_activities') 
                                              ->Where('booklist_id',$data['booklist_id'])
                                              //->Where('type','Replacement')
                                              ->Where('status',1)
                                              ->lists('subactivities_description','subbooklist_id');

                                             

		return $activities;
	}
	/*public function parts_booksubactivities(Request $data)
	{
		$activities = \DB::table('book_sublist_activities') 
                                              ->Where('booklist_id',$data['booklist_id'])
                                              //->Where('type','Replacement')
                                              ->Where('status',1)
                                              ->lists('subactivities_description','subbooklist_id');

		return $activities;
	}*/

	public function labour_booksubactivities(Request $data)
	{
		$activities = \DB::table('book_sublist_activities') 
                                              ->Where('booklist_id',$data['booklist_id'])
                                              ->Where('status',1)
                                              ->lists('subactivities_description','subbooklist_id');

		return $activities;
	}



	public function model_package(Request $data)
	{
		$model_package = \DB::table('list_package')->Where('market',$data['market'])->Where('model_name',$data['model_name'])->Where('package_status',1)->Where('status',1)->lists('package_name','package_id');

		$model_name = $data['model_name'];

		$model_activities = \DB::table('list_part')
								->join('list_activities',function($join) use($model_name)
								{
									$join->on('list_activities.activities_description','=','list_part.activities')
										->where('list_activities.model_name','=',$model_name);
								})
								->Where('list_part.model_name',$data['model_name'])
								->Where('list_part.status',1)
								->get(["list_part.*","list_activities.interval_kms"]);

		return array($model_package,$model_activities);
	}



//Azhar created Fuso to scc api key 26-10-2017
	public function fusologin(Request $data){
		try{
			//dd($data->all());
		///dd($data['fuid']);
		$gd_id ="";
			$checkFuso = \DB::table('users')->Where('fuso_id',$data['fuid'])->get();
			//dd($checkFuso);
			if(!empty($checkFuso)){


				if(!empty($data['gd_fuid'])){

					$id = \DB::table('users')->Where('fuso_id',$data['gd_fuid'])->pluck('id');
					if(!empty($id))
					{

						$gd_id = $id[0];
					}
					else{
						$gd_id ="";
					}


				}
				//Update
				$update = \DB::table('users')->Where('fuso_id',$data['fuid'])->Where('user_typename',$data['role'])->Where('email',$data['email'])
				->update(["email" => $data['email'],"name" => $data['uname'],"password"=> bcrypt($data['pwd']),"mobile" => $data['mobile'],"country_name" => $data['country'],"gd_id" => $gd_id]);

				return Redirect("/auth/login?email=".$data['email']."&password=".$data['pwd']);

			}
			else{
				$ins ="";

				//Insert
				if($data['role'] == "GD"){
					$ins['user_type'] = '4';
				}
				else if($data['role'] == "Dealer"){
					$ins['user_type'] = '7';
				}
				else if($data['role'] == "MPC"){
					$ins['user_type'] = '6';
				}
				else if($data['role'] == "DICV"){
					$ins['user_type'] = '2';
				}
				else if($data['role'] == "MFTBC"){
					$ins['user_type'] = '3';
				}

				if(!empty($data['gd_fuid'])){

					$id = \DB::table('users')->Where('fuso_id',$data['gd_fuid'])->pluck('id');
					if(!empty($id))
					{

					//$ins['gd_id'] = $data['gd_fuid'];
						$ins['gd_id'] = $id[0];
					}


				}
				

				$ins['fuso_id'] = $data['fuid'];
				$ins['name'] = $data['uname'];
				$ins['user_typename'] = $data['role'];
				$ins['email'] = $data['email'];
				$ins['mobile'] = $data['mobile'];
				$ins['country_name'] = $data['country'];
				$ins['market'] = $data['market'];
				$ins['password'] = bcrypt($data['pwd']);



				$insert  = \DB::table('users')->insert($ins);


				return Redirect("/auth/login?email=".$data['email']."&password=".$data['pwd']);

			}

		}
		catch(\Illuminate\Database\QueryException $exception)
        {
            $a = $exception->errorInfo;
            return view('errors.custom')->with('message',end($a));
        }
	}
}
?>