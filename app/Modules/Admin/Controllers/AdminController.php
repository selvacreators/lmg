<?php
namespace App\Modules\Admin\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests;
//use App\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Session\Store;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Auth\Registrar;
use App\Modules\Admin\Models\TestModel;


class AdminController extends Controller
{
	public function __construct(Store $session)
	{
		$this->session = $session;
	}
	public function login()
	{
		return view('admin.login');
	}
}
?>