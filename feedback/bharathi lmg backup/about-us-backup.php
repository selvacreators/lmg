<section class="about-content-left p80">
<div class="container">
<div class="row">
<div class="col-md-6 view-animate fadeInLeftSm delay-04">
<div class="about-content">
<div class="about-info details">
<h3>THE FACT</h3>

<p>We spend 1/3 of our whole life or 200,000 hours in bed. In today&#39;s busy society, time in bed (for sleeping, internet surfing, social media, TV, reading,...) occupies more than time spent at work, with family, with friends and other activities.</p>
</div>
<span class="watermark-text">FACT</span></div>
</div>

<div class="col-md-6 view-animate fadeInRightSm delay-04">
<div class="image"><img alt="The Fact" class="img-responsive" src="/frontend/images/about/image3.jpg" /></div>
</div>
</div>
</div>
</section>

<section class="light-bg-grey about-content-right p80">
<div class="container">
<div class="row">
<div class="col-md-6 view-animate fadeInLeftSm delay-04">
<div class="image"><img alt="Americanstar" class="img-responsive" src="/frontend/images/about/image1.jpg" /></div>
</div>

<div class="col-md-6 view-animate fadeInRightSm delay-04">
<div class="about-content">
<div class="about-info details">
<h3>THE STORY</h3>

<p>Described as a gentle god with wings on forehead and is able to get anybody fall into sleep within minutes, Hypnos God of Greek Legends is World Luxury Mattress Gallery (LMG)&#39;s symbol, reflective of LMG&#39;s STORY.</p>
</div>
<span class="watermark-text">STORY</span></div>
</div>
</div>
</div>
</section>

<section class="about-content-left p80">
<div class="container">
<div class="row">
<div class="col-md-6 view-animate fadeInLeftSm delay-04">
<div class="about-content">
<div class="about-info details">
<h3>COMMITMENT</h3>

<p>In partnership with world-class brands, LMG&#39;s commitment is to perfection, continuously investing in modern technology, innovative products and recruiting international professionals. Our artistically inspired, luxurious quality mattresses and accessories create an unsurpassed sleep experience, allowing our customers to achieve the fulfilling lifestyle each deserves.</p>
</div>
<span class="watermark-text">COMMITMENT</span></div>
</div>

<div class="col-md-6 view-animate fadeInRightSm delay-04">
<div class="image"><img alt="COMMITMENT" class="img-responsive pull-right" src="/frontend/images/about/image4.jpg" /></div>
</div>
</div>
</div>
</section>

<section class="light-bg-grey about-content-right p80">
<div class="container">
<div class="row">
<div class="col-md-6 fadeInLeftSm delay-04">
<div class="image"><img alt="Americanstar" class="img-responsive" src="/images/catalog/4/image5.jpg" /></div>
</div>

<div class="col-md-6 view-animate fadeInRightSm delay-04">
<div class="about-content">
<div class="about-info details">
<h3>THE ADVANTAGES</h3>

<p>LMG is an exclusive distributor in Southeast Asia of the top world-class bedding brands: Therapedic, AmericanStar, Latexco, Kluft, Aireloom, Brentwood Home.</p>

<p>LMG is the first in Vietnam to own &quot;Reveal by Xsensor&quot; a machine measures body pressure to advise suitable mattress &amp; pillow best fit to each consumer. Guarantee customer satisfaction.</p>

<p>The first LMG store was launched in Sala City, Ho Chi Minh. For the first time in Vietnam, the strategic location of Sala City offers customers a luxurious shopping experience dedicated for the household, sleep, furniture and decoration in a convenient location.</p>
</div>
<span class="watermark-text">ADVANTAGES</span></div>
</div>
</div>
</div>
</section>

<section class="about-content-left p80">
<div class="container">
<div class="row">
<div class="col-md-6 view-animate fadeInLeftSm delay-04">
<div class="about-content">
<div class="about-info details">
<h3>THE COLLECTIONS</h3>

<p>With selective materials &amp; innovative technology, all of LMG&rsquo;s collections are made by craftsmen with passion. Our products are perfect in each piece of coil, layers of latex, layer of foam as well as fabrice&rsquo;s pattern materials.</p>
<p>Each of LMG&rsquo;s collections is an episode of an artistic story reflecting dreams, ambitions, passions. For perfect sleep, LMG brings to you a fulfilling lifestyle of luxury, you deserve.</p>
</div>
<span class="watermark-text">COLLECTIONS</span></div>
</div>

<div class="col-md-6 view-animate fadeInRightSm delay-04">
<div class="image"><img alt="The Collections" class="img-responsive" src="/frontend/images/about/image2.jpg" /></div>
</div>
</div>
</div>
</section>