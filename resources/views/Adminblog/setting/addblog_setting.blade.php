@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/admin/insert/blog-setting','class'=>'form-horizontal blog-setting-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/admin/blog/setting" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Add Blog Setting</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" id="lang_code" class="form-control">
                                            @foreach($language as $lang)
                                            <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="blog_title" class="control-label col-sm-3">Blog Title:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="blog_title" id="blog_title" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_slider_show" class="control-label col-sm-3">Number of show artical slider:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="no_slider_show" id="no_slider_show" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="blog_title" class="control-label col-sm-3">Blog Post Per Page:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="post_perpage" id="post_perpage" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="blog_title" class="control-label col-sm-3">Set Featured Article Slider:</label>
                                    <div class="col-sm-9">
                                        <select name="featureBlogPost[]" id="featureBlogPost" class="form-control" multiple>
                                            <option value="">Select</option>
                                            @if($blogPost)
                                            @foreach($blogPost as $row)
                                            @if($row->featured_article == 1)
                                            <option value="{{$row->post_id}}">{{$row->post_title}}</option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="meta_title" class="control-label col-sm-3">Meta Title:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="meta_title" id="meta_title" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="meta_keywords" class="control-label col-sm-3">Meta Keywords:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="meta_keywords" id="meta_keywords" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="blog_banner" class="control-label col-sm-3">Blog Image:<span class="note">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="file" name="blog_banner" id="blog_banner" class="form-control" accept="image/*" data-type="image" onchange="readURL(this);">
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="StrImg" width="100" height="100">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="blog_desc" class="control-label col-sm-3">Blog Description:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea name="blog_desc" id="blog_desc" rows="4" cols="10" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="meta_desc" class="control-label col-sm-3">Meta Description:</label>
                                    <div class="col-sm-9">
                                        <textarea name="meta_desc" id="meta_desc" rows="4" cols="10" class="form-control" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status<span class="note">*</span> :</label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Enable</option>
                                            <option value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".blog-setting-form").validate({
    rules:
    {
        lang_code: "required",
        blog_title: "required",
        meta_title : "required",
        blog_banner : "required",
        blog_desc : "required",
        status : "required"
    },
    messages:
    {
        lang_code: "Please choose language",
        blog_title: "Please enter Blog title",
        meta_title: "Please enter meta title",
        blog_banner: "Please choose Blog banner",
        blog_desc: "Please enter Blog description",
        status: "Please select status"
    }
  });
});
function readURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#StrImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection