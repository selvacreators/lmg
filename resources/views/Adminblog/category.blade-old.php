@extends('layouts.layout_admin')
@section('admin-style')
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
                @foreach($categories as $category)
                <div class="card-header">
                     <a href="{{ route('editCategory', $category->blog_categories_id) }}" class="btn btn-success btn-sm pull-right">Edit Category</a>
                    <!-- <a href="/admin/post/create" class="btn btn-success btn-sm pull-right">Add Post</a>
                                    <a href="/admin/post/{id}/edit" class="btn btn-success btn-sm pull-right">Edit Post</a> -->
                    <h3 class="card-title">Categories</h3>
                </div>
                <div class="card-body">                   
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Action</th>
                            {{--<th>Actions</th>--}}
                        </tr>
                        
                        <tr>
                            <td>{{ $category->blog_categories_id }}</td>
                            <td>{{ $category->category_name }}</td>
                            <td>{{ $category->category_slug }}</td>
                            <td>
                                <a href="{{ route('editCategory', $category->blog_categories_id) }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                               
                                <a href="{{ route('deleteCategory', $category->blog_categories_id) }}" onclick="return confirm('Are You Sure Delete Chat?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                            </td>
                        </tr>

                        
                    </table>
                    <input type="text" id="new_cat" name="new_cat" value="" /> <button id="create_category" class="btn btn-success">Create Category</button>

                    
                </div>
                @endforeach
            </div>
        </div>

</div>
</div>
</div>
@endsection

@section('admin-script')
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
    $(document).ready(function() {
        
        // categories
        $('#create_category').click(function(e) {
            e.preventDefault();
            $(this).addClass('disabled');
            var btn = $(this);
            $.post('/admin/blog/create_category', {
                _token: "{{ csrf_token() }}",
                category_name: $('#new_cat').val()
            }, function(data) {
                $(btn).removeClass('disabled');
                if (data.status == 'success') {
                    toastr.success('Category created.');
                    console.log(data.object);
                    // TODO add table row
                } else {
                    toastr.error(data.error);
                }
            }, 'json');
        });
        
    });
    </script>
@endsection
