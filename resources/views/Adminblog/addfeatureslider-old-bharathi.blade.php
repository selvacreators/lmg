@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/addslides','class'=>'form-horizontal fslider-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/admin/blog/feature-slider" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Create Feature Slider</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="language" class="form-control" id="language">
                                            @foreach($language as $lng)
                                            <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="title" class="control-label col-sm-3">Feature Title:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="title" name="title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="feat_des" class="control-label col-sm-3">Feature Content:</label>
                                    <div class="col-sm-9">
                                        <textarea rows="5" name="feat_des" id="feat_des" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Enable</option>
                                            <option value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group ecatalogue-table fslider-table">
                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Upload</th>
                                                    <th>Sort Order</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                        <div class="clearfix"></div>
                                        <div class="multi" id="multi_ecatalogue_btn">
                                            <div class="form-group row">
                                                <input type="button" id="addECatalogueImg" value="Add New Image" class="btn btn-info btn-daimler">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
var options = { 
    filebrowserImageBrowseUrl: route_prefix + '?type=Images',
    filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: route_prefix + '?type=Files',
    filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
};
</script>
<script type="text/javascript">
CKEDITOR.replace('feat_des', options);
CKEDITOR.config.allowedContent = true;
</script>
<script type="text/javascript">
$(document).ready(function(){
    $(".fslider-form").validate({
        rules: {
            language: "required",
            title : "required",
            status : "required"
        },
        messages: {
            language: "Please select language",
            title: "Please enter title",
            status: "Please select status"
        }
    });
    $("#txtslimage").change(function() {
        if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif)$/) ) {
            if(this.files[0].size>2000000) {
              alert('File size is larger than 2MB!');
              $('#store_logo').val();
            }
        } else alert('This is not an image file!');
    });
    $("#addECatalogueImg").click(function() {
        var newRow = $("<tr>");
        var cols = "";
        cols += '<td><input type="file" name="ecatalogue_image[]" id="ecatalogue_image" class="form-control" ></td><td><input type="text" name="eci_sort_order[]" id="eci_sort_order" class="form-control" autocomplete="off"/></td><td><button type="button" class="btn btn-danger feaDel"><i class="fa fa-trash"></i></button></td></tr>';
        newRow.append(cols);
        $("tbody").append(newRow);
    });
    $(".fslider-table").on("click", ".feaDel", function (event) {
        $(this).closest("tr").remove();
    });
});
</script>
@endsection