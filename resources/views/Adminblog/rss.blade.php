@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
                <div class="card-header">
                    <!-- <a href="/Admin/post/create" class="btn btn-success btn-sm pull-right">Add Post</a>
                                    <a href="/Admin/post/{id}/edit" class="btn btn-success btn-sm pull-right">Edit Post</a> -->
                   <h3>RSS Options</h3>
                </div>
                <div class="card-body"> 

                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="option_rss_name">Site name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="option_rss_name" value="{{ $option_rss_name }}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="option_rss_number">Number of posts</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="option_rss_number" value="{{ $option_rss_number }}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="option_rss_fullposts">Full post or Excerpt ?</label>
                            <div class="col-sm-10">
                                <select type="text" class="form-control" id="option_rss_fullposts">
                                    <option value="excerpt">Excerpts only</option>
                                    <option value="full">Full posts</option>
                                </select>
                            </div>
                        </div>

                        <button id="btn_save_options" type="submit" class="col-md-offset-2 btn btn-primary">Save options</button>
                    </form>
                </div>
            </div>
        </div>

</div>
</div>
</div>

    <script>
    $(document).ready(function() {
        $('#btn_save_options').click(function(e) {
            e.preventDefault();
            $.post('/admin/blog/save_options', {
                _token: "{{ csrf_token() }}",
                rss_name: $('#option_rss_name').val(),
                rss_number: $('#option_rss_number').val(),
                rss_fullposts: $('#option_rss_fullposts').val()
            }, function(data) {
                console.log('options saved');
            });
        });
        
    });
    </script>

@stop