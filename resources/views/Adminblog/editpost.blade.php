@extends('layouts.layout_admin')
@section('admin-style')
<link href="{{ Config::get('app.head_url') }}assets/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
@endsection
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/admin/blog/update/'.$blogPosts->post_id.'/post','class'=>'form-horizontal category-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                          <div class="card">
                            <div class="card-header">
                                <a href="/admin/blog" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Post</h3>
                            </div>
                            <div class="card-body">
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="lang_code" id="lang_code" class="form-control">
                                    @foreach($language as $lang)
                                    @if($blogPosts->lang_code == $lang->lang_code)
                                    <option value="{{$lang->lang_code}}" selected="selected">{{$lang->lang_name}}</option>
                                    @else
                                    <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                    @endif
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="post_title">Post Title:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="post_title" id="post_title" value="{{$blogPosts->post_title}}" class="form-control">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="post_slug">Post Slug:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="post_slug" id="post_slug" class="form-control" value="{{$blogPosts->post_slug}}">
                                  <div id="perror"></div>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="tags">Tags:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="tags" id="tags" class="form-control" value="{{$blogPosts->post_tags}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="published_at">Publish Date:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="published_at" id="date_available" class="form-control datepicker" value="{{$blogPosts->publish_date}}" data-date-format="YYYY-MM-DD">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="category">Category:<span class="note">*</span></label>
          <?php
          $category_id = array();
          if(!empty($post_category)){
            foreach ($post_category as $key => $value) {
             $category_id[] = $value->categories_id;
            }
             
          }
          ?>                      
                                <div class="col-sm-9">
                          <select name="category[]" id="category" class="form-control" multiple>
                                     @if(!empty($blogCategories))
                                        @foreach($blogCategories as $key => $row)

                                      @if(in_array($row->blog_categories_id,$category_id))

                                            <option selected="selected" value="{{$row->blog_categories_id}}">{{$row->category_name}}</option>
                                       @else
                                            <option value="{{$row->blog_categories_id}}" >{{$row->category_name}}</option>
                                        @endif

                                        @endforeach
                                          @endif
                        </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="rposts">Related Posts:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <?php $postid[] = '';?>
                                  @if($rblogs)
                                    @foreach($rblogs as $key => $row)
                                      <?php $postid[] = $row->rposts_id;?>
                                    @endforeach
                                  @endif
                                  <select name="rposts[]" id="rposts" class="form-control" multiple>
                                      @if($blogs)
                                        @foreach($blogs as $key => $row)
                                          @if($blogPosts->post_id != $row->post_id)
                                            @if(in_array($row->post_id,$postid))
                                              <option value="{{$row->post_id}}" selected="selected">{{$row->post_title}}</option>
                                            @else
                                              <option value="{{$row->post_id}}">{{$row->post_title}}</option>
                                            @endif
                                          @endif
                                        @endforeach
                                      @endif
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="short_desc">Short Description:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <textarea name="short_desc" class="form-control" rows="3">{{$blogPosts->short_desc}}</textarea>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="featured_article" class="control-label col-sm-3">set featured article:</label>
                                <div class="col-sm-9">
                                  <select name="featured_article" id="featured_article" class="form-control">
                                    <option value="">Select</option>
                                    <option value="1" <?php if($blogPosts->featured_article == 1) { echo "selected";} ?>>Yes</option>
                                    <option value="2" <?php if($blogPosts->featured_article == 2) { echo "selected";} ?>>No</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="blog_banner" class="control-label col-sm-3">Banner Image or Video:<span class="note">*</span></label>
                                <div class="col-sm-3">
                                  <input type="file" name="blog_banner" id="blog_banner" class="form-control" accept="image/*" data-type="image" onchange="readSliderURL(this);">
                                  <input type="hidden" name="Insert_blog_banner" id="Insert_blog_banner" value="{{$blogPosts->blog_banner}}">
                                </div>
                                @if($blogPosts->blog_banner != '')
                                 <div class="col-sm-4">
                                    <img src="{{Config::get('app.head_url')}}images/blog/{{$blogPosts->blog_banner}}" id="StrImg" width="100" height="100">
                                </div>
                                @else
                                 <div class="col-sm-4">
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="StrImg" width="100" height="100">
                                </div>
                                @endif
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="content">Content:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <textarea name="content" id="Blog_content">{{$blogPosts->post_content}}</textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_title">Meta Title:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{$blogPosts->meta_title}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_keyword">Meta Keyword:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_keyword" id="meta_keyword" class="form-control" value="{{$blogPosts->meta_keyword}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_desc">Meta Description:</label>
                                <div class="col-sm-9">
                                  <textarea name="meta_desc" id="meta_desc" rows="3" cols="10" class="form-control">{{$blogPosts->meta_desc}}</textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="sort_order">Sort Order:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="sort_order" id="sort_order" class="form-control" value="{{$blogPosts->sort_order}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="status" id="status" class="form-control">
                                    <option value="">Select</option>
                                    <option value="1" <?php if($blogPosts->post_status == 1) { echo "selected";} ?>>Enable</option>
                                    <option value="2" <?php if($blogPosts->post_status == 2) { echo "selected";} ?>>Disable</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('date-script')
<script src="{{ Config::get('app.head_url') }}assets/js/moment.min.js"></script>
<script src="{{ Config::get('app.head_url') }}assets/js/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){

  md.initFormExtendedDatetimepickers();

  $(".category-form").validate({
    rules:
    {
      lang_code : "required",
      post_title : "required",
      published_at : "required",
      category: "required",
      short_desc : "required",
      content : "required",
      meta_title : "required",
      status : "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      post_title: "Please enter post title",
      published_at: "Please select published date",
      category: "Please choose category",
      short_desc: "Please enter short description",
      content: "Please enter content",
      meta_title: "Please enter meta title",
      status: "Please choose status"
    }
  });

  $("#blog_banner").change(function() {
        if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif|mp4)$/) ) {
            if(this.files[0].size>2000000) {
              alert('File size is larger than 2MB!');
              $('#store_logo').val();
            }
        } else alert('This is not an image file!');
    });
});

function readSliderURL(input)
{
  if (input.files && input.files[0])
  {
      var reader = new FileReader();
      reader.onload = function (e)
      {
          $('#StrImg')
          .attr('src', e.target.result)
          .width(100);
      };
      reader.readAsDataURL(input.files[0]);
  }
}
</script>


<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
    $('#Blog_content').ckeditor({      
      filebrowserImageBrowseUrl: route_prefix + '?type=Images',
      filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
      filebrowserBrowseUrl: route_prefix + '?type=Files',
      filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
  </script>
@endsection