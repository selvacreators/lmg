@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/admin/blog/update/category','class'=>'form-horizontal category-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="bcategoryid" value="{{$editBlogCategories[0]->blog_categories_id}}">
                          <div class="card">
                            <div class="card-header">
                                <a href="/admin/blog/category" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Blog Category</h3>
                            </div>
                            <div class="card-body">
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="lang_code" id="lang_code" class="form-control">
                                    @foreach($language as $lang)
                                    <option <?=(($lang->lang_code == $editBlogCategories[0]->lang_code?'selected':''));?> value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="category_name">Category Name:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="category_name" id="category_name" class="form-control" value="{{$editBlogCategories[0]->category_name}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="category_slug">Category Slug:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="category_slug" id="category_slug" class="form-control" value="{{$editBlogCategories[0]->category_slug}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="parentid">Parent:</label>
                                <div class="col-sm-9">
                                  <select name="parentid" id="parentid" class="form-control">
                                    <option value="">Select</option>
                                    @if($blogCategories)
                                      @foreach($blogCategories as $row)
                                        @if($row->blog_categories_id != $editBlogCategories[0]->blog_categories_id)
                                          <option <?=(($row->blog_categories_id == $editBlogCategories[0]->parent_id)?'selected':'');?> value="{{$row->blog_categories_id}}">{{$row->category_name}}</option>
                                        @endif
                                      @endforeach
                                    @endif
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_title">Meta Title:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{$editBlogCategories[0]->meta_title}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_keyword">Meta Keyword:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_keyword" id="meta_keyword" class="form-control" value="{{$editBlogCategories[0]->meta_keyword}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_desc">Meta Description:</label>
                                <div class="col-sm-9">
                                  <textarea name="meta_desc" id="meta_desc" rows="3" cols="10" class="form-control">{{$editBlogCategories[0]->meta_desc}}</textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="status" id="status" class="form-control">
                                    <option value="">Select</option>
                                    <option <?=(($editBlogCategories[0]->status == '1'?'selected':''));?> value="1">Enable</option>
                                    <option <?=(($editBlogCategories[0]->status == '2'?'selected':''));?> value="2">Disable</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
    $('#cate_desc').ckeditor({      
      filebrowserImageBrowseUrl: route_prefix + '?type=Images',
      filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
      filebrowserBrowseUrl: route_prefix + '?type=Files',
      filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
  </script>


<script type="text/javascript">
$(document).ready(function(){
  $(".category-form").validate({
    rules:
    {
      lang_code : "required",
      category_name: "required",
      category_slug : "required",
      meta_title : "required",
      status : "required"
    },
    messages:
    {
      lang_code: "Please enter name",
      category_name: "Please enter category name",
      category_slug: "Please enter category slug",
      meta_title: "Please enter meta title",
      status: "Please choose status"
    }
  });
});
</script>
@endsection