@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
                <div class="card-header">
                    
                    <a href="{{ action('\App\Modules\Blog\Controllers\BlogAdminController@createPost') }}" class="btn btn-success">Create post</a>
                    <a href="{{ action('\App\Modules\Blog\Controllers\BlogAdminController@editPost') }}" class="btn btn-success">Edit post</a>
                    <h3 class="card-title">View And Edit Blog Post</h3>
                </div>
                <div class="card-body">                    
                    <h2>Posts</h2>

                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>ID</th>
                            <th>Status</th>
                            <th>Published on</th>
                            <th>Title</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->post_id }}</td>
                            <td>{{ $post->post_title }}</td>
                            <td>{{ $post->post_content }}</td>
                            <td>{{ $post->post_slug }}</td>

                            <td>{{ $post->status }}</td>
                            <td>{{ $post->published_at }}</td>
                            <td>{{ $post->title }}</td>
                            <td>
                                <a href="{{ action('\App\Modules\Blog\Controllers\BlogAdminController@editPost', $post->id) }}" class="btn btn-primary">Edit</a>
                                @if (!$post->is_published() )
                                <button data-id="{{$post->id}}" class="btn btn-publish btn-success">Publish</button>
                                @endif
                            </td>
                        </tr>

                        @endforeach
                    </table>
                    

                    
                </div>
            </div>
        </div>

</div>
</div>
</div>

@endsection
@section('footer-scripts')
    <script>
    $(document).ready(function() {
        // publishing
        $('.btn-publish').click(function(e) {
            e.preventDefault();
            $(this).addClass('disabled');
            var post_id = $(this).data('id');
            var btn = $(this);
            $.post('/admin/blog/publish_post', {
                _token: "{{ csrf_token() }}",
                post_id: post_id
            }, function(data) {
                $(btn).removeClass('disabled');
            }, 'json');
        });
    });
    </script>

@stop