@extends('layouts.layout_home')
@section('content')
@include('includes.header')
@include('includes.breadcrumb')

<main class="content-wrapper product-detail-page">
    <div class="page-content">
	<div class="container-fluid">
        @if(Session::has('successReview'))
        <h4 align="center">{{ Session::get('successReview') }}</h4>
        @endif
        @if(Session::has('successQuestion'))
        <h4 align="center">{{ Session::get('successQuestion') }}</h4>
        @endif
        <?php if(1==1):
            $totalRating = sizeof($reviews);
            function ratingPercent($rating,$totalRatings)
            {
                try
                {
                    $percent = ($rating/$totalRatings)*100;
                }
                catch(Exception $ex)
                {
                    $percent = 0;
                }
                return $percent;
            }
        ?>
		<div class="row product pt60 pb40">
			<div class="col-md-7 col-lg-6 col-xs-12">
				<div class="product-image">
                    <div class="image">
     
                        <a href="{{ URL(config::get('app.head_url')) }}/product/images/{{$product['product']->prod_slider_image}}" title="Aston" class="cloud-zoom" style="cursor:move" rel="position:'inside', showTitle: false" id='zoom1'>

                             <img itemprop="image" src="{{ URL(config::get('app.head_url')) }}/product/images/{{$product['product']->prod_slider_image}}" title="Aston" alt="Aston" />
                        </a>
                        <!-- zoom link-->
                        <a href="{{ URL(config::get('app.head_url')) }}/product/images/{{$product['product']->prod_slider_image}}" title="Aston" id="zoom-btn" class="colorbox">
                            <i class="ion-arrow-resize"></i>
                        </a>            
                    
                    </div>

                    <div class="image-additional">      
                        <ul class="image_carousel">
                            <!-- Additional images -->
                             @if(!empty($product['product_images_gallery']))
                             @foreach($product['product_images_gallery'] as $key => $val)
                            
                            <li>
                                <a href="{{ URL(config::get('app.head_url')) }}/product/large/{{$val->image}}" title="Aston" class="cloud-zoom-gallery colorbox" rel="useZoom: 'zoom1', smallImage: '{{ URL(config::get('app.head_url')) }}/product/large/{{$val->image}}'">
                                    <img src="{{ URL(config::get('app.head_url')) }}/product/thumb/{{$val->image}}" title="Aston" width="83" height="83" alt="Aston" />
                                </a>
                            </li>
                            @endforeach
                             @endif
                            <!-- Show even the main image among the additional if  -->
                            <li>
                                <a href="{{ URL(config::get('app.head_url')) }}/product/images/{{$product['product']->prod_slider_image}}" title="Aston" class="cloud-zoom-gallery colorbox" rel="useZoom: 'zoom1', smallImage: '{{ URL(config::get('app.head_url')) }}/product/images/{{$product['product']->prod_slider_image}}'">
                                    <img src="{{ URL(config::get('app.head_url')) }}/product/images/{{$product['product']->prod_slider_image}}" title="Aston" width="83" height="83" alt="Aston" />
                                </a>
                            </li>
                            
                        </ul>
        
                    </div>

                </div>

			</div>
			<div class="col-md-5 col-lg-6 col-xs-12">
				<div class="product-info">
                    @if(!empty($product['product_collections']))
					<div class="col-details">
                        <div class="col-name h1">{{$product['product_collections']->collection_name}}</div>
						<div class="sub-heading slogan">{{$product['product_collections']->col_caption}}</div>
					</div>
                    @endif
                        <div class="product-details">
        					<h1 class="product-name h3">{{ $product['product']->name }}</h1>
        					<div class="review rating-summary">
                               <span class="rating_stars rating r{{ $totalRating }} rating-result">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                               </span>
                               <a class="to_review" href="#review-summary">{{ $totalRating }} reviews</a>
                                <a class="to_review" href="#review-summary">Write a review</a>
                               
                                
                            </div>
                        </div>
                        <div class="product-short-desc">
                         
                            <?php echo html_entity_decode($product['product_description']->short_desc); ?>
                        </div>
                        <div class="buttons mt20">
                            <a href="#product-features" class="btn btn-square product_features anchor"><span>Features</span></a>
                            <a href="#product-info" class="btn btn-square product_info anchor"><span>More Information</span></a>
                        </div>
                        <div class="stores mt20">
                            <div class="title h4 text-center">Available at</div>
                            <ul>
                            @if(!empty($product['product_to_store']))
                                
                            @foreach($product['product_to_store'] as $prod_store)
                            <li>
                                    <div class="h5 store-title">{{$prod_store->store_name}}</div>
                                    <div class="map-list">
                                        <i class="ion-ios-location-outline"></i>
                                        <p>{{$prod_store->store_address}}, {{$prod_store->store_city}}, {{$prod_store->store_state}}, {{$prod_store->store_country}}</p>
                                    </div>
                                </li>

                            @endforeach

                            @endif
                            </ul>
                            
                            <div class="text-center">
                                <?php $locale = url(Session::get('locale')); ?>
                                <a href="{{ URL($locale.'/') }}stores/" class="btn btn-square "><span>See All</span></a>
                                
                            </div>
                           
                        </div>
                        <div class="contact-info mt20">
                            <div class="contact-phone">
                                <a href="tel:{{$csetting->phone}}">
                                    <i class="ion-ios-telephone-outline"></i>{{$csetting->phone}}</a>
                            </div>
                            <div class="chat">
                                 <a href="{{$csetting->chat}}"><i class="ion-chatbubble-working"></i> Chat with us</a>
                            </div>
                        </div>
    				</div>
    			</div>
    		</div>

            <!-- Product Features -->
             
             
            <div class="row product-features mb40 pt20">
                <div id="product-features">
                   
                    @if(!empty($product['product_features']))
                    <div class="container">
                        <div class="row">
                        <div class="content-heading text-center">
                             <h2>Product Features</h2>
                        </div>
                   
                        <?php $i=0; ?>
                        @foreach($product['product_features'] as $keys => $features)
                            <?php $i++; 
                                if($i % 2 == 0) 
                                {
                                    $class= 'even';
                                }
                                else
                                {
                                    $class= 'odd';
                                }
                            ?>

                            @if($class == 'odd')
                                <div class="col-md-12 {{$class}}" >
                                    <div class="row mb40">                                

                                         <div class="col-md-6 text-justify"> 
                                             <div class="title-feature">
                                                <h4 class="mt0">{{ucfirst($features->name)}}</h4>
                                            </div>                 
                                            <?php echo html_entity_decode($features->prod_des); ?>               
                                        </div>
                                        <div class="col-md-6">
                                            <div class="image">
                                                <img src="{{ config::get('app.head_url')}}product/features/{{$features->prod_img_or_vdo}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                
                            @endif
                            @if($class == 'even')

                             <div class="col-md-12 {{$class}}" >
                                    <div class="row mb40">                                

                                        <div class="col-md-6">
                                            
                                            <div class="image">
                                                <img src="{{ config::get('app.head_url')}}product/features/{{$features->prod_img_or_vdo}}">
                                            </div>
                                        </div>

                                        <div class="col-md-6 text-justify"> 
                                            <div class="title-feature">
                                                <h4 class="mt0">{{ucfirst($features->name)}}</h4>
                                            </div>                  
                                            <?php echo html_entity_decode($features->prod_des); ?>               
                                        </div>
                                       
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                 

                            @endif

                            
                        @endforeach
                        </div>
                    </div>
                    @endif
                    @if(!empty($product['product_description']))
                        <?php $service = json_decode($product['product_description']->service);?>
                        @if (is_array($service) || is_object($service))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row icon-bg mt30">
                                    <div class="icon-area  pt40 pb40">
                                        @foreach($service as $key => $ser)
                                        <div class="icon-box">
                                            <div class="icon-img">
                                                <img src="{{ config::get('app.head_url')}}product/service_img/{{$ser->image}}">
                                            </div>
                                            <div class="icon-text">{{$ser->text}}</div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                </div>
            </div>
            @if(!empty($product['product_description']->desc) || !empty($product['product_attibute']))
            <?php $sclass=''; $class = ((!empty($product['product_description']->desc))?'active':'');
            if(empty($class)) $sclass = 'active';?>
            <div class="row product-information pt40">
                <div id="product-info" class="container">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div class="tab-center text-center">
                                @if(!empty($product['product_attibute']))
                                <ul id="productTab" class="nav nav-pills text-center mb30">
                                    <li role="presentation" class="{{$class}}">
                                        <a href="#product-details" data-toggle="tab" role="tab">Product Details</a>
                                    </li>
                                    <li role="presentation" class="{{$sclass}}">
                                        <a href="#specification" data-toggle="tab" role="tab">Specification</a>
                                    </li>
                                </ul>
                                @endif
                                <div class="tab-content">
                                    @if(!empty($product['product_description']->desc))
                                    <div id="product-details" class="tab-pane fade {{$class}} in">
                                        <div class="content-heading text-center">
                                            <h2>Product Details</h2>
                                        </div>
                                        <?php echo html_entity_decode($product['product_description']->desc); ?>                                    
                                    </div>
                                    @endif
                                    @if(!empty($product['product_attibute']))
                                    <div id="specification" class="tab-pane fade {{$sclass}}">
                                        <div class="table-responsive col-md-6 mr-auto">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    @foreach($product['product_attibute'] as $row)
                                                    <tr>
                                                        <td>{{$row->name}}</td>
                                                        <td><?php echo nl2br($row->text).'<br>';?></td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @endif
                                 </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row reviews mt40">
                <div class="container">

                    <div class="row">
                        <div class="content-heading text-center">
                            <h2 class="h2">Reviews</h2>
                        </div>
                        <div id="review-summary" class="ratings col-xs-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="rating-summary rate-score clearfix">
                                        <span class="rating_stars rating r{{ $totalRating }} rating-result">
                                              <span class="ion-ios-star-outline"></span>
                                              <span class="ion-ios-star-outline"></span>
                                              <span class="ion-ios-star-outline"></span>
                                              <span class="ion-ios-star-outline"></span>
                                              <span class="ion-ios-star-outline"></span>
                                       </span>
                                        <!-- <span class="star-rating">{{ $ratings['averageRating'] }}</span> -->
                                        <p class="rating-score-des"> {{ $totalRating }} Reviews</p>
                                    </div>
                                    <div class="question-block">
                                        <p>
                                            <a class="to_question" onclick="$('a[href=\'#questions\']').trigger('click');">{{$question['questions']}} Questions</a>

                                            <span>/</span>
                                            <a class="to_question" onclick="$('a[href=\'#questions\']').trigger('click');">{{$question['answers']}} Answers</a>

                                        </p>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="rate-detail">
                                        <ul class="rate-list">

                                            <li>
                                                <span class="star-rating" data-value="5"></span>
                                                <span class="rate-count rate-title">( {{ $ratings[5] }} )</span>
                                                <span class="rate-graph">
                                                    <span class="rate-graph-bar" style="width: {{ ratingPercent( $ratings[5], $totalRating ) }}%;"></span>



                                                </span>
                                            </li>
                                            <li>
                                                <span class="star-rating" data-value="4"></span>
                                                <span class="rate-count rate-title">( {{ $ratings[4] }} )</span>
                                                <span class="rate-graph">
                                                    <span class="rate-graph-bar" style="width: {{ ratingPercent( $ratings[4], $totalRating ) }}%;"></span>



                                                </span>
                                            </li>
                                            <li>
                                                <span class="star-rating" data-value="3"></span>
                                                <span class="rate-count rate-title">( {{ $ratings[3] }} )</span>
                                                <span class="rate-graph">
                                                    <span class="rate-graph-bar" style="width: {{ ratingPercent( $ratings[3], $totalRating ) }}%;"></span>



                                                </span>
                                            </li>
                                            <li>
                                                <span class="star-rating" data-value="2"></span>
                                                <span class="rate-count rate-title">( {{ $ratings[2] }} )</span>
                                                <span class="rate-graph">
                                                    <span class="rate-graph-bar" style="width: {{ ratingPercent( $ratings[2], $totalRating ) }}%;"></span>



                                                </span>
                                            </li>
                                            <li>
                                                <span class="star-rating" data-value="1"></span>
                                                <span class="rate-count rate-title">( {{ $ratings[1] }} )</span>
                                                <span class="rate-graph">
                                                    <span class="rate-graph-bar" style="width: {{ ratingPercent( $ratings[1], $totalRating ) }}%;"></span>



                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="buttons">
                                        <button id="review-form" class="btn btn-square btn-secondary">


                                            Write a Review
                                        </button>
                                        <button id="ask-question" class="btn btn-square btn-secondary">


                                         Ask a Question
                                     </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="write-a-review" class="write-review col-xs-12 pt40">
                            <div class="content-heading">
                                <h3 class="title">Write a review</h3> 
                            </div>
                            <form action="{{ route('review') }}" method="post" id="commentform" class="comment-form">
                                {{ csrf_field() }}
                                <div class="row mt40">                                    
                                    <?php
                                    foreach ($errors->review_errors->all('<li class="text-danger">:message</li>') as $message)
                                    {
                                        echo $message;
                                    }
                                    ?>
                                    <div class="comment-form-rating  pl15 pr15">

                                        <p>Score :</p>   
                                        <div class="control review-control-vote">
                                            <input type="radio" name="rating" id="Quality_1" value="1" class="radio" required="required">
                                            <label class="rating-1" for="Quality_1" title="1 star" id="Quality_1_label">
                                            <span>1 star</span>
                                            </label>
                                                <input type="radio" name="rating" id="Quality_2" value="2" class="radio">
                                            <label class="rating-2" for="Quality_2" title="2 stars" id="Quality_2_label">
                                            <span>2 stars</span>
                                            </label>
                                                <input type="radio" name="rating" id="Quality_3" value="3" class="radio" >
                                            <label class="rating-3" for="Quality_3" title="3 stars" id="Quality_3_label">
                                            <span>3 stars</span>
                                            </label>
                                                <input type="radio" name="rating" id="Quality_4" value="4" class="radio" >
                                            <label class="rating-4" for="Quality_4" title="4 stars" id="Quality_4_label">
                                            <span>4 stars</span>
                                            </label>
                                                <input type="radio" name="rating" id="Quality_5" value="5" class="radio">
                                            <label class="rating-5" for="Quality_5" title="5 stars" id="Quality_5_label">
                                            <span>5 stars</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 mt40">
                                       <div class="form-group required">
                                            <label for="review_title_field"><span>Title<em>*</em></span></label>
                                            <div class="control">
                                                <input class="form-control input-text" value="{{ old('review_title') }}" type="text" name="review_title" id="review_title_field" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group  required">
                                            <label for="review_summary"><span>Reviews&nbsp;<em>*</em></span></label>
                                            <div class="control">
                                                <input type="hidden" name="review_product" value="<?=$product['product']->product_id;?>">
                                                <textarea name="summary" style="min-height: auto; height: 133px;" id="summary" cols="5" rows="3" data-validate="{required:true}" required="required">{{ old('review_summary') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                       <div class="form-group  required">
                                            <label for="name"><span>Use Your Name&nbsp;<em>*</em></span></label>
                                            <div class="control">
                                               <input class="form-control input-text" value="{{ old('review_user_name') }}" type="text" name="name" id="name" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group  required">
                                            <label for="email"><span>Email&nbsp;<em>*</em></span></label>
                                            <div class="control">
                                                <input type="hidden" name="lang" value="{{Session::get('locale')}}"/>
                                                <input class="form-control input-text" value="{{ old('review_user_email') }}" type="email" name="email" id="email" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button">
                                        <button type="submit" name="review_submit" id="review_submit" value="Submit Review" class="action submit btn btn-primary"><span>Submit Review</span></button>
                                    </div>
                                </div>
                             </form>
                        </div>
                        <div id="questionBlock" class="product-questions col-xs-12 pt40">
                            <div class="content-heading">
                                <h3 class="title">Ask a Questions</h3> 
                            </div>
                            <form action="{{ route('question') }}" method="post" id="questionform" class="question-form">
                                {{ csrf_field() }}
                                <div class="row mt40">
                                    <?php
                                    foreach ($errors->question_errors->all('<li class="text-danger">:message</li>') as $message)
                                        echo $message;
                                    ?>
                                    <div class="col-xs-12 mt40">                                       
                                        <div class="form-group  required">
                                            <label for="summary"><span>Questions&nbsp;<em>*</em></span></label>
                                            <div class="control">
                                                <input type="hidden" name="question_product" value="<?=$product['product']->product_id;?>">
                                                <input type="hidden" name="lang" value="{{Session::get('locale')}}"/>
                                                <textarea name="summary" style="min-height: auto; height: 133px;" id="summary" cols="5" rows="3" required="required">{{ old('question_summary') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                       <div class="form-group required">
                                            <label for="name"><span>Use Your Name&nbsp;<em>*</em></span></label>
                                            <div class="control">
                                               <input class="form-control input-text" value="{{ old('question_user_name') }}" type="text" name="name" id="name" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group  required">
                                            <label for="email"><span>Email&nbsp;<em>*</em></span></label>
                                            <div class="control">
                                                <input class="form-control input-text" value="{{ old('question_user_email') }}" type="email" name="email" id="email" required="required">
                                            </div>
                                        </div>
                                     </div>
                                    <div class="button">
                                        <button type="submit" name="question_submit" id="question_submit" value="Post" class="action submit btn btn-primary"><span>Post</span></button>
                                    </div>

                                </div>
                             </form>
                        </div>
                        <div class="reviews-block questions-block mt40 col-xs-12">
                            <ul id="reviewTab" class="nav nav-tabs mb30">
                                <li role="presentation" class="active">
                                    <a href="#reviews" data-toggle="tab" role="tab">Reviews ({{ sizeof($reviews) }})</a>
                                </li>
                                <li role="presentation">
                                    <a href="#questions" data-toggle="tab"  role="tab">Questions ({{$question['questions']}})</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="reviews" class="tab-pane in active">
                                    <div class="reviews">     
                                        <div id="comments">
                                            <ol class="commentlist">
                                                <?php if(sizeof($reviews)): foreach($reviews as $review): ?>

                                                    <li class="comment even thread-even">

                                                    <div class="comment_container clearfix">

                                                        <div class="icon avatar">
                                                            <span>{{ substr(trim( $review->name ), 0, 1) }} </span>
                                                        </div>
                                                        <div class="comment-text">
                                                            <div class="meta">
                                                                <time class="review_published-date pull-right">{{ date('M d, Y',strtotime($review->created_at)) }}</time>
                                                                <h5><strong class="review_author">{{ $review->name }}</strong></h5>
                                                            </div>
                                                            <div class="review rating-summary clearfix">
                                                                <span class="rating_stars rating r{{ $review->rating }} rating-result" data-value="{{ $review->rating }}">
                                                                      <span class="ion-ios-star-outline"></span>
                                                                      <span class="ion-ios-star-outline"></span>
                                                                      <span class="ion-ios-star-outline"></span>
                                                                      <span class="ion-ios-star-outline"></span>
                                                                      <span class="ion-ios-star-outline"></span>
                                                               </span>
                                                            </div>
                                                            <div class="description">
                                                                <p class="Reviews-title"><strong>{{ $review->title }}</strong></p>
                                                                <p class="content-description">{{ $review->text }}</p>
                                                            </div>
                                                            <!--<div class="comment-foot"> 
                                                                    <div class="pull-left">
                                                                        <a href=""><i class="ion-share"></i> Share |</a>
                                                                    </div> 
                                                                    <div class="pull-right">
                                                                        <span>Was This Review Helpful?</span>
                                                                        <span class="like"><i class="ion-thumbsup"></i></span>
                                                                        <span class="dis_like"><i class="ion-thumbsdown"></i></span>
                                                                    </div>
                                                                </div>
                                                            -->
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php endforeach; endif; ?>
                                                <!-- #comment-## -->
                                            </ol>
                                        </div>
                                    </div>                                   
                                </div>
                                <div id="questions" class="tab-pane ">
                                    <div class="questions">
                                        <?php if(sizeof($question['datas'])): foreach($question['datas'] as $q): ?>
                                        <article class="question-block">
                                             <div class="question-top-block">
                                                <div class="question-avatar">
                                                    <span>{{ substr(trim( $q['username'] ), 0, 1) }}</span>
                                                </div>                                              
                                                <div class="question-inner">
                                                    <div class="meta">
                                                        <h4 class="question_author"> <a href="#"><strong >{{ $q['username'] }}</strong></a></h4>
                                                        <time class="question-date pull-right" datetime="">{{ date('M d, Y', strtotime($q['created_on']) ) }}</time>
                                                        <p class="question-desc">Q: {{ rtrim( $q['question'], '?' ) }} ?</p>
                                                        <span class="question-comment">
                                                    </div>
                                                    @if($q['answer'])
                                                    <div class="answer-block">
                                                        <a href="#"><i class="icon-comment"></i>Answer</a></span>
                                                        <p class="answer-desc">{{$q['answer']}}</p>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                         </article>
                                         <?php endforeach; endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($product['related_products']))
            <div id="related-products" class="row pt40 pb40">
                <div class="container">
                    <div class="row">
                        <div class="content-heading text-center">
                            <h2 class="title">
                                Related Products
                            </h2>
                        </div>
                        @foreach($product['related_products'] as $key => $getimage)
                        <div class="product-layout col-md-3 col-sm-6 item mb30">
                            <div class="product-thumb">
                                <div class="image">
                                    <a href="{{ URL($familer_path,[$getimage->product_slug]).'.html' }}">
                                       <img src="{{ Config::get('app.head_url') }}product/images/{{$getimage->prod_slider_image}}">
                                    </a>
                                </div>
                                <span class="product-label {{$getimage->comfort}}">{{ucfirst($getimage->comfort)}}
                                </span>
                            </div>
                            <div class="product-details text-center">
                                <h3 class="name h4">
                                    {{ucfirst($getimage->name)}}
                                </h3>
                                
                                <div class="buttons">
                                    <a href="{{ URL($familer_path,[$getimage->product_slug]).'.html' }}" class="btn btn-round btn-primary">See It</a>
                                </div>
                                <div class="product-desc"> 
                                </div>
                                <!-- <div class="product-attr"> - 3 Zone</div> -->
                                <div class="product-attr"></div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            @if(!empty($product['other_products']))
            <div id="other-products" class="row pt40 pb40">
                <div class="container">
                    <div class="row">
                        <div class="content-heading text-center">
                            <h2 class="title">
                                More Products For A Better Night's Sleep
                            </h2>
                        </div>
                        <div class="mr-auto col-md-7">
                            @foreach($product['other_products'] as $key => $getimage)
                            <div class="product-layout col-md-6 col-sm-6 item mb30">
                                <div class="product-thumb">
                                    <div class="image">
                                        <a href="{{ URL($familer_path,[$getimage->product_slug]).'.html' }}">
                                           <img src="{{ Config::get('app.head_url') }}product/images/{{$getimage->prod_slider_image}}">
                                        </a>
                                    </div>
                                    <span class="product-label {{$getimage->comfort}}">{{ucfirst($getimage->comfort)}}
                                    </span>
                                </div>
                                <div class="product-details text-center">
                                    <h3 class="name">
                                        <a href="{{ URL($familer_path,[$getimage->product_slug]).'.html' }}">{{ucfirst($getimage->name)}}</a>
                                    </h3>
                                    <div class="product-desc"> 
                                    </div>
                                    <!-- <div class="product-attr"> - 3 Zone</div> -->
                                    <div class="product-attr"></div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                
            </div>
            @endif
        </div>
        <?php endif; ?>
	</div>
</main>
@include('includes.newsletter-form')
@include('includes.footer')
@endsection

@section('script')
<script type="text/javascript">
// $(".product_features").click(function() {
//     $('html, body').animate({
//         scrollTop: $(".product-features").offset().top
//     }, 1000);
// });
$('.product_features').on("click",function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    console.log(target.offset().top - 150);
    if (target.length) {
      $('html,body').animate({
        scrollTop: (target.offset().top - 150) // 70px offset for navbar menu
      }, 1000);
      return false;
    }
  }
});
$('.product_info').on("click",function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    console.log(target.offset().top - 90);
    if (target.length) {
      $('html,body').animate({
        scrollTop: (target.offset().top - 90) // 70px offset for navbar menu
      }, 1000);
      return false;
    }
  }
});
// $(".product_info").click(function() {
//     $('html, body').animate({
//         scrollTop: $(".product-information").offset().top
//     }, 1000);
// });
$(".to_review").click(function() {
    $('html, body').animate({
        scrollTop: $(".row.reviews").offset().top
    }, 1000);
});
$('#write-a-review').hide();
$('#review-form').click(function() {
    $('#write-a-review').show();
    $('#questionBlock').hide();
});
$('#questionBlock').hide();
$('#ask-question').click(function() {
    $('#questionBlock').show();
    $('#write-a-review').hide();
});
$('#reviewTab li a').click(function (e) {
    e.preventDefault();
    $('html, body').stop().animate();
    $(this).tab('show');
    //console.log(stop);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
var owlAdditionals = $('.image_carousel');
var wrapperWidth = $(".image-additional").width();
//var itemWidth = ( + 6);
//var itemcalc = Math.round(wrapperWidth / itemWidth);
owlAdditionals.owlCarousel({
items : 6,
mouseDrag: true,
dots: false,
nav: true,
slideSpeed:200,
navText: [
"<span class='slide_arrow_prev add_img'><i class='ion-ios-arrow-left'></i></span>",
"<span class='slide_arrow_next add_img'><i class='ion-ios-arrow-right'></i></span>"
]
});
});
</script>

<script src="{{ Config::get('app.head_url') }}frontend/js/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript"><!--
$(document).ready(function() {
   $('.colorbox').colorbox({
      overlayClose: true,
      maxWidth:'95%',
      rel:'gallery',
      opacity: 0.5,
      width:"600px",
   	  height: "400px"
}); 
});
//--></script>
<script src="{{ Config::get('app.head_url') }}frontend/js/cloud-zoom.1.0.2.min.js"></script>
<script type="text/javascript">
        jQuery(function($) {
            //Product thumbnails
            $(".cloud-zoom-gallery").last().removeClass("cboxElement");
            $(".cloud-zoom-gallery").click(function() {
                $("#zoom-btn").attr('href', $(this).attr('href'));
                $("#zoom-btn").attr('title', $(this).attr('title'));
            
                    $(".cloud-zoom-gallery").each(function() {
                        $(this).addClass("cboxElement");
                    });
                    $(this).removeClass("cboxElement");
                });
        });
</script>
@endsection