<!doctype html>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Guided Online Tutorial</title>

<head>
<link href="{{ Config::get('app.head_url') }}help_css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="{{ Config::get('app.head_url') }}help_css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}help_css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{ Config::get('app.head_url') }}help_css/new_scroll.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- 27-04 team B-->
<script>
 $(document).ready(function() { 
//$(".openpanel").on("click", function() {
  // $("#demo3,#collapseTwoOne").collapse('show');
//});
//$(".closepanel").on("click", function() {
//	$('#sub'+i).parent().addClass('active');
   //$("#demo3,#collapseTwoOne").collapse('hide');
//});
$('#MainMenu,#accordion21').on('show.bs.collapse', function () {
   $('#MainMenu,#accordion21 .in').collapse('hide');
});
$('#MainMenu,#accordion55').on('show.bs.collapse', function () {
   $('#MainMenu,#accordion55 .in').collapse('hide');
});

$('#MainMenu,#accordion66').on('show.bs.collapse', function () {
   $('#MainMenu,#accordion66 .in').collapse('hide');
});

});


</script>


<script>
$(document).ready(function(e) { 
//Next Button
$('.next').on('click',function(){



if($('.menus1').hasClass('active')) {
        $("#demo4,accordion21").collapse('show');
          // $("#demo3,#collapseTwoOne").collapse('show');
        }
if($('.menus2').hasClass('active')) {
           $("#demo4,accordion21").collapse('hide');
		}



//Descriptive Manuals
if($('.menu1').hasClass('active')) {
        $("#demo3,accordion21").collapse('show');
          // $("#demo3,#collapseTwoOne").collapse('show');
        }
if($('.menu2').hasClass('active')) {

$("#demo4,#collapse24").collapse('show');
           //$("#demo3,#collapseTwoTwo").collapse('show');
        }
		
if($('.menu33').hasClass('active')) {
           $("#demo3").collapse('hide');
        }	
if($('.menu3').hasClass('active')) {

$("#demo5,#collapse32").collapse('show');
          // $("#demo3,#collapse3").collapse('show');
        }
		
if($('.menu44').hasClass('active')) {
           $("#demo4").collapse('hide');
		}
		
		
		
		
		
		
if($('.menu4').hasClass('active')) {
           $("#accordion21,#collapse4").collapse('show');
        }	
if($('.menu5').hasClass('active')) {
           $("#demo3,#collapse5").collapse('show');
        }	
if($('.menu6').hasClass('active')) {
           $("#demo3,#collapse6").collapse('show');
        }	
if($('.menu7').hasClass('active')) {
           $("#demo3,#collapse7").collapse('show');
        }	
if($('.menu8').hasClass('active')) {
           $("#demo3,#collapse88").collapse('show');
        }
if($('.menu332').hasClass('active')) {
           $("#demo3").collapse('hide');
        }			
	
 //Illustrated Catalogues
	
if($('.menu9').hasClass('active')) {
           $("#demo4,#collapse23").collapse('show');
        }
if($('.menu10').hasClass('active')) {
           $("#demo4,#collapse24").collapse('show');
        }
if($('.menu11').hasClass('active')) {
           $("#demo4,#collapse25").collapse('show');
        }
if($('.menu12').hasClass('active')) {
           $("#demo4,#collapse26").collapse('show');
        }
if($('.menu34').hasClass('active')) {
           $("#demo4").collapse('hide');
        }			
// Animated Interactive Contents

if($('.menu13').hasClass('active')) {
           $("#demo5,#collapse27").collapse('show');
        }	
if($('.menu14').hasClass('active')) {
           $("#demo5,#collapse28").collapse('show');
        }	

if($('.menu15').hasClass('active')) {
           $("#demo5,#collapse29").collapse('show');
        }	
if($('.menu16').hasClass('active')) {
           $("#demo5,#collapse30").collapse('show');
        }
if($('.menu17').hasClass('active')) {
           $("#demo5,#collapse30").collapse('hide');
        }		
	

 //$('span.menu2').hasClass('active');
//{
// $("#accordion21,#collapseTwoTwo").collapse('show');
// }
 
});
});

</script>

<script>
$(document).ready(function(e) { 
//previous Button
$('.previous').on('click',function(){
//$(".previous").click(function(){
//Animated

if($('.menus2').hasClass('active')) {

 $("#demo4,accordion21").collapse('show');
                  }
if($('.menus3').hasClass('active')) {
           $("#demo4,accordion21").collapse('hide');
        }					  
				  
				  
				  
				  
				  
		
if($('.menu3').hasClass('active')) {
           $("#demo5").collapse('hide');
        }	

		
if($('.menu33').hasClass('active')) {
        $("#demo3,accordion21").collapse('show');
                }		
	if($('.menu2').hasClass('active')) {
           $("#demo4").collapse('hide');
        }	
		
		
		
if($('.menu18').hasClass('active')) {
            $("#demo5,#collapse29").collapse('show');
        }	
if($('.menu19').hasClass('active')) {
            $("#demo5,#collapse28").collapse('show');
        }	
if($('.menu20').hasClass('active')) {
            $("#demo5,#collapse27").collapse('show');
        }	
if($('.menu34').hasClass('active')) {
            $("#demo5").collapse('hide');
        }
		
	//Illustrated Catalogues		
if($('.menu21').hasClass('active')) {
            $("#demo4,#collapse26").collapse('show');
        }	
if($('.menu22').hasClass('active')) {
            $("#demo4,#collapse25").collapse('show');
        }	
if($('.menu23').hasClass('active')) {
            $("#demo4,#collapse24").collapse('show');
        }	
if($('.menu24').hasClass('active')) {
            $("#demo4,#collapse23").collapse('show');
        }
if($('.menu333').hasClass('active')) {
            $("#demo4").collapse('hide');
        }
	
	
if($('.menu25').hasClass('active')) {
            $("#demo3,#collapse88").collapse('show');
        }		
if($('.menu26').hasClass('active')) {
            $("#demo3,#collapse7").collapse('show');
        }	
if($('.menu27').hasClass('active')) {
            $("#demo3,#collapse6").collapse('show');
        }	
if($('.menu28').hasClass('active')) {
            $("#demo3,#collapse5").collapse('show');
        }	
if($('.menu29').hasClass('active')) {
            $("#demo3,#collapse4").collapse('show');
        }	
if($('.menu30').hasClass('active')) {
            $("#demo3,#collapse3").collapse('show');
        }	
if($('.menu31').hasClass('active')) {
            $("#demo3,#collapseTwoTwo").collapse('show');
        }	
if($('.menu32').hasClass('active')) {
            $("#demo3,#collapseTwoOne").collapse('show');
        }	
		
if($('.menu1').hasClass('active')) {
            $("#demo3,#collapseTwoOne").collapse('hide');
        }
	

//Descriptive Manuals
});
});

</script>

<!-- <script>
 $(document).ready(function(e) { 
	i=0;
	$('.next').on('click',function(){
	if(i > 0 && i < 10)
	 {
		//$('.mtree-node').removeClass('mtree-active mtree-open');
			$('.sub'+i).trigger('click');
			alert();
			$('.sub'+i).parent().addClass('active');
	 		i++;
	 }
});
});

</script> --> 
<!-- <script>
$(document).ready(function(e) { 
$('.next').on('click',function(){
if $('span.menu1').hasClass('active');
alert();
 $("#demo3,#collapseTwoOne").collapse('show');
});
});
});
</script> -->
<!-- 27-04 team B-->

<script>
$(document).ready(function(e) {
	
    	$('.scrollbox2').enscroll({
    horizontalScrolling: true,
    verticalTrackClass: 'vertical-track2',
    verticalHandleClass: 'vertical-handle2',
    horizontalTrackClass: 'horizontal-track2',
    horizontalHandleClass: 'horizontal-handle2',
    cornerClass: 'corner2'
});
});
</script>

<script src="{{ Config::get('app.head_url') }}help_js/enscroll.js" type="text/javascript"></script>

<!-- Latest compiled JavaScript -->
<script src="{{ Config::get('app.head_url') }}help_js/bootstrap.min.js"></script>




<script src="{{ Config::get('app.head_url') }}help_js/respond.min.js"></script>

<script>
$("#button").click(function(){
    var index = $("#accordion").accordion('option','active');
    var total = $("#accordion").children('div').length;
    index++;

    // include restart same as previous answer     
    if (index >= total) {
        index = 0;
    }

    $("#accordion").accordion("option", "active", index);

}
</script>

<script>
$(function () {

    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
	
		$('.tree li.parent_li > ul li.parent_li > ul').hide('fast');
		
	$('.tree li.parent_li > span').on('click', function (e) {
		 $(this).next("ul").slideToggle();

    });

var win_hin = $(window).height();
	var a = win_hin-160;
	var b = win_hin-140;
	$("#iframe").css("height",b);
	$("#aside").css("height",b);
	
});
$(window).resize(function(){
var my_win = $(window).width();

	var a = win_hin-160;
	var b = win_hin-140;
	$("#iframe").css("height",b);
	$("#aside").css("height",b);
	
	
	});
$(document).ready(function(){
	
var my_win = $(window).width();
var my_heg = $(window).height();
	
if(my_win == 1280 && my_heg == 1024 )
{
//alert(my_win);
	var a = 190;
	var b = (my_heg-a);
	alert(b);
	$("#iframe").css("height",b);
	$("#aside").css("height",b);

}
if(my_win > 1679)
{
	var win_hin = $(window).height();
	var b = win_hin-190;
	
	$("#iframe").css("height",b);
	$("#aside").css("height",b);
	

}
var a = document.querySelectorAll('.sss').length;
var allvalue = document.querySelectorAll('.sss');
var b = [allvalue];
//alert(b[2]);

	});

</script>
<script>

 $( document ).ready(function() {
 
 $("#scc").off("click");
 
 $(".video").hide();

$(".sss").click(function(){
$("#bgimage").hide();
 $(".video").show();


var name = $(this).attr("alt");
var idnum = $(this).attr('id');
$('.video').attr('id', idnum);
$(".sss").removeClass("active");
$(this).addClass('active');


$('.sour1').attr('src', '{{ Config::get('app.head_url') }}help_video/' + name + '.webm');
$('.sour2').attr('src', '{{ Config::get('app.head_url') }}help_video/' + name + '.mp4');
 $('.sour3').attr('src', '{{ Config::get('app.head_url') }}help_video/' + name + '.ogg');


$(".video").load();



  document.getElementById("heading").innerHTML=$(this).text();
  });

$(".next").click(function(){
$("#bgimage").hide();
 $(".video").show();
 
var x = $(".video").attr("id");
if(x<15){
var newid = parseInt(x)+1;
var newid2 = $("#"+newid).attr("alt");

$(".sss").removeClass("active");
$("#" + newid).addClass('active');


 
$('.sour1').attr('src', '{{ Config::get('app.head_url') }}help_video/' + newid2 + '.webm');
$('.sour2').attr('src', '{{ Config::get('app.head_url') }}help_video/' + newid2 + '.mp4');
 $('.sour3').attr('src', '{{ Config::get('app.head_url') }}help_video/' + newid2 + '.ogg');
 $(".video").load();
 
 $('.video').attr('id', newid);
  document.getElementById("heading").innerHTML=$("#" + newid).text();
  $('ul').show('fast');
  }
   else {
  $("#bgimage").show();
 $(".video").hide();
   document.getElementById("heading").innerHTML="End";
  }
});


$(".previous").click(function(){
$("#bgimage").hide();
 $(".video").show();
var x = $(".video").attr("id");
if(x>4){
var newid = parseInt(x)-1;
var newid2 = $("#"+newid).attr("alt");
$(".sss").removeClass("active");
$("#" + newid).addClass('active');


$('.sour1').attr('src', '{{ Config::get('app.head_url') }}help_video/' + newid2 + '.webm');
$('.sour2').attr('src', '{{ Config::get('app.head_url') }}help_video/' + newid2 + '.mp4');
 $('.sour3').attr('src', '{{ Config::get('app.head_url') }}help_video/' + newid2 + '.ogg');
 $(".video").load();
 
  $('.video').attr('id', newid);
  document.getElementById("heading").innerHTML=$("#" + newid).text();
  $('ul').show('fast');
  }
  else {
  $("#bgimage").show();
 $(".video").hide();
   document.getElementById("heading").innerHTML="Welcome";
  }
});

$(".fuso").click(function(){
$("#bgimage").show();
 $(".video").hide();
document.getElementById("heading").innerHTML="Welcome"
})

});


</script>

<script type="text/javascript">
function audios()

{


 var x = $("#audiosimg").attr("class");
if(x==1){
var vid = document.getElementById("audios");
 vid.muted = true;
 $('#audiosimg').attr('src', '{{ Config::get('app.head_url') }}help_images/stop.png');
 $('#audiosimg').attr('class', '2');
}
else if(x==2){
var vid = document.getElementById("audios");
 vid.muted = false;
 $('#audiosimg').attr('src', '{{ Config::get('app.head_url') }}help_images/play.png');
 $('#audiosimg').attr('class', '1');
}

}

</script>
<style>
/* 27-07*/
a:link {
    color: #333 ;
    }

.panel-body {
    padding:0 !important;
}
#demo3, #demo4, #demo5 {
    padding: 0 5px !important;
}
.panel {
margin-bottom: 0px !important;
box-shadow:none !important;
}
.list-group-item {
        border:none !important;
		padding: 0 !important;
		/* padding: 7px 5px !important; */
	}
a.list-group-item:focus, a.list-group-item:hover, button.list-group-item:focus, button.list-group-item:hover {
    background-color:#fff !important;
	border:none !important;
}
/* 27-07*/

a:focus {
    outline:none !important;
}	
.gra {background: rgba(46,46,46,1);
background: -moz-linear-gradient(top, rgba(46,46,46,1) 0%, rgba(227,227,227,1) 51%, rgba(46,46,46,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(46,46,46,1)), color-stop(51%, rgba(227,227,227,1)), color-stop(100%, rgba(46,46,46,1)));
background: -webkit-linear-gradient(top, rgba(46,46,46,1) 0%, rgba(227,227,227,1) 51%, rgba(46,46,46,1) 100%);
background: -o-linear-gradient(top, rgba(46,46,46,1) 0%, rgba(227,227,227,1) 51%, rgba(46,46,46,1) 100%);
background: -ms-linear-gradient(top, rgba(46,46,46,1) 0%, rgba(227,227,227,1) 51%, rgba(46,46,46,1) 100%);
background: linear-gradient(to bottom, rgba(46,46,46,1) 0%, rgba(227,227,227,1) 51%, rgba(46,46,46,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2e2e2e', endColorstr='#2e2e2e', GradientType=0 );;
}
.iframe_content {
    top: 44px;
    width: 80%;
}
/* .iframe_content {
    top: 44px;
    width: 95.9% !important;
}*/
@media only screen and (width : 1280px) and (height: 1024px) {

.iframe_content > img {
    max-width: 90% !important;
}
video {
    max-width: 95% !important;
}

.body_content #aside, .body_content #iframe {
    height: 860px !important;
}

}

@media only screen and (min-width:1025px) and (max-width:1280px){
.iframe_content{
width:100% !important;top:130px !important;
}
}

@media only screen and (min-width:1699px) and (max-width:1999px){
.iframe_content, .iframe_content #bgimage {
  width:85% !important;
    top: 44px !important;
}
}


</style>
</head>

<body>
<div class="parent"> <img src="{{ Config::get('app.head_url') }}help_images/header.jpg" width="100%">
  <div id="menu">
    <ul>
      <li> <!-- <a style="color:white;text-decoration:none;" href="../index.html"><img src="images/home.png"> &nbsp;Home</a> --> </li>

	        <li> <span onclick="previous();" class="previous"><img src="{{ Config::get('app.head_url') }}help_images/ar1.png"> Previous</span> </li>
      <li> <span onclick="next();" class="next">Next  <img src="{{ Config::get('app.head_url') }}help_images/ar2.png"></span></li>

	 
    </ul>
 <img src="{{ Config::get('app.head_url') }}help_images/play.png" id="audiosimg" class="1" onclick="audios();">
   
   
  </div>
  <div class="body_content">
    <div id="aside">
      <div class="scrollbox2">
        <div class="tree well">
          <ul>
            <li> <span class="" id="scc" ><strong style="color:#F00">Service Contract Calculator</strong></span> 
              <ul>
				<div id="MainMenu">
					<div class="list-group panel">
						
						<div class="collapse" id="demo0">
						</div>
					  
					  
					    
						
						<a href="#demo55" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu">
						
						<li>
					   <span alt="1_login_page" id="4" class="sss" title="Collapse this branch" style="color:#333;">
					   <img src="{{ Config::get('app.head_url') }}help_icons/login.png" /><strong> Login Page</strong> </span> 
					 </li> 
						 <li><span class="sss menus1" id="5" alt="2_ scc_dashboard"><img src="{{ Config::get('app.head_url') }}help_icons/SCC dash boardb.png">
						<strong> SCC Dashboard</strong></span></li>
						
						</a>
						
						<div class="collapse" id="demo55">
						
					  </div>
						
					  <!--Child -->
					  <a href="#demo4" class="list-group-item " data-toggle="collapse" data-parent="#MainMenu">
					    <li><span class="sss menus3" id="6" alt="3_data_maturity"><img src="{{ Config::get('app.head_url') }}help_icons/Data Maturity_ 01.png"> <strong> Data Maturity </strong></span></a></li>
					  
					  <div class="collapse" id="demo4">
					
					<div class="panel-group" id="accordion55">
					<ul>
					
					
						 <li>
								<div class="panel">
                                    <a data-toggle="collapse" data-parent="#accordion55" href="#collapse24">
									<span alt="3_1_data_maturity-parts" class="sss menu33" id="7">
									<img src="{{ Config::get('app.head_url') }}help_icons/4.png"> Parts</span>
                                    </a>
                                    
                                </div>
								</li>
								<li>
								<div class="panel">
                                    <a data-toggle="collapse" data-parent="#accordion55" href="#collapse25">
									<span class="sss" alt="3_2_data_maturity-labor" id="8">
									<img src="{{ Config::get('app.head_url') }}help_icons/4.png"> Labor</span> 
                                    </a>
                                    
                                </div>
								</li>
								<li>
								<div class="panel">
                                    <a data-toggle="collapse" data-parent="#accordion55" href="#collapse26">
									<span class="sss" alt="3_3_data_maturity-interval" id="9"><img src="{{ Config::get('app.head_url') }}help_icons/4.png"> Interval</span> 
                                    </a>
                                    
                                </div>
								</li>
								
				    </ul>
					
					</div>
					  </div>
					  
									
					<a href="#demo6" class="list-group-item list-group-item" data-toggle="collapse" data-parent="#MainMenu">
						 
						  <li><span class="sss menus2" id="10" alt="4_create_quote"><img src="{{ Config::get('app.head_url') }}help_icons/Create Quote _01.png"><strong> Create Quote</strong></span></li>
						  
						  
						     <li><span class="sss" id="11" alt="5_created_quote_list"><img src="{{ Config::get('app.head_url') }}help_icons/Create Quote_1.png"><strong> Created Quote List</strong></span></li>
						  
						  	    <li><span class="sss" id="12" alt="6_market_settings"><img src="{{ Config::get('app.head_url') }}help_icons/Market Setting _1.png"><strong> Market Settings</strong></span></li>
								
									    <li><span class="sss" id="13" alt="7_campaign_dashboard"><img src="{{ Config::get('app.head_url') }}help_icons/Campaign Dashboard_01.png"><strong> Campaign Dashboard</strong></span></li>
										
								 <li><span class="sss" id="14" alt="8_create_campaign"><img src="{{ Config::get('app.head_url') }}help_icons/create campaign_01.png"><strong> Create Campaign</strong></span></li>
						  
						   <li><span class="sss" id="15" alt="9_view_campaign"><img src="{{ Config::get('app.head_url') }}help_icons/View campaign_01.png"><strong> View campaign</strong></span></li>
						  
						  
					 </a>
						
					  
					  
						<div class="collapse" id="demo6">
						
					  </div>
					  
					 
			
				 
					  
					</div>
				
	  
			</ul>
			 
            </li>
			
			
			<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      
      
      
    </div>  
  </div>  
</div>
          </ul>
        </div>
      </div>
    </div>
    <div id="iframe" class="gra">
       
        <h1 id="heading">Welcome </h1>
        <div style="width: 100%; float: left; text-align: center;">
        <div class="iframe_content">
       
		<img src="{{ Config::get('app.head_url') }}help_images/ui.jpg"  id="bgimage" style="border: 1px solid #4c4b4b; width:85% !important;">
		
          <video id="3" class="video" width="100%" height="90%"  onclick="if(this.paused){this.play(); } else{this.pause();}" controls  autoplay >
	 	 
		 <source class="sour2" src="{{ Config::get('app.head_url') }}help_video/login.mp4" type="video/mp4">
        
                    Your browser does not support the video tag.
       
			</video>
		</div>
        </div>
        
        <!--  <iframe id="frmLoad" allowtransparency="true" src="video.html"
                            width="100%" height="750px"  style="border: solid 0px blue; background-color: Transparent;"
                            frameborder="0" scrolling="no"></iframe> --> 
<audio src="{{ Config::get('app.head_url') }}help_video/bgm.mp3" type="audio/mpeg" id="audios" autoplay loop> </audio>

      </div>
    </div>
  </div>
  <!--body_content-->
  
  <footer class="fluid footer"> 
   <div style="background:black">
   <p style="text-align:center; color:#fff; margin: 0;">© 2017 Mitsubishi Fuso Truck and Bus Corporation. All right reserved.</p>
   
   </div>
  
  <!-- <img src="images/footer.png" width="100%"> --> </footer>
</div>
</body>
</html>
