@extends('layouts.layout_login')
@section('content')
  
<div style="    position: fixed;
    width: 100%;
    z-index: 1;
    top: 0px;">
<header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <strong>Email: </strong>support@firstfeet.in
                    &nbsp;&nbsp;
                    <strong>Support: </strong>+90-897-678-44
                </div>

            </div>
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero" align="center">
        <div class="container">
            <div class="navbar-header" style="width: 100%;">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="" href="/">
                <img src="img/Daimler_AG.png" id="loginavatar" class="img-responsive" />
                </a>

            </div>

            
        </div>
    </div>
    <!-- LOGO HEADER END-->


    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav">
                            <li><a href="/login">Login</a></li>
                            <li><a class="menu-top-active">Register</a></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
   
   </div>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Please Register To Login </h4>

                </div>

            </div>
            <div class="row">
                <div class="col-md-6">

                     {!! Form::open(array('url' => '/user/register','class'=>'form-register','method'=>'post')) !!}
                   
                     <h4> Register with <strong>First Feet Business Service </strong></h4>
                    <br />
                     
                        <div class="form-group">
                          
                           <input type="text" class="form-control" name="name" placeholder="Full Name" required>
                      </div>


                       <div class="form-group">
                          
                           <input type="text" class="form-control" name="email" placeholder="userId" required>
                      </div>


                       <div class="form-group">
                          
                           <input type="password" class="form-control" name="password" placeholder="Password" required>
                      </div>
                      
                      <hr />
                      <input type="submit" class="btn btn-info" value="Sign In" />
                      &nbsp;

                        {!! Form::close() !!}
                </div>
                <div class="col-md-6">
                    <div class="alert alert-info">
                        Lorem ipsum dolor sit amet ipsum dolor sit ame with basic pages you need to craft your project. 
                        Lorem ipsum dolor sit amet ipsum dolor.
                        <br />
                         <strong> Some of its features are given below :</strong>
                        <ul>
                            <li>
                                Lorem ipsum dolor sit amet ipsum dolor sit ame
                            </li>
                            <li>
                                 Aamet ipsum dolor sit ame
                            </li>
                            <li>
                                Lorem ipsum dolor sit amet ipsum dolor
                            </li>
                            <li>
                                Clean and light code used.
                            </li>
                        </ul>
                       
                    </div>
                    <div class="alert alert-success">
                         <strong> Instructions To Use:</strong>
                        <ul>
                            <li>
                               Lorem ipsum dolor sit amet ipsum dolor sit ame
                            </li>
                            <li>
                                 Aamet ipsum dolor sit ame
                            </li>
                            <li>
                               Lorem ipsum dolor sit amet ipsum dolor
                            </li>
                            <li>
                                 Cpsum dolor sit ame
                            </li>
                        </ul>
                       
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2017 | By : <a href="http://www.firstfeetconsulting.com/" target="_blank">First Feet Business Service</a>
                </div>

            </div>
        </div>
    </footer>


@if(Session::has('message'))
      <div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

                        <center>

                            <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

                        </center>
                 {{ Session::forget('message') }}
              </div>
            </div>
        </div>
    </div>
 @endif


@stop