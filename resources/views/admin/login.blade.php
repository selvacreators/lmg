@extends('layouts.layout_login')
@section('content')
<style type="text/css">
body{
height: 100%;
background: url("{{ Config::get('app.head_url') }}img/b1.jpg") no-repeat center center fixed;
background-size: cover;
overflow: hidden;
}
label
{
color: white;
}
.form-control{
border:0px;
}
.input-group-addon{
border:0px;
color:#b5b5b5;
}
input::-webkit-input-placeholder {
color: #B5B5B5 !important;
}
input:-moz-placeholder { /* Firefox 18- */
color: #B5B5B5 !important;  
}
input::-moz-placeholder {  /* Firefox 19+ */
color: #B5B5B5 !important;  
}
input:-ms-input-placeholder {  
color: #B5B5B5 !important;  
}
footer {
    position: fixed;
    height: 26px;
    bottom: 0;
    width: 100%;
    padding:0px 20px;
    background-color:#0d1221;
    margin-left:-29px;
}
</style>
<div class="row">
<div class="col-md-offset-8 col-md-4" style="background: rgba(13, 18, 33, 0.8); height: 100vh;">
                    <div class="col-md-11" style="margin-top: 30%;">
                     {!! Form::open(array('url' => '/auth/login','class'=>'form-signin','method'=>'post')) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 style="color: #ffffff; border-left: 7px solid #08395a;  padding-left: 7px;">Login</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            <div class="input-group">
                        <span class="input-group-addon"  style=" background: #08395a"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control"  style=" background: #08395a;color:#ffffff" name="email" placeholder="User Name" autocomplete="off">
  <span class="input-group-addon" style=" background: #08395a"><i class="fa fa-plus-circle"></i></span>
                    </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="input-group">
                        <span class="input-group-addon"  style=" background: #08395a"><i class="fa fa-lock"></i></span>
                        <input type="password" name="password" class="form-control" style=" background: #08395a;color:#ffffff" name="username" placeholder=" Password" autocomplete="off">
  <span class="input-group-addon" style=" background: #08395a"><i class="fa fa-plus-circle"></i></span>
                    </div>
                            </div>
                        </div>
                        <br>
                        <!-- <div class="row">
                            <div class="col-sm-6">
                                <a href="#" style="color: white">Don't have an account?</a>
                            </div>
                            <div class="col-sm-6" align="right">
                                <a href="#" style="color: white">Forgot your password?</a>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <!-- <input type="checkbox" name=""> <span style="color: #ffffff;">Remember me</span> -->
                                    <a href="#" style="color: white">Forgot your password?</a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="submit" style="background: #ff0000;border-color: #08395a;font-size: 16px;padding: 5px 30px;border-radius: 4px;float: right;font-weight: 200;" class="btn btn-daimler" value="Sign In">
                                </div>
                            </div>
                        </div>
                        <footer class="fixed-bottom"><p> &copy; <?php echo date('Y'); ?> LMGWorld. All right reserved.</p></footer>
                    {!! Form::close() !!}
                    </div>
                </div>
                

            </div>

        
    <!-- CONTENT-WRAPPER SECTION END-->
  


@if(Session::has('message'))
      <div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

                        <center>

                            <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

                        </center>
                 {{ Session::forget('message') }}
              </div>
            </div>
        </div>
    </div>
 @endif

@stop