
  

@section('content')



 
 <div class="container">
   <div class="panel panel-success">
     <div class="panel-heading">Integrating CKeditor in Laravel 5.3 </div>
     <div class="panel-body">
       <textarea id="ckeditor" class="ckeditor"> </textarea>  
        
     </div>
   </div>
 </div>


<script src="{{ Config::get('app.head_url') }}admin/ckeditor/ckeditor.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>
<script type="text/javascript">  

        CKEDITOR.replace( 'editor1', {
    height: 300,
    filebrowserUploadUrl: "admin/ckeditor/upload.php",
  } ); 
        

       </script> 
