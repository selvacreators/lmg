<!doctype html>
<html>
   <head>
      <style>
         body{margin:0; padding: 0;}
         .banner-color {
         background-color: #eb681f;
         }
         .title-color {
         color: #0066cc;
         }
         .button-color {
         background-color: #0066cc;
         }
         @media screen and (min-width: 500px) {
         .banner-color {
         background-color: #0066cc;
         }
         .title-color {
         color: #eb681f;
         }
         .button-color {
         background-color: #eb681f;
         }
         }
      </style>
   </head>
   <body>
      <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
         <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
            <tbody>
               <tr>
                  <td align="center">
                     <center style="width:100%">
                        <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:700px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif" width="700">
                           <tbody>
                              <tr>
                                 <td bgcolor="#F3F3F3" width="100%" style="background-color:#f3f3f3;padding:5px 10px;border-bottom:1px solid #ececec">
                                    <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="left" valign="middle" width="50%">
                                                <a target='_blank' href="{{ URL(Config::get('app.head_url'))}}">
                                                <img src="{{URL(Config::get('app.head_url').'logo/'.$csetting->store_logo)}}" width='347' style='outline:none;text-decoration:none;width:auto;max-width:100%;float:left;clear:both;display:block' align='left'></a>
                                             </td>
                                             <?php $date = date_create($getdata['mattressInfo']->created_at);?>
                                             <td valign="middle" width="50%" align="right" style="padding:0 0 0 10px">
                                                <span style="margin:0;
                                                color:#4c4c4c;
                                                white-space:normal;
                                                display:inline-block;
                                                text-decoration:none;
                                                font-size:12px;
                                                line-height:20px">{{date_format($date,"l d, F Y")}}</span>
                                             </td>
                                             <td width="1">&nbsp;</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center" bgcolor="#8BC34A" style="padding:10px 15px;color:#ffffff" class="banner-color">
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                               <tbody>
                                                                  <tr>
                                                                     <td align="center" width="100%">
                                                                        <h1 style="padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px">Thanks for completing the LMG Mattress Selector
                                                                           <sup style="font-size:8px;line-height:0;vertical-align:15px;margin-left:2px">TM</sup>
                                                                        </h1>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>    
                                                      <tr>
                                                         <td align="center" style="padding:10px 0 0px 0">
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                               <tbody>
                                                                  <tr>
                                                                     <td align="center" width="100%" style="padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;">
                                                                        <h3 style="font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: center;" class="title-color">Where to buy?</h3>
                                                                        <p style="margin: 12px 0 10px 0;font-size: 15px;text-align: center;">Find the retailer closest to you and get directions</p>
                                                                        <div style="font-weight: 200; text-align: center; margin: 25px;"><a target="_blank" style="padding:0.6em 1em;border-radius:600px;color:#ffffff;font-size:14px;text-decoration:none;font-weight:bold" class="button-color" href="{{URL(Config::get('app.head_url').$lang.'/stores')}}">Find a Stores</a></div>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                      
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="center" width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center" valign="middle" width="100%" style="border-top:1px solid #d9d9d9;padding:0px;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px">
                                                            <h3 style="font-weight: 600;padding: 20px 0 0 0;margin: 0px;font-size: 16px;line-height: 24px;text-align: center;" class="title-color">User Details and Requirements</h3>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td align="center" width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center" style="padding:0 0 8px 0" width="100%"></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="center" width="100%">
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;font-family:Helvetica,Arial,sans-serif">
                                                <tbody>
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Name
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['userInfo']['name']}}
                                                   </td>
                                                </tr>
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Email
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['userInfo']['email']}}
                                                   </td>
                                                </tr>
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Phone
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['userInfo']['phone']}}
                                                   </td>
                                                </tr>
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;border:none;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Message
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['userInfo']['message']}}
                                                   </td>
                                                </tr>
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px"><strong></strong>
                                                   </td>
                                                </tr>
                                                @if($getdata['mattressInfo']->gender != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Mattress for
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->gender}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->lookingmattress != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Looking for a new mattress
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->lookingmattress}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->sleep_guest != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Looking for - Big enough for 2 people
                                                   </td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->sleep_guest}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->weightrange != '' && $getdata['mattressInfo']->partner_weightrange == '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Weight Range</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->weightrange}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->agerange != '' && $getdata['mattressInfo']->partner_weightrange == '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Age Range</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->agerange}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->yourposition != '' && $getdata['mattressInfo']->partnerposition == '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:10px">Sleep Position</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:10px">{{$getdata['mattressInfo']->yourposition}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->agerange != '' && $getdata['mattressInfo']->partner_weightrange != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;border-right: 1px solid #e5e7e8;">Weight Range</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding: 0;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;font-family:Helvetica,Arial,sans-serif">
                                                         <tbody>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">You: {{$getdata['mattressInfo']->weightrange}}</td>
                                                            </tr>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;">You Partner: {{$getdata['mattressInfo']->partner_weightrange}}</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->weightrange != '' && $getdata['mattressInfo']->partner_agerange != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;border-right: 1px solid #e5e7e8;">Age Range</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding: 0;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;font-family:Helvetica,Arial,sans-serif">
                                                         <tbody>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">You: {{$getdata['mattressInfo']->agerange}}</td>
                                                            </tr>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;">You Partner: {{$getdata['mattressInfo']->partner_agerange}}</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->yourposition != '' && $getdata['mattressInfo']->partnerposition != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;border-right: 1px solid #e5e7e8;">Sleep Position</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding: 0;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;font-family:Helvetica,Arial,sans-serif">
                                                         <tbody>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">You: {{$getdata['mattressInfo']->yourposition}}</td>
                                                            </tr>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;">You Partner: {{$getdata['mattressInfo']->partnerposition}}</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->issues != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Aches / Pain</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->issues}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->hotcold != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">Temperature</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->hotcold}}</td>
                                                </tr>
                                                @endif
                                                @if($getdata['mattressInfo']->comfortablesleep != '')
                                                <tr style="vertical-align:top;text-align:left;padding:0; border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;border-right: 1px solid #e5e7e8;">Mattress Feeling</td>
                                                   <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding: 0;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding:0;font-family:Helvetica,Arial,sans-serif">
                                                         <tbody>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;border-bottom-width:1px;border-bottom-color:#e5e7e8;border-bottom-style:solid;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px">{{$getdata['mattressInfo']->comfor_table}}</td>
                                                            </tr>
                                                            <tr style="vertical-align:top;text-align:left;padding:0;" align="left">
                                                               <td align="left" valign="top" style="font-size:12px;color:#7ea2c3;word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;font-weight:normal;line-height:19px;width:auto!important;margin:0;padding:8px 10px;">{{$getdata['mattressInfo']->comfortablesleep}}</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                @endif
                                                </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td align="center" width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center" style="padding:0 0 8px 0" width="100%"></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td align="left">
                                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="right" width="100%">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;margin:0 auto;padding:0; width: 100%;">
                                                   <tbody>
                                                      <tr style="vertical-align:top;text-align:left;padding:0" align="left">
                                                         <td style="text-align:right;word-break:break-word;border-collapse:collapse!important;vertical-align:top;color:#003a70;font-family:Helvetica,Arial,sans-serif;font-weight:normal;line-height:19px;font-size:12px;margin:0;padding:0px 10px 0px 0px" align="right" valign="top">
                                                            <a href="{{URL()}}" style="color:#eb681f;font-size:13px;font-weight:normal;line-height:1.2em;text-decoration:none;" target="_blank" data-saferedirecturl="{{URL()}}">lmgworld.com</a><br><br>Copyright 2018 All rights reserved<br>
                                                            <a href="{{URL(Config::get('app.head_url').$lang.'/privacy-terms')}}" style="color:#003a70;text-decoration:underline;font-size:12px" target="_blank" data-saferedirecturl="{{URL(Config::get('app.head_url').$lang.'/privacy-terms')}}">Privacy policy</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL(Config::get('app.head_url').$lang.'/privacy-terms')}}" style="color:#003a70;text-decoration:underline;font-size:12px" target="_blank" data-saferedirecturl="{{URL(Config::get('app.head_url').$lang.'/privacy-terms')}}">Terms and conditions</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL(Config::get('app.head_url').$lang.'/contact')}}" style="color:#003a70;text-decoration:underline;font-size:12px" target="_blank" data-saferedirecturl="{{URL(Config::get('app.head_url').$lang.'/contact')}}">Contact Us</a>
                                                      </td>
                                                   </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td align="center" width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center" style="padding:0 0 8px 0" width="100%"></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </center>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </body>
</html>