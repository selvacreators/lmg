<div class="form-group row">
    <label for="Product_image" class="control-label col-sm-3">Image:</label>
    <div class="col-sm-4">
        <input type="file" name="Product_image" id="Product_image" class="form-control">
        <input type="hidden" name="InsertProduct_image" id="InsertProduct_image" class="form-control" value="{{$product['product']->prod_slider_image}}">
    </div>
    <div class="col-sm-4">
        @if($product['product']->prod_slider_image)
        <img src="{{Config::get('app.head_url')}}product/large/{{$product['product']->prod_slider_image}}" width="100" height="100">
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="sku" class="control-label col-sm-3">Sku:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="sku" id="sku" 
        value="{{$product['product']->sku}}">
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('sku').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="quantity" class="control-label col-sm-3">Quantity:</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="quantity" id="quantity" value="{{$product['product']->quantity}}">
    </div>
</div>
<!-- <div class="form-group row">
    <label for="stock_status" class="control-label col-sm-3">Stock Status:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="stock_status" class="form-control" id="stock_status">
            @if($productInfo->stock_status)
                @foreach($productInfo->stock_status as $ststatus)
                    <option {{ $ststatus->stock_status_id == $product['product']->stock_status_id ? 'selected' : '' }} value="{{$ststatus->stock_status_id}}">{{$ststatus->name}}</option>
                @endforeach
            @endif
        </select>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('stock_status').'</div>';?>
    </div>
</div> -->
<div class="form-group row">
    <label for="price" class="control-label col-sm-3">Price:</label>
    <div class="col-sm-9">
        <input type="text" name="price" id="price" class="form-control" autocomplete="off" value="{{$product['product']->price}}" />
    </div>
</div>
<div class="form-group row">
    <label for="date_available" class="control-label col-sm-3">Date Available:</label>
    <div class="col-sm-9">
        <input type="text" name="date_available" id="date_available" class="form-control datepicker" value="{{$product['product']->date_available}}" data-date-format="YYYY-MM-DD">
    </div>
</div>
<div class="form-group row">
    <label for="product_type" class="control-label col-sm-3">Product Type:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="product_type" class="form-control" id="product_type">
            <option value="">Select</option>
            @if($productInfo->product_type)
            @foreach($productInfo->product_type as $row)
            <option {{ $product['product']->ptype_id == $row->id ? 'selected="selected"' : '' }} value="{{$row->id}}">{{$row->ptype_name}}</option>
            @endforeach
            @endif
        </select>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('product_type').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="weight_class_id" class="col-sm-3 col-form-label">Weight:</label>
    <div class="col-sm-4">
        <select name="weight_class_id" class="form-control" id="weight_class_id">
            <option value="">Select</option>
            @if($productInfo->getweight)
                @foreach($productInfo->getweight as $weight)
                    <option {{ $product['product']->weight_class_id == $weight->weight_class_id ? 'selected="selected"' : '' }} value="{{$weight->weight_class_id}}">{{$weight->title}}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-4">
        <input type="text" name="weight" id="weight" class="form-control" placeholder="weight" value="{{round($product['product']->weight)}}">
    </div>
</div>
<div class="form-group row">
    <label for="length_class_id" class="col-sm-3 col-form-label">length:</label>
    <div class="col-sm-4">
        <select name="length_class_id" class="form-control" id="length_class_id">
            <option value="">Select</option>
            @if($productInfo->getlength)
                @foreach($productInfo->getlength as $lengt)
                    <option {{ $product['product']->length_class_id == $lengt->length_class_id ? 'selected="selected"' : '' }} value="{{$lengt->length_class_id}}">{{$lengt->title}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="width" class="col-sm-3 col-form-label"></label>
    <div class="col-sm-3">
        <input type="text" name="length" id="length" class="form-control" placeholder="length" value="{{round($product['product']->length)}}">
    </div>
    <div class="col-sm-3">
        <input type="text" name="width" value="{{round($product['product']->width)}}" id="width" class="form-control" placeholder="Width">
    </div>
    <div class="col-sm-3">
        <input type="text" name="height" value="{{round($product['product']->height)}}" id="height" class="form-control" placeholder="height">
    </div>
</div>
<div class="form-group row">
    <label for="comfort" class="control-label col-sm-3">Comfort:</label>
    <div class="col-sm-9">
        <select name="comfort" class="form-control" id="comfort">
          <option value="">Select</option>
          <option value="firm" <?php if($product['product']->comfort =='firm'){ echo "selected";} ?>>Firm</option>
          <option value="plush"  <?php if($product['product']->comfort =='plush'){ echo "selected";} ?>>Plush</option>
          <option value="medium" <?php if($product['product']->comfort =='medium'){ echo "selected";} ?>>Medium</option>
      </select>
  </div>
</div>
<div class="form-group row">
    <label for="subtract" class="control-label col-sm-3">Subtract:</label>
    <div class="col-sm-9">
        <select name="subtract" class="form-control" id="subtract">
            <option value="">Select</option>
            <option value="1" <?php if($product['product']->subtract =='1'){ echo "selected";} ?>>1</option>
            <option value="2" <?php if($product['product']->subtract =='2'){ echo "selected";} ?>>2</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="sort_order" class="control-label col-sm-3">Sort Order:</label>
    <div class="col-sm-9">
        <input type="text" name="sort_order" id="sort_order" class="form-control" value="{{$product['product']->sort_order}}">
    </div>
</div>
<div class="form-group row">
    <label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="status" class="form-control" id="status">
            <option value="">Select</option>
            <option value="1" <?php if($product['product']->status =='1'){ echo "selected";} ?>>Enable</option>
            <option value="2" <?php if($product['product']->status =='2'){ echo "selected";} ?>>Disable</option>
        </select>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('status').'</div>';?>
    </div>
</div>