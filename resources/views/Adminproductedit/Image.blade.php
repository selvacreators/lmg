<div class="row">
    <div id="img-row" class="col-sm-12">
        <table class="table" width="50%">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Sort Order</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($product['product_images_gallery'] as $pimages)        
            <tr>
                <td><img src="{{Config::get('app.head_url')}}product/thumb/{{$pimages->image}}" width="100" class="img-responsive"><input type="hidden" name="Insert_image_gallery[]" id="Insert_image_gallery" class="form-control" value="{{$pimages->image}}"></td>
                <td>{{$pimages->sort_order}}<input type="hidden" name="Insert_sort_order[]" id="Insert_sort_order" class="form-control" value="{{$pimages->sort_order}}"></td>
                <td><button type="button" data-id="{{$pimages->product_image_id}}" class="btn btn-danger imgDel"><i class="fa fa-trash"></i></button></td>
            </tr>
            @endforeach
            </tbody>
            <tfoot></tfoot>
        </table>
    </div>
    <div class="col-md-12">
        <div class="form-group row">
            <div class="col-sm-12">
                <div id="uploader">
                    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                </div>
                <input id="upload_image" type="hidden" value="" name="upload_image">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
{{-- <div class="row multi" id="multi">
    <div class="col-md-10">
        <div class="col-md-6">
            <table class="table Imageorder-list">
            </table>
        </div>
    </div>
</div> --}}