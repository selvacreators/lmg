@foreach($product['product_attibute'] as $key => $prod_attri)
<div class="row">
    <div class="col-md-12">
        <div class="attribute">
            <div class="col-sm-6">
                <div class="form-group row">
                    <label for="attribute_id" class="control-label col-sm-3">Attribute:</label>
                    <div class="col-sm-9">
                        <select name="attribute_id[]" class="form-control" id="attribute_id">
                            <option value="">Select</option>
                            @foreach($productInfo->attribute as $attr)
                                @if($prod_attri->attribute_id == $attr->attribute_id)
                                    <option value="{{$attr->attribute_id}}" selected>{{$attr->name}}</option>
                                @else
                                    <option value="{{$attr->attribute_id}}">{{$attr->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                    <label for="text" class="control-label col-sm-3">Text:<span class="note">*</span></label>
                    <div class="col-sm-9">
                        <textarea cols="10" rows="3" name="text[]" id="text" class="form-control">{{$prod_attri->text}}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-12"><button type="button" class="attrDel btn btn-sm btn-danger"><i class="fa fa-trash"></i><div class="ripple-container"></div></button></div>
        </div>
    </div>
</div>
@endforeach
<div class="row multi" id="multi_attr_list">
    <div class="col-md-12 attribute-list"></div>
</div>
<div class="row multi" id="multi_attr_btn">
    <div class="col-sm-6">
        <div class="form-group row">
            <div class="col-sm-9">
                <input type="button" id="addattri" value="Add Attribute" class="btn btn-info btn-daimler"><!-- btn-daimler -->
            </div>
        </div>
    </div>
</div>