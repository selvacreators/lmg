@extends('layouts.layout_admin')

@section('admin-style')

<link href="{{ Config::get('app.head_url') }}assets/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />

<link href="{{ Config::get('app.head_url') }}assets/css/demo.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}admin/plupload/jquery.plupload.queue.css" type="text/css" media="screen">
<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}
.tab-pane .table tbody > tr > td:first-child {
  width: 95px !important;
}
.form-group row input[type=file] {
 position: absolute;
 top: 35px;
 right: 0;
 cursor: pointer;
 filter: alpha(opacity=0);
 opacity: 1;
 direction: ltr;
 z-index: 1;
}
#ajax-loader{
  position: fixed;
  top: 50px;    
  width: 100%;
  height: 100%;
  background: rgba(0,0,0,0.3);
  z-index: 9999;
  text-align:center;
}
#column-left + div #ajax-loader{
  left:50px;
}
#column-left.active + div #ajax-loader{
  left: 250px;
}
.spinner{
  border: 5px solid #f3f3f3;
  -webkit-animation: spin 1s linear infinite;
  animation: spin 1s linear infinite;
  border-top: 5px solid #555;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  position: absolute;
  top:50%;
  left:40%;
}
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

@endsection
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            {!! Form::open(array('url' => '/Admin/edit_Product','class'=>'form-horizontal','method'=>'post','enctype'=>'multipart/form-data')) !!}
            <input name="_token" type="hidden" value="{{csrf_token()}}">
            <input name="txt_product_id" type="hidden" value="{{$product['product']->product_id}}">
            <div class="card">
              <div class="card-header">
                <a href="/Admin/product" class="btn btn-primary btn-sm pull-right">Back</a>
                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                <h3 class="card-title">Edit Product</h3>
              </div>
              <div class="card-body">
                <nav class="nav-container">
                  <ul class='nav nav-tabs' role="tablist">
                    <li><a data-toggle="tab" class="active show" href="#Product">Product</a></li>
                    <li><a data-toggle="tab" href="#Data">Data</a></li>
                    <li><a data-toggle="tab" href="#Link">Link</a></li>
                    <li><a data-toggle="tab" href="#Attribute">Attribute</a></li>
                    <li><a data-toggle="tab" href="#Image">Image</a></li>
                    <li><a data-toggle="tab" href="#Features">Features</a></li>
                  </ul>
                </nav>
                <div class="tab-content">
                  <div id="Product" class="tab-pane fade active in">
                    <div id="ajax-loader"  style="display: none;">
                      <div class="spinner"></div>
                    </div>
                    @include('Adminproductedit.product_form')
                  </div>
                  <div id="Data" class="tab-pane fade">
                    @include('Adminproductedit.data')     
                  </div>
                  <!-- Product Data Tab End-->
                  <!-- Product Attribute start Here-->
                  <div id="Link" class="tab-pane fade">
                   @include('Adminproductedit.link')                
                 </div>
                 <!-- Product Attribute End Here-->
                  <!-- Product Attribute start Here-->
                  <div id="Attribute" class="tab-pane fade">
                   @include('Adminproductedit.attribute')                
                 </div>
                 <!-- Product Attribute End Here-->
                 <!-- Procut Image Start Here -->
                 <div id="Image" class="tab-pane fade">
                   @include('Adminproductedit.Image')           
                 </div>
                 <!-- Product Image end Here -->
                 <!-- Product Features end Here -->
                 <div id="Features" class="tab-pane fade">
                   @include('Adminproductedit.features')           
                 </div>
                 <!-- Product Image end Here -->
                </div>
              </div>
            </div>
            {!! Form::close() !!}
            <!-- Tab panes -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('date-script')
<script src="{{ Config::get('app.head_url') }}assets/js/moment.min.js"></script>
<script src="{{ Config::get('app.head_url') }}assets/js/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('admin-script')

<script type="text/javascript">
   $(document).ready(function() {
      md.initFormExtendedDatetimepickers();
    });
</script>

<script src="{{ Config::get('app.head_url') }}admin/ckeditor/ckeditor.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
  };
</script>


<script type="text/javascript">

CKEDITOR.replace('short_desc', options);
CKEDITOR.replace('desc', options);
CKEDITOR.config.allowedContent = true;

$(document).ready(function() {
// .features
  $("body").on("click", ".feaDelele", function (event) {

    $(this).closest("div.features").remove();
    var id = $(this).data('id');
    if(id != undefined && id != ''){
      $.post('/Admin/deleteprodFeatimg', {
          _token: "{{ csrf_token() }}",
          id:id
      }, function(data) {
          console.log(data);
      }, 'json');
    }
  });
  $("body").on("click", ".imgDel", function (event) {
    $(this).closest("tr").remove();
    var id = $(this).data('id');
    $.post('/Admin/deleteprodsubimg', {
        _token: "{{ csrf_token() }}",
        id:id
    }, function(data) {
        console.log(data);
    }, 'json');
  });
  $("#Attribute").on("click", ".attrDel", function (event) {
    // $(this).closest("tr").remove();
    $(this).closest("div.attribute").remove();
    var id = $(this).data('id');
    $.post('/Admin/deleteprodAttrimg', {
        _token: "{{ csrf_token() }}",
        id:id
    }, function(data) {
        // console.log(data);
    }, 'json');
  });
  var rowNum = 0;
  $(".fea_desc").each(function(){
    CKEDITOR.replace('feat_des_'+rowNum, options);
    CKEDITOR.config.allowedContent = true;
    rowNum++;
  });
  $("#addfeature").click(function() {
  // $(".addfeature").on("click", function() {
    rowNum++;
    var newRow = $("<div class='features'>");
    var cols = "";
    cols += '<div class="form-group row"><label for="heading" class="control-label col-sm-3">Content Heading:</label><div class="col-sm-9"><input type="text" name="heading[]" id="heading" class="form-control" autocomplete="off"/></div></div><div class="form-group row"><label for="Feat_image_or_video" class="col-sm-3 control-label">Feature Images/Videos:</label><div class="col-sm-4"><input type="file" name="Feat_image_or_video[]" id="Feat_image_or_video" class="form-control" size=30"></div></div><div class="form-group row"><label for="Feat_desc" class="control-label col-sm-3">Feature Content:</label><div class="col-sm-9"><textarea cols="10" rows="5" name="Feat_desc[]" id="feat_des_'+rowNum+'" class="form-control fea_desc"></textarea></div></div><div class="form-group row"><label for="fsort_order" class="control-label col-sm-3">Feature Sort Order:</label><div class="col-sm-9"><input type="text" name="fsort_order[]" id="fsort_order" class="form-control" autocomplete"off"/></div></div><div class="form-group row bmd-form-group"><button type="button" class="btn btn-danger feaDelele"><i class="fa fa-trash"></i></button></div>';
    newRow.append(cols);
    $(".feature-list").append(newRow);
    CKEDITOR.replace('feat_des_'+rowNum, options);
    CKEDITOR.config.allowedContent = true;
  });
  
  $('#sku').blur(function() {
    var getsku = $(this).val();
    var tokens = $('#tokens').val();
    $.ajax({
      type:'GET',
      url:"{{URL('/Admin/checksku')}}",
      data:{'_token':tokens,'getsku':getsku},
      success:function(res){
        //console.log(res);
        if(res == 1){
          $('#error').html('Wrong Sku already Exits.');
          $('#sku').focus();
          $('#btnadd').attr("disabled", "disabled");
        }
        else{
          $('#error').html('');
          $('#btnadd').removeAttr("disabled");; 
        }
      }
    });
  });
  var counter = 0;
  $("#addattri").on("click", function()
  {
    var attribute_id = $('#attribute_id').val();
    var newRow = $("<div class='attribute'>");
    var cols = "";
    cols += '<div class="col-sm-6"><div class="form-group row"><label for="attribute_id" class="control-label col-sm-3">Attribute:</label><div class="col-sm-9"><select name="attribute_id[]" class="form-control" id="attribute_id"><option value="">Select</option>@foreach($productInfo->attribute as $attr)<option value="{{$attr->attribute_id}}">{{$attr->name}}</option>@endforeach</select></div></div></div><div class="col-sm-6"><div class="form-group row"><label for="text" class="control-label col-sm-3">Text:</label><div class="col-sm-9"><textarea cols="10" rows="3" name="text[]" id="text" class="form-control"></textarea></div></div></div><div class="col-md-12"><button type="button" class="attrDel btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></div>';
    newRow.append(cols);
    $(".attribute-list").append(newRow);
  });
  // $("#addimages").on("click", function () {
  //   var newRow = $("<tr class='vinrow'>");
  //   var cols = "";
  //   cols += '<td><input required type="file" id="image" class="form-control odom" name="image[]" style="width: 320px;"></td>';
  //   cols += '<td><input required type="text" id="Imagesort_order" class="form-control odom" name="Imagesort_order[]" style="width: 320px;"></td>';
  //   cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger" value="-"></td>';
  //   newRow.append(cols);
  //   $("table.Imageorder-list").append(newRow);
  //   counter++;
  //   var count =0;
  //   $('.txtecount').each(function(){
  //     if($(this).val() !=""){
  //     count++;
  //     }
  //   });
  // });
  // $("table.Imageorder-list").on("click", ".ibtnDel", function (event) {
  //   $(this).closest("tr").remove();       
  //   counter -= 1
  //   var count =0;
  // });
});
</script>
<script type="text/javascript" src="{{ Config::get('app.head_url') }}admin/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="{{ Config::get('app.head_url') }}admin/plupload/jquery.plupload.queue/jquery.plupload.queue.min.js"></script>
<script type="text/javascript">
// Convert divs to queue widgets when the DOM is ready
$(function(){
    $("#language").change(function(){
      var lang = $(this).val(), ajxLoader = $("#ajax-loader");
      var pro_id = $("input[name='txt_product_id']").val();
      $.ajax({
        url: '/Admin/getProductByLang?lang='+lang+'&p='+pro_id,
        // url: '/Admin/get-product-info?lang='+lang+'&p='+pro_id,
        type: 'get',
        dataType: 'json',
        beforeSend: function(){ ajxLoader.show(); },
        success:function(result){

          // $('#rproduct,#oproduct,#brand,#collection_id,#categories_id,#weight_class_id,#length_class_id,#store_id,#product_type,#attribute_id').empty();
          // $('#brand,#collection_id,#product_type,#attribute_id').append('<option value="">Select</option>');
          // $(".attribute-list").html('');

          console.log(result);
          var oalldata = jQuery.parseJSON(result.allData);

          var select;

          // if(oalldata.product_type && result.data)
          // {
          //   $.each( oalldata.product_type,function( key, value )
          //   {
          //     if(oalldata.id == value.brand_id)
          //     {
          //       select = 'selected';
          //     $('#brand_id').append('<option '+select+' value="'+ value.brand_id +'">'+ value.brand_name +'</option>');
          //     }
          //     else
          //     {
          //       console.log(3);
          //     }
          //   });
          // }
        },
        complete:function(){ ajxLoader.hide(); }
      });
    });

    // function populateFormData(response) {
    //   $("#product_name").val(response.name);
    //   $("#product_slug").val(response.product_slug);
    //   $("#short_desc").val(response.short_desc);
    //   $("#desc").val(response.desc);

    //   CKEDITOR.instances.short_desc.setData(response.short_desc); 
    //   CKEDITOR.instances.desc.setData(response.desc); 

    //   $("#meta_title").val(response.meta_title);
    //   $("#meta_keyword").val(response.meta_keyword);
    //   $("#meta_desc").val(response.meta_desc);
    //   $("#tag").val(response.tag);

    //  var obj1 = JSON.parse(response.service);

    //  $('#service_textarea').val(obj1.one.text);
    //  $('#service_text2').val(obj1.two.text);
    //  $('#service_text3').val(obj1.three.text);

    //  $('#image_one').hide();
    //  $('#image_two').hide();
    //  $('#image_three').hide();

    //  $('#firstdiv').append('<img src="{{Config::get('app.head_url')}}product/service_img/' + obj1.one.image + '" width="80" height="80"/>');
    //  $('#seconddiv').append('<img src="{{Config::get('app.head_url')}}product/service_img/' + obj1.two.image + '" width="80" height="80"/>');
    //  $('#thirdiv').append('<img src="{{Config::get('app.head_url')}}product/service_img/' + obj1.three.image + '" width="80" height="80"/>');

    //  $('#insert_service_image_text1').val(obj1.one.image);
    //  $('#insert_service_image_text2').val(obj1.two.image);
    //  $('#insert_service_image_text3').val(obj1.three.image);
    // }

    $("#uploader").pluploadQueue({
    // General settings
    runtimes : 'html5,flash,silverlight,html4',
    url : '/Admin/products/uploadImage_ajax?diretorio=large',
    //url : 'http://localhost/completed/sgihair/admin/products/uploadImage_ajax',
    chunk_size : '10mb',
    unique_names : true,
    //resize : {width : 109, height : 73, crop: true},
    
    filters : {
        max_file_size : '10mb',
        mime_types: [
        {title : "Image files", extensions : "jpg,gif,png"}
        ]
    },
    flash_swf_url : '/plupload/js/Moxie.swf',
    silverlight_xap_url : '/plupload/js/Moxie.xap',
    preinit : {
        Init: function(up, info) {
            log('[Init]', 'Info:', info, 'Features:', up.features);
        },

        UploadFile: function(up, file) {
            log('[UploadFile]', file);
        }
    },
    init : {
        PostInit: function() {
            log('[PostInit]');
            /*document.getElementById('uploadfiles').onclick = function() {
                uploader.start();
                return false;
            };*/
        },

        Browse: function(up) {
            // Called when file picker is clicked
            log('[Browse]');
        },

        Refresh: function(up) {
            // Called when the position or dimensions of the picker change
            log('[Refresh]');
        },

        StateChanged: function(up) {
            // Called when the state of the queue is changed
            log('[StateChanged]', up.state == plupload.STARTED ? "STARTED" : "STOPPED");
        },

        QueueChanged: function(up) {

            if(up.state == 1){
               $('input[type="submit"]').prop('disabled', true);
               $('.plupload_start').addClass('upload_file');
           }
           else if(up.state == 2){
               $('input[type="submit"]').prop('disabled', false);
               $('.plupload_start').removeClass('upload_file');
           }
            // Called when queue is changed by adding or removing files
            log('[QueueChanged]');
        },

        OptionChanged: function(up, name, value, oldValue) {
            // Called when one of the configuration options is changed
            log('[OptionChanged]', 'Option Name: ', name, 'Value: ', value, 'Old Value: ', oldValue);
        },

        BeforeUpload: function(up, file) {
            console.log(file);
            // Called right before the upload for a given file starts, can be used to cancel it if required
            if(!('thumb' in file)){
                up.settings.url = '/Admin/products/uploadImage_ajax?diretorio=thumb&newName='+file.id;

            }else{
                up.settings.url = '/Admin/products/uploadImage_ajax?diretorio=small&newName='+file.id
            }  

            //log('[BeforeUpload]', 'File: ', file);
            console.log(up.settings.resize);
        },

        UploadProgress: function(up, file) {
            // Called while file is being uploaded
            log('[UploadProgress]', 'File:', file, "Total:", up.total);
        },

        FileFiltered: function(up, file) {
            // Called when file successfully files all the filters
            log('[FileFiltered]', 'File:', file);
        },

        FilesAdded: function(up, files) {
            // Called when files are added to queue
            log('[FilesAdded]');

            plupload.each(files, function(file) {
                log('  File:', file);
            });
        },

        FilesRemoved: function(up, files) {
            // Called when files are removed from queue
            log('[FilesRemoved]');

            plupload.each(files, function(file) {
                log('  File:', file);
            });
        },

        FileUploaded: function(up, file, info) {
            // Called when file has finished uploading
            if(!('thumb' in file)) {
                file.thumb = true;
                file.loaded = 0;
                file.percent = 0;
                file.status = plupload.QUEUED;
                up.trigger("QueueChanged");
                up.refresh();
            }

            log('[FileUploaded] File:', file, "Info:", info);
        },

        ChunkUploaded: function(up, file, info) {
            // Called when file chunk has finished uploading
            log('[ChunkUploaded] File:', file, "Info:", info);
        },

        UploadComplete: function(up, files) {
            
            var json_text = '';
            var arr = [];
            
            var htmlData = '';
            $(files).each(function(i, v) {
                arr[i] = v.target_name;
                htmlData += '<tr><td><img src="{{Config::get('app.head_url')}}product//thumb/'+v.target_name+'" class="img-responsive"></td><td><input type="text" style="width:250px" name="Imagesort_order[]" class="form-control" autocomplete="off" value="0" /><input type="hidden" name="ImageList[]" value="'+v.target_name+'" /></td><td><button type="button" class="btn btn-danger imgDel"><i class="fa fa-trash"></i></button></td></tr>';
            });
            json_text = JSON.stringify(arr);
            $("#upload_image").val(json_text);
            $("#img-row table tfoot").append(htmlData);

            // Called when all files are either uploaded or failed
            log('[UploadComplete]');
        },

        Destroy: function(up) {
            // Called when uploader is destroyed
            log('[Destroy] ');
        },

        Error: function(up, args) {
            // Called when error occurs
            log('[Error] ', args);
        }
    }
});

function log() {
    var str = "";

    plupload.each(arguments, function(arg) {
        var row = "";

        if (typeof(arg) != "string") {
            plupload.each(arg, function(value, key) {
                // Convert items in File objects to human readable form
                if (arg instanceof plupload.File) {
                    // Convert status to human readable
                    switch (value) {
                        case plupload.QUEUED:
                        value = 'QUEUED';
                        break;

                        case plupload.UPLOADING:
                        value = 'UPLOADING';
                        break;

                        case plupload.FAILED:
                        value = 'FAILED';
                        break;

                        case plupload.DONE:
                        value = 'DONE';
                        break;
                    }
                }

                if (typeof(value) != "function") {
                    row += (row ? ', ' : '') + key + '=' + value;
                }
            });

            str += row + " ";
        } else {
            str += arg + " ";
        }
    });
    var log = $('#log');
    log.append(str + "\n");
    //log.scrollTop(log[0].scrollHeight);
}
});
</script>
@endsection