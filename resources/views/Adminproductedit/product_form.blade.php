<div class="form-group row">
    <label for="language" class="col-sm-3 control-label">Language:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="language" class="form-control" id="language" value="{{ old('language') }}">
            <option value="">Select</option>
            @if($storeLang)
            @foreach($storeLang as $lang)
            <option {{ $product['product_description']->lang_code == $lang->lang_code ? 'selected="selected"' : '' }} value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
            @endforeach
            @endif
        </select>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('language').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="product_name" class="col-sm-3 control-label">Product Name:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="product_name" id="product_name" value="{{$product['product_description']->name}}" class="form-control"/>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('product_name').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-3 control-label">Product Slug:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="product_slug" id="product_slug" class="form-control" value="{{$product['product_description']->product_slug}}"/>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('product_slug').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-3 control-label">Short Description:</label>
     <div class="clearfix"></div>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="short_desc" id="short_desc" class="form-control">{{$product['product_description']->short_desc}}</textarea>
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-3 control-label">Product Description:</label>
    <div class="clearfix"></div>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="desc" id="desc" class="form-control">{{$product['product_description']->desc}}</textarea>
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-3 control-label">Meta Title:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{$product['product_description']->meta_title}}"/>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('meta_title').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-3 control-label">Meta Keyword:</label>
    <div class="col-sm-9">
        <input type="text" name="meta_keyword" id="meta_keyword" class="form-control" value="{{$product['product_description']->meta_keyword}}" />
    </div>
</div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-3 control-label">Meta Description:</label>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="meta_desc" id="meta_desc" class="form-control">{{$product['product_description']->meta_desc}}</textarea>
    </div>
</div>
<fieldset class="form-group">
<?php
$service  = json_decode($product['product_description']->service);
?>
<legend><h4>Features Icon:</h4></legend>
<div class="col-md-4">
    <div class="row">
        <div class="col-sm-12" id="firstdiv">
            <label for="Image" class="control-label">Icon Image:</label>
            <input type="file" name="service_image[one]" value="" id="service_image1" class="form-control">
            <input type="hidden"  name="insert_service_image[one]" value="<?=(isset($service->one->image))?$service->one->image:null?>" id="insert_service_image_text1" class="form-control">
            @if(!empty($service->one->image))
            <img src="{{Config::get('app.head_url')}}product/service_img/<?=(isset($service->one->image))?$service->one->image:null?>" id="image_one" width="80" height="80"/>
            @endif
        </div>
        <div class="col-sm-12">
            <label for="Image" class="control-label">Icon Text:</label>
            <textarea type="text" name="service_text[one]" id="service_textarea" class="form-control" row="5"><?php echo (isset($service->one->text))?$service->one->text:null; ?></textarea>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-sm-12" id="seconddiv">
            <label for="Image" class="control-label">Icon Image:</label>
            <input type="file" name="service_image[two]" value="" id="service_image1" class="form-control">
            <input type="hidden"  name="insert_service_image[two]" value="<?=(isset($service->two->image))?$service->two->image:null?>" id="insert_service_image_text2" class="form-control">
            @if(!empty($service->two->image))
            <img src="{{Config::get('app.head_url')}}product/service_img/<?=(isset($service->two->image))?$service->two->image:null?>" id="image_two" width="80" height="80"/>
            @endif
        </div>
        <div class="col-sm-12">
            <label for="Image" class="control-label">Icon Text:</label>
            <textarea type="text" name="service_text[two]" id="service_text2" class="form-control" row="5"><?php echo (isset($service->two->text))?$service->two->text:null; ?></textarea>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-sm-12" id="thirdiv">
            <label for="Image" class="control-label">Icon Image:</label>
            <input type="file" name="service_image[three]" value="" id="service_image1" class="form-control">
            <input type="hidden"  name="insert_service_image[three]" value="<?=(isset($service->three->image))?$service->three->image:null?>" id="insert_service_image_text3" class="form-control">
            @if(!empty($service->three->image))
            <img src="{{Config::get('app.head_url')}}product/service_img/<?=(isset($service->three->image))?$service->three->image:null?>" id="image_three" width="80" height="80"/>
            @endif
        </div>
        <div class="col-sm-12">
            <label for="Image" class="control-label">Icon Text:</label>
            <textarea type="text" name="service_text[three]" id="service_text3" class="form-control" row="5"><?php echo (isset($service->three->text))?$service->three->text:null; ?></textarea>
        </div>
    </div>
</div>
</fieldset>