@foreach($product['product_features'] as $key =>  $product_featur)
@if(!empty($product_featur))
<div class="row">
    <div class="col-md-12 product-forms">
        <div class="features">
            <div class="form-group row">
                <label for="heading" class="control-label col-sm-3">Content Heading:</label>
                <div class="col-sm-9">
                    <input type="text" name="heading[]" id="heading" value="{{$product_featur->name}}" class="form-control" autocomplete="off"/>
                    <input type="hidden" name="pfeature_id[]" id="pfeature_id" value="{{$product_featur->product_feature_id}}" class="form-control" autocomplete="off"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="Feat_image_or_video" class="col-sm-3 control-label">Feature Images/Videos:</label>
                <div class="col-sm-4">
                    <input type="file" name="Feat_image_or_video[]">
                    <input type="hidden" name="oldFeat_image_video[]" id="oldFeat_image" class="form-control" value="{{$product_featur->prod_img_or_vdo}}">
                </div>
                <div class="col-sm-4">
                    @if($product_featur->prod_img_or_vdo)
                    <img src="{{Config::get('app.head_url')}}product/features/{{$product_featur->prod_img_or_vdo}}" height="100" width="100">
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Feat_desc" class="control-label col-sm-3">Feature Content:</label>
                <div class="col-sm-9">
                    <textarea cols="10" rows="5" name="Feat_desc[]" id="feat_des_<?=$key++;?>" class="form-control fea_desc">{{$product_featur->prod_des}}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="fsort_order" class="control-label col-sm-3">Feature Sort Order:</label>
                <div class="col-sm-9">
                    <input type="text" name="fsort_order[]" id="fsort_order" value="{{$product_featur->sort_order}}" class="form-control" autocomplete="off"/>
                </div>
            </div>
            <div class="form-group row bmd-form-group"><button type="button" data-id="{{$product_featur->product_feature_id}}" class="btn btn-danger feaDelele"><i class="fa fa-trash"></i><div class="ripple-container"></div></button></div>
        </div>
    </div>
</div>
@else
<div class="features"></div>

@endif
@endforeach
<div class="row multi">
    <div class="col-md-12 feature-list"></div>
</div>
<div class="row multi">
    <div class="col-md-12">
        <div class="form-group">
            <input type="button" id="addfeature" value="Add Feature" class="btn btn-info btn-daimler"><!-- btn-daimler -->
        </div>
    </div>
</div>