@if($productInfo->store_location)
@if(sizeof($product['product_to_store']))
@foreach($product['product_to_store'] as $psvalue)
  <?php $store_id[] =  $psvalue->store_id; ?>
@endforeach
<div class="form-group row">
    <label for="store_id" class="control-label col-sm-3">Store Locator:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="store_id[]" id="store_id">
            @if($productInfo->store_location)
                @foreach($productInfo->store_location as $store_loca)
                    @if(in_array($store_loca->store_id,$store_id))
                        <option value="{{$store_loca->store_id}}" selected>{{$store_loca->store_name}}</option>
                    @else
                        <option value="{{$store_loca->store_id}}">{{$store_loca->store_name}}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
</div>
@else
<div class="form-group row">
    <label for="store_id" class="control-label col-sm-3">Store Locator:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="store_id[]" id="store_id">
            @foreach($productInfo->store_location as $store_loca)
            <option value="{{$store_loca->store_id}}">{{$store_loca->store_name}}</option>
            @endforeach
        </select>
    </div>
</div>
@endif
@endif
<div class="form-group row">
    <label for="brand" class="control-label col-sm-3">Brand:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="brand" class="form-control" id="brand">
            <option value="">Select</option>
            @if($productInfo->brands)
              @foreach($productInfo->brands as $brand)
                <option {{ $product['product']->brand_id == $brand->brand_id ? 'selected' : '' }} value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
              @endforeach
            @endif
        </select>
      <?php echo '<div class="text-danger">'.$errors->product_errors->first('brand').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="collection_id" class="control-label col-sm-3">Collection:</label>
    <div class="col-sm-9">
        <select class="selectpicker form-control" title="Choose City" data-size="7" name="collection_id" id="collection_id">
            <option value="">Select</option>
            @if(!empty($product['product_collections']))
                @foreach($productInfo->collection as $col)
                    <option {{ $product['product_collections']->collection_id == $col->collection_id ? 'selected' : '' }} value="{{$col->collection_id}}">{{$col->collection_name}}</option>
                @endforeach
            @else
                @foreach($productInfo->collection as $col)
                    <option {{$col->collection_id}} value="{{$col->collection_id}}">{{$col->collection_name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
@if(sizeof($product['product_category']))
@foreach($product['product_category'] as $pcvalue)
  <?php $cate_id[] =  $pcvalue->categories_id;?>
@endforeach
<div class="form-group row">
    <label for="Image" class="control-label col-sm-3">Categories:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="categories_id[]" id="categories_id">
            @if($productInfo->categories)
                @foreach($productInfo->categories as $cate)
                    @if(in_array($cate->categories_id,$cate_id))
                        <option value="{{$cate->categories_id}}" selected>{{$cate->category_name}}</option>
                    @else                            
                        <option value="{{$cate->categories_id}}">{{$cate->category_name}}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
</div>
@else
<div class="form-group row">
    <label for="Image" class="control-label col-sm-3">Categories:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="categories_id[]" id="categories_id">
            @if($productInfo->categories)
                @foreach($productInfo->categories as $cate)
                    <option value="{{$cate->categories_id}}">{{$cate->category_name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
@endif

<!-- Related Product -->

@if(sizeof($product['related_products']))
@foreach($product['related_products'] as $rel_product)
  <?php $rproduct_id[] =  $rel_product->rproduct_id;?>
@endforeach
<div class="form-group row">
    <label for="product_id" class="control-label col-sm-3">Related Product:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose Related Product" data-size="7" name="rproduct[]" id="rproduct">
            @foreach($productInfo->ProductDesc as $pro)
                @if(in_array($pro->product_id,$rproduct_id))
                    <option value="{{$pro->product_id}}" selected>{{$pro->name}}</option>
                @else
                    <option value="{{$pro->product_id}}">{{$pro->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
@else
<div class="form-group row">
    <label for="store_id" class="control-label col-sm-3">Related Product:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose Related Product" data-size="7" name="rproduct[]" id="rproduct">
            <option value="">Select</option> 
            @foreach($productInfo->ProductDesc as $pro)
            <option value="{{$pro->product_id}}">{{$pro->name}}</option>
            @endforeach
        </select>
    </div>
</div>
@endif

<!-- Related Product -->


<!-- Other Product -->

@if(sizeof($product['other_products']))
@foreach($product['other_products'] as $otherproduct)
  <?php $oproduct_id[] = $otherproduct->oproduct_id;?>
@endforeach
<div class="form-group row">
    <label for="product_id" class="control-label col-sm-3">Other Product:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose Other Product" data-size="7" name="oproduct[]" id="oproduct">
        @foreach($productInfo->ProductDesc as $other_pro)
            @if(in_array($other_pro->product_id,$oproduct_id))
                <option value="{{$other_pro->product_id}}" selected="selected">{{$other_pro->name}}</option>
            @else                            
                <option value="{{$other_pro->product_id}}">{{$other_pro->name}}</option>
            @endif
        @endforeach
        </select>
    </div>
</div>
@else
<div class="form-group row">
    <label for="store_id" class="control-label col-sm-3">Other Product:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose Other Product" data-size="7" name="oproduct[]" id="oproduct">
            @foreach($productInfo->ProductDesc as $pro)
            <option value="{{$pro->product_id}}">{{$pro->name}}</option>
            @endforeach
        </select>
    </div>
</div>
@endif
<!-- Other Product -->
<div class="form-group row">
    <label for="tag" class="col-sm-3 control-label">Product Tag:</label>
    <div class="col-sm-9">
        <input type="text" name="tag" id="tag" value="{{$product['product_description']->tag}}" class="form-control"/>
    </div>
</div>