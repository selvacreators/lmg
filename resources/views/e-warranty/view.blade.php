@extends('layouts.layout_home')
@section('style')
<?php $locale = url(Session::get('locale'));?>
<link rel="stylesheet" type="text/css" href="{{ Config::get('app.head_url') }}frontend/css/bootstrap-datepicker.css">
@endsection
@section('content')

@include('includes.header')
@include('includes.breadcrumb')
<div class="page-title-wrapper pt40">
	<div class="container">
		<div class="row">
			<div class="text-center">
				<div class="content-heading">
					<h1 class="title">Mattress E-Warranty</h1>
				</div>
			</div>
		</div>
	</div>
</div>	
<main class="content-wrapper">
	<div class="page-content">
		<section class="e-warranty pt60 pb60">
			<div class="container">
				<div class="row">
					@if(Session::has('ewarranty_msg'))
                    <div class="alert alert-success">{{Session::get('ewarranty_msg')}}</div>
                    @endif
					<form method="post" autocomplete="off" name="myForm" id="ewarranty" class="form ewarranty-form" action="{{ URL('e-warranty-info')}}">
						<div class="col-sm-6 second-col">						  
                         	<div class="row mb15">
                         		<div class="pl15 pr15">
                         			<h4>Personal Information</h4>
                         			<p class="smallfont">Note:* Please ensure correct information for valid warranty</p>
                         		</div>
                         		<div class="form-group">
                         			<div class="col-sm-6 r-mb15">
                         				<label class="select">
                         				<select autocomplete="off" name="salutation" id="salutation" class="e-warranty-select">
                         					<option value="">Salutation</option>
                         					<option value="Prof">Prof</option>
                         					<option value="Dr">Dr</option>
                         					<option value="Mr">Mr</option>
                         					<option value="Mrs">Mrs</option>
                         					<option value="Mdm">Mdm</option>
                         					<option value="Miss">Miss</option>
                         					<option value="Ms">Ms</option>
                         				</select>
                         				<input id="tokens" name="_token" type="hidden" value="{{csrf_token()}}">
                         				</label>
                         			</div>
                         			<div class="col-sm-6">
                         				<input type="text" autocomplete="off" name="name" id="name" class="form-control" placeholder="Name" required>
                         			</div>
                         		</div>
                         	</div>
                         	<div class="row mb15">
                         		<div class="form-group">
                         			<div class="col-sm-12">
                         				<input type="email" autocomplete="off" name="email" id="email" class="form-control" placeholder="Email" required>
                         			</div>
                         		</div>
                         	</div>
                         	<div class="row mb15">
                         		<div class="form-group">
                         			<div class="col-sm-6 r-mb15">
                         				<input type="text" autocomplete="off" name="telephone" id="telephone" class="form-control" placeholder="Mobile">
                         			</div>
                         			<div class="col-sm-6">
                         				<div class="input-group date" id="datetimepicker1">
                         					<input type="text" autocomplete="off" name="date_of_birth" id="dateofbirth" min="1930-01-01" max="2010-01-01" class="form-control datepicker-dob" placeholder="Date of Birth" required>
                         					<span class="input-group-addon"><span class="ion-android-calendar"></span></span>
                         				</div>
                         			</div>
                         		</div>
                         	</div>
                         	<div class="row mb15">
                         		<div class="form-group">
                         			<div class="col-md-9 col-sm-8 r-mb15">
                         				<input type="text" class="form-control" id="address" placeholder="Address" autocomplete="off" name="address" required>
                         			</div>
                         			<div class="col-md-3 col-sm-4">
                         				<input type="text" autocomplete="off" name="postal" id="postal" class="form-control" placeholder="Postal Code" required>
                         			</div>
                         		</div>
                         	</div>
                         	<div class="row mb15">
                         		<div class="pl15 pr15">
                         			<h4>Product Information</h4>								
                         		</div>
                         		<div class="form-group">
                         			<div class="col-sm-6 r-mb15">
                         				<input type="text" class="form-control" id="pin" autocomplete="off" name="purchase_invoice_number" placeholder="Purchase Invoice Number" required>
                         			</div>
                         			<div class="col-sm-6">
                         				<label class="select">
                         				<select autocomplete="off" name="place_of_purchase" id="place_of_purchase" class="e-warranty-select" required>
                         					<option selected value="">Place of Purchase</option>
                         					<option value="Posh Mattress Boutique @ Expo Fair">Posh Mattress Boutique @ Expo Fair</option>
                         					<option value="Posh Mattress Boutique @ Atrium Fair">Posh Mattress Boutique @ Atrium Fair</option>
                         				</select>
                         				</label>
                         			</div>
                         		</div>
                         	</div>
						 	<div class="row mb15">
						 		<span class="pl15 pr15">Please Specify:</span>
						 		<div class="clearfix"></div>
						 		<div class="form-group">
						 			<div class="col-sm-6 r-mb15">
						 				<div class="input-group date" id="datetimepicker1">
						 					<input type="text" autocomplete="off" name="date_of_purchase" id="dop" class="form-control datepicker" placeholder="Date of Purchase" required>
						 					<span class="input-group-addon"><span class="ion-android-calendar"></span></span>
						 				</div>
						 			</div>
						 			<div class="col-sm-6">
						 				<div class="input-group date" id="datetimepicker1">
						 					<input type="text" id="dod" autocomplete="off" name="date_of_delivery" class="form-control datepicker" placeholder="Date of Delivery" required>
						 					<span class="input-group-addon"><span class="ion-android-calendar"></span></span>
						 				</div>
						 			</div>
						 		</div>
						 	</div>
						 	<div class="row mb15">
						 		<div class="form-group">
						 			<div class="col-sm-6 r-mb15">
						 				<input type="text" autocomplete="off" name="model_name" id="model_name" class="form-control" placeholder="Model Name" required>
						 			</div>
						 			<div class="col-sm-6">
						 				<input type="text" autocomplete="off" name="serial_number" id="serial_number" class="form-control" placeholder="Serial Number" required>
						 			</div>
						 		</div>
						 	</div>
                         	<div class="row mb15">
                         		<div class="form-group">
                         			<div class="col-sm-6 col-xs-12 r-mb15">
                         				<label for="product_type" class="select">
                         				<select class="sample1 trigger e-warranty-select" id="product_type" autocomplete="off" name="product_type" required>
                         					<option value="">Product Type</option>
                         					@if($productType)
                         					@foreach($productType as $row)
                         					<option value="{{$row->id.'$$'.$row->ptype_name}}">{{$row->ptype_name}}</option>
                         					@endforeach
                         					@endif
                         				</select>
                         				</label>
                         				<span class="drop-model-error1 unhappyMessage"></span>
                         			</div>
                         			<div class="col-sm-6 col-xs-12">
                         				<label for="product_size" class="select">
                         				<select class="samplesize1 trigger e-warranty-select" id="product_size" autocomplete="off" name="product_size" required>
                         					<option value="">Size</option>
                         				</select>
                         				</label>
                         				<span class="drop-size-error1 unhappyMessage"></span>
                         			</div>
                         		</div>
                         	</div>
						</div>
						<div class="col-sm-6 bg-grey pt40 pb15">
							@if(isset($csetting))
								@if($csetting->show_feedback == 1)
								<div class="row">
									<div class="pl15 pr15">
										<h4>Your Valuable Feedback</h4>
										<span>Please rate your purchasing experience:</span>
									</div>
									<div class="clearfix"></div>
									<div class="form-group">
										<div class="col-sm-12">
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_experience" class="experience" value="Excellent" checked> Excellent 
											</label>
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_experience" class="experience" value="Good"> Good 
											</label>
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_experience" clas s="experience" value="Can be improved"> Can be improved 
											</label>
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_experience" class="experience" value="Bad"> Bad 
											</label>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="pl15 pr15">
										<span>Please rate our delivery process:</span>
									</div>
									<div class="clearfix"></div>
									<div class="form-group">
										<div class="col-sm-12">
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_delivery" value="Excellent" checked> Excellent
											</label>
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_delivery" value="Good"> Good
											</label>
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_delivery" value="Can be improved"> Can be improved
											</label>
											<label class="radio-inline">
												<input type="radio" autocomplete="off" name="purchase_delivery" value="Bad"> Bad
											</label>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<textarea autocomplete="off" name="message" id="message" class="form-control" cols="30" rows="3" placeholder="Message" required></textarea>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								@endif
							@endif
							<div class="row pt15 mb15">
								<div class="form-group">
									<div class="col-sm-12">
										<input type="checkbox" autocomplete="off" name="terms" id="terms" required><span id="termslabel"> Please read and check to accept the <a target="_blank" style='color: #f7401c; font-size: 13px ;' href='{{ URL($locale.'/privacy-terms/') }}'>terms and conditions</a> of the warranty for your purchase.</span>
									</div>
								</div>
							</div>
							<div style="height: 10px;">&nbsp;</div>
							<div class="row mb15">
								<div class="form-group">
									<div class="col-sm-12">
										<p>I agree that Sommeil Terre Pte Ltd may collect and use my personal data for providing marketing materials that I have agreed to receive. To unsubscribe, I will be required to write to 
										@if(isset($csetting))
											<a href="mailto:{{$csetting->email}}">{{$csetting->email}}</a></p>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="clearfix"></div>
								<div class="form-group">	
		                        	<div class="col-md-8 col-sm-12">
		                        		<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LeEMVgUAAAAAJe2hGQFR0peUHuJGpKaIlHx_R-X"></div>
		                        		<script src='https://www.google.com/recaptcha/api.js'></script>
		                        		<div class="clearfix" style="margin-bottom:15px;"></div>
		                        	</div>
		                        	<div class="col-md-4 col-sm-12">
		                        		<div style="padding-top: 5px;">
		                        			<!-- [submit id:send class:btn-red-3 "Submit"] -->
		                        			<input type="submit" autocomplete="off" name="submit" id="send" class="btn btn-primary btn-round" value="Submit">
		                        		</div>
		                        	</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
</main>

@include('includes.footer')
@endsection
@section('script')
<script src="{{ Config::get('app.head_url') }}frontend/js/bootstrap-datepicker.js"></script>
<script src="{{ Config::get('app.head_url') }}frontend/js/happy.js"></script>
<script src="{{ Config::get('app.head_url') }}frontend/js/happyjs-methods.js"></script>
<script src="{{ Config::get('app.head_url') }}assets/js/jquery.validate.js"></script>

<script type="text/javascript">

	$( document ).ready(function() {
		var screenwidth = $( document ).width();
		if(screenwidth < 801 )
		{
			var dateofbirth = $(".datepicker-dob");
			dateofbirth.each(function(){
			    // $(this).removeClass("datepicker-dob");
			});
			var collection = $(".datepicker");
			collection.each(function(){
				// $(this).removeClass("datepicker");
			});
			$('.datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true
			});
			$('.datepicker-dob').datepicker({
				format: 'dd/mm/yyyy',
			    // startDate: '-87y',
			    
			    startDate: '1/1/1930',
			    endDate: '-6y',
			    autoclose: true
			});
		}
		else
		{
			$('.datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true
			});
			$('.datepicker-dob').datepicker({
				format: 'dd/mm/yyyy',
			    // startDate: '-87y',
			    startDate: '1/1/1930',
			    endDate: '-6y',
			    autoclose: true
			});
		}
		// $( "#ewarranty" ).submit(function( event ) {
		// 	event.preventDefault();
		// 	var recaptcha = document.forms["myForm"]["g-recaptcha-response"].value;
		// 	if (recaptcha == ""){
		// 		alert("Please fill reCAPTCHA");
		// 		return false;
		// 	}
		// });
		$("#product_type").change(function()
	    {
	      var str = $(this).val();
	      var type = str.split("$$");
	      var tokens = $('#tokens').val();

	      $.ajax({
	        url:"{{URL('/getproduct-size')}}",
	        type: 'get',
	        data:{'_token':tokens,'type':type[0]},
	        dataType: 'json',
	        success:function(result){
	          $('#product_size').empty();
	          $('#product_size').append('<option value="">Size</option>');
	          $.each( result,function( key, value )
	          {
	            $('#product_size').append('<option value="'+ value.id + '$$' + Math.round(value.dwidth)+' x '+Math.round(value.dheight)+' '+value.unit +'">'+ Math.round(value.dwidth)+' x '+Math.round(value.dheight)+' '+value.unit+'</option>');
	          });
	        }
	      });
	    });
    });
	$('#send').click(function(e){
	    if($("#send").hasClass("disabled")){
	      e.preventDefault();
	      alert("Please Check the verification Captcha!!!!");
	    }
	});
	function recaptchaCallback() {
		$('#send').removeClass('disabled');
	};
    $(document).on("pagebeforeshow", function(){
      alert("Load function");
    })
</script>
@endsection