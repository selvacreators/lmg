@extends('layouts.layout_home')
@section('style')
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}mattress_selector/css/main.css">
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}mattress_selector/css/responsive.css">
@endsection
@section('content')
@include('includes.header')
@include('includes.breadcrumb')
<main class="content-wrapper">
    <div class="page-content">
        <section class="start-up">
            <div class="parallax">
                <div class="background-image">
                    <img src="{{config::get('app.head_url')}}mattress_selector/images/mattress-selector.jpg">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="content-heading text-center">
                                <h1 class="title">Mattress Selector</h1>
                                <h2 class="h4 text-center">Select and buy your perfect mattress in 3 easy steps</h2>
                            </div>
                        </div>                       
                        <div class="mstart pt40 pb80">
                            <div class="col-md-4 m-cell">
                                <div class="mbox">
                                    <div class="mstart-icon mt20 mb20"><i class="ion-ios-settings-strong"></i></div>
                                    <h3>Complete your sleep profile</h3>
                                    <div class="mbox-content">
                                        <p>The Mattress Selector will ask you a series of questions about the support you need from your bed to determine the best match of LMG product’s technology and comfort choices.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 m-cell">
                                <div class="mbox">
                                    <div class="mstart-icon mt20 mb20"><i class="ion-android-pin"></i></div>
                                    <h3>View where to buy: In-store from one of LMG Gallery</h3>
                                    <div class="mbox-content">
                                        <p>By specifying your location we will then present a recommended LMG Mattress range and list of stores local to you where you can view and test LMG products.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 m-cell">
                                <div class="mbox">
                                    <div class="mstart-icon mt20 mb20"><i class="ion-paper-airplane"></i></div>
                                    <h3>Save your Mattress Selector Results</h3>
                                    <div class="mbox-content">
                                        <p>The Mattress Selector will ask you a series of questions about the support you need from your bed to determine the best match of LMG product’s technology and comfort choices</p>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <div class="col-md-12 text-center mbox-bottom pt80">
                            <button name="mstart" class="btn btn-primary btn-round" id="mstart" type="button"> Get Started</button>
                            <div class="clearfix mt30"></div>
                            <?php $locale = url(Session::get('locale')); ?>
                            <a href="{{ URL($locale.'/') }}mattresses/" class="btn btn-round btn-primary">View Our Full Mattress Range </a>
                            <div class="box max-width50 mr-auto mt40">
                                <p>We understand that choosing your new mattress is not a decision you can make without careful consideration.</p>
                                <p>To help make your decision easier, use our Mattress Selector to determine what's most important to you when it comes to getting a comfortable night's sleep and we'll provide a recommendation based on your unique needs.</p>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </section>
        <section id="mattress-selector">            
            <div class="container-fluid no-pad">
                <div class="row-fluid">
                    <div class="col-md-12 no-pad">
                        <form action="{{ URL('add_mattressselect')}}" method="POST" class="form-horizontal form-inline" role="form" name="frmmattress" id="frmmattress">
                         <input name="_token" type="hidden" value="{{csrf_token()}}">
                            <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false" data-wrap="false">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                                    <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                                </ol>
                                <div class="carousel-inner">                                 
                                    
                                    <div id="gender" class="item active">
                                        <section class="section-heading">
                                            <h2 class="heading-title q1">Who are you buying a mattress for?</h1>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option">
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="option-male selector_img"></span>
                                                                <span class="label-text">Myself</span>                                                                
                                                                <input name="gender" data-title="Myself" data-section="gender" class="next-slide" id="radio1" value="Myself" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="option-female selector_img"></span>
                                                                <span class="label-text">My partner & I</span>
                                                                
                                                                <input name="gender" class="next-slide" data-title="My partner & I" data-section="gender" id="radio1" value="My Partner & I" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="option-Mypartner selector_img"></span>
                                                                <span class="label-text">My partner, I & My kids</span>
                                                                
                                                                <input name="gender" class="next-slide" data-title="My partner, I & My kids" data-section="gender" id="radio1" value="My partner, I & My kids" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="option-spare selector_img"></span>
                                                                <span class="label-text">Just for the spare bedroom</span>
                                                                
                                                                <input name="gender" class="next-slide" data-title="spare bedroom" data-section="gender" id="radio1" value="Just for the spare bedroom" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-lg margin-auto next-slide" data-title=""  data-section="people">Continue <i class="ion-ios-arrow-right" ></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    <!--People-->
                                    <div id="people" class="item ">
                                        <section class="section-heading">
                                            <h2 class="heading-title q2">What size bed are you looking for - big enough for 2 people?</h2>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option">
                                                    <div class="col-sm-6 col-md-4 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="wr-less50 selector_img"></span>
                                                                <span class="label-text">Yes, my guests need space</span>                                                                
                                                                <input name="2people" data-title="Less than 50 kg" data-section="people" class="next-slide" id="2people" value="Yes, my guests need space" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-4 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="wr-fiftysixty selector_img"></span>
                                                                <span class="label-text">No, my guests are usually single</span>
                                                                
                                                                <input name="2people" class="next-slide" data-title="50-60 kg" data-section="people" id="2people" value="No, my guests are usually single" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-4 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="wr-sixtyseventy selector_img"></span>
                                                                <span class="label-text">Don't know</span>
                                                                
                                                                <input name="2people" class="next-slide" data-title="60-70 kg" data-section="people" id="2people" value="Don't know" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                   

                                                    <div class="clearfix"></div>
                                                    <div class="btn-group">
                                                            <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>
                                                            <button type="button" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="people">Continue <i class="ion-ios-arrow-right" ></i> </button>
                                                      
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                    <!---->
                                    <div id="looking-mattress" class="item new_mattress">
                                        <section class="section-heading">
                                            <h2 class="heading-title q3">Why are you looking for a new mattress?</h2>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option">
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="lm-old selector_img"></span>
                                                                <span class="label-text">My current mattress is old and worn out</span>                                                                
                                                                <input name="lookingmattress" data-title="old and worn out" data-section="looking-mattress" class="next-slide" id="radio1" value="My current mattress is old and worn out" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="lm-married selector_img"></span>
                                                                <span class="label-text">I am getting married or moving in with someone</span>
                                                                
                                                                <input name="lookingmattress" class="next-slide" data-title="getting married or moving" data-section="looking-mattress" id="radio1" value="I am getting married or moving in with someone" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="lm-quality selector_img"></span>
                                                                <span class="label-text">I need a better quality mattress to improve my quality of sleep</span>
                                                                
                                                                <input name="lookingmattress" class="next-slide" data-title="Improve my quality of sleep" data-section="looking-mattress" id="radio1" value="I need a better quality mattress to improve my quality of sleep" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="lm-hotel selector_img"></span>
                                                                <span class="label-text">I recently had a great night’s sleep at a hotel and want to look into purchasing this mattress</span>
                                                                
                                                                <input name="lookingmattress" class="next-slide" data-title="Great night’s sleep at a hotel" data-section="looking-mattress" id="radio1" value="I recently had a great night’s sleep at a hotel and want to look into purchasing this mattress" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="btn-group">
                                                            <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>

                                                            <button type="button" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="looking-mattress">Continue <i class="ion-ios-arrow-right" ></i> </button>
                                                      
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                    <!--Weight Range-->
                                    <div id="weight-range" class="item">
                                        <section class="section-heading">
                                            <h2 class="heading-title q4">Please select your weight range</h2>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option your-weight">
                                                    <div class="content-heading">
                                                        <h4 class="h4 title t1">You</h4>
                                                    </div>
                                                    
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="mw-less50 selector_img"></span>
                                                                <span class="label-text"><50 kg</span>                                                                
                                                                <input name="weightrange" data-title="Less than 50 kg" data-section="weight-range" class="next-slide" id="radio1" value="<50 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="mw-above50 selector_img"></span>
                                                                <span class="label-text">50-60 kg</span>
                                                                
                                                                <input name="weightrange" class="next-slide" data-title="50-60 kg" data-section="weight-range" id="radio1" value="50-60 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="mw-above60 selector_img"></span>
                                                                <span class="label-text">60-70 kg</span>
                                                                
                                                                <input name="weightrange" class="next-slide" data-title="60-70 kg" data-section="weight-range" id="radio1" value="60-70 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="mw-great70 selector_img"></span>
                                                                <span class="label-text">>70 kg</span>
                                                                
                                                                <input name="weightrange" class="next-slide" data-title="above 70 kg" data-section="weight-range" id="radio1" value=">70 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="mselector-option partner-weight">
                                                    <div class="content-heading">
                                                        <h4 class="h4 title t2">You Partner</h4>
                                                    </div>
                                                    
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="wr-less50 selector_img"></span>
                                                                <span class="label-text"><50 kg</span>                                                                
                                                                <input name="partner_weightrange" data-title="Less than 50 kg" data-section="weight-range" class="next-slide" id="radio1" value="<50 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="wr-fiftysixty selector_img"></span>
                                                                <span class="label-text">50-60 kg</span>
                                                                
                                                                <input name="partner_weightrange" class="next-slide" data-title="50-60 kg" data-section="weight-range" id="radio1" value="50-60 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="wr-sixtyseventy selector_img"></span>
                                                                <span class="label-text">60-70 kg</span>
                                                                
                                                                <input name="partner_weightrange" class="next-slide" data-title="60-70 kg" data-section="weight-range" id="radio1" value="60-70 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="wr-great70 selector_img"></span>
                                                                <span class="label-text">>70 kg</span>
                                                                
                                                                <input name="partner_weightrange" class="next-slide" data-title="above 70 kg" data-section="weight-range" id="radio1" value=">70 kg" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="clearfix"></div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>
                                                    <button type="button" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="weight-range">Continue <i class="ion-ios-arrow-right" ></i> </button>
                                                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                    <!--Age Range-->
                                    <div id="age-range" class="item">
                                        <section class="section-heading">
                                            <h2 class="heading-title q5">Please select your age range</h2>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option your-age">
                                                    <div class="content-heading">
                                                        <h4 class="h4 title t1">You</h4>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="am-30yrs selector_img"></span>
                                                                <span class="label-text"><30 yrs</span>
                                                                <input name="agerange" data-title="Less than 30 yrs" data-section="age-range" class="next-slide" id="radio1" value="<30 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="am-30to35yrs selector_img"></span>
                                                                <span class="label-text">30-35 yrs</span>
                                                                
                                                                <input name="agerange" class="next-slide" data-title="30-35 yrs" data-section="age-range" id="radio1" value="30-35 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="am-36to45yrs selector_img"></span>
                                                                <span class="label-text">36-45 yrs</span>
                                                                
                                                                <input name="agerange" class="next-slide" data-title="36-45 yrs" data-section="age-range" id="radio1" value="36-45 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="am-46to55yrs selector_img"></span>
                                                                <span class="label-text">46-55 yrs</span>
                                                                
                                                                <input name="agerange" class="next-slide" data-title="46-55 yrs" data-section="age-range" id="radio1" value="46-55 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="am-above55 selector_img"></span>
                                                                <span class="label-text">>55 yrs</span>
                                                                
                                                                <input name="agerange" class="next-slide" data-title="Above 55 yrs" data-section="age-range" id="radio1" value=">55 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mselector-option partner-age">
                                                    <div class="content-heading">
                                                        <h4 class="h4 title t2">You Partner</h4>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="aw-30yrs selector_img"></span>
                                                                <span class="label-text"><30 yrs</span>                                                                
                                                                <input name="partner_agerange" data-title="Less than 30 yrs" data-section="age-range" class="next-slide" id="radio1" value="<30 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="aw-30to35yrs selector_img"></span>
                                                                <span class="label-text">30-35 yrs</span>
                                                                
                                                                <input name="partner_agerange" class="next-slide" data-title="30-35 yrs" data-section="age-range" id="radio1" value="30-35 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="aw-36to45yrs selector_img"></span>
                                                                <span class="label-text">36-45 yrs</span>
                                                                
                                                                <input name="partner_agerange" class="next-slide" data-title="36-45 yrs" data-section="age-range" id="radio1" value="36-45 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="aw-46to55yrs selector_img"></span>
                                                                <span class="label-text">46-55 yrs</span>
                                                                
                                                                <input name="partner_agerange" class="next-slide" data-title="46-55 yrs" data-section="age-range" id="radio1" value="46-55 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc age_range">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="aw-above55 selector_img"></span>
                                                                <span class="label-text">>55 yrs</span>
                                                                
                                                                <input name="partner_agerange" class="next-slide" data-title="Above 55 yrs" data-section="age-range" id="radio1" value=">55 yrs" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>
                                                    <button type="button" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="age-range">Continue <i class="ion-ios-arrow-right" ></i> </button>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                    <!--sleep position-->
                                    <div id="sleep-position" class="item">
                                        <section class="section-heading">
                                            <h2 class="heading-title q6">What is your usual sleep position?</h2>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option your-position">

                                                    <div class="content-heading">
                                                        <h4 class="h4 title t1">You</h4>
                                                    </div> 
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="sp-y-back selector_img"></span>
                                                                <span class="label-text">Back</span>                                                                
                                                                <input name="yourposition" data-title="Back" data-section="sleep-position" class="next-slide" id="radio1" value="Back" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="sp-y-stomach selector_img"></span>
                                                                <span class="label-text">Stomach </span>
                                                                
                                                                <input name="yourposition" class="next-slide" data-title="Stomach" data-section="sleep-position" id="radio1" value="Stomach" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="sp-y-side selector_img"></span>
                                                                <span class="label-text">Side </span>
                                                                
                                                                <input name="yourposition" class="next-slide" data-title="Side" data-section="sleep-position" id="radio1" value="Side" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="sp-y-toss selector_img"></span>
                                                                <span class="label-text">Toss & turn throughout the night</span>
                                                                
                                                                <input name="yourposition" class="next-slide" data-title="Toss & turn" data-section="sleep-position" id="radio1" value="Toss & turn throughout the night" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="mselector-option partner-position">   

                                                    <div class="content-heading">
                                                        <h4 class="h4 title t2">You Partner</h4>
                                                    </div> 
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="sp-p-back selector_img"></span>
                                                                <span class="label-text">Back</span>                                                                
                                                                <input name="partnerposition" data-title="Back" data-section="sleep-position" class="next-slide" id="radio1" value="Back" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="sp-p-stomach selector_img"></span>
                                                                <span class="label-text">Stomach </span>
                                                                
                                                                <input name="partnerposition" class="next-slide" data-title="Stomach" data-section="sleep-position" id="radio1" value="Stomach" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="sp-p-side selector_img"></span>
                                                                <span class="label-text">Side </span>
                                                                
                                                                <input name="partnerposition" class="next-slide" data-title="Side" data-section="sleep-position" id="radio1" value="Side" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="sp-p-toss selector_img"></span>
                                                                <span class="label-text">Toss & turn throughout the night</span>
                                                                
                                                                <input name="partnerposition" class="next-slide" data-title="Toss & turn" data-section="sleep-position" id="pat-pos__Toss" value="Toss & turn throughout the night" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>

                                                    
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>
                                                    <button type="button" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="sleep-position">Continue <i class="ion-ios-arrow-right" ></i> </button>
                                                  
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                    <!--issues during or after sleep-->
                                    <div id="issues" class="item">
                                        <section class="section-heading">
                                            <h2 class="heading-title q7">Do you experience any issues during or after sleep?</h2>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option">
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="i-hip selector_img"></span>
                                                                <span class="label-text">HIP pain</span>                                                                
                                                                <input name="issues" data-title="HIP pain" data-section="issues" class="next-slide" id="radio1" value="HIP pain" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="i-shoulder selector_img"></span>
                                                                <span class="label-text">Shoulder pain</span>
                                                                
                                                                <input name="issues" class="next-slide" data-title="Shoulder pain" data-section="issues" id="radio1" value="Shoulder pain" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="i-back selector_img"></span>
                                                                <span class="label-text">Back pain</span>
                                                                
                                                                <input name="issues" class="next-slide" data-title="Back pain" data-section="issues" id="radio1" value="Back pain" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3 col-xs-12 width-calc">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="i-none selector_img"></span>
                                                                <span class="label-text">NONE</span>
                                                                
                                                                <input name="issues" class="next-slide" data-title="NONE" data-section="issues" id="radio1" value="NONE" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>
                                                            <button type="button" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="issues">Continue <i class="ion-ios-arrow-right" ></i> </button>
                                                      
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                    <!--too hot or too cold-->
                                    <div id="hot-cold" class="item">
                                        <section class="section-heading">
                                            <h2 class="heading-title q8">Do you sleep too hot or too cold?</h2>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">
                                                <div class="mselector-option">
                                                    <div class="col-sm-4 col-md-4 col-xs-6">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="hc-cool selector_img"></span>
                                                                <span class="label-text">Cool </span>                                                                
                                                                <input name="hotcold" data-title="Cool" data-section="hot-cold" class="next-slide" id="radio1" value="Cool" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-xs-6">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="hc-hot selector_img"></span>
                                                                <span class="label-text">Hot </span>
                                                                
                                                                <input name="hotcold" class="next-slide" data-title="Hot" data-section="hot-cold" id="radio1" value="Hot" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-xs-6">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="hc-noissue selector_img"></span>
                                                                <span class="label-text">No Issue</span>
                                                                
                                                                <input name="hotcold" class="next-slide" data-title="No Issue" data-section="hot-cold" id="radio1" value="No Issue" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="clearfix"></div>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>
                                                            <button type="button" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="hot-cold">Continue <i class="ion-ios-arrow-right" ></i>  </button>
                                                      
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                    <!--comfortable sleep-->
                                    <div id="comfortable-sleep" class="item">
                                        <section class="section-heading">
                                            <div class="heading-title q9">On which surface do you think you would have the most comfortable sleep? (How firm would you like your mattress?)</div>
                                        </section>
                                        <div class="container">
                                            
                                            <div class="carousel-caption mycarsel2">

                                            <input type="hidden" name="comfor_table" id="comfor_table">
                                                <div class="mselector-option">
                                                    <div class="col-sm-4 col-md-4 col-xs-6">                                                        
                                                        <div class="form-group margin-auto">
                                                            <label class="control-label">
                                                                <span class="cs-plush selector_img"></span>
                                                                <span class="label-text">PLUSH</span> 
                                                                <p class="label-text">I like my mattress to be really soft and forgiving</p>                                                           
                                                                <input name="comfortablesleep" data-title="PLUSH" data-section="comfortable-sleep" class="next-slide" id="radio1" value="I like my mattress to be really soft and forgiving" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-xs-6">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="cs-medium selector_img"></span>
                                                                <span class="label-text">MEDIUM </span>
                                                                <p class="label-text">I want my mattress to be soft, with just a bit of resistance</p>

                                                                <input name="comfortablesleep" class="next-slide" data-title="MEDIUM" data-section="comfortable-sleep" id="radio1" value="I want my mattress to be soft, with just a bit of resistance" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 col-md-4 col-xs-6">
                                                        <div class="form-group margin-auto">                                                            
                                                           <label class="control-label">
                                                                <span class="cs-firm selector_img"></span>
                                                                <span class="label-text">FIRM </span>
                                                                <p class="label-text">I prefer my mattress to be firm, with just a bit of give</p>
                                                                <input name="comfortablesleep" class="next-slide" data-title="FIRM" data-section="comfortable-sleep" id="radio1" value="I prefer my mattress to be firm, with just a bit of give" type="radio">
                                                            </label>
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="clearfix"></div>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary btn-lg margin-auto left" ><i class="ion-ios-arrow-left" ></i>Back</button>
                                                <button type="button" id="mattress_submit" class="btn btn-primary btn-lg btn-block margin-auto next-slide" data-title=""  data-section="comfortable-sleep"><i class="ion-ios-arrow-right" ></i>Submit </button>
                                                      
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!---->


                                </div> <a class="left carousel-control" href="#myCarousel" data-slide=""><span class="icon-prev ion-ios-arrow-back"></span></a>
                                <a class="right carousel-control" href="#myCarousel" data-slide=""><span class="icon-next ion-ios-arrow-forward"></span></a>

                            </div>
                            <div class="mattress-que-ans"> 
                                <div class="container">
                                    <div class="row">
                                        <div id="Mattress_AppendText"  class="col-sm-12">
                                            <span id="app-gender"></span>
                                            <span id="Mattress"></span>
                                            <span id='look_Mattress'></span>
                                            <span id='weight_range'>
                                                <span id='weightYou'></span>
                                                <span id='PartnerWeight'></span>
                                            </span>
                                            <span id='agerange'>
                                                <span id='ageYou'></span>
                                                <span id='PartnerAge'></span>
                                            </span>
                                            <span id='your_position'>
                                                <span id='positionYou'></span>
                                                <span id='PartnerPosition'></span>
                                            </span>
                                            <span id='issues_data'></span>
                                            <span id='hotcold_data'></span>
                                            <span id='finalsleep_data'></span>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
           <!--  <div class="street"></div> -->
        </section>
        <section id="test" class="section-blue"></section>
    </div>
</main>
@include('includes.newsletter-form')
@include('includes.footer')
@endsection

@section('script')      
        
        <script type="text/javascript" src="{{ Config::get('app.head_url') }}frontend/js/bootstrap-notify.js"></script>

        <script type="text/javascript" src="{{ Config::get('app.head_url') }}mattress_selector/js/custom.js?v=1.2"></script>


    <script type="text/javascript">
        function carouselNormalization() {
          var items = $('#myCarousel .item'), //grab all slides
            heights = [], //create empty array to store height values
            tallest; //create variable to make note of the tallest slide

          if (items.length) {
            function normalizeHeights() {
              items.each(function() { //add heights to array
                heights.push($(this).height());
              });
              tallest = Math.max.apply(null, heights); //cache largest value
              items.each(function() {
                $(this).css('min-height', tallest + 'px');
              });
            };
            normalizeHeights();

            $(window).on('resize orientationchange', function() {
              tallest = 0, heights.length = 0; //reset vars
              items.each(function() {
                $(this).css('min-height', '0'); //reset min-height
              });
              normalizeHeights(); //run it again 
            });
          }
        }
        $(document).ready(function (){

            $('#mattress-selector, #mattressResult').hide();
            $('#mstart').click(function() {
               $('#mattress-selector').show();
                $('.start-up').hide();
            });
            $('#mattress_submit').click(function() {
               $('#mattressResult').show();
                $('.start-up').hide();
                $('#mattress-selector').hide();
            });

            $('input[name=comfortablesleep]').on('change', function() {
                var data_tit=  $("input[name='comfortablesleep']:checked").attr('data-title');
                $('#comfor_table').val(data_tit);
            });

            $('#mattress_submit').click(function(){
                //alert('data');
                $('#mattress-selector').hide();
                $('#test').show();

                var str = $("#frmmattress").serializeArray();
                $.ajax({
                    type: "POST",  
                    //url: "update.php",  
                    url: "{{ URL('/add_mattressselect') }}",  
                    data: str,  
                    success: function(value) {  
                    console.log(value);
                        $("#test").html(value);
                    }
                });
            });
        });

    /*==========================================================
            50. promo popup open when window is load
    ======================================================================*/
    
    $('#mattress_submit').click(function () {
    // jQuery("body").on("click","#mattress_submit #send",function(){
        setTimeout(function() {
            if ($('.xs-promoPopup').length) {
                if ($(window).width() >= 991) {
                    $.magnificPopup.open({
                        items: {
                            src: '.xs-promoPopup' 
                        },
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: true,
                        midClick: true,
                        removalDelay: 300,
                        callbacks:{
                            beforeOpen: function() {
                                this.st.mainClass = "my-mfp-zoom-in xs-promo-popup";
                            }
                        }
                    }); 
                }
            }
        }, 2000);
    });
    function showPopup(){
         if ($('.xs-promoPopup').length) {
                if ($(window).width() >= 991) {
                    $.magnificPopup.open({
                        items: {
                            src: '.xs-promoPopup' 
                        },
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: true,
                        midClick: true,
                        removalDelay: 300,
                        callbacks:{
                            beforeOpen: function() {
                                this.st.mainClass = "my-mfp-zoom-in xs-promo-popup";
                            }
                        }
                    }); 
                }
            }
    }
    </script>
@endsection