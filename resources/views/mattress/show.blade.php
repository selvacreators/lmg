<div id="mattressResult">
    <section class="mattress-selector-result pt60 pb40">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="panel panel-transparent">
                        <div class="panel-body">
                            <div class="mattress-profile">
                                <div class="profile-img">
                                   @if($mat_val->gender == 'Myself')
                                   <span class="option-male selector_img"></span>
                                   @elseif($mat_val->gender == 'My partner, I & My kids')
                                   <span class="option-Mypartner selector_img"></span>
                                   @elseif($mat_val->gender == 'Just for the spare bedroom')
                                   <span class="option-spare selector_img"></span>
                                   {{-- @elseif($mat_val->gender == 'My partner & I') --}}
                                   @else
                                   <span class="option-female selector_img"></span>
                                   @endif
                                </div>
                                <div class="profile-content">
                                    <h2>Here's your Mattress Profile.</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Mattress for:</div>
                                <div class="selection br-lf-green mats-select-ans">{{$mat_val->gender}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!empty($mat_val->lookingmattress))
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Looking for a new mattress:</div>
                                <div class="selection br-lf-green mats-select-ans">{{$mat_val->lookingmattress}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!--  @if(!empty($mat_val->sleep_guest)) -->
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Looking for - Big enough for 2 people:</div>
                                <div class="selection br-lf-green mats-select-ans">{{$mat_val->sleep_guest}}</div>
                            </div>
                        </div>
                    </div>
                </div>
               <!--  @endif -->
                
                @if(!empty($mat_val->weightrange))
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Weight Range:</div>
                                <div class="selection br-lf-green mats-select-ans">
                                   <p>You :{{$mat_val->weightrange}}</p>
                                    @if(!empty($mat_val->partner_weightrange))
                                    <p>You Partner: {{$mat_val->partner_weightrange}}</p>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($mat_val->agerange))
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Age Range:</div>
                                <div class="selection br-lf-green mats-select-ans">
                                    <p>You: {{$mat_val->agerange}}</p>
                                     @if(!empty($mat_val->partner_agerange))
                                    <p>You Partner: {{$mat_val->partner_agerange}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($mat_val->yourposition))
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">
                                    Sleep Position:
                                </div>
                                <div class="selection br-lf-green mats-select-ans">
                                    <p>You: {{$mat_val->yourposition}}</p>
                                    @if(!empty($mat_val->partnerposition))
                                    <p>You Partner: {{$mat_val->partnerposition}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($mat_val->issues))
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Aches / Pain:</div>
                                <div class="selection br-lf-green mats-select-ans">{{$mat_val->issues}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($mat_val->hotcold))
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Temperature:</div>
                                <div class="selection br-lf-green mats-select-ans">{{$mat_val->hotcold}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($mat_val->comfortablesleep))
                <div class="col-md-6 col-sm-6">
                    <div class="panel result">
                        <div class="panel-body pt0 pb0 pl20 pr20 br-green">
                            <div class="row">
                                <div class="selection">Mattress Feeling:</div>
                                <div class="selection br-lf-green mats-select-ans">
                                    {{$mat_val->comfor_table}}
                                    <p>{{$mat_val->comfortablesleep}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="clearfix"></div>
                <div class="col-md-12 text-center">
                    <div class="form-group">
                        <button type="button" id="send" onclick="showPopup();" class="btn btn-primary btn-lg btn-round">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($recommandProduct)
    <section class="top-mattress-recommendation pt40 pb40">
        <div class="container">
            <div class="row">
                <div class="content-heading">
                    <h2 class="title">
                        Top 3 Mattress Recommendation
                    </h2>
                </div>
            </div>
            <div class="row product-layout eq_height">
                @foreach($recommandProduct as $product)
                @if(!empty($product))
                <div class="product-layout product-grid col-sm-4 mb30 item">
                    <div class="product-thumb">
                        <div class="image">
                            <a href="#">
                                <img src="{{Config::get('app.head_url')}}product/images/{{$product[0]->prod_slider_image}}"/>
                            </a>
                        </div>
                        <span class="product-label"></span>
                    </div>
                    <div class="product-details">
                        <h4 class="product-name">{{$product[0]->name}}</h4>
                        <div class="product-attr"></div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </section>
    @endif
    @if($relateProduct)
    <section class="related-products pt40 pb40">
        <div class="container">
            <div class="row">
                <div class="content-heading">
                    <h2 class="title">
                        Other Related Products
                    </h2>
                </div>
            </div>
            <div class="row product-layout eq_height">
                @foreach($relateProduct as $rproduct)
                @if(!empty($rproduct))
                    <div class="product-layout product-grid col-sm-4 mb30 item">
                        <div class="product-thumb">
                            <div class="image">
                                <a href="#">
                                    <img src="{{Config::get('app.head_url')}}product/images/{{$rproduct[0]->prod_slider_image}}"/>
                                </a>
                            </div>
                            <span class="product-label"></span>
                        </div>
                        <div class="product-details">
                            <h4 class="product-name">{{$rproduct[0]->name}}</h4>
                            <div class="product-attr"></div>
                        </div>
                    </div>
                @endif
                @endforeach
            </div>
        </div>
    </section>
    @endif
</div>
<!-- promo popup wraper -->
<div class="zoom-anim-dialog mfp-hide xs-promoPopup">
    <div class="col-xs-12">
        <div class="row">
            <!-- <div class="col-lg-6 xs-padding-0">
                <div class="promo-image-container">
                    <img src="assets/images/promo-images.png" alt="">
                </div>
            </div> -->
            <div class="col-md-10 align-self-center mr-auto">
                <div class="promo-text-container text-center">
                    <div class="promo-text-content">
                        <h3>Contact Your Store</h3>
                        <p>Get exclusive discounts and coupons</p>
                    </div>
                    <div class="alert alert-success" id="msg" style="display: none;">
                        Thanks for completing our Mattress Selector.<br>
                        <span>We have emailed you a copy of your personalised LMG World mattress recommendation.</span>
                    </div>
                    <form action="{{ URL('/mattress_mail') }}" method="POST" class="xs-newsletter newsLetter-v2 form-horizontal" name="mattress_mail" id="mattress_mail">
                            <input name="_token" type="hidden" value="{{csrf_token()}}"> 
        					<input name="txtmattress_selector_id" type="hidden" value="{{$mat_val->id}}"> 
                        <div class="form-group">
                            <label for="xs-promo-name" class="control-label col-sm-2">Name :</label> 
                            <div class="col-sm-10">
                                <input type="text" name="name" id="xs-promo-email" placeholder="Enter your name...." class="form-control">
                            </div>   
                            
                        </div>
                        <div class="form-group">
                            <label for="xs-promo-email" class="control-label col-sm-2">Email :</label> 
                            <div class="col-sm-10">
                                <input type="email" name="email" id="xs-promo-email" placeholder="Enter your Email...." class="form-control">
                            </div>   
                            
                        </div>
                        <div class="form-group">
                            <label for="xs-promo-phone" class="control-label col-sm-2">Phone :</label> 
                            <div class="col-sm-10">
                                <input type="text" name="phone" id="xs-promo-phone" placeholder="Enter your Phone Number...." class="form-control">
                            </div>   
                            
                        </div>
                        <div class="form-group">
                            <label for="xs-promo-text" class="control-label col-sm-2">Message :</label> 
                            <div class="col-sm-10">
                                <textarea name="message" id="xs-promo-text" row="3" placeholder="Enter your Message...." class="form-control"></textarea>
                            </div>   
                            
                        </div>
                        <div class="form-group">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="" class="ml15 form-control">
                                    I'm happy to receive promotional updates & Sleep Guide tips from LMG World.
                            </label>
                        </div>
                        <div class="form-group">
                            <button type="button" id="send_mail" class="btn btn-primary btn-round pl15 pr15"><i class="fa fa-paper-plane"></i>Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  <!-- END promo popup wraper -->
<!-- <script src="{{ Config::get('app.head_url') }}frontend/js/jquery.matchHeight.min.js"></script> -->
<script>
    $('#send_mail').click(function(){
        // $('#mattress-selector').hide();
        // $('#test').show();
        var str = $("#mattress_mail").serializeArray();
        $.ajax({
            type: "POST",  
            url: "{{ URL('/mattress_mail') }}",  
            data: str,  
            success: function(value) {
                console.log(value);
                if(value == 1){
                    $('#msg').show();
                    $('#mattress_mail').hide();
                }
            }
        });
    });
</script>