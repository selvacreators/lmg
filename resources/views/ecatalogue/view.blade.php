@extends('layouts.layout_home')

@section('content')
@include('includes.header')

<div class="breadcrumbs">
    <div class="container-fluid">
        <ul class="items">
            <li class="item home">
                <a href="{{URL('/')}}" title="Go to Home Page">
                Home </a>
            </li>
            <li class="item cms_page"><strong>{{$currentPath}}</strong></li>
        </ul>
    </div>
</div>

<div class="page-title-wrapper pt60">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <div class="content-heading">                
                	<h1 class="title"><strong>Mattress Catalogue</strong></h1>              
                </div>
            </div>
        </div>
    </div>
</div>

<main class="content-wrapper">
    <div class="page-content">
      	<section class="e-catalogue pt40 pb40">
        <div class="container">
            <div class="row">
                <div id="catalog" class="col-sm-12">
                    <div class="eq_height row catalog-grid">
                    	@if($ecatalogue_desc)
                      	@foreach($ecatalogue_desc as $key =>  $val)
                    	<div class="col-md-4 item">
                            <div class="image">
                                <a href="{{ URL($path.'/'.$val->ec_slug)}}">
                                <img src="{{Config::get('app.head_url')}}product/ecatalogue/{{$val->ec_slug}}/{{$val->ec_image}}" class="img-responsive"/>
                                </a>
                                <h3 class="h4 cat-name text-center">{{$val->ec_name}}</h3>
                            </div>                    		
                            
                    	</div>
                    	@endforeach
                      	@endif
                    </div>
                    <!-- <div class="pagination">
                        <ul class="list-page">
                            <li><a href="#" class="page-number prev"><i class="ion-ios-arrow-left" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="page-number">1</a></li>
                            <li><a href="#" class="page-number current">2</a></li>
                            <li><a href="#" class="page-number ">3</a></li>
                            <li><a href="#" class="page-number next"><i class="ion-ios-arrow-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
      </section>
    </div>
</main>
@include('includes.newsletter-form')
@include('includes.footer')
@endsection
@section('script')
<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.matchHeight.min.js"></script>
<script type="text/javascript">
    $(window).load(function() {
        $('#catalog .catalog-grid.eq_height .item').matchHeight();
    });
</script>
<!-- <script type="text/javascript" src="{{ Config::get('app.head_url') }}ecatalogue/lib/turn.html4.min.js"></script> -->
@endsection