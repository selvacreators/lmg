@extends('layouts.layout_home')

@section('style')
<link type="text/css" rel="stylesheet" href="{{Config::get('app.head_url')}}ecatalogue/flipbook.css"></link>
<style type="text/css">
body{overflow: auto;}
.page-1 .s1{
    -webkit-transform: translateY(100px);
    -webkit-transition: all 1.5s;
    opacity: 0;
}
.page-1 .s2{
    -webkit-transform: translateY(100px);
    -webkit-transition: all 1s;
    -webkit-transition-delay: 0.5s;
    opacity: 0;
    /* font-family: 'Carrois Gothic SC', sans-serif; */
    font-style: normal;
    font-weight: 400;
    font-size: 40px;
    text-align: center;
    color: white;
}

.page-1.animation-on .s1{
    -webkit-transform: translateY(0px);
    opacity: 1;
}
.page-1.animation-on .s2{
    -webkit-transform: translateY(0px);
    opacity: 1;
}

</style>
@endsection

@section('content')
@include('includes.header')
@include('includes.breadcrumb')

<div class="catalog-app mt40 mb40">

    <div id="viewer">
        <div id="flipbook" class="ui-flipbook">
          <!-- Do not place the content here -->

          @if($ecatalogue_desc)
          @foreach($ecatalogue_desc as $key =>  $val)
          <div class="flip-item">
            <img src="{{Config::get('app.head_url')}}product/ecatalogue/{{$val->ec_slug}}/{{$val->ec_image}}" class="flip-img" alt="Mattress E-catalogue"/>
        </div>
        @endforeach
        @endif
        <a ignore="1" class="ui-arrow-control ui-arrow-next-page"></a>
        <a ignore="1" class="ui-arrow-control ui-arrow-previous-page"></a>
    </div>
</div>

<!-- controls -->
<div id="controls">
    <div class="all">
        <div class="ui-slider" id="page-slider">
            <div class="bar">
                <div class="progress-width">
                    <div class="progress">
                        <div class="handler"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui-options" id="options">
            <a class="ui-icon" id="ui-icon-table-contents">
              <i class="fa ion-navicon-round"></i>
          </a>
          <a class="ui-icon show-hint" title="Miniatures" id="ui-icon-miniature">
              <i class="fa ion-ios-list"></i>
          </a>
          <a class="ui-icon" id="ui-icon-zoom">
              <i class="fa ion-ios-browsers-outline"></i>
          </a>
          <a class="ui-icon show-hint" title="Share" id="ui-icon-share">
              <i class="fa ion-ios-redo"></i>
          </a>
          <a class="ui-icon show-hint" title="Full Screen" id="ui-icon-full-screen">
              <i class="fa ion-arrow-resize"></i>
          </a>
          <a class="ui-icon show-hint" id="ui-icon-toggle">
              <i class="fa fa-ellipsis-v"></i>
          </a>
      </div>

      <!-- zoom slider -->
      <div id="zoom-slider-view" class="zoom-slider">
        <div class="bg">
            <div class="ui-slider" id="zoom-slider">
                <div class="bar">
                    <div class="progress-width">
                        <div class="progress">
                            <div class="handler"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / zoom slider -->
</div>

<div id="ui-icon-expand-options">
    <a class="ui-icon show-hint"><i class="fa fa-ellipsis-h"></i></a>
</div>
</div>
<!-- / controls -->

<!-- miniatures -->
<div id="miniatures" class="ui-miniatures-slider">

</div>
<!-- / miniatures -->
</div>

  @include('includes.newsletter-form')
@include('includes.footer')
@endsection

@section('script')

<script type="text/javascript" src="{{Config::get('app.head_url')}}ecatalogue/underscore-min.js"></script>
<script type="text/javascript" src="{{Config::get('app.head_url')}}ecatalogue/backbone-min.js"></script>
<script type="text/javascript" src="{{Config::get('app.head_url')}}ecatalogue/turn.min.js"></script>
<script type="text/javascript" src="{{Config::get('app.head_url')}}ecatalogue/app.js"></script>

<script type="text/javascript">

  // Change these settings
  var count = $('flip-img').length;
  var url = window.location.pathname;
  var filename = url.substring(url.lastIndexOf('/')+1);
  //var myImg = document.querySelector(".flip-img");
  //var realWidth = myImg.naturalWidth / 2;
  //var realHeight = myImg.naturalHeight /2;
  //console.log('width' +(realWidth / 2), 'Height'+(realHeight / 2));
  FlipbookSettings = {
    
    options: {
      pageWidth: 1200,
      pageHeight: 1200,
      pages: count

  },
    
    pageFolder: 'product/ecatalogue',
    pageName: filename

    //loadRegions: false
};
$(window).load(function(event) {
    $("#flipbook .page-1").addClass('animation-on');
    
    
//alert(filename);
});
setTimeout(function(){
  $('.ui-menu').css({"display":"none"});  
}, 1000);

$(document).ready(function(){  
  
  $('body').find('.breadcrumbs').each(function(){
      $('.breadcrumbs').css({"position":"relative", "margin":"0px"});
  });
  
});
</script>


@endsection
