@extends('layouts.layout_home')
@section('style')
    <link rel="stylesheet" href="{{ Config::get('app.head_url') }}owl_carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ Config::get('app.head_url') }}owl_carousel/assets/owl.theme.default.min.css">
@endsection
@section('content')

@include('includes.header')

@if(!empty($slider))
		<section id="hero" class="hero-fullscreen parallax" data-overlay-dark="7">
  		 <div class="hero-slider">
  		@foreach($slider as $value)
        <div class="item">
        	<img src="{{ Config::get('app.head_url') }}slider/{{$value->slider_image}}">
        </div>
  		@endforeach
  		</div>
  </section>
@endif

@include('includes.footer')
@endsection
