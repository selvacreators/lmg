@extends('layouts.layout_home')
@section('content')
@include('includes.header')
@include('includes.breadcrumb')
    
    <main class="content-wrapper">
        <div class="page-content">
            <div class="contact-bg bg1"> <!-- /public -->
                <div class="contact_area" style="background-image: url('/frontend/images/parallax/contact-page-bg.jpg');">
                    <div class="container">
                        <div class="row">
                            <div class="text-center">
                                <div class="content-heading">
                                    <h2 class="text-uppercase">Store Locator</h2>
                                    <p>Come and Visit us at our Showroom - World Luxury Mattress Gallery, and try our products yourself</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-form-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="panel panel-default loc-panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Store Locator</h3>
                                </div>
                                <div class="panel-body panel-no-padding">
                                    <div class="row">
                                        <div class="col-md-4" style="padding:0px;">
                                            <div style="padding:10px;">
                                                <form role="form" id="loc-form" action="https://storelocatorscript.com/locator/demo/" accept-charset="utf-8" method="post">
                                                    <input name="latitude" type="hidden" id="form_latitude" value="">
                                                    <input name="longitude" type="hidden" id="form_longitude" value="">
                                                    <input name="iso2" value="" type="hidden" id="form_iso2">
                                                    <div class="input-group">
                                                        <input type="input" name="location" id="form_location" class="form-control" placeholder="Enter a Location" autofocus="autofocus">
                                                        <span class="input-group-btn">
                                                            <button class="btn search-btn" type="submit" style="border-right:none;"><i class="fa fa-search"></i>
                                                            </button>
                                                            <button class="btn loc-btn" type="button"><i class="fa fa-crosshairs"></i><span style="display:none;"> Obtaining Location</span></button>
                                                        </span>
                                                    </div>
                                                    <!-- multiple geocode result drop down -->
                                                    <div class="dropdown location-dropdown">
                                                        <a role="button" class="hide dropdown-toggle" data-toggle="dropdown"></a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dlabel">
                                                        </ul>
                                                    </div>
                                                    <input type="hidden" name="cats" id="form_cats" value="" autocomplete="off">
                                                </form>
                                            </div>
                                            <div class="result-set-container" style="">
                                                <div class="result-options" style="padding: 5px 10px; overflow: auto; display: none;">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-sm btn-default back-btn"><i class="fa fa-angle-double-left"></i></button>
                                                        <button type="button" class="btn btn-sm btn-default forward-btn"><i class="fa fa-angle-double-right"></i> </button>
                                                        <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#filter-modal"><i class="fa fa-filter"></i> Filter  </button>
                                                        <div class="pull-right text-muted btn-sm"><span class="total_items"></span> results in <span class="distance"></span></div>
                                                    </div>
                                                </div>
                                                <div class="list-group loc-result-set " style="margin-bottom:0px; max-height: 480px; overflow: auto;">
                                                    <div class="highlight list-group-item default-item" style="margin-bottom:0px;">
                                                        <p class="list-group-item-text"><strong>Please enter a location</strong></p>
                                                        <div>We'll show you the nearest store.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Contact Form -->
    </main>

@include('includes.newsletter-form')

@include('includes.footer')

@stop