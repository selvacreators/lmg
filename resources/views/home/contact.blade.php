@extends('layouts.layout_home')
@section('content')
@include('includes.header')
@include('includes.breadcrumb')
    
    <main class="content-wrapper">
        <div class="page-content">
            <div class="contact-bg bg1"> <!-- /public -->
                <div class="contact_area" style="background-image: url('{{Config::get('app.head_url')}}frontend/images/parallax/contact-page-bg.jpg');">
                    <div class="container">
                        <div class="row">
                            <div class="text-center">
                                <div class="content-heading">
                                    <h1 class="text-uppercase">Still Have Question?</h1>
                                    <p>Come and Visit us at our Showroom - World Luxury Mattress Gallery, and try our products yourself</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="text-center">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <h3 class="text-uppercase">Phone</h3>
                                    <a class="btn btn-default btn-primary" href="tel:{{str_replace(' ','',$csettings->phone)}}">Call Us</a>
                                    <p>{{$csettings->phone}}</p>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <h3 class="text-uppercase">Live Chat</h3>
                                    <a class="btn btn-default btn-primary" href="{{$csettings->chat}}">Live Now</a>
                                    <p><?php echo html_entity_decode($csettings->open_time);?></p>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <h3 class="text-uppercase">Email</h3>
                                    <a class="btn btn-default btn-primary" href="mailto:{{$csettings->email}}">Say Hi</a>
                                    <p><a href="mailto:{{$csettings->email}}">{{$csettings->email}}</a></p>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <h3 class="text-uppercase">LMG World</h3>
                                    <a class="btn btn-default btn-primary" href="https://goo.gl/maps/HSNzfdFFoa92" target="_blank">Map Me</a>
                                    <address><?php echo html_entity_decode($csettings->address);?></address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-form-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6 col-xs-12 padding-top40 margin-bottom60">
                            <h2 class="text-uppercase text-center margin-bottom15">OR USE THIS FORM</h2>
                            @if(Session::has('enq_msg'))
                                <div class="alert alert-success">
                                    {{Session::get('enq_msg')}}
                                </div>
                            @endif
                            <form action="{{ URL('enquiry')}}" class="form contact" id="contact-form" method="post">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                <div class="form-group required">
                                    <label for="name">Name</label><input class="input-text form-control" data-placement="bottom" id="name" name="name" type="text" required>
                                </div>
                                <div class="form-group required"><label for="email">Email</label> <input class="input-text form-control" data-placement="bottom" id="email" name="email" type="email" required></div>
                                <div class="form-group"><label for="telephone">Phone</label> <input class="input-text form-control" data-placement="bottom" id="telephone" name="telephone" type="text"></div>
                                <div class="form-group required"><label for="content">Content</label><textarea class="input-text form-control" cols="5" id="content" name="content" rows="4" data-placement="bottom" required></textarea></div>
                                <button class="action submit btn btn-primary" title="Send Messages" name="submit" value="Contact Enquiry" type="submit"><span>Send Message</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-collection contact-bg">
                <div class="contact_area parallax"> <!-- /public -->
                    <div class="background-image">
                        <img src="{{Config::get('app.head_url')}}frontend/images/parallax/contact-page-bg-2.jpg">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="content-heading text-center">
                                <?php $locale = url(Session::get('locale'));?>
                                <h2 class="text-uppercase">INTERESTED IN A GOOD NIGHT'S SLEEP?</h2>
                                <a class="btn btn-default btn-primary" href="{{ URL($locale.'/'.'brands')}}/">Check our collection</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Contact Form -->
    </main>

@include('includes.newsletter-form')

@include('includes.footer')

@stop