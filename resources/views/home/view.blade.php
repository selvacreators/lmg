@extends('layouts.layout_home')
@section('content')
@include('includes.header')
  <main class="content">
    <div class="page-content">
      <div class="container_overlay">
        <!-- Hero -->
        <section id="hero" class="hero-fullscreen parallax" data-overlay-dark="8">  
          <div class="background-image">                      
            <!-- <video autoplay="true" data-youtube-video-id="gMOw8F_qrlo" id="vossen-youtube"></video> -->
            <div class="hero-section">
              
              <video autoplay="" loop="" muted="" class="video-bg-hero">
               <!--  <source src="{{ Config::get('app.head_url') }}Home/img/video/lmg.mp4" type="video/mp4">
                <source src="{{ Config::get('app.head_url') }}Home/img/video/lmg.mp4" type="video/webm"> -->
                  @if($video[0]->lang_code == Session::get('locale'))
                  <source src="slidervideo/{{$video[0]->upload_video}}" type="video/mp4">
                  <source src="slidervideo/{{$video[0]->upload_video}}" type="video/webm">
                  @else
                  <source src="slidervideo/{{$video[0]->upload_video}}" type="video/mp4">
                  <source src="slidervideo/{{$video[0]->upload_video}}" type="video/webm">
                  @endif
              </video>
            </div>
          </div>
        </section>
        <!-- End Hero --> 
       </div>             
        <!-- About Us -->
    </div>
  <!-- End Contact Form -->
  </main>
@include('includes.footer')
@endsection