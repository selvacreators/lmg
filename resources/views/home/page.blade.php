@extends('layouts.layout_home')

@section('content')

@include('includes.header')

@include('includes.breadcrumb')

@if(!empty($getdata))

  @if(!empty($getdata->content_heading))
  <div class="page-title-wrapper">
    <div class="container">
      <div class="row">
        <div class="text-center">
          <div class="content-heading">
            <h1 class="title">{{$getdata->content_heading}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
  <main class="content-wrapper">
    <div class="page-content">
      <?php echo html_entity_decode($getdata->content);?>
    </div>
    <!-- End Contact Form -->
  </main>
  @include('includes.newsletter-form')
@else
  <main class="content-wrapper">
    <section class="page-content">
      <div style="min-height: 514px;">
        <h2 style="min-height: 100px;padding-top: 250px;padding-bottom: 80px;"><center>page under construction</center></h2>
      </div>
    </section>
  </main>
@endif

@include('includes.footer')

@endsection