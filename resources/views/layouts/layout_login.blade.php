<!DOCTYPE html>
<html>
<head>
	<title>login Page</title>
	<link href="{{ Config::get('app.head_url') }}assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ Config::get('app.head_url') }}assets/css/bootstrapValidator.min.css" rel="stylesheet">
    <link href="{{ Config::get('app.head_url') }}assets/css/font-awesome.css" rel="stylesheet">
    <link href="{{ Config::get('app.head_url') }}assets/css/styles.css" rel="stylesheet">
    <script type="text/javascript" src="{{ Config::get('app.head_url') }}assets/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="{{ Config::get('app.head_url') }}assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ Config::get('app.head_url') }}assets/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="{{ Config::get('app.head_url') }}assets/js/login.js"></script>
    <style type="text/css">
    .page-head-line {
        font-weight: 900;
        padding-bottom: 20px;
        border-bottom: 2px solid #000000;
        text-transform: initial;
        color: #fff;
        font-size: 19px;
        margin-bottom: 40px;
    }
    </style>
</head>
<body>
@yield('content')
</body>
</html>