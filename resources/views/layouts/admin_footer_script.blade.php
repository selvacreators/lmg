
<!--   Core JS Files   -->
<!--   Core JS Files   -->
<script src="{{ Config::get('app.head_url') }}assets/js/core/jquery.min.js"></script>

<script src="{{ Config::get('app.head_url') }}assets/js/core/popper.min.js"></script>

<script src="{{ Config::get('app.head_url') }}assets/js/bootstrap-material-design.js"></script>

<script src="{{ Config::get('app.head_url') }}assets/js/plugins/arrive.min.js" type="text/javascript"></script>


<script src="{{ Config::get('app.head_url') }}assets/js/jquery.validate.js"></script>

<script src="{{ Config::get('app.head_url') }}assets/js/bootstrap-multiselect.js"></script>

<script src="{{ Config::get('app.head_url') }}assets/js/plugins/bootstrap-notify.js"></script>
<script src="{{ Config::get('app.head_url') }}assets/js/plugins/chartist.min.js"></script>
<script src="{{ Config::get('app.head_url') }}assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="{{ Config::get('app.head_url') }}assets/js/plugins/demo.js"></script>

@yield('date-script')

<script src="{{ Config::get('app.head_url') }}assets/js/material-dashboard.js?v=2.0.0"></script>

<script src="{{ Config::get('app.head_url') }}admin/js/common.js"></script>

<script src="{{ Config::get('app.head_url') }}admin/datatables/js/jquery.dataTables.min.js"></script>

<script src="{{ Config::get('app.head_url') }}admin/datatables/js/dataTables.responsive.min.js"></script>

@yield('admin-script')

<script type="text/javascript">
    $(document).ready(function() {

        $('.datatable').dataTable({
          "pageLength": 20
        });

        //init wizard

        // demo.initMaterialWizard();

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();
        demo.initCharts();
      
    });
$( document ).ready(function() {
    $('#alertbox').modal('show'); 
});
</script>

