<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ Config::get('app.head_url') }}assets/img/apple-icon.png">
    <link rel="icon" href="{{ Config::get('app.head_url') }}assets/img/favicon.png">
    <title>LMG Administration</title>
    <!--     Fonts and icons     -->
   
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
   
    <link rel="stylesheet" href="{{ Config::get('app.head_url') }}assets/css/material-dashboard.css?v=2.0.0">  
    <link href="{{ Config::get('app.head_url') }}assets/assets-for-demo/demo.css" rel="stylesheet" />
    <link href="{{ Config::get('app.head_url') }}assets/css/bootstrap-multiselect.css"  rel="stylesheet"/>
    <!-- dataTables -->
    <link href="{{ Config::get('app.head_url') }}admin/datatables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="{{ Config::get('app.head_url') }}admin/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{ Config::get('app.head_url') }}assets/css/bootstrapValidator.min.css" rel="stylesheet">
    
    <style type="text/css" media="screen">
        .pagination>li>a, .pagination>li>span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #428bca;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }    
        .multiselect-container>li>a>label.radio, .multiselect-container>li>a>label.checkbox{
            width: 100%;
        }
    </style>
    @yield('admin-style')
</head>
<body class="">
    <div class="wrapper">    	 
    	<header id="header" class="navbar navbar-static-top fixed-top">
    		<!-- Navbar -->
    		<div class="navbar-header">
    			<div class="logo">
			        <a type="button" id="button-menu" class="pull-left"><i class="fa fa-dedent fa-lg"></i></a>
                    <a href="/Admin/dashboard" class="simple-text logo-normal">Creative LMG WORLD</a>
                     <!-- http://www.creative-tim.com -->
			    </div>
			</div>
    		<ul class="nav pull-right">
				<li><a href="/Admin/auth/logout"><span class="hidden-xs hidden-sm hidden-md">Logout</span> <i class="fa fa-sign-out fa-lg"></i></a></li>
			</ul>
    	</header>
       
    	<nav id="column-left" class="sidebar">
            <div class="sidebar-wrapper">
                <ul id="menu" class="nav">
                    <li id="dashboard" class="{{ Request::is('Admin/dashboard') ? 'active' : '' }} ">
                        <a href="/Admin/dashboard">
                            <i class="material-icons">dashboard</i>
                            <span>Dashboard	</span>
                        </a>
                    </li>
                    @if(Session::get('user_type') == 5)
                    <li id="users" class="{{ Request::is('Admin/users') ? 'active' : '' }}">
                        <a href="/Admin/users">
                            <i class="material-icons">person</i>
                            <span>Admin Users Profile</span>
                        </a>
                    </li>
                    @endif
                    <li class="{{ Request::is('Admin/common-setting') ? 'active' : '' }}">
                        <a class="parent">
                            <i class="material-icons">bubble_chart</i>
                            <span>Enquiry</span>
                        </a>
                        <ul>
                            <li><a href="/Admin/contact-enquiry">Contact Enquiry</a></li>
                            <li><a href="/Admin/newsletter-subscribers">Newsletter Subscribers</a></li>
                            <!-- <li><a href="/Admin/member-subscribers">Member Subscribers</a></li> -->
                        </ul>
                    </li>
                      <li class="{{ Request::is('Admin/catalog') ? 'active' : '' }}">
                         <a class="parent">
                       <!--  <a href="/Admin/product"> -->
                            <i class="material-icons">content_paste</i>
                            <span>Catalog</span>
                        </a>
                        <ul>
                          <li><a href="/Admin/categories">categories</a></li>
                          <li><a href="/Admin/product">Product</a></li>
                          <li><a href="/Admin/product-type">Product Type</a></li>
                          <li><a href="/Admin/review">Product Review</a></li>
                          <li><a href="/Admin/question">Product Questions</a></li>
                          <!-- <li><a href="/Admin/stocks">Stock Status</a></li> -->
                          <li><a href="/Admin/weight">Weight</a></li>
                          <li><a href="/Admin/length">Length</a></li>
                          <li><a class="parent" >Attribute</a>
                            <ul>
                                <li><a href="/Admin/attribute">Attribute</a></li>
                                <li><a href="/Admin/attribute_group">Attribute Group</a></li>
                            </ul>
                          </li>
                          <li><a href="/Admin/option">Option</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('Admin/menu') ? 'active' : '' }}">
                        <a class="parent">
                            <i class="material-icons">bubble_chart</i>
                            <span>Menus</span>
                        </a>

                        <ul>
                                  <li><a href="/Admin/menutype">Main Menu</a></li>
                                  <li><a href="/Admin/AddSubMenu">Submenu</a></li>
                            </ul>
                    </li>
                    <li class="{{ Request::is('Admin/slider') ? 'active' : '' }}">
                        <a class="parent">
                            <i class="material-icons">bubble_chart</i>
                            <span>Banner</span>
                        </a>

                        <ul>
                                <li><a href="/Admin/slider">Home Sliders</a></li>
                                <li><a href="/Admin/videos">Home VideoBanner</a></li>
                            </ul>
                    </li>
                    <li class="{{ Request::is('Admin/Promotation') || Request::is( route('admin.headerPromo') ) ? 'active' : '' }}">
                        <a class="parent">
                            <i class="material-icons">bubble_chart</i>
                            <span>Promotion</span>
                        </a>
                        <ul>
                            <li><a href="{{route('admin.headerPromo')}}">Header Promotion</a></li>
                            <li><a href="/Admin/newsletter_popup">NewsLetter Popup</a></li>
                            <li><a href="/Admin/promo_banner">Promo Banner</a></li>
                            <li><a href="/Admin/exit_intent">Exit Intent Popup</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('Admin/pages') ? 'active' : '' }}">
                        <a class="parent">
                            <i class="material-icons">bubble_chart</i>
                            <span>Content</span>
                        </a>
                       
                             <ul>
                                  <li><a href="/Admin/pages">Pages</a></li>
                                  
                            </ul>
                        
                    </li>
                    <li class="{{ Request::is('Admin/ecatalogue') ? 'active' : '' }}">
                        <a href="/Admin/ecatalogue">
                            <i class="material-icons">location_ons</i>
                            <span>E-Catalogue</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('Admin/mattress-select') ? 'active' : '' }}">
                        <a href="/Admin/mattress-select">
                            <i class="material-icons">notifications</i>
                            <span>Mattress Selector</span>
                        </a>
                    </li>
                    {{-- <li class="{{ Request::is('Admin/refer') ? 'active' : '' }}">
                        <a href="/Admin/refer">
                            <i class="material-icons">unarchive</i>
                            <span>Refer a Friend</span>
                        </a>
                    </li> --}}
                    <li class="{{ Request::is('Admin/e-warranty') ? 'active' : '' }}">
                        <a href="/Admin/e-warranty">
                            <i class="material-icons">unarchive</i>
                            <span>E-Warranty</span>
                        </a>
                    </li>

                   
                     <li class="{{ Request::is('Admin/blog') ? 'active' : '' }}">
                        <a class="parent">
                            <i class="material-icons">bubble_chart</i>
                            <span>Blog</span>
                        </a>
                       
                             <ul>
                                <!-- <li><a href="/admin/blog/feature-slider">Blog Feature Sliders</a></li> -->
                                  <li><a href="/admin/blog">Manage Posts</a></li>
                                  <li><a href="/admin/blog/category">Manage Catagory</a></li>
                                  <!-- <li><a href="/Admin/blog_comment">Manage Comment</a></li> -->
                                  <!-- <li><a href="/admin/blog/rss">Manage RSS</a></li> -->
                                  <li><a href="/admin/blog/setting">Blog Setting</a></li>
                            </ul>
                    </li>
                    {{-- <li id="catalog" class="{{ Request::is('Admin/catalog') ? 'active' : '' }}">
                        <a href="/Admin/footer">
                            <i class="material-icons">content_paste</i>
                            <span>footer</span>
                        </a>
                    </li> --}}
                    <li id="catalog" class="{{ Request::is('Admin/brands') ? 'active' : '' }}">
                        <a href="/Admin/brands">
                            <i class="material-icons">content_paste</i>
                            <span>Brands</span>
                        </a>
                    </li>
                    <li id="catalog" class="{{ Request::is('Admin/collection') ? 'active' : '' }}">
                        <a href="/Admin/collection">
                            <i class="material-icons">content_paste</i>
                            <span>Collection</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('Admin/common-setting') ? 'active' : '' }}">
                        <a class="parent">
                            <i class="material-icons">bubble_chart</i>
                            <span>Setting</span>
                        </a>
                        <ul>
                            <li><a href="/Admin/common-setting">Common Setting</a></li>
                            <li><a href="/Admin/brandsetting">Brand setting</a></li>
                            <li><a href="/Admin/manage-store-locators">Manage Store Locators</a></li>
                            <li><a href="/Admin/language">language</a></li>
                            <li><a href="/Admin/chats">Chats</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    	<div id="content">@yield('content')</div>
    </div>
</body>
@include('layouts.admin_footer_script')
</html>