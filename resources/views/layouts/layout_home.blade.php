<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

@if(isset($metaData['meta_title']) && trim($metaData['meta_title']) != '' )
        <title>{{ $metaData['meta_title'] }} | LMG World</title>
@else
        <title>Welcome To World Luxury Mattress Gallery | LMG World</title>
@endif

@if(isset($metaData['meta_desc']) && trim($metaData['meta_desc']) != '' )
        <meta name="description" content="{{$metaData['meta_desc']}}" />
        
@else
        <meta name="description" content="Being a symbol of luxury lifestyle, LMG World is proud to be an exclusive distributor in Vietnam and South East Asia providing the world class mattresses." />
       
@endif

@if(isset($metaData['meta_keyword']) && trim($metaData['meta_keyword']) != '' )
        <meta name="keyword" content="{{$metaData['meta_keyword']}}" />
@else
        <meta name="keyword" content="LMG World" />
@endif
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Open Graph data -->
@if(isset($metaData['meta_title']) && trim($metaData['meta_title']) != '' )        
        <meta property="og:title" content="{{ $metaData['meta_title'] }} | LMG World" />
@else
        <meta property="og:title" content="Welcome To World Luxury Mattress Gallery | LMG World" />
@endif

@if(isset($metaData['meta_desc']) && trim($metaData['meta_desc']) != '' )       
        <meta property="og:description" content="{{$metaData['meta_desc']}}" />
@else
        <meta property="og:description" content="Being a symbol of luxury lifestyle, LMG World is proud to be an exclusive distributor in Vietnam and South East Asia providing the world class mattresses." />        
@endif       
        <meta property="og:url" content="{{ Request::url() }}" />
        <meta property="og:type" content="website" />
        @if(isset($csetting))
        <meta property="og:site_name" content="{{$csetting->store_name}}" />
        <meta property="og:image" content="{{URL(Config::get('app.head_url').'logo/'.$csetting->store_logo)}}" />
        @endif

       <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary" />
@if(isset($metaData['meta_title']) && trim($metaData['meta_title']) != '' )
        <meta name="twitter:title" content="{{ $metaData['meta_title'] }} | LMG World">
@else
        <meta name="twitter:title" content="Welcome To World Luxury Mattress Gallery | LMG World"> 
@endif
 @if(isset($metaData['meta_desc']) && trim($metaData['meta_desc']) != '' )
        <meta name="twitter:description" content="{{$metaData['meta_desc']}}">
 @else
        <meta name="twitter:description" content="Being a symbol of luxury lifestyle, LMG World is proud to be an exclusive distributor in Vietnam and South East Asia providing the world class mattresses.">        
 @endif        
        <meta name="twitter:site" content="{{'@'.$social->twitter_user}}" />
        <meta name="twitter:creator" content="{{'@'.$social->twitter_user}}" />
        @if(isset($csetting))
        <meta name="twitter:image" content="{{URL(Config::get('app.head_url').'logo/'.$csetting->store_logo)}}">
        @endif

        {{--@if(!empty($social))--}}
        <meta property="fb:app_id" content="{{$social->facebook_app_id}}" />
        {{--@endif--}}
        
        <meta name="identifier-URL" content="{{URL()}}">
        <link rel="canonical" href="{{URL()}}" />
        <link href="{{ Config::get('app.head_url') }}frontend/img/assets/favicon.png" rel="icon" type="image/png"> 
        <link href="{{ Config::get('app.head_url') }}frontend/css/init.css" rel="stylesheet" type="text/css">
        <link href="{{ Config::get('app.head_url') }}frontend/css/ion-icons.min.css" rel="stylesheet" type="text/css">
        <link href="{{ Config::get('app.head_url') }}frontend/css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css">       
        <link href="{{ Config::get('app.head_url') }}frontend/css/theme.css" rel="stylesheet" type="text/css">  
        <link href="{{ Config::get('app.head_url') }}frontend/css/custom.css" rel="stylesheet" type="text/css">
        <link href="{{ Config::get('app.head_url') }}frontend/css/responsive.css" rel="stylesheet" type="text/css">
        
        @yield('style')
        @if(isset($Exist_Intent) && sizeof($Exist_Intent))
         <link rel="stylesheet" href="{{ Config::get('app.head_url') }}frontend/css/ouibounce.css">
        @endif
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Poppins:400,700|Hind:400,300|Barlow+Condensed:400,700|Cookie:400,700|Cuprum:400,700" rel="stylesheet" type="text/css"> -->

        <!-- Google Webmaster Tool -->
        <!-- <meta name="google-site-verification" content="McE-k0kxp6eut3V5r9j76uDWnVWAmWX41pscPcLbNO8" /> -->
        <!-- End Google Webmaster Tool -->
       

 

    </head>
    <?php
    if($currentPath == 'index'){
        $classpname = 'home';
        $pa_view = "";
    }
    else
    {
        $classpname = $currentPath;
        $pa_view = "cms-page-view";
    }
    ?>
    <!--<body data-fade-in="true" class="cms-home">-->
    <body data-fade-in="true" class="cms-<?php echo $classpname; ?> <?php echo $pa_view; ?>">
    <div class="pre-loader"><div></div></div>
    @yield('content')
    @include('includes.script')
    @yield('script')
    </body>
</html>