@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/edit_chat','class'=>'form-horizontal chat-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="chatid" value="{{$chat[0]->chat_id}}">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/chats" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Chat</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="language" class="form-control" id="language">
                                            @foreach($language as $lng)
                                                @if($lng->lang_code == $chat[0]->lang_code)
                                                    <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                                @else
                                                    <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="chatname" class="control-label col-sm-3">Chat Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="chatname" name="chatname" value="{{$chat[0]->chat_name}}" placeholder="LMG WOLRD EN live chat">
                                    </div>
                                </div>
                                <div class="form-group row" id="textareaeditor">
                                    <label class="control-label col-sm-3" for="script_code">Live Script:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea rows="6" cols="50" name="script_code" id="script_code" class="form-control">{{$chat[0]->chat_script}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?=(($chat[0]->status=='1')?'selected':'');?>>Enable</option>
                                            <option value="2" <?=(($chat[0]->status=='2')?'selected':'');?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".chat-form").validate({
    rules: {
        language: "required",
        chatname : "required",
        script_code : "required",
        status : "required"
    },
    messages: {
      language: "Please select language",
      chatname: "Please enter chat name",
      script_code: "Please enter script code",
      status: "Please select status"
    }
  });
});
</script>
@endsection