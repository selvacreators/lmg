@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
              <div class="card-header">
                <a href="/Admin/add/mattress" class="btn btn-success btn-sm pull-right">Add New Questions</a>
                <h3 class="card-title">Mattress Questions List</h3>
              </div>
              <div class="card-body">
                <nav class="nav-container">
                  <ul class='nav nav-tabs text-center' role="tablist">
                    @if($language)
                    @foreach($language as $key =>$lan)
                    @if($lan->lang_code == 'en')
                    <?php $check = "active";?>
                    @else
                    <?php $check = "";?>
                    @endif
                    <li><a data-toggle="tab" class="{{$check}} show" href="#{{$lan->lang_name}}">{{$lan->lang_name}}</a></li>
                    @endforeach
                    @endif
                  </ul>
                </nav>
                <div class="tab-content pt-0 pb-0">
                  @if($language)
                  @foreach($language as $key =>$lan)
                  @if($lan->lang_code == 'en')
                  <?php $check = "active";?>
                  @else
                  <?php $check = "";?>
                  @endif
                  <div id="{{$lan->lang_name}}" class="tab-pane {{$check}} in fieldset">
                    <table id="datatable" class="datatable table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Language</th>
                          <th>Buy Mattress</th>
                          <th>Recommanded Product</th>
                          <th>Status</th>
                          <th class="col-xs-2">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i=1;?>
                        @if($view_mattress)
                        @foreach($view_mattress as $row)
                        @if($row->lang_code == $lan->lang_code)
                        <tr>
                          <td>{{$i++}}</td>
                          <td>{{$row->lang_name}}</td>
                          <td>{{$row->buying_mattress}}</td>
                          <td>{{$row->buying_mattress}}</td>
                          <td>
                            @if($row->status == '1')
                            <span>Enable</span>
                            @else
                            <span>Disable</span>
                            @endif
                          </td>
                          <td>
                            <a href="/Admin/editMattress_selector/{{$row->admin_matt_id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                            <a href="/Admin/deleteMattress_selector/{{$row->admin_matt_id}}" onclick="return confirm('Are You Sure Delete Mattress?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o "></i></button></a>
                          </td>
                        </tr>
                        @endif
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
                  @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('admin-script')
@endsection