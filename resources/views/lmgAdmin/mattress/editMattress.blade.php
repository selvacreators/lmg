@extends('layouts.layout_admin')
@section('admin-style')
<style type="text/css">
    .label-radio label{
        font-weight: normal;
        padding-left: 20px;
    }
</style>
@endsection
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="col-xs-12">
                {!! Form::open(array('url' => '/Admin/edit_mattress','class'=>'form-horizontal select-mattress-form','method'=>'post','enctype'=>'multipart/form-data')) !!}
                <input name="mattress_id" type="hidden" value="{{$mattress[0]->admin_matt_id}}">
                <div class="card">
                    <div class="card-header">
                      <div class="pull-right">
                        <button class="btn btn-sm btn-success">Save</button>
                        <button class="btn btn-sm btn-danger" type="reset">Reset<div class="ripple-container"></div></button>
                        <a href="/Admin/mattress-select" class="btn btn-primary btn-sm">Back</a>
                      </div>
                      <h3 class="card-title">Updated Mattress</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                          <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                          <div class="col-sm-9">
                            <select name="lang_code" id="lang_code" class="form-control" required="required">
                              <option value="">Select</option>
                              @foreach($language as $value)
                              @if($value->lang_code == $mattress[0]->lang_code)
                              <option value="{{$value->lang_code}}" selected="selected">{{$value->lang_name}}</option>
                              @else
                              <option value="{{$value->lang_code}}">{{$value->lang_name}}</option>
                              @endif
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3 text-right"> 
                                <label class="control-label" for="Language">Questions:</label>
                            </div>
                            <div class="col-md-9">
                                <div id="buying_Mattress">
                                    <div>
                                        <label class="control-label" for="Language">buying mattress?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="buying" class="experience" value="Myself" data-title="myself" <?php if($mattress[0]->buying_mattress == 'Myself'){ echo "checked";}?>> Myself 
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="buying" class="experience" value="My partner & I" data-title="mypartner" <?php if($mattress[0]->buying_mattress == 'My partner & I'){ echo "checked";}?>> My partner & I 
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="buying" class="experience" value="My partner, I & My kids" data-title="mypartner_kids" <?php if($mattress[0]->buying_mattress == 'My partner, I & My kids'){ echo "checked";}?>> My partner, I & My kids

                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="buying" class="experience" value="Just for the spare bedroom" data-title="spare_bedroom" <?php if($mattress[0]->buying_mattress == 'Just for the spare bedroom'){ echo "checked";}?>> Just for the spare bedroom 
                                        </label>
                                    </div>
                                </div>
                                <div id="new_mattress">
                                    <div>
                                        <label class="control-label mt30" for="Language">Why are you looking for a New mattress?</label>
                                    </div>
                                    
                                    <div class="label-radio">
                                        <label class="radio">
                                            <input type="radio" name="new_mattress" class="experience" value="My current mattress is old and worn out" <?php if($mattress[0]->mattress_new == 'My current mattress is old and worn out'){ echo "checked";}?>> My current mattress is old and worn out 
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="new_mattress" class="experience" value="I am getting married or moving in with someone" <?php if($mattress[0]->mattress_new == 'I am getting married or moving in with someone'){ echo "checked";}?>> I am getting married or moving in with someone 
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="new_mattress" class="experience" value="I need a better quality mattress to improve my quality of sleep" <?php if($mattress[0]->mattress_new == 'I need a better quality mattress to improve my quality of sleep'){ echo "checked";}?>> I need a better quality mattress to improve my quality of sleep
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="new_mattress" class="experience" value="I recently had a great night’s sleep at a hotel and want to look into purchasing this mattress" <?php if($mattress[0]->mattress_new == 'I recently had a great night’s sleep at a hotel and want to look into purchasing this mattress'){ echo "checked";}?>> I recently had a great night’s sleep at a hotel and want to look into purchasing this mattress 
                                        </label>
                                    
                                    </div>
                                </div>

                                <div id="your_weight_range">
                                    <div>
                                        <label class="control-label mt30" for="Language">Please select your weight range?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="weight_range" class="experience" value="<50 kg" <?php if($mattress[0]->weight_range == '<50 kg'){ echo "checked";}?>> <50 kg
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="weight_range" class="experience" value="50-60 kg" <?php if($mattress[0]->weight_range == '50-60 kg'){ echo "checked";}?>> 50-60 kg
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="weight_range" class="experience" value="60-70 kg" <?php if($mattress[0]->weight_range == '60-70 kg'){ echo "checked";}?>> 60-70 kg
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="weight_range" class="experience" value=">70 kg" <?php if($mattress[0]->weight_range == '>70 kg'){ echo "checked";}?>> >70 kg 
                                            </label> 
                                                     
                                    </div>                                  
                                </div>

                                <div id="your_Partner_weight_range">
                                    <div>
                                        <label class="control-label  mt30" for="Language">Please select your Partner weight range?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="Partner_weight_range" class="experience" value="<50 kg" <?php if($mattress[0]->partner_weight_range == '<50 kg'){ echo "checked";}?>> <50 kg
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="Partner_weight_range" class="experience" value="50-60 kg" <?php if($mattress[0]->partner_weight_range == '50-60 kg'){ echo "checked";}?>> 50-60 kg
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="Partner_weight_range" class="experience" value="60-70 kg" <?php if($mattress[0]->partner_weight_range == '60-70 kg'){ echo "checked";}?>> 60-70 kg
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="Partner_weight_range" class="experience" value=">70 kg" <?php if($mattress[0]->partner_weight_range == '>70 kg'){ echo "checked";}?>> >70 kg 
                                            </label> 
                                            
                                    </div>
                                </div>

                                <div id="your_age_range">
                                    <div>
                                        <label class="control-label  mt30" for="Language">Please select your age range?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="age_range" class="experience" value="<30 yrs" <?php if($mattress[0]->age_range == '<30 yrs'){ echo "checked";}?>> <30 yrs
                                            </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="age_range" class="experience" value="30-35 yrs" <?php if($mattress[0]->age_range == '30-35 yrs'){ echo "checked";}?>> 30-35 yrs
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="age_range" class="experience" value="36-45 yrs" <?php if($mattress[0]->age_range == '36-45 yrs'){ echo "checked";}?>> 36-45 yrs
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="age_range" class="experience" value="46-55 yrs" <?php if($mattress[0]->age_range == '46-55 yrs'){ echo "checked";}?>> 46-55 yrs
                                        </label> 
                                        <label class="radio-inline">
                                            <input type="radio" name="age_range" class="experience" value=" <?php if($mattress[0]->age_range == '>55 yrs'){ echo "checked";}?>>55 yrs">>55 yrs
                                        </label> 
                                            
                                    </div>
                                </div>

                                <div id="your_partner_range">

                                    <div>
                                        <label class="control-label  mt30" for="Language">Please select your Partner age range?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="Partner_age_range" class="experience" value="<30 yrs" <?php if($mattress[0]->partner_age_range == '<30 yrs'){ echo "checked";}?>> <30 yrs
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="Partner_age_range" class="experience" value="30-35 yrs" <?php if($mattress[0]->partner_age_range == '30-35 yrs'){ echo "checked";}?>> 30-35 yrs
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="Partner_age_range" class="experience" value="36-45 yrs" <?php if($mattress[0]->partner_age_range == '36-45 yrs'){ echo "checked";}?>> 36-45 yrs
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="Partner_age_range" class="experience" value="46-55 yrs" <?php if($mattress[0]->partner_age_range == '46-55 yrs'){ echo "checked";}?>> 46-55 yrs
                                            </label> 
                                            <label class="radio-inline">
                                                <input type="radio" name="Partner_age_range" class="experience" value=">55 yrs" <?php if($mattress[0]->partner_age_range == '>55 yrs'){ echo "checked";}?>>>55 yrs
                                            </label> 
                                           
                                    </div>
                                </div>

                                <div id="your_position">
                                    <div>
                                        <label class="control-label  mt30" for="Language">what is Your usual Sleep Position You?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="your_sleep_position" class="experience" value="Back" <?php if($mattress[0]->your_sleep_position == 'Back'){ echo "checked";}?>> back
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="your_sleep_position" class="experience" value="Stomach" <?php if($mattress[0]->your_sleep_position == 'Stomach'){ echo "checked";}?>> Stomach
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="your_sleep_position" class="experience" value="Side" <?php if($mattress[0]->your_sleep_position == 'Side'){ echo "checked";}?>> Side
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="your_sleep_position" class="experience" value="Toss & turn throughout the night" <?php if($mattress[0]->your_sleep_position == 'Toss & turn throughout the night'){ echo "checked";}?>> Toss & turn throughout the night</label>
                                                  
                                    </div>
                                </div>

                                <div id="your_Partner_position">
                                    <div>
                                        <label class="control-label  mt30" for="Language">what is Your usual Sleep Position Partner?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="partner_sleep_position" class="experience" value="Back" <?php if($mattress[0]->partner_sleep_position == 'Back'){ echo "checked";}?>> back
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="partner_sleep_position" class="experience" value="Stomach" <?php if($mattress[0]->partner_sleep_position == 'Stomach'){ echo "checked";}?>> Stomach
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="partner_sleep_position" class="experience" value="Side" <?php if($mattress[0]->partner_sleep_position == 'Side'){ echo "checked";}?>> Side
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="partner_sleep_position" class="experience" value="Toss & turn throughout the night" <?php if($mattress[0]->partner_sleep_position == 'Toss & turn throughout the night'){ echo "checked";}?>>Toss & turn throughout the night
                                        </label>
                                        <div class="clearfix"></div>             
                                    </div>
                                </div>

                                <div id="after_sleep">
                                    <div>
                                        <label class="control-label  mt30" for="Language">Do you experience any issues during or after sleep?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="issues" class="experience" value="HIP pain" <?php if($mattress[0]->issues == 'HIP pain'){ echo "checked";}?>> HIP pain
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="issues" class="experience" value="Shoulder pain" <?php if($mattress[0]->issues == 'Shoulder pain'){ echo "checked";}?>> Shoulder pain
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="issues" class="experience" value="Back pain" <?php if($mattress[0]->issues == 'Back pain'){ echo "checked";}?>> Back pain
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="issues" class="experience" value="NONE" <?php if($mattress[0]->issues == 'NONE'){ echo "checked";}?>>NONE </label>
                                            <div class="clearfix"></div>             
                                    </div>
                                </div>

                                <div id="hot_cool">
                                    <div>
                                        <label class="control-label  mt30" for="Language">Do you sleep too hot or too cold?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="sleep_too" class="experience" value="Cool" <?php if($mattress[0]->sleep_too == 'Cool'){ echo "checked";}?>> Cool
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sleep_too" class="experience" value="Hot" <?php if($mattress[0]->sleep_too == 'Hot'){ echo "checked";}?>> Hot
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sleep_too" class="experience" value="No Issue" <?php if($mattress[0]->sleep_too == 'No Issue'){ echo "checked";}?>>No Issue
                                        </label>
                                        
                                    </div>
                                </div>

                                <div id="big_people">
                                    <div>
                                        <label class="control-label  mt30" for="Language">What size bed are you looking for - big enough for 2 people?</label>
                                    </div>
                                    <div>
                                        <label class="radio-inline">
                                            <input type="radio" name="sleep_guest" class="experience" value="Yes, my guests need space" <?php if($mattress[0]->sleep_guest == 'Yes, my guests need space'){ echo "checked";}?>> Yes, my guests need space
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sleep_guest" class="experience" value="No, my guests are usually single" <?php if($mattress[0]->sleep_guest == 'No, my guests are usually single'){ echo "checked";}?>> No, my guests are usually single
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sleep_guest" class="experience" value="Dont know" <?php if($mattress[0]->sleep_guest == 'Dont know'){ echo "checked";}?>>Don't know
                                        </label>
                                        
                                    </div>
                                </div>

                                <div id="final-result">
                                    <div>
                                        <label class="mt30" for="Language">On which surface do you think you would have the most comfortable sleep? (How firm would you like your mattress?)
                                        </label>
                                    </div>
                                    <div class="label-radio">
                                        <label class="radio">
                                            <input type="radio" name="comfortable_sleep" class="experience" value="I like my mattress to be really soft and forgiving" <?php if($mattress[0]->comfortable_sleep == 'I like my mattress to be really soft and forgiving'){ echo "checked";}?>> I like my mattress to be really soft and forgiving
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="comfortable_sleep" class="experience" value="I want my mattress to be soft, with just a bit of resistance" <?php if($mattress[0]->comfortable_sleep == 'I want my mattress to be soft, with just a bit of resistance'){ echo "checked";}?>> I want my mattress to be soft, with just a bit of resistance
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="comfortable_sleep" class="experience" value="I prefer my mattress to be firm, with just a bit of give" <?php if($mattress[0]->comfortable_sleep == 'I prefer my mattress to be firm, with just a bit of give'){ echo "checked";}?>>I prefer my mattress to be firm, with just a bit of give
                                        </label>
                                        
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="control-label col-sm-3" for="txtsubmenulink">Recommendation Mattress:</label>
                            @foreach($matt_selector_product as $key1 => $recom_pro)

                                  @foreach($recom_pro->product_recommandation as $key2 => $recom_val)
                                    <?php $pro[] = $recom_val->product_id; ?>
                                     @endforeach
                            @endforeach


         
                            <div class="col-sm-9">

                              <select name="recommandation_product[]" id="recommandation_product" class="form-control" multiple>
                                <option value="">Select</option>
                                @foreach($product as $key => $value)
                               
                             @if(in_array($value->product_id,$pro)) 

                                <option value="{{$value->product_id}}" selected="selectd">{{$value->name}}</option>
                                @else
                                <option value="{{$value->product_id}}">{{$value->name}}</option>
                                @endif
                                @endforeach
                              </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-sm-3" for="txtsubmenulink">Related Mattress:</label>


                            @foreach($matt_selector_product as $key4 => $recom_pro1)

                                  @foreach($recom_pro1->product_related_product as $key5 => $recom_val1)
                                    <?php $pro1[] = $recom_val1->product_id; ?>
                                     @endforeach
                            @endforeach
                            <div class="col-sm-9">
                              <select name="related_product[]" id="related_product" class="form-control" multiple>
                                <option value="">Select</option>
                                @foreach($product as $value)
                             @if(in_array($value->product_id,$pro1)) 
                                <option value="{{$value->product_id}}" selected="selectd">{{$value->name}}</option>
                                @else
                                <option value="{{$value->product_id}}">{{$value->name}}</option>
                                @endif
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="control-label col-sm-3" for="Status">Status:</label>
                            <div class="col-sm-9">
                              <select name="Status" id="Status" class="form-control" required="required">
                                <option value="">Select</option>
                                <option value="1" <?php if($mattress[0]->status == '1'){ echo "selected";} ?>>Active</option>
                                <option value="2" <?php if($mattress[0]->status == '2'){ echo "selected";} ?>>DeActive</option>
                              </select>
                            </div>
                          </div>

                    </div>
                </div>
               {!! Form::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')

<script type="text/javascript">
$(document).ready(function(){
  $(".select-mattress-form").validate({
    rules: {
        lang_code: "required",
        Status : "required"
    },
    messages: {
      lang_code: "Please select language",
      Status: "Please select status"
    }
  });

    if($("input[data-title='myself']").is(":checked"))  {

        $('#your_Partner_weight_range').hide();
        $('#your_partner_range').hide();
        $('#your_Partner_position').hide();
        $('#big_people').hide();

        $('#new_mattress').show();
        $('#your_weight_range').show();
        $('#your_age_range').show();
        $('#after_sleep').show();
        $('#hot_cool').show();
        $('#your_position').show();
        
    } 
    else if($("input[data-title='mypartner']").is(":checked") || $("input[data-title='mypartner_kids']").is(":checked")){

        $('#new_mattress').show();
        $('#your_weight_range').show();
        $('#your_Partner_weight_range').show();
        $('#your_age_range').show();
        $('#your_partner_range').show();
        $('#your_position').show();
        $('#your_Partner_position').show();

        $('#big_people').hide();
    }
     else if ($("input[data-title='spare_bedroom']").is(":checked")) {

        $('#new_mattress').hide();
        $('#your_Partner_weight_range').hide();
        $('#your_partner_range').hide();
        $('#your_weight_range').hide();
        $('#your_age_range').hide();
        $('#your_Partner_position').hide();
        $('#your_position').hide();
        $('#after_sleep').hide();
        $('#hot_cool').hide(); 

        $('#big_people').show();

     }
     else {

            $('#new_mattress').show();
        $('#your_Partner_weight_range').show();
        $('#your_partner_range').show();
        $('#your_weight_range').show();
        $('#your_age_range').show();
        $('#your_Partner_position').show();
        $('#your_position').show();
        $('#after_sleep').show();
        $('#hot_cool').show(); 

        $('#big_people').show();
     }
  $("input[name='buying']").click(function() {
    var value = $(this).val();

    // $("div.desc").hide();
    // $("#Cars" + test).show();
    if(value == 'Myself'){       

        $('#your_Partner_weight_range').hide();
        $('#your_partner_range').hide();
        $('#your_Partner_position').hide();

        $('#new_mattress').show();
        $('#your_weight_range').show();
        $('#your_age_range').show();
        $('#after_sleep').show();
        $('#hot_cool').show();
        $('#your_position').show();

        $('#your_Partner_weight_range').hide();
        $('#your_partner_range').hide();
        $('#your_Partner_position').hide();   
        $('#big_people').hide();
    
    }

    if(value == 'My partner & I' || value == 'My partner, I & My kids'){
        
        $('#new_mattress').show();
        $('#your_weight_range').show();
        $('#your_Partner_weight_range').show();
        $('#your_age_range').show();
        $('#your_partner_range').show();
        $('#your_position').show();
        $('#your_Partner_position').show();
        $('#after_sleep').show();
        $('#hot_cool').show();
        
    }

    if(value == 'Just for the spare bedroom'){        
    

        $('#new_mattress').hide();
        $('#your_Partner_weight_range').hide();
        $('#your_partner_range').hide();
        $('#your_weight_range').hide();
        $('#your_age_range').hide();
        $('#your_Partner_position').hide();
        $('#your_position').hide();
        $('#after_sleep').hide();
        $('#hot_cool').hide(); 

        $('#big_people').show();
     
    }

  });
  
});
</script>
@endsection