@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/insert/store-locator','class'=>'form-horizontal store-locator-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/manage-store-locators" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Add Store Locator</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label col-sm-4" for="lang_code">language:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <select name="lang_code" id="lang_code" class="form-control">
                                                    @foreach($language as $lang)
                                                    <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="control-label col-sm-4">Store Name:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type=text name="name" id="name"  value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="address" class="control-label col-sm-4">Address:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <textarea name="address" id="address" rows="3" cols="10" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="city_province" class="control-label col-sm-4">City/Province:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="city_province" id="city_province" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="state_region" class="control-label col-sm-4">State/Region:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="state_region" id="state_region" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="postalcode" class="control-label col-sm-4">Postal Code:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="postalcode" id="postalcode" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="country" class="control-label col-sm-4">Country:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <select name="country" id="country" class="form-control">
                                                    <option value="">Select</option>
                                                    @if($country)
                                                        @foreach($country as $row)
                                                        <option>{{$row->nicename}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="zoom" class="control-label col-sm-4">Zoom:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="zoom" id="zoom" value="" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="radius" class="control-label col-sm-4">Radius:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="radius" id="radius" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="latitude" class="control-label col-sm-4">Latitude:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="latitude" id="latitude" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="longitude" class="control-label col-sm-4">Longitude:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="longitude" id="longitude" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="control-label col-sm-4">Email:</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="email" id="email" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="control-label col-sm-4">Phone:</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="phone" id="phone" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fax" class="control-label col-sm-4">Fax:</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="fax" id="fax" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-4" for="status">Status:<span class="note">*</span></label>
                                            <div class="col-sm-8">
                                                <select name="status" id="status" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="1">Enable</option>
                                                    <option value="2">Disable</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".store-locator-form").validate({
    rules:
    {
        name: "required",
        address : "required",
        city_province : "required",
        state_region : "required",
        postalcode : "required",
        country : "required",
        latitude : "required",
        longitude : "required",
        zoom : "required",
        radius : "required",
        email: {
            required: true,
            email: true
        },
        status : "required"
    },
    messages:
    {
      name: "Please enter name",
      email: "Please enter email address",
      address: "Please enter address",
      city_province: "Please enter city or province",
      state_region: "Please enter state or region",
      postalcode: "Please enter postal code",
      country: "Please choose country",
      latitude: "Please enter latitude",
      longitude: "Please enter longitude",
      zoom: "Please enter zoom",
      radius: "Please enter radius",
      status: "Please select status"
    }
  });
});
</script>
@endsection