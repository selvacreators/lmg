@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/page','class'=>'form-horizontal page-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                            <input name="page_id" type="hidden" value="{{$getpage[0]->page_id}}">
                            <div class="card">
                                <div class="card-header">
                                    <a href="/Admin/pages" class="btn btn-primary btn-sm pull-right">Back</a>
                                    <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                    <h3 class="card-title">Edit Page</h3>
                                </div>
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="language" class="form-control" id="language">
                                                @foreach($language as $lng)
                                                    @if($lng->lang_code == $getpage[0]->lang_code)
                                                        <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                                    @else
                                                        <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="page_name" class="control-label col-sm-3">Page Name:<span class="note">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="page_name" name="page_name" value="{{$getpage[0]->page_name}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="page_title" class="control-label col-sm-3">Page Title:<span class="note">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="page_title" name="page_title" value="{{$getpage[0]->page_title}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="page_slug" class="control-label col-sm-3">Page Slug / Url:<span class="note">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="page_slug" name="page_slug" value="{{$getpage[0]->slug}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="content_heading" class="control-label col-sm-3">Content Heading:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="content_heading" name="content_heading" value="{{$getpage[0]->content_heading}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="content-editor" class="control-label col-sm-3">Content:</label>
                                        <div class="col-sm-9">
                                            <textarea cols="10" rows="5" name="content" id="ck_content-editor" class="form-control ckeditor">{{$getpage[0]->content}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="meta_title" class="control-label col-sm-3">Meta Title:<span class="note">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{$getpage[0]->meta_title}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="meta_tag_desc" class="control-label col-sm-3">Meta Tag Description:</label>
                                        <div class="col-sm-9">
                                            <textarea cols="10" rows="5" name="meta_tag_desc" id="meta_tag_desc" class="form-control">{{$getpage[0]->meta_desc}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="meta_tag_keywords" class="control-label col-sm-3">Meta Tag Keywords:</label>
                                        <div class="col-sm-9">
                                            <textarea cols="10" rows="5" name="meta_tag_keywords" id="meta_tag_keywords" class="form-control">{{$getpage[0]->meta_keyword}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="meta_tag_keywords" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="status" class="form-control" id="status" required="required">
                                                <option value="">Select</option>
                                                <option value="1" <?php if($getpage[0]->status =='1'){ echo "selected";} ?>>Enable</option>
                                                <option value="2" <?php if($getpage[0]->status =='2'){ echo "selected";} ?>>Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
    $('#ck_content-editor').ckeditor({      
      filebrowserImageBrowseUrl: route_prefix + '?type=Images',
      filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
      filebrowserBrowseUrl: route_prefix + '?type=Files',
      filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
  </script>


<script type="text/javascript">
$(document).ready(function(){
  $(".page-form").validate({
    rules: {
        language: "required",
        page_name : "required",
        page_title : "required",
        page_slug : "required",
        meta_title : "required",
        status : "required"
    },
    messages: {
      language: "Please select language",
      page_name: "Please enter page name",
      page_title: "Please enter page title",
      page_slug: "Please enter page slug",
      meta_title: "Please enter meta title",
      status: "Please select status"
    }
  });
});
</script>
@endsection