@extends('layouts.layout_admin')
@section('admin-style')
<link href="{{ Config::get('app.head_url') }}assets/css/demo.css" rel="stylesheet" />

<style type="text/css" media="screen">
#ajax-loader{
  position: fixed;
  top: 50px;    
  width: 100%;
  height: 100%;
  background: rgba(0,0,0,0.3);
  z-index: 9999;
  text-align:center;
}
#column-left + div #ajax-loader{
  left:50px;
}
#column-left.active + div #ajax-loader{
  left: 250px;
}
.spinner{
  border: 5px solid #f3f3f3;
  -webkit-animation: spin 1s linear infinite;
  animation: spin 1s linear infinite;
  border-top: 5px solid #555;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  position: absolute;
  top:50%;
  left:40%;
}
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
@endsection
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/edit_categories','class'=>'form-horizontal category-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div id="ajax-loader"  style="display: none;"><div class="spinner"></div></div>
                        <input type="hidden" name="categories_id" value="{{$categories->categories_id}}">
                          <div class="card">
                            <div class="card-header">
                                <a href="/Admin/categories" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Category</h3>
                            </div>
                            <div class="card-body">
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="lang_code" id="language" class="form-control">
                                     @foreach($language as $lang)
                                      <option value="{{$lang->lang_code}}" <?php if($categories->lang_code  == $lang->lang_code){ echo "selected";}?>>{{$lang->lang_name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="category_name">Category Name:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="category_name" id="category_name" class="form-control" value="{{$categories->category_name}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="category_slug">Url/Slug:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="category_slug" id="category_slug" class="form-control" value="{{$categories->category_slug}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="parent_id">Parent:</label>
                                <div class="col-sm-9">
                                  <select name="parent_id" id="parent_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($allCategories as $ca_name)
                                    <option value="{{$ca_name->categories_id}}" <?php if($categories->parent_id  == $ca_name->categories_id){ echo "selected";}?>>{{$ca_name->category_name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="category_image" class="control-label col-sm-3">Category Image:</label>
                                <div class="col-sm-3">
                                  <input type="file" name="category_image" id="category_image" class="form-control" accept="image/*" data-type="image" onchange="readCategoryURL(this);">
                                  <input type="hidden" name="InsertCategory_image" id="InsertCategory_image" class="form-control" value="{{$categories->cate_image}}">
                                </div>
                                <div class="col-sm-4" id="category-img">
                                  @if($categories->cate_image)
                                  <img src="{{Config::get('app.head_url')}}category/{{$categories->cate_image}}" id="StrImg" width="150"/>
                                  @else
                                  <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="StrImg" width="150"/>
                                  @endif
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="cate_desc" class="control-label col-sm-3">Category Description:</label>
                                <div class="col-sm-9">
                                  <textarea cols="10" rows="5" name="cate_desc" id="cate_desc" class="ckeditor form-control">{{$categories->cate_desc}}</textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_title">Meta Title:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_title" value="{{$categories->meta_title}}" id="meta_title" class="form-control" >
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_keyword">Meta Keyword:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_keyword" value="{{$categories->meta_keywords}}" id="meta_keyword" class="form-control" >
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_desc">Meta Description:</label>
                                <div class="col-sm-9">
                                  <textarea name="meta_desc" id="meta_desc" rows="3" cols="10" class="form-control">{{$categories->meta_description}}</textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="col_status">Status:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="col_status" id="col_status" class="form-control">
                                    <option value="" >Select</option>
                                    <option value="1" <?php if($categories->status  == '1'){ echo "selected";}?>>Enable</option>
                                    <option value="2" <?php if($categories->status  == '2'){ echo "selected";}?>>Disable</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@section('admin-script')
<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
  $('#cate_desc').ckeditor({      
    filebrowserImageBrowseUrl: route_prefix + '?type=Images',
    filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: route_prefix + '?type=Files',
    filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
  });
</script>

<script type="text/javascript">

$(document).ready(function(){
  $(".category-form").validate({
    rules:
    {
      lang_code : "required",
      category_name: "required",
      category_slug : "required",
      meta_title : "required",
      col_status : "required"
    },
    messages:
    {
      lang_code: "Please enter name",
      category_name: "Please enter category name",
      category_slug: "Please enter category slug",
      meta_title: "Please enter meta title",
      col_status: "Please choose status"
    }
  });
  $("#language").change(function(){
    var lang = $(this).val(), ajxLoader = $("#ajax-loader");
    var cate_id = $("input[name='categories_id']").val();
    // alert(cate_id);
    var data = '';
    $.ajax({
      url: '/Admin/getCategoryByLang?lang='+lang+'&c='+cate_id,
      type: 'get',
      dataType: 'json',
      beforeSend: function(){ ajxLoader.show(); },
      success:function(result){
        // console.log(result);
        if(result.data)
        {
          var count = Object.keys(result.data).length;
          if(count)
          {
            populateFormData(result.data,result.allCategories);
          }
        }
        else
        {
          data.parent_id = null;
          populateFormData(data,result.allCategories);
        }
        // if(result.allCategories)
        // {
        //   $('#parent_id').empty();
        //   $.each( result.allCategories,function( key, value )
        //   {
        //     console.log(value);
        //     $('#parent_id').append('<option value="'+ value.categories_id +'">'+ value.category_name +'</option>');
        //   });
        // }
      },
      complete:function(){ ajxLoader.hide(); }
    });
  });
  function populateFormData(data,allcate)
  {
    if(allcate)
    {
      $('#parent_id').empty();
      $('#parent_id').append("<option value=''>Select</option>");
      $.each( allcate,function( key, value )
      {
        console.log(value);
        $('#parent_id').append('<option '+ ((value.categories_id == data.parent_id)?'selected':'') +' value="'+ value.categories_id +'">'+ value.category_name +'</option>');
      });
    }
    if(data){
      $("#category_name").val(data.category_name);
      $("#category_slug").val(data.category_slug);

      CKEDITOR.instances.cate_desc.setData(data.cate_desc);

      $("#meta_title").val(data.meta_title);
      $("#meta_keyword").val(data.meta_keywords);
      $("#meta_desc").val(data.meta_description);
    }
  }
});
function readCategoryURL(input)
{
  if (input.files && input.files[0])
  {
    var reader = new FileReader();
    reader.onload = function (e)
    {
      $('#StrImg')
      .attr('src', e.target.result)
      .width(100);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
$("#category_image").change(function() {
  if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif)$/) ) {
      if(this.files[0].size>2000000) {//1048576
        // alert('File size is smaller than 2MB!');
        alert('File size is larger than 2MB!');
        $('#store_logo').val();
      }
  } else alert('This is not an image file!');
});
</script>
@endsection