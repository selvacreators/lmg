@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/add_categories','class'=>'form-horizontal category-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                          <div class="card">
                            <div class="card-header">
                                <a href="/Admin/categories" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button id="btnadd" class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Create New Category</h3>
                            </div>
                            <div class="card-body">
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="lang_code" id="language" class="form-control">
                                    @foreach($language as $lang)
                                    <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="category_name">Category Name:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="category_name" id="category_name" class="form-control">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="category_slug">Url/Slug:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="category_slug" id="category_slug" class="form-control">
                                  <div id="perror"></div>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="parent_id">Parent:</label>
                                <div class="col-sm-9">
                                  <select name="parent_id" id="parent_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($categories_name as $ca_name)
                                    <option value="{{$ca_name->categories_id}}">{{$ca_name->category_name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="category_image" class="control-label col-sm-3">Category Image:</label>
                                <div class="col-sm-3">
                                  <input type="file" name="category_image" id="category_image" class="form-control" accept="image/*" data-type="image" onchange="readCategoryURL(this);">
                                </div>
                                <div class="col-sm-4">
                                  <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="StrImg" width="100" height="100">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="cate_desc" class="control-label col-sm-3">Category Description:</label>
                                <div class="col-sm-9">
                                  <textarea cols="10" rows="5" name="cate_desc" id="cate_desc" class="ckeditor form-control"></textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_title">Meta Title:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_title" id="meta_title" class="form-control" >
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_keyword">Meta Keyword:</label>
                                <div class="col-sm-9">
                                  <input type="text" name="meta_keyword" id="meta_keyword" class="form-control" >
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="meta_desc">Meta Description:</label>
                                <div class="col-sm-9">
                                  <textarea name="meta_desc" id="meta_desc" rows="3" cols="10" class="form-control"></textarea>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="col_status">Status:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="col_status" id="col_status" class="form-control">
                                    <option value="" >Select</option>
                                    <option value="1">Enable</option>
                                    <option value="2">Disable</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
  $('#cate_desc').ckeditor({      
    filebrowserImageBrowseUrl: route_prefix + '?type=Images',
    filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: route_prefix + '?type=Files',
    filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
  });
</script>

<script type="text/javascript">
$(document).ready(function(){
  $(".category-form").validate({
    rules:
    {
      lang_code : "required",
      category_name: "required",
      category_slug : "required",
      meta_title : "required",
      col_status : "required"
    },
    messages:
    {
      lang_code: "Please enter name",
      category_name: "Please enter category name",
      category_slug: "Please enter category slug",
      meta_title: "Please enter meta title",
      col_status: "Please choose status"
    }
  });
  $('#category_slug').blur(function() {
    var getslug = $(this).val();
    var lang = $('#language').val();
    $.ajax({
      type:'GET',
      url:"{{URL('/Admin/checkCategoryslug')}}",
      data:{'lang':lang,'getslug':getslug},
      success:function(res){
        if(res == 1){
          $('#perror').html('Wrong category slug already Exits.');
          $('#category_slug').focus();
          $('#btnadd').attr("disabled", "disabled");
        }
        else{
          $('#perror').html('');
          $('#btnadd').removeAttr("disabled");; 
        }
      }
    });
  });
  $("#language").change(function(){
    var lang = $(this).val(), ajxLoader = $("#ajax-loader");
    $.ajax({
      url: '/Admin/getCategoryByLang?lang='+lang,
      type: 'get',
      dataType: 'json',
      beforeSend: function(){ ajxLoader.show(); },
      success:function(result){
        $('#parent_id').empty();
        $('#parent_id').append("<option value=''>Select</option>");
        $.each( result.allCategories,function( key, value )
        {
          $('#parent_id').append('<option value="'+ value.categories_id +'">'+ value.category_name +'</option>');
        });
      },
      complete:function(){ ajxLoader.hide(); }
    });
  });
});
function readCategoryURL(input)
{
  if (input.files && input.files[0])
  {
    var reader = new FileReader();
    reader.onload = function (e)
    {
      $('#StrImg')
      .attr('src', e.target.result)
      .width(100);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
$("#category_image").change(function() {
  if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif)$/) ) {
      if(this.files[0].size>2000000) {//1048576
        // alert('File size is smaller than 2MB!');
        alert('File size is larger than 2MB!');
        $('#store_logo').val();
      }
  } else alert('This is not an image file!');
});
</script>
@endsection