@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            {!! Form::open(array('url' => '/Admin/add_ecatalogue','class'=>'form-horizontal e-catalogue-form','method'=>'post','enctype'=>'multipart/form-data')) !!}
              <div class="card">
                <div class="card-header">
                  <a href="/Admin/ecatalogue" class="btn btn-primary btn-sm pull-right">Back</a>
                  <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                  <h3 class="card-title">Create E-Catalogue</h3>
                </div>
                <div class="card-body">
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="ecatalogue_name">Name:<span class="note">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" name="ecatalogue_name" id="ecatalogue_name" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="ecatalogue_slug">Slug:<span class="note">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" name="ecatalogue_slug" id="ecatalogue_slug" class="form-control">
                      <div id="perror"></div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="lang_code">Language:<span class="note">*</span></label>
                    <div class="col-sm-9">
                      <select name="lang_code" id="lang_code" class="form-control">
                        <option value="">Select</option>
                        @foreach($language as $lang)
                        <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="ewidth">Width:<span class="note">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" name="ewidth" id="ewidth" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="eheight">Height:<span class="note">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" name="eheight" id="eheight" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="sort_order">Sort Order:<span class="note">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" name="sort_order" id="sort_order" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="col_status">Status:<span class="note">*</span></label>
                    <div class="col-sm-9">
                      <select name="col_status" id="col_status" class="form-control">
                        <option value="" >Select</option>
                        <option value="1">Enable</option>
                        <option value="2">Disable</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                  <div class="col-md-9">
                    <div class="form-group row ecatalogue-table">
                      <table class="table table-responsive">
                        <thead>
                          <tr>
                            <th>Upload</th>
                            <th>Sort Order</th>
                            <th>Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      <div class="clearfix"></div>
                      <div class="multi" id="multi_ecatalogue_btn">
                        <div class="form-group row">
                          <input type="button" id="addECatalogueImg" value="Add New Image" class="btn btn-info btn-daimler">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function()
{
  $('#ecatalogue_slug').blur(function()
  {
    var getecatalogue = $(this).val();
    var tokens = $('#tokens').val();
    $.ajax({
      type:'GET',
      url:"{{URL('/Admin/check/ecatalogueslug')}}",
      data:{'_token':tokens,'getecatalogue':getecatalogue},
      success:function(res){
        console.log(res);
        if(res == 1){
          $('#perror').html('ecatalogue already exists');
          $('#ecatalogue_slug').focus();
          $('#save').attr("disabled", "disabled");
        }
        else{
          $('#perror').html('');
          $('#save').removeAttr("disabled");
        }
      }
    });
  });
  $("#addECatalogueImg").click(function() {
    var newRow = $("<tr>");
    var cols = "";
    cols += '<td><input type="file" name="ecatalogue_image[]" id="ecatalogue_image" class="form-control" ></td><td><input type="text" name="eci_sort_order[]" id="eci_sort_order" class="form-control" autocomplete="off"/></td><td><button type="button" class="btn btn-danger feaDel"><i class="fa fa-trash"></i></button></td></tr>';
    newRow.append(cols);
    $("tbody").append(newRow);
  });
  $(".ecatalogue-table").on("click", ".feaDel", function (event) {
    $(this).closest("tr").remove();
  });
  $(".e-catalogue-form").validate({
    rules: {
      ecatalogue_name : "required",
      ecatalogue_slug : "required",
      lang_code: "required",
      ewidth : "required",
      eheight : "required",
      sort_order : "required",
      col_status : "required"
    },
    messages: {
      ecatalogue_name: "Please enter name",
      ecatalogue_slug: "Please enter slug",
      lang_code: "Please select language",
      ewidth: "Please enter width",
      eheight: "Please enter height",
      sort_order: "Please enter sort order",
      col_status: "Please select status"
    }
  });
});
</script>
@endsection