@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/insert/common-setting','class'=>'form-horizontal csetting-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/common-setting" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Add New Common Setting</h3>
                            </div>
                            <div class="card-body">
                                 <nav class="nav-container">
                                    <ul class='nav nav-tabs text-center' role="tablist">
                                        <li><a href="#tab-general" data-toggle="tab" class="active show">General Setting</a></li>
                                        <li><a href="#tab-data" data-toggle="tab">Data</a></li>
                                        <li><a href="#tab-social" data-toggle="tab">Social Plugin</a></li>
                                    </ul>
                                </nav>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-general">
                                        <div id="commonSetting">
                                            <div class="form-group row">
                                                <label for="store_name" class="control-label col-sm-3">Store Name:<span class="note">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="store_name" name="store_name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="store_logo" class="control-label col-sm-3">Store Logo:<span class="note">*</span></label>
                                                <div class="col-sm-3">
                                                    <input type="file" name="store_logo" id="store_logo" class="form-control" accept="image/*" data-type="image" onchange="readStoreURL(this);">
                                                </div>
                                                <div class="col-sm-4">
                                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="StrImg" width="100" height="100">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="fav_icon" class="control-label col-sm-3">Fav Icon:</label>
                                                <div class="col-sm-3">
                                                    <input type="file" name="fav_icon" id="fav_icon" class="form-control" onchange="readFavURL(this);">
                                                </div>
                                                <div class="col-sm-4">
                                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="FavImg" width="60" height="60">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="phone" class="control-label col-sm-3">Phone:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="phone" id="phone" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="control-label col-sm-3">Email:</label>
                                                <div class="col-sm-9">
                                                    <input type="email" name="email" id="email" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="address" class="control-label col-sm-3">Address:</label>
                                                <div class="col-sm-9">
                                                    <textarea cols="10" rows="5" name="address" id="address" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="opening_timings" class="control-label col-sm-3">Opening Timings:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="opening_timings" id="opening_timings" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="show_feedback" class="control-label col-sm-3">Mattress E-Warranty Feedback:</label>
                                                <div class="col-sm-9">
                                                    <input type="radio" name="show_feedback" id="show_feedback" value="1" checked> Show
                                                    <input type="radio" name="show_feedback" id="show_feedback" value="2"> Hide
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="chat_url" class="control-label col-sm-3">Chat Url:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="chat_url" id="chat_url" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="store_apikey" class="control-label col-sm-3">Store API Key:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="store_apikey" id="store_apikey" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="footer_copyright" class="control-label col-sm-3">Footer Copyright:<span class="note">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="footer_copyright" id="footer_copyright" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="footer_link" class="control-label col-sm-3">Footer Link:<span class="note">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="footer_link" id="footer_link" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="">Select</option>
                                                        <option value="1">Enable</option>
                                                        <option value="2">Disable</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-data">
                                        <div id="language" class="">
                                            <nav class="nav-container">
                                              <ul class='nav nav-tabs text-center' role="tablist">
                                                @if($language)
                                                @foreach($language as $key =>$lan)
                                                @if($lan->lang_code == 'en')
                                                <?php $check = "active";?>
                                                @else
                                                <?php $check = "";?>
                                                @endif
                                                <li><a data-toggle="tab" class="{{$check}} show" href="#{{$lan->lang_name}}">{{$lan->lang_name}}</a></li>
                                                @endforeach
                                                @endif
                                              </ul>
                                            </nav>
                                            <div class="tab-content pt-0 pb-0">
                                                @if($language)
                                                  @foreach($language as $key =>$lan)
                                                  @if($lan->lang_code == 'en')
                                                  <?php $check = "active";?>
                                                  @else
                                                  <?php $check = "";?>
                                                  @endif
                                                <div id="{{$lan->lang_name}}" class="tab-pane {{$check}} in fieldset">
                                                    <div class="form-group row">
                                                        <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                                        <div class="col-sm-9">
                                                            <select name="language" class="form-control" id="language">
                                                                @foreach($language as $lng)
                                                                <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="meta_title" class="control-label col-sm-3">Meta Title:<span class="note">*</span></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="meta_title" id="meta_title" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="meta_tag_desc" class="control-label col-sm-3">Meta Tag Description:</label>
                                                        <div class="col-sm-9">
                                                            <textarea cols="10" rows="5" name="meta_tag_desc" id="meta_tag_desc" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="meta_tag_keywords" class="control-label col-sm-3">Meta Tag Keywords:</label>
                                                        <div class="col-sm-9">
                                                            <textarea cols="10" rows="5" name="meta_tag_keywords" id="meta_tag_keywords" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-social">
                                        <div id="language" class="">
                                            <div class="form-group row">
                                                <label for="facebook_api" class="control-label col-sm-3">Facebook APP ID:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="facebook_api" id="facebook_api" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="facebook_username" class="control-label col-sm-3">Facebook Page Url (or Username):</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="facebook_username" id="facebook_username" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="twitter_api" class="control-label col-sm-3">Twitter APP ID:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="twitter_api" id="twitter_api" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="twitter_username" class="control-label col-sm-3">Twitter Page Url (or Username):</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="twitter_username" id="twitter_username" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="google_api" class="control-label col-sm-3">Google+ Page Url:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="google_api" id="google_api" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="youtube_api" class="control-label col-sm-3">Youtube ID:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="youtube_api" id="youtube_api" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="linkedin_api" class="control-label col-sm-3">LinkedIn Url:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="linkedin_api" id="linkedin_api" class="form-control" autocomplete="off"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
    $(".csetting-form").validate({
        rules: {
            language: "required",
            store_name : "required",
            store_logo : "required",
            footer_copyright : "required",
            footer_link : "required",
            phone : "required",
            email:{
                email: true
            },
            // phone:{
            //     minlength:9,
            //     maxlength:15,
            //     number: true
            // },
            meta_title : "required",
            status : "required"
        },
        messages: {
            language: "Please select language",
            store_name: "Please enter store name",
            store_logo: "Please choose store logo",
            email: "Please enter valid email address",
            phone: "Please enter valid mobile number",
            meta_title: "Please enter meta title",
            footer_copyright: "Please enter footer copyright",
            footer_link: "Please enter footer link",
            status: "Please select status"
        }
    });
    $("#store_logo").change(function() {
        if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif)$/) ) {
            if(this.files[0].size>2000000) {//1048576
              // alert('File size is smaller than 2MB!');
              alert('File size is larger than 2MB!');
              $('#store_logo').val();
            }
        } else alert('This is not an image file!');
    });
});
function readStoreURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#StrImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readFavURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#FavImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection