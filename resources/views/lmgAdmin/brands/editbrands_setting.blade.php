@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/brands-setting','class'=>'form-horizontal brands-setting-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input name="brand_setid" type="hidden" value="{{$brand_set->brands_setting_id}}"> 
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/brandsetting" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Brand Setting</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" class="form-control" id="lang_code">
                                            @foreach($language as $lng)
                                                @if($lng->lang_code == $brand_set->lang_code)
                                                    <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                                @else
                                                    <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="page_title" class="control-label col-sm-3">Page Title:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="page_title" id="page_title" value="{{$brand_set->page_tittle}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="brand_header" class="control-label col-sm-3">Brand Heading:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="brand_header" id="brand_header" value="{{$brand_set->brand_header}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="brand_slug" class="control-label col-sm-3">Brand Slug:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="brand_slug" id="brand_slug" value="{{$brand_set->brand_slug}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="meta_title" class="control-label col-sm-3">Meta Title:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="meta_title" id="meta_title" value="{{$brand_set->meta_title}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="meta_keywords" class="control-label col-sm-3">Meta Keywords:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="meta_keywords" id="meta_keywords" value="{{$brand_set->meta_keywords}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="brand_banner" class="control-label col-sm-3">Banner Image:<span class="note">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="file" name="brand_banner" id="brand_banner" class="form-control" accept="image/*" data-type="image" onchange="readURL(this);">
                                        <input type="hidden" name="insertbrand_banner" id="insertbrand_banner" value="{{$brand_set->brand_banner}}">
                                    </div>
                                    <div class="col-sm-4">
                                        @if(!empty($brand_set->brand_banner))
                                        <img src="{{ Config::get('app.head_url') }}brand/brand_banner/{{$brand_set->brand_banner}}" id="StrImg" width="100" height="100">
                                        @else
                                        <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="StrImg" width="100"/>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="brands_desc" class="control-label col-sm-3">Brands Description:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea name="brands_desc" id="brands_desc" rows="4" cols="10" class="form-control">{{$brand_set->brands_desc}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="meta_desc" class="control-label col-sm-3">Meta Description:</label>
                                    <div class="col-sm-9">
                                        <textarea name="meta_desc" id="meta_desc" rows="4" cols="10" class="form-control" >{{$brand_set->meta_desc}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status<span class="note">*</span> :</label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                        <option value="1" <?php if($brand_set->status == '1') { echo "selected";} else { echo "";} ?>>Enable</option>
                                        <option value="2" <?php if($brand_set->status == '2') { echo "selected";} else { echo "";} ?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".brands-setting-form").validate({
    rules:
    {
        lang_code: "required",
        page_title: "required",
        brand_header : "required",
        meta_title : "required",
        brands_desc : "required",
        status : "required"
    },
    messages:
    {
        lang_code: "Please choose language",
        page_title: "Please enter page title",
        brand_header: "Please enter brand heading",
        meta_title: "Please enter meta title",
        brands_desc: "Please enter brands description",
        status: "Please select status"
    }
  });
});
function readURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#StrImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection