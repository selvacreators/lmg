@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/insert/brand','class'=>'form-horizontal brand-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/brands" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button id="btnadd" class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Add Brand</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <select name="lang_code" id="language" class="form-control">
                                        @foreach($storeLang as $lang)
                                        <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="brand_name" class="control-label col-sm-3">Brand Name:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="brand_name" id="brand_name" value="" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">            
                                  <label class="control-label col-sm-3" for="breadcumb_title">Page Title:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="breadcumb_title" id="breadcumb_title" class="form-control">
                                  </div>       
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="btn_url">Page Slug & Button Url:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="btn_url" id="btn_url" class="form-control">
                                    <div id="perror"></div>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="brand_banner">Banner Image:</label>
                                  <div class="col-sm-3">
                                    <input type="file" name="brand_banner" id="brand_banner" class="form-control"  accept="image/*" data-type="image" onchange="readBannerURL(this);">
                                  </div>
                                  <div class="col-sm-4">
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="BannImg" width="100" height="100">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="brand_caption" class="control-label col-sm-3">Brand Caption:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="brand_caption" id="brand_caption" value="" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">            
                                  <label class="control-label col-sm-3" for="txtbrandlog">Brand Logo:</label>
                                  <div class="col-sm-3">
                                    <input type="file" name="txtbrandlog" id="txtbrandlog" class="form-control" accept="image/*" data-type="image" onchange="readLogoURL(this);">
                                  </div>
                                  <div class="col-sm-4">
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="LogoImg" width="100" height="100">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="feature_image">Feature Image:</label>
                                  <div class="col-sm-3">
                                    <input type="file" name="feature_image" id="feature_image" class="form-control" accept="image/*" data-type="image" onchange="readFeatureURL(this);">
                                  </div>
                                  <div class="col-sm-4">
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="FeatImg" width="100" height="100">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="position">Image Position:</label>
                                  <div class="col-sm-9">
                                    <select name="position" id="position" class="form-control">
                                      <option value="left">Left</option>
                                      <option value="right">Right</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="btn_text">Button Text:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="btn_text" id="btn_text" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row textareaeditor">            
                                  <label class="control-label col-sm-3" for="brand_sort_desc">Short Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="brand_sort_desc" rows="4" class="form-control" id="brand_sort_desc"></textarea>
                                  </div>       
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="brand_des">Brand Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="brand_des" id="brand_des"></textarea>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_title">Meta Title:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="meta_title" id="meta_title" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_keyword">Meta Keyword:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="meta_keyword" id="meta_keyword" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_desc">Meta Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="meta_desc" id="meta_desc" rows="4" class="form-control"></textarea>
                                  </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sort_order" class="control-label col-sm-3">Sort Order:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="sort_order" id="sort_order" value="" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="col_status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="col_status" id="col_status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Enable</option>
                                            <option value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".brand-form").validate({
    rules:
    {
      lang_code: "required",
      brand_name: "required",
      btn_url: "required",
      meta_title: "required",
      col_status: "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      brand_name: "Please enter brand name",
      btn_url: "Please enter page slug/url",
      meta_title: "Please enter meta title",
      col_status: "Please select status"
    }
  });
});
function readFeatureURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#FeatImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readLogoURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#LogoImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readBannerURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#BannImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>

<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
$('#brand_des').ckeditor({      
  filebrowserImageBrowseUrl: route_prefix + '?type=Images',
  filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
  filebrowserBrowseUrl: route_prefix + '?type=Files',
  filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
});
$('#btn_url').blur(function() {
    var getslug = $(this).val();
    var lang = $('#language').val();
    $.ajax({
      type:'GET',
      url:"{{URL('/Admin/checkBrandslug')}}",
      data:{'lang':lang,'getslug':getslug},
      success:function(res){
        console.log(res);
        if(res == 1){
          $('#perror').html('Wrong page slug/url already Exits.');
          $('#btn_url').focus();
          $('#btnadd').attr("disabled", "disabled");
        }
        else{
          $('#perror').html('');
          $('#btnadd').removeAttr("disabled");; 
        }
      }
    });
  });
</script>
@endsection