@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/brand','class'=>'form-horizontal brand-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="brandid" value="{{$brands->brand_id}}"/>
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/brands" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Brand</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <select name="lang_code" id="language" class="form-control">
                                        @foreach($language as $lng)
                                          @if($lng->lang_code == $brands->lang_code)
                                            <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                          @else
                                            <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                          @endif
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="brand_name" class="control-label col-sm-3">Brand Name:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="brand_name" id="brand_name" value="{{$brands->brand_name}}" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">            
                                  <label class="control-label col-sm-3" for="breadcumb_title">Page Title:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="breadcumb_title" id="breadcumb_title" class="form-control" value="{{$brands->breadcumb_title}}">
                                  </div>       
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="btn_url">Page Slug & Button Url:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="btn_url" id="btn_url" class="form-control" value="{{$brands->btn_url}}">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="brand_banner">Banner Image:</label>
                                  <div class="col-sm-3">
                                    <input type="file" name="brand_banner" id="brand_banner" class="form-control"  accept="image/*" data-type="image" onchange="readBannerURL(this);">
                                    <input type="hidden" name="insertbrands_banner" id="insertbrands_banner" value="{{$brands->brands_banner}}">
                                  </div>
                                  <div class="col-sm-4">
                                    @if(!empty($brands->brands_banner))
                                    <img src="{{Config::get('app.head_url')}}brand/brand_banner/{{$brands->brands_banner}}" id="BannImg" width="100">
                                    @else
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="BannImg" width="100">
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="brand_caption" class="control-label col-sm-3">Brand Caption:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="brand_caption" id="brand_caption" value="{{$brands->brand_heading}}" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">            
                                  <label class="control-label col-sm-3" for="txtbrandlog">Brand Logo:</label>
                                  <div class="col-sm-3">
                                    <input type="file" name="txtbrandlog" id="txtbrandlog" class="form-control" accept="image/*" data-type="image" onchange="readLogoURL(this);">
                                    <input type="hidden" name="insertbrands_logo" id="insertbrands_logo" value="{{$brands->brand_logo}}">
                                  </div>
                                  <div class="col-sm-4">
                                    @if(!empty($brands->brand_logo))
                                    <img src="{{Config::get('app.head_url')}}brand/brand_logo/{{$brands->brand_logo}}" id="LogoImg" width="100">
                                    @else
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="LogoImg" width="100">
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="feature_image">Feature Image:</label>
                                  <div class="col-sm-3">
                                    <input type="file" name="feature_image" id="feature_image" class="form-control" accept="image/*" data-type="image" onchange="readFeatureURL(this);">
                                    <input type="hidden" name="insertbrandfeatureimg" id="insertbrandfeatureimg" value="{{$brands->brand_featureimg}}">
                                  </div>
                                  <div class="col-sm-4">
                                    @if(!empty($brands->brand_featureimg))
                                    <img src="{{ Config::get('app.head_url') }}brand/brand_collection/{{$brands->brand_featureimg}}" id="FeatImg" name="brand_feature_img" width="100">
                                    @else
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="FeatImg" width="100">
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="position">Image Position:</label>
                                  <div class="col-sm-9">
                                    <select name="position" id="position" class="form-control">
                                      <option value="">Select</option>
                                      <option value="left" <?php if($brands->imge_position == 'left') { echo "selected";} else { echo "";} ?>>Left</option>
                                      <option value="right" <?php if($brands->imge_position == 'right') { echo "selected";} else { echo "";} ?>>Right</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="btn_text">Button Text:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="btn_text" id="btn_text" class="form-control" value="{{$brands->btn_text}}">
                                  </div>
                                </div>
                                <div class="form-group row textareaeditor">            
                                  <label class="control-label col-sm-3" for="brand_sort_desc">Short Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="brand_sort_desc" rows="4" class="form-control" id="brand_sort_desc">{{$brands->brand_sort_desc}}</textarea>
                                  </div>       
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="brand_des">Brand Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="brand_des" id="brand_des">{{$brands->brand_description}}</textarea>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_title">Meta Title:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{$brands->brand_meta_title}}">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_keyword">Meta Keyword:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="meta_keyword" id="meta_keyword" class="form-control" value="{{$brands->brand_meta_keyword}}">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_desc">Meta Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="meta_desc" id="meta_desc" rows="4" class="form-control">{{$brands->brand_meta_desc}}</textarea>
                                  </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sort_order" class="control-label col-sm-3">Sort Order:</label>
                                    <div class="col-sm-9">
                                      <input type="text" name="sort_order" id="sort_order" value="{{$brands->sort_order}}" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="col_status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="col_status" id="col_status" class="form-control">
                                          <option value="">Select</option>
                                          <option value="1" <?php if($brands->brand_status == '1') {?> selected="selected" <?php }?>>Enable</option>
                                          <option value="2" <?php if($brands->brand_status == '2') {?> selected="selected" <?php }?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".brand-form").validate({
    rules:
    {
      lang_code: "required",
      brand_name: "required",
      btn_url: "required",
      meta_title: "required",
      col_status: "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      brand_name: "Please enter brand name",
      btn_url: "Please enter page slug/url",
      meta_title: "Please enter meta title",
      col_status: "Please select status"
    }
  });
  $("#language").change(function(){
    var lang = $(this).val(), ajxLoader = $("#ajax-loader");
    var brand_id = $("input[name='brandid']").val();
    $.ajax({
      url: '/Admin/getBrandByLang?lang='+lang+'&b='+brand_id,
      type: 'get',
      dataType: 'json',
      beforeSend: function(){ ajxLoader.show(); },
      success:function(result){
        if(result.data)
        {
          var count = Object.keys(result.data).length;
          if(count)
          {
            populateFormData(result.data);
          }
        }
      },
      complete:function(){ ajxLoader.hide(); }
    });
  });
  function populateFormData(response) {

    console.log(response);
    $("#brand_name").val(response.brand_name);
    $("#breadcumb_title").val(response.breadcumb_title);
    $("#btn_url").val(response.btn_url);
    $("#brand_caption").val(response.brand_heading);
    $("#btn_text").val(response.btn_text);
    $("#brand_sort_desc").val(response.brand_sort_desc);
    $("#meta_title").val(response.brand_meta_title);
    $("#meta_keyword").val(response.brand_meta_keyword);
    $("#meta_desc").val(response.brand_meta_desc);
    $("#sort_order").val(response.sort_order);

    CKEDITOR.instances.brand_des.setData(response.brand_description);
  }
});
function readFeatureURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#FeatImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readLogoURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#LogoImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readBannerURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#BannImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>

<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
  $('#brand_des').ckeditor({      
    filebrowserImageBrowseUrl: route_prefix + '?type=Images',
    filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: route_prefix + '?type=Files',
    filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
  });
</script>
@endsection