@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">
   <div class="row"> 
       <h2> 
        Add Footer
    </h2>
</div>
<div class="content">
    <div class="container-fluid">
        
        <form method="POST" action="{{ URL('Admin/addfoots')}}" >
          <input name="_token" type="hidden" value="{{csrf_token()}}"> 

          
          <table class="table table-responsive">

            <tr>
                <td>Footer Link</td>
                <td><input type=text name="footerlink" id="footerlink" class="form-control" required="required"></td>
            </tr>
            
            <tr>
                <td>Footer Copyright</td>
                <td><input type="text" name="footer_copyright" id="footer_copyright" class="form-control" ></td>
            </tr>
            
            <tr>
                <td>Footer Status</td>
                <td>
                    <select name="footerstatus" id="footerstatus" class="form-control" required="required">
                        <option value="" >select</option>
                        <option value="1">Active</option>
                        <option value="2">DeActive</option>
                    </select>
                </td>
            </tr>

            
            <tr id="textareaeditor">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>Footer Content</td>
                        </tr>
                        <tr>
                            <td> <textarea name="editor1" id="editor1">
                                This is Footer content.
                            </textarea></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td><input type="reset" class="btn btn-primary btn-sm" ></td>
                <td colspan="2"><input type="submit" value="Add" class="btn btn-primary btn-sm" /></td>
            </tr>
        </table>
    </form>
</div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

              <center>

                <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

            </center>
            {{ Session::forget('message') }}
        </div>
    </div>
</div>
</div>
@endif
@stop
@section('admin-script')

<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
    $('#editor1').ckeditor({      
      filebrowserImageBrowseUrl: route_prefix + '?type=Images',
      filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
      filebrowserBrowseUrl: route_prefix + '?type=Files',
      filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
  </script>


@endsection
