@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">

    <div class="inner-content">
        <div class="container-fluid">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">Edit Sub Menu</h3>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ URL('Admin/editsubmenus')}}" class="form-horizontal">
                      <input name="_token" type="hidden" value="{{csrf_token()}}"> 
                      <input type="hidden" name="submenu_id" value="{{$results[0]->sub_id}}"> 


                         <div class="form-group row">
                        <label class="control-label col-sm-3" for="menutype">Menu Type <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="menutype" id="menutype" class="form-control" required>
                                <option value="" >select</option>
                                <option value="1" <?php if($results[0]->menu_type == 1) { echo "selected";} else { echo "";} ?>>Category</option>
                                <option value="2" <?php if($results[0]->menu_type == 2) { echo "selected";} else { echo "";} ?>>Collection</option>
                                <option value="3" <?php if($results[0]->menu_type == 3) { echo "selected";} else { echo "";} ?>>Pages</option>
                                <option value="4" <?php if($results[0]->menu_type == 4) { echo "selected";} else { echo "";} ?>>Custom Menu</option>
                            </select>
                        </div>
                    </div>

                      <div class="form-group row">
                        <label class="control-label col-sm-3" for="menuName">Main Menu Name:</label>
                        <div class="col-sm-9">
                            <select name="menuName" id="menuName" class="form-control" required="required">
                                <option value="" >select</option>
                                @foreach($getmenus as $value)

                                <option value="{{$value->menu_id}}" <?php if($results[0]->menu_id == $value->menu_id) { echo "selected= selected"; } ?>>{{$value->menu_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="txtsubmenuname">SubMenu Name:</label>
                        <div class="col-sm-9">
                            <input type="text" name="txtsubmenuname" id="txtsubmenuname" value="{{$results[0]->sub_name}}" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group row" id="submenu_link">
                        <label class="control-label col-sm-3" for="txtsubmenulink">SubMenu Link:</label>
                        <div class="col-sm-9">

                            @if($results[0]->sub_link == 1 || $results[0]->sub_link == 3)
                            <input type="text" name="txtsubmenulink" id="txtsubmenulink" value="{{$results[0]->sub_link}}" class="form-control" readonly="readonly" 
                            style="background-color: lightgray; ">

                              @else

                            <input type="text" name="txtsubmenulink" id="txtsubmenulink" value="{{$results[0]->sub_link}}" class="form-control" >

                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group row" id="page_name">
                        <label class="control-label col-sm-3" for="pagename">Page Name:</label>
                        <div class="col-sm-9">
                            <select name="pagename" id="pagename" class="form-control">
                                <option value="" >select</option>
                                @foreach($pages as $val)

                                @if($results[0]->sub_link == $val->slug)    
                                <option value="{{$val->slug}}" selected="selected">{{$val->page_name}}</option>
                                @else    
                                <option value="{{$val->slug}}">{{$val->page_name}}</option>
                                @endif

                                @endforeach
                            </select>
                        </div>
                    </div>
                   
                    <div class="form-group row" id="category_name">
                        <label class="control-label col-sm-3" for="pagename">Categories Name:</label>
                        <div class="col-sm-9">
                            <select name="category_slug" id="category_slug" class="form-control">
                                <option value="" >select</option>
                                @foreach($categories as $cate)

                                 @if($results[0]->sub_link == $cate->category_slug)  
                                <option value="{{$cate->category_slug}}" selected="selected">{{$cate->category_name}}</option>
                                @else
                                <option value="{{$cate->category_slug}}">{{$cate->category_name}}</option>
                                @endif

                                @endforeach
                            </select>
                        </div>
                    </div>
                    

               
                        


                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="submenustatus">SubMenu Status:</label>
                        <div class="col-sm-9">
                            <select name="submenustatus" id="submenustatus" class="form-control" required="required">
                                <option value="" >select</option>
                                <option value="1" <?php if($results[0]->submenu_status == 1) { echo "selected= selected"; } ?>>Active</option>
                                <option value="2" <?php if($results[0]->submenu_status == 2) { echo "selected= selected"; } ?>>DeActive</option>
                            </select>
                        </div>
                    </div>
                    <div class="buttons text-center">
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <button class="btn btn-success" type="submit">Save</button>
                    </div> 
                    
                </form>
            </div>
        </div>


    </div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

              <center>

                <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

            </center>
            {{ Session::forget('message') }}
        </div>
    </div>
</div>
</div>
@endif
@stop


@section('admin-script')
<script type="text/javascript">
   
   $(document).ready(function() {

    var menutypes = $('#menutype').val();

        if(menutypes == 1){
         $('#page_name').hide();
         //$('#submenu_link').hide();
         //$('#collection_name').hide();
          $('#category_name').show();
    }
    if(menutypes == 2){
         $('#page_name').hide();
        $('#submenu_link').show();
        // $('#collection_name').show();
          $('#category_name').hide();
    }
    if(menutypes == 3){
        //alert(typeno);
         $('#page_name').show();
        // $('#submenu_link').hide();
        // $('#collection_name').hide();
          $('#category_name').hide();
    }
    if(menutypes == 4){
         $('#page_name').hide();
         //$('#submenu_link').show();
        // $('#collection_name').hide();
          $('#category_name').hide();
    }
    if(menutypes == ""){
        //console.log('test');
        $('#page_name').hide();
        //$('#submenu_link').show();
         //$('#collection_name').hide();
        $('#category_name').hide();
    }

   });

$('#menutype').change(function(){
    var typeno = $(this).val();
   // alert(typeno);

    if(typeno == 1){
         $('#page_name').hide();
         $('#submenu_link').hide();
         //$('#collection_name').hide();
          $('#category_name').show();
          
           $('#txtsubmenulink').val('');
    }
    if(typeno == 2){
         $('#page_name').hide();
         $('#submenu_link').show();
         //$('#collection_name').show();
          $('#category_name').hide();

          // $('#txtsubmenulink').val('');
    }
    if(typeno == 3){
        //alert(typeno);
         $('#page_name').show();
         $('#submenu_link').hide();
         //$('#collection_name').hide();
          $('#category_name').hide();

          $('#txtsubmenulink').val('');
    }
    if(typeno == 4){
         $('#page_name').hide();
         $('#submenu_link').show();
         //$('#collection_name').hide();
          $('#category_name').hide();

          $('#pagename').val('');
          $('#category_slug').val('');
         // $('#collection_slug').val('');
    }
    if(typeno == ""){
        //console.log('test');
        $('#page_name').hide();
        $('#submenu_link').show();
       //  $('#collection_name').hide();
        $('#category_name').hide();


          $('#pagename').val('');
          $('#category_slug').val('');
//$('#collection_slug').val('');
    }
});
</script>
@endsection