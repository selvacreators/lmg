@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table id="datatable1" class="table table-striped table-advance table-hover bordered dt-responsive nowrap">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Menu Name</th>
                        <th>Menu Type</th>
                        <th class="col-xs-2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                       <td>1</td>
                       <td>Main Menu</td>
                       <td>Main Menu</td>
                       <td>
                        <a href="/Admin/editMenutype/M"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>                      
                      </td>
                     </tr>
                      <tr>
                       <td>2</td>
                       <td>Utility Menu</td>
                       <td>Utility Menu</td>
                       <td>
                        <a href="/Admin/editMenutype/U"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>                      
                      </td>
                     </tr>
                      <tr>
                       <td>3</td>
                       <td>Footer Menu</td>
                       <td>Footer Menu</td>
                       <td>
                        <a href="/Admin/editMenutype/F"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>                      
                      </td>
                     </tr>  
                  </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('admin-script')
  <script src="{{ Config::get('app.head_url') }}admin/datatables/js/jquery.dataTables.min.js"></script>
  <script src="{{ Config::get('app.head_url') }}admin/datatables/js/dataTables.responsive.min.js"></script>
  <script>
      $(document).ready(function () {
          $('#datatable').dataTable();
      });
  </script>
@endsection