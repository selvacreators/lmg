@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute fixed-top">
    <div class="container-fluid">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="#pablo">Utillity</a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <form class="navbar-form">
          <div class="input-group no-border">
            <input type="text" value="" class="form-control" placeholder="Search...">
            <button type="submit" class="btn btn-white btn-round btn-just-icon">
              <i class="material-icons">search</i>
              <div class="ripple-container"></div>
            </button>
          </div>
        </form>
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#pablo">
              <i class="material-icons">dashboard</i>
              <p>
                <span class="d-lg-none d-md-block">Stats</span>
              </p>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">notifications</i>
              <span class="notification">5</span>
              <p>
                <span class="d-lg-none d-md-block">Some Actions</span>
              </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Mike John responded to your email</a>
              <a class="dropdown-item" href="#">You have 5 new tasks</a>
              <a class="dropdown-item" href="#">You're now friend with Andrew</a>
              <a class="dropdown-item" href="#">Another Notification</a>
              <a class="dropdown-item" href="#">Another One</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#pablo">
              <i class="material-icons">person</i>
              <p>
                <span class="d-lg-none d-md-block">Account</span>
              </p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="content">
    <div class="container-fluid">
      

      <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6">
          <a href="/Admin/addUtillity" class="btn btn-primary btn-sm pull-right">Add Utillity Nav </a>
        </div>
      </div>
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <table class="table table-striped table-advance table-hover">
              <h4>View & Edit Utillity</h4>
              <hr>
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Menu Name</th>
                  <th>Menu Link</th>
                  <th>Menu Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; foreach ($Utillity as $user): ?>
                <tr>
                 <td><?php echo $i; ?></td>
                 <td><?php echo $user->Utillity_name; ?></td>
                 <td><?php echo $user->Utillity_link; ?></td>
                 <td><?php echo $user->Utillity_status; ?></td>
                 <td>
                  <a href="/Admin/editUtillity/{{$user->ut_id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                  <a href="/Admin/deleteUtillity/{{$user->ut_id}}" onclick="return confirm('Are you sure Delete Menu?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o "></i></button>
                  </a>
                </td>
              </tr>
              <?php $i++; endforeach; ?>


            </tbody>
          </table>
        </div>
      </div>
    </div>
    <?php echo $Utillity->render(); ?>
    
    
  </div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">

        <h4 align="center">{{ Session::get('message') }}</h4>

        <center>

          <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

        </center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif

@stop
@section('admin-script')
  <script src="{{ Config::get('app.head_url') }}admin/datatables/js/jquery.dataTables.min.js"></script>
  <script src="{{ Config::get('app.head_url') }}admin/datatables/js/dataTables.responsive.min.js"></script>
  <script>
      $(document).ready(function () {
          $('#datatable').dataTable();
      });
  </script>
@endsection