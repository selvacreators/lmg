@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            {!! Form::open(array('url' => '/Admin/editmenus','class'=>'form-horizontal menu-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
              <div class="card">
                <div class="card-header">
                  <a href="/Admin/menutype" class="btn btn-primary btn-sm pull-right">Back</a>
                  <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                  <h3 class="card-title">Edit Main Menu</h3>
                </div>
                <div class="card-body">
                  <input name="_token" type="hidden" value="{{csrf_token()}}">
                  <input name="txtmenu_id"  type="hidden" value="{{$results[0]->menu_id}}">
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="menutype">Language <span class="note">*</span></label>
                    <div class="col-sm-9">
                      <select name="lang_code" id="lang_code" class="form-control">
                        @foreach($language as $lang)
                        <option value="{{$lang->lang_code}}" <?php if($results[0]->lang_code == $lang->lang_code) { echo "selected";} else { echo "";} ?>>{{$lang->lang_name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="menutype">Menu Type <span class="note">*</span></label>
                    <div class="col-sm-9">
                        <select name="menutype" id="menutype" class="form-control">
                            <option value="">Select</option>
                            <option value="1" <?php if($results[0]->menu_type == 1) { echo "selected";} else { echo "";} ?>>Category</option>
                            <option value="2" <?php if($results[0]->menu_type == 2) { echo "selected";} else { echo "";} ?>>Collection</option>
                            <option value="3" <?php if($results[0]->menu_type == 3) { echo "selected";} else { echo "";} ?>>Pages</option>
                            <option value="4" <?php if($results[0]->menu_type == 4) { echo "selected";} else { echo "";} ?>>Custom Menu</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="txtmenu">Menu Name</label>
                    <div class="col-sm-9">
                      <input type=text name=txtmenu id="txtmenu" value="{{$results[0]->menu_name}}" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row" id="menu_link">
                      <label class="control-label col-sm-3" for="txtmenulink">Menu Link</label>
                      <div class="col-sm-9">
                          @if($results[0]->menu_type == 1 || $results[0]->menu_type == 3)
                          <input type="text" name="txtmenulink" id="txtmenulink" value="{{$results[0]->menu_link}}" class="form-control" readonly="readonly">
                          @else
                          <input type="text" name="txtmenulink" id="txtmenulink" value="{{$results[0]->menu_link}}" class="form-control" >
                          @endif
                      </div>
                  </div>
                  <div class="form-group row" id="megaMenu">
                      <label class="control-label col-sm-3" for="megamenu">Mega Menu</label>
                      <div class="col-sm-9">
                          <select name="megamenu" id="megamenu" class="form-control" onchange="showEditor();">
                              <option value="">Select</option>
                              <option value="1" <?php if($results[0]->mega_menu == 1) { echo "selected";} else { echo "";} ?>>Enable</option>
                              <option value="2" <?php if($results[0]->mega_menu == 2) { echo "selected";} else { echo "";} ?>>Disable</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group row" id="page_name">
                      <label class="control-label col-sm-3" for="pagename">Page Name</label>
                      <div class="col-sm-9">
                          <select name="pagename" id="pagename" class="form-control">
                              <option value="">Select</option>
                              @foreach($pages as $val)
                              @if($results[0]->menu_link == $val->slug)    
                              <option value="{{$val->slug}}" selected="selected">{{$val->page_name}}</option>
                              @else    
                              <option value="{{$val->slug}}">{{$val->page_name}}</option>
                              @endif
                              @endforeach
                          </select>
                      </div>
                  </div>
                  <div class="form-group row" id="category_name">
                    <label class="control-label col-sm-3" for="pagename">Categories Name</label>
                    <div class="col-sm-9">
                      <select name="category_slug" id="category_slug" class="form-control">
                        <option value="">Select</option>
                        @foreach($categories as $cate)
                        @if($results[0]->menu_link == $cate->category_slug)  
                        <option value="{{$cate->category_slug}}" selected="selected">{{$cate->category_name}}</option>
                        @else
                        <option value="{{$cate->category_slug}}">{{$cate->category_name}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="menuPosition">Menu Position<span class="note">*</span></label>
                    <div class="col-sm-9">
                    <select name="menuPosition" id="menuPosition" class="form-control" onchange="showPostion();">
                        <option value="">Select</option>
                        <option value="M" <?php if($results[0]->menu_position == 'M') { echo "selected";} else { echo "";} ?>>Main Menu</option>
                        <option value="U" <?php if($results[0]->menu_position == 'U') { echo "selected";} else { echo "";} ?>>Utility Menu</option>
                        <option value="F" <?php if($results[0]->menu_position == 'F') { echo "selected";} else { echo "";} ?>>Footer Menu</option>
                    </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3" for="txtbuttontext">Button Text</label>
                    <div class="col-sm-9">
                      <input type="text" name="txtbuttontext" value="{{$results[0]->button_text}}" id="txtbuttontext" class="form-control" >
                    </div>
                  </div>
                  <div class="form-group col-md-4">
                    <label class="control-label col-sm-6" for="sortOrder">Sort Order<span class="note">*</span></label>
                    <div class="col-sm-6">
                      <input type="text" name="sortOrder" id="sortOrder" value="{{$results[0]->sort_order}}" class="form-control" >
                    </div>
                  </div>
                  <div class="form-group row">
                      <label class="control-label col-sm-3" for="menustatus">Menu Status</label>
                      <div class="col-sm-9">
                          <select name="menustatus" id="menustatus" class="form-control">
                            <option value="1" <?php if($results[0]->menu_status == 1) { echo "selected";} else { echo "";} ?>>Enable</option>
                            <option value="2"  <?php if($results[0]->menu_status == 2) { echo "selected";} else { echo "";} ?>>Disable</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group row" id="textareaeditor">
                      <label class="control-label col-sm-3" for="editor1">Mega Menu Content</label>                            
                      <div class="col-sm-9">                               
                          <textarea name="editor1" id="editor1" rows="10" cols="20">
                              {{$results[0]->megamenu_content}}
                          </textarea>                             
                          
                      </div>
                  </div>
                </div>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('admin-script')
<script>
  var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>
<script>
$(document).ready(function() {
  $(".menu-form").validate({
    rules: {
      lang_code: "required",
      menutype : "required",
      txtmenu : "required",
      menuPosition : "required"
    },
    messages: {
      lang_code: "Please select language",
      menutype: "Please select menu type",
      txtmenu: "Please enter menu name",
      menuPosition: "Please select menu position"
    }
  });

  //Menu Position Check 

  var menuPosition = $('#menuPosition').val();

  if(menuPosition == "M")
  {
    $('#megaMenu').show();
  }
  else{
    $('#megaMenu').hide();
  }

var megamenu = $('#megamenu').val();

if(megamenu == 1){

  $('#textareaeditor').show();
  $('#editor1').show();
  $('#editor1').ckeditor({
    filebrowserImageBrowseUrl: route_prefix + '?type=Images',
    filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: route_prefix + '?type=Files',
    filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
  });
}
else{

$('#textareaeditor').hide();
$('#editor1').hide();
}
var menutypes = $('#menutype').val();
if(menutypes == 1){
$('#page_name').hide();
//$('#submenu_link').hide();
//$('#collection_name').hide();
$('#category_name').show();
}
if(menutypes == 2){
$('#page_name').hide();
$('#submenu_link').show();
// $('#collection_name').show();
$('#category_name').hide();
}
if(menutypes == 3){
//alert(typeno);
$('#page_name').show();
// $('#submenu_link').hide();
//$('#collection_name').hide();
$('#category_name').hide();
}
if(menutypes == 4){
$('#page_name').hide();
//$('#submenu_link').show();
// $('#collection_name').hide();
$('#category_name').hide();
}
if(menutypes == ""){
//console.log('test');
$('#page_name').hide();
//$('#submenu_link').show();
//$('#collection_name').hide();
$('#category_name').hide();
}
});
function showEditor(){
var megamenu = $('#megamenu').val();
// alert(megamenu);
if(megamenu == 1){

$('#textareaeditor').show();
$('#editor1').show();
CKEDITOR.replace( 'editor1' );
}
else{

$('#textareaeditor').hide();
$('#editor1').hide();
}
}

function showPostion(){

    var postition = $('#menuPosition').val();

    if(postition == 'M'){
       $('#megaMenu').show();
    }
    else{
      $('#megaMenu').hide();
      $('#textareaeditor').hide();
        $('#editor1').hide();
    }
}



$('#menutype').change(function(){
    var typeno = $(this).val();
   // alert(typeno);

    if(typeno == 1){
         $('#page_name').hide();
         $('#menu_link').hide();
         //$('#collection_name').hide();
          $('#category_name').show();

           $('#menu_link').val('');
    }
    if(typeno == 2){
         $('#page_name').hide();
         $('#menu_link').show();
         //$('#collection_name').show();
          $('#category_name').hide();

           //$('#menu_link').val('');
    }
    if(typeno == 3){
        //alert(typeno);
         $('#page_name').show();
         $('#menu_link').hide();
        // $('#collection_name').hide();
          $('#category_name').hide();

          
           $('#menu_link').val('');
    }
    if(typeno == 4){
         $('#page_name').hide();
         $('#menu_link').show();
         //$('#collection_name').hide();
          $('#category_name').hide();

          $('#pagename').val('');
          $('#category_slug').val('');
          //$('#collection_slug').val('');
    }
    if(typeno == ""){
        //console.log('test');
        $('#page_name').hide();
        $('#menu_link').show();
        // $('#collection_name').hide();
        $('#category_name').hide();


          $('#pagename').val('');
          $('#category_slug').val('');
          //$('#collection_slug').val('');
    }
});
</script>
@endsection