@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">

  <div class="inner-content">
    <div class="container-fluid">
      <div class="row">
        {!! Form::open(array('url' => '/Admin/addsubmenus','class'=>'form-horizontal','method'=>'post','enctype'=>'multipart/form-data')) !!}
        <input name="_token" type="hidden" value="{{csrf_token()}}"> 
        <div class="col-md-12">
          <div class="pull-right">
            <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Create Sub Menu</h3>
          </div>
          <div class="card-body">

          	  <div class="form-group row">
                        <label class="control-label col-sm-3" for="menutype">Menu Type <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="menutype" id="menutype" class="form-control" required>
                                <option value="" >select</option>
                                <option value="1">Category</option>
                                <option value="2">Collection</option>
                                <option value="3">Pages</option>
                                <option value="4">Custom Menu</option>
                            </select>
                        </div>
               </div>


            <div class="form-group row">
              <label class="control-label col-sm-3" for="menuName">Main Menu Name:</label>
              <div class="col-sm-9">
                <select name="menuName" id="menuName" class="form-control" required="required">
                  <option value="" >select</option>
                  @foreach($getmenu as $value)

                  <option value="{{$value->menu_id}}">{{$value->menu_name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3" for="txtsubmenuname">SubMenu Name:</label>
              <div class="col-sm-9">
                <input type="text" name="txtsubmenuname" id="txtsubmenuname" class="form-control" required="required">
              </div>
            </div>
            <div class="form-group row" id="submenu_link">
              <label class="control-label col-sm-3" for="txtsubmenulink">SubMenu Link:</label>
              <div class="col-sm-9">
                <input type="text" name="txtsubmenulink" id="txtsubmenulink" class="form-control" >
              </div>
            </div>

             <div class="form-group row" id="page_name">
                        <label class="control-label col-sm-3" for="pagename">Page Name:</label>
                        <div class="col-sm-9">
                            <select name="pagename" id="pagename" class="form-control">
                                <option value="" >select</option>
                                @foreach($pages as $val)
                                <option value="{{$val->slug}}">{{$val->page_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" id="category_name">
                        <label class="control-label col-sm-3" for="pagename">Categories Name:</label>
                        <div class="col-sm-9">
                            <select name="category_slug" id="category_slug" class="form-control">
                                <option value="" >select</option>
                                @foreach($categories as $cate)
                                <option value="{{$cate->category_slug}}">{{$cate->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                   
            <div class="form-group row">
              <label class="control-label col-sm-3" for="submenustatus">SubMenu Status:</label>
              <div class="col-sm-9">
                <select name="submenustatus" id="submenustatus" class="form-control" required="required">
                  <option value="" >select</option>
                  <option value="1">Active</option>
                  <option value="2">DeActive</option>
                </select>
              </div>
            </div>
            <div class="buttons text-center">
              <button class="btn btn-danger" type="reset">Reset</button>
              <button class="btn btn-success" type="submit">Add Submenu</button>
            </div>
            
            
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">

        <h4 align="center">{{ Session::get('message') }}</h4>

        <center>

          <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

        </center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('admin-script')
<script type="text/javascript">
    $(document).ready(function() {
     

        $('#page_name').hide();
        $('#category_name').hide();
        $('#collection_name').hide();
    });
  


$('#menutype').change(function(){
    var typeno = $(this).val();
   // alert(typeno);

    if(typeno == 1){
         $('#page_name').hide();
         $('#submenu_link').hide();
         //$('#collection_name').hide();
          $('#category_name').show();
    }
    if(typeno == 2){
         $('#page_name').hide();
         $('#submenu_link').show();
         //$('#collection_name').show();
          $('#category_name').hide();
    }
    if(typeno == 3){
         $('#page_name').show();
         $('#submenu_link').hide();
        // $('#collection_name').hide();
          $('#category_name').hide();
    }
    if(typeno == 4){
         $('#page_name').hide();
         $('#submenu_link').show();
         //$('#collection_name').hide();
          $('#category_name').hide();
    }
    if(typeno == ""){
        //console.log('test');
        $('#page_name').hide();
        $('#submenu_link').show();
         //$('#collection_name').hide();
        $('#category_name').hide();
    }
});
</script>
@endsection