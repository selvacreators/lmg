
@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">

    <div class="inner-content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Create Utility Menu</h3>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ URL('Admin/add_Utillitys')}}" >
                      <input name="_token" type="hidden" value="{{csrf_token()}}"> 
                      <div class="form-group">
                        <label class="control-label col-xs-3" for="txtUtillity">Utillity Name:</label>
                        <div class="col-xs-6">
                            <input type=text name="txtUtillity" id="txtUtillity" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="txtUtillitylink">Utillity Link:</label>
                        <div class="col-xs-6">
                            <input type="text" name="txtUtillitylink" id="txtUtillitylink" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="Utillitystatus">Utillity Status:</label>
                        <div class="col-xs-6">
                            <select name="Utillitystatus" id="Utillitystatus" class="form-control" required="required">
                                <option value="" >select</option>
                                <option value="1">Active</option>
                                <option value="2">DeActive</option>
                            </select>
                        </div>
                    </div>
                    <div class="buttons text-center">
                        <button class="btn btn-success" type="reset">Reset</button>
                        <button class="btn btn-success" type="submit">Add Menu</button>
                    </div> 
                    
                </form>
            </div>
        </div>

    </div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

              <center>

                <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

            </center>
            {{ Session::forget('message') }}
        </div>
    </div>
</div>
</div>
@endif
@stop