@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/addmenus','class'=>'form-horizontal menu-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/menutype" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Create Menu</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="lang_code" class="control-label col-sm-3">Language<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" class="form-control" id="lang_code">
                                            @foreach($language as $lng)
                                            <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="menutype">Menu Type <span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="menutype" id="menutype" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Category</option>
                                            <option value="2">Collection</option>
                                            <option value="3">Pages</option>
                                            <option value="4">Custom Menu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="txtmenu">Menu Name <span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type=text name=txtmenu id="txtmenu" class="form-control"="required">
                                    </div>
                                </div>
                                <div class="form-group row" id="menu_link">
                                    <label class="control-label col-sm-3" for="txtmenulink">Menu Link</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="txtmenulink" id="txtmenulink" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group row" id="page_name">
                                    <label class="control-label col-sm-3" for="pagename">Page Name</label>
                                    <div class="col-sm-9">
                                        <select name="pagename" id="pagename" class="form-control">
                                            <option value="" >Select</option>
                                            @foreach($pages as $val)
                                            <option value="{{$val->slug}}">{{$val->page_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" id="category_name">
                                    <label class="control-label col-sm-3" for="pagename">Categories Name</label>
                                    <div class="col-sm-9">
                                        <select name="category_slug" id="category_slug" class="form-control">
                                            <option value="" >Select</option>
                                            @foreach($categories as $cate)
                                            <option value="{{$cate->category_slug}}">{{$cate->category_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="menuPosition">Menu Position <span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="menuPosition" id="menuPosition" class="form-control"="required">
                                            <option value="" >Select</option>
                                            <option value="M">Main Menu</option>
                                            <option value="U">Utility Menu</option>
                                            <option value="F">Footer Menu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div><hr><br>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-sm-6" for="txtbuttontext">Button Style</label>
                                    <div class="col-sm-6">
                                        <select name="txtbuttontext" id="txtbuttontext" class="form-control"="required">
                                            <option value="1">Enable</option>
                                            <option selected value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-sm-6" for="megamenu">Mega Menu</label>
                                    <div class="col-sm-6">
                                        <select name="megamenu" id="megamenu" class="form-control" onchange="showEditor();">
                                            <option value="1">Enable</option>
                                            <option selected value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-sm-6" for="sortOrder">Sort Order</label>
                                    <div class="col-sm-6">
                                         <input type="text" name="sortOrder" id="sortOrder" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label col-sm-6" for="menustatus">Menu Status</label>
                                    <div class="col-sm-6">
                                        <select name="menustatus" id="menustatus" class="form-control"="required">
                                            <option value="1">Enable</option>
                                            <option selected value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row" id="textareaeditor">
                                    <div class="form-group" id="textareaeditor">
                                        <label class="control-label col-sm-3" for="editor1">Mega Menu Content</label>
                                        <div class="col-sm-9">                               
                                            <textarea name="editor1" id="editor1" class="form-control">This is Mega Menu content.</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".menu-form").validate({
        rules: {
            lang_code: "required",
            menutype : "required",
            txtmenu : "required",
            menuPosition : "required"
        },
        messages: {
            lang_code: "Please select language",
            menutype: "Please select menu type",
            txtmenu: "Please enter menu name",
            menuPosition: "Please select menu position"
        }
    });
});
function readSliderURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#StrImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#textareaeditor').hide();
        $('#editor1').hide();

        $('#page_name').hide();
        $('#category_name').hide();
        $('#collection_name').hide();
    });
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    function showEditor()
    {
        var megamenu = $('#megamenu').val();
       // alert(megamenu);
       if(megamenu == 1){

        $('#textareaeditor').show();
        $('#editor1').show();
        
        $('#editor1').ckeditor({      
          filebrowserImageBrowseUrl: route_prefix + '?type=Images',
          filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
          filebrowserBrowseUrl: route_prefix + '?type=Files',
          filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
        });
    }
    else{
        $('#textareaeditor').hide();
        $('#editor1').hide();
    }
}
$('#menutype').change(function(){
    var typeno = $(this).val();
   // alert(typeno);

    if(typeno == 1){
         $('#page_name').hide();
         $('#menu_link').hide();
         //$('#collection_name').hide();
          $('#category_name').show();
    }
    if(typeno == 2){
         $('#page_name').hide();
         $('#menu_link').show();
         //$('#collection_name').show();
          $('#category_name').hide();
    }
    if(typeno == 3){
         $('#page_name').show();
         $('#menu_link').hide();
        // $('#collection_name').hide();
          $('#category_name').hide();
    }
    if(typeno == 4){
         $('#page_name').hide();
         $('#menu_link').show();
         //$('#collection_name').hide();
          $('#category_name').hide();
    }
    if(typeno == ""){
        //console.log('test');
        $('#page_name').hide();
        $('#menu_link').show();
        // $('#collection_name').hide();
        $('#category_name').hide();
    }
});
</script>
@endsection