@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute fixed-top">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <a class="navbar-brand" href="#pablo">Edit Utillity</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <form class="navbar-form">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control" placeholder="Search...">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#pablo">
                            <i class="material-icons">dashboard</i>
                            <p>
                                <span class="d-lg-none d-md-block">Stats</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">notifications</i>
                            <span class="notification">5</span>
                            <p>
                                <span class="d-lg-none d-md-block">Some Actions</span>
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Mike John responded to your email</a>
                            <a class="dropdown-item" href="#">You have 5 new tasks</a>
                            <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                            <a class="dropdown-item" href="#">Another Notification</a>
                            <a class="dropdown-item" href="#">Another One</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#pablo">
                            <i class="material-icons">person</i>
                            <p>
                                <span class="d-lg-none d-md-block">Account</span>
                            </p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            
            <form method="POST" action="{{ URL('Admin/edit_Utillitys')}}" >
              <input name="_token" type="hidden" value="{{csrf_token()}}"> 
              <input name="txtut_id" type="hidden" value="{{$utres[0]->ut_id}}"> 

              
              <table class="table table-responsive">

                <tr>
                    <td>Utillity Name</td>
                    <td><input type=text name="txtUtillity" id="txtUtillity" value="{{$utres[0]->Utillity_name}}" class="form-control" required="required"></td>
                </tr>

                <tr>
                    <td>Utillity Link</td>
                    <td><input type="text" name="txtUtillitylink" id="txtUtillitylink" value="{{$utres[0]->Utillity_link}}" class="form-control" required="required"></td>
                </tr>
                <tr>
                    <td>Utillity Status</td>
                    <td>
                        <select name="Utillitystatus" id="Utillitystatus" class="form-control" required="required">
                            <option value="" >select</option>
                            <option value="1" <?php if($utres[0]->Utillity_status == 1) { echo "selected";} else { echo "";} ?>>Active</option>
                            <option value="2" <?php if($utres[0]->Utillity_status == 2) { echo "selected";} else { echo "";} ?>>DeActive</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td><input type="reset" class="btn btn-primary btn-sm" ></td>
                    <td colspan="2"><input type="submit" value="Submit" class="btn btn-primary btn-sm" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

              <center>

                <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

            </center>
            {{ Session::forget('message') }}
        </div>
    </div>
</div>
</div>
@endif
@stop