@extends('layouts.layout_admin')
@section('admin-style')
<link href="{{ Config::get('app.head_url') }}assets/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
@endsection
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Newsletter Subscribers List</h3>
              </div>
              <div class="card-body">
                <nav class="nav-container">
                  <ul class='nav nav-tabs text-center' role="tablist">
                  @if($language)
                    @foreach($language as $key =>$lan)
                      @if($lan->lang_code == 'en')
                        <?php $check = "active";?>
                      @else
                        <?php $check = "";?>
                      @endif
                      <li><a data-toggle="tab" class="{{$check}} show" href="#{{$lan->lang_name}}">{{$lan->lang_name}}</a></li>
                    @endforeach
                  @endif
                  </ul>
                </nav>
                <div class="tab-content pt-0 pb-0">
                  @if($language)
                    @foreach($language as $key =>$lan)
                      @if($lan->lang_code == 'en')
                        <?php $check = "active";?>
                        <?php  $langcode = $lan->lang_code; ?>
                      @else
                        <?php $check = "";?>
                        <?php  $langcode = $lan->lang_code; ?>
                      @endif
                      <div id="{{$lan->lang_name}}" class="tab-pane {{$check}} in fieldset">
                        <form method="POST" action="{{ URL('/Admin/enquiry_export')}}" class="form-inline pull-right">
                            <div class="row mb20">
                              <input name="lang_code" type="hidden" value="{{$langcode}}">
                              <input name="type" type="hidden" id="type" value="newsletter">
                              <input name="_token" type="hidden" value="{{csrf_token()}}">
                                <div class="mr20">
                                  <input type="text" name="fromdate" class="form-control datepicker" value="" placeholder="Start Date" data-date-format="YYYY-MM-DD">
                                  <?php echo '<div class="text-danger">'.$errors->enquiry_errors->first('fromdate').'</div>';?>
                                </div>
                                <div class="mr20">
                                  <input type="text" name="todate" class="form-control datepicker" value="" placeholder="End Date" data-date-format="YYYY-MM-DD">
                                </div>
                                <button type="submit" name="btnadd" id="btnadd" class="btn btn-sm btn-success">Export Data</button>
                            </div>
                        </form>
                        <table id="datatable" class="datatable table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                          <thead>
                            <tr>
                              <th>S.No</th>
                              <th>Language</th>
                              <th>Date</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Email</th>
                              <th>Status</th>
                              <th class="col-xs-2">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $i=1;?>
                            @if($nlenquiry)
                            @foreach($nlenquiry as $row)
                            @if($row->lang_code == $lan->lang_code)
                            <tr>
                              <td><?php echo $i++;?></td>
                              <td><?php echo $row->lang_code;?></td>
                              <td><?php echo date_format(new DateTime($row->created_at),'Y/m/d');?></td>
                              <td><?php echo $row->first_name;?></td>
                              <td><?php echo $row->last_name;?></td>
                              <td><?php echo $row->email;?></td>
                              <td>
                              @if($row->status == '1')
                                <label class="label label-info">Pending</label>
                              @elseif($row->status == '2')
                                <label class="label label-success">Done</label>
                              @else
                                <label class="label label-danger">Reject</label>
                              @endif
                              </td>
                              <td>
                                <a href="/Admin/edit/newsletter-subscribers/{{$row->id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                                <a href="/Admin/delete/newsletter-subscribers/{{$row->id}}" onclick="return confirm('Are You Sure Delete Newsletter Subscribers?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                              </td>
                            </tr>
                            @endif
                            @endforeach
                            @endif
                          </tbody>
                        </table>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop

@section('date-script')
<script src="{{ Config::get('app.head_url') }}assets/js/moment.min.js"></script>
<script src="{{ Config::get('app.head_url') }}assets/js/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('admin-script')
<script type="text/javascript">
   $(document).ready(function() {
      md.initFormExtendedDatetimepickers();
    });
</script>
@endsection