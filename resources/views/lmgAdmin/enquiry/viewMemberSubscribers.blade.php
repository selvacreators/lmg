@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Newsletter Subscribers List</h3>
              </div>
              <div class="card-body">
                <nav class="nav-container">
                  <ul class='nav nav-tabs text-center' role="tablist">
                    <li><a data-toggle="tab" class="active show" href="#vietname">Vietnam</a></li>
                    <li><a data-toggle="tab" href="#english">English</a></li>
                  </ul>
                </nav>
                <div class="tab-content pt-0 pb-0">
                  <div id="vietname" class="tab-pane active in fieldset">
                    <table id="datatable" class="datatable table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Language</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Sented On</th>
                          <th>Status</th>
                          <th class="col-xs-2">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i=1;?>
                        @if($mssubscribe)
                        @foreach($mssubscribe as $row)
                        @if($row->lang_code == 'vi')
                        <tr>
                          <td><?php echo $i++;?></td>
                          <td><?php echo $row->lang_code;?></td>
                          <td><?php echo $row->name;?></td>
                          <td><?php echo $row->email;?></td>
                          <td><?php echo $row->created_at;?></td>
                          <td>
                          <?php if($row->status == '1') {?>
                            <label class="label label-info">Pending</label>
                          <?php } else { ?>
                            <label class="label label-success">Done</label>
                          <?php } ?>
                          </td>
                          <td>
                            <a href="/Admin/edit/newsletter-subscribers/{{$row->enq_id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                            <a href="/Admin/delete/newsletter-subscribers/{{$row->enq_id}}" onclick="return confirm('Are You Sure Delete Newsletter Subscribers?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                          </td>
                        </tr>
                        @endif
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
                  <div id="english" class="tab-pane fade fieldset">
                    <table id="datatable" class="datatable table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Language</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Sented On</th>
                          <th>Status</th>
                          <th class="col-xs-2">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i=1;?>
                        @if($mssubscribe)
                        @foreach($mssubscribe as $row)
                        @if($row->lang_code == 'en')
                        <tr>
                          <td><?php echo $i++;?></td>
                          <td><?php echo $row->lang_code;?></td>
                          <td><?php echo $row->name;?></td>
                          <td><?php echo $row->email;?></td>
                          <td><?php echo $row->created_at;?></td>
                          <td>
                          <?php if($row->status == '1') {?>
                            <label class="label label-info">Pending</label>
                          <?php } else { ?>
                            <label class="label label-success">Done</label>
                          <?php } ?>
                          </td>
                          <td>
                            <a href="/Admin/edit/newsletter-subscribers/{{$row->enq_id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                            <a href="/Admin/delete/newsletter-subscribers/{{$row->enq_id}}" onclick="return confirm('Are You Sure Delete Newsletter Subscribers?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></a>
                          </td>
                        </tr>
                        @endif
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('admin-script')
  <script src="{{ Config::get('app.head_url') }}admin/datatables/js/jquery.dataTables.min.js"></script>
  <script src="{{ Config::get('app.head_url') }}admin/datatables/js/dataTables.responsive.min.js"></script>
  <script>
    $(document).ready(function () {
        $('.datatable').dataTable();
    });
  </script>
@endsection