@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/newsletter-subscribers','class'=>'form-horizontal newsletter-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="eid" value="{{$nlenquiry->id}}">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/newsletter-subscribers" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Newsletter Subscribers</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        @foreach($language as $lng)
                                          @if($lng->lang_code == $nlenquiry->lang_code)
                                            <label class="control-label">{{$lng->lang_name}}</label>
                                            <input type="hidden" name="lang_code" value="{{$lng->lang_code}}"/>
                                          @endif
                                        @endforeach
                                    </div>
                                </div>
                              
                                <div class="form-group row">
                                    <label for="fname" class="control-label col-sm-3">First Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="fname" name="fname" value="{{$nlenquiry->first_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lname" class="control-label col-sm-3">Last Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="lname" name="lname" value="{{$nlenquiry->last_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="control-label col-sm-3">Email:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="email" name="email" value="{{$nlenquiry->email}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?=(($nlenquiry->status=='1')?'selected':'');?>>Pending</option>
                                            <option value="2" <?=(($nlenquiry->status=='2')?'selected':'');?>>Done</option>
                                            <option value="3" <?=(($nlenquiry->status=='3')?'selected':'');?>>Reject</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')

<script src="{{ Config::get('app.head_url') }}admin/ckeditor/ckeditor.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
CKEDITOR.replace('content-editor');
CKEDITOR.config.allowedContent = true;
</script>
<script type="text/javascript">
$(document).ready(function(){
  $(".newsletter-form").validate({
    rules: {
        language: "required",
        fname : "required",
        lname : "required",
        email: {
            required: true,
            email: true
        },
        status : "required"
    },
    messages: {
      language: "Please select language",
      fname: "Please enter first name",
      lname: "Please enter last name",
      email: "Please enter valid email address",
      status: "Please select status"
    }
  });
});
</script>
@endsection