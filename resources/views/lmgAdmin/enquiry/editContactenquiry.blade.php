@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/contact-enquiry','class'=>'form-horizontal contact-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="eid" value="{{$cenquiry->enq_id}}">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/contact-enquiry" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Contact Enquiry</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        @foreach($language as $lng)
                                          @if($lng->lang_code == $cenquiry->lang_code)
                                            <label class="control-label">{{$lng->lang_name}}</label>
                                            <input type="hidden" name="lang_code" value="{{$lng->lang_code}}"/>
                                          @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="control-label col-sm-3">Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$cenquiry->name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="control-label col-sm-3">Email:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="email" name="email" value="{{$cenquiry->email}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="mobile" class="control-label col-sm-3">Phone:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{$cenquiry->mobile}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="comment" class="control-label col-sm-3">Comment:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" cols="5" id="comment" name="comment" rows="4">{{$cenquiry->comment}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?=(($cenquiry->status=='1')?'selected':'');?>>Pending</option>
                                            <option value="2" <?=(($cenquiry->status=='2')?'selected':'');?>>Done</option>
                                            <option value="3" <?=(($cenquiry->status=='3')?'selected':'');?>>Reject</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".contact-form").validate({
    rules: {
        language: "required",
        name : "required",
        comment : "required",
        email: {
            required: true,
            email: true
        },
        mobile:{
            minlength:9,
            maxlength:10,
            number: true
        },
        status : "required"
    },
    messages: {
      language: "Please select language",
      name: "Please enter name",
      comment: "Please enter comment",
      email: "Please enter valid email address",
      mobile: "Please enter your mobile number",
      status: "Please select status"
    }
  });
});
</script>
@endsection