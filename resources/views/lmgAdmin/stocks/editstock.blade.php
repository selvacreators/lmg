@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/edit_stock','class'=>'form-horizontal stock-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input name="txtstock_id" type="hidden" value="{{$stock[0]->stock_status_id}}">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/stocks" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Stock</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="lang" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang" id="lang" class="form-control">
                                            @foreach($language as $lang)
                                            <option value="{{$lang->lang_code}}" <?php if($stock[0]->lang_code == $lang->lang_code) { echo "selected";}?>>{{$lang->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="stock_name" class="control-label col-sm-3">Stock Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="stock_name" id="stock_name" value="{{$stock[0]->name}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option <?php if($stock[0]->st_status == '1') { echo "selected";}?> value="1">Enable</option>
                                            <option <?php if($stock[0]->st_status == '2') { echo "selected";}?> value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".stock-form").validate({
    rules:
    {
        lang : "required",
        stock_name: "required",
        status : "required"
    },
    messages:
    {
      lang: "Please choose language",
      stock_name: "Please enter stock name",
      status: "Please select status"
    }
  });
});
</script>
@endsection