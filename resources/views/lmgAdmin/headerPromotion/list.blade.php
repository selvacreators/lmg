@extends('layouts.layout_admin')
@section('content')
<style type="text/css">
.alert { margin-top: 15px; }
.alert .close{
margin-top: 12px;
opacity: 1;
text-shadow: none;
}
</style>
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            @if(session('success'))
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Success!</strong> {{ session('success') }}
            </div>
            @endif
            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Header Bar Promotions</h3>
              <div class="pull-right">              
                <a href="/Admin/create-header-promotion" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp; Add New</a>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="datatable" class="datatable table table-striped table-advance table-hover bordered dt-responsive nowrap">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Promo Code</th>
                      <th>Promo Message</th>
                      <th>Device Type</th>
                      <th>Language</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th class="col-xs-2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(sizeof($promotions)): $i = 1; foreach($promotions as $promotion): ?>
                      <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $promotion->code }}</td>
                        <td>{{ str_limit($promotion->msg,10) }}</td>
                        <td>{{ $promotion->d_rule }}</td>
                        <td>{{ $promotion->lang }}</td>
                        <td>{{ $promotion->date_from }}</td>
                        <td>{{ $promotion->date_to }}</td>
                        <td>
                       <a href="{{ route('admin.editHeaderPromo',$promotion->promo_id) }}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                         <!--  <a href="#"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a> -->
                          <a href="{{ route('admin.deleteHeaderPromo',$promotion->promo_id) }}"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o "></i></button></a>
                        </td>
                      </tr>
                    <?php endforeach; endif; ?>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('admin-script')
@endsection