@extends('layouts.layout_admin')
@section('admin-style')
<link href="{{ Config::get('app.head_url') }}assets/css/jquery-ui.css" rel="stylesheet" />
<style type="text/css" media="screen">
.note {
  position: absolute;
  margin-top: 4px;
  margin-left: 8px;
  font-size: 1.4rem;
  font-weight: 500;
  line-height: 1;
  color: #e22626;
}
.ui-widget {font-size: inherit;}
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 40%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
.ui-timepicker-div .ui_tpicker_unit_hide{ display: none; }

.ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input { background: none; color: inherit; border: none; outline: none; border-bottom: solid 1px #555; width: 95%; }
.ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input:focus { border-bottom-color: #aaa; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 40% 10px 10px; }

/* Shortened version style */
.ui-timepicker-div.ui-timepicker-oneLine { padding-right: 2px; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time, 
.ui-timepicker-div.ui-timepicker-oneLine dt { display: none; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time_label { display: block; padding-top: 2px; }
.ui-timepicker-div.ui-timepicker-oneLine dl { text-align: right; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd, 
.ui-timepicker-div.ui-timepicker-oneLine dl dd > div { display:inline-block; margin:0; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_minute:before,
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_second:before { content:':'; display:inline-block; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_millisec:before,
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_microsec:before { content:'.'; display:inline-block; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide,
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide:before{ display: none; }
</style>
@endsection
@section('content')

<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                      @if(isset($promo))
                          {!! Form::model($promo, array('url' => route('admin.updateHeaderPromo', $promo->promo_id),'class'=>'form-horizontal category-form','method'=>'post'))!!}
                      @else
                          {!! Form::open(array('url' => route('admin.saveHeaderPromo'),'class'=>'form-horizontal category-form','method'=>'post', 'autocomplete' => 'off'))!!}
                      @endif
                          <div class="card">
                            <div class="card-header">
                                <a href="{{ route('admin.headerPromo') }}" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="button" onclick="location.reload()">Reset</button></div>
                                <h3 class="card-title">Create New Header Bar Promotion</h3>
                            </div>
                            <div class="card-body">
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="msg">Promo Message <span class="note">*</span></label>
                                <div class="col-sm-6">
                                  {!! Form::text('msg', null, ['class'=> 'form-control', 'required' => 'required']) !!}
                                  <label class="text-danger">{{ $errors->first('msg') }}</label>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="code">Promo Code <span class="note">*</span></label>
                                <div class="col-sm-6">
                                  {!! Form::text('code', null, ['class'=> 'form-control', 'required' => 'required']) !!}
                                  <label class="text-danger">{{ $errors->first('code') }}</label>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="btn_txt">Promo Button Label </label>
                                <div class="col-sm-6">
                                  {!! Form::text('btn_txt', null, ['class'=> 'form-control']) !!}
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="url">Promo Page url </label>
                                <div class="col-sm-6">
                                  {!! Form::text('url', null, ['class'=> 'form-control']) !!}
                                </div>
                              </div>

                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="d_rule">Display Rule <span class="note">*</span></label>
                                <div class="col-sm-6">
                                  <?php $d_rule = array('All' => 'All Device','Mobile' => 'Mobile', 'Desktop' => 'Desktop'); ?>
                                  {!! Form::select('d_rule', $d_rule, Input::old('d_rule'), ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                                <label class="text-danger">{{ $errors->first('d_rule') }}</label>
                              </div>

                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="lang">language <span class="note">*</span></label>
                                <div class="col-sm-6">
                                  {!! Form::select('lang', $language, Input::old('lang'), ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                                <label class="text-danger">{{ $errors->first('lang') }}</label>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="user_type">User Type <span class="note">*</span></label>
                                <div class="col-sm-6">
                                  <?php $options = array('All' => 'All', 'New User' => 'New User', 'Returning User' => 'Returning User'); ?>
                               {!! Form::select('user_type', $options, Input::old('user_type'), array('class' => 'form-control')) !!}                                 
                               <label class="text-danger">{{ $errors->first('user_type') }}</label>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="pages">Page URL to Show </label>
                                <div class="col-sm-6">
                                  {!! Form::textarea('pages', null, ['class' => 'form-control', 'rows' => '5']) !!}
                                  <p class="help-block">Enter each url in seperate lines</p>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="date_from">Start Time </label>
                                <div class="col-sm-6">
                                   {!! Form::text('date_from', null, ['class'=> 'form-control','readonly','id' =>'date_from']) !!}
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="date_to">End Time </label>
                                <div class="col-sm-6">
                                   {!! Form::text('date_to', null, ['class'=> 'form-control','readonly','id' =>'date_to']) !!}
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="status">Status <span class="note">*</span></label>
                                <div class="col-sm-6">
                                 

                                   <?php $options1 = array(1  =>'Enabled', 2 =>'Disabled'); ?>
                               {!! Form::select('status', $options1, Input::old('status'), array('class' => 'form-control','required' => 'required')) !!}
                               <label class="text-danger">{{ $errors->first('status') }}</label>
                                </div>
                              </div>
                            </div>
                          </div>
                       {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
 <script src="{{ Config::get('app.head_url') }}assets/js/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-sliderAccess.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#date_from,#date_to').datetimepicker({
        dateFormat:'dd-mm-yy',
        timeFormat:'hh:mm tt',
        changeMonth: true,
        changeYear: true,
      });
    })
  </script>
@endsection