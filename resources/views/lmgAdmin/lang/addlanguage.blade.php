@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/add_language','class'=>'form-horizontal language-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/language" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Add Language</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="lang_name" class="control-label col-sm-3">Language Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type=text name="lang_name" id="lang_name"  value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lang_code" class="control-label col-sm-3">Language Code:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="lang_code" id="lang_code" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sort_order" class="control-label col-sm-3">Sort Order:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="sort_order" id="sort_order" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Enable</option>
                                            <option value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".language-form").validate({
    rules:
    {
        lang_name: "required",
        lang_code : "required",
        sort_order : "required",
        status : "required"
    },
    messages:
    {
      lang_name: "Please select language",
      lang_code: "Please enter name",
      sort_order: "Please enter sort order",
      status: "Please select status"
    }
  });
});
</script>
@endsection