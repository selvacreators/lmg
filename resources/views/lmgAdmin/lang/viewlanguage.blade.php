@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
              <div class="card-header">
                <a href="/Admin/add/language" class="btn btn-success btn-sm pull-right">Add New Language</a>
                <h3 class="card-title">Language List</h3>
              </div>
              <div class="card-body">
                <table id="datatable" class="datatable table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Language Code</th>
                      <th>Language Name</th>
                      <th>Sort Order</th>
                      <th>Status</th>
                      <th class="col-xs-2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1;?>
                    @if($language)
                    @foreach($language as $row)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$row->lang_code}}</td>
                      <td>{{$row->lang_name}}</td>
                      <td>{{$row->sort_order}}</td>
                      <td>
                      <?php if($row->status == '1') {?>
                        <label class="label label-success">Enable</label>
                      <?php } else { ?>
                        <label class="label label-info">Disable</label>
                      <?php } ?>
                      </td>
                      <td>
                        <a href="/Admin/edit/language/{{$row->lang_id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a href="/Admin/delete/language/{{$row->lang_id}}" onclick="return confirm('Are You Sure Delete Language?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o "></i></button></a>
                      </td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('admin-script')
@endsection