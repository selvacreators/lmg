@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/collection','class'=>'form-horizontal collection-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input name="col_id" type="hidden" value="{{$collection->collection_id}}">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/collection" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Collection</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="lang_code">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" id="lang_code" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($language as $lang)
                                            <option value="{{$lang->lang_code}}" <?php if($collection->lang_code == $lang->lang_code) { echo "selected";}?>>{{$lang->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="collection_name" class="control-label col-sm-3">Collection Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="collection_name" id="collection_name" value="{{$collection->collection_name}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">            
                                  <label class="control-label col-sm-3" for="btn_url">Page Slug & Button Url:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="btn_url" id="btn_url" value="{{$collection->collection_btn_url}}" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">
                                    <label for="col_caption" class="control-label col-sm-3">Collection Caption:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="col_caption" id="col_caption" value="{{$collection->col_caption}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="brand_id">Brand Name:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <select name="brand_id" id="brand_id" class="form-control">
                                      <option value="">Select</option>
                                      @foreach($brand as $brand)
                                      <option value="{{$brand->brand_id}}" <?=(($collection->brand_id == $brand->brand_id)?'selected':'');?>>{{$brand->brand_name}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="collection_banner">Banner Image:</label>
                                  <div class="col-sm-4">
                                    <input type="file" name="collection_banner" id="collection_banner" class="form-control" accept="image/*" data-type="image" onchange="readBannerURL(this);">
                                    <input type="hidden" name="insert_collection_banner" id="insert_collection_banner" value="{{$collection->collection_banner}}">
                                    <div class="clearfix"></div>
                                    <label class="error">Files size must be below 1 MB with 1600x400</label>
                                  </div>
                                  <div class="col-sm-3">
                                    @if(!empty($collection->collection_banner))
                                    <img src="{{ Config::get('app.head_url') }}collection/banner/{{$collection->collection_banner}}" id="BannImg" width="100"/>
                                    @else
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="BannImg" width="100">
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="col_des">Collection Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="col_des" id="col_des">{{$collection->col_des}}</textarea>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="feature_image">Feature Image:</label>
                                  <div class="col-sm-4">
                                    <input type="file" name="feature_image" id="feature_image" class="form-control" accept="image/*" data-type="image" onchange="readFeatureURL(this);">
                                    <input type="hidden" name="insertfeature_image" id="insertfeature_image" value="{{$collection->feature_image}}">
                                    <div class="clearfix"></div>
                                    <label class="error">Files size must be below 1 MB with 1260x900</label>
                                  </div>
                                  <div class="col-sm-3">
                                    @if(!empty($collection->feature_image))
                                    <img src="{{ Config::get('app.head_url') }}collection/images/{{$collection->feature_image}}" id="FeatImg" width="100"/>
                                    @else
                                    <img src="{{Config::get('app.head_url')}}logo/temp.jpg" id="BannImg" width="100"/>
                                    @endif
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="position">Image Position:</label>
                                  <div class="col-sm-9">
                                    <select name="position" id="position" class="form-control">
                                      <option value="left" <?php if($collection->position == 'left') { echo "selected";} else { echo "";} ?>>Left</option>
                                      <option value="right" <?php if($collection->position == 'right') { echo "selected";} else { echo "";} ?>>Right</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="btn_text">Button Text:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="btn_text" id="btn_text" class="form-control" value="{{$collection->collection_btn_text}}">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_title">Meta Title:<span class="note">*</span></label>
                                  <div class="col-sm-9">
                                    <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{$collection->meta_title}}" >
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_keyword">Meta Keyword:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="meta_keyword" id="meta_keyword" class="form-control" value="{{$collection->meta_keyword}}" >
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="meta_desc">Meta Description:</label>
                                  <div class="col-sm-9">
                                    <textarea name="meta_desc" id="meta_desc" rows="6" cols="10" class="form-control" >{{$collection->meta_desc}}</textarea>
                                  </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sort_order" class="control-label col-sm-3">Sort Order:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="sort_order" id="sort_order" value="{{$collection->sort_order}}" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="col_status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="col_status" id="col_status" class="form-control">
                                          <option value="" >Select</option>
                                          <option value="1" <?php if($collection->col_status == '1') { echo "selected";} else { echo "";} ?>>Enable</option>
                                          <option value="2" <?php if($collection->col_status == '2') { echo "selected";} else { echo "";} ?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".collection-form").validate({
    rules:
    {
      lang_code: "required",
      collection_name: "required",
      brand_id: "required",
      meta_title: "required",
      btn_url: "required",
      col_status: "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      collection_name: "Please enter collection name",
      brand_id: "Please enter brand name",
      meta_title: "Please enter meta title",
      btn_url: "Please enter button url",
      col_status: "Please select status"
    }
  });
  $("#lang_code").change(function(){
    var lang = $(this).val(), ajxLoader = $("#ajax-loader");
    var c_id = $("input[name='col_id']").val();
    $.ajax({
      url: '/Admin/getCollectionByLang?lang='+lang+'&c='+c_id,
      type: 'get',
      dataType: 'json',
      beforeSend: function(){ ajxLoader.show(); },
      success:function(result){
        $('#brand_id').empty();
        $('#brand_id').append('<option>Select</option>');
        var select;
        $.each( result.bdata,function( key, value )
        {
          if(result.bdata && result.cdata)
          {
            if(result.cdata.brand_id == value.brand_id){
              select = 'selected';
            $('#brand_id').append('<option '+select+' value="'+ value.brand_id +'">'+ value.brand_name +'</option>');
            }
            $('#brand_id').append('<option value="'+ value.brand_id +'">'+ value.brand_name +'</option>');
          }
          else
          {
            $('#brand_id').append('<option value="'+ value.brand_id +'">'+ value.brand_name +'</option>');
          }
        });
        if(result.cdata)
        {
          var count = Object.keys(result.cdata).length;
          if(count)
          {
            populateFormData(result.cdata);
          }
        }
      },
      complete:function(){ ajxLoader.hide(); }
    });
  });
  function populateFormData(response) {
    $("#collection_name").val(response.collection_name);
    $("#collection_slug").val(response.collection_slug);
    $("#col_caption").val(response.col_caption);

    CKEDITOR.instances.col_des.setData(response.col_des); 
    $("#btn_text").val(response.collection_btn_text);
    $("#btn_url").val(response.collection_btn_url);
    $("#meta_title").val(response.meta_title);
    $("#meta_keyword").val(response.meta_keyword);
    $("#meta_desc").val(response.meta_desc);
    $("#sort_order").val(response.sort_order);
    $('#position').empty();
    var select;
    // if(response.position){
    //   select = 'selected';
    // }
    $('#position').append('<option '+(response.position == 'left'?'selected':'')+' value="left">Left</option><option '+(response.position == 'right'?'selected':'')+' value="right">Right</option>');
  }
});
</script>

<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
    $('#col_des').ckeditor({      
      filebrowserImageBrowseUrl: route_prefix + '?type=Images',
      filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
      filebrowserBrowseUrl: route_prefix + '?type=Files',
      filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
  </script>

<script>

function readFeatureURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#FeatImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readBannerURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#BannImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection