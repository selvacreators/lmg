@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/insert/video','class'=>'form-horizontal video-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/videos" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Add New Video</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="language" class="form-control" id="language">
                                            @foreach($language as $lng)
                                            <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtvideoname" class="control-label col-sm-3">Video Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtvideoname" name="txtvideoname">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtvideo" class="control-label col-sm-3">Upload Video:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                      <input type="file" name="txtvideo" id="txtvideo" class="form-control" accept=".mp4">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtshowpage" class="control-label col-sm-3">Show Home Page:</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="txtshowpage" id="txtshowpage" value="1" checked> Show
                                        <input type="radio" name="txtshowpage" id="txtshowpage" value="2"> Hide
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="videostatus" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="videostatus" id="videostatus" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Enable</option>
                                            <option value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
    $(".video-form").validate({
        rules: {
            language: "required",
            txtvideoname : "required",
            txtvideo : "required",
            txtshowpage : "required",
            videostatus : "required"
        },
        messages: {
            language: "Please select language",
            txtvideoname: "Please enter video name",
            txtvideo: "Please upload video",
            txtshowpage : "Please choose show option",
            videostatus: "Please select status"
        }
    });
});
</script>
@endsection