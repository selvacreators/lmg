@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/video','class'=>'form-horizontal video-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="video_id" value="{{$video[0]->video_id}}">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/videos" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Video</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="language" class="form-control" id="language">
                                        @foreach($language as $lng)
                                        @if($lng->lang_code == $video[0]->lang_code)
                                          <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                        @else
                                          <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                        @endif
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtvideoname" class="control-label col-sm-3">Video Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtvideoname" name="txtvideoname" value="{{$video[0]->video_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtvideo" class="control-label col-sm-3">Upload Video:<span class="note">*</span></label>
                                    <div class="col-sm-4">
                                      <input type="file" name="txtvideo" id="txtvideo" class="form-control" accept=".mp4">
                                      <input type="hidden" name="insertvideo" id="insertvideo" value="{{$video[0]->upload_video}}">
                                    </div>
                                    <div class="col-sm-4">
                                      <video width="300" height="200" controls name="uploadvideo" id="uploadvideo"><source src="{{ Config::get('app.head_url') }}slidervideo/{{$video[0]->upload_video}}" type="video/mp4"></video>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtshowpage" class="control-label col-sm-3">Show Home Page:</label>
                                    <div class="col-sm-9">
                                        <input type="radio" name="txtshowpage" id="txtshowpage" value="1" <?php if($video[0]->home_show_status == 1) { echo "checked"; } ?>> Show
                                        <input type="radio" name="txtshowpage" id="txtshowpage" value="2" <?php if($video[0]->home_show_status == 2) { echo "checked"; } ?>> Hide
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="videostatus" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="videostatus" id="videostatus" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?php if($video[0]->video_status == 1) { echo "selected= selected"; } ?>>Enable</option>
                                            <option value="2" <?php if($video[0]->video_status == 2) { echo "selected= selected"; } ?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
    $(".video-form").validate({
        rules: {
            language: "required",
            txtvideoname : "required",
            txtshowpage : "required",
            videostatus : "required"
        },
        messages: {
            language: "Please select language",
            txtvideoname: "Please enter video name",
            txtshowpage : "Please choose show option",
            videostatus: "Please select status"
        }
    });
});
</script>
@endsection