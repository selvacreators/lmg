@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/edit_banner','class'=>'form-horizontal slider-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                         <input type="hidden" value="{{$promo_banner->promo_banner_id}}" class="form-control" id="promo_id" name="promo_id">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/promo_banner" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Promo Banner</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                            <select name="language" class="form-control" id="language">
                                @foreach($language as $lng)
                                @if($promo_banner->lang_code == $lng->lang_code)
                                <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                @else
                                <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                @endif
                                @endforeach
                            </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtslidername" class="control-label col-sm-3">Promo Banner Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtslidername" name="txtslidername" value="{{$promo_banner->banner_title}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtslimage" class="control-label col-sm-3">Promo Banner Image:<span class="note">*</span></label>
                                    <div class="col-sm-3">
                                        <input type="file" name="txtslimage" id="txtslimage" class="form-control" accept="image/*" data-type="image" onchange="readSliderURL(this);">
                                        <input type="text" hidden class="form-control" id="insert_images" name="insert_images" value="{{$promo_banner->banner_image}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="{{Config::get('app.head_url')}}promo-banner/{{$promo_banner->banner_image}}" id="StrImg" width="100" height="100">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="banner_btn_txt" class="control-label col-sm-3">Promo Banner Button Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="banner_btn_txt" name="banner_btn_txt" value="{{$promo_banner->banner_btn_txt}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="banner_btn_txt" class="control-label col-sm-3">Promo Banner to Show Page:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="pageShow" id="pageShow">
                                            <option value="">Select</option>
                                            <option value="Category_list" <?php if($promo_banner->banner_show_page == "Category_list"){ echo "selected";}else{ echo "";}?>>Category List Page</option>
                                            <option value="Blog_list" <?php if($promo_banner->banner_show_page == "Blog_list"){ echo "selected";}else{ echo "";}?>>Blog Post Page</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="banner_btn_url" class="control-label col-sm-3">Promo Banner Button Url:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="banner_btn_url" name="banner_btn_url" value="{{$promo_banner->banner_btn_url}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtsliderdesc" class="control-label col-sm-3">Promo Banner Description:</label>
                                    <div class="col-sm-9">
                                        <textarea name="desc" id="desc" rows="6" class="form-control"> {{$promo_banner->banner_desc}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sliderstatus" class="control-label col-sm-3">Promo Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="sliderstatus" id="sliderstatus" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?php if($promo_banner->banner_status == 1){ echo "selected";}else{ echo "";}?>>Enable</option>
                                            <option value="2" <?php if($promo_banner->banner_status == 2){ echo "selected";}else{ echo "";}?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
    $(".slider-form").validate({
        rules: {
            language: "required",
            txtslidername : "required",
            //txtslimage : "required",
            sliderstatus : "required"
        },
        messages: {
            language: "Please select language",
            txtslidername: "Please enter slider name",
           // txtslimage: "Please choose slider image",
            sliderstatus: "Please select status"
        }
    });
    $("#txtslimage").change(function() {
        if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif)$/) ) {
            if(this.files[0].size>2000000) {
              alert('File size is larger than 2MB!');
              $('#store_logo').val();
            }
        } else alert('This is not an image file!');
    });
});
function readSliderURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#StrImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
        $('#insert_images').val('');
    }
}
</script>
@endsection