@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            {!! Form::open(array('url' => '/Admin/add_exit_intent','class'=>'form-horizontal','method'=>'post','enctype'=>'multipart/form-data')) !!}
            <input name="_token" type="hidden" value="{{csrf_token()}}"> 
            <div class="card">
                
                <div class="card-header">
                    <a href="/Admin/exit_intent" class="btn btn-primary btn-sm pull-right">Back</a>
                    <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                    <h3 class="card-title">Add Exit Intent Popup</h3>
                </div>
                <div class="card-body">

                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                        <div class="col-sm-9">
                            <select name="lang_code" id="lang_code" class="form-control">
                                @foreach($language as $lang)
                                <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                 
                    
               
                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="pouptitle">Name:</label>
                        <div class="col-sm-9">
                            <input type="text" name=popup_title id="popup_title" class="form-control" required="required">
                        </div>
                    </div>
               
                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="pouptitle">Background Image:</label>
                        <div class="col-sm-9">
                            <input type="file" name=intent_image id="intent_image" class="form-control" >
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="coupon_code">Coupon Code:</label>
                        <div class="col-sm-9">
                            <input type="text" name=coupon_code id="coupon_code" class="form-control">
                        </div>
                    </div>
               

                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="button_text">button Text:</label>
                        <div class="col-sm-9">
                            <input type="text" name=button_text id="button_text" class="form-control">
                        </div>
                    </div>
               

                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="status">Disable Form:</label>
                        <div class="col-sm-9">
                            <select name="disable_form" id="disable_form" class="form-control">
                                <option value="2">Disable</option>
                                <option value="1">Enable</option>
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="status">Social Login:</label>
                        <div class="col-sm-9">
                            <select name="social_login" id="social_login" class="form-control">
                                <option value="2">Disable</option>
                                <option value="1">Enable</option>
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" id="textareaeditor">
                        <label class="control-label col-sm-3" for="editor1">Content:</label>
                        <div class="col-sm-9">
                            <textarea rows="10" cols="50" name="popup_content" id="popup_content" class="form-control"></textarea>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="status">Status:</label>
                        <div class="col-sm-9">
                            <select name="status" id="status" class="form-control" required="required">
                                <option value="" >Select</option>
                                <option value="1">Enable</option>
                                <option value="2">Disable</option>
                            </select>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="card-footer">
                    <div class="buttons text-center">
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 align="center">{{ Session::get('message') }}</h4>
                <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
                {{ Session::forget('message') }}
            </div>
        </div>
    </div>
</div>
@endif
@stop