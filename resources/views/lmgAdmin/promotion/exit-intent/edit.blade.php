@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            {!! Form::open(array('url' => '/Admin/edit_exit_intent','class'=>'form-horizontal','method'=>'post','enctype'=>'multipart/form-data')) !!}
            <input name="_token" type="hidden" value="{{csrf_token()}}"> 
            <input name="exit_intent_id" type="hidden" value="{{$intent[0]->exit_intent_id}}"> 
            <div class="card">
                <div class="card-header">
                    <a href="/Admin/exit_intent" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                    <h3 class="card-title">Edit Exit Intent Popup</h3>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                        <div class="col-sm-9">
                            <select name="lang_code" class="form-control" id="lang_code">
                                @foreach($language as $lng)
                                    @if($lng->lang_code == $intent[0]->lang_code)
                                        <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                    @else
                                        <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
               
                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="pouptitle">Name:</label>
                        <div class="col-sm-9">
                            <input type="text" name=popup_title id="popup_title" class="form-control" required="required" value="{{$intent[0]->popup_title}}">
                        </div>
                    </div>
               
                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="pouptitle">Background Image:</label>
                        <div class="col-sm-9">
                            <input type="file" name="intent_image" id="intent_image" class="form-control" >
                            <img src="{{ Config::get('app.head_url')}}promotion/{{$intent[0]->intent_image}}" alt="" id="showimag" width="100">
                            <input type="hidden" name="Upload_images" id="Upload_images" value="{{$intent[0]->intent_image}}" >
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="coupon_code">Coupon Code:</label>
                        <div class="col-sm-9">
                            <input type="text" name="coupon_code" value="{{$intent[0]->coupon_code}}" id="coupon_code" class="form-control">
                        </div>
                    </div>
               

                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="button_text">button Text:</label>
                        <div class="col-sm-9">
                            <input type="text" name="button_text" value="{{$intent[0]->button_text}}" id="button_text" class="form-control">
                        </div>
                    </div>
               

                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="status">Disable Form:</label>
                        <div class="col-sm-9">
                            <select name="disable_form" id="disable_form" class="form-control">
                                <option value="2" <?php if($intent[0]->disable_form == 2) { echo "selected";} else { echo "";} ?>>Disable</option>
                                <option value="1" <?php if($intent[0]->disable_form == 1) { echo "selected";} else { echo "";} ?>>Enable</option>
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="status">Social Login:</label>
                        <div class="col-sm-9">
                            <select name="social_login" id="social_login" class="form-control">
                                <option value="2" <?php if($intent[0]->social_login == 2) { echo "selected";} else { echo "";} ?>>Disable</option>                               
                                <option value="1" <?php if($intent[0]->social_login == 1) { echo "selected";} else { echo "";} ?>>Enable</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" id="textareaeditor">
                        <label class="control-label col-sm-3" for="editor1">Content:</label>
                        <div class="col-sm-9">
                            <textarea rows="10" cols="50" name="popup_content" id="popup_content" class="form-control">{{$intent[0]->popup_content}}</textarea>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="control-label col-sm-3" for="status">Status:</label>
                        <div class="col-sm-9">
                            <select name="status" id="status" class="form-control" required="required">
                                <option value="" >Select</option>
                                <option value="1" <?php if($intent[0]->status == 1) { echo "selected";} else { echo "";} ?>>Enable</option>
                                <option value="2" <?php if($intent[0]->status == 2) { echo "selected";} else { echo "";} ?>>Disable</option>
                                
                            </select>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="card-footer">
                    <div class="buttons text-center">
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 align="center">{{ Session::get('message') }}</h4>
                <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
                {{ Session::forget('message') }}
            </div>
        </div>
    </div>
</div>
@endif
@stop

@section('admin-script')
<script type="text/javascript">
  $(document).ready(function() {
    
   // alert('fdsf');

   $('#intent_image').click(function(event) {
      
      //  alert('Tet');
      $('#Upload_images').val('');
      $('#showimag').hide();
  });
});
</script>
@endsection