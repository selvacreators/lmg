@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/edit_popup','class'=>'form-horizontal popup-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                         <input type="text" value="{{$popup[0]->popup_id}}" class="form-control" id="popup_id" name="popup_id">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/newsletter_popup" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Promo Banner</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                            <select name="language" class="form-control" id="language">
                                @foreach($language as $lng)
                                @if($popup[0]->lang_code == $lng->lang_code)
                                <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                @else
                                <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                @endif
                                @endforeach
                            </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="popup_title" class="control-label col-sm-3">Popup Title:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="popup_title" name="popup_title" value="{{$popup[0]->popup_title}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="popup_image" class="control-label col-sm-3">Popup Banner Image:<span class="note">*</span></label>
                                    <div class="col-sm-3">
                                        <input type="file" name="popup_image" id="popup_image" class="form-control" accept="image/*" data-type="image" onchange="readSliderURL(this);">
                                        <input type="text" hidden class="form-control" id="insert_images" name="insert_images" value="{{$popup[0]->popup_image}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="{{Config::get('app.head_url')}}promotion/{{$popup[0]->popup_image}}" id="StrImg" width="100" height="100">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="popup_btn_txt" class="control-label col-sm-3">Popup Button Text:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="popup_btn_txt" name="popup_btn_txt" value="{{$popup[0]->popup_btn_txt}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="popup_btn_url" class="control-label col-sm-3">Popup Button Url:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="popup_btn_url" name="popup_btn_url" value="{{$popup[0]->popup_btn_url}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="popup_desc" class="control-label col-sm-3">Popup Description:</label>
                                    <div class="col-sm-9">
                                        <textarea name="popup_desc" id="popup_desc" rows="6" class="form-control"> {{$popup[0]->popup_desc}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="popup_status" class="control-label col-sm-3">Popup Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="popup_status" id="popup_status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?php if($popup[0]->popup_status == 1){ echo "selected";}else{ echo "";}?>>Enable</option>
                                            <option value="2" <?php if($popup[0]->popup_status == 2){ echo "selected";}else{ echo "";}?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
    $(".popup-form").validate({
        rules: {
            language: "required",
            popup_title : "required",
            popup_status : "required"
        },
        messages: {
            language: "Please select language",
            popup_title: "Please Enter Popup Title",
            popup_status: "Please select status"
        }
    });
    $("#txtslimage").change(function() {
        if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif)$/) ) {
            if(this.files[0].size>2000000) {
              alert('File size is larger than 2MB!');
              $('#store_logo').val();
            }
        } else alert('This is not an image file!');
    });
});
function readSliderURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#StrImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
        $('#insert_images').val('');
    }
}
</script>
@endsection