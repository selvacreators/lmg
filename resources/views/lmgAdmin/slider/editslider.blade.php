@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/editslidespage','class'=>'form-horizontal slider-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="sliderid" value="{{$sliders[0]->slider_id}}">
                        <div class="card">
                            <div class="card-header">
                                <a href="/Admin/slider" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Slider</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="language" class="form-control" id="language">
                                        @foreach($language as $lng)
                                        @if($lng->lang_code == $sliders[0]->lang_code)
                                          <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                        @else
                                          <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                        @endif
                                      @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtslidername" class="control-label col-sm-3">Slider Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtslidername" name="txtslidername" value="{{$sliders[0]->slider_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtslimage" class="control-label col-sm-3">Slider Image:<span class="note">*</span></label>
                                    <div class="col-sm-3">
                                        <input type="file" name="txtslimage" id="txtslimage" class="form-control" accept="image/*" data-type="image" onchange="readSliderURL(this);">
                                        <input type="hidden" name="insertimage" id="insertimage" value="{{$sliders[0]->slider_image}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="{{Config::get('app.head_url')}}slider/{{$sliders[0]->slider_image}}" id="StrImg" width="100" height="100">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtsliderdesc" class="control-label col-sm-3">Slider Description:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="txtsliderdesc" id="txtsliderdesc" class="form-control" value="{{$sliders[0]->slider_description}}"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sliderstatus" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="sliderstatus" id="sliderstatus" class="form-control">
                                          <option value="">Select</option>
                                          <option value="1" <?php if($sliders[0]->slider_status =='1'){ echo "selected";} ?>>Enable</option>
                                          <option value="2" <?php if($sliders[0]->slider_status =='2'){ echo "selected";} ?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
    $(".slider-form").validate({
        rules: {
            language: "required",
            txtslidername : "required",
            sliderstatus : "required"
        },
        messages: {
            language: "Please select language",
            txtslidername: "Please enter slider name",
            sliderstatus: "Please select status"
        }
    });
    $("#txtslimage").change(function() {
        if (this.files && this.files[0] && this.files[0].name.match(/\.(jpg|jpeg|png|gif)$/) ) {
            if(this.files[0].size>2000000) {
              alert('File size is larger than 2MB!');
              $('#store_logo').val();
            }
        } else alert('This is not an image file!');
    });
});
function readSliderURL(input)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            $('#StrImg')
            .attr('src', e.target.result)
            .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection