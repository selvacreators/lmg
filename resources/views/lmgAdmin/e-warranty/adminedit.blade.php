@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/ewarranty','class'=>'form-horizontal ewarranty-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="ewarrantyId" value="{{$warranty->ewarranty_id}}">
                        <div class="card">
                            <div class="card-header">
                              <a href="/Admin/e-warranty" class="btn btn-primary btn-sm pull-right">Back</a>
                              <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                              <h3 class="card-title">Edit E-Warranty Details</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="language" class="form-control" id="language">
                                            @foreach($language as $lng)
                                                @if($lng->lang_code == $warranty->lang_code)
                                                    <option value="{{$lng->lang_code}}" selected="selected">{{$lng->lang_name}}</option>
                                                @else
                                                    <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="salutation" class="control-label col-sm-3">Salutation:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="salutation" class="form-control" id="salutation">
                                            <option value="">Select</option>
                                            <option @if($warranty->salutation == 'Prof') selected="selected" @endif>Prof</option>
                                            <option @if($warranty->salutation == 'Dr') selected="selected" @endif>Dr</option>
                                            <option @if($warranty->salutation == 'Mr') selected="selected" @endif>Mr</option>
                                            <option @if($warranty->salutation == 'Mrs') selected="selected" @endif>Mrs</option>
                                            <option @if($warranty->salutation == 'Mdm') selected="selected" @endif>Mdm</option>
                                            <option @if($warranty->salutation == 'Miss') selected="selected" @endif>Miss</option>
                                            <option @if($warranty->salutation == 'Ms') selected="selected" @endif>Ms</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="control-label col-sm-3">Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$warranty->name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="control-label col-sm-3">Email:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="email" name="email" value="{{$warranty->email}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="control-label col-sm-3">Phone:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{$warranty->phone}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-sm-3" for="dob">Date of Birth:</label>
                                  <div class="col-sm-9">
                                    <input type="text" name="dob" value="{{$warranty->dob}}" id="dob" class="form-control date_available">
                                  </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="address">Address:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="address" value="{{$warranty->address}}" id="address" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="postal_code">Postal Code:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="postal_code" value="{{$warranty->postal_code}}" id="postal_code" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="purchase_invoice_num">Purchased Invoice No:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="purchase_invoice_num" value="{{$warranty->purchase_invoice_num}}" id="purchase_invoice_num" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="place_of_purchase">Place of Purchase:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="place_of_purchase" value="{{$warranty->place_of_purchase}}" id="place_of_purchase" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="date_of_purchase">Date of Purchase :</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="date_of_purchase" value="{{$warranty->date_of_purchase}}" id="date_of_purchase" class="form-control date_available">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="date_of_delivery">Date of Delivery :</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="date_of_delivery" value="{{$warranty->date_of_delivery}}" id="date_of_delivery" class="form-control date_available">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="model_name">Model Name :</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="model_name" value="{{$warranty->model_name}}" id="model_name" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="serial_number">Serial Number :</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="serial_number" value="{{$warranty->serial_number}}" id="serial_number" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="product_type">Product Type :</label>
                                    <div class="col-sm-9">
                                        <select name="product_type" id="product_type" class="form-control">
                                            <option value="">Select</option>
                                            <option value="">Product Type</option>
                                            @if($productType)
                                            @foreach($productType as $row)
                                            <option <?=(($row->id.'$$'.$row->ptype_name == $warranty->product_type)?'selected':'');?> value="{{$row->id.'$$'.$row->ptype_name}}">{{$row->ptype_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <?php $esize = explode('$$',$warranty->product_size);?>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="product_size">Product Size :</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="product_size" value="{{$esize[1]}}" id="product_size" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="purchase_experience">Purchased Experience :</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="purchase_experience" value="{{$warranty->purchase_experience}}" id="purchase_experience" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="purchase_delivery">Purchase Delivery :</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="purchase_delivery" value="{{$warranty->purchase_delivery}}" id="purchase_delivery" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="message">Message :</label>
                                    <div class="col-sm-9">
                                      
                                      <textarea cols="10" rows="5" name="message" id="message" class="form-control" >{{$warranty->message}}</textarea>
                                       
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="3" <?=(($warranty->status=='1')?'selected':'');?>>Pending</option>
                                            <option value="1" <?=(($warranty->status=='2')?'selected':'');?>>Done</option>
                                            <option value="2" <?=(($warranty->status=='3')?'selected':'');?>>Reject</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                       </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".ewarranty-form").validate({
    rules: {
        language: "required",
        salutation : "required",
        name : "required",
        email: {
            required: true,
            email: true
        },
        phone:{
            minlength:9,
            maxlength:10,
            number: true
        },
        status : "required"
    },
    messages: {
      language: "Please select language",
      salutation: "Please choose salutation",
      name: "Please enter name",
      email: "Please enter valid email address",
      phone: "Please enter valid phone number",
      status: "Please select status"
    }
  });
});
</script>
@endsection