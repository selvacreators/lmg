<div class="page-bottom">
    <section class="newsletter-signup pt40">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h4 class="mb20">GET USEFUL TIPS AND NEW COLLECTION UPDATES</h4>
                    {{-- @if(Session::has('subscribe_msg'))
                        <div class="alert alert-success">
                            {{Session::get('subscribe_msg')}} 
                        </div>
                    @endif --}}

                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif


                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    {{-- <form action="{{ URL('subscribe')}}" class="newsletter form-inline" method="post">
                          <input name="_token" type="hidden" value="{{csrf_token()}}">
                     --}}

                     {!! Form::open(array('route' => 'subscribe', 'class' => 'newsletter form-inline')) !!}
                       
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="First Name" name="fname" id="fname" required="required">
                          <input type="hidden" name="type" value="{{ Request::path() }}">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="Last Name" name="lname" id="lname" required="required">
                        </div>
                        <div class="form-group">                                      
                          <input type="email" class="form-control" placeholder="Email" name="email" id="email" required="required">
                        </div>
                        <button type="submit" name="submit" value="Newsletter Subscribe" class="btn btn-primary mb0">Subscribe</button>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
    <section class="call-to-action pt40 pb40">
        <div class="container">                      
            <div class="row">
                <div class="content-heading">
                    <h4 class="title">SPEAK WITH A SLEEP CONSULTANT</h4>
                </div>
                <div class="width-auto-margin">
                    <div class="col-md-offset-2 col-lg-3 col-md-3 col-sm-4 col-xs-12 width-cal">
                        <div class="row">
                            <div class="panel-block-row col-md-12 col-xs-12">
                                <div class="icon-service">
                                    <div class="icon"><a title="Lmg Stores" href="{{Config::get('app.head_url')}}"><i class="pe-7s-map-marker"> </i></a></div>
                                    <div class="text">
                                        <?php $locale = url(Session::get('locale')); ?>
                                        <h6 class="margin-bottom5"><a title="LMG Store Locator" target="_blank" href="{{ URL($locale.'/')}}stores/">Store Locator</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(isset($csetting))
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 margin-bottom30 width-cal">
                        <div class="row">
                            <div class="panel-block-row col-md-12 col-xs-12">
                                <div class="icon-service">
                                    <div class="icon"><a title="Chat" href="{{$csetting->chat}}"><i class="pe-7s-chat"> </i></a></div>
                                    <div class="text">
                                        <h6 class="margin-bottom5"><a title="Chat" href="{{$csetting->chat}}">Chat</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(isset($csetting))
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 margin-bottom30 width-cal">
                        <div class="row">
                            <div class="panel-block-row col-md-12 col-xs-12">
                                <div class="icon-service">
                                    <div class="icon"><a title="Lmg Stores" href="tel:{{str_replace(' ','',$csetting->phone)}}"><i class="pe-7s-phone"> </i></a></div>
                                    <div class="text">
                                        <h6 class="margin-bottom5"><a title="Lmg Stores" href="tel:{{str_replace(' ','',$csetting->phone)}}">{{$csetting->phone}}</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>