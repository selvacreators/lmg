<div class="form-search">
	<div class="container">
		<div class="form-header text-center">
			<h4 class="title">Let us help you find your perfect mattress  
</h4>
		<button class="search-close" onclick="showHideFormSearch()"><i class="pe-7s-close"></i></button>
		</div>
		
		<form class="form minisearch" id="search_mini_form" action="" method="get">
		
		<input id="search"
			   type="text"
			   name="q"
			   value=""
			   class="searchbox input-text form-control"
			   maxlength="128"
			   placeholder="Search for..."
			   role="combobox"
			   aria-expanded="true"
			   aria-haspopup="false"
			   aria-autocomplete="both"
			   autocomplete="off"/>
		<div class="searchsubmit">
			<input type="submit" id="searchsubmit" value="">
		</div>
		<div class="search-inner">
			<div id="search_autocomplete" class="search-autocomplete" style="display: none;"></div>
		</div>
		
		</form>
	
	</div>
</div>
<script type="text/javascript">
    function showHideFormSearch(){
        (function($) {
            if($('.search-form').hasClass('not-full')){
                $('html').toggleClass('search-not-full');
            }
            $('html').toggleClass('search-open');
            setTimeout(focusSearchField, 500);
        })(jQuery);
    }
    function focusSearchField(){
        (function($) {
            $('#search_mini_form input#search').focus();
        })(jQuery);
    }
        
</script> 