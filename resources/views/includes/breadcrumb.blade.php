<div class="breadcrumbs">
  <div class="container-fluid">
    <ul class="items">
      <li class="item  cms-home">
        <a href="{{URL('/')}}" title="Go to Home Page">
        Home </a>
      </li>
      @if(isset($breadcrumbs) && sizeof($breadcrumbs))
        @foreach($breadcrumbs as $bc)
          @if(isset($bc['last']))
            <li class="item cms_page"><strong>{{$bc['name']}}</strong></li>
          @else
            <li class="item cms_page"><a href="{{$bc['url'].'/'}}">{{$bc['name']}}</a></li>
          @endif
        @endforeach
      @else
        <li class="item cms_page"><strong>{{ucwords(str_replace('-', ' ', $currentPath))}}</strong></li>
      @endif
    </ul>
  </div>
</div>