
    <script src="{{ Config::get('app.head_url') }}frontend/js/jquery.js"></script>    
    <script src="{{ Config::get('app.head_url') }}frontend/js/jquery.cookie.js"></script>
    <script src="{{ Config::get('app.head_url') }}frontend/js/init.js"></script>    
    <script src="{{ Config::get('app.head_url') }}assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <script src="{{ Config::get('app.head_url') }}frontend/js/scripts.js"></script> 
    <!-- <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b192f848e56ee0011c80411&product=inline-share-buttons' async='async'></script> -->
    
    <!--<script type="text/javascript" src="{{ Config::get('app.head_url') }}assets/js/jquery.validate.js"></script>
    <script type="text/javascript" src="{{ Config::get('app.head_url') }}frontend/js/jquery-validate.bootstrap-tooltip.js" /></script> -->
   
    <script>
       
      $(document).ready(function () {
        $('.sidebar-btn').on('click', function () {
            $('#mySidenav').addClass('open');
        });
        $('.closebtn').on('click', function () {
            $('#mySidenav').removeClass('open');
        });
        
        $('.header_promo .close').on('click', function() {
               $('.header_promo').addClass('promo');
        });

        var winWidth = $(window).width();        
        var contentHeight = $(".content-wrapper").height();
        var pageBottom = $(".page-bottom").height();
        var totHeight = contentHeight + pageBottom; 
        
        if(winWidth > 1200){
            if ( totHeight <= $(window).height()){
                $(".footer").addClass("fixed");
                
            }
            else if(totHeight > $(window).height()){
                $(".footer").removeClass("fixed");
            }
        }
      });
      
    </script>
    <script type="text/javascript">
  $('#telephone,#postal').keyup(function(e){
    if (/\D/g.test(this.value))
    {
      // Filter non-digits from input value.
      this.value = this.value.replace(/\D/g, '');
    }
  });
    document.addEventListener("DOMContentLoaded", function() {
        var elements = document.getElementsByName("name");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your name");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("fname");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your first name");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("lname");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your last name");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("email");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your email");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("content");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your message");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("date_of_birth");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please choose your date of birth");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("address");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your address");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("postal");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your postal");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("purchase_invoice_number");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your purchase invoice number");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("place_of_purchase");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please select place of purchase");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("date_of_purchase");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please choose date of purchase");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("date_of_delivery");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please choose date of delivery");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("model_name");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter model name");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("serial_number");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter serial number");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("product_type");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please select product type");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("product_size");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please select product size");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        // var elements = document.getElementsByName("purchase_experience");
        // for (var i = 0; i < elements.length; i++) {
        //     elements[i].oninvalid = function(e) {
        //         e.target.setCustomValidity("");
        //         if (!e.target.validity.valid) {
        //             e.target.setCustomValidity("Please rate purchase experience");
        //         }
        //     };
        //     elements[i].oninput = function(e) {
        //         e.target.setCustomValidity("");
        //     };
        // }

        // var elements = document.getElementsByName("purchase_delivery");
        // for (var i = 0; i < elements.length; i++) {
        //     elements[i].oninvalid = function(e) {
        //         e.target.setCustomValidity("");
        //         if (!e.target.validity.valid) {
        //             e.target.setCustomValidity("Please rate delivery process");
        //         }
        //     };
        //     elements[i].oninput = function(e) {
        //         e.target.setCustomValidity("");
        //     };
        // }

        var elements = document.getElementsByName("message");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter your message");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("terms");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please choose agree");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("g-recaptcha-response");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please fill reCAPTCHA");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("review_title");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter title");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("summary");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please enter summary");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }

        var elements = document.getElementsByName("rating");
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Please choose rating");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    });


    $(document).ready(function(){
        $(".drop-lft").click(function(){
            $(".dropdown-menu").remove();
        });
    });
    try {
        if (!window.localStorage || !window.sessionStorage) {
            throw new Error();
        }

        localStorage.setItem('storage_test', 1);
        localStorage.removeItem('storage_test');
    } catch(e) {
        (function () {
            var Storage = function (type) {
                var data;

                function createCookie(name, value, days) {
                    var date, expires;

                    if (days) {
                        date = new Date();
                        date.setTime(date.getTime()+(days * 24 * 60 * 60 * 1000));
                        expires = '; expires=' + date.toGMTString();
                    } else {
                        expires = '';
                    }
                    document.cookie = name + '=' + value+expires+'; path=/';
                }

                function readCookie(name) {
                    var nameEQ = name + '=',
                        ca = document.cookie.split(';'),
                        i = 0,
                        c;

                    for (i=0; i < ca.length; i++) {
                        c = ca[i];

                        while (c.charAt(0) === ' ') {
                            c = c.substring(1,c.length);
                        }

                        if (c.indexOf(nameEQ) === 0) {
                            return c.substring(nameEQ.length, c.length);
                        }
                    }

                    return null;
                }

                function setData(data) {
                    data = encodeURIComponent(JSON.stringify(data));
                    createCookie(type === 'session' ? getSessionName() : 'localStorage', data, 365);
                }

                function clearData() {
                    createCookie(type === 'session' ? getSessionName() : 'localStorage', '', 365);
                }

                function getData() {
                    var data = type === 'session' ? readCookie(getSessionName()) : readCookie('localStorage');

                    return data ? JSON.parse(decodeURIComponent(data)) : {};
                }

                function getSessionName() {
                    if (!window.name) {
                        window.name = new Date().getTime();
                    }

                    return 'sessionStorage' + window.name;
                }

                data = getData();

                return {
                    length: 0,
                    clear: function () {
                        data = {};
                        this.length = 0;
                        clearData();
                    },

                    getItem: function (key) {
                        return data[key] === undefined ? null : data[key];
                    },

                    key: function (i) {
                        var ctr = 0,
                            k;

                        for (k in data) {
                            if (ctr.toString() === i.toString()) {
                                return k;
                            } else {
                                ctr++
                            }
                        }

                        return null;
                    },

                    removeItem: function (key) {
                        delete data[key];
                        this.length--;
                        setData(data);
                    },

                    setItem: function (key, value) {
                        data[key] = value.toString();
                        this.length++;
                        setData(data);
                    }
                };
            };

            window.localStorage.__proto__ = window.localStorage = new Storage('local');
            window.sessionStorage.__proto__ = window.sessionStorage = new Storage('session');
        })();
    }
  
    

</script>
<!--Start of Tawk.to Script-->
@if(isset($chat_details) && sizeof($chat_details))
    @foreach($chat_details as $chat)
        @if(strtoupper(Session::get('locale')) == strtoupper($chat->lang_code))
        <?php echo html_entity_decode($chat->chat_script);?>
        @endif
    @endforeach
@endif

<!-- promo popup wraper -->
@if(isset($popup) && sizeof($popup))
@foreach($popup as $data)
<div class="alert alert-success" id="msg" style="display: none;">
    Thanks for Subscribing our LMG world Gallery Mail List.<br>
    <!-- <span>We have emailed you a copy of your personalised LmgWorld mattress recommendation.</span> -->
</div>
<div id="newsletter-popup" class="zoom-anim-dialog mfp-hide newsletter_popup">
    <div class="block-popup-subscribe">
        <div class="row">            
            <div class="col-xs-12 col-md-6 eq_height align-self-center">
                <div class="promo-text-container text-center">
                    <div class="promo-text-content">
                      <div class="pop-sletter-title">
                          <h3>{{$data->popup_title}}</h3>
                          <!-- <p>Get exclusive discounts and coupons</p> -->
                      </div>
                      <div class="newsletter-popup-content">
                        <p>{{$data->popup_desc}}</p>
                      </div>
                    </div>
                    
                    <div class="newsletter-popup-form">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif


                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif
                   {!! Form::open(array('route' => 'subscribe', 'class' => 'form newsLetter-v2', 'name' => 'newsletterPopup-form', 'id' => 'newsletter-popup-validate-detail')) !!}
                    
                        <div class="form-group">
                                <input type="email" name="email" id="newsletter-popup-email" placeholder="Enter your Email...." class="form-control">
                            
                        </div>
                        <div class="form-group">
                            @if(!empty($data->popup_btn_txt))
                            <button type="submit" name="submit" id="send_mail" value="Newsletter Subscribe" title="{{$data->popup_btn_txt}}" class="action btn btn-primary btn-round pl15 pr15"><i class="fa fa-paper-plane"></i><span>{{$data->popup_btn_txt}}</span></button>
                            @endif

                        </div>
                        <div class="checkbox text-center">
                          <label for="dont_show">
                            <input id="dont_show" onClick="dontShowPopup();" type="checkbox"> Don't show this popup again </label>
                        </div>
                     {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @if(!empty($data->popup_image))
            <div class="col-xs-12 col-lg-6 eq_height xs-padding-0">
                <div class="promo-image-container pt60">
                    <img src="{{ URL() }}/public/promotion/{{$data->popup_image}}" alt="">
                </div>
            </div>
            @endif
        </div>
    </div>
</div>  <!-- END promo popup wraper -->
@endforeach

<script type="text/javascript">
function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    

    function dontShowPopup(){
        
       
          if($('#dont_show').prop('checked')) {
            var d = new Date();
            d.setTime(d.getTime() + (24*60*60*1000*365));
            var expires = "expires="+d.toUTCString();
            document.cookie = 'newsletterpopup' + "=" + 'nevershow' + "; " + expires;
          } else {
            document.cookie = 'newsletterpopup' + "= ''; -1";
          }
       
    }
    
$(document).ready(function() {

  
  $(function(modal) {
    $(window).load(function () {
        if ($('#newsletter-popup').length) {
          if (getCookie('newsletterpopup')!='nevershow') {
            $.magnificPopup.open({
              items: {
                src: '#newsletter-popup' 
              },
              focus: '#xs-promo-email',
              type: 'inline',
              fixedContentPos: false,
              fixedBgPos: true,
              overflowY: 'auto',
              closeBtnInside: true,
              preloader: true,
              midClick: true,
              removalDelay: 300,
              callbacks:{
                beforeOpen: function() {
                  this.st.mainClass = "my-mfp-zoom-in modal_newsletter_popup";
                }
              }
            }); 
          }
        }
      });
  })
});
</script>
@endif
@if(isset($Exist_Intent) && sizeof($Exist_Intent)) 
<!-- use ouibounce -->
<script src="{{ Config::get('app.head_url') }}frontend/js/ouibounce.js"></script>
@foreach($Exist_Intent as $val)
<!-- Ouibounce Modal -->
    <div id="ouibounce-modal">
        <!---->
      <div class="underlay" ></div>
      <div class="modal">
        <div class="modal-title">
           <h3>{{$val->popup_title}}</h3>
          <button type="button" class="close_popup" data-dismiss="modal"></button>
        </div>
        
        <div class="modal-body" style="background-image: url('{{ Config::get('app.head_url')}}promotion/{{$val->intent_image}}');">
            <div class="content-title">
               <div class="h4 pull-left">Use Coupon Code here:</div>
               <div class="coupon-text pull-left">
                    <span class="h4 coupon-treasure">
                        <span id="coupon_code">
                            @if(!empty($val->coupon_code))
                            {{$val->coupon_code}}
                         @endif
                        </span>
                        <span id="coupon_text">Coupon Code:</span>
                    </span>
                 </div>
            </div>
            <div class="clearfix"></div>
            <div class="coupon_content mt20">
               <div class="promo-text-content">
                    <div class="coupon-desc"> 
                    <?php echo html_entity_decode($val->popup_content); ?>
                    </div>
                </div>            
                <!--v-html--> 
            @if($val->social_login != 2)
              <p class="mt10 mb10">Follow Us to See Your Coupon Code</p>

              <!-- <div class="social-icons sharing-container">
                <div class="sharethis-inline-share-buttons"></div>

                  <div class="sharethis-inline-follow-buttons"></div>

                  <div class=clear-both></div>
              </div> -->
                <div class="sharing-container">
                  <ul id=sharing-accounts>
                    <li class="facebook-like sharing-account" id="lmg-fb-like"> 
                        <div class="wrap-img-ghost"></div>
                        <div class="fb-like"  data-href="https://www.facebook.com/{{$social->facebook_user}}" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"  onclick="showCoupon_code();" id="coupon" >
                        </div>
                       
                        <div style="clear: both"></div>
                        <script>
                            function showCoupon_code(){
                                $('#coupon').remove();
                                $('#coupon_code').show();
                                $('#coupon_text').hide();
                            }
                            $(document).ready(function(){
                               setFBLikeVisibility();
                            });

                            function setFBLikeVisibility(){
                                if(typeof($('div.fb-like').html()) != 'undefined'){

                                    if($('div.fb-like iframe').css('visibility') == 'visible'){
                                        $('div.fb-like iframe').css('visibility', 'inherit');
                                        /* or $('div.fb-like iframe').css('visibility', ''); */
                                    } else {
                                        setTimeout('setFBLikeVisibility()', 100);
                                    }
                                 }
                            }
                        </script>
                        @if(!empty($social->facebook_app_id))
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId={{$social->facebook_app_id}}";
                              fjs.parentNode.insertBefore(js, fjs);
                              }(document, 'script', 'facebook-jssdk'));
                            </script>
                        @else
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = '//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
                            fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        @endif
                    </li>
                    <li class="twitter-follow sharing-account" id="tweet-follow">
                        <div class="wrap-img-ghost"></div> 
                        <span class=sharing-bubble></span> 
                        <a href="//twitter.com/intent/follow?screen_name={{$social->twitter_user}}&original_referer={{URL()}}" class="twitter-follow-button scn-twitter-follow social-sharing" data-show-count="false" data-show-screen-name="false" data-size="large"> <i></i> <span>Follow</span> </a>
                        @if(!empty($social->twitter_user)) 
 
                         <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
                         
                         @endif
                    </li>
                    <li class="google-plus-one sharing-account">
                        <div class="wrap-img-ghost"></div>
                        <div class="g-plusone social-sharing" data-size="tall" data-href="//plus.google.com/{{$social->google_plus_page}}" > </div>
                         @if(!empty($social->google_plus_page))
                         <script src="//apis.google.com/js/platform.js" async defer></script>
                         @endif 
                    </li>
                    <!-- <li class="pinterest-share sharing-account">
                        <div class="wrap-img-ghost"></div> <a >Follow</a> </li> -->
                    <li class="instagram-share sharing-account">
                        <div class="wrap-img-ghost"></div> 
                        <a href="//instagram.com/{{$social->linkedin_id}}" class="instagram-follow social-sharing">Follow</a> 
                         @if(!empty($social->linkedin_id))
                        <!-- <script src="//platform.instagram.com/en_US/embeds.js"></script> -->
                        <script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
                        @endif
                    </li> 
                 

                  </ul>
                </div>
            @endif
            </div>
        </div>
         @if($val->disable_form == 2)
        <div class="modal-footer">
            @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif


                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ $message }}</strong>
                    </div>
                    @endif
           {!! Form::open(array('route' => 'subscribe', 'class' => 'form-inline', 'name' => 'exit_intent_popup')) !!}
          
              <div class="form-group">
                <input type="email" name="email" id="newsletter-popup-email" placeholder="Enter your Email...." class="form-control">
                  
              </div>
              <div class="form-group">
                  <button type="submit" title="{{$val->button_text}}" class="action btn btn-primary btn-round pl15 pr15" value="Newsletter Subscribe"><i class="fa fa-paper-plane"></i><span>{{$val->button_text}}</span></button>
              </div>
            
          </form>
          {!! Form::close() !!}
            <div><span>Subscribe to the LMG World mailing list to receive updates on new arrivals, special offers and our promotions.</span></div>
        </div>
         @endif
      </div>
    </div>

@endforeach
    <!-- <script src="http://cdn.jsdelivr.net/sharrre/1.3.4/jquery.sharrre-1.3.4.min.js"></script> -->
     <script src="{{ Config::get('app.head_url') }}frontend/js/jquery.sharrre-1.3.4.min.js"></script>
    <script>
        function initShare() {
      $(".social-buttons").sharrre({
        share: {
          twitter: true,
          facebook: true,
          googlePlus: true
        },
        template:
          '<div class="box"><div class="left">Share</div><div class="middle"><a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="twitter"><i class="fa fa-twitter"></i></a><a href="#" class="googleplus"><i class="fa fa-google-plus"></i></a></div><div class="right">{total}</div></div>',
        enableHover: false,
        enableTracking: true,
        render: function(api, options) {
          $(api.element).on("click", ".twitter", function() {
            api.openPopup("twitter");
          });
          $(api.element).on("click", ".facebook", function() {
            api.openPopup("facebook");
          });
          $(api.element).on("click", ".googleplus", function() {
            api.openPopup("googlePlus");
          });
        }
      });
    }
        $('#coupon_code').hide();
        $('.coupon_text').show();
        $('#tweet-follow').click(function(){
            $('.coupon_code').show();
            $('.coupon_text').hide();
        });
      // if you want to use the 'fire' or 'disable' fn,
      // you need to save OuiBounce to an object
      var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'), {
        aggressive: true,
        timer: 0,
        callback: function() { console.log('ouibounce fired!'); }
      });

      $('body').on('click', function() {
        $('#ouibounce-modal').hide();
      });

      $('#ouibounce-modal .close_popup').on('click', function() {
        $('#ouibounce-modal').hide('slow');
      });

      $('#ouibounce-modal .modal').on('click', function(e) {
        e.stopPropagation();
      });
      
      // $('.fb-like').click(function(event) {
      //   alert('check');
      // });
      // function showpopup(){
      //   alert('dfsdf');
      // }

      
</script>

@endif




