<?php $locale = url(Session::get('locale'));?>
<?php if(isset($headerBarPromo) && sizeof($headerBarPromo)): ?>

    <div class="header_promo">        
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php foreach ($headerBarPromo as $hp) : ?>
                <?php 
                    $content = '<span class="promo-offer"><span class="promo-desc">'.$hp->msg.'</span>';
                    $content .= '<span class="promo-text">Promo: <span class="promo-code">'.$hp->code.'</span></span>';
                    if(trim($hp->btn_txt) != '')
                    {
                        $content .= '<a href="'.$hp->url.'" class="alert-link">'.$hp->btn_txt.'</a>';
                    }
                    else
                    {
                        $content = '<a href="'.$hp->url.'" class="alert-link">'.$content.'</a>';
                    }
                    $content .= '</span>';
                    echo $content;
                ?>
                
            <?php endforeach; ?>
        </div>
       
    </div>
<?php endif; ?>
<header class="header">
    <nav class="navbar nav-down" data-fullwidth="true" data-menu-style="transparent"  data-animation="shrink">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                    <div class="navbar-header logo">
                        <button type="button" class="navbar-toggle collapsed hidden-sm hidden-xs" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                            <img src="{{ Config::get('app.head_url') }}frontend/images/hamburger-white.svg" alt="Toggle Menu">
                        </button>
                        @if(isset($csetting))
                        <a href="{{URL('/')}}" class="navbar-brand to-top">
                           <img src="{{ Config::get('app.head_url') }}logo/{{$csetting->store_logo}}">
                        </a>
                        @endif
                    </div>
                </div>
                @if(isset($csetting))
                <div class="col-lg-2 col-md-4 col-sm-6 hidden-xs auto-width">
                    <div class="top-links">
                        <ul>
                            <li><a href="{{$csetting->chat}}">Support</a></li>
                            <li class="menu-icon mobile"><a href="tel:{{$csetting->phone}}"><span class="rounded-border pe-7s-call icon-size-16"></span>{{$csetting->phone}}</a></li>
                            <li class="menu-icon"><a href="mailto:{{$csetting->email}}"><span class="pe-7s-mail icon-size-25"></span>{{$csetting->email}}</a></li>
                        </ul>
                    </div>
                </div>
                @endif
                <div class="col-lg-6 col-md-4 hidden-xs auto-width">
                    <div id="main-nav" class="navbar-collapse collapse">
                        <div class="container">
                @if(!empty($res_menu))
                <ul class="nav navbar-nav menu-right">
                @foreach($res_menu as $key => $menu)

                <?php $ressub = DB::table('menu_sub')->where('menu_id',$menu->menu_id)->where('submenu_status',1)->get(); ?> 

                    @if(!empty($ressub))

                         @if($menu->mega_menu != 1)
                            <li class="dropdown">
                                <a class="drop-lft" href="{{ URL($locale.'/'.$menu->menu_link.'/') }}">{{$menu->menu_name}}</a> <a href="#" class="dropdown-toggle drop-lft-last" data-toggle="dropdown"><span class="caret"></span></a> <!--<span class="caret"></span>--><!--submenu -->

                            <ul class="dropdown-menu">
                                @foreach($ressub as $subsmenu)
                                <?php $submenuUrl = str_replace(' ' ,'-',$subsmenu->sub_link); ?>
                                <li>
                                <a href="{{ URL($locale.'/'.$menu->menu_link.'/'.$submenuUrl.'/') }}">{{ucwords($subsmenu->sub_name)}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @else
                        <li class="dropdown megamenu vos">
                                <a class="drop-lft" href="{{ URL($locale.'/'.$menu->menu_link.'/') }}">{{$menu->menu_name}}</a>  <a href="#" class="dropdown-toggle drop-lft-last" data-toggle="dropdown"><span class="caret"></span></a><!--<span class="caret"></span>-->

                                 <ul class="dropdown-menu fullwidth">
                                    <li class="megamenu-content withdesc">
                                        <?php echo html_entity_decode($menu->megamenu_content); ?>
                                    </li>
                                 </ul>
                             </li>

                        @endif

                     @else
                        <li>
                        <a href="{{ URL($locale.'/'.$menu->menu_link.'/') }}">{{$menu->menu_name}}</a>
                        </li>
                     @endif
                    

                    @endforeach
                
                </ul>
                @endif 
                        </div>  
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 auto-width pull-right pl0">
                    <div class="table-icon-menu">
                        <div class="search-form">
                            @include('includes.search_form')
                            <div class="actions-search">
                            <a class="search-icon action-search" onclick="showHideFormSearch()">
                                <i class="pe-7s-search"></i><span>Search</span>
                            </a >
                            </div>
                            
                        </div>
                        <div class="navbar-toggle-menu">
                            <button class="sidebar-btn">
                               <img src="{{ Config::get('app.head_url') }}frontend/images/menu-icon.svg" class="cls-1" alt="Toggle Menu">
                            </button>
                            
                        </div>
                    </div>
                    
                </div>
                <div id="mySidenav" class="sidenav collapse">
                    <div class="btn-close"><a href="javascript:void(0)" class="closebtn">&times;</a></div>
                        <div class="off_canvas-menu">
                        <ul id="append" class="nav"></ul>
                        <ul>
                            
                    @if(!empty($getutl))

                        @foreach($getutl as $key => $utl)

                            <?php 
                            if( ! strpos($utl->menu_link, '.html') )
                                $utl->menu_link = (trim($utl->menu_link) != '')?$utl->menu_link.'/':null; 
                            ?>

                            @if(!empty($utl->menu_name ) && !$utl->button_text)
                            <li>
                                @if($utl->menu_link)
                            <?php $pages = $utl->menu_link; ?>
                            @else
                            <?php $pages = "#"; ?>
                            @endif
                               <a href="{{ url($locale.'/'.$pages) }}" >{{$utl->menu_name}}</a>
                           </li>
                            @else
                            <li>
                                <div class="buttons">
                                    <a href="{{ url($locale.'/'.$utl->menu_link) }}" class="btn">{{$utl->menu_name}}</a> 
                                </div>
                            </li>
                             @endif
                            
                        @endforeach
                    @endif                        
                        </ul>
                        </div>
                </div>
                
                
            </div>
        </div>
    </nav>
</header>