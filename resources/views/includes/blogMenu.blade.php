<div class="blog-menu" id="masthead">
  <div class="container">
    <div class="row">
      <div class="blog-menu">
        <nav role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand" href="#">Blog Categories</div>
          </div>
          <div class="container">
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
              @if(sizeof($categories_menu))
              <ul class="nav navbar-nav blog-nav">
                @foreach($categories_menu as $cm)
                <li class="{{ (isset($cm['child']) && sizeof($cm['child'])) ? 'dropdown': null }}" >
                  <a href="{{ URL(Session::get('locale'),['blog',$cm['slug']])}}/" class="{{ (isset($cm['child']) && sizeof($cm['child'])) ? 'dropdown-toggle': null }}">
                  {{$cm['name']}}</a>
                  @if(isset($cm['child']) && sizeof($cm['child']))
                  <span class="caret"></span>
                  @endif
                  @if( isset($cm['child']) && sizeof($cm['child']) )
                  <ul class="dropdown-menu">
                    @foreach($cm['child'] as $ch)
                    <li class="kopie"><a href="{{ URL(Session::get('locale') ,['blog',$ch['slug']])}}/">{{$ch['name']}}</a>
                    </li>
                    @endforeach
                  </ul>
                  @endif
                </li>
                @endforeach
              </ul>
              @endif
            </div>
          </div>
        </nav>
      </div>
    </div>
  </div>
</div>