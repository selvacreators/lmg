 <footer class="footer">
    <div class="copyright text-center">
        <div class="container">                    
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="footer-menu">
                        @if(isset($csetting))
                        <div class="col-md-offset-2 col-md-2 col-sm-3 col-xs-12 text-right finfo">
                            <div class="copyright-text vertical-separator">
                                <a href="{{$csetting->footer_link}}" class="hide-on-mobile hide-on-desk vertical-separator">{{$csetting->footer_copyright}}</a>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-8 col-sm-6 col-xs-12 text-center auto-width finfo">
                            <div class="list-seperator">
                                @if(!empty($footer_menu))
                                <ul>
                                    @foreach($footer_menu as $key => $menu)
                                    <?php $locale = url(Session::get('locale')); ?>
                                        <li>
                                            <a href="{{ URL($locale.'/'.$menu->menu_link) }}">{{$menu->menu_name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12 finfo">
                            <div class="swicther">
                                <div class="btn-group dropup switcher-language" id="switcher-language">
                                    <ul class="dropdown-menu" role="menu">
                                        <?php foreach($langData as $ld):
                                            if(Session::get('locale') == $ld->lang_code)
                                            {
                                                $activeLangName = $ld->lang_name;
                                                $activeLangCode = $ld->lang_code;
                                            }
                                        ?>
                                            <li>
                                                <a href="{{url('langSwitch/'.$ld->lang_code)}}" class="language">
                                                <img src="{{ Config::get('app.head_url') }}flags/{{$ld->lang_code}}.png" alt="" /> 
                                                <span lang="{{$ld->lang_code}}">{{$ld->lang_name}}</span>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>                              
                                    </ul>
                                    <button type="button" class="switcher-toggle dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ Config::get('app.head_url') }}flags/{{$activeLangCode}}.png" alt="" /><span class="text">{{$activeLangName}}</span> <span class="caret"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>