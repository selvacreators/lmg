<div class="promo-banner">
     <div class="image">
        <img src="{{ Config::get('app.head_url') }}promo-banner/{{$banner->banner_image}}" >
    </div>
    <div class="promo-button">
        <a href="{{URl($banner->banner_btn_url)}}" class="btn promo-btn">{{$banner->banner_btn_txt}}</a>
    </div>
    <div class="promo-text">
        <p>{{$banner->banner_desc}}</p>
    </div>
</div>
<div class="clearfix mt20"></div>
