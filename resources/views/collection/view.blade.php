@extends('layouts.layout_home')

@section('content')
@include('includes.header')

@include('includes.breadcrumb')
  <div class="page-title-wrapper subbrand-pagetitle">
      <div class="parallax">
        <div class="background-image">
          <img src="{{ Config::get('app.head_url') }}brand/brand_banner/{{$collection[0]->brands_banner}}">
        </div>
        <div class="clearfix"></div>
        <div class="container">
          <div class="row">
            <div class="text-center">
              <div class="content-heading">
                <h1 class="title">{{$collection[0]->brand_name}}</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <main class="content-wrapper">
    <div class="page-content">
      <section class="brand-collection">
        <div class="container">
          <div class="row pt40 mb40">
            <?php $i=0; ?>
           @foreach($collection as $key => $val)
           @if($key == 0)
            <div class="col-md-3 col-sm-12">
              <div class="brand-title image pull-left">
                <img src="{{ URL(Config::get('app.head_url')) }}/brand/brand_logo{{'/'.$val->brand_logo}}" alt="" width="500">
              </div>
            </div>
            <div class="col-md-9 col-sm-12">
              <div class="text-justify">
                <div class="desc">
                  <?php echo  html_entity_decode($val->brand_description); ?>
                </div>
              </div>
            </div>
            @endif
            @endforeach
          </div>
        </div>
        

        <div class="container-fluid">
        <div class="all-brands">
        <div class="content">

          @foreach($collection as $key => $val)
          @if($val->position == 'left')
            <section class="{{$val->imge_position}}-content">  
              <div class="row row-odd ">
                <div class="col-md-6 view-animate fadeInLeftSm delay-1 active pl0 pr0">
                <div class="image  promoimage promoimage-animated">
                  <img alt="{{$val->breadcumb_title}}" class="img-responsive" src="{{ Config::get('app.head_url') }}collection/images/{{$val->feature_image}}">
                </div>
                </div>

                <div class="col-md-6 view-animate fadeInRightSm delay-1 active">
                <div class="brand-details">
                <div class="brand-name">
                <div class="brand-title image">
                  <h2 class="collection-name h3">{{$val->collection_name}}</h2>
                  <h3 class="brand-slogan h4">{{$val->col_caption}}</h3>
                </div>
                </div>

                <div class="brand-description">
                <?php echo html_entity_decode($val->col_des); ?>

                </div>

                <div class="brand-button">
                  <a class="btn btn-default" href="{{ URL( $path , [$val->collection_btn_url] ).'/' }}" title="{{$val->breadcumb_title}}">{{$val->btn_text}}</a>
                </div>
                </div>

                </div>
              </div>
            </section>
          @endif
          @if($val->position == 'right')
            <section class="{{$val->imge_position}}-content">  
              <div class="row row-odd ">
                <div class="col-md-6 col-md-push-6 view-animate fadeInRightSm delay-1 active pl0 pr0">
                  <div class="image  promoimage promoimage-animated">
                    <img alt="{{$val->breadcumb_title}}" class="img-responsive" src="{{ Config::get('app.head_url') }}collection/images/{{$val->feature_image}}">
                    <!-- <img alt="{{$val->breadcumb_title}}" class="img-responsive" src="{{ Config::get('app.head_url') }}brand/brand_collection{{$val->brand_featureimg}}"> -->
                  </div>
                </div>

                <div class="col-md-6 col-md-pull-6 view-animate fadeInLeftSm delay-1 active">
                  <div class="brand-details">
                    <div class="brand-name">
                      <div class="brand-title image">
                        <h2 class="collection-name h3">{{$val->collection_name}}</h2>
                        <h3 class="brand-slogan h4">{{$val->col_caption}}</h3>
                      </div>
                    </div>

                    <div class="brand-description">
                      <?php echo html_entity_decode($val->col_des); ?>
                    </div>

                    <div class="brand-button">
                      <a class="btn btn-default" href="{{ URL( $path , [$val->collection_btn_url] ).'/' }}" title="{{$val->breadcumb_title}}">{{$val->btn_text}}</a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          @endif
          @endforeach
        </div>
        </div>
        </div>
      </section>
    </div>
  </main>
@include('includes.newsletter-form')
        
@include('includes.footer')

@stop