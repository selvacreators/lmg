 @if(!empty($col_name))
 <?php $locale = url(Session::get('locale')); ?>
 <nav>
    <ul class="nav category-nav collapsible-menu">
        <li class="has-children"><a href="{{ URL($colection_path)}}/" class="pl10"><strong>{{ucfirst($brands_Name)}}</strong></a><span class="arrow"></span>

            <ul class="nav nav-pills nav-stacked">
                @foreach($col_name as $col)
                <?php $col_url = $colection_path.'/'.$col->collection_slug.'/'; ?>
                <li class="active">
                    <a href="{{URL($col_url)}}/">
                        <i class="ion-ios-arrow-forward"></i>
                        <span class="text-uppercase pr5">{{$col->collection_name}}</span>Collection
                    </a>
                </li>
                @endforeach
            </ul>
        </li>
        <li class="pl10"><a href="{{URL($locale.'/')}}accessories/"><strong>Bedding Accessories</a></strong></li>
        <li class="pl10"><a href="{{URL($locale.'/')}}beds/"><strong>Bed Frames / Adjustable Beds</a></strong></li>
        <li class="pl10"><a href="{{URL($locale.'/')}}brands/"><strong>Our World Class Brands</a></strong></li>
    </ul>
    
</nav>

@endif

                    