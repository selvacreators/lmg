@extends('layouts.layout_home')

@section('content')

@include('includes.header')
@include('includes.breadcrumb')
<?php if(isset($brands_Name)): ?>
    <?php if(!empty($collection[0]->collection_banner)): ?>
    <div class="page-title-wrapper pt60">
      <div class="parallax">
       
        <div class="background-image">
          <img src="{{ Config::get('app.head_url') }}collection/banner/{{$collection[0]->collection_banner}}">
        </div>
       
        <div class="clearfix"></div>
       
        <div class="container">
          <div class="row">
            <div class="text-center">
              <div class="content-heading">
                <h1 class="title">{{$collection[0]->brand_name}}</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php else: ?>
    <div class="page-title-wrapper pt60">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <div class="content-heading"><h1 class="title">{{$currentPath}}</h1></div>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
<?php endif ?>
<?php if (!isset($brands_Name)) : ?>
    <?php if (sizeof($getproductImage)) : ?>
        <?php if($getcategory[0]->cate_desc && $getcategory[0]->cate_image): ?>
            <div class="page-title-wrapper pt60">
                <div class="container-fluid">
                    <div class="row">
                        <div class="cat-header">                       
                            <div class="content-heading">
                                <h1 class="title h3">{{$getcategory[0]->category_name}}</h1>
                            </div>
                            <div class="col-md-6 text-justify">
                                <?php echo html_entity_decode($getcategory[0]->cate_desc); ?>
                            </div>
                            <div class="col-md-6">
                                <img src="{{ Config::get('app.head_url')}}product/category/{{$getcategory[0]->cate_image}}" class="img-responsive img-full">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else: ?> 
            <div class="page-title-wrapper pt60">
                <div class="container">
                    <div class="row">
                        <div class="text-center">
                            <div class="content-heading">                
                            <h1 class="title">{{$currentPath}}</h1>              
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        <?php endif; ?>
    <?php else: ?>
    <div class="page-title-wrapper pt60">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <div class="content-heading">                
                    <h1 class="title">{{$currentPath}}</h1>              
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
<?php endif ?>
<main class="content-wrapper">
    <div class="page-content">      
        <div class="container-fluid">
            <div class="row">
                <section class="product-category pt40 pb40">

                    <!-- Filter Show Section  Start Here-->
                    <aside class="col-md-3 col-sm-4 category-sidebar">  
                        <?php if(!isset($brands_Name) && sizeof($getproductImage)): ?>  
                            @include('collection.filter')                       
                        <?php endif; ?>
                        
                        <?php if(isset($brands_Name)): ?>
                             @include('collection.nav')
                        <?php endif; ?>
                        @if($promo_banner)
                          @foreach($promo_banner as $key => $banner)
                            @if(isset($banner->banner_show_page) && $banner->banner_show_page == 'Category_list')
                              @include('includes.promobanner')
                            @endif
                          @endforeach
                        @endif    
                    </aside>

                    <div id="content" class="col-md-9 col-sm-8">
                        <div class="category-filter">
                            <nav class="top-control box-has-content">
                                <div class="show-text">Showing <span id="showing-prod">1 - <?php echo count($getproductImage); ?></span> of <span id="tot-prod"><?php echo count($getproductImage); ?></span> results</div>
                                <ul class="group-control">                                    
                                    <li class="item-control" >
                                        <ul class="group-button">
                                            <li class="active"><a href="#" id="grid_view_icon" class="grip-button active"><i class="ion-grid"></i></a></li>
                                            <li><a href="#" id="list_view_icon" class="list-button"><i class="ion-ios-list-outline"></i></a></li>
                                        </ul>
                                    </li>                   
                                </ul>
                            </nav>
                        </div>
                        <div id="main" class="eq_height row product-grid product-holder">
                            @foreach($getproductImage as $getimage)
                            <div class="product-layout col-md-4 col-sm-6 item mb30">
                                <div class="item-height">
                                    <div class="product-thumb">
                                        <div class="image">
                                            <a href="{{ URL($path,[$getimage->product_slug]).'.html' }}">
                                               <img src="{{ Config::get('app.head_url') }}product/images/{{$getimage->prod_slider_image}}">
                                            </a>
                                        </div>
                                        <span class="product-label {{$getimage->comfort}}">{{ucfirst($getimage->comfort)}}
                                        </span>
                                    </div>
                                    <div class="product-details">
                                        <h3 class="product-name">
                                            <a href="{{ URL($path,[$getimage->product_slug]).'.html' }}">{{ucfirst($getimage->name)}}</a>
                                        </h3>
                                        <div class="product-desc"> 
                                        </div>
                                        <div class="product-attr"></div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="pagination">
                            <ul class="list-page">
                                <!-- <li><a href="#" class="page-number prev"><i class="ion-ios-arrow-left" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="page-number">1</a></li>
                                <li><a href="#" class="page-number">2</a></li>
                                <li><a href="#" class="page-number">3</a></li>
                                <li><a href="#" class="page-number next"><i class="ion-ios-arrow-right" aria-hidden="true"></i></a></li> -->
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
@include('includes.newsletter-form')

@include('includes.footer')
@endsection
@section('script')
<script src="{{ Config::get('app.head_url') }}frontend/js/colorbox/jquery.colorbox-min.js"></script>
<!-- <script src="{{ Config::get('app.head_url') }}frontend/js/chosen.min.js"></script> -->
<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.matchHeight.min.js"></script>

<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.plugins.adv.js"></script>
<script src="{{ Config::get('app.head_url') }}frontend/js/adv_ajaxfilter.js"></script>

<script type="text/javascript">
    /*$(document).ready(function(){
        $(".chosen-select").chosen({disable_search_threshold: 10});
    });*/
    // Equal height on product items
    $(document).ready(function(){
        $('#content #main.eq_height .item .item-height').matchHeight();
    });
    $(window).load(function() {
       $('#content #main.eq_height .item .item-height').matchHeight();
    });
    $(function() {
        var pv = $.cookie('product_view');
        if (pv == 'g') {
            $('#main').removeClass('product-list');
            $('#main').addClass('product-grid');
            $('#list_view_icon').removeClass();
            $('#grid_view_icon').addClass('active');
        } else if (pv == 'l') {
            $('#main').removeClass('product-grid');
            $('#main').addClass('product-list');
            $('#grid_view_icon').removeClass();
            $('#list_view_icon').addClass('active');
        } else {
            $('#grid_view_icon').addClass('active');
        }
    });
    $(window).load(function() {
        $('#grid_view_icon').click(function() {
            $(this).parent().addClass('active');
            $('#list_view_icon').parent().removeClass('active');
            $(this).addClass('active');
            $('#list_view_icon').removeClass();
            $('#main').fadeTo( 150, 0, function() {
                $('.product-layout').addClass('col-md-4');
                $('.product-layout').removeClass('col-md-12');
                $(this).removeClass('product-list');
                $(this).addClass('product-grid').fadeTo( 500, 1);
                $('#content #main.eq_height .item .item-height').matchHeight();
                $.cookie('product_view', 'g');
            });
            return false;
        });
        $('#list_view_icon').click(function() {
            $(this).parent().addClass('active');
            $('#grid_view_icon').parent().removeClass('active');
            $(this).addClass('active');
            $('#grid_view_icon').removeClass();
            $('#main').fadeTo( 150, 0, function() {
                $('.product-layout').addClass('col-md-12');
                $('.product-layout').removeClass('col-md-4');
                $(this).removeClass('product-grid');
                $(this).addClass('product-list').fadeTo( 500, 1);
                $.cookie('product_view', 'l');
            });
            return false;
        });
    });

//  var start = 0;
// var nb = 5;
// var end = start + nb;
// var length = $('.image img').length;
// var list = $('.image img');

// list.hide().filter(':lt('+(end)+')').show();


// $('.prev, .next').click(function(e){
//    e.preventDefault();

//    if( $(this).hasClass('prev') ){
//        start -= nb;
//    } else {
//        start += nb;
//    }

//    if( start < 0 || start >= length ) start = 0;
//    end = start + nb;       

//    if( start == 0 ) list.hide().filter(':lt('+(end)+')').show();
//    else list.hide().filter(':lt('+(end)+'):gt('+(start-1)+')').show();
// });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".collapsible-menu").on("click", "li > .arrow", function(e) {
            var p = $(this).parent();
            $(".has-children").find('.nav-pills.nav-stacked').removeClass('hide');
            if(p.hasClass("has-children")) p.toggleClass("open").siblings(".open").removeClass("open");
            $(".has-children.open").find('.nav-pills.nav-stacked').addClass('hide');
            //p.find('.nav-pills.nav-stacked').removeClass('hide');
        });
    });
</script>
@endsection