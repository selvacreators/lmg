@if(!empty($menu_list))
 <?php $locale = url(Session::get('locale')); ?>
     <nav>
            <ul class="nav category-nav collapsible-menu">
                <li class="has-children">
                    <a href="{{ URL($locale)}}/{{$currentPath1}}/" class="pl10"><strong>{{ucfirst($currentPath1)}}</strong></a><span class="arrow"></span>
                    <ul class="nav nav-pills nav-stacked">
                        @foreach($menu_list as $menudata)
                           <?php $col_url = $colection_path.'/'.$menudata->sub_link; ?>
                        <li class="active">
                            <a href="{{URL($col_url)}}">
                                <i class="ion-ios-arrow-forward"></i>
                                <span class="text-uppercase pr5">{{$menudata->sub_name}}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                <?php if ($currentPath1 == 'accessories'): ?>
                    
                    <li class="pl10"><a href="{{URL($locale.'/')}}beds/"><strong>Bed Frames / Adjustable Beds</a></strong></li>
                <?php elseif ($currentPath1 == 'beds'): ?>
                     <li class="pl10"><a href="{{URL($locale.'/')}}accessories/"><strong>Bedding Accessories</a></strong></li> 
                <?php else: ?>
                    <li class="pl10"><a href="{{URL($locale.'/')}}accessories/"><strong>Bedding Accessories</a></strong></li>
                    <li class="pl10"><a href="{{URL($locale.'/')}}beds/"><strong>Bed Frames / Adjustable Beds</a></strong></li>            
                <?php endif; ?>              

                <li class="pl10"><a href="{{URL($locale.'/')}}brands/"><strong>Our World Class Brands</a></strong></li>
            </ul>            
        </nav>

@endif
