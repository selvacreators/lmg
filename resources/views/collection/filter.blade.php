 @section('style')
<link rel="stylesheet" type="text/css" href="{{ Config::get('app.head_url') }}frontend/css/chosen.min.css">
<link rel="stylesheet" type="text/css" href="{{ Config::get('app.head_url') }}frontend/css/adv_ajaxfilter.css">
<!-- <link rel="stylesheet" type="text/css" href="{{ Config::get('app.head_url') }}frontend/css/jquery.loadmask.css"> -->

@endsection
<?php if ($currentPath1 == 'mattresses'): ?>
    

      <div id="filter-col">

    <h3>Filter</h3>

    <div class="box" id="adv_ajaxfilter_box">


        <div class="bordered_content">

            <?php if($filters && sizeof($filters)): ?>
            <form id="adv_ajaxfilter">
                <script type="text/javascript">
                        function afterload(){
                            
                        }
                    </script>
                <?php foreach($filters as $filKey => $filValue): ?>
                    <div id="refined_filter"></div>
                    <?php if($filKey == 'Brands' || $filKey == 'Comfort' || $filKey == 'Size' || $filKey == 'Type'):?>
                    
                    @if($currentPath != $currentPath1)
                        <div class="attribute_box option_box" id="{{ $filKey }}" style="<?php if($filKey == 'Type'): echo 'display:none;'; endif;?>">
                            <div class="option_name ">{{ $filKey }}</div>
                            <div class="collapsible filters">
                                <?php if(sizeof($filValue)): foreach($filValue as $key => $value): ?>
                                <input class="filtered a_name" id="attribute_{{ $filKey }}_{{ $key }}" type="checkbox" name="attribute_value[{{ strtolower($filKey) }}][]" value="{{ $value }}">
                                <label for="attribute_{{ $filKey }}_{{ $key }}">{{ $value }}</label>

                                <?php endforeach; endif; ?>
                                <input type="text" name="currPath" value="{{$currentPath}}">
                                <input type="text" name="path" value="{{$path}}">
                            </div>
                        </div>
                    @else

                        <div class="attribute_box option_box" id="{{ $filKey }}">
                            <div class="option_name ">{{ $filKey }}</div>
                            <div class="collapsible filters">
                                <?php if(sizeof($filValue)): foreach($filValue as $key => $value): ?>
                                <input class="filtered a_name" id="attribute_{{ $filKey }}_{{ $key }}" type="checkbox" name="attribute_value[{{ strtolower($filKey) }}][]" value="{{ $value }}">
                                <label for="attribute_{{ $filKey }}_{{ $key }}">{{ $value }}</label>

                                <?php endforeach; endif; ?>
                                <input type="text" name="currPath" value="{{$currentPath}}">
                                <input type="text" name="path" value="{{$path}}">
                            </div>
                        </div>
                    @endif
                    
                    <?php endif; ?>
                <?php endforeach; ?>
            </form>
            <?php endif; ?>
            <div class="clear_filter">
                <a class="btn btn-default">Reset Filter</a>
            </div>
        </div>
    </div>
    
</div>
<?php else: ?>
    <?php if ($currentPath1 != 'mattresses'): ?>
        <?php if(isset($getcategory)): ?>
             @include('collection.nav_cate')
         <?php  endif; ?>
     <?php  endif; ?>

<?php endif ?>
