<div class="form-group row required">
    <label for="language" class="control-label col-sm-3">Language:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="language" class="form-control" id="language" value="{{ old('language') }}">
            <option value="">Select</option>
            @if($storeLang)
            @foreach($storeLang as $lang)
            <option {{ old('language') == $lang->lang_code ? 'selected="selected"' : '' }} value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
            @endforeach
            @endif
        </select>
        <?php if(empty($errors->any())):?>
            <div class='text-danger'>
            <?php echo $errors->product_errors->first('language'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="form-group row required">
    <label for="product_name" class="control-label col-sm-3">Product Name:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" value="{{ old('product_name') }}" name="product_name" id="product_name"/>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('product_name').'</div>';?>
    </div>
</div>
<div class="form-group row required">
    <label for="product_slug" class="control-label col-sm-3">Product Slug:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" value="{{ old('product_slug') }}" name="product_slug" id="product_slug" class="form-control" autocomplete="off" placeholder="Example: american-star"/>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('product_slug').'</div>';?>
        <div id="perror"></div>
    </div>
</div>
<div class="form-group row">
    <label for="short_desc" class="control-label col-sm-3">Short Description:</label>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="short_desc" id="short_desc" class="ckeditor form-control">{{ old('short_desc') }}</textarea>
    </div>
</div>
<div class="form-group row">
    <label for="desc" class="control-label col-sm-3">Product Description:</label>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="desc" id="desc" class="ckeditor form-control">{{ old('desc') }}</textarea>
    </div>
</div>
<div class="form-group row required">
    <label for="meta_title" class="control-label col-sm-3">Meta Title:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" value="{{ old('meta_title') }}" name="meta_title" id="meta_title" class="form-control"/>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('meta_title').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="meta_keyword" class="control-label col-sm-3">Meta Keyword:</label>
    <div class="col-sm-9">
        <input type="text" value="{{ old('meta_keyword') }}" name="meta_keyword" id="meta_keyword" class="form-control" />
    </div>
</div>
<div class="form-group row">
    <label for="meta_desc" class="control-label col-sm-3">Meta Description:</label>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="meta_desc" id="meta_desc" class="form-control">{{ old('meta_desc') }}</textarea>
    </div>
</div>
<fieldset class="form-group">
    <legend><h4>Features Icon:</h4></legend>
    <div class="col-md-4">
        <div class="row">
            <div class="col-sm-12">
                <label for="Image" class="control-label">Icon Image:</label>            
                <input type="file" name="service_image[one]" id="service_image1" class="form-control">
            </div>
            
            <div class="col-sm-12">
                <label for="Image" class="control-label">Icon Text:</label>
                <textarea type="text" name="service_text[one]" value="{{ old('service_text[one]') }}" row="5" id="service_text1" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-sm-12">
                <label for="Image" class="control-label">Icon Image:</label>
                <input type="file" name="service_image[two]" id="service_image2" class="form-control">
            </div>
            <div class="col-sm-12">
                <label for="Image" class="control-label">Icon Text:</label>
                <textarea type="text" name="service_text[two]" value="{{ old('service_text[two]') }}" row="5" id="service_text2" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-sm-12">
                <label for="Image" class="control-label">Icon Image:</label>
                <input type="file" name="service_image[three]" id="service_image3" class="form-control">
            </div>
            <div class="col-sm-12">            
                <label for="Image" class="control-label">Icon Text:</label>
                <textarea type="text" name="service_text[three]" value="{{ old('service_text[three]') }}" row="5" id="service_text3" class="form-control"></textarea>
            </div>
        </div>
    </div>
</fieldset>