@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">         
    <div class="inner-content">
        <div class="container-fluid"> 
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title ">Updated Weight</h3>
                </div>
                <div class="card-body">                   
                    {!! Form::open(array('url' => '/Admin/edit_weight','class'=>'form-horizontal weight-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                          <input name="_token" type="hidden" value="{{csrf_token()}}"> 
                          <input name="weightclass_id" type="hidden" value="{{$weight[0]->weight_class_id}}"> 
                                                    
                            <div class="form-group row">
                                <label class="control-label col-xs-3" for="pagename">Language:<span class="note">*</span></label>
                                <div class="col-xs-6">
                                    <select name="lang_code" id="lang_code" class="form-control">
                                        @foreach($language as $val)
                                        @if($val->lang_code == $weight[0]->lang_code)
                                        <option value="{{$val->lang_code}}" selected="selected">{{$val->lang_name}}</option>
                                        @else
                                        <option value="{{$val->lang_code}}" >{{$val->lang_name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-xs-3" for="txtmenu">Value:<span class="note">*</span></label>
                                <div class="col-xs-6">
                                    <input type="text" name="value" id="value" value="{{round($weight[0]->value)}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-xs-3" for="default">Default:<span class="note">*</span></label>
                                <div class="col-xs-6">
                                    <input type="text" name="default" id="default"  value="{{$weight[0]->default}}" class="form-control">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label class="control-label col-xs-3" for="txtmenu">Title:<span class="note">*</span></label>
                                <div class="col-xs-6">
                                    <input type="text" name="title" id="title" value="{{$weight[0]->title}}"  class="form-control">
                                </div>
                            </div>

                             <div class="form-group row">
                                <label class="control-label col-xs-3" for="txtmenu">Unit:<span class="note">*</span></label>
                                <div class="col-xs-6">
                                    <input type="text" name="unit" id="unit" value="{{$weight[0]->unit}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                <div class="col-xs-6">
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="1">Enable</option>
                                        <option value="2">Disable</option>
                                    </select>
                                </div>
                            </div>                       
                            <div class="buttons text-center">
                                <button class="btn btn-danger" type="reset">Reset</button>
                                <button class="btn btn-success" type="submit">Save</button>
                            </div>
                    {!! Form::close() !!}
                </div>
            </div>  
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".weight-form").validate({
    rules:
    {
      lang_code : "required",
      value: "required",
      default : "required",
      title : "required",
      unit : "required",
      status : "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      value: "Please enter value",
      default: "Please enter default",
      title: "Please enter title",
      unit: "Please enter unit",
      status: "Please choose status"
    }
  });
});
</script>
@endsection