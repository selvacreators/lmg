@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
              <div class="card-header">
                <a href="/Admin/AddQuestion" class="btn btn-success btn-sm pull-right">Add New Question</a>
                <h3 class="card-title">Product Questions List</h3>
              </div>
              <div class="card-body">
                <nav class="nav-container">
                  <ul class='nav nav-tabs text-center' role="tablist">
                  @if($language)
                    @foreach($language as $key =>$lan)
                      @if($lan->lang_code == 'en')
                        <?php $check = "active";?>
                      @else
                        <?php $check = "";?>
                      @endif
                      <li><a data-toggle="tab" class="{{$check}} show" href="#{{$lan->lang_name}}">{{$lan->lang_name}}</a></li>
                    @endforeach
                  @endif
                  </ul>
                </nav>
                <div class="tab-content pt-0 pb-0">
                  @if($language)
                    @foreach($language as $key =>$lan)
                      @if($lan->lang_code == 'en')
                        <?php $check = "active";?>
                      @else
                        <?php $check = "";?>
                      @endif
                      <div id="{{$lan->lang_name}}" class="tab-pane {{$check}} in fieldset">
                        <table id="datatable" class="datatable table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                          <thead>
                            <tr>
                              <th>S.No</th>
                              <th>Language</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Product Name</th>
                              <th>Questions</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th class="col-xs-2">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $i=1;?>
                            @if($question)
                            @foreach($question as $user)
                            @if($user->lang_code == $lan->lang_code)
                            <tr>
                              <td>{{$i++}}</td>
                              <td>{{$user->lang_code}}</td>
                              <td>{{$user->name}}</td>
                              <td>{{$user->emailid}}</td>
                              <td>{{$user->q_name}}</td>
                              <td>{{str_limit($user->question,20,' ...')}}</td>
                              <td>{{date_format(new DateTime($user->created_at),'Y/m/d')}}</td>
                              <td>
                              @if($user->status == '1')
                                <label class="label label-success">Enable</label>
                              @elseif($user->status == '2')
                                <label class="label label-danger">Disable</label>
                              @else
                                <label class="label label-info">Pending</label>
                              @endif
                              </td>
                              <td><a href="/Admin/editquestion/{{$user->question_id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                              <a href="/Admin/deletequestion/{{$user->question_id}}" onclick="return confirm('Are You Sure Delete Question?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o "></i></button></a>
                              </td>
                            </tr>
                            @endif
                            @endforeach
                            @endif
                          </tbody>
                        </table>
                      </div>
                    @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4 align="center">{{ Session::get('message') }}</h4>
        <center><button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button></center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('admin-script')
@endsection