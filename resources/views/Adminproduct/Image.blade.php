<div class="row">
    <div id="img-row" class="col-md-12">
        <table class="table" width="50%">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Sort Order</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot></tfoot>
        </table>
    </div>
    <div class="col-md-12">
        <div class="form-group row">
            <div class="col-sm-12">
                <div id="uploader">
                    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                </div>
                <input id="upload_image" type="hidden" value="" name="upload_image">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row multi" id="multi">
    <div class="col-md-10">
        <div class="col-md-6">
            <table class="table Imageorder-list"></table>
        </div>
    </div>
</div>