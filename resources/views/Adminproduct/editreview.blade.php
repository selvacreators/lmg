@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">         
    <div class="inner-content">
        <div class="container-fluid"> 
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title ">Update Product Review</h3>
                </div>
                <div class="card-body">                    

                    <form method="POST" action="{{ URL('Admin/edit_review')}}" class="form-horizontal review-form">
                      <input name="_token" type="hidden" value="{{csrf_token()}}"> 
                      <input name="pre_review_id" type="hidden" value="{{$review[0]->review_id}}"> 
                                                
                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="pagename">product:</label>
                            <div class="col-xs-6">
                                <select name="product_id" id="product_id" class="form-control" required="required">
                                    <option value="" >select</option>
                                    @foreach($product as $val)
                                    @if($val->product_id == $review[0]->product_id)
                                    <option value="{{$val->product_id}}" selected="selected">{{$val->name}}</option>
                                    @else
    
                                    <option value="{{$val->product_id}}">{{$val->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Title:</label>
                            <div class="col-xs-6">
                                <input type="text" name="title" value="{{$review[0]->title}}" id="title" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Name:</label>
                            <div class="col-xs-6">
                                <input type="text" name="name" value="{{$review[0]->name}}" id="name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Email Id:</label>
                            <div class="col-xs-6">
                                <input type="email" name="emailid" value="{{$review[0]->emailid}}" id="emailid" class="form-control">
                            </div>
                        </div>


                         <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Rating:</label>
                            <div class="col-xs-6">
                                <label class="radio-inline"> 
                    <input type="radio" name="rating" value="1" <?php if($review[0]->rating == '1'){ echo "checked";}?>>
                      1
                      </label>
                      <label class="radio-inline"> 
                        <input type="radio" name="rating" value="2" <?php if($review[0]->rating == '2'){ echo "checked";}?>>
                      2
                      </label>
                      <label class="radio-inline"> 
                        <input type="radio" name="rating" value="3" <?php if($review[0]->rating == '3'){ echo "checked";}?>>
                      3
                      </label>
                      <label class="radio-inline"> 
                        <input type="radio" name="rating" value="4" <?php if($review[0]->rating == '4'){ echo "checked";}?>>
                      4
                      </label>
                      <label class="radio-inline">
                       <input type="radio" name="rating" value="5" <?php if($review[0]->rating == '5'){ echo "checked";}?>>
                      5
                      </label>
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Text:</label>
                            <div class="col-sm-9">
                            <textarea cols="10" rows="5" name="text" id="text" class="form-control" >{{$review[0]->text}}</textarea>
                        </div>
                    </div>

                        
                        
                       
             <div class="form-group row">
                  <label class="control-label col-xs-3" for="menustatus">Status:</label>
                  <div class="col-xs-6">
                      <select name="status" id="status" class="form-control" required="required">
                          <option value="">select</option>
                          <option value="1" <?php if($review[0]->status == '1'){ echo "selected";}?>>Active</option>
                          <option value="2" <?php if($review[0]->status == '2'){ echo "selected";}?>>DeActive</option>
                      </select>
                  </div>
              </div>
                       
                        <div class="buttons text-center">
                            <button class="btn btn-danger" type="reset">Reset</button>
                            <button class="btn btn-success" type="submit">Save</button>
                        </div>                   

                </form>
            </div>
        </div>  
    </div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

              <center>

                <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

            </center>
            {{ Session::forget('message') }}
        </div>
    </div>
</div>
</div>
@endif
@stop

@section('admin-script')

<script type="text/javascript">
$(document).ready(function() {
  $('.review-form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        excluded: ':disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            product_id: {
              validators: {
                notEmpty: {
                  message: 'The product is required'
                }
              }
            },
            status: {
              validators: {
                notEmpty: {
                  message: 'The status is required'
                }
              }
            },
            title: {
                message: 'The title is not valid',
                validators: {
                    notEmpty: {
                        message: 'The title is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 50,
                        message: 'The title must be more than 2 and less than 50 characters long'
                    }
                }
            },
            name: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 50,
                        message: 'The name must be more than 2 and less than 50 characters long'
                    }
                }
            },
            emailid: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            rating: {
                validators: {
                    notEmpty: {
                        message: 'The rating is required'
                    }
                }
            }
        }
  });
});
</script>
@endsection
