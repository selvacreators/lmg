@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/insert/product-type','class'=>'form-horizontal product-type-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                          <div class="card">
                            <div class="card-header">
                                <a href="/Admin/product-type" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button id="save" class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Create Product Type</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" id="lang_code" class="form-control">
                                          <option value="">Select</option>
                                            @foreach($language as $lang)
                                            <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="name">Type Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="sort_order">Sort Order:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="sort_order" id="sort_order" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Enable</option>
                                            <option value="2">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".product-type-form").validate({
    rules:
    {
      lang_code : "required",
      name: "required",
      slug: "required",
      status : "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      name: "Please enter type name",
      slug: "Please enter type slug",
      status: "Please choose status"
    }
  });
  $("#language").change(function()
  {
    var lang = $(this).val(), ajxLoader = $("#ajax-loader");
    alert(lang);
    $.ajax({
      // url: '/getRelatedProduct?lang='+lang,
      url: '/Admin/get-product-type-info?lang='+lang,
      type: 'get',
      dataType: 'json',
      beforeSend: function(){ ajxLoader.show(); },
      success:function(result){

        $('#rproduct,#oproduct,#brand,#collection_id,#categories_id,#stock_status,#weight_class_id,#length_class_id,#store_id,#product_type,#attribute_id').empty();
        $('#brand,#collection_id,#stock_status,#product_type,#attribute_id').append('<option value="">Select</option>');
        $(".attribute-list").html('');

        $.each( result.stock_status,function( key, value )
        {
          $('#stock_status').append('<option value="'+ value.stock_status_id +'">'+ value.name +'</option>');
        });
        $.each( result.product_type,function( key, value )
        {
          $('#product_type').append('<option value="'+ value.id +'">'+ value.ptype_name +'</option>');
        });
      },
      complete:function(){ ajxLoader.hide(); }
    });
  });
});
</script>
@endsection