<div class="form-group row">
    <label for="store_id" class="control-label col-sm-3">Store Locator:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="store_id[]" id="store_id">
            @if($productInfo->store_location)
                @foreach($productInfo->store_location as $store_loca)
                    <option value="{{$store_loca->store_id}}">{{$store_loca->store_name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group row required">
    <label for="brand" class="control-label col-sm-3">Brand:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="brand" class="form-control" id="brand">
          <option value="">Select</option>
          @if($productInfo->brands)
              @foreach($productInfo->brands as $brand)
                <option {{ old('brand') == $brand->brand_id ? 'selected="selected"' : '' }} value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
              @endforeach
          @endif
      </select>
      <?php echo '<div class="text-danger">'.$errors->product_errors->first('brand').'</div>';?>
  </div>
</div>
<div class="form-group row">
    <label for="collection_id" class="control-label col-sm-3">Collection:</label>
    <div class="col-sm-9">
        <select class="selectpicker form-control" data-live-search="true" data-style="btn select-with-transition" title="Choose City" data-size="7" name="collection_id" id="collection_id">
            <option value="">Select</option>
            @if($productInfo->collection)
                @foreach($productInfo->collection as $col)
                    <option {{ old('collection_id') == $col->collection_id ? 'selected="selected"' : '' }} value="{{$col->collection_id}}">{{$col->collection_name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="categories_id" class="control-label col-sm-3">Categories:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="categories_id[]" id="categories_id">
            @if($productInfo->categories)
                @foreach($productInfo->categories as $cate)
                    <option value="{{$cate->categories_id}}">{{$cate->category_name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="store_id" class="control-label col-sm-3">Related Product:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose Related Product" data-size="7" name="rproduct[]" id="rproduct">
            @if($productInfo->ProductDesc)
                @foreach($productInfo->ProductDesc as $row)
                    <option value="{{$row->product_id}}">{{$row->name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="store_id" class="control-label col-sm-3">Other Product:</label>
    <div class="col-sm-9">
        <select class="selectpicker multiselect" data-style="btn select-with-transition" multiple title="Choose Other Product" data-size="7" name="oproduct[]" id="oproduct">
            @if($productInfo->ProductDesc)
                @foreach($productInfo->ProductDesc as $row)
                    <option value="{{$row->product_id}}">{{$row->name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="tag" class="control-label col-sm-3">Product Tag:</label>
    <div class="col-sm-9">
        <input type="text" value="{{ old('tag') }}" name="tag" id="tag" class="form-control" />
    </div>
</div>