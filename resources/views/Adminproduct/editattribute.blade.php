@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">         
    <div class="inner-content">
        <div class="container-fluid"> 
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title ">Edit Attribute</h3>
                </div>
                <div class="card-body">                    

                    <form method="POST" action="{{ URL('Admin/edit_attribute')}}" class="form-horizontal">
                      <input name="_token" type="hidden" value="{{csrf_token()}}"> 
                      <input name="attribute_id" type="hidden" value="{{$attribute[0]->attribute_id}}"> 


                       <div class="form-group row">
                            <label class="control-label col-xs-3" for="pagename">Language:</label>
                            <div class="col-xs-6">
                                <select name="lang_code" id="pagename" class="form-control" required="required">
                                    <option value="" >select</option>
                                    @foreach($language as $val)
                                    @if($val->lang_code == $attribute[0]->lang_code)
                                    <option value="{{$val->lang_code}}" selected="selected">{{$val->lang_name}}</option>
                                    @else
                                    <option value="{{$val->lang_code}}">{{$val->lang_name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                                                
                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="pagename">Attribute Group:</label>
                            <div class="col-xs-6">
                                <select name="group_id" id="group_id" class="form-control" required="required">
                                    <option value="" >select</option>
                                    @foreach($attr_group as $val)
                                    @if($val->attribute_group_id == $attribute[0]->attribute_group_id)
                                    <option value="{{$val->attribute_group_id}}" selected="selected">{{$val->name}}</option>
                                      @else
                                    <option value="{{$val->attribute_group_id}}">{{$val->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">name:</label>
                            <div class="col-xs-6">
                                <input type=text name="name" id="name" class="form-control" value="{{$attribute[0]->name}}" required="required">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Sort Order:</label>
                            <div class="col-xs-6">
                                <input type=text name="sort_order" id="sort_order" class="form-control" value="{{$attribute[0]->sort_order}}" onkeypress="return AllowNumbersOnly(event)">
                            </div>
                        </div>

                       
                        <div class="buttons text-center">
                            <button class="btn btn-danger" type="reset">Reset</button>
                            <button class="btn btn-success" type="submit">Save</button>
                        </div>                   

                </form>
            </div>
        </div>  
    </div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

              <center>

                <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

            </center>
            {{ Session::forget('message') }}
        </div>
    </div>
</div>
</div>
@endif
@stop
@section('admin-script')
<script>
    function AllowNumbersOnly(e) {
  var charCode = (e.which) ? e.which : e.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    e.preventDefault();
  }
}
</script>

@endsection