@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">         
    <div class="inner-content">
        <div class="container-fluid"> 
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title ">Updated Length</h3>
                </div>
                <div class="card-body">                    

                    <form method="POST" action="{{ URL('Admin/edit_length')}}" class="form-horizontal">
                      <input name="_token" type="hidden" value="{{csrf_token()}}"> 
                      <input name="lengthclass_id" type="hidden" value="{{$length[0]->length_class_id}}"> 
                                                
                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="pagename">Language:</label>
                            <div class="col-xs-6">
                                <select name="lang_code" id="lang_code" class="form-control" required="required">
                                    <option value="" >select</option>
                                    @foreach($language as $val)
                                    @if($val->lang_code == $length[0]->lang_code)
                                    <option value="{{$val->lang_code}}" selected="selected">{{$val->lang_name}}</option>
                                    @else
                                    <option value="{{$val->lang_code}}" >{{$val->lang_name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Value:</label>
                            <div class="col-xs-6">
                                <input type=text name="value" id="value" value="{{round($length[0]->value)}}" class="form-control" required="required">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Default:</label>
                            <div class="col-xs-6">
                                <input type=text name="default" id="default"  value="{{$length[0]->default}}" class="form-control">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Title:</label>
                            <div class="col-xs-6">
                                <input type=text name="title" id="title" value="{{$length[0]->title}}"  class="form-control">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="control-label col-xs-3" for="txtmenu">Unit:</label>
                            <div class="col-xs-6">
                                <input type=text name="unit" id="unit" value="{{$length[0]->unit}}" class="form-control">
                            </div>
                        </div>
                        
                       
                     <!--    <div class="form-group row">
                            <label class="control-label col-xs-3" for="menustatus">Status:</label>
                            <div class="col-xs-6">
                                <select name="st_status" id="st_status" class="form-control" required="required">
                                    <option value="" >select</option>
                                    <option value="1">Active</option>
                                    <option value="2">DeActive</option>
                                </select>
                            </div>
                        </div> -->
                       
                        <div class="buttons text-center">
                            <button class="btn btn-danger" type="reset">Reset</button>
                            <button class="btn btn-success" type="submit">Save</button>
                        </div>                   

                </form>
            </div>
        </div>  
    </div>
</div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

              <h4 align="center">{{ Session::get('message') }}</h4>

              <center>

                <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

            </center>
            {{ Session::forget('message') }}
        </div>
    </div>
</div>
</div>
@endif

@stop