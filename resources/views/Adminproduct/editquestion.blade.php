@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/edit_question','class'=>'form-horizontal question-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                          <input type="hidden" name="questionid" value="{{$question[0]->question_id}}">
                          <div class="card">
                            <div class="card-header">
                                <a href="/Admin/question" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Create Product Question</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" id="lang_code" class="form-control">
                                            @foreach($language as $lang)
                                            <option <?php if($lang->lang_code == $question[0]->lang_code){?>selected="selected"<?php }?> value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="name">Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{$question[0]->name}}" name="name" id="name" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="emailid">Email:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="email" value="{{$question[0]->emailid}}" name="emailid" id="emailid" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="product_id">product:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="product_id" id="product_id" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($product as $val)
                                            <option <?php if($val->product_id == $question[0]->product_id){?> selected="selected" <?php }?> value="{{$val->product_id}}">{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                        
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="question">Question:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" value="{{$question[0]->question}}" name="question" id="question" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="answer">Answer:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea cols="10" rows="3" name="answer" id="answer" class="form-control">{{$question[0]->answer}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?php if($question[0]->status  == '1'){ echo "selected";}?>>Enable</option>
                                            <option value="2" <?php if($question[0]->status  == '2'){ echo "selected";}?>>Disable</option>
                                            <option value="3" <?php if($question[0]->status  == '3'){ echo "selected";}?>>Pending</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".question-form").validate({
    rules:
    {
      lang_code : "required",
      name: "required",
      emailid: {
        required: true,
        email: true
      },
      product_id : "required",
      question : "required",
      answer : "required",
      status : "required"
    },
    messages:
    {
      lang_code: "Please enter name",
      name: "Please enter name",
      emailid: "Please enter valid email address",
      product_id: "Please choose product",
      question: "Please enter enter question",
      answer: "Please enter answer",
      status: "Please choose status"
    }
  });
});
</script>
@endsection