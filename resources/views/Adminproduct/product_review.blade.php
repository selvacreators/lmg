@section('admin-style')
<link href="{{ Config::get('app.head_url') }}assets/css/demo.css" rel="stylesheet" />

<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}
  .tab-pane .table tbody > tr > td:first-child {
    width: 95px !important;
}

.form-group row input[type=file] {
  
     position: absolute;
    top: 35px;
    right: 0;
    /* width: 100%; */
    /* height: 100%; */
    /* margin: 0; */
    /* font-size: 23px; */
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 1;
    direction: ltr;
    z-index: 1;
}
</style>
@endsection
@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">
    
      <div class="content">

<div class="container-fluid">
              
<div class="row">
  <span><h3>Add Product Data</h3></span> 
</div>
  <hr> 
  <ul class='nav nav-wizard'>
  
  <li><a href="#`">Product</a></li>

  <li ><a href='#'>Data</a></li>

  <li ><a href='#'>Attribute</a></li>


  <li><a href='#'>Image</a></li>

  <li  class='active'><a href='#'>Review</a></li>

  <li><a href='#'>Option</a></li>
  <li><a href='#'>Filter</a></li>

  <li><a href='#'>Discount</a></li>

  <li><a href='#'>Special</a></li>

  <li><a href='#'>Features</a></li>

</ul>
          
  <div class="col-xs-12">
      <form method="POST" action="{{ URL('Admin/add_ProductReview')}}" enctype="multipart/form-data">
      <input name="_token" type="hidden" value="{{csrf_token()}}">
      <input name="product_id" id="product_id" type="hidden" value="{{$pro_id}}">
        
       <div class="row">
                <div class="col-sm-4">
                  <div class="form-group row">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Name<span class="note">*</span></label>
                  </div>
                  </div>
                  <br>
                  <input type="text" id="name" name="name" class="form-control" required="required">
                </div>
                </div>

         
         <div class="col-sm-4">
                  <div class="form-group row">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Product Name</label>
                  </div>
                  </div>
                  <br> <input type="text" name="text" value="{{$product[0]->product_name}}" id="text" class="form-control" autocomplete="off" readonly="readonly" disabled="disabled" />
                </div>
          </div>
               
      </div> 
    
      <div class="row">
      <div class="col-sm-4">
                  <div class="form-group row">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Rating</label>
                  </div>
                  </div>
                  <br> <div class="col-sm-10">
                      <label class="radio-inline"> <input type="radio" name="rating" value="1" required="required">
                      1
                      </label>
                      <label class="radio-inline"> <input type="radio" name="rating" value="2">
                      2
                      </label>
                      <label class="radio-inline"> <input type="radio" name="rating" value="3">
                      3
                      </label>
                      <label class="radio-inline"> <input type="radio" name="rating" value="4">
                      4
                      </label>
                      <label class="radio-inline"> <input type="radio" name="rating" value="5">
                      5
                      </label>
                      </div>
                </div>
          </div>

        <div class="col-sm-4">
                  <div class="form-group row">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Review Date</label>
                  </div>
                  <input type="text" name="review_date" id="review_date" class="form-control date_available">
                  </div>
          </div>
         </div>
               
      </div>

      <div class="row">
         <div class="col-sm-6">
                  <div class="form-group row">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Text</label>
                  </div>
                  <textarea name="text" id="text" cols="70" class="form-control" rows="5"></textarea>
                  </div>
          </div>
         </div>
      </div>
<div class="row">
         <div class="col-sm-6">
                  <div class="form-group row">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Status</label>
                  </div>
                  <select name="status" id="status" class="form-control" required="required">
                  <option value="">--Selected--</option>
                  <option value="1">Approved</option>
                  <option value="2">Pending</option>
                  <option value="3">Not-Approved</option>
                  </select>
                  </div>
          </div>
         </div>
      </div>

  

  
      <div class="butn pull-right">
      <input type="submit" value="Next"  id="btnadd" class="btn btn-daimler" />
      </div>            

      </form> 
      <!-- Tab panes -->

  </div>
  <div class="clearfix"></div>
  </div>


</div>
</div>

</div>

@stop