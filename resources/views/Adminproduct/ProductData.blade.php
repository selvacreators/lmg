@section('admin-style')
<link href="{{ Config::get('app.head_url') }}assets/css/demo.css" rel="stylesheet" />
<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}
.tab-pane .table tbody > tr > td:first-child {
  width: 95px !important;
}

.form-group row input[type=file] {

 position: absolute;
 top: 35px;
 right: 0;
 /* width: 100%; */
 /* height: 100%; */
 /* margin: 0; */
 /* font-size: 23px; */
 cursor: pointer;
 filter: alpha(opacity=0);
 opacity: 1;
 direction: ltr;
 z-index: 1;
}
</style>
@endsection
@extends('layouts.layout_admin')
@section('content')


<div class="main-panel">

  <div class="inner-content">

    <div class="container-fluid">

      <div class="row">
        <span><h3>Add Product Data</h3></span> 
      </div>
      <hr> 
      <ul class='nav nav-wizard'>

        <li><a href="#`">Product</a></li>

        <li class='active'><a href='#'>Data</a></li>

        <li><a href='#'>Attribute</a></li>


        <li><a href='#'>Image</a></li>

        <li><a href='#'>Review</a></li>
        <li><a href='#'>Option</a></li>

        <li><a href='#'>Filter</a></li>

        <li><a href='#'>Discount</a></li>

        <li><a href='#'>Special</a></li>

        <li><a href='#'>Features</a></li>

      </ul>

      <div class="col-xs-12">
        <form method="POST" action="{{ URL('Admin/add_ProductData')}}" enctype="multipart/form-data">
          <input name="_token" type="hidden" value="{{csrf_token()}}">
          <input name="product_id" id="product_id" type="hidden" value="{{$pro_id}}">

          <div class="row">
            <div class="col-sm-3">
              <div class="form-group row">
                <div class="row">
                  <div class="col-md-6">
                    <label>Language<span class="note">*</span></label>
                  </div>
                </div>
                <br>
                <select name="lang_code" class="form-control" id="lang_code" required="required">
                  <option value="">--Select--</option>
                  @foreach($lnguage as $lng)
                  <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group row">
                <div class="row">

                 <div class="col-md-6">
                   <label>Product Slug<span class="note">*</span></label>
                 </div>
               </div>
               <br>
               <input type="text" name="product_slug"  id="product_slug" class="form-control" autocomplete="off" required="required" />
             </div>
           </div>
           <div class="col-sm-3">
            <div class="form-group row">
              <div class="row">

               <div class="col-md-6">
                 <label>Product Name</label>
               </div>
             </div>
             <br>
             <input type="text" name="name"  id="name" class="form-control" />
           </div>
         </div>
       </div>

       <div class="row">
        <div class="col-sm-3">
          <div class="form-group row">
            <div class="row">

             <div class="col-md-6">
               <label>Meta Iitle<span class="note">*</span></label>
             </div>
           </div>
           <br>
           <input type="text" name="meta_title"  id="meta_title" class="form-control" required="required"/>
         </div>
       </div>
       <div class="col-sm-3">
        <div class="form-group row">
          <div class="row">

           <div class="col-md-6">
             <label>Meta Keyword</label>
           </div>
         </div>
         <br>
         <input type="text" name="meta_keyword"  id="meta_keyword" class="form-control"/>
       </div>
     </div>
     <div class="col-sm-3">
      <div class="form-group row">
        <div class="row">

         <div class="col-md-6">
           <label>Product Tag</label>
         </div>
       </div>
       <br>
       <input type="text" name="tag"  id="tag" class="form-control"/>
     </div>
   </div>


 </div>
 <div class="row">
  <div class="col-sm-3">
    <div class="form-group row">
      <div class="row">

       <div class="col-md-6">
         <label>Product Description</label>
       </div>
     </div>
     <br>
     <textarea cols="10" rows="5" name="desc"  id="desc" class="form-control"></textarea>
   </div>
 </div>
</div>

<div class="row">
  <div class="col-sm-10">
    <div class="form-group row">
      <div class="row">

       <div class="col-md-6">
         <label>Meta Description</label>
       </div>
     </div>
     <br>
     <textarea cols="10" rows="5" name="meta_desc"  id="meta_desc" class="form-control"></textarea>
   </div>
 </div>
</div>

<div class="butn pull-right">
  <input type="submit" value="Next"  id="btnadd" class="btn btn-daimler" />
</div>            

</form> 
<!-- Tab panes -->

</div>
<div class="clearfix"></div>
</div>


</div>
</div>

</div>
@stop
@section('admin-script')
  <script src="{{ Config::get('app.head_url') }}admin/ckeditor/ckeditor.js"></script>
  <script src="{{ Config::get('app.head_url') }}admin/ckeditor/adapters/jquery.js"></script>
  <script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
  CKEDITOR.replace('meta_desc');
  CKEDITOR.config.allowedContent = true;
</script>
@endsection
