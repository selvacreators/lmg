@section('admin-style')
<link href="{{ Config::get('app.head_url') }}assets/css/demo.css" rel="stylesheet" />

<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}
  .tab-pane .table tbody > tr > td:first-child {
    width: 95px !important;
}

.form-group row input[type=file] {
  
     position: absolute;
    top: 35px;
    right: 0;
    /* width: 100%; */
    /* height: 100%; */
    /* margin: 0; */
    /* font-size: 23px; */
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 1;
    direction: ltr;
    z-index: 1;
}
</style>
@endsection
@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">
    
      <div class="content">

<div class="container-fluid">
              
<div class="row">
  <span><h3>Add Product Data</h3></span> 
</div>
  <hr> 
  <ul class='nav nav-wizard'>
  
  <li><a href="#`">Product</a></li>

  <li ><a href='#'>Data</a></li>

  <li class='active'><a href='#'>Attribute</a></li>


  <li><a href='#'>Image</a></li>

  <li><a href='#'>Review</a></li>
  <li><a href='#'>Option</a></li>

  <li><a href='#'>Filter</a></li>

  <li><a href='#'>Discount</a></li>

  <li><a href='#'>Special</a></li>

  <li><a href='#'>Features</a></li>

</ul>
          
  <div class="col-xs-12">
      <form method="POST" action="{{ URL('Admin/add_ProductAttribute')}}" enctype="multipart/form-data">
      <input name="_token" type="hidden" value="{{csrf_token()}}">
      <input name="product_id" id="product_id" type="hidden" value="{{$pro_id}}">
        
       <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Attribute<span class="note">*</span></label>
                  </div>
                  </div>
                  <br>
                  <select name="attribute_id[]" class="form-control" id="attribute_id">
                  <option value="">--Select--</option>
                  @foreach($attribute as $attr)
                  <option value="{{$attr->attribute_id}}">{{$attr->name}}</option>
                  @endforeach
                </select>
                </div>
                </div>

         <div class="col-sm-3">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Language<span class="note">*</span></label>
                  </div>
                  </div>
                  <br>
                  <select name="lang_code[]" class="form-control" id="lang_code" required="required">
                                        <option value="">--Select--</option>
                                        @foreach($lnguage as $lng)
                                        <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                        @endforeach
                                      </select>
                </div>
          </div>
         <div class="col-sm-3">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Text<span class="note">*</span></label>
                  </div>
                  </div>
                  <br> <input type="text" name="text[]"  id="text" class="form-control" autocomplete="off" />
                </div>
          </div>
               
      </div>

      <div class="row multi" id="multi">
           <div class="col-md-10" >
                <div class="col-md-6" >
                <table class="table order-list">
                </table>
                </div>
         </div>
     </div>

     <div class="row" align="center">
           <input type="button" id="addattri" value="Add Attribute" class="btn btn-daimler col-md-2">
      </div>

  
      <div class="butn pull-right">
      <input type="submit" value="Next"  id="btnadd" class="btn btn-daimler" />
      </div>            

      </form> 
      <!-- Tab panes -->

  </div>
  <div class="clearfix"></div>
  </div>


</div>
</div>

</div>
@stop
@section('admin-script')

<script type="text/javascript">
  $(document).ready(function () {
     var counter = 0;



    $("#addattri").on("click", function () {

      var attribute_id = $('#attribute_id').val();


        //if(attribute_id != ''){

          var newRow = $("<tr class='vinrow'>");
          var cols = "";

          cols += ' <td style="border:0px solid #ddd;"><select name="attribute_id[]" class="form-control" id="attribute_id" style="width:220px;"><option value="">--Select--</option>@foreach($attribute as $attr)<option value="{{$attr->attribute_id}}">{{$attr->name}}</option>@endforeach</select></td><td style="border:0px solid #ddd;"></td>';

          cols += '<td style="border:0px solid #ddd;"><select name="lang_code[]" class="form-control" id="lang_code" required="required" style="width: 220px;"><option value="">--Select--</option>@foreach($lnguage as $lng)<option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>@endforeach</select></td>';
          cols += '<td style="border:0px solid #ddd;"><input required type="text" id="" class="form-control odom" name="text[]" style="width: 220px;"></td>';

          cols += '<td style="border:0px solid #ddd;"><input type="button" class="ibtnDel btn btn-md btn-danger" value="-"></td>';
          newRow.append(cols);
          $("table.order-list").append(newRow);
          // $("multi").append(newRow);
          counter++;
       // }
   
var count =0;

      $('.txtecount').each(function(){
        if($(this).val() !=""){
        count++;
        }
      });
    
});


     $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1


        var count =0;

      // $('.txtecount').each(function(){
      //   if($(this).val() !=""){
      //   count++;
      //   }
      // });
      // //alert("No of empty text box="+count);
      // if(count == 0){
      //    $('#selModel').hide();
      //    $('#selModel').attr('disabled',true);
      //     $('#model_name').show();
      //     //$('#model_name').attr('disabled',false);
      // }
      // $('#VinNo').html('Total VIN Entered =' + count );
    });
});

</script>
@endsection
