@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/add_question','class'=>'form-horizontal question-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                          <div class="card">
                            <div class="card-header">
                                <a href="/Admin/question" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Create Product Question</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" id="lang_code" class="form-control">
                                            @foreach($language as $lang)
                                            <option value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="name">Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="emailid">Email:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="email" name="emailid" id="emailid" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="product_id">product:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="product_id" id="product_id" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($product as $val)
                                            <option value="{{$val->product_id}}">{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                        
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="question">Question:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="question" id="question" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="answer">Answer:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea cols="10" rows="3" name="answer" id="answer" class="form-control" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Enable</option>
                                            <option value="2">Disable</option>
                                            <option value="3">Pending</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".question-form").validate({
    rules:
    {
      lang_code : "required",
      name: "required",
      emailid: {
        required: true,
        email: true
      },
      product_id : "required",
      question : "required",
      answer : "required",
      status : "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      name: "Please enter name",
      emailid: "Please enter valid email address",
      product_id: "Please choose product",
      question: "Please enter question",
      answer: "Please enter answer",
      status: "Please choose status"
    }
  });
});
</script>
@endsection