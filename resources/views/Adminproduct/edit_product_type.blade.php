@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/product-type','class'=>'form-horizontal product-type-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                        <input type="hidden" name="ptypeid" value="{{$productType->id}}">
                          <div class="card">
                            <div class="card-header">
                                <a href="/Admin/product-type" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Product Type</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="lang_code">language:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="lang_code" id="lang_code" class="form-control">
                                            <option value="">Select</option>
                                            @foreach($language as $lang)
                                            <option <?php if($lang->lang_code == $productType->lang_code){?>selected="selected"<?php }?> value="{{$lang->lang_code}}">{{$lang->lang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="name">Type Name:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" id="name" value="{{$productType->ptype_name}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="sort_order">Sort Order:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="sort_order" id="sort_order" value="{{$productType->sort_order}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1" <?php if($productType->status  == '1'){ echo "selected";}?>>Enable</option>
                                            <option value="2" <?php if($productType->status  == '2'){ echo "selected";}?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".product-type-form").validate({
    rules:
    {
      lang_code : "required",
      name: "required",
      sort_order: "required",
      status : "required"
    },
    messages:
    {
      lang_code: "Please choose language",
      name: "Please enter type name",
      sort_order: "Please enter sort order",
      status: "Please choose status"
    }
  });
});
</script>
@endsection