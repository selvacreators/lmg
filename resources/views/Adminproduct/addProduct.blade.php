@extends('layouts.layout_admin')

@section('admin-style')

<link href="{{ Config::get('app.head_url') }}assets/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />

<link href="{{ Config::get('app.head_url') }}assets/css/demo.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}admin/plupload/jquery.plupload.queue.css" type="text/css" media="screen">
<style type="text/css">
#ajax-loader{
  position: fixed;
  top: 50px;    
  width: 100%;
  height: 100%;
  background: rgba(0,0,0,0.3);
  z-index: 9999;
  text-align:center;
}
#column-left + div #ajax-loader{
  left:50px;
}
#column-left.active + div #ajax-loader{
  left: 250px;
}
.spinner{
  border: 5px solid #f3f3f3;
  -webkit-animation: spin 1s linear infinite;
  animation: spin 1s linear infinite;
  border-top: 5px solid #555;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  position: absolute;
  top:50%;
  left:40%;
}
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
@endsection
@section('content')
<div class="main-panel">
  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            {!! Form::open(array('url' => '/Admin/add_Product','class'=>'form-horizontal product-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
            <input type="hidden" id="tokens" value="{{csrf_token()}}">
            <div class="card">
              <div class="card-header">
                @if ($errors->product_errors->any())
                {{ implode('', $errors->all('<div>:message</div>')) }}
                  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>  Warning: Please check the form carefully for errors!      
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                  </div>
                 @endif
                <a href="/Admin/product" class="btn btn-primary btn-sm pull-right">Back</a>
                <div class="pull-right"><button id="productsubmit" class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                <h3 class="card-title">Create New Product</h3>
              </div>
              <div class="card-body">
                <nav class="nav-container">
                  <ul class='nav nav-tabs' role="tablist">
                    <li><a data-toggle="tab" class="active show" href="#Product">Product</a></li>
                    <li><a data-toggle="tab" href="#Data">Data</a></li>
                    <li><a data-toggle="tab" href="#Link">Link</a></li>
                    <li><a data-toggle="tab" href="#Attribute">Attribute</a></li>
                    <li><a data-toggle="tab" aria-controls="Image" href="#Image">Image</a></li>
                    <li><a data-toggle="tab" href="#Features">Features</a></li>
                  </ul>
                </nav>
                <div class="tab-content">
                  <!-- Procut Form Start Here -->
                  <div id="Product" class="tab-pane fade active in fieldset">
                    <div id="ajax-loader"  style="display: none;">
                      <div class="spinner"></div>
                    </div>
                    @include('Adminproduct.product_form')
                  </div>
                  <!-- Product Form Tab End-->
                  <!-- Procut Data Start Here -->
                  <div id="Data" class="tab-pane fade fieldset">
                    @include('Adminproduct.data')     
                  </div>
                  <!-- Product Data Tab End-->
                  <!-- Procut Link Start Here -->
                  <div id="Link" class="tab-pane fade fieldset">
                    @include('Adminproduct.link')     
                  </div>
                  <!-- Product Link Tab End-->
                  <!-- Product Attribute start Here-->
                  <div id="Attribute" class="tab-pane fade fieldset">
                   @include('Adminproduct.attribute')                
                  </div>
                 <!-- Product Attribute End Here-->
                 <!-- Procut Image Start Here -->
                 <div id="Image" class="tab-pane fade fieldset">
                   @include('Adminproduct.Image')           
                 </div>
                 <!-- Product Image end Here -->
                 <!-- Product Features start Here -->
                 <div id="Features" class="tab-pane fade fieldset">
                   @include('Adminproduct.features')           
                 </div>
                 <!-- Product Features end Here -->
               </div>
              </div>
            </div>
           {!! Form::close() !!}
           <!-- Tab panes -->
          </div>
        </div>
      </div>
     <div class="clearfix"></div>
    </div>
 </div>
</div>
@endsection

@section('date-script')
<script src="{{ Config::get('app.head_url') }}assets/js/moment.min.js"></script>
<script src="{{ Config::get('app.head_url') }}assets/js/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('admin-script')

<script type="text/javascript">
   $(document).ready(function() {
      md.initFormExtendedDatetimepickers();
    });
</script>
<script>
   var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
</script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/ckeditor.js"></script>
<script src="{{Config::get('app.head_url')}}admin/ckeditor/adapters/jquery.js"></script>
<script src="{{ Config::get('app.head_url') }}admin/ckeditor/common.js"></script>

<script>
  var options = {
    filebrowserImageBrowseUrl: route_prefix + '?type=Images',
    filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: route_prefix + '?type=Files',
    filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
  };
</script>

<script type="text/javascript">
  
CKEDITOR.replace('short_desc', options);
CKEDITOR.replace('desc', options);
CKEDITOR.config.allowedContent = true;

$(document).ready(function()
{
  $('#product_slug').blur(function()
  {
    var getproduct = $(this).val();
    var tokens = $('#tokens').val();
    $.ajax({
      type:'GET',
      url:"{{URL('/Admin/checkslug')}}",
      data:{'_token':tokens,'getproduct':getproduct},
      success:function(res){
        if(res == 1){
          $('#perror').html('Product slug already exists');
          $('#product_slug').focus();
          $('#save').attr("disabled", "disabled");
        }
        else{
          $('#perror').html('');
          $('#save').removeAttr("disabled");
        }
      }
    });
  });

  $('#sku').blur(function(){
    var getsku = $(this).val();
    var tokens = $('#tokens').val();
    $.ajax({
      type:'GET',
      url:"{{URL('/Admin/checksku')}}",
      data:{'_token':tokens,'getsku':getsku},
      success:function(res){
        console.log(res);
        if(res == 1){
          $('#sku-error').html('Wrong sku already exists');
          $('#sku').focus();
          $('#save').attr("disabled", "disabled");
        }
        else{
          $('#sku-error').html('');
          $('#save').removeAttr("disabled");
        }
      }
    });
  });

  // $(".addDimension").click(function() {
  //   rowNum++;
  //   var newRow = $("<div class='form-group row dimensions'>");
    
  //   var cols = '<div class="col-sm-3"></div><div class="col-sm-3"><input type="text" name="width[]" id="width" class="form-control" placeholder="Width"></div><div class="col-sm-3"><input type="text" name="height[]" id="height" class="form-control" placeholder="height"></div><div class="col-sm-3"><button type="button" class="btn btn-sm btn-danger dimenDel"><i class="fa fa-trash"></i></button></div>';
    
  //   newRow.append(cols);
  //   $(".dimension-list").append(newRow);
  // });

  $("#addattri").on("click", function(){
    
    var attribute_id = $('#attribute_id').val();
    var newRow = $("<div class='attribute'>");
    var cols = "";
    cols += '<div class="col-sm-6"><div class="form-group row"><label for="attribute_id" class="control-label col-sm-3">Attribute:</label><div class="col-sm-9"><select name="attribute_id[]" class="form-control" id="attribute_id"><option value="">Select</option>@foreach($productInfo->attribute as $attr)<option value="{{$attr->attribute_id}}">{{$attr->name}}</option>@endforeach</select></div></div></div><div class="col-sm-6"><div class="form-group row"><label for="text" class="control-label col-sm-3">Text:</label><div class="col-sm-9"><textarea cols="10" rows="3" name="text[]" id="text" class="form-control"></textarea></div></div></div><div class="col-md-12"><button type="button" class="ibtnDel btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></div>';
    newRow.append(cols);
    $(".attribute-list").append(newRow);
  });
  var rowNum = 1;
  $("#addfeature").click(function() {
    rowNum++;
    var newRow = $("<div class='features'>");
    var cols = "";
    
    cols += '<div class="form-group row"><label for="heading" class="control-label col-sm-3">Content Heading:</label><div class="col-sm-9"><input type="text" name="heading[]" id="heading" class="form-control" autocomplete="off"/></div></div><div class="form-group row"><label for="Feat_image_or_video" class="col-sm-3 control-label">Feature Images/Videos:</label><div class="col-sm-4"><input type="file" name="Feat_image_or_video[]" id="Feat_image_or_video" class="form-control" size=30"></div></div><div class="form-group row"><label for="Feat_desc" class="control-label col-sm-3">Feature Content:</label><div class="col-sm-9"><textarea cols="10" rows="5" name="Feat_desc[]" id="feat_des_'+rowNum+'" class="form-control"></textarea></div></div><div class="form-group row"><label for="fsort_order" class="control-label col-sm-3">Feature Sort Order:</label><div class="col-sm-9"><input type="text" name="fsort_order[]" id="fsort_order" class="form-control" autocomplete"off"/></div></div><div class="form-group row bmd-form-group"><button type="button" class="btn btn-danger feaDel"><i class="fa fa-trash"></i></button></div>';
    
    newRow.append(cols);
    $(".feature-list").append(newRow);

    CKEDITOR.replace('feat_des_'+rowNum, options);
    CKEDITOR.config.allowedContent = true;
  });
  $(".feature-list").on("click", ".feaDel", function (event) {
    $(this).closest("div.features").remove();
  });
  $("body").on("click", ".imgDel", function (event) {
    $(this).closest("tr").remove();
  });
  $(".attribute-list").on("click", ".ibtnDel", function (event) {
    $(this).closest("div.attribute").remove();
  });
  $(".dimension-list").on("click", ".dimenDel", function (event) {
    $(this).closest("div.dimensions").remove();
  });
  // $("#addimages").on("click", function(){
  //   var newRow = $("<tr class='vinrow'>");
  //   var cols = "";
  //   cols += '<td><input required type="file" id="image" class="form-control odom" name="image[]" style="width: 320px;"></td>';
  //   cols += '<td><input required type="text" id="Imagesort_order" class="form-control odom" name="Imagesort_order[]" style="width: 320px;"></td>';
  //   cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger" value="-"></td>';
  //   newRow.append(cols);
  //   $("table.Imageorder-list").append(newRow);
  // });
  // $("table.Imageorder-list").on("click", ".ibtnDel", function (event) {
  //   $(this).closest("tr").remove();
  // });
});
</script>
<script type="text/javascript" src="{{ Config::get('app.head_url') }}admin/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="{{ Config::get('app.head_url') }}admin/plupload/jquery.plupload.queue/jquery.plupload.queue.min.js"></script>
{{-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script> --}}
<script type="text/javascript">
// Convert divs to queue widgets when the DOM is ready
  $(function(){
    // Highlight any found errors
    /* var danger = $('.text-danger').parent().val();
    if(danger == ''){
    $('.text-danger').each(function() {
      var element = $(this).parent().parent();

      if (element.hasClass('form-group')) {
        element.addClass('has-error');
      }
    });
    } */
    $("#uploader").pluploadQueue({
        // General settings
        runtimes : 'html5,flash,silverlight,html4',
        url : '/Admin/products/uploadImage_ajax?diretorio=large',
        //url : 'http://localhost/completed/sgihair/admin/products/uploadImage_ajax',
        chunk_size : '10mb',
        unique_names : true,
        //resize : {width : 109, height : 73, crop: true},
        
        filters : {
            max_file_size : '10mb',
            mime_types: [
            {title : "Image files", extensions : "jpg,gif,png"}
            ]
        },
        flash_swf_url : '/plupload/js/Moxie.swf',
        silverlight_xap_url : '/plupload/js/Moxie.xap',
        preinit : {
            Init: function(up, info) {
                log('[Init]', 'Info:', info, 'Features:', up.features);
            },
            
            UploadFile: function(up, file) {
                log('[UploadFile]', file);
            }
        },
        init : {
            PostInit: function() {
                log('[PostInit]');
                /*document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };*/
            },
            
            Browse: function(up) {
                // Called when file picker is clicked
                log('[Browse]');
            },
            
            Refresh: function(up) {
                // Called when the position or dimensions of the picker change
                log('[Refresh]');
            },
            
            StateChanged: function(up) {
                // Called when the state of the queue is changed
                log('[StateChanged]', up.state == plupload.STARTED ? "STARTED" : "STOPPED");
            },
            
            QueueChanged: function(up) {

                if(up.state == 1){
                   $('input[type="submit"]').prop('disabled', true);
                   $('.plupload_start').addClass('upload_file');
               }
               else if(up.state == 2){
                   $('input[type="submit"]').prop('disabled', false);
                   $('.plupload_start').removeClass('upload_file');
               }
                // Called when queue is changed by adding or removing files
                log('[QueueChanged]');
            },
            
            OptionChanged: function(up, name, value, oldValue) {
                // Called when one of the configuration options is changed
                log('[OptionChanged]', 'Option Name: ', name, 'Value: ', value, 'Old Value: ', oldValue);
            },
            
            BeforeUpload: function(up, file) {
                console.log(file);
                // Called right before the upload for a given file starts, can be used to cancel it if required
                if(!('thumb' in file)){
                    up.settings.url = '/Admin/products/uploadImage_ajax?diretorio=thumb&newName='+file.id;

                }else{
                    up.settings.url = '/Admin/products/uploadImage_ajax?diretorio=small&newName='+file.id
                }  

                //log('[BeforeUpload]', 'File: ', file);
                console.log(up.settings.resize);
            },
            
            UploadProgress: function(up, file) {
                // Called while file is being uploaded
                log('[UploadProgress]', 'File:', file, "Total:", up.total);
            },
            
            FileFiltered: function(up, file) {
                // Called when file successfully files all the filters
                log('[FileFiltered]', 'File:', file);
            },
            
            FilesAdded: function(up, files) {
                // Called when files are added to queue
                log('[FilesAdded]');
                
                plupload.each(files, function(file) {
                    log('  File:', file);
                });
            },
            
            FilesRemoved: function(up, files) {
                // Called when files are removed from queue
                log('[FilesRemoved]');
                
                plupload.each(files, function(file) {
                    log('  File:', file);
                });
            },
            
            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                if(!('thumb' in file)) {
                    file.thumb = true;
                    file.loaded = 0;
                    file.percent = 0;
                    file.status = plupload.QUEUED;
                    up.trigger("QueueChanged");
                    up.refresh();
                }

                log('[FileUploaded] File:', file, "Info:", info);
            },
            
            ChunkUploaded: function(up, file, info) {
                // Called when file chunk has finished uploading
                log('[ChunkUploaded] File:', file, "Info:", info);
            },
            
            UploadComplete: function(up, files) {
                
                var json_text = '';
                var arr = [];
                
                var htmlData = '';
                $(files).each(function(i, v) {
                    arr[i] = v.target_name;
                    htmlData += '<tr><td><img src="/product/thumb/'+v.target_name+'" class="img-responsive"></td><td><input type="text" style="width:250px" name="Imagesort_order[]" class="form-control" autocomplete="off" value="0" /><input type="hidden" name="ImageList[]" value="'+v.target_name+'" /></td><td><button type="button" class="btn btn-danger imgDel"><i class="fa fa-trash"></i></button></td></tr>';
                });
                json_text = JSON.stringify(arr);
                $("#upload_image").val(json_text);
                $("#img-row table tbody").append(htmlData);

                // Called when all files are either uploaded or failed
                log('[UploadComplete]');
            },
            
            Destroy: function(up) {
                // Called when uploader is destroyed
                log('[Destroy] ');
            },
            
            Error: function(up, args) {
                // Called when error occurs
                log('[Error] ', args);
            }
        }
    });
    function log()
    {
      var str = "";
      
      plupload.each(arguments, function(arg) {
          var row = "";
          
          if (typeof(arg) != "string") {
              plupload.each(arg, function(value, key) {
                      // Convert items in File objects to human readable form
                      if (arg instanceof plupload.File) {
                          // Convert status to human readable
                          switch (value) {
                              case plupload.QUEUED:
                              value = 'QUEUED';
                              break;
                              
                              case plupload.UPLOADING:
                              value = 'UPLOADING';
                              break;
                              
                              case plupload.FAILED:
                              value = 'FAILED';
                              break;
                              
                              case plupload.DONE:
                              value = 'DONE';
                              break;
                          }
                      }
                      
                      if (typeof(value) != "function") {
                          row += (row ? ', ' : '') + key + '=' + value;
                      }
                  });
              
              str += row + " ";
          } else {
              str += arg + " ";
          }
      });
      var log = $('#log');
      log.append(str + "\n");
      //log.scrollTop(log[0].scrollHeight);
    }
    $("#language").change(function()
    {
      var lang = $(this).val(), ajxLoader = $("#ajax-loader");
      $.ajax({
        url: '/Admin/get-product-info?lang='+lang,
        type: 'get',
        dataType: 'json',
        beforeSend: function(){ ajxLoader.show(); },
        success:function(result)
        {
          //#stock_status
          $('#rproduct,#oproduct,#brand,#collection_id,#categories_id,#weight_class_id,#length_class_id,#store_id,#product_type,#attribute_id').empty();
          $('#brand,#collection_id,#product_type,#attribute_id').append('<option value="">Select</option>');
          $(".attribute-list").html('');

          // $.each( result.stock_status,function( key, value )
          // {
          //   $('#stock_status').append('<option value="'+ value.stock_status_id +'">'+ value.name +'</option>');
          // });
          $.each( result.product_type,function( key, value )
          {
            $('#product_type').append('<option value="'+ value.id +'">'+ value.ptype_name +'</option>');
          });
          $.each( result.getweight,function( key, value )
          {
            $('#weight_class_id').append('<option value="'+ value.weight_class_id +'">'+ value.title +'</option>');
          });
          $.each( result.getlength,function( key, value )
          {
            $('#length_class_id').append('<option value="'+ value.length_class_id +'">'+ value.title +'</option>');
          });
          $.each( result.store_location,function( key, value )
          {
            $('#store_id').append('<option value="'+ value.store_id +'">'+ value.store_name +'</option>');
          });
          $.each( result.brands,function( key, value )
          {
            $('#brand').append('<option value="'+ value.brand_id +'">'+ value.brand_name +'</option>');
          });
          $.each( result.collection,function( key, value )
          {
            $('#collection_id').append('<option value="'+ value.collection_id +'">'+ value.collection_name +'</option>');
          });
          $.each( result.categories,function( key, value )
          {
            $('#categories_id').append('<option value="'+ value.categories_id +'">'+ value.category_name +'</option>');
          });
          $.each( result.ProductDesc,function( key, value )
          {
            $('#rproduct,#oproduct').append('<option value="'+ value.product_id +'">'+ value.name +'</option>');
          });
        },
        complete:function(){ ajxLoader.hide(); }
      });
    });
  });
</script>
@endsection