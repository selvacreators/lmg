<div class="form-group row">
    <label for="Image" class="control-label col-sm-3">Image:</label>
    <div class="col-sm-9">
        <input type="file" name="Product_image" id="Product_image" class="form-control">
    </div>
</div>
<div class="form-group row required">
    <label for="sku" class="col-sm-3 control-label">Sku:<span class="note">*</span></label>
    <div class="col-sm-9">
        <input type="text" class="form-control" value="{{ old('sku') }}" name="sku" id="sku"/>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('sku').'</div>';?>
        <div id="sku-error"></div>
    </div>
</div>
<div class="form-group row">
    <label for="quantity" class="col-sm-3 control-label">Quantity:</label>
    <div class="col-sm-9">
        <input type="text" value="{{ old('quantity') }}" class="form-control" name="quantity" id="quantity" value="0">
    </div>
</div>
<!-- <div class="form-group row">
    <label for="stock_status" class="control-label col-sm-3">Stock Status:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="stock_status" class="form-control" id="stock_status">
            <option value="">Select</option>
            @if($productInfo->stock_status)
            @foreach($productInfo->stock_status as $ststatus)
            <option {{ old('stock_status') == $ststatus->stock_status_id ? 'selected="selected"' : '' }} value="{{$ststatus->stock_status_id}}">{{$ststatus->name}}</option>
            @endforeach
            @endif
        </select>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('stock_status').'</div>';?>
    </div>
</div> -->
<div class="form-group row">
    <label for="price" class="control-label col-sm-3">Price:</label>
    <div class="col-sm-9">
        <input type="text" value="{{ old('price') }}" name="price" id="price" class="form-control" autocomplete="off" value="0" />
    </div>
</div>
<div class="form-group row">
    <label for="date_available" class="control-label col-sm-3">Date Available:</label>
    <div class="col-sm-9">
        <input type="text" value="{{ old('date_available') }}" name="date_available" id="date_available" class="form-control datepicker" value="" data-date-format="YYYY-MM-DD">
    </div>
</div>
<div class="form-group row required">
    <label for="product_type" class="control-label col-sm-3">Product Type:<span class="note">*</span></label>
    <div class="col-sm-9">
        <select name="product_type" class="form-control" id="product_type">
            <option value="">Select</option>
            @if($productInfo->product_type)
            @foreach($productInfo->product_type as $row)
            <option {{ old('product_type') == $row->id ? 'selected="selected"' : '' }} value="{{$row->id}}">{{$row->ptype_name}}</option>
            @endforeach
            @endif
        </select>
        <?php echo '<div class="text-danger">'.$errors->product_errors->first('product_type').'</div>';?>
    </div>
</div>
<div class="form-group row">
    <label for="weight_class_id" class="col-sm-3 col-form-label">Weight:</label>
    <div class="col-sm-3">
        <select name="weight_class_id" class="form-control" id="weight_class_id">
            <option value="">Select</option>
            @if($productInfo->getweight)
                @foreach($productInfo->getweight as $weight)
                @if($weight->default == 1)
                    <option {{ old('weight_class_id') == $weight->weight_class_id ? 'selected="selected"' : '' }} value="{{$weight->weight_class_id}}" selected="selected">{{$weight->title}}</option>
                @else
                    <option {{ old('weight_class_id') == $weight->weight_class_id ? 'selected="selected"' : '' }} value="{{$weight->weight_class_id}}">{{$weight->title}}</option>
                @endif
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-3">
        <input type="text" value="{{ old('weight') }}" name="weight" id="weight" class="form-control" placeholder="weight">
    </div>
</div>
<div class="form-group row">
    <label for="length_class_id" class="col-sm-3 col-form-label">Length:</label>
    <div class="col-sm-3">
        <select name="length_class_id" class="form-control" id="length_class_id">
            <option value="">Select</option>
            @if($productInfo->getlength)
                @foreach($productInfo->getlength as $lengt)
                    @if($lengt->default == 1)
                        <option {{ old('length_class_id') == $lengt->length_class_id ? 'selected="selected"' : '' }} value="{{$lengt->length_class_id}}" selected="selected">{{$lengt->title}}</option>
                    @else
                        <option {{ old('length_class_id') == $lengt->length_class_id ? 'selected="selected"' : '' }} value="{{$lengt->length_class_id}}">{{$lengt->title}}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="width" class="col-sm-3 col-form-label"></label>
    <div class="col-sm-3">
        <input type="text" value="{{ old('length') }}" name="length" id="length" class="form-control" placeholder="length">
    </div>
    <div class="col-sm-3">
        <input type="text" name="width" value="{{ old('width') }}" id="width" class="form-control" placeholder="Width">
    </div>
    <div class="col-sm-3">
        <input type="text" name="height" value="{{ old('height') }}" id="height" class="form-control" placeholder="height">
    </div>
</div>
<div class="form-group row">
    <label for="comfort" class="control-label col-sm-3">Comfort:</label>
    <div class="col-sm-9">
        <select name="comfort" class="form-control" id="comfort">
          <option value="">Select</option>
          <option {{ old('comfort') == 'firm' ? 'selected="selected"' : '' }} value="firm">Firm</option>
          <option {{ old('comfort') == 'plush' ? 'selected="selected"' : '' }} value="plush">Plush</option>
          <option {{ old('comfort') == 'medium' ? 'selected="selected"' : '' }} value="medium">Medium</option>
      </select>
  </div>
</div>
<div class="form-group row">
<label for="subtract" class="control-label col-sm-3">Subtract:</label>
<div class="col-sm-9">
    <select name="subtract" class="form-control" id="subtract">
        <option value="">Select</option>
        <option {{ old('subtract') == '1' ? 'selected="selected"' : '' }} value="1">1</option>
        <option {{ old('subtract') == '2' ? 'selected="selected"' : '' }} value="2">2</option>
    </select>
</div>
</div>
<div class="form-group row">
<label for="sort_order" class="control-label col-sm-3">Sort Order:</label>
<div class="col-sm-9">
    <input type="text" name="sort_order" value="{{ old('sort_order') }}" id="sort_order" class="form-control">
</div>
</div>
<div class="form-group row required">
<label for="status" class="control-label col-sm-3">Status:<span class="note">*</span></label>
<div class="col-sm-9">
    <select name="status" class="form-control" id="status">
        <option value="">Select</option>
        <option {{ old('status') == '1' ? 'selected="selected"' : '' }} value="1">Enable</option>
        <option {{ old('status') == '2' ? 'selected="selected"' : '' }} value="2">Disable</option>
    </select>
    <?php echo '<div class="text-danger">'.$errors->product_errors->first('status').'</div>';?>
</div>
</div>