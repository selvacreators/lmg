@extends('layouts.layout_home') @section('style')
<link rel="stylesheet" type="text/css" 
href="{{Config::get('app.head_url')}}frontend/css/blog.css">
<!-- <link rel="stylesheet" type="text/css" 
href="{{Config::get('app.head_url')}}frontend/css/bootsnav.css">
-->
<style type="text/css" media="screen">
.pagination{
  width: 20%;
}
.blog-image.clip
{
  position:relative;
  width:100%;
  overflow:hidden;
}

img#clipped
{
  position:absolute;  
  object-fit: cover;
  width: 100%;
  
}
.row div[class*='-6'] .blog-image.clip{  
  height:250px;
}
.row div[class*='-4'] .blog-image.clip{
  height:220px;
}
.row div[class*='-6'] .blog-image.clip #clipped{
  height: 300px;
  clip: rect(0px, 560px, 250px, 0px);
}
.row div[class*='-4'] .blog-image.clip #clipped{
 height: 300px;
  clip: rect(0px, 360px, 220px, 0px);
}
</style>  
@endsection @section('content') @include('includes.header') @include('includes.breadcrumb')

<div class="page-title-wrapper">
  <div class="parallax">
    <div class="background-image">
      <img src="{{Config::get('app.head_url')}}images/blog/{{$blog_set[0]->blog_banner}}">
    </div>

    <div class="clearfix"></div>

    <div class="container">
      <div class="row">
        <div class="text-center">
          <div class="content-heading">
            <h1 class="title">{{$blog_set[0]->blog_tittle}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<main class="content-wrapper">
  <section class="page-content pt40">
    <div class="blog-menu" id="masthead">
      <div class="container">
        <div class="row">
          <div class="blog-menu">
            <nav role="navigation">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand" href="#">Blog Categories</div>
              </div>

              <div class="container">
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                  @if(sizeof($categories_menu))

                  <ul class="nav navbar-nav">
                    @foreach($categories_menu as $cm)
                    <li class="{{ (isset($cm['child']) && sizeof($cm['child'])) ? 'dropdown': null }}" >
                      <a href="{{ URL($category_path.'/'.$cm['slug'])}}/" class="{{ (isset($cm['child']) && sizeof($cm['child'])) ? 'dropdown-toggle': null }}">{{$cm['name']}}</a>
                      @if(isset($cm['child']) && sizeof($cm['child']))
                      <span class="caret"></span>
                      @endif
                      @if( isset($cm['child']) && sizeof($cm['child']) )
                      <ul class="dropdown-menu">
                        @foreach($cm['child'] as $ch)
                      <!--   <li class="kopie"><a href="{{ URL($category_path.'/'.$cm['slug'].'/'.$ch['slug'])}}/">{{$ch['name']}}</a>
                        </li> -->
                         <li class="kopie"><a href="{{ URL($category_path.'/'.$ch['slug'])}}/">{{$ch['name']}}</a>
                        </li>
                        @endforeach
                      </ul>
                      @endif

                    </li>
                    @endforeach
                  </ul>

                  @endif
                </div>

              </div>
            </nav>
          </div>
        </div>

      </div>
    </div>
    @if(!empty($blog_post)) 
    <div class="blog-main">
      <div class="container">
        <div class="owl-carousel">
         @foreach($blog_post as $key1 => $val1)
         @if($val1->featured_article == 1)
         <div class="item">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <a href="{{$val1->post_slug}}">
            <img src="{{Config::get('app.head_url')}}images/blog/{{$val1->blog_banner}}" />
          </a>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 blog-main-content">
            <h3>{{$val1->post_title}}</h1>
            <!-- <h1>{{$val1->post_type}}</h1> -->
            <p>
              <?php echo html_entity_decode($val1->short_desc); ?>
            </p>
          </div>
        </div>
        @endif
        @endforeach
      </div>


    </div>
  </div>
  @endif
  <!--Blog Main-->

  <!--Blog main below-->
  <section class="blog-main-blw">
    <div class="container">
      @if(!empty($select_categories_data)) 

      @foreach($select_categories_data as $key1 => $val)

      <div id="page{{$key1}}" class="page{{$key1}}">

        <div class="col-md-6 col-sm-6">
          <div class="blog-blw-bx">
            <a href="{{ URL($path.'/'.$val->post_slug) }}/"><img src="{{Config::get('app.head_url')}}images/blog/{{$val->blog_banner}}" /></a>
            <div class="blog-blw-bx-down">

              <!-- <h3>{{$val->post_title}}</h3> -->
                <h5><a href="{{ URL($path.'/'.$val->post_slug)}}">{{$val->post_title}}</a></h5>
              <!-- <h1>{{$val->post_type}}</h1> -->
              <p>
                <?php echo html_entity_decode($val->post_content); ?>
              </p>
            </div>
          </div>
        </div>

      </div>
      @endforeach 
      @endif
      <!--Pagination-->
      <div class="blog-pagination">
        <ul id="pagination-demo" class="pagination-lg"></ul>
      </div>
      <!--Pagination-->

    </div>
  </section>

  <div align="center">
    <?php echo $select_categories_data->render(); ?>
  </div>
</section>

</main>
  @include('includes.newsletter-form')
@include('includes.footer') @endsection @section('script')
<script src="{{Config::get('app.head_url')}}js/jquery.twbsPagination.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

    $('.blog-menu .navbar-nav li.dropdown>.caret, .dropdown-submenu>.caret').on('click', function () {
      $(this).closest('.dropdown').siblings().removeClass('open');
      $(this).closest('.dropdown').toggleClass('open');

      return false;            
    });

  });
</script>
@endsection