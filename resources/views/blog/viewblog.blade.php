@extends('layouts.layout_home') @section('style')

<link rel="stylesheet" type="text/css" 
href="{{Config::get('app.head_url')}}frontend/css/blog.css">
<!-- <link rel="stylesheet" type="text/css" 
href="{{Config::get('app.head_url')}}frontend/css/bootsnav.css">
-->
<style type="text/css" media="screen">
  .pagination{
    width: 20%;
  }
  .post-image.clip
  {
    position:relative;
    width:100%;
    overflow:hidden;
  }
  img#clipped
  {
    position:absolute;  
    object-fit: cover;
    width: 100%;
  }
  .row div[class*='-6'] .post-image.clip{  
    height:250px;
  }
  .row div[class*='-4'] .post-image.clip{
    height:220px;
  }
  .row div[class*='-6'] .post-image.clip #clipped{
    height: 300px;
    clip: rect(0px, 560px, 250px, 0px);
  }
  .row div[class*='-4'] .post-image.clip #clipped{
   height: 300px;
    clip: rect(0px, 360px, 220px, 0px);
  }
</style> 
@endsection @section('content') @include('includes.header') @include('includes.breadcrumb')
<div class="page-title-wrapper subblog-title-wrapper">
  <div class="parallax">
    <div class="background-image">
      <img src="{{Config::get('app.head_url')}}images/blog/{{$blog_setting->blog_banner}}">
    </div>
    <div class="clearfix"></div>
    <div class="container">
      <div class="row">
        <div class="text-center">
          <div class="content-heading">
            <h1 class="title">{{$blog_setting->blog_tittle}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<main class="content-wrapper">
  <section class="page-content pt40">
    @include('includes.blogMenu')
    @if(!empty($feature_posts) && !empty($blog_setting->featu_article_perpage) && $blog_setting->no_slider_show > 0)
    <div class="blog-main">
      <div class="container">
        <div class="owl-carousel">
          <?php $i=1; $bFeatid = json_decode($blog_setting->featu_article_perpage);
          foreach ($bFeatid as $key => $row) { $blog['bFeatid'][$key] = $row; }?>
          @foreach($feature_posts as $key1 => $val1)
            @if($val1->featured_article == 1 && in_array($val1->post_id, $blog['bFeatid']))
              <div class="item">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="post-image">
                    <a href="{{ URL(Session::get('locale'), ['blog', $val1->post_slug])}}.html"><img src="{{Config::get('app.head_url')}}images/blog/{{$val1->blog_banner}}"/></a>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 blog-main-content">
                  <a href="{{ URL(Session::get('locale'), ['blog', $val1->post_slug])}}.html"><h3>{{$val1->post_title}}</h3></a>
                  <p><?php echo html_entity_decode($val1->short_desc);?></p>
                </div>
              </div>
              <?php if ($i++ == $blog_setting->no_slider_show){ break; }?>
            @endif
          @endforeach
        </div>
      </div>
    </div>
    @endif
  <!--Blog Main-->
</section>
<!--Blog main below-->
<section class="blog-main-blw">
  <div class="container">
    @if(!empty($blog_post_paginate))
      <div id="blog_load" class="" style="position: relative;">
      <div class="eq_height row blog-container">
        <?php $num = 0; $category_slug = ''; $category_name = '';?>
          @foreach($blogList as $key => $post)
            <div class="col-md-6 col-sm-6">
              <div class="blog-blw-bx item">
                <div class="post-image clip">
                  <a href="{{ URL(Session::get('locale'), ['blog', $post->post_slug])}}.html"><img src="{{Config::get('app.head_url')}}images/blog/{{$post->blog_banner}}" class="post_img" id="clipped"/></a>
                </div>
                <div class="blog-blw-bx-down">
                  <?php if( isset($post->category_data) && sizeof($post->category_data)): ?>
                  <div class="category-name">
                    <h2 class="h4">
                    <?php
                      $cat_reset = reset($post->category_data);
                      $category_slug = $cat_reset['category_slug'];
                      $category_name = $cat_reset['category_name'];

                      if($category_slug != $currentPath && $currentPath != 'blog')
                      {
                        $category_slug = $currentPath;
                        $category_name = ucwords( str_replace('-', ' ', $currentPath));
                      }
                    ?>
                    <a href="{{ URL( Session::get('locale') , ['blog', $category_slug] )}}/">{{$category_name}}</a>
                    </h2>
                  </div>
                  <?php endif; ?>

                  <h3><a href="{{ URL(Session::get('locale'), ['blog',$category_slug, $post->post_slug])}}.html">{{$post->post_title}}</a></h3>
                  <p><?php echo html_entity_decode($post->short_desc);?></p>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        {!! $blogList->render() !!}
      </div>
    @endif
  </div>
</section>
</main>
@include('includes.newsletter-form')
@include('includes.footer') @endsection @section('script')
<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.matchHeight.min.js"></script>
<!-- <script src="{{Config::get('app.head_url')}}js/jquery.twbsPagination.js"></script>
<script src="{{Config::get('app.head_url')}}js/custom.js"></script> -->
<script type="text/javascript">
  $(document).ready(function(){
    $('.blog-container.eq_height .item').matchHeight();
    $('.blog-nav li.dropdown>.caret, .dropdown-submenu>.caret').on('click', function () {
      $(this).closest('.dropdown').siblings().removeClass('open');
      $(this).closest('.dropdown').toggleClass('open');
      return false;
    });
  });
</script>

<script type="text/javascript">

        $(function() {
            $('body').on('click', '.pagination a', function(e) {
                e.preventDefault();

                //$('#blog_load a').css('color', '#dfecf6');
                $('#blog_load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

                var url = $(this).attr('href');
                getArticles(url);
                window.history.pushState("", "", url);
            });

            function getArticles(url) {
                $.ajax({
                    url : url
                }).done(function (data) {
                    $('.articles').html(data);
                }).fail(function () {
                    alert('Articles could not be loaded.');
                });
            }
        });
    </script>
@endsection