@extends('layouts.layout_home') @section('style')

<style type="text/css" media="screen">
.page-title-wrapper>.parallax{
  padding-bottom: 0px;
}
.page-title-wrapper>.parallax > .container{
  margin: auto auto 0;
}
.bg-white-trans{background: rgba(255,255,255,0.5);}
.pagination{
  width: 20%;
}
.post-image.clip
{
  position:relative;
  width:100%;
  overflow:hidden;
}

img#clipped
{
  position:absolute;  
  object-fit: cover;
  width: 100%;
  
}

.row div[class*='-4'] .post-image.clip{
  height:200px;
}

.row div[class*='-4'] .post-image.clip #clipped{
     height: 200px;
    clip: rect(0px, 360px, 200px, 0px);
}
.category-name {
    display: inline-block;
    background: #2c5d75;
    color: #fff;
    padding: 6px 15px;
    border-radius: 20px;
    position: relative;
    top: -20px;
    min-width: 100px;
}
.category-name span {
    letter-spacing: 4px;
}
</style>   
@endsection 
@section('content') 
@include('includes.header') 
@include('includes.breadcrumb')

<div class="page-title-wrapper detailblog-pagetitle-wrapper">
  <div class="parallax">
    <div class="background-image">
      <img src="{{Config::get('app.head_url')}}images/blog/{{$blog_post->blog_banner}}">
    </div>

    <div class="clearfix"></div>

    <div class="container">
      <div class="row bg-white-trans" >
        <div class="text-center">
          <div class="category-name">              
              <span class="h5">{{ ucfirst( str_replace('-', ' ', explode('/',$path)[2])) }}</span>
          </div>
          <div class="content-heading">            
            <h1 class="title mt0 mb0">{{$blog_post->post_title}}</h1>
          </div>
        </div>
        
      </div>
    </div>

  </div>
</div>

<main class="content-wrapper">
  <section class="page-content pt40">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
            <!-- AddThis Button START -->
              <div class="share_this">
               <div class="share_buttons">
                  <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                      <a class="addthis_button_preferred_1"></a>
                      <a class="addthis_button_preferred_2"></a>
                      <a class="addthis_button_preferred_3"></a>
                      <a class="addthis_button_preferred_4"></a>
                      <a class="addthis_button_compact"></a>
                      <a class="addthis_counter addthis_bubble_style"></a>

                  </div>
                </div>
              </div>
              <div class="post-content">
                <?php echo html_entity_decode($blog_post->post_content); ?>  
              </div>
           </div>
           <aside class="col-md-3 category-sidebar">
            <div class="title">
               <h6>VIEW STORIES BY TOPICS</h6>
             </div>
             <nav>
              @if(sizeof($categories_menu))
                <ul class="nav category-nav collapsible-menu">
                  @foreach($categories_menu as $cm)
                    <li class="has-children">
                      <a href="{{ URL(Session::get('locale'), ['blog',$cm['slug']] )}}/" class="pl10">
                        <strong>{{$cm['name']}}</strong>
                      </a> 
                      @if(isset($cm['child']) && sizeof($cm['child']))
                      <span class="arrow"></span>
                      @endif

                      @if( isset($cm['child']) && sizeof($cm['child']) )
                      <ul class="nav nav-pills nav-stacked">
                        @foreach($cm['child'] as $ch)
                        <li>
                          <a href="{{ URL(Session::get('locale'), ['blog',$ch['slug']])}}/">
                          <i class="ion-plus-round"></i>
                          <span class="pl10">{{$ch['name']}}</span>
                        </a>
                        </li>
                        @endforeach
                      </ul>
                      @endif
                    </li>
                  @endforeach
                </ul>
              @endif
             </nav>
             @if($promo_banner)
              @foreach($promo_banner as $key => $banner)
                @if($banner->banner_show_page == 'Blog_list')
                  @include('includes.promobanner')
                @endif
              @endforeach
             @endif 
           </aside>
      </div>
    </div>
    
    <?php if(sizeof($blog_realtedpost)): ?>
    <div class="container">
      <div class="row">
        <div class="related-posts pt40 pb40 mb40 mt40">
          <div class="content-heading text-center">
            <h2 class="title">Related Posts</h2>
          </div>
          <?php $i=0; foreach($blog_realtedpost as $image): ?>
            <div class="col-md-4">
              <div class="image">
                  <div class="post-image clip">
                    <img src="{{Config::get('app.head_url')}}images/blog/{{$image->blog_banner}}" class="img-responsive" id="clipped">
                  </div>
              </div>
              <div class="cat-details">
                <div class="cat-name text-center mt20">
                  <?php
                  $category_slug = '';
                  $category_name = '';
                  if(isset($image->category_data) && sizeof($image->category_data)):
                    $cat_reset = reset($image->category_data);
                    $category_slug = $cat_reset['category_slug'];
                    $category_name = $cat_reset['category_name'];
                  endif;
                  ?>
                <span class="h5">
                <a href="{{ URL( Session::get('locale') , ['blog', $category_slug] )}}/">{{ $category_name }}</a>
              </span>
                  <span class="pl10 pr10">|</span>
                <time class="h5">
                  <?php echo date('M d',strtotime($image->publish_date)); ?>
                </time>
                </div>
                <div class="clearfix"></div>
                <div class="post-title text-center">
                  <h3 class="h5">
                    <a href="{{ URL(Session::get('locale'), ['blog',$category_slug, $image->post_slug])}}.html">
                      {{$image->post_title}}
                    </a>
                    </h3>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <?php endif; ?>
  </section>
</main>

  @include('includes.newsletter-form')
@include('includes.footer') @endsection @section('script')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
       
        $(".collapsible-menu").on("click", "li > .arrow", function(e) {
        var p = $(this).parent();
         $(".has-children").find('.nav-pills.nav-stacked').removeClass('hide');
        if(p.hasClass("has-children")) p.toggleClass("open").siblings(".open").removeClass("open");
        $(".has-children.open").find('.nav-pills.nav-stacked').addClass('hide');
        //p.find('.nav-pills.nav-stacked').removeClass('hide');
      });

    $(".breadcrumbs").css('background','rgba(255,255,255,0.4)');                              

        
    });
    </script>
@endsection