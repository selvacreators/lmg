<div id="blog-container" class="">
  @foreach($blogList as $key => $post)
            <div class="col-md-6 col-sm-6">
              <div class="blog-blw-bx item">
                <div class="post-image clip">
                  <a href="{{ URL(Session::get('locale'), ['blog', $post->post_slug])}}.html"><img src="{{Config::get('app.head_url')}}images/blog/{{$post->blog_banner}}" class="post_img" id="clipped"/></a>
                </div>
                <div class="blog-blw-bx-down">
                  <?php if( isset($post->category_data) && sizeof($post->category_data)): ?>
                  <div class="category-name">
                    <h2 class="h4">
                    <?php
                      $cat_reset = reset($post->category_data);
                      $category_slug = $cat_reset['category_slug'];
                      $category_name = $cat_reset['category_name'];

                      if($category_slug != $currentPath && $currentPath != 'blog')
                      {
                        $category_slug = $currentPath;
                        $category_name = ucwords( str_replace('-', ' ', $currentPath));
                      }
                    ?>
                    <a href="{{ URL( Session::get('locale') , ['blog', $category_slug] )}}/">{{$category_name}}</a>
                    </h2>
                  </div>
                  <?php endif; ?>

                  <h3><a href="{{ URL(Session::get('locale'), ['blog',$category_slug, $post->post_slug])}}.html">{{$post->post_title}}</a></h3>
                  <p><?php echo html_entity_decode($post->short_desc);?></p>
                </div>
              </div>
            </div>
          @endforeach
</div>