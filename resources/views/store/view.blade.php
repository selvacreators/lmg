<?php if(isset($apikey))
$apikey = $apikey->store_api;?>
@extends('layouts.layout_home')
@section('style')
<link rel="stylesheet" type="text/css" href="{{Config::get('app.head_url')}}storelocator/storelocator.css">

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $apikey;?>&libraries=places"></script>

<style type="text/css">
#panel {
    width: 300px;
    height:600px;
    float: left;
    margin-right: 10px;
}
#map-canvas,  #store-list-map {
    height: 600px;
}
</style>
@endsection

@section('content')

@include('includes.header')

<div class="breadcrumbs">
    <div class="container-fluid">
        <ul class="items">
            <li class="item home">
                <a href="{{URL('/')}}" title="Go to Home Page">
                Home </a>
            </li>
            <li class="item cms_page"><strong>{{$currentPath}}</strong></li>

        </ul>
    </div>
</div>
<div class="page-title-wrapper">
    <div class="container">
      <div class="row">
        <div class="text-center">
          <div class="content-heading">
            <h1 class="title">{{$currentPath}}</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
<main class="content-wrapper">
  <div class="page-content">
    <div class="container">
        <div class="row">
            <?php $markers = array(); ?>
            <?php $totalStoreCount = count($StoreCollection); ?>
            <div class="store-list-container pb40">
                <div class="col-md-4">
                    <div class="store-list">
                        <div class="block-title">
                            <h3 class="title"><span><?php echo "LMG Store List"; ?></span></h3>
                        </div>
                        <div id="panel">
                            <div class="items">
                                <?php if (!$totalStoreCount): ?>
                                <div class="alert alert-warning"><?php echo 'There are no stores.' ?></div>
                                <?php else: ?>
                                <ul class="stores">
                                    <?php $i = 0;
                                        foreach ($StoreCollection as $store): $i++; ?>
                                        <?php $markers[$i]['name'] = '<div class="map-short-info"><h3>' . $store->store_name . '</h3><p>'; ?>

                                        <?php $markers[$i]['lat'] = $store->store_longitude; ?>
                                        <?php $markers[$i]['long'] = $store->store_latitude; ?>
                                        <li>
                                            <div class="store-infor">
                                                <div class="store-content">
                                                    <a class="store-logo" title="<?php echo $store->store_name;?>" href="#"></a>
                                                    <div class="description">
                                                        <h4 class="h5"><?php echo $store->store_name;?></h4>
                                                        <p>
                                                            <?php echo $store->store_address; ?>,<br>
                                                            <?php echo $store->store_city; ?>,
                                                            <?php echo $store->store_state; ?>,
                                                            <?php echo $store->store_country; ?> - 
                                                            <?php echo $store->store_pincode; ?>
                                                            <?php $markers[$i]['name'] .= $store->store_address; ?>
                                                            <?php $markers[$i]['name'] .= ' ' . $store->store_city; ?>
                                                            <?php $markers[$i]['name'] .= ' ' . $store->store_state; ?>
                                                            <?php $markers[$i]['name'] .= ' ' . $store->store_pincode; ?>
                                                            <?php $markers[$i]['name'] .= ' ' . $store->store_country; ?>
                                                        </p>

                                                        <?php $markers[$i]['name'] .= '</p>'; ?>

                                                        <?php if ($store->store_phone): ?>
                                                        <p><?php echo 'Phone: '. $store->store_phone ?></p>

                                                        <?php $markers[$i]['name'] .= '<p>' . 'Phone: ' . '<a href="tel:' . $store->store_phone . '">' . $store->store_phone . '</a></p>'; ?>
                                                        <?php endif; ?>

                                                        <?php if ($store->store_email): ?>
                                                        <p><?php echo 'Email: '. $store->store_email ?></p>

                                                        <?php $markers[$i]['name'] .= '<p>' . 'Email: ' . '<a href="mailto:' . $store->store_email . '">' . $store->store_phone . '</a></p>'; ?>
                                                        <?php endif; ?>

                                                        <?php /* $markers[$i]['name'] .= '<p><a href="">' . 'Details' . '</a></p>'; */ ?>

                                                        <?php $markers[$i]['name'] .= '</div>'; ?>
                                                        
                                                        <?php $storeName = $store->store_name; ?>
                                                        <?php $storeLat = $store->store_latitude; ?>
                                                        <?php $storeLong = $store->store_longitude; ?>  
                                                        <?php $storeRadius = $store->radius; ?>
                                                        <?php $storeZoomLevel = $store->zoom; ?>

                                                        <?php if ($storeLat && $storeLong): ?>
                                                        <button onclick="initializeMap(<?php echo $storeLat; ?>, <?php echo $storeLong; ?>, <?php echo $storeRadius; ?>,'<?php echo str_replace('"', '-quotation-', $markers[$i]['name']); ?>', 'store-list-map');" class="action btn btn-primary" title="<?php echo 'View Map' ?>" type="button">
                                                        <span><?php echo 'View on Map';?></span>
                                                    </button>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
              
                    <?php if ($totalStoreCount): ?>
                    <div>
                        <div id="store-list-map">&nbsp;</div>
                    </div>
                    <?php if (is_array($markers) && count($markers) > 0) : ?>
                    <script>
                        //<![CDATA[
                        var markers = [
                        <?php foreach ($markers as $marker) : ?>
                        ['<?php echo $marker['name'];?>', <?php echo $marker['lat'];?>, <?php echo $marker['long'];?>],
                        <?php endforeach; ?>
                        
                        ];
                            //]]>
                    </script>
                    <?php endif; ?>
                        <div id="show-all-store" class="text-right padding-top20">
                            <button onclick="drawMap(markers, googleMapDivId);" class="btn btn-lg btn-secondary"
                            title="<?php echo 'Show All Stores' ?>" type="button">
                                <span>
                                    <?php echo 'Show All Stores' ?>
                                </span>
                            </button>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  </div>
</main>

@include('includes.newsletter-form')
@include('includes.footer')
@section('script')

<script src="{{Config::get('app.head_url')}}storelocator/storelocator.js"></script>

<script type="text/javascript">


    var googleMapDivId = 'store-list-map';
    var storeLat = <?php echo $storeLat; ?>;
    var storeLong = <?php echo $storeLong; ?>;
    $(function() {
        $(window).load(function () {

            google.maps.event.addDomListener(window, 'load', drawMap(markers, googleMapDivId, false, false));

            $('#search_radius').click(function(){
                var $form = $('#search_by_distance');
                if ($form.valid()) {
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "http://maps.googleapis.com/maps/api/geocode/json",
                        data: {'address': $('#location').val(),'sensor':false},
                        success: function(data){
                            if(data.results.length){
                                latitude = data.results[0].geometry.location.lat;
                                longitude = data.results[0].geometry.location.lng;
                                $('#lat_search').val(latitude);
                                $('#long_search').val(longitude);

                                $("#search_by_distance").submit();
                            }
                        }
                    });
                }
            });
        });

    });
</script>
@endsection

@stop