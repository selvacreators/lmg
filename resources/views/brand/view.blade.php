<?php $locale = Session::get('locale');?>

@extends('layouts.layout_home')

@section('content')

@include('includes.header')

@include('includes.breadcrumb')

  @if(!empty($brandsetting))
  <div class="page-title-wrapper pt60">
      @if($brandsetting->brand_banner)
      <div class="parallax">
        <div class="background-image">            
          <img src="{{ Config::get('app.head_url') }}brand/brand_banner/{{$brandsetting->brand_banner}}">   
        </div>
        <div class="container">
          <div class="row">
            <div class="text-center col-xs-12">
              <div class="content-heading">
                @if(!empty($brandsetting))
                <h1 class="title">{{$brandsetting->brand_header}}</h1>
                @else
                <h1 class="title">Brand Collection</h1>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      @else
      <div class="clearfix"></div>
      <div class="container">
        <div class="row">
          <div class="text-center col-xs-12">
            <div class="content-heading">
              @if(!empty($brandsetting))                 
              <h1 class="title">{{$brandsetting->brand_header}}</h1>
              @else
              <h1 class="title">Brand Collection</h1>
              @endif
            </div>
          </div>
        </div>
      </div>
      @endif
  </div>
  @endif
  <main class="content-wrapper">
    <div class="page-content">
      <section class="brand-collection pt40">
        <div class="container">
          <div class="row">
            <div class="text-center col-xs-12">
              <div class="content-heading">
                @if(!empty($brandsetting))
                  <p class="mb40">{{$brandsetting->brands_desc}}</p>
                @endif
              </div>
            </div>
          </div>
        </div>
        @if($brand)
        <div class="container-fluid">
          <div class="all-brands">
            <div class="content">
              @foreach($brand as $key => $val)
                @if(@$val->brand_featureimg && @$val->brand_description)
                  @if($val->imge_position == 'left')
                    <section class="{{$val->imge_position}}-content">  
                      <div class="row row-odd">
                        <div class="col-md-6 view-animate fadeInLeftSm delay-1 active pl0 pr0">
                          <div class="image  promoimage promoimage-animated">
                            <img alt="{{$val->breadcumb_title}}" class="img-responsive" src="{{ Config::get('app.head_url') }}brand/brand_collection/{{$val->brand_featureimg}}">
                          </div>
                        </div>
                        <div class="col-md-6 view-animate fadeInRightSm delay-1 active">
                          <div class="brand-details">
                            <div class="brand-name">
                              <div class="brand-title image"><img alt="{{$val->breadcumb_title}}" class="img-responsive" 
                              src="{{ Config::get('app.head_url') }}brand/brand_logo/{{$val->brand_logo}}"></div>
                            </div>
                            <div class="brand-description">
                              <?php echo html_entity_decode($val->brand_description); ?>
                            </div>
                            <div class="brand-button">
                            <a class="btn btn-default" href="{{ URL( $locale , [$currentPath,$val->btn_url] ).'/' }}" title="{{$val->breadcumb_title}}">{{$val->btn_text}}</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  @endif
                  @if($val->imge_position == 'right')
                    <section class="{{$val->imge_position}}-content">  
                      <div class="row row-odd">
                        <div class="col-md-6 col-md-push-6 view-animate fadeInRightSm delay-1 active pl0 pr0">
                          <div class="image promoimage promoimage-animated">
                            <img alt="{{$val->breadcumb_title}}" class="img-responsive" src="{{ Config::get('app.head_url') }}brand/brand_collection/{{$val->brand_featureimg}}">
                          </div>
                        </div>
                        <div class="col-md-6 col-md-pull-6 view-animate fadeInLeftSm delay-1 active">
                          <div class="brand-details">
                            <div class="brand-name">
                              <div class="brand-title image"><img alt="{{$val->breadcumb_title}}" class="img-responsive" 
                              src="{{ Config::get('app.head_url') }}brand/brand_logo/{{$val->brand_logo}}"></div>
                            </div>
                            <div class="brand-description">
                              <?php echo html_entity_decode($val->brand_description); ?>
                            </div>
                            <div class="brand-button text-right">
                              <a class="btn btn-default" href="{{ URL( $locale , [$currentPath,$val->btn_url] ).'/' }}" title="{{$val->breadcumb_title}}">{{$val->btn_text}}</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  @endif
                @endif
              @endforeach
            </div>
          </div>
        </div>
        @endif
      </section>
    </div>
  </main>
@include('includes.newsletter-form')

@include('includes.footer')

@stop