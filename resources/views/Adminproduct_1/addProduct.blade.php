@extends('layouts.layout_admin') @section('content')

<link href="{{ Config::get('app.head_url') }}css/demo.css" rel="stylesheet" />
<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}

</style>
<div class="main-panel">

  <div class="inner-content">
    <div class="container-fluid">

      <div class="col-xs-12">
        {!! Form::open(array('url' => '/Admin/add_Product','class'=>'form-horizontal','method'=>'post','enctype'=>'multipart/form-data')) !!}                
        <input name="_token" type="hidden" value="{{csrf_token()}}">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title pull-left">Add Product</h3>
            <div class="pull-right">
              <button class="btn btn-success">Save</button>
            </div>          

          </div>

          <div class="card-body">
            <nav class="nav-container">
              <ul class='nav nav-tabs' role="tablist">

                <li class='active'><a data-toggle="tab" href="#Product">Product</a></li>

                <li><a data-toggle="tab" href="#Data">Data</a></li>

                <li><a data-toggle="tab" href="#Attribute">Attribute</a></li>

                <li><a data-toggle="tab" aria-controls="Image" href="#Image">Image</a></li>

                <!-- <li><a data-toggle="tab" href="#Review">Review</a></li> -->

                <!-- <li><a data-toggle="tab" href="#menu6">Option</a></li>

                 <li><a data-toggle="tab" href="#menu7">Filter</a></li> 

                <li><a data-toggle="tab" href="#menu8">Discount</a></li>

                <li><a data-toggle="tab" href="#menu9">Special</a></li>-->

                <li><a data-toggle="tab" href="#Features">Features</a></li>

              </ul>
            </nav>
            <div class="tab-content">

              <div id="Product" class="tab-pane fade active in">
                @include('Adminproduct.product_form')
              </div>
              <div id="Data" class="tab-pane fade">
                @include('Adminproduct.data')     
              </div>
              <!-- Product Data Tab End-->
              <!-- Product Attribute start Here-->
              <div id="Attribute" class="tab-pane fade">
               @include('Adminproduct.attribute')                
             </div>
             <!-- Product Attribute End Here-->

             <!-- Procut Image Start Here -->
             <div id="Image" class="tab-pane fade">
               @include('Adminproduct.Image')           
             </div>
             <!-- Product Image end Here -->

             <!-- Product Features end Here -->
             <div id="Features" class="tab-pane fade">
               @include('Adminproduct.features')           
             </div>
             <!-- Product Image end Here -->
           </div>
         </div>
         <div class="card-footer">

         </div>
       </div>
       {!! Form::close() !!}
       <!-- Tab panes -->
     </div>
     <div class="clearfix"></div>
   </div>


 </div>
</div>
<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.js"></script>

<script src="{{ Config::get('app.head_url') }}ck_editor/ckeditor.js" />
<script src="{{ Config::get('app.head_url') }}ck_editor/sample.js"></script>
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}ck_editor/samples.css">
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}ck_editor/neo.css">
<script>
  CKEDITOR.replace('short_desc');
  CKEDITOR.config.allowedContent = true;
</script>
<script>
  CKEDITOR.replace('desc');
  CKEDITOR.config.allowedContent = true;
</script>
<script>
  $(document).ready(function() {
  //$('#editor1').hide();

  $('#sku').blur(function() {
    var getsku = $(this).val();
    var tokens = $('#tokens').val();
          //console.log(getval);
          $.ajax({
            type:'GET',
            url:"{{URL('/Admin/checksku')}}",
            data:{'_token':tokens,'getsku':getsku},
            success:function(res){
                  //console.log(res);
                  if(res == 1){
                    $('#error').html('Wrong Sku already Exits.');
                    $('#sku').focus();
                    $('#btnadd').attr("disabled", "disabled");
                  }
                  else{
                   $('#error').html('');
                   $('#btnadd').removeAttr("disabled");; 
                 }

               }
             })

        });

  var counter = 0;



  $("#addattri").on("click", function () {

    var attribute_id = $('#attribute_id').val();
    var newRow = $("<tr class='vinrow'>");
    var cols = "";

    cols += ' <td style="border:0px solid #ddd;"><select name="attribute_id[]" class="form-control" id="attribute_id" style="width:220px;"><option value="">--Select--</option>@foreach($attribute as $attr)<option value="{{$attr->attribute_id}}">{{$attr->name}}</option>@endforeach</select></td><td style="border:0px solid #ddd;"></td>';

    cols += '<td style="border:0px solid #ddd;"><select name="lang_code[]" class="form-control" id="lang_code" required="required" style="width: 220px;"><option value="">--Select--</option>@foreach($lnguage as $lng)<option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>@endforeach</select></td>';
    cols += '<td style="border:0px solid #ddd;"><input required type="text" id="" class="form-control odom" name="text[]" style="width: 220px;height:30px;"></td>';

    cols += '<td style="border:0px solid #ddd;"><input type="button" class="ibtnDel btn btn-md btn-danger" value="-"></td>';
    newRow.append(cols);
    $("table.Dataorder-list").append(newRow);
          // $("multi").append(newRow);
          counter++;
       // }

       var count =0;

       $('.txtecount').each(function(){
        if($(this).val() !=""){
          count++;
        }
      });

     });


  $("table.Dataorder-list").on("click", ".ibtnDel", function (event) {
    $(this).closest("tr").remove();       
    counter -= 1
    var count =0;
  });



  var counter = 0;



  $("#addimages").on("click", function () {

    var newRow = $("<tr class='vinrow'>");
    var cols = "";


    cols += '<td style="border:0px solid #ddd;"><input required type="file" id="image" class="form-control odom" name="image[]" style="width: 320px;"></td>';

    cols += '<td style="border:0px solid #ddd;"><input required type="text" id="Imagesort_order" class="form-control odom" name="Imagesort_order[]" style="width: 320px;"></td>';

    cols += '<td style="border:0px solid #ddd;"><input type="button" class="ibtnDel btn btn-md btn-danger" value="-"></td>';
    newRow.append(cols);
    $("table.Imageorder-list").append(newRow);
          // $("multi").append(newRow);
          counter++;
       // }

       var count =0;

       $('.txtecount').each(function(){
        if($(this).val() !=""){
          count++;
        }
      });

     });


  $("table.Imageorder-list").on("click", ".ibtnDel", function (event) {
    $(this).closest("tr").remove();       
    counter -= 1


    var count =0;

  });

});
</script>
@stop