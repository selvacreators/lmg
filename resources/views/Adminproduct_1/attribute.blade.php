<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <div class="row mb30">
                <div class="col-sm-6">
                    <label>Attribute<span class="note">*</span></label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <select name="attribute_id[]" class="form-control" id="attribute_id">
                        <option value="">--Select--</option>
                        @foreach($attribute as $attr)
                        <option value="{{$attr->attribute_id}}">{{$attr->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <div class="row mb30">
                <div class="col-sm-6">
                    <label>Language<span class="note">*</span></label>
                </div>
            </div>
             <div class="row">
                <div class="col-sm-12">
                    <select name="lang_code[]" class="form-control" id="lang_code" required="required">
                        <option value="">--Select--</option>
                        @foreach($lnguage as $lng)
                        <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <div class="row mb30">
                <div class="col-sm-6">
                    <label>Text<span class="note">*</span></label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <input type="text" name="text[]" id="text" style="height:30px;" class="form-control" autocomplete="off" />
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row multi" id="multi">
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-6">
                <table class="table Dataorder-list">
                </table>
            </div>
        </div>
        
    </div>
</div>


<div class="row" align="center">
    <input type="button" id="addattri" value="Add Attribute" class="btn btn-daimler col-md-2">
</div>
