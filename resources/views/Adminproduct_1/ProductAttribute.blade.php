@extends('layouts.layout_admin')
@section('content')

<link href="{{ Config::get('app.head_url') }}css/demo.css" rel="stylesheet" />

<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}
.col-xs-2{
  width:16.333333%;
  float: left;
  position: relative;
}
.col-xs-10{
  width:83.333333%;
  float: left;
  position: relative;
}
.tabs-left {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}

.tabs-left>li {
  float: none;
  margin-bottom: 2px;
  width: 100%;
 position: relative;
    border: 1px solid #dadada;
    padding: 5px;
    margin-bottom: 5px;
}
.tabs-left>li {
  margin-right: -1px;
}

.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tab-content {
    padding-left: 20px;
}
.form-control{
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  }
  .tab-pane .table tbody > tr > td:first-child {
    width: 95px !important;
}

.form-group input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
    margin: 0;
    font-size: 23px;
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 0;
    direction: ltr;
  }
</style>
<div class="main-panel">
    
      <div class="content">

<div class="container-fluid">
              
<div class="row">
  <span><h3>Add Product Data</h3></span> 
</div>
  <hr> 
  <ul class='nav nav-wizard'>
  
  <li><a href="#`">Product</a></li>

  <li ><a href='#'>Data</a></li>

  <li class='active'><a href='#'>Attribute</a></li>


  <li><a href='#'>Image</a></li>

  <li><a href='#'>Review</a></li>
  <li><a href='#'>Option</a></li>

  <li><a href='#'>Filter</a></li>

  <li><a href='#'>Discount</a></li>

  <li><a href='#'>Special</a></li>

  <li><a href='#'>Features</a></li>

</ul>
          
  <div class="col-xs-12">
      <form method="POST" action="{{ URL('Admin/add_ProductAttribute')}}" enctype="multipart/form-data">
      <input name="_token" type="hidden" value="{{csrf_token()}}">
      <input name="product_id" id="product_id" type="hidden" value="{{$pro_id}}">
        
       <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Attribute<span class="note">*</span></label>
                  </div>
                  </div>
                  <br>
                  <select name="attribute_id[]" class="form-control" id="attribute_id">
                  <option value="">--Select--</option>
                  @foreach($attribute as $attr)
                  <option value="{{$attr->attribute_id}}">{{$attr->name}}</option>
                  @endforeach
                </select>
                </div>
                </div>

         <div class="col-sm-3">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Language<span class="note">*</span></label>
                  </div>
                  </div>
                  <br>
                  <select name="lang_code[]" class="form-control" id="lang_code" required="required">
                                        <option value="">--Select--</option>
                                        @foreach($lnguage as $lng)
                                        <option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
                                        @endforeach
                                      </select>
                </div>
          </div>
         <div class="col-sm-3">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Text<span class="note">*</span></label>
                  </div>
                  </div>
                  <br> <input type="text" name="text[]"  id="text" class="form-control" autocomplete="off" />
                </div>
          </div>
               
      </div>

      <div class="row multi" id="multi">
           <div class="col-md-10" >
                <div class="col-md-6" >
                <table class="table order-list">
                </table>
                </div>
         </div>
     </div>

     <div class="row" align="center">
           <input type="button" id="addattri" value="Add Attribute" class="btn btn-daimler col-md-2">
      </div>

  
      <div class="butn pull-right">
      <input type="submit" value="Next"  id="btnadd" class="btn btn-daimler" />
      </div>            

      </form> 
      <!-- Tab panes -->

  </div>
  <div class="clearfix"></div>
  </div>


</div>
</div>

</div>
<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.js"></script>

<script>
  $(document).ready(function () {
     var counter = 0;



    $("#addattri").on("click", function () {

      var attribute_id = $('#attribute_id').val();


        //if(attribute_id != ''){

          var newRow = $("<tr class='vinrow'>");
          var cols = "";

          cols += ' <td style="border:0px solid #ddd;"><select name="attribute_id[]" class="form-control" id="attribute_id" style="width:220px;"><option value="">--Select--</option>@foreach($attribute as $attr)<option value="{{$attr->attribute_id}}">{{$attr->name}}</option>@endforeach</select></td><td style="border:0px solid #ddd;"></td>';

          cols += '<td style="border:0px solid #ddd;"><select name="lang_code[]" class="form-control" id="lang_code" required="required" style="width: 220px;"><option value="">--Select--</option>@foreach($lnguage as $lng)<option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>@endforeach</select></td>';
          cols += '<td style="border:0px solid #ddd;"><input required type="text" id="" class="form-control odom" name="text[]" style="width: 220px;"></td>';

          cols += '<td style="border:0px solid #ddd;"><input type="button" class="ibtnDel btn btn-md btn-danger" value="-"></td>';
          newRow.append(cols);
          $("table.order-list").append(newRow);
          // $("multi").append(newRow);
          counter++;
       // }
   
var count =0;

      $('.txtecount').each(function(){
        if($(this).val() !=""){
        count++;
        }
      });
    
});


     $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1


        var count =0;

      // $('.txtecount').each(function(){
      //   if($(this).val() !=""){
      //   count++;
      //   }
      // });
      // //alert("No of empty text box="+count);
      // if(count == 0){
      //    $('#selModel').hide();
      //    $('#selModel').attr('disabled',true);
      //     $('#model_name').show();
      //     //$('#model_name').attr('disabled',false);
      // }
      // $('#VinNo').html('Total VIN Entered =' + count );
    });
});

</script>
@stop