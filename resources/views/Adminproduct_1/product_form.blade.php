  <div class="form-group">
    <label for="product_name" class="control-label col-sm-3">Product Name :</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name">
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="col-sm-3 control-label">sku :</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="sku" id="sku" required="required">
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="col-sm-3 control-label">Quantity :</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="quantity" id="quantity">
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">StockStatus :</label>
    <div class="col-sm-6">
        <select name="stock_status" class="form-control" id="stock_status">
            @foreach($stock_status as $ststatus)
            @if($ststatus->name == 'In-Stock')
            <option value="{{$ststatus->stock_status_id}}" selected="selected">{{$ststatus->name}}</option>
            @else
            <option value="{{$ststatus->stock_status_id}}">{{$ststatus->name}}</option>
            @endif
            @endforeach
        </select>
    </div>
</div>


<div class="form-group">
    <label for="Image" class="control-label col-sm-3">Image :</label>
    <div class="col-sm-6">
        <input type="file" name="Product_image" id="Product_image" class="form-control">
    </div>
</div>

     <!--    <div class="form-group">
            <label for="Image" class="control-label col-sm-3">Video :</label>
            <div class="col-sm-6">
                <input type="file" name="prodcut_video" id="prodcut_video" class="form-control" autocomplete="off" />
            </div>
        </div> -->

    <div class="form-group">
        <label for="Image" class="control-label col-sm-3">Brand :</label>
        <div class="col-sm-6">
            <select name="brand_id" class="form-control" id="brand_id">
              <option value="">--Select--</option> 
              @foreach($brands as $brand)
              <option value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
              @endforeach
          </select>
      </div>
  </div>

  <div class="form-group">
    <label for="Image" class="control-label col-sm-3">Collection :</label>
    <div class="col-sm-6">
        <select class="selectpicker" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="collection_id[]" id="collection_id">
            @foreach($collection as $col)
            <option value="{{$col->collection_id}}">{{$col->collection_name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Price :</label>
    <div class="col-sm-6">
        <input type="text" name="price" id="price" required="required" class="form-control" autocomplete="off" value="0" />
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Date Available :</label>
    <div class="col-sm-6">
        <input type="text" name="date_available" id="date_available" class="form-control date_available" required="required">
    </div>
</div>


<div class="form-group">
    <label for="inputPassword" class="col-sm-3 col-form-label">Weight:</label>
    <div class="col-sm-3">
        <select name="weight_class_id" class="form-control" id="weight_class_id">
            @foreach($weight as $weight)

            @if($weight->default == 1)
            <option value="{{$weight->weight_class_id}}" selected="selected">{{$weight->title}}</option>
            @else
            <option value="{{$weight->weight_class_id}}">{{$weight->title}}</option>
            @endif
            @endforeach
        </select>
    </div>

    <div class="col-sm-3">

        <input type="text" name="weight" id="weight" class="form-control" placeholder="weight">
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="col-sm-3 col-form-label">length:</label>
    <div class="col-sm-3">
        <select name="length_class_id" class="form-control" id="length_class_id">
            @foreach($length as $lengt)
            @if($lengt->default == 1)
            <option value="{{$lengt->length_class_id}}" selected="selected">{{$lengt->title}}</option>
            @else
            <option value="{{$lengt->length_class_id}}">{{$lengt->title}}</option>
            @endif
            @endforeach
        </select>
    </div>

    <div class="col-sm-3">
        <input type="text" name="length" id="length" class="form-control" placeholder="length">
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="col-sm-3 col-form-label">Dimension:</label>
    <div class="col-sm-3">
        <input type="text" name="width" id="width" class="form-control" placeholder="Width">
    </div>

    <div class="col-sm-3">
        <input type="text" name="height" id="height" class="form-control" placeholder="height">
    </div>
</div>



<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Comfort :</label>
    <div class="col-sm-6">
        <select name="comfort" class="form-control" id="comfort">
          <option value="">--Select--</option>
          <option value="firm">Firm</option>
          <option value="plush">Plush</option>
          <option value="medium">Medium</option>
      </select>
  </div>
</div>

<div class="form-group">
<label for="inputPassword" class="control-label col-sm-3">Subtract :</label>
<div class="col-sm-6">
    <select name="subtract" class="form-control" id="subtract">
        <option value="">--Select--</option>
        <option value="1">1</option>
        <option value="2">2</option>
    </select>
</div>
</div>

<div class="form-group">
<label for="inputPassword" class="control-label col-sm-3">Sort Order :</label>
<div class="col-sm-6">
    <input type="text" name="sort_order" id="sort_order" class="form-control" required="required">
</div>
</div>


<div class="form-group">
<label for="inputPassword" class="control-label col-sm-3">Status :</label>
<div class="col-sm-6">
    <select name="status" class="form-control" id="status" required="required">
        <option value="">--Select--</option>
        <option value="1">Active</option>
        <option value="2">De-Active</option>
    </select>
</div>
</div>