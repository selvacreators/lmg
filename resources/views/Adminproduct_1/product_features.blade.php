@extends('layouts.layout_admin')
@section('content')

<link href="{{ Config::get('app.head_url') }}css/demo.css" rel="stylesheet" />

<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}
.col-xs-2{
  width:16.333333%;
  float: left;
  position: relative;
}
.col-xs-10{
  width:83.333333%;
  float: left;
  position: relative;
}
.tabs-left {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}

.tabs-left>li {
  float: none;
  margin-bottom: 2px;
  width: 100%;
 position: relative;
    border: 1px solid #dadada;
    padding: 5px;
    margin-bottom: 5px;
}
.tabs-left>li {
  margin-right: -1px;
}

.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tab-content {
    padding-left: 20px;
}
.form-control{
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  }
  .tab-pane .table tbody > tr > td:first-child {
    width: 95px !important;
}

.form-group input[type=file] {
  
     position: absolute;
    top: 35px;
    right: 0;
    /* width: 100%; */
    /* height: 100%; */
    /* margin: 0; */
    /* font-size: 23px; */
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 1;
    direction: ltr;
    z-index: 1;
}
</style>
<div class="main-panel">
    
      <div class="content">

<div class="container-fluid">
              
<div class="row">
  <span><h3>Add Product Data</h3></span> 
</div>
  <hr> 
  <ul class='nav nav-wizard'>
  
  <li><a href="#`">Product</a></li>

  <li ><a href='#'>Data</a></li>

  <li ><a href='#'>Attribute</a></li>


  <li><a href='#'>Image</a></li>

  <li  ><a href='#'>Review</a></li>
  <li class='active'><a href='#'>Features</a></li>

  <li><a href='#'>Option</a></li>
  <li><a href='#'>Filter</a></li>

  <li><a href='#'>Discount</a></li>

  <li><a href='#'>Special</a></li>


</ul>
          
  <div class="col-xs-12">
      <form method="POST" action="{{ URL('Admin/add_ProductFeature')}}" enctype="multipart/form-data">
      <input name="_token" type="hidden" value="{{csrf_token()}}">
      <input name="product_id" id="product_id" type="hidden" value="{{$pro_id}}">
        
       <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Name<span class="note">*</span></label>
                  </div>
                  </div>
                  <br>
                  <input type="text" id="name" name="name" class="form-control" required="required">
                </div>
                </div>
      </div> 
    


      <div class="row">
         <div class="col-sm-6">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Product Description</label>
                  </div>
                  <textarea name="prod_des" id="prod_des" cols="70" class="form-control" rows="5"></textarea>
                  </div>
          </div>
         </div>
      </div>


      <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Image</label>
                  </div>
                  </div>
                  <br>
                  <input type="file" id="prod_image" name="prod_image" class="form-control" >
                </div>
                </div> 

                <div class="col-sm-4">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Video</label>
                  </div>
                  </div>
                  <br>
                  <input type="file" id="prod_video" name="prod_video" class="form-control" >
                </div>
                </div>
      </div> 
<div class="row">
         <div class="col-sm-6">
                  <div class="form-group">
                  <div class="row">
                  <div class="col-md-6">
                  <label>Status</label>
                  </div>
                  <select name="status" id="status" class="form-control" required="required">
                  <option value="">--Selected--</option>
                  <option value="1">Active</option>
                  <option value="2">De-Active</option>
                  </select>
                  </div>
          </div>
         </div>
      </div>

  

  
      <div class="butn pull-right">
      <input type="submit" value="Next"  id="btnadd" class="btn btn-daimler" />
      </div>            

      </form> 
      <!-- Tab panes -->

  </div>
  <div class="clearfix"></div>
  </div>


</div>
</div>

</div>
<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.js"></script>
@stop