@section('admin-style')
<link rel="stylesheet" href="{{ Config::get('app.head_url') }}admin/plupload/jquery.plupload.queue.css" type="text/css" media="screen">
@endsection

                            <div class="row">
                                <div id="img-row">
                                    <table class="table" width="50%">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Sort Order</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12">
                                <div class="form-group">
                                <div class="col-sm-12">
                                <div id="uploader">
                                    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                                </div>
                                <input id="upload_image" type="hidden" value="" name="upload_image">

                                </div>
                                </div>
                                </div>

                                <div class="clearfix"></div>
                                

                            </div>

                            <div class="row multi" id="multi">
                                <div class="col-md-10">
                                    <div class="col-md-6">
                                        <table class="table Imageorder-list">
                                        </table>
                                    </div>
                                </div>
                            </div>

@section('admin-script')
<script type="text/javascript" src="{{ Config::get('app.head_url') }}admin/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="{{ Config::get('app.head_url') }}admin/plupload/jquery.plupload.queue/jquery.plupload.queue.min.js"></script>

<script type="text/javascript">
// Convert divs to queue widgets when the DOM is ready
$(function() {
 
    $("#uploader").pluploadQueue({
        // General settings
        runtimes : 'html5,flash,silverlight,html4',
        url : '/Admin/products/uploadImage_ajax?diretorio=large',
        //url : 'http://localhost/completed/sgihair/admin/products/uploadImage_ajax',
        chunk_size : '10mb',
        unique_names : true,
        //resize : {width : 109, height : 73, crop: true},
         
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png"}
            ]
        },
        flash_swf_url : '/plupload/js/Moxie.swf',
        silverlight_xap_url : '/plupload/js/Moxie.xap',
        preinit : {
            Init: function(up, info) {
                log('[Init]', 'Info:', info, 'Features:', up.features);
            },
 
            UploadFile: function(up, file) {
                log('[UploadFile]', file);
            }
        },
        init : {
            PostInit: function() {
                log('[PostInit]');
                /*document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };*/
            },
 
            Browse: function(up) {
                // Called when file picker is clicked
                log('[Browse]');
            },
 
            Refresh: function(up) {
                // Called when the position or dimensions of the picker change
                log('[Refresh]');
            },
  
            StateChanged: function(up) {
                // Called when the state of the queue is changed
                log('[StateChanged]', up.state == plupload.STARTED ? "STARTED" : "STOPPED");
            },
  
            QueueChanged: function(up) {

                if(up.state == 1){
                     $('input[type="submit"]').prop('disabled', true);
                     $('.plupload_start').addClass('upload_file');
                }
                else if(up.state == 2){
                     $('input[type="submit"]').prop('disabled', false);
                     $('.plupload_start').removeClass('upload_file');
                }
                // Called when queue is changed by adding or removing files
                log('[QueueChanged]');
            },
 
            OptionChanged: function(up, name, value, oldValue) {
                // Called when one of the configuration options is changed
                log('[OptionChanged]', 'Option Name: ', name, 'Value: ', value, 'Old Value: ', oldValue);
            },
 
            BeforeUpload: function(up, file) {
                console.log(file);
                // Called right before the upload for a given file starts, can be used to cancel it if required
                if(!('thumb' in file)){
                    up.settings.url = '/Admin/products/uploadImage_ajax?diretorio=thumb&newName='+file.id;

                }else{
                    up.settings.url = '/Admin/products/uploadImage_ajax?diretorio=small&newName='+file.id
                }  

                //log('[BeforeUpload]', 'File: ', file);
                console.log(up.settings.resize);
            },
  
            UploadProgress: function(up, file) {
                // Called while file is being uploaded
                log('[UploadProgress]', 'File:', file, "Total:", up.total);
            },
 
            FileFiltered: function(up, file) {
                // Called when file successfully files all the filters
                log('[FileFiltered]', 'File:', file);
            },
  
            FilesAdded: function(up, files) {
                // Called when files are added to queue
                log('[FilesAdded]');
  
                plupload.each(files, function(file) {
                    log('  File:', file);
                });
            },
  
            FilesRemoved: function(up, files) {
                // Called when files are removed from queue
                log('[FilesRemoved]');
  
                plupload.each(files, function(file) {
                    log('  File:', file);
                });
            },
  
            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                if(!('thumb' in file)) {
                    file.thumb = true;
                    file.loaded = 0;
                    file.percent = 0;
                    file.status = plupload.QUEUED;
                    up.trigger("QueueChanged");
                    up.refresh();
                }

                log('[FileUploaded] File:', file, "Info:", info);
            },
  
            ChunkUploaded: function(up, file, info) {
                // Called when file chunk has finished uploading
                log('[ChunkUploaded] File:', file, "Info:", info);
            },
 
            UploadComplete: function(up, files) {
                
                var json_text = '';
                var arr = [];
                
                var htmlData = '';
                $(files).each(function(i, v) {
                    arr[i] = v.target_name;
                    htmlData += '<tr><td><img src="/public/product/thumb/'+v.target_name+'"></td><td><input type="text" style="width:250px" name="Imagesort_order[]" class="form-control" autocomplete="off" value="0" /><input type="hidden" style="width:250px" name="ImageList[]" value="'+v.target_name+'" /></td></tr>';
                });
                json_text = JSON.stringify(arr);
                $("#upload_image").val(json_text);
                $("#img-row table tbody").append(htmlData);

                // Called when all files are either uploaded or failed
                log('[UploadComplete]');
            },
 
            Destroy: function(up) {
                // Called when uploader is destroyed
                log('[Destroy] ');
            },
  
            Error: function(up, args) {
                // Called when error occurs
                log('[Error] ', args);
            }
        }
    });
 
    function log() {
        var str = "";
 
        plupload.each(arguments, function(arg) {
            var row = "";
 
            if (typeof(arg) != "string") {
                plupload.each(arg, function(value, key) {
                    // Convert items in File objects to human readable form
                    if (arg instanceof plupload.File) {
                        // Convert status to human readable
                        switch (value) {
                            case plupload.QUEUED:
                                value = 'QUEUED';
                                break;
 
                            case plupload.UPLOADING:
                                value = 'UPLOADING';
                                break;
 
                            case plupload.FAILED:
                                value = 'FAILED';
                                break;
 
                            case plupload.DONE:
                                value = 'DONE';
                                break;
                        }
                    }
 
                    if (typeof(value) != "function") {
                        row += (row ? ', ' : '') + key + '=' + value;
                    }
                });
 
                str += row + " ";
            } else {
                str += arg + " ";
            }
        });
 
        var log = $('#log');
        log.append(str + "\n");
        //log.scrollTop(log[0].scrollHeight);
    }
});
</script>
@endsection                            