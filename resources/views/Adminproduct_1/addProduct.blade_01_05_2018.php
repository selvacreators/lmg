@extends('layouts.layout_admin')
@section('content')

<link href="{{ Config::get('app.head_url') }}css/demo.css" rel="stylesheet" />
<style type="text/css" media="screen">
label{
  color: #000000;
}
.note{
  color: red;
}
.col-xs-2{
  width:16.333333%;
  float: left;
  position: relative;
}
.col-xs-10{
  width:83.333333%;
  float: left;
  position: relative;
}
.tabs-left {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-left {
  border-right: 1px solid #ddd;
}

.tabs-left>li {
  float: none;
  margin-bottom: 2px;
  width: 100%;
 position: relative;
    border: 1px solid #dadada;
    padding: 5px;
    margin-bottom: 5px;
}
.tabs-left>li {
  margin-right: -1px;
}

.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
  border-bottom-color: #ddd;
  border-right-color: transparent;
}

.tabs-left>li>a {
  border-radius: 4px 0 0 4px;
  margin-right: 0;
  display:block;
}
.tab-content {
    padding-left: 20px;
}
.form-control{
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  }
  .tab-pane .table tbody > tr > td:first-child {
    width: 95px !important;
}

.form-group input[type=file] {
  
     position: absolute;
    top: 35px;
    right: 0;
    /* width: 100%; */
    /* height: 100%; */
    /* margin: 0; */
    /* font-size: 23px; */
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 1;
    direction: ltr;
    z-index: 1;
}
</style>
<div class="main-panel">
    
      <div class="content">
          <div class="container-fluid">
              
<div class="row">
  <span><h3>Add Product</h3></span> 
</div>
  <hr> 
  <ul class='nav nav-wizard'>
  
  <li class='active'><a href="#`">Product</a></li>

  <li><a href='#'>Data</a></li>

  <li><a href='#'>Attribute</a></li>
  
  <li><a href='#'>Image</a></li>

  <li><a href='#'>Review</a></li>

  <li><a href='#'>Option</a></li>



  <li><a href='#'>Filter</a></li>

  <li><a href='#'>Discount</a></li>

  <li><a href='#'>Special</a></li>

  <li><a href='#'>Features</a></li>

</ul>
          
        <div class="col-xs-12">
                     <form method="POST" action="{{ URL('Admin/add_Product')}}" enctype="multipart/form-data">
                      <input name="_token" type="hidden" value="{{csrf_token()}}">

            <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                  <div class="row">
                                     <div class="col-md-6">
                                     <label>Product Name<span class="note">*</span></label>
                                     </div>
                                     </div>
                                     <br>
                                    <input type="text" required name="product_name" id="product_name" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  <div class="row">
                                    
                                     <div class="col-md-6">
                                     <label>sku</label>
                                     </div>
                                     </div>
                                     <br>
                                    <input type="text" name="sku" id="sku" required class="form-control" autocomplete="off"/>
                                     <span id="error" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">
                                    
                                   <div class="col-md-6">
                                   <label>quantity</label>
                                   </div>
                                   </div>
                                   <br>
                                    <input type="text" name="quantity"  id="quantity" class="form-control" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">
                                    
                                   <div class="col-md-6">
                                   <label>Stock Status</label>
                                   </div>
                                   </div>
                                   <br>
                                      <select name="stock_status" class="form-control" id="stock_status">
                                      <option value="">--Select--</option>
                                      @foreach($stock_status as $ststatus)
                                      <option value="{{$ststatus->stock_status_id}}">{{$ststatus->name}}</option>
                                      @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>
        
                      
                      
                       <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">                                 
                                      <div class="col-md-6">
                                     <label style="margin-top:10px">image</label>
                                     </div>
                                     <br>
                                      <input type="file" name="image" id="image"  class="form-control" >
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>video</label>
                                   </div>
                                   </div>
                                   <br>
                                    <input type="file" name="prodcut_video"  id="prodcut_video" class="form-control" autocomplete="off" />
                                </div>
                            </div>

                             <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Brand</label>
                                   </div>
                                   </div>
                                   <br>
                                        <select name="brand_id" class="form-control" id="brand_id">
                                        <option value="">--Select--</option> 
                                        @foreach($brands as $brand)
                                        <option value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
                                        @endforeach
                                        </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Collection</label>
                                   </div>
                                   </div>
                                        <select class="selectpicker" data-style="btn select-with-transition" multiple title="Choose City" data-size="7" name="collection_id[]" id="collection_id">
                            @foreach($collection as $col)
                            <option value="{{$col->collection_id}}">{{$col->collection_name}}</option>
                            @endforeach
                          </select>
                                </div>
                            </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Price</label>
                                   </div>
                                   </div>
                                   <br>
                                    <input type="text" name="price"  id="price" required="required" class="form-control" autocomplete="off" />
                                </div>
                            </div>


                             <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Date Available</label>
                                   </div>
                                   </div>
                                   <br>
                                    <input type="text" name="date_available" id="date_available" class="form-control date_available" required="required">
                                </div>
                            </div> 

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Weight Class</label>
                                   </div>
                                   </div>
                                   <br>
                                   <select name="weight_class_id" class="form-control" id="weight_class_id">
                            <option value="">--Select--</option> 
                            @foreach($weight as $weight)
                            <option value="{{$weight->weight_class_id}}">{{$weight->title}}</option>
                            @endforeach
                          </select>
                                  
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Weight</label>
                                   </div>
                                   </div>
                                   <br>  <input type="text" name="weight" id="weight" class="form-control" placeholder="weight">
                                  
                                </div>
                            </div>
                          </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>length</label>
                                   </div>
                                   </div>
                                   <br>
                                    <select name="length_class_id" class="form-control" id="length_class_id">
                                      <option value="">--Select--</option>
                                      @foreach($length as $lengt)
                                      <option value="{{$lengt->length_class_id}}">{{$lengt->title}}</option>
                                      @endforeach
                                      </select>
                                  
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>length</label>
                                   </div>
                                   </div>
                                   <br>
                                    <input type="text" name="length" id="length" class="form-control" placeholder="Dimensions">
                                  
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>width</label>
                                   </div>
                                   </div>
                                   <br><input type="text" name="width" id="width" class="form-control" required="required">
                                  
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>height</label>
                                   </div>
                                   </div>
                                   <br><input type="text" name="height" id="height" class="form-control" required="required">
                                  
                                </div>
                            </div>
                       </div>


                       <div class="row">
                        

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Comfort</label>
                                   </div>
                                   </div>
                                    <br>
                                    <select name="comfort" class="form-control" id="comfort">
                                    <option value="">--Select--</option>
                                    <option value="Firm">Firm</option>
                                    <option value="Plus">Plus</option>
                                    <option value="Medium">Medium</option>
                                    </select>
                                  
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>subtract</label>
                                   </div>
                                   </div>
                                   <br>
                           <select name="subtract" class="form-control" id="subtract">
                            <option value="">--Select--</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                                  
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Sort Order</label>
                                   </div>
                                   </div>
                                   <br><input type="text" name="sort_order" id="sort_order" class="form-control" required="required">
                                  
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="row">                                    
                                   <div class="col-md-6">
                                   <label>Status</label>
                                   </div>
                                   </div>
                                   <br><select name="status" class="form-control" id="status" required="required">
                            <option value="">--Select--</option>
                            <option value="1">Active</option>
                            <option value="2">De-Active</option>
                          </select>
                                  
                                </div>
                            </div>
                          </div>
            
            <div class="butn pull-right">
              <input type="submit" value="Next"  id="btnadd" class="btn btn-daimler" />
            </div>            

                  </form> 
            <!-- Tab panes -->
            
        </div>
        <div class="clearfix"></div>
    </div>
             
         
              </div>
      </div>

</div>



<script src="{{ Config::get('app.head_url') }}frontend/js/jquery.js"></script>
        <script>
                $(document).ready(function() {
                //$('#editor1').hide();

                    $('#sku').blur(function() {
                        var getsku = $(this).val();
                        var tokens = $('#tokens').val();
                        //console.log(getval);
                        $.ajax({
                            type:'GET',
                            url:"{{URL('/Admin/checksku')}}",
                            data:{'_token':tokens,'getsku':getsku},
                            success:function(res){
                                //console.log(res);
                                if(res == 1){
                                    $('#error').html('Wrong Sku already Exits.');
                                    $('#sku').focus();
                                    $('#btnadd').attr("disabled", "disabled");
                                }
                                else{
                                   $('#error').html('');
                                    $('#btnadd').removeAttr("disabled");; 
                                }

                            }
                        })

                    });
                });

        </script>
@stop