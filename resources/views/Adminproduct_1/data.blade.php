<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Language :</label>
    <div class="col-sm-6">

        <select name="Data_lang_code" class="form-control" id="Data_lang_code" required="required">
@foreach($lnguage as $lng)
<option value="{{$lng->lang_code}}">{{$lng->lang_name}}</option>
@endforeach
</select>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Product Slug :</label>
    <div class="col-sm-6">
        <input type="text" name="product_slug" id="product_slug" class="form-control" autocomplete="off" required="required" />
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Product Name :</label>
    <div class="col-sm-6">
        <input type="text" name="name" id="name" class="form-control" />
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Meta Title :</label>
    <div class="col-sm-6">
        <input type="text" name="meta_title" id="meta_title" class="form-control" required="required" />
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Meta Keyword:</label>
    <div class="col-sm-6">
        <input type="text" name="meta_keyword" id="meta_keyword" class="form-control" />
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Product Tag:</label>
    <div class="col-sm-6">
        <input type="text" name="tag" id="tag" class="form-control" />
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Meta Description:</label>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="meta_desc" id="meta_desc" class="form-control"></textarea>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Short Description:</label>
   
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="short_desc" id="short_desc" class="form-control"></textarea>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Product Description:</label>
   
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="desc" id="desc" class="form-control"></textarea>
    </div>
</div>