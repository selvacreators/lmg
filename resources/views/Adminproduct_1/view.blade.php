@extends('layouts.layout_admin')
@section('content')

<div class="main-panel">

  <div class="inner-content">
    <div class="container-fluid">
      <div class="row mt">
        <div class="col-md-12">
          <div class="content-panel">
            <div class="card">
              <div class="card-header">
                <a href="/Admin/addProduct" class="btn btn-primary btn-sm pull-right">Add Product</a>
                <h4 style="font-weight: bold;"><i class="fa fa-angle-right"></i> View And Edit Product List</h4>
              </div>
              <div class="card-body">
                @if($product->count() > 0)
                <table id="datatable" class="table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                  <thead>
                    <tr>
                      <th style="font-weight: bold;">S.No</th>
                      <th style="font-weight: bold;">Product Name</th>
                      <th style="font-weight: bold;">Qty</th>
                      <th style="font-weight: bold;">Price</th>
                      <th style="font-weight: bold;">Date</th>
                      <th style="font-weight: bold;">Status</th>
                      <th style="font-weight: bold;" class="col-xs-2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1;?>
                    @foreach($product as $prod)
                    <tr>
                     <td><?php echo $i; ?></td>
                      <td></td>
                     <td><?php echo $prod->quantity; ?></td>
                     <td><?php echo $prod->price; ?></td>
                     <td><?php echo $prod->date_available; ?></td>
                     <td>
                      <?php 
                      if($prod->status == '1')
                      {
                        ?>
                        <span>Active</span>
                        <?php
                      }
                      else{
                        ?>
                        <span>De-Active</span>
                        <?php
                      }
                      ?>                                    
                      <td>
                        <a href="/Admin/editProduct/{{$prod->product_id}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
                        <a href="/Admin/deleteProduct/{{$prod->product_id}}" onclick="return confirm('Are you sure Delete Menu?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o "></i></button>
                        </a>
                      </td>                                     </td>


                    </tr>
                    <?php $i++; ?>     

                    @endforeach
                  </tbody>
                </table>

                @else
                <table id="datatable" class="table table-striped table-advance table-hover bordered dt-responsive nowrap">                 
                  <thead>
                    <tr>
                      <th style="font-weight: bold;">S.No</th>
                      <th style="font-weight: bold;">Product Name</th>
                      <th style="font-weight: bold;">Product Image</th>
                      <th style="font-weight: bold;">Modal</th>
                      <th style="font-weight: bold;">Price</th>
                      <th style="font-weight: bold;">Quantity</th>
                      <th style="font-weight: bold;">Status</th>
                      <th style="font-weight: bold;">Action</th>
                    </tr>
                  </thead>
                  <tr>
                    <td align="center" colspan="8">No video Found..!</td>
                  </tr>
                </table>
                @endif

              </div>

            </div>
            
          </div>
        </div>
      </div>


      <?php echo $product->render(); ?>   
    </div>
  </div>

</div>

@if(Session::has('message'))
<div id="alertbox" class="modal fade" style=" background-color: rgba(0,0,0,.5);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">

        <h4 align="center">{{ Session::get('message') }}</h4>

        <center>

          <button type="button" class="btn btn2 href2" data-dismiss="modal">CLOSE</button>

        </center>
        {{ Session::forget('message') }}
      </div>
    </div>
  </div>
</div>
@endif

@stop