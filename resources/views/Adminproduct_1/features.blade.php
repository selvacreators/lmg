<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Content Heading:</label>
    <div class="col-sm-9">
        <input type="text" name="heading" id="heading" class="form-control" autocomplete="off" />
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="col-sm-3 control-label">Feature Images:</label>
    <div class="col-sm-9">
        <input type="file" name="Feat_image" id="Feat_image" class="form-control" >
    </div>
   
</div>
<div class="form-group">
    <label for="inputPassword" class="col-sm-3 control-label">Feature Videos:</label>
    <div class="col-sm-9">
        <input type="file" name="Feat_video" id="Feat_video" class="form-control" >
    </div>
</div>
<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Feature Content:</label>
    <div class="col-sm-9">
        <textarea cols="10" rows="5" name="Feat_desc" id="Feat_desc" class="form-control"></textarea>
    </div>
</div>

<div class="form-group">
    <label for="inputPassword" class="control-label col-sm-3">Status:</label>
    <div class="col-sm-3">        
    <select name="Feat_status" id="Feat_status" class="form-control" required="required">
        <option value="">--Selected--</option>
        <option value="1">Active</option>
        <option value="2">De-Active</option>
    </select>
    </div>
</div>