@extends('layouts.layout_admin')
@section('content')
<div class="main-panel">
    <div class="inner-content">
        <div class="container-fluid">
            <div class="row mt">
                <div class="col-md-12">
                    <div class="content-panel">
                        {!! Form::open(array('url' => '/Admin/update/admin-user','class'=>'form-horizontal user-form','method'=>'post','enctype'=>'multipart/form-data'))!!}
                          <input type="hidden" name="uid" value="{{$users->id}}"/>
                          <div class="card">
                            <div class="card-header">
                                <a href="/Admin/users" class="btn btn-primary btn-sm pull-right">Back</a>
                                <div class="pull-right"><button class="btn btn-sm btn-success">Save</button><button class="btn btn-sm btn-danger" type="reset">Reset</button></div>
                                <h3 class="card-title">Edit Admin User</h3>
                            </div>
                            <div class="card-body">
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="name">Name:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="name" id="name" class="form-control" value="{{$users->name}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="username">Username:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <input type="text" name="username" id="username" class="form-control" value="{{$users->email}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="password">Password:</label>
                                <div class="col-sm-9">
                                  <input type="password" name="password" id="password" class="form-control">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="control-label col-sm-3" for="status">Status:<span class="note">*</span></label>
                                <div class="col-sm-9">
                                  <select name="status" id="status" class="form-control">
                                    <option value="">Select</option>
                                    <option <?=(($users->status == 1)?'selected':'');?> value="1">Enable</option>
                                    <option <?=(($users->status == 2)?'selected':'');?> value="2">Disable</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-script')
<script type="text/javascript">
$(document).ready(function(){
  $(".user-form").validate({
    rules:
    {
      name: "required",
      username: "required",
      password: {
        required: false,
        minlength: 4
      },
      status : "required"
    },
    messages:
    {
      name: "Please enter name",
      username: "Please enter login user name",
      password:'Please enter minimum 4 length password',
      status: "Please choose user status"
    }
  });
});
</script>
@endsection