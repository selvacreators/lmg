@extends('layouts.layout_home')

@section('content')

@include('includes.header')

<main class="content-wrapper">
<div class="page-content">

	<section class="error-page custom-bg-404 parallax">
		<div class="background-image">
			<img src="{{ Config::get('app.head_url') }}frontend/images/parallax/contact-page-bg-2.jpg">
		</div>
		<div class="container">
			<div class="row">
				<div class="max-width70 mr-auto">
					<div class="text-center">
						<h2>SORRY! WE CAN NOT FIND THAT PAGE.<br>REST EASY, WE'LL GET YOU BACK ON YOUR WAY</h2>
						<h3>Feel free to return to the homepage, or contact us to find what you were looking for. We're sorry for the inconvenience.</h3>
					</div>
					@if(!empty($res_menu))
					<?php
					$cname = 'contact';
					$clink = 'contact';
					$locale = url(Session::get('locale'));
					foreach ($res_menu as $key => $value)
					{
						if($value->menu_name == 'Contact')
						{
							$cname = $value->menu_name;
							$clink = $value->menu_link;
						}
					}?>
					@endif
					<div class="buttons mt30 mb20">
						<div class="mr-auto text-center">
							<a href="{{ URL('/') }}" class="btn btn-default">Back To Home</a>
							<a href="{{ URL($locale.'/'.$clink.'/') }}" class="btn btn-default">{{$cname}}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- <div style="min-height: 514px;">
	  <h2 style="min-height: 100px;padding-top: 250px;padding-bottom: 80px;"><center>Page not found</center></h2>
	</div> -->
</div>
</main>
@include('includes.footer')

@stop