            var chart;

            var chartData = [
                {
                    "country": "Silver",
                    "visits": 4025
                },
                {
                    "country": "Gold",
                    "visits": 1882
                },
                {
                    "country": "Platinum",
                    "visits": 1809
                }
            ];

            var chartData2 = [
                {
                    "name": "Total Packages Sold",
                    "open": 0,
                    "close": 2000,
                    "color": "#169b2f",
                    "balloonValue": 2000
                },
                {
                    "name": "Q - 1",
                    "open": 2000,
                    "close": 1500,
                    "color": "#5A130A",
                    "balloonValue": 500
                },
                {
                    "name": "Q - 2",
                    "open": 1500,
                    "close": 800,
                    "color": "#5A130A",
                    "balloonValue": 700
                },
                {
                    "name": "Q - 3",
                    "open": 800,
                    "close": 500,
                    "color": "#5A130A",
                    "balloonValue": 300
                },
                {
                    "name": "Q - 4",
                    "open": 500,
                    "close": 0,
                    "color": "#5A130A",
                    "balloonValue": 500
                },
            ];

            var chartData3 = [
                {
                    "name": "Total Packages",
                    "open": 0,
                    "close": 2000,
                    "color": "#71180C",
                    "balloonValue": 2000
                },
                {
                    "name": "May",
                    "open": 2000,
                    "close": 1500,
                    "color": "#007A93",
                    "balloonValue": 500
                },
                {
                    "name": "Apr",
                    "open": 1500,
                    "close": 800,
                    "color": "#007A93",
                    "balloonValue": 700
                },
                {
                    "name": "Mar",
                    "open": 800,
                    "close": 500,
                    "color": "#007A93",
                    "balloonValue": 300
                },
                {
                    "name": "Feb",
                    "open": 500,
                    "close": 200,
                    "color": "#007A93",
                    "balloonValue": 300
                },
                {
                    "name": "Jan",
                    "open": 200,
                    "close": 0,
                    "color": "#007A93",
                    "balloonValue": 200
                },
            ];

            var chartData4 = [
                {
                    "name": "Total Packages Sold",
                    "open": 0,
                    "close": 5000,
                    "color": "#169b2f",
                    "balloonValue": 5000
                },
                {
                    "name": "2017",
                    "open": 5000,
                    "close": 3000,
                    "color": "#444444",
                    "balloonValue": 2000
                },
                {
                    "name": "2016",
                    "open": 3000,
                    "close": 2000,
                    "color": "#444444",
                    "balloonValue": 1000
                },
                {
                    "name": "2015",
                    "open": 2000,
                    "close": 500,
                    "color": "#444444",
                    "balloonValue": 1500
                },
                {
                    "name": "2014",
                    "open": 500,
                    "close": 200,
                    "color": "#444444",
                    "balloonValue": 300
                },
                {
                    "name": "2013",
                    "open": 200,
                    "close": 0,
                    "color": "#444444",
                    "balloonValue": 200
                },
            ];

            var chartData5 = [
                {
                    "Model": 'FECCD6LGSK',
                    "Silver": 300,
                    "Gold": 230,
                    "Platinum": 145,
                    "Customized": 211
                },
                {
                    "Model": 'FE84CE6ADK',
                    "Silver": 231,
                    "Gold": 132,
                    "Platinum": 411,
                    "Customized": 312
                },
                {
                    "Model": 'F4CE6WLGSK',
                    "Silver": 231,
                    "Gold": 235,
                    "Platinum": 275,
                    "Customized": 135
                },
                {
                    "Model": 'FE84CG6ADK',
                    "Silver": 295,
                    "Gold": 235,
                    "Platinum": 265,
                    "Customized": 278
                },
                {
                    "Model": 'FE84CE6LBH2',
                    "Silver": 246,
                    "Gold": 230,
                    "Platinum": 285,
                    "Customized": 255
                }
            ];

            var chartData6 = [
                {
                    package_type: "Silver",
                    package_data: 100
                }, {
                    package_type: "Gold",
                    package_data: 75
                }, {
                    package_type: "Platinum",
                    package_data: 50
                },
                {
                    package_type: "Customized",
                    package_data: 50
                }
            ];

            var chartData7 = [
                {
                    "date": "2009-10-02",
                    "value": 5
                },
                {
                    "date": "2009-10-03",
                    "value": 15
                },
                {
                    "date": "2009-10-04",
                    "value": 13
                },
                {
                    "date": "2009-10-05",
                    "value": 17
                },
                {
                    "date": "2009-10-06",
                    "value": 15
                },
                {
                    "date": "2009-10-09",
                    "value": 19
                },
                {
                    "date": "2009-10-10",
                    "value": 21
                },
                {
                    "date": "2009-10-11",
                    "value": 20
                },
                {
                    "date": "2009-10-12",
                    "value": 20
                },
                {
                    "date": "2009-10-13",
                    "value": 19
                },
                {
                    "date": "2009-10-16",
                    "value": 25
                },
                {
                    "date": "2009-10-17",
                    "value": 24
                },
                {
                    "date": "2009-10-18",
                    "value": 26
                },
                {
                    "date": "2009-10-19",
                    "value": 27
                },
                {
                    "date": "2009-10-20",
                    "value": 25
                },
                {
                    "date": "2009-10-23",
                    "value": 29
                },
                {
                    "date": "2009-10-24",
                    "value": 28
                },
                {
                    "date": "2009-10-25",
                    "value": 30
                },
                {
                    "date": "2009-10-26",
                    "value": 72
                },
                {
                    "date": "2009-10-27",
                    "value": 43
                },
                {
                    "date": "2009-10-30",
                    "value": 31
                },
                {
                    "date": "2009-11-01",
                    "value": 30
                },
                {
                    "date": "2009-11-02",
                    "value": 29
                },
                {
                    "date": "2009-11-03",
                    "value": 27
                },
                {
                    "date": "2009-11-04",
                    "value": 26
                }
            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "country";
                chart.startDuration = 1;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.labelRotation = 90;
                categoryAxis.gridPosition = "start";

                // value
                // in case you don't want to change default settings of value axis,
                // you don't need to create it, as one value axis is created automatically.

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "visits";
                graph.balloonText = "[[category]]: <b>[[value]]</b>";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 0.8;
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorAlpha = 0;
                chartCursor.zoomable = false;
                chartCursor.categoryBalloonEnabled = false;
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-right";

                chart.write("chartdiv");
            });


            AmCharts.ready(function () {
                // Waterfall chart is a simple serial chart with some specific settings
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData2;
                chart.categoryField = "name";
                chart.columnWidth = 0.6;
                chart.startDuration = 1;

                // AXES
                // Category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0.1;
                categoryAxis.axisAlpha = 0;
                categoryAxis.gridPosition = "start";

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.gridAlpha = 0.1;
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "close";
                graph.openField = "open";
                graph.type = "column";
                graph.lineAlpha = 1;
                graph.lineColor = "#BBBBBB";
                graph.colorField = "color";
                graph.fillAlphas = 0.8;
                graph.balloonText = "<span style='color:[[color]]'>[[category]]</span><br><span style='font-size:13px;'><b>[[balloonValue]]</b></span>";
                graph.labelText = "[[balloonValue]]";
                chart.addGraph(graph);

                // Trend lines used for connectors
                

                // WRITE
                chart.write("chartdiv1");
            });


            AmCharts.ready(function () {
                // Waterfall chart is a simple serial chart with some specific settings
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData3;
                chart.categoryField = "name";
                chart.columnWidth = 0.6;
                chart.startDuration = 1;

                // AXES
                // Category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0.1;
                categoryAxis.axisAlpha = 0;
                categoryAxis.gridPosition = "start";

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.gridAlpha = 0.1;
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "close";
                graph.openField = "open";
                graph.type = "column";
                graph.lineAlpha = 1;
                graph.lineColor = "#BBBBBB";
                graph.colorField = "color";
                graph.fillAlphas = 0.8;
                graph.balloonText = "<span style='color:[[color]]'>[[category]]</span><br><span style='font-size:13px;'><b>[[balloonValue]]</b></span>";
                graph.labelText = "[[balloonValue]]";
                chart.addGraph(graph);

                // Trend lines used for connectors
                

                // WRITE
                chart.write("chartdiv2");
            });


            AmCharts.ready(function () {
                // Waterfall chart is a simple serial chart with some specific settings
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData4;
                chart.categoryField = "name";
                chart.columnWidth = 0.6;
                chart.startDuration = 1;

                // AXES
                // Category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0.1;
                categoryAxis.axisAlpha = 0;
                categoryAxis.gridPosition = "start";

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.gridAlpha = 0.1;
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "close";
                graph.openField = "open";
                graph.type = "column";
                graph.lineAlpha = 1;
                graph.lineColor = "#BBBBBB";
                graph.colorField = "color";
                graph.fillAlphas = 0.8;
                graph.balloonText = "<span style='color:[[color]]'>[[category]]</span><br><span style='font-size:13px;'><b>[[balloonValue]]</b></span>";
                graph.labelText = "[[balloonValue]]";
                chart.addGraph(graph);

                // Trend lines used for connectors
                

                // WRITE
                chart.write("chartdiv3");
            });


            AmCharts.ready(function () {
                // Waterfall chart is a simple serial chart with some specific settings
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData5;
                chart.categoryField = "Model";
                chart.startDuration = 1;
                chart.plotAreaBorderColor = "#DADADA";
                chart.plotAreaBorderAlpha = 1;
                // this single line makes the chart a bar chart
                chart.rotate = true;

                // AXES
                // Category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridPosition = "start";
                categoryAxis.gridAlpha = 0.1;
                categoryAxis.axisAlpha = 0;

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                valueAxis.gridAlpha = 0.1;
                valueAxis.position = "top";
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // first graph
                var graph1 = new AmCharts.AmGraph();
                graph1.type = "column";
                graph1.title = "Silver";
                graph1.valueField = "Silver";
                graph1.balloonText = "Silver:[[value]]";
                graph1.lineAlpha = 0;
                graph1.fillColors = "#707070";
                graph1.fillAlphas = 1;
                chart.addGraph(graph1);

                // second graph
                var graph2 = new AmCharts.AmGraph();
                graph2.type = "column";
                graph2.title = "Gold";
                graph2.valueField = "Gold";
                graph2.balloonText = "Gold:[[value]]";
                graph2.lineAlpha = 0;
                graph2.fillColors = "#71180C";
                graph2.fillAlphas = 1;
                chart.addGraph(graph2);

                // second graph
                var graph3 = new AmCharts.AmGraph();
                graph3.type = "column";
                graph3.title = "Platinum";
                graph3.valueField = "Platinum";
                graph3.balloonText = "Platinum:[[value]]";
                graph3.lineAlpha = 0;
                graph3.fillColors = "#E6E6E6";
                graph3.fillAlphas = 1;
                chart.addGraph(graph3);

                // second graph
                var graph4 = new AmCharts.AmGraph();
                graph4.type = "column";
                graph4.title = "Customized";
                graph4.valueField = "Customized";
                graph4.balloonText = "Customized:[[value]]";
                graph4.lineAlpha = 0;
                graph4.fillColors = "#00677F";
                graph4.fillAlphas = 1;
                chart.addGraph(graph4);

                // LEGEND
                var legend = new AmCharts.AmLegend();
                chart.addLegend(legend);

                chart.creditsPosition = "top-right";

                // Trend lines used for connectors
                

                // WRITE
                chart.write("chartdiv4");
            });


            AmCharts.ready(function () {
            
              // Waterfall chart is a simple serial chart with some specific settings
                chart = new AmCharts.AmPieChart();
                chart.colors = ["#E6E6E6", "#00677F", "#71180C", "#C8C8C8", "#00566A", "#5A130A", "#9E9E9E", "#004355", "#440E07", "#707070", "#5097AB", "#444444", "#A6CAD8", "#4B0C25"];
                chart.dataProvider = chartData6;
                chart.titleField = "package_type";
                chart.valueField = "package_data";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 2;
                chart.labelsEnabled = false;
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>";
                
                // Trend lines used for connectors
                
                // LEGEND
                var legend = new AmCharts.AmLegend();
                chart.addLegend(legend);


                // WRITE
                chart.write("chartdiv5");
            });


createFromToFields();

            // create from/to field values programatically
            function createFromToFields(){
                for(var i = 0; i < chartData.length; i++){
                    var value = chartData[i].value;
                    chartData[i].fromValue = value - value * 0.2;
                    chartData[i].toValue = value + value * 0.2;
                }
            }

            AmCharts.ready(function () {
                // Waterfall chart is a simple serial chart with some specific settings
                var chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData7;

                chart.categoryField = "date";
                chart.dataDateFormat = "YYYY-MM-DD";

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
                categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
                categoryAxis.gridAlpha = 0;
                categoryAxis.tickLength = 0;
                categoryAxis.axisAlpha = 0;
                
                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.dashLength = 4;
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);


                // FROM GRAPH
                var fromGraph = new AmCharts.AmGraph();
                fromGraph.type = "line";
                fromGraph.valueField = "fromValue";
                fromGraph.lineAlpha = 0;
                fromGraph.showBalloon = false;
                chart.addGraph(fromGraph);

                // TO GRAPH
                var toGraph = new AmCharts.AmGraph();
                toGraph.type = "line";
                toGraph.valueField = "toValue";
                toGraph.lineAlpha = 0;
                toGraph.fillAlphas = 0.2;
                toGraph.fillToGraph = fromGraph;
                toGraph.showBalloon = false;
                chart.addGraph(toGraph);


                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.type = "line";
                graph.valueField = "value";
                graph.lineColor = "yellow";
            
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-right";
                

                // WRITE
                chart.write("chartdiv6");
            });
        
		
		
		
        // Hema Rajeshwari
            
            
 
var chart = AmCharts.makeChart("chartdiv7", {
    "type": "serial",
    "theme": "light",
    "legend": {
    
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
        "useGraphSettings": true,
        "markerSize": 10
    },
    "dataProvider": [{
        "year": "Mar",
        "europe": 200,
        "namerica": 100,
        
    }, {
        "year": "Apr",
        "europe": 300,
        "namerica": 400,
       
    }, {
        "year": "May",
        "europe": 300,
        "namerica": 200,
        
    }],
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.5,
        "gridAlpha": 0
       
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Unsold",
        "type": "column",
        "color": "#000000",
		"fillColors": "#B5B5B5",
        "valueField": "europe"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Sold",
        "type": "column",
        "color": "#000000",
        "valueField": "namerica"
    }, ],
  //  "rotate": true,
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
    "export": {
        "enabled": true
     }
});