$( document ).ready(function() {
     $('#alertbox').modal('show'); 
});
$( document ).ready(function() {
  $('.form-register').bootstrapValidator({
                ignore: ":hidden",
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
               
                name: {
                    validators: {
                        notEmpty: {
                            message: "Name is required "
                        },
                        stringLength: {
                            min: 3,max: 16,message: 'Name should range between 3 to 16 characters'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: "Password is required "
                        },
                        stringLength: {
                            min: 3,max: 16,message: 'Password should range between 3 to 16 characters'
                        }
                    }
                },
                 email: {
                    validators: {
                        notEmpty: {
                            message: 'UserId is required'
                        },
                        stringLength: {
                            max: 128,
                        }
                    }
                },
                

            }

        });  
});