$(document).ready(function () {

    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
});

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

  $( function() {
    $( "#datepicker" ).datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: '+0D',
        onSelect: function(dateText, inst) {

                var maxday = parseInt($('#contract_term').val());

                var maxDate = $(this).datepicker("getDate");

                var set = new Date(maxDate.getFullYear()+maxday,maxDate.getMonth(),maxDate.getDate()-1);
                var set1 = formatDate(set);
                $("#expiry_month").val(set1);

        }
    });
  });





  $( function() {
    $( "#datepicker1" ).datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: '+0D',
        onSelect: function(dateText, inst) {

                var maxday = parseInt($('#contract_term').val());

                var maxDate = $(this).datepicker("getDate");

                var set = new Date(maxDate.getFullYear()+maxday,maxDate.getMonth(),maxDate.getDate()-1);
                var set1 = formatDate(set);
                $("#expiry_month").val(set1);

        }
    });
     $( "#expiry_month" ).datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: '+0D'
         });
 
    
 
  });






  $( function() {
    $( "#datepicker2" ).datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: '+0D'
    });
  });

$(document).on('change', '#market_name', function () {
    $("#market_name1").val($(this).find(":selected").text());
});


$(document).on('change', '#model_name', function () {
    $("#model_name1").val($(this).find(":selected").text());
});

$(document).on('change', '#model_id1', function () {
    $("#model_name1").val($(this).find(":selected").text());
});

$(document).on('change', '#book_id1', function () {
    $("#book_name1").val($(this).find(":selected").text());
});

$(document).on('change', '#book_name', function () {
    $("#book_name1").val($(this).find(":selected").text());
});

$(document).on('change', '#market_id1', function () {
    $("#market_name1").val($(this).find(":selected").text());
});

$(document).on('change', '#market_name', function () {


       var book_id = $(this).val(); 

       $('#booksel_name').empty();
       $('#booksel_id1').empty();

       $('#booksel_name').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));
       $('#booksel_id1').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/linkbookactivities',
                    data : { "_token": token, "book_id": book_id },

                    success : function(response) {
                        

                        $('#booksel_name').empty();
                        $('#booksel_id1').empty();

                        $('#booksel_name').append($("<option disabled selected></option>").attr("value","").text("Select"));
                        $('#booksel_id1').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response, function(key,value) {
                            $('#booksel_name').append($("<option></option>").attr("value",key).text(value));
                            $('#booksel_id1').append($("<option></option>").attr("value",key).text(value));
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});

$(document).on('change', '#market_name', function () {


       var book_id = $(this).val(); 

       $('#modelsel_id1').empty();
      /* $('#booksel_id1').empty();*/

       $('#modelsel_id1').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));
       /*$('#booksel_id1').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));*/


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/linkmarketactivities',
                    data : { "_token": token, "book_id": book_id },

                    success : function(response) {
                        

                        $('#modelsel_id1').empty();
                       /* $('#booksel_id1').empty();*/

                        $('#modelsel_id1').append($("<option disabled selected></option>").attr("value","").text("Select"));
                       /* $('#booksel_id1').append($("<option disabled selected></option>").attr("value","").text("Select"));*/

                         $.each(response, function(key,value) {
                            $('#modelsel_id1').append($("<option></option>").attr("value",key).text(value));
                            /*$('#booksel_id1').append($("<option></option>").attr("value",key).text(value));*/
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});
$(document).on('change', '#book_id1', function () {


       var book_id = $(this).val(); 

       $('#bookactivities').empty();

       $('#bookactivities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/bookparts_activities',
                    data : { "_token": token, "book_id": book_id },

                    success : function(response) {
                        

                        $('#bookactivities').empty();

                        $('#bookactivities').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response, function(key,value) {
                            $('#bookactivities').append($("<option></option>").attr("value",key).text(value));
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});

$(document).on('change', '#bookactivities', function () {
    $("#bookactivities1").val($(this).find(":selected").text());

     var bookactivities = $(this).val(); 

       $('#booksub_activities').empty();

       $('#booksub_activities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/bookparts_subactivities',
                    data : { "_token": token, "booklist_id": bookactivities },

                    success : function(response) {
                        

                        $('#booksub_activities').empty();

                        $('#booksub_activities').append($("<option disabled selected></option>").attr("value","").text("Select"));
                        
                        $('#booksub_activities').append($("<option></option>").attr("value","").text("Over All"));

                         $.each(response, function(key,value) {
                            $('#booksub_activities').append($("<option></option>").attr("value",key).text(value));
                        });

                         if(response == '')
                         {
                            $('#booksub_activities').empty();
                            $('#booksub_activities').append($("<option selected></option>").attr("value","").text("Over All"));
                         }
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});
$(document).on('change', '#book_name', function () {


       var book_id = $(this).val(); 

       $('#bookactivities').empty();
       $('#bookactivities_labour').empty();

       $('#bokactivities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));
       $('#bookactivities_labour').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/bookactivities',
                    data : { "_token": token, "book_id": book_id },

                    success : function(response) {
                        

                        $('#bookactivities').empty();
                        $('#bookactivities_labour').empty();

                        $('#bookactivities').append($("<option disabled selected></option>").attr("value","").text("Select"));
                        $('#bookactivities_labour').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response, function(key,value) {
                            $('#bookactivities').append($("<option></option>").attr("value",key).text(value));
                            $('#bookactivities_labour').append($("<option></option>").attr("value",key).text(value));
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});


$(document).on('change', '#bookactivities_labour', function () {
    $("#bookactivities1").val($(this).find(":selected").text());

     var bookactivities = $(this).val(); 

       $('#booksub_activities').empty();

       $('#booksub_activities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/labour_booksubactivities',
                    data : { "_token": token, "booklist_id": bookactivities },

                    success : function(response) {
                        

                        $('#booksub_activities').empty();

                        $('#booksub_activities').append($("<option disabled selected></option>").attr("value","").text("Select"));
                        
                        $('#booksub_activities').append($("<option></option>").attr("value","").text("Over All"));

                         $.each(response, function(key,value) {
                            $('#booksub_activities').append($("<option></option>").attr("value",key).text(value));
                        });

                         if(response == '')
                         {
                            $('#booksub_activities').empty();
                            $('#booksub_activities').append($("<option selected></option>").attr("value","").text("Over All"));
                         }
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});


$(document).on('change', '#activities', function () {
    $("#activities1").val($(this).find(":selected").text());

     var activities = $(this).val(); 

       $('#sub_activities').empty();

       $('#sub_activities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/parts_subactivities',
                    data : { "_token": token, "activities_id": activities },

                    success : function(response) {
                        

                        $('#sub_activities').empty();

                        $('#sub_activities').append($("<option disabled selected></option>").attr("value","").text("Select"));
						
						$('#sub_activities').append($("<option></option>").attr("value","").text("Over All"));

                         $.each(response, function(key,value) {
                            $('#sub_activities').append($("<option></option>").attr("value",key).text(value));
                        });

                         if(response == '')
                         {
							$('#sub_activities').empty();
                            $('#sub_activities').append($("<option selected></option>").attr("value","").text("Over All"));
                         }
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});

$(document).on('change', '#activities_labour', function () {
    $("#activities1").val($(this).find(":selected").text());

     var activities = $(this).val(); 

       $('#sub_activities').empty();

       $('#sub_activities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/labour_subactivities',
                    data : { "_token": token, "activities_id": activities },

                    success : function(response) {
                        

                        $('#sub_activities').empty();

                        $('#sub_activities').append($("<option disabled selected></option>").attr("value","").text("Select"));
						
						$('#sub_activities').append($("<option></option>").attr("value","").text("Over All"));

                         $.each(response, function(key,value) {
                            $('#sub_activities').append($("<option></option>").attr("value",key).text(value));
                        });

                         if(response == '')
                         {
							$('#sub_activities').empty();
                            $('#sub_activities').append($("<option selected></option>").attr("value","").text("Over All"));
                         }
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});

$(document).on('keyup', '#interval_kms', function () {
    $("#interval_miles").val(Math.round($(this).val() * 0.62));
});

$( document ).ready(function() {
     $('#alertbox').modal('show'); 
});


$(document).on('keyup', '#odometer', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});

$(document).on('blur', '#odometer', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});

$(document).on('keyup', '#warrenty_period_month', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});

$(document).on('keyup', '#warrenty_period', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});

/*$(document).on('keyup', '#km_year', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});

$(document).on('keyup', '#km_year', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});

$(document).on('blur', '#km_year', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});

$(document).on('keyup', '#km_year', function () {

    var contract_term = $("#contract_term").val();
    var km_year = $("#km_year").val();

    $("#total_km").val(contract_term * km_year);
});

$(document).on('blur', '#km_year', function () {

    var contract_term = $("#contract_term").val();
    var km_year = $("#km_year").val();

    $("#total_km").val(contract_term * km_year);
});
*/




$(document).on('keyup', '#contract_start_km', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});




$(document).on('blur', '#contract_start_km', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});


$(document).on('keyup', '#contract_end_km', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});




$(document).on('blur', '#contract_end_km', function () {

    $(this).val($(this).val().replace(/\D/, ''));
});


$(document).on('keyup', '#contract_start_km', function () {

    var contract_term = $("#contract_term").val();
    var contract_start_km = $("#contract_start_km").val();
    var contract_end_km = $("#contract_end_km").val();
var total= contract_end_km-contract_start_km;
var final_tot=(total/contract_term);

    var final_tot1=Math.floor(final_tot);
    $("#km_year").val(final_tot1);
});




$(document).on('blur', '#contract_start_km', function () {

     var contract_term = $("#contract_term").val();
    var contract_start_km = $("#contract_start_km").val();
    var contract_end_km = $("#contract_end_km").val();
var total= contract_end_km-contract_start_km;
var final_tot=(total/contract_term);

var final_tot1=Math.floor(final_tot);
    $("#km_year").val(final_tot1);
});














$(document).on('keyup', '#contract_end_km', function () {

    var contract_term = $("#contract_term").val();
    var contract_start_km = $("#contract_start_km").val();
    var contract_end_km = $("#contract_end_km").val();
var total= contract_end_km-contract_start_km;
var final_tot=(total/contract_term);



    var final_tot1=Math.floor(final_tot);
    $("#km_year").val(final_tot1);
});




$(document).on('blur', '#contract_end_km', function () {

     var contract_term = $("#contract_term").val();
    var contract_start_km = $("#contract_start_km").val();
    var contract_end_km = $("#contract_end_km").val();
var total= contract_end_km-contract_start_km;
var final_tot=(total/contract_term);

var final_tot1=Math.floor(final_tot);
    $("#km_year").val(final_tot1);
});

$(document).on('change', '#contract_term', function () {

    var contract_term = $("#contract_term").val();
   // var km_year = $("#km_year").val();

//    $("#total_km").val(contract_term * km_year);

  var contract_start_km = $("#contract_start_km").val();
    var contract_end_km = $("#contract_end_km").val();
var total= contract_end_km-contract_start_km;
var final_tot=(total/contract_term);

var final_tot1=Math.floor(final_tot);

$("#km_year").val(final_tot1);


    var maxday = parseInt($('#contract_term').val());

    var maxDate = $('.contract_starts').datepicker("getDate");

    var set = new Date(maxDate.getFullYear()+maxday,maxDate.getMonth(),maxDate.getDate()-1);
    var set1 = formatDate(set);

    $("#expiry_month").val(set1);

});


$(document).on('click', '#menu-toggle', function () {
    $("#img-world").toggle();
});

$(document).on('change', '#user_type', function () {
    var user_type = $("#user_type").val();
    if(user_type == 4)
    {
        $("#market_col").css("display","block");
        $("#market").attr("disabled",false);
    }
    else
    {
        $("#market_col").css("display","none");
        $("#market").attr("disabled",true);
    }
});



$(document).on('change', '#activities', function () {
    var model_name = $("#model_name").val();
    var activities = $(this).val();

    $("#getparts").attr("href","/GD/getParts/"+model_name+"/"+activities);
    $("#getlabor_hr").attr("href","/GD/getlaborhr/"+model_name+"/"+activities);
});



$(document).on('change', '#group_name', function () {


       var group_id = $(this).val(); 

       $('#subgroup_name').empty();

       $('#subgroup_name').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/subgroup_name',
                    data : { "_token": token, "group_id": group_id },

                    success : function(response) {
                        

                        $('#subgroup_name').empty();

                        $('#groupname_text').val($('#group_name').find(":selected").text());

                        $('#subgroup_name').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response, function(key,value) {
                            $('#subgroup_name').append($("<option></option>").attr("value",value).text(value));
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});


$(document).on('change', '#model_id1', function () {


       var model_id = $(this).val(); 

       $('#activities').empty();

       $('#activities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/parts_activities',
                    data : { "_token": token, "model_id": model_id },

                    success : function(response) {
                        

                        $('#activities').empty();

                        $('#activities').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response, function(key,value) {
                            $('#activities').append($("<option></option>").attr("value",key).text(value));
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});
$(document).on('change', '#model_name', function () {


       var model_id = $(this).val(); 

       $('#activities').empty();
       $('#activities_labour').empty();

       $('#activities').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));
       $('#activities_labour').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/activities',
                    data : { "_token": token, "model_id": model_id },

                    success : function(response) {
                        

                        $('#activities').empty();
                        $('#activities_labour').empty();

                        $('#activities').append($("<option disabled selected></option>").attr("value","").text("Select"));
                        $('#activities_labour').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response, function(key,value) {
                            $('#activities').append($("<option></option>").attr("value",key).text(value));
                            $('#activities_labour').append($("<option></option>").attr("value",key).text(value));
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});





$(document).on('change', '#model_package', function () {


       var model_package = $(this).val(); 
       var market = $("#market").val(); 

       $('#packages').empty();

       $('#packages').append($("<option disabled selected ></option>").attr("value","").text("Just a Sec..."));


                
                var token = $("#_token").val();

                $.ajax({
                    type    : 'POST',
                    dataType: 'json',
                    url     : '/index.php/get/model_package',
                    data : { "_token": token, "model_name": model_package, "market": market },

                    success : function(response) {
                        

                        $('#packages').empty();

                        $('#packages').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response[0], function(key,value) {
                            $('#packages').append($("<option></option>").attr("value",value).attr("data-package_id",key).text(value));
                        });

                        $('#packages_activities').empty();

                        $('#packages_activities').append($("<option disabled selected></option>").attr("value","").text("Select"));

                         $.each(response[1], function(k,v) {
                            $('#packages_activities').append($("<option></option>").attr("value",v.activities).attr("data-part_code",v.part_code).attr("data-interval_kms",v.interval_kms).attr("data-part_name",v.part_name).attr("data-part_number",v.part_number).text(v.activities +'-'+ v.part_code));
                        });
                    },
                    error   : function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(errorThrown);
                    }

        
                });
});