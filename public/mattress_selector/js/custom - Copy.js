var popup = $.cookie("popMeUp");
console.debug($.cookie("popMeUp"));
if (popup == '1') {
    //$("#redirectModel").modal("show");
}
var goto = 1;
var cookies = $.cookie();
for (var cookie in cookies) {
    $.removeCookie(cookie);
}

$("input[name=gender]").on('change', function() {
    var selValue = $("input[name=gender]:checked").val();
    var ques = $('body').find('.heading-title.q1').text();
    $("#app-gender").remove();
    if (selValue) {
        if ($("#app-gender").length) {

            var datas_gen1 = "<span id='app-gender'><span  class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue + ".</span></span>";
            $("#app-gender").html(datas_gen1);
        } else {


            var datas_gen2 = "<span id='app-gender'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue + ".</span></span>";
            $("#Mattress_AppendText").append(datas_gen2);
        }
    }


});

$("input[name=2people]").on('change', function() {
    var selValue1 = $("input[name=2people]:checked").val();
    var ques = $('body').find('#people .q2').text();
    if (selValue1) {

        if ($('#Mattress').length) {
            var datas1 = "<span id='Mattress'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue1 + ".</span></span>";
            $("#Mattress").html(datas1);
        } else {
            var datas1 = "<span id='Mattress'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue1 + ".</span></span>";
            $("#Mattress_AppendText").append(datas1);
        }

    }

});

//New One

$("input[name=lookingmattress]").on('change', function() {
    var lookingmattress = $("input[name=lookingmattress]:checked").val();
    var ques = $('body').find('#looking-mattress .q3').text();
    if (lookingmattress) {

        if ($('#look_Mattress').length) {
            var lookingmattress_datas1 = "<span id='look_Mattress'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + lookingmattress + ".</span></span>";
            $("#look_Mattress").html(lookingmattress_datas1);
        } else {
            var lookingmattress_datas1 = "<span id='look_Mattress'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + lookingmattress + ".</span></span>";
            $("#Mattress_AppendText").append(lookingmattress_datas1);
        }

    }

});

$("input[name=weightrange]").on('change', function() {
    var weightrange = $("input[name=weightrange]:checked").val();
    var ques = $('body').find('#weight-range .q4').text();
    var title = $('body').find('#weight-range.double .t1').text();

    if (weightrange) {

        if ($('#weight_range').length) {
            var weightrange_datas1 = "<span id='weight_range'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ":" + weightrange + ".</span></span>";
            $("#weight_range").html(weightrange_datas1);

        } else {
            var weightrange_datas1 = "<span id='weight_range'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ":" + weightrange + ".</span></span>";
            $("#Mattress_AppendText").append(weightrange_datas1);

        }

    }

});

$("input[name=partner_weightrange]").on('change', function() {
    var partner_weightrange = $("input[name=partner_weightrange]:checked").val();
    var title = $('body').find('#weight-range.double .t2').text();
    if (partner_weightrange) {

        if ($('#Partweight_range').length) {

            var partner_weightrange_datas1 = "<span id='Partweight_range'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ":" + partner_weightrange + ".</span></span>";
            $("#weight_range").html(partner_weightrange_datas1);
        } else {

            var partner_weightrange_datas1 = "<span id='Partweight_range'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ":" + partner_weightrange + ".</span></span>";
            $("#Mattress_AppendText").append(partner_weightrange_datas1);
        }

    }

});

$("input[name=agerange]").on('change', function() {
    var agerange = $("input[name=agerange]:checked").val();
    var ques = $('body').find('#age-range .q5').text();
    if (agerange) {

        if ($('#agerange').length) {
            var agerange_datas1 = "<span id='agerange'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + agerange + ".</span></span>";
            $("#agerange").html(agerange_datas1);
        } else {
            var agerange_datas1 = "<span id='agerange'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + agerange + ".</span></span>";
            $("#Mattress_AppendText").append(agerange_datas1);
        }

    }

});

$("input[name=partner_agerange]").on('change', function() {
    var partner_agerange = $("input[name=partner_agerange]:checked").val();
    var ques = $('body').find('#weight-range .q4').text();
    if (partner_agerange) {

        if ($('#Partner_agerange').length) {
            var partner_weightrange_datas1 = "<span id='Partner_agerange'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + partner_weightrange + ".</span></span>";
            $("#Partner_agerange").html(partner_weightrange_datas1);
        } else {
            var partner_agerange_datas1 = "<span id='Partner_agerange'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + partner_agerange + ".</span></span>";
            $("#Mattress_AppendText").append(partner_agerange_datas1);
        }

    }

});

$("input[name=yourposition]").on('change', function() {
    var yourposition = $("input[name=yourposition]:checked").val();
    var ques = $('body').find('#sleep-position .q6').text();
    if (yourposition) {

        if ($('#your_position').length) {
            var yourposition_datas1 = "<span id='your_position'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + yourposition + ".</span></span>";
            $("#your_position").html(yourposition_datas1);
        } else {
            var yourposition_datas1 = "<span id='your_position'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + yourposition + ".</span></span>";
            $("#Mattress_AppendText").append(yourposition_datas1);
        }

    }

});

$("input[name=partnerposition]").on('change', function() {
    var partnerposition = $("input[name=partnerposition]:checked").val();
    var ques = $('body').find('#sleep-position .q6').text();
    if (partnerposition) {

        if ($('#partner_position').length) {
            var partner_position_datas = "<span id='partner_position'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + partnerposition + ".</span></span>";
            $("#partner_position").html(partner_position_datas);
        } else {
            var partner_position_data1 = "<span id='partner_position'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + partnerposition + ".</span></span>";
            $("#Mattress_AppendText").append(partner_position_data1);
        }

    }

});


$("input[name=issues]").on('change', function() {
    var issues = $("input[name=issues]:checked").val();
    var ques = $('body').find('#issues .q7').text();
    if (issues) {

        if ($('#issues_data').length) {
            var issues_data = "<span id='issues_data'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + issues + ".</span></span>";
            $("#issues_data").html(issues_data);
        } else {
            var issues_data1 = "<span id='issues_data'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + issues + ".</span></span>";
            $("#Mattress_AppendText").append(issues_data1);
        }

    }

});

$("input[name=hotcold]").on('change', function() {
    var hotcold = $("input[name=hotcold]:checked").val();
    var ques = $('body').find('#hot-cold .q8').text();
    if (hotcold) {

        if ($('#hotcold_data').length) {
            var issues_data = "<span id='hotcold_data'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + hotcold + ".</span></span>";
            $("#hotcold_data").html(hotcold_data);
        } else {
            var hotcold_data1 = "<span id='hotcold_data'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + hotcold + ".</span></span>";
            $("#Mattress_AppendText").append(hotcold_data1);
        }

    }

});

$("input[name=comfortablesleep]").on('change', function() {
    var selValue3 = $("input[name=comfortablesleep]:checked").val();
    var ques = $('body').find('#comfortable-sleep .q9').text();

    if (selValue3) {
        if ($('#finalsleep_data').length) {
            // console.log('check leffvel1');
            $('#finalsleep_data').text('');
            var datas2 = "<span id='finalsleep_data'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue3 + ".</span></span>";
            $("#Mattress_AppendText").append(datas2);
        } else {
            //console.log('check leffvel2');
            var datas3 = "<span id='finalsleep_data'><span class='ques'>" + ques + "=</span> <span class='ans'>" + selValue3 + ". </span></span>";
            $("#Mattress_AppendText").append(datas3);
        }

    }

});




$(".next-slide, .right").click(function() {

    //alert( $(".active ").attr('class') )

    var section = $(this).attr('data-section');
    var title = $(this).attr('data-title');
    console.log('section - ' + section);
    console.log('title - ' + title);
    var proceed = false;

    // ------------ Matress Start VALIDATION ----------//
    if (section == 'mstart') {
        var mstartValid = false;
        var mstartInput = $("input[name=mstart]").val();
        if (mstartInput != '' && mstartInput != undefined) {
            mstartValid = true;
            proceed = true;
        }
        if (mstartValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Mattress Selector'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('mattress_start', title);
            $("#myCarousel").carousel('next');
        }
    }

    // ------------ GENDER VALIDATION ----------//
    if (section == 'gender') {

        var genderValid = false;
        if (title == 'spare bedroom') {

            $("#people").addClass('item').removeClass('hidden');
            $("#looking-mattress, #weight-range, #age-range, #sleep-position, #issues, #hot-cold").addClass('hidden').removeClass('item');

        }

        // if(title == 'Myself' || title == 'My partner & I' || title == 'My partner, I & My kids'){

        //      $("#looking-mattress").addClass('item').removeClass('hidden');
        //      $("#people").addClass('hidden').removeClass('item');                     
        // }

        if (title == 'Myself') {

            $("#looking-mattress").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $(".content-heading").hide();
            $("#weight-range,.partner-weight").addClass('hidden').removeClass('item');
            $("#age-range,.partner-age").addClass('hidden').removeClass('item');
            $("#sleep-position,.partner-position").addClass('hidden').removeClass('item');
        }

        if (title == 'My partner & I' || title == 'My partner, I & My kids') {

            $("#looking-mattress").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $(".content-heading").show();
            $("#weight-range,.partner-weight").addClass('item double').removeClass('hidden');
            $("#age-range,.partner-age").addClass('item double').removeClass('hidden');
            $("#sleep-position,.partner-position").addClass('item double').removeClass('hidden');
            //$("#weight-range,.partner-weight").addClass('hidden').removeClass('item');   
        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var genderRadio = $("input[name=gender]:checked").val();
        if (genderRadio != '' && genderRadio != undefined) {
            genderValid = true;
            proceed = true;
        }
        if (genderValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Why are you buying a mattress for?'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('compare_gender', title);
            $("#myCarousel").carousel('next');
        }

    }


    // ------------ Looking mattress ----------//
    if (section == 'looking-mattress') {
        var lookingValid = false;

        if (title == 'old and worn out' || title == 'getting married or moving' || title == 'Improve my quality of sleep' || title == 'Great night’s sleep at a hotel') {
            $("#weight-range").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

            //     //$("#looking-mattress, #weight-range, #age-range, #sleep-position, #issues, #hot-cold").addClass('hidden').removeClass('item');

        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var lookingRadio = $("input[name=lookingmattress]:checked").val();
        if (lookingRadio != '' && lookingRadio != undefined) {
            lookingValid = true;
            proceed = true;
        }
        if (lookingValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Why are you looking for a new mattress?'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('looking-mattress', title);
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ Weight Range ----------//
    if (section == 'weight-range') {
        var weightValid = false;

        if (title == '50-60 kg' || title == 'Less than 50 kg' || title == '60-70 kg' || title == 'above 70 kg') {

            $("#age-range").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');
        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var weightRadio = $("input[name=weightrange]:checked").val();
        var weightRadio1 = $("input[name=partner_weightrange]:checked").val();


        var compare_gender_name = $.cookie('compare_gender');
        if (compare_gender_name == "Myself") {

            if (weightRadio != '' && weightRadio != undefined) {
                weightValid = true;
                proceed = true;
            }
        }

        if (compare_gender_name != "Myself") {
            if (weightRadio != '' && weightRadio != undefined && weightRadio1 != '' && weightRadio1 != undefined) {
                weightValid = true;
                proceed = true;
            }
        }

        if (compare_gender_name == 'Myself') {

            //$("#looking-mattress").addClass('item').removeClass('hidden');
            //$("#people").addClass('hidden').removeClass('item');   
            $("#weight-range,.your-weight").addClass('item').removeClass('hidden');
            // $("#age-range,.partner-age").addClass('hidden').removeClass('item');   
            // $("#sleep-position,.partner-position").addClass('hidden').removeClass('item');   
        }

        if (compare_gender_name == 'My partner & I' || title == 'My partner, I & My kids') {

            $("#looking-mattress").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#weight-range,.partner-weight").addClass('item').removeClass('hidden');
        }



        if (weightValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select your weight range'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('weight-range', title);
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ Peopel ----------//
    if (section == 'people') {
        var genderValid = false;

        if (title != '') {
            $("#people").each(function() {
                $(this).removeClass('selected');
            });
            $(this).addClass('selected');
        }


        if (title == 'Less than 50 kg' || title == '50-60 kg' || title == '60-70 kg') {
            $("#comfortable-sleep").addClass('item').removeClass('hidden');
            $("#looking-mattress, #weight-range, #age-range, #sleep-position, #issues, #hot-cold ").addClass('hidden').removeClass('item');
        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }


        var weightValid = false;
        var weightRadio = $("input[name=2people]:checked").val();
        if (weightRadio != '' && weightRadio != undefined) {
            weightValid = true;
            proceed = true;
        }
        if (weightValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select your people'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {

            $.cookie('people', title);
            $("#myCarousel").carousel('next');
        }



    }


    // ------------ Age Range ----------//
    if (section == 'age-range') {
        var ageValid = false;

        if (title == 'Less than 30 yrs' || title == '30-35 yrs' || title == '36-45 yrs' || title == '46-55 yrs' || title == 'Above 55 yrs') {

            $("#sleep-position").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

        }


        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var ageRadio = $("input[name=agerange]:checked").val();
        var ageRadio1 = $("input[name=partner_agerange]:checked").val();


        var compare_gender_name = $.cookie('compare_gender');
        if (compare_gender_name == "Myself") {

            if (ageRadio != '' && ageRadio != undefined) {
                ageValid = true;
                proceed = true;
            }
        }

        if (compare_gender_name != "Myself") {
            if (ageRadio != '' && ageRadio != undefined && ageRadio1 != '' && ageRadio1 != undefined) {
                ageValid = true;
                proceed = true;
            }
        }




        if (ageValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select your age range'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('age-range', title);
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ sleep position ----------//
    if (section == 'sleep-position') {
        var sleepValid = false;

        if (title == 'Back' || title == 'Stomach' || title == 'Side' || title == 'Toss & turn') {

            $("#issues").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var sleepRadio = $("input[name=yourposition]:checked").val();
        var sleepRadio1 = $("input[name=partnerposition]:checked").val();


        var compare_gender_name = $.cookie('compare_gender');
        if (compare_gender_name == "Myself") {

            if (sleepRadio != '' && sleepRadio != undefined) {
                sleepValid = true;
                proceed = true;
            }
        }

        if (compare_gender_name != "Myself") {

            if (sleepRadio != '' && sleepRadio != undefined && sleepRadio1 != '' && sleepRadio1 != undefined) {
                sleepValid = true;
                proceed = true;
            }
        }




        if (sleepValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Sleep Position'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('sleep-position', title);
            $("#myCarousel").carousel('next');
        }


    }



    // ------------ issues during or after sleep ----------//
    if (section == 'issues') {
        var issuesValid = false;

        if (title == 'HIP pain' || title == 'Shoulder pain' || title == 'Back pain' || title == 'NONE') {

            $("#hot-cold").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var issuesRadio = $("input[name=issues]:checked").val();
        if (issuesRadio != '' && issuesRadio != undefined) {
            issuesValid = true;
            proceed = true;
        }
        if (issuesValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select issues during or after sleep'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('issues', title);
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ Hot Cold ----------//
    if (section == 'hot-cold') {
        var climateValid = false;

        if (title == 'Cool' || title == 'Hot' || title == 'No Issue') {

            $("#comfortable-sleep").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');;

        }
        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var climateRadio = $("input[name=hotcold]:checked").val();
        if (climateRadio != '' && climateRadio != undefined) {
            climateValid = true;
            proceed = true;
        }
        if (climateValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select sleep too hot or cold'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('hot-cold', title);
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ comfortable sleep ----------//
    if (section == 'comfortable-sleep') {
        var comfortableValid = false;
        var comfortableRadio = $("input[name=comfortablesleep]:checked").val();
        if (comfortableRadio != '' && comfortableRadio != undefined) {
            comfortableValid = true;
            proceed = true;
        }
        if (comfortableValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Most Comfortable Sleep'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('comfortable-sleep', title);
            $("#myCarousel").carousel('next');
        }
    }




});



$(".left").click(function() {
    $("#myCarousel").carousel('prev');
    //$('.carousel-inner input[type="radio"]:checked').val();
    //$('input[type="radio"]').prop('checked', false);
});