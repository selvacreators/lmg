var popup = $.cookie("popMeUp");
console.debug($.cookie("popMeUp"));
if (popup == '1') {
    //$("#redirectModel").modal("show");
}
var goto = 1;
var cookies = $.cookie();
for (var cookie in cookies) {
    $.removeCookie(cookie);
}
function gender() {
  if($("input[name=gender]").length){
    $("input[name=gender]").on('change', function() {
      var selValue = $("input[name=gender]:checked").val();
      var ques = $('body').find('.heading-title.q1').text(); 
      
      if (selValue) {
          if ($("#app-gender").length > 0) {

              var datas_gen1 = "<span  class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue + ".</span>";
              $("#app-gender").html(datas_gen1);
          } else {


              var datas_gen2 = "<span id='app-gender'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue + ".</span></span>";
              $("#Mattress_AppendText").append(datas_gen2);
          }
      }
    });
  }
}
 
function people(){
  if ($("input[name=2people]").length) {
    $("input[name=2people]").on('change', function() {
      var selValue1 = $("input[name=2people]:checked").val();
      var ques = $('body').find('#people .q2').text();
      if (selValue1) {

          if ($('#Mattress').length) {
              var datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue1 + ".</span>";
              $("#Mattress").html(datas1);
          } else {
              var datas1 = "<span id='Mattress'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue1 + ".</span></span>";
              $("#Mattress_AppendText").append(datas1);
          }

      }

    });
  }
}


//New One

function lookMat() {
  if($("input[name=lookingmattress]").length){
    $("input[name=lookingmattress]").on('change', function() {
      var lookingmattress = $("input[name=lookingmattress]:checked").val();
      var ques = $('body').find('#looking-mattress .q3').text();
      if (lookingmattress) {

          if ($('#look_Mattress').length) {
              var lookingmattress_datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + lookingmattress + ".</span>";
              $("#look_Mattress").html(lookingmattress_datas1);
          } else {
              var lookingmattress_datas1 = "<span id='look_Mattress'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + lookingmattress + ".</span></span>";
              $("#Mattress_AppendText").append(lookingmattress_datas1);
          }

      }

    });
  }
}

function weightRange(){
  if($("input[name=weightrange]").length){
    $("input[name=weightrange]").on('change', function() {
      var weightrange = $("input[name=weightrange]:checked").val();
      var ques = $('body').find('#weight-range .q4').text();
      var title = $('body').find('#weight-range.double .t1').text();

      if (weightrange) {

          if ($('#weightYou').length) {
              var weightrange_datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ": " + weightrange + ".</span>";
              $("#weightYou").html(weightrange_datas1);

          } else {
              var weightrange_datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ": " + weightrange + ".</span></span>";
              $("#weight_range #weightYou").append(weightrange_datas1);

          }

      }

    });
  }
}

function partnerweightRange(){
  if($("input[name=partner_weightrange]").length){
    $("input[name=partner_weightrange]").on('change', function() {
      var weightrange = $("input[name=partner_weightrange]:checked").val();
      //var ques = $('body').find('#weight-range .q4').text();
      var title = $('body').find('#weight-range.double .t2').text();

      if (weightrange) {

          if ($('#PartnerWeight').length) {
              var weightrange_datas1 = "<span class='ans'>" + title + ": " + weightrange + ".</span>";
              $("#PartnerWeight").html(weightrange_datas1);

          } else {
              var weightrange_datas1 = "<span class='ans'>" + title + ": " + weightrange + ".</span>";
              $("#weight_range #PartnerWeight").append(weightrange_datas1);

          }

      }

    });
  }
}

function ageRange(){
  if($("input[name=agerange]").length){
    $("input[name=agerange]").on('change', function() {
      var agerange = $("input[name=agerange]:checked").val();
      var ques = $('body').find('#age-range .q5').text();
      var title = $('body').find('#age-range.double .t1').text();

      if (agerange) {

          if ($('#ageYou').length) {
              var agerange_datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ": " + agerange + ".</span>";
              $("#ageYou").html(agerange_datas1);

          } else {
              var agerange_datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ": " + agerange + ".</span></span>";
              $("#agerange #ageYou").append(agerange_datas1);

          }

      }

    });
  }
  
}

function partnerageRange (){

  if($("input[name=partner_agerange]").length){
    $("input[name=partner_agerange]").on('change', function() {
      var agerange = $("input[name=partner_agerange]:checked").val();
      //var ques = $('body').find('#weight-range .q4').text();
      var title = $('body').find('#age-range.double .t2').text();

      if (agerange) {

          if ($('#PartnerAge').length) {
              var agerange_datas1 = "<span class='ans'>" + title + ": " + agerange + ".</span>";
              $("#PartnerAge").html(agerange_datas1);

          } else {
              var agerange_datas1 = "<span class='ans'>" + title + ": " + agerange + ".</span>";
              $("#agerange #PartnerAge").append(agerange_datas1);

          }

      }

    });
  }
  
}

function sleepPosition(){
  if($("input[name=yourposition]").length){
    $("input[name=yourposition]").on('change', function() {
      var yourposition = $("input[name=yourposition]:checked").val();
      var ques = $('body').find('#sleep-position .q6').text();
      var title = $('body').find('#sleep-position .t1').text();

      if (yourposition) {

          if ($('#positionYou').length) {
              var your_position_datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ": " + yourposition + ".</span>";
              $("#positionYou").html(your_position_datas1);

          } else {
              var your_position_datas1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + title + ": " + yourposition + ".</span></span>";
              $("#your_position #positionYou").append(your_position_datas1);

          }

      }

    });
  }
  
}

function partnersleepPosition(){
  if($("input[name=partnerposition]").length){
    $("input[name=partnerposition]").on('change', function() {
      var partnerposition = $("input[name=partnerposition]:checked").val();
      //var ques = $('body').find('#weight-range .q4').text();
      var title = $('body').find('#sleep-position.double .t2').text();

      if (partnerposition) {

          if ($('#PartnerPosition').length) {
              var partner_position_datas = "<span class='ans'>" + title + ": " + partnerposition + ".</span>";
              $("#PartnerPosition").html(partner_position_datas);

          } else {
              var partner_position_datas = "<span class='ans'>" + title + ": " + partnerposition + ".</span>";
              $("#your_position #PartnerPosition").append(partner_position_datas);

          }

      }

    });
  }
  
}


function issues(){
  if ($("input[name=issues]").length) {
    $("input[name=issues]").on('change', function() {
      var issues = $("input[name=issues]:checked").val();
      var ques = $('body').find('#issues .q7').text();
      if (issues) {

          if ($('#issues_data').length) {
              var issues_data = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + issues + ".</span>";
              $("#issues_data").html(issues_data);
          } else {
              var issues_data1 = "<span id='issues_data'><span class='ques'>" + ques + "=</span>  <span class='ans'>" + issues + ".</span></span>";
              $("#Mattress_AppendText").append(issues_data1);
          }

      }

    });
  }
}


function hotCold(){
  if ($("input[name=hotcold]").length) {
    $("input[name=hotcold]").on('change', function() {
      var hotcold = $("input[name=hotcold]:checked").val();
      var ques = $('body').find('#hot-cold .q8').text();
      if (hotcold) {

          if ($('#hotcold_data').length) {
              var hotcold_data = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + hotcold + ".</span>";
              $("#hotcold_data").html(hotcold_data);
          } else {
              var hotcold_data1 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + hotcold + ".</span>";
              $("#Mattress_AppendText").append(hotcold_data1);
          }

      }

    });
  }
}

function comfortSleep(){
  if ($("input[name=comfortablesleep]").length) {
    $("input[name=comfortablesleep]").on('change', function() {
      var selValue3 = $("input[name=comfortablesleep]:checked").val();
      var ques = $('body').find('#comfortable-sleep .q9').text();

      if (selValue3) {
          if ($('#finalsleep_data').length) {
              // console.log('check leffvel1');
              $('#finalsleep_data').text('');
              var datas2 = "<span class='ques'>" + ques + "=</span>  <span class='ans'>" + selValue3 + ".</span>";
              $("#finalsleep_data").append(datas2);
          } else {
              //console.log('check leffvel2');
              var datas3 = "<span id='finalsleep_data'><span class='ques'>" + ques + "=</span> <span class='ans'>" + selValue3 + ". </span></span>";
              $("#Mattress_AppendText").append(datas3);
          }

      }

    });
  }
}
$('.mattress-que-ans').hide();

$(".next-slide, .right").click(function() {

    //alert( $(".active ").attr('class') )
    $('.mattress-que-ans').show();
    var section = $(this).attr('data-section');
    var title = $(this).attr('data-title');
    //console.log('section - ' + section);
    //console.log('title - ' + title);
    var proceed = false;

    // ------------ GENDER VALIDATION ----------//
    if (section == 'gender') {

        var genderValid = false;
       
        if (title == 'spare bedroom') { 
                 
            $('#Mattress').show();
            $('#look_Mattress').hide();
            $('#weight_range').hide();
            $('#agerange').hide();
            $('#your_position').hide();
            $('#issues_data').hide();
            $('#hotcold_data').hide();
            $("#people").addClass('item').removeClass('hidden');
            $("#looking-mattress, #weight-range, #age-range, #sleep-position, #issues, #hot-cold").addClass('hidden').removeClass('item');

        }

        // if(title == 'Myself' || title == 'My partner & I' || title == 'My partner, I & My kids'){

        //      $("#looking-mattress").addClass('item').removeClass('hidden');
        //      $("#people").addClass('hidden').removeClass('item');                     
        // }

        if (title == 'Myself') {          
            
            $('#look_Mattress').show();
            $('#weight_range').show();
            $('#agerange').show();
            $('#your_position').show();
            $('#issues_data').show();
            $('#hotcold_data').show();
            $('#Mattress').hide();
            $("#looking-mattress").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $(".content-heading").hide();
            $("#weight-range").removeClass('double');
            $("#weight-range,.partner-weight").addClass('hidden').removeClass('item');
            $("#age-range,.partner-age").addClass('hidden').removeClass('item');
            $("#sleep-position,.partner-position").addClass('hidden').removeClass('item');
        }

        if (title == 'My partner & I' || title == 'My partner, I & My kids') {
            
           $('#look_Mattress').show();
            $('#weight_range').show();
            $('#agerange').show();
            $('#your_position').show();
            $('#issues_data').show();
            $('#hotcold_data').show();

            $('#Mattress').hide();
            $("#looking-mattress").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $(".content-heading").show();
            $("#weight-range").addClass('double');
            $("#weight-range,.partner-weight").addClass('item double').removeClass('hidden');
            $("#age-range,.partner-age").addClass('item double').removeClass('hidden');
            $("#sleep-position,.partner-position").addClass('item double').removeClass('hidden');
            //$("#weight-range,.partner-weight").addClass('hidden').removeClass('item');   
        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var genderRadio = $("input[name=gender]:checked").val();
        if (genderRadio != '' && genderRadio != undefined) {
            genderValid = true;
            proceed = true;
        }
        if (genderValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Why are you buying a mattress for?'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('compare_gender', title);
            gender();

            $("#myCarousel").carousel('next');
        }

    }

    // ------------ Looking mattress ----------//
    if (section == 'looking-mattress') {
        var lookingValid = false;

        if (title == 'old and worn out' || title == 'getting married or moving' || title == 'Improve my quality of sleep' || title == 'Great night’s sleep at a hotel') {
            
            $("#weight-range").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

            //     //$("#looking-mattress, #weight-range, #age-range, #sleep-position, #issues, #hot-cold").addClass('hidden').removeClass('item');

        }

        /*if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }*/

        var lookingRadio = $("input[name=lookingmattress]:checked").val();
        if (lookingRadio != '' && lookingRadio != undefined) {
            lookingValid = true;
            proceed = true;
        }
        if (lookingValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Why are you looking for a new mattress?'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('looking-mattress', title);
            lookMat();
            $("#myCarousel").carousel('next');
        }
    }

    // ------------ Weight Range ----------//
    if (section == 'weight-range') {
        var weightValid = false;

        if (title == '50-60 kg' || title == 'Less than 50 kg' || title == '60-70 kg' || title == 'above 70 kg') {
           
             //partnerweightRange();
            // weightRange();
            $("#age-range").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');
        }

        /*if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }*/

        var weightRadio = $("input[name=weightrange]:checked").val();
        var weightRadio1 = $("input[name=partner_weightrange]:checked").val();


        var compare_gender_name = $.cookie('compare_gender');
        if (compare_gender_name == "Myself") {
            //partnerweightRange();
            // weightRange();
            if (weightRadio != '' && weightRadio != undefined) {
                weightValid = true;
                proceed = true;
            }
        }

        if (compare_gender_name != "Myself") {
            partnerweightRange();
            // weightRange();
            if (weightRadio != '' && weightRadio != undefined && weightRadio1 != '' && weightRadio1 != undefined) {

                weightValid = true;
                proceed = true;

            }
        }

        if (compare_gender_name == 'Myself') {
            
            // weightRange(); 
            //$("#looking-mattress").addClass('item').removeClass('hidden');
            //$("#people").addClass('hidden').removeClass('item');   
            $("#weight-range,.your-weight").addClass('item').removeClass('hidden');
            // $("#age-range,.partner-age").addClass('hidden').removeClass('item');   
            // $("#sleep-position,.partner-position").addClass('hidden').removeClass('item');   
        }

        if (compare_gender_name == 'My partner & I' || title == 'My partner, I & My kids') {
            
            $("#looking-mattress").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#weight-range,.partner-weight").addClass('item').removeClass('hidden');
        }

        

        if (weightValid == false) {
            weightRange();
            partnerweightRange();
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select your weight range'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('compare_gender_name', title);
            weightRange();
            partnerweightRange();
            $("#myCarousel").carousel('next');
        }
        //partnerweightRange();
        //weightRange();
    }


    // ------------ Peopel ----------//
    if (section == 'people') {
        var genderValid = false;

        if (title != '') {
            $("#people").each(function() {
                $(this).removeClass('selected');
            });
            $(this).addClass('selected');
        }


        if (title == 'Less than 50 kg' || title == '50-60 kg' || title == '60-70 kg') {
            
            $("#comfortable-sleep").addClass('item').removeClass('hidden');
            $("#looking-mattress, #weight-range, #age-range, #sleep-position, #issues, #hot-cold ").addClass('hidden').removeClass('item');
        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }


        var weightValid = false;
        var weightRadio = $("input[name=2people]:checked").val();
        if (weightRadio != '' && weightRadio != undefined) {
            weightValid = true;
            proceed = true;
        }
        if (weightValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select your people'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {

            people();
            $.cookie('people', title);
            $("#myCarousel").carousel('next');
        }



    }


    // ------------ Age Range ----------//
    if (section == 'age-range') {
        var ageValid = false;

        if (title == 'Less than 30 yrs' || title == '30-35 yrs' || title == '36-45 yrs' || title == '46-55 yrs' || title == 'Above 55 yrs') {
            
            $("#sleep-position").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

        }


        /*if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }*/

        var ageRadio = $("input[name=agerange]:checked").val();
        var ageRadio1 = $("input[name=partner_agerange]:checked").val();


        var compare_gender_name = $.cookie('compare_gender');
        if (compare_gender_name == "Myself") {
            partnerageRange();
            if (ageRadio != '' && ageRadio != undefined) {
                ageValid = true;
                proceed = true;
            }
        }

        if (compare_gender_name != "Myself") {
            if (ageRadio != '' && ageRadio != undefined && ageRadio1 != '' && ageRadio1 != undefined) {
                ageValid = true;
                proceed = true;
            }
        }




        if (ageValid == false) {
            ageRange();
            partnerageRange();
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select your age range'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('age-range', title);
            ageRange();
            partnerageRange();
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ sleep position ----------//
    if (section == 'sleep-position') {
        var sleepValid = false;

        if (title == 'Back' || title == 'Stomach' || title == 'Side' || title == 'Toss & turn') {
            
            $("#issues").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var sleepRadio = $("input[name=yourposition]:checked").val();
        var sleepRadio1 = $("input[name=partnerposition]:checked").val();


        var compare_gender_name = $.cookie('compare_gender');
        if (compare_gender_name == "Myself") {

            if (sleepRadio != '' && sleepRadio != undefined) {
                sleepValid = true;
                proceed = true;
            }
        }

        if (compare_gender_name != "Myself") {

            if (sleepRadio != '' && sleepRadio != undefined && sleepRadio1 != '' && sleepRadio1 != undefined) {
                sleepValid = true;
                proceed = true;
            }
        }




        if (sleepValid == false) {
          sleepPosition();
          partnersleepPosition();
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Sleep Position'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('sleep-position', title);
            sleepPosition();
            partnersleepPosition();
            $("#myCarousel").carousel('next');
        }


    }



    // ------------ issues during or after sleep ----------//
    if (section == 'issues') {
        var issuesValid = false;

        if (title == 'HIP pain' || title == 'Shoulder pain' || title == 'Back pain' || title == 'NONE') {
            
            $("#hot-cold").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');
            $("#comfortable-sleep").addClass('hidden').removeClass('item');

        }

        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var issuesRadio = $("input[name=issues]:checked").val();
        if (issuesRadio != '' && issuesRadio != undefined) {
            issuesValid = true;
            proceed = true;
        }
        if (issuesValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select issues during or after sleep'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('issues', title);
            issues();
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ Hot Cold ----------//
    if (section == 'hot-cold') {
        var climateValid = false;

        if (title == 'Cool' || title == 'Hot' || title == 'No Issue') {
           
            $("#comfortable-sleep").addClass('item').removeClass('hidden');
            $("#people").addClass('hidden').removeClass('item');;

        }
        if (title != '' && title != undefined) {
            genderValid = true;
            proceed = true;
        }

        var climateRadio = $("input[name=hotcold]:checked").val();
        if (climateRadio != '' && climateRadio != undefined) {
            climateValid = true;
            proceed = true;
        }
        if (climateValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select sleep too hot or cold'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            $.cookie('hot-cold', title);
            hotCold();
            $("#myCarousel").carousel('next');
        }
    }


    // ------------ comfortable sleep ----------//
    if (section == 'comfortable-sleep') {
        var comfortableValid = false;
        var comfortableRadio = $("input[name=comfortablesleep]:checked").val();
        if (comfortableRadio != '' && comfortableRadio != undefined) {
            comfortableValid = true;
            proceed = true;
        }
        if (comfortableValid == false) {
            $.notify({
                // options
                icon: 'ion-android-alert',
                message: 'Select Most Comfortable Sleep'
            }, {
                // settings
                placement: {
                    from: "bottom",
                    align: "right"
                },
                type: 'warning'
            });
            console.log('select radio button');
            proceed = false;
        }
        if (proceed == true) {
            comfortSleep();
            $.cookie('comfortable-sleep', title);
            $("#myCarousel").carousel('next');
        }
    }

});

$(".left").click(function() {
    $("#myCarousel").carousel('prev');
    //$('.carousel-inner input[type="radio"]:checked').val();
    $('input[type="radio"]').prop('checked', false);
});