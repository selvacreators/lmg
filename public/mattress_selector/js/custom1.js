var popup = $.cookie("popMeUp");
console.debug($.cookie("popMeUp"));
if(popup == '1')
{
    //$("#redirectModel").modal("show");
}
var goto = 1;
var cookies = $.cookie();
for(var cookie in cookies) {
   $.removeCookie(cookie);
}

$(".next-slide, .right").click(function(){

                //alert( $(".active ").attr('class') )

                var section = $(this).attr('data-section');
                var title = $(this).attr('data-title');
                 console.log('section - '+section);
                 console.log('title - '+title);
                var proceed = false;

                // ------------ GENDER VALIDATION ----------//
                if(section=='gender')
                {
                    var genderValid = false;
                    var genderRadio = $("input[name=gender]:checked").val();
                    if (genderRadio != '' && genderRadio != undefined){ genderValid = true; proceed=true; }
                    if(genderValid == false) { 
                         $.notify({
                            // options
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'Select Gender' 
                        },{
                            // settings
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            type: 'warning'
                        });
                        console.log('select radio button'); proceed = false;
                     }
                  if(proceed == true)
                  {
                    $.cookie('compare_gender', title);  
                    $("#myCarousel").carousel('next');
                  }
                }

                // ------------ GENDER VALIDATION ----------//
                if(section=='selectLoan')
                {
                    var genderValid = false;

                    // ---------- HIDE LOAN AMOUNT SLIDE -------------//
                    if(title !='')
                    {
                        $(".select-loan").each(function() {
                            $(this).removeClass('selected');
                        });
                        $(this).addClass('selected');
                    }
                    if(title == 'Personal Loan' || title == 'Car Loan' || title =='Home Loan' )
                    {
                        $("#loan-amount-div, #employee-type-div").addClass('item').removeClass('hidden');
                        $("#length-of-business-div, #annual-turn-over-div, #insurence-type-div, #account-maintaning-in-div").addClass('hidden').removeClass('item');
                    }

                    if(title == 'Business Loan')
                    {
                        $("#loan-amount-div, #length-of-business-div, #annual-turn-over-div").addClass('item').removeClass('hidden');
                        $("#employee-type-div, #salary-deposit-div, #salary-amount-div, #account-maintaning-in-div, #date-of-joining-div, #insurence-type-div").addClass('hidden').removeClass('item');
                    }

                    if(title == 'Insurance')
                    {
                        $("#insurence-type-div").addClass('item').removeClass('hidden');
                        $("#employee-type-div, #salary-deposit-div, #salary-amount-div, #account-maintaning-in-div, #date-of-joining-div, #loan-amount-div, #length-of-business-div, #annual-turn-over-div").addClass('hidden').removeClass('item');
                    }

                    if(title == 'Credit Card')
                    {
                        $("#employee-type-div").addClass('item').removeClass('hidden');
                        $("#loan-amount-div").addClass('hidden').removeClass('item');

                    }
                    if (title == 'Business Cards')
                    {
                        $("#length-of-business-div, #annual-turn-over-div").addClass('item').removeClass('hidden');
                        $("#employee-type-div, #loan-amount-div, #salary-amount-div, #salary-deposit-div, #account-maintaning-in-div, #date-of-joining-div, #insurence-type-div").addClass('hidden').removeClass('item');
                    }

                    if(title == 'Personal Account')
                    {
                        $("#salary-amount-div,#account-maintaning-in-div").addClass('item').removeClass('hidden');
                        $("#employee-type-div,  #loan-amount-div, #length-of-business-div, #salary-deposit-div, #annual-turn-over-div, #date-of-joining-div, #insurence-type-div").addClass('hidden').removeClass('item');
                    }

                    if(title == 'Business Account')
                    {
                        $("#account-maintaning-in-div").addClass('item').removeClass('hidden');
                        $("#salary-amount-div, #employee-type-div, #loan-amount-div, #length-of-business-div, #salary-deposit-div, #annual-turn-over-div, #date-of-joining-div, #insurence-type-div").addClass('hidden').removeClass('item');
                    }

                    if (title != '' && title != undefined){ genderValid = true; proceed=true; }
                    //if(genderValid == false) { console.log('select radio button'); proceed = false; }


                     if(title == '' && $.cookie('compare_selected_loan') == undefined)
                    {
                        $.notify({
                            // options
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'Please Select Product Type' 
                        },{
                            // settings
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            type: 'warning'
                        });
                        proceed = false;
                    }

                    if($.cookie('compare_selected_loan') != '')
                    {
                        if($( ".select-loan" ).hasClass( "selected" ))
                          {
                            $("#myCarousel").carousel('next');
                          }
                    }

                  if(proceed == true)
                  {
                    $.cookie('compare_selected_loan', title); 
                    $("#myCarousel").carousel('next');
                  }
                }

                 // ------------ Place Selection ----------//
                if(section=='cityName')
                {

                    var id = $(this).attr('id');
                    var cityValid = false;

                    if (title != '' && title != undefined){

                        $(".select-city").each(function() {
                            $(this).removeClass('selected');
                        });

                        $(this).addClass('selected');
                        cityValid = true; proceed=true; $.cookie('compare_city_name', title); 
                    }
                    if(cityValid == false && $.cookie("compare_city_name") == undefined) { 
                        //console.log('select radio button'); 
                        $.notify({
                            // options
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'Please select the city' 
                        },{
                            // settings
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            type: 'warning'
                        });
                        proceed = false;
                     }
                     if($.cookie("compare_city_name") != ''){ $.cookie('compare_city_name', $.cookie("compare_city_name")); proceed = true;}
                  if(proceed == true)
                  { 
                //console.debug($.cookie("compare_city_name"));
                    if($( ".select-city" ).hasClass( "selected" ))
                      {
                        $("#myCarousel").carousel('next');
                      }

                  }
                }

                // --------------- LOAN AMOUNT --------------//
                if(section=='loanAmount')
                {
                    if(title == '')
                    {
                        $.cookie('compare_loan_amount', title); 
                        $("#myCarousel").carousel('next');
                    }
                    else
                    {
                        var amount = $("#amount").val();
                        if(amount <= 0)
                        {
                            $.notify({icon: 'glyphicon glyphicon-warning-sign', message: 'You forget to give us your loan amount' },
                                { placement: {from: "bottom",align: "right"}, type: 'warning'});
                        }
                        else
                        {
                            $("#myCarousel").carousel('next');
                            $.cookie('compare_loan_amount', amount); 
                        }
                    }

                    console.debug($.cookie("compare_loan_amount"));
                }
                
                if(section=='bankWantToApply')
                {
                    if(title == '')
                    {
                        $.cookie('salary_deposit_to', title); 
                    }
                    else
                    {
                        $.cookie('salary_deposit_to', title); 
                    }
                    $("#myCarousel").carousel('next');
                }
                
                // --------------- EMPLOYEE TYPE --------------//
                if(section=='employee-type')
                {

                    var empValid = false;
                    var empRadio = $("input[name=empType]:checked").val();

                    
                        proceed = true;
                        if (empRadio != '' && empRadio != undefined)
                        {
                            if(empRadio == 'Salaried')
                            {
                                $("#salary-amount-div, #date-of-joining-div, #salary-deposit-div").addClass('item').removeClass('hidden');
                                $("#length-of-business-div, #annual-turn-over-div, #account-maintaning-in-div, #insurence-type-div").removeClass('item').addClass('hidden');

                                var company_name = $("#companyName").val();
                                company_name = $.trim(company_name);
                                if(company_name !='' && company_name != undefined)
                                {
                                    $.cookie('company_name', company_name);
                                    proceed=true; empValid = true;
                                }
                                else
                                {
                                    $.notify({ icon: 'glyphicon glyphicon-warning-sign', message: 'Please Enter Your Company Name'},
                                        {placement: {from: "bottom",align: "right"},type: 'warning'});
                                    $("#companyName").focus();
                                    proceed=false; 
                                }
                                //empValid = true; proceed=true; 
                            }
                            else
                            {
                                $("#salary-amount-div, #date-of-joining-div, #salary-deposit-div, #account-maintaning-in-div, #insurence-type-div").removeClass('item').addClass('hidden');
                                $("#length-of-business-div, #annual-turn-over-div").addClass('item').removeClass('hidden');

                                empValid = true; proceed=true; 
                            }
                        }
                        else{
                            $.notify({
                            // options
                            icon: 'glyphicon glyphicon-warning-sign',
                            message: 'You forgot to select your employee type' 
                        },{
                            // settings
                            placement: {
                                from: "bottom",
                                align: "right"
                            },
                            type: 'warning'
                        });
                        }

                        if(empValid == false) 
                        {
                            console.log('select radio button'); proceed = false;
                        }

                    if(proceed == true)
                    {
                        var company_name = $("#companyName").val();
                        company_name = $.trim(company_name);

                        $.cookie('company_name', company_name);
                        $.cookie('employee_type', empRadio);
                        $("#myCarousel").carousel('next');
                    }
                }

                // --------------- SALARY AMOUNT --------------//
                if(section=='salaryAmount')
                {
                    var amount = $("#amount-1").val();
                    //alert(amount);
                    $.cookie('salary_amount', amount); 
                    $("#myCarousel").carousel('next');
                }

                if(section=='accountMaintaningBank')
                {
                    if(title == '')
                    {
                        $.cookie('account_maintaning_bank', title); 
                    }
                    else
                    {
                        $.cookie('account_maintaning_bank', title); 
                    }
                    $("#myCarousel").carousel('next');
                }

                // --------------- SALARY AMOUNT --------------//
                if(section=='dateOfJoining')
                {
                    var doj = $("#doj").val();
                    if(doj !='' && doj != undefined)
                    {
                        $.cookie('date_of_join', doj); 
                        $("#myCarousel").carousel('next');
                    }
                    else
                    {
                        $("#doj").focus();
                    }
                }

                // ----------- LENGTH OF BUSINESS ----------//
                if(section=='lengthOfBusiness')
                {
                    var amount = $("#amount-3").val();
                    if(amount!='' && $.isNumeric(amount) && amount >= 1)
                    {
                        //alert(amount);
                        $.cookie('length_of_business', amount); 
                        $("#myCarousel").carousel('next');
                    }
                    else
                    {
                        $.notify({icon: 'glyphicon glyphicon-warning-sign', message: 'Please Select Length of Your Business' },
                        { placement: {from: "bottom",align: "right"}, type: 'warning'});
                        $("#amount-3").focus();
                    }
                }

                //-----------//
                if(section=='annualTurnOver')
                {
                    var amount = $("#amount-2").val();
                    if(amount!='' && $.isNumeric(amount))
                    {
                        //alert(amount);
                        $.cookie('annual_turnover', amount); 
                        $("#myCarousel").carousel('next');
                    }
                    else
                    {
                        $("#amount-2").focus();
                    }
                }


                if(section=='insuranceType')
                {
                        $.cookie('insurance_type', title);
                        $("#myCarousel").carousel('next');
                }

                //----------- SUBMIT THE FORM -------------//
                if(section=='sumitTheForm')
                {
                    var name = $("#fullName").val();
                    name = $.trim(name);
                    var mobile = $("#mobileNumber").val();
                    mobile = $.trim(mobile);
                    var email = $("#emailID").val();
                    email = $.trim(email);
                    if(name=='')
                    {
                        $("#fullName").focus();
                    }
                    else if(mobile =='')
                    {
                        $("#mobileNumber").focus();
                    }
                    else if(email =='' || validateEmail(email) == false)
                    {
                        $("#emailID").focus();
                    }
                    else
                    {
                        $.cookie('full_name', name); 
                        $.cookie('mobile_number', mobile); 
                        $.cookie('email_id', email);
                        if(goto == 1) {
                        window.location.href='landing_thankyou.php';
                        }else{ } goto++;
                    }
                }
            });

            function validateEmail(sEmail) {
                var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            
            $("#select-other-bank").change(function(){
                $.cookie('salary_deposit_to', this.value); 
                $("#myCarousel").carousel('next');
            });
            
            $("#account-maintaning-dropdown").change(function(){
                $.cookie('account_maintaning_bank', this.value); 
                $("#myCarousel").carousel('next');
            });

            $(".left").click(function(){
                $("#myCarousel").carousel('prev');
            });

            // $(".right").click(function(){
            //     $("#myCarousel").carousel('next');
            // });

            /*$("#submit_details").click(function(){
                //console.debug($.cookie("compare_gender"));
            });*/