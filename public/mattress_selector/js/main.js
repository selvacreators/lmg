$( document ).ready(function() { 
	//NAV MENU HOVER //
  

    //Date picker
    /*$('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd M yy",
        onSelect: function (selected, inst) {
            $(this).next('i.md.md-calendar.md-2x').removeClass('md-calendar').addClass('md-calendar-remove').attr('data-original-title', 'Click to clear selected date');
        }
    }).attr('placeholder', 'Click for datepicker').wrap('<span class="calHolder"></span>').after('<i class="md md-calendar md-2x" rel="tooltip" title="Click for datepicker" data-placement="left"></i>');

    $(".calHolder").click(function () {
        if ($(this).parent().find('input').val()) {
            $(this).parent().find('input').val('');
            $(this).children('i.md.md-calendar-remove.md-2x').removeClass('md-calendar-remove').addClass('md-calendar').attr('data-original-title', 'Click for datepicker');
        } else {
            $(this).parent().find('input').focus();
        }
    });*/

    //Carousel
    /*$("#myCarousel").carousel({
        interval : false,//sliding time
        pause: true
    });*/
    
		
		    $("#slider").slider({
              range: "min",
              animate: true,
              values:50000,//50000
              min: 50000,//50000
              max: 10000000,
              step: 1000000,//950000
              stop: function( event, ui ) {
                  $( ".ui-slider-label" )
                      update(1,ui.value);
               },
              slide: function(event, ui) {
                //update(1,ui.value); //changed
                $( "#amount" ).val(ui.value);
              }
          }).slider("pips", {
             step: 2,
             rest: "label",
             labels: { first: "50000", last: "10M above" }
        }).slider("float");
		
		
		
        /*$("#slider-1").slider({
              range: "min",
              animate: true,
              values:3000,//5000
              min: 3000,
              max: 100000,
              step: 1000,
              stop: function( event, ui ) {
                  $( ".ui-slider-label" )
                      update(1,ui.value);
               },
              slide: function(event, ui) {
                //update(1,ui.value); //changed
                $( "#amount-1" ).val(ui.value);
              }
          }).slider("pips", {
             step: 25,
             rest: "label",
             labels: { first: "3000", last: "100000" }
        }).slider("float");*/

        
        $("#slider-2").slider({
              range: "min",
              animate: true,
              values:1,
              min: 0,
              max: 100,
              step: 1,
              stop: function( event, ui ) {
                  $( ".ui-slider-label" )
                      update(1,ui.value);
               },
              slide: function(event, ui) {
                //update(1,ui.value); //changed
                $( "#amount-2" ).val(ui.value);
              }
          }).slider("pips", {
             step: 10,
             rest: "label",
             labels: { first: "<1M", last: "100M" }
        }).slider("float");

        $("#slider-3").slider({
              range: "min",
              animate: true,
              values:1,
              min: 1,
              max: 25,
              step: 1,
              stop: function( event, ui ) {
                  $( ".ui-slider-label" )
                      update(1,ui.value);
               },
              slide: function(event, ui) {
                //update(1,ui.value); //changed
                $( "#amount-3" ).val(ui.value);
              }
          }).slider("pips", {
             step: 5,
             rest: "label",
             labels: { first: "1 year", last: "25 years" }
        }).slider("float");

          //Added, set initial value.
          $("#amount").val(0);
          $("#amount-1").val(3000);
          $("#amount-2").val(1);
          $("#amount-3").val(1);
          //$("#amount-label").text(5000);
          
          update();

           $("#amount").keyup(function() {
              $("#slider").slider("value" , $(this).val())
           });
           $("#amount-1").keyup(function() {
              $("#slider-1").slider("value" , $(this).val())
           });
           $("#amount-2").keyup(function() {
              $("#slider-2").slider("value" , $(this).val())
           });
           $("#amount-3").keyup(function() {
              $("#slider-3").slider("value" , $(this).val())
           });


        $("#slider .ui-slider-label").click(function(){
              $amount = $( "#slider" ).slider( "value" );
              $( "#amount" ).val($amount);
         });
         $("#slider-1 .ui-slider-label").click(function(){
              $amount_amt = $( "#slider-1" ).slider( "value" );
              $( "#amount-1" ).val($amount_amt);
         });
         $("#slider-2 .ui-slider-label").click(function(){
              $amount_amt1 = $( "#slider-2" ).slider( "value" );
              $( "#amount-2" ).val($amount_amt1);
         });
         $("#slider-3 .ui-slider-label").click(function(){
              $amount_amt2 = $( "#slider-3" ).slider( "value" );
              $( "#amount-3" ).val($amount_amt2);
         });
});

      //changed. now with parameter
      /*function update(slider,val) {
       
        $amount = $( "#slider" ).slider( "value" );
        $amount_amt = $( "#slider-1" ).slider( "value" );
        $amount_amt1 = $( "#slider-2" ).slider( "value" );
        $amount_amt2 = $( "#slider-3" ).slider( "value" );
       
         
      }*/
