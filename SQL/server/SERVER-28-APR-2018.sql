DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(10) NOT NULL,
  `lang_name` varchar(50) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `lang_code` (`lang_code`)
) ENGINE=MyISAM AUTO_INCREMENT=136 DEFAULT CHARSET=latin1 COMMENT='Website Languages';

DELETE FROM `language`;

INSERT INTO `language` (`lang_id`, `lang_code`, `lang_name`, `sort_order`, `status`) VALUES
	(1, 'en', 'English', 1, 1),
	(2, 'aa', 'Afar', 0, 2),
	(3, 'ab', 'Abkhazian', 0, 2),
	(4, 'af', 'Afrikaans', 0, 2),
	(5, 'am', 'Amharic', 0, 2),
	(6, 'ar', 'Arabic', 0, 2),
	(7, 'as', 'Assamese', 0, 2),
	(8, 'ay', 'Aymara', 0, 2),
	(9, 'az', 'Azerbaijani', 0, 2),
	(10, 'ba', 'Bashkir', 0, 2),
	(11, 'be', 'Belarusian', 0, 2),
	(12, 'bg', 'Bulgarian', 0, 2),
	(13, 'bh', 'Bihari', 0, 2),
	(14, 'bi', 'Bislama', 0, 2),
	(15, 'bn', 'Bengali/Bangla', 0, 2),
	(16, 'bo', 'Tibetan', 0, 2),
	(17, 'br', 'Breton', 0, 2),
	(18, 'ca', 'Catalan', 0, 2),
	(19, 'co', 'Corsican', 0, 2),
	(20, 'cs', 'Czech', 0, 2),
	(21, 'cy', 'Welsh', 0, 2),
	(22, 'da', 'Danish', 0, 2),
	(23, 'de', 'German', 0, 2),
	(24, 'dz', 'Bhutani', 0, 2),
	(25, 'el', 'Greek', 0, 2),
	(26, 'eo', 'Esperanto', 0, 2),
	(27, 'es', 'Spanish', 0, 2),
	(28, 'et', 'Estonian', 0, 2),
	(29, 'eu', 'Basque', 0, 2),
	(30, 'fa', 'Persian', 0, 2),
	(31, 'fi', 'Finnish', 0, 2),
	(32, 'fj', 'Fiji', 0, 2),
	(33, 'fo', 'Faeroese', 0, 2),
	(34, 'fr', 'French', 0, 2),
	(35, 'fy', 'Frisian', 0, 2),
	(36, 'ga', 'Irish', 0, 2),
	(37, 'gd', 'Scots/Gaelic', 0, 2),
	(38, 'gl', 'Galician', 0, 2),
	(39, 'gn', 'Guarani', 0, 2),
	(40, 'gu', 'Gujarati', 0, 2),
	(41, 'ha', 'Hausa', 0, 2),
	(42, 'hi', 'Hindi', 0, 2),
	(43, 'hr', 'Croatian', 0, 2),
	(44, 'hu', 'Hungarian', 0, 2),
	(45, 'hy', 'Armenian', 0, 2),
	(46, 'ia', 'Interlingua', 0, 2),
	(47, 'ie', 'Interlingue', 0, 2),
	(48, 'ik', 'Inupiak', 0, 2),
	(49, 'in', 'Indonesian', 0, 2),
	(50, 'is', 'Icelandic', 0, 2),
	(51, 'it', 'Italian', 0, 2),
	(52, 'iw', 'Hebrew', 0, 2),
	(53, 'ja', 'Japanese', 0, 2),
	(54, 'ji', 'Yiddish', 0, 2),
	(55, 'jw', 'Javanese', 0, 2),
	(56, 'ka', 'Georgian', 0, 2),
	(57, 'kk', 'Kazakh', 0, 2),
	(58, 'kl', 'Greenlandic', 0, 2),
	(59, 'km', 'Cambodian', 0, 2),
	(60, 'kn', 'Kannada', 0, 2),
	(61, 'ko', 'Korean', 0, 2),
	(62, 'ks', 'Kashmiri', 0, 2),
	(63, 'ku', 'Kurdish', 0, 2),
	(64, 'ky', 'Kirghiz', 0, 2),
	(65, 'la', 'Latin', 0, 2),
	(66, 'ln', 'Lingala', 0, 2),
	(67, 'lo', 'Laothian', 0, 2),
	(68, 'lt', 'Lithuanian', 0, 2),
	(69, 'lv', 'Latvian/Lettish', 0, 2),
	(70, 'mg', 'Malagasy', 0, 2),
	(71, 'mi', 'Maori', 0, 2),
	(72, 'mk', 'Macedonian', 0, 2),
	(73, 'ml', 'Malayalam', 0, 2),
	(74, 'mn', 'Mongolian', 0, 2),
	(75, 'mo', 'Moldavian', 0, 2),
	(76, 'mr', 'Marathi', 0, 2),
	(77, 'ms', 'Malay', 0, 2),
	(78, 'mt', 'Maltese', 0, 2),
	(79, 'my', 'Burmese', 0, 2),
	(80, 'na', 'Nauru', 0, 2),
	(81, 'ne', 'Nepali', 0, 2),
	(82, 'nl', 'Dutch', 0, 2),
	(83, 'no', 'Norwegian', 0, 2),
	(84, 'oc', 'Occitan', 0, 2),
	(85, 'om', '(Afan)/Oromoor/Oriya', 0, 2),
	(86, 'pa', 'Punjabi', 0, 2),
	(87, 'pl', 'Polish', 0, 2),
	(88, 'ps', 'Pashto/Pushto', 0, 2),
	(89, 'pt', 'Portuguese', 0, 2),
	(90, 'qu', 'Quechua', 0, 2),
	(91, 'rm', 'Rhaeto-Romance', 0, 2),
	(92, 'rn', 'Kirundi', 0, 2),
	(93, 'ro', 'Romanian', 0, 2),
	(94, 'ru', 'Russian', 0, 2),
	(95, 'rw', 'Kinyarwanda', 0, 2),
	(96, 'sa', 'Sanskrit', 0, 2),
	(97, 'sd', 'Sindhi', 0, 2),
	(98, 'sg', 'Sangro', 0, 2),
	(99, 'sh', 'Serbo-Croatian', 0, 2),
	(100, 'si', 'Singhalese', 0, 2),
	(101, 'sk', 'Slovak', 0, 2),
	(102, 'sl', 'Slovenian', 0, 2),
	(103, 'sm', 'Samoan', 0, 2),
	(104, 'sn', 'Shona', 0, 2),
	(105, 'so', 'Somali', 0, 2),
	(106, 'sq', 'Albanian', 0, 2),
	(107, 'sr', 'Serbian', 0, 2),
	(108, 'ss', 'Siswati', 0, 2),
	(109, 'st', 'Sesotho', 0, 2),
	(110, 'su', 'Sundanese', 0, 2),
	(111, 'sv', 'Swedish', 0, 2),
	(112, 'sw', 'Swahili', 0, 2),
	(113, 'ta', 'Tamil', 0, 2),
	(114, 'te', 'Telugu', 0, 2),
	(115, 'tg', 'Tajik', 0, 2),
	(116, 'th', 'Thai', 0, 2),
	(117, 'ti', 'Tigrinya', 0, 2),
	(118, 'tk', 'Turkmen', 0, 2),
	(119, 'tl', 'Tagalog', 0, 2),
	(120, 'tn', 'Setswana', 0, 2),
	(121, 'to', 'Tonga', 0, 2),
	(122, 'tr', 'Turkish', 0, 2),
	(123, 'ts', 'Tsonga', 0, 2),
	(124, 'tt', 'Tatar', 0, 2),
	(125, 'tw', 'Twi', 0, 2),
	(126, 'uk', 'Ukrainian', 0, 2),
	(127, 'ur', 'Urdu', 0, 2),
	(128, 'uz', 'Uzbek', 0, 2),
	(129, 'vi', 'Vietnamese', 2, 1),
	(130, 'vo', 'Volapuk', 0, 2),
	(131, 'wo', 'Wolof', 0, 2),
	(132, 'xh', 'Xhosa', 0, 2),
	(133, 'yo', 'Yoruba', 0, 2),
	(134, 'zh', 'Chinese', 0, 2),
	(135, 'zu', 'Zulu', 0, 2);

DROP TABLE IF EXISTS category;

ALTER TABLE `collection`
	ENGINE=InnoDB;

ALTER TABLE `collection`
	ALTER `main_image` DROP DEFAULT,
	ALTER `feature_image` DROP DEFAULT,
	ALTER `collection_banner` DROP DEFAULT,
	ALTER `collection_btn_text` DROP DEFAULT,
	ALTER `collection_btn_url` DROP DEFAULT,
	ALTER `meta_keyword` DROP DEFAULT;
ALTER TABLE `collection`
	COMMENT='Collection Master which handle like a category in ecommerce',
	ADD COLUMN `lang_code` VARCHAR(10) NOT NULL DEFAULT 'en' AFTER `collection_id`,
	ADD COLUMN `collection_slug` VARCHAR(255) NOT NULL AFTER `collection_name`,
	CHANGE COLUMN `main_image` `main_image` VARCHAR(255) NULL AFTER `brand_id`,
	CHANGE COLUMN `feature_image` `feature_image` VARCHAR(255) NULL AFTER `main_image`,
	CHANGE COLUMN `collection_banner` `collection_banner` VARCHAR(255) NULL AFTER `feature_image`,
	CHANGE COLUMN `collection_btn_text` `collection_btn_text` VARCHAR(65) NULL AFTER `position`,
	CHANGE COLUMN `collection_btn_url` `collection_btn_url` VARCHAR(225) NULL AFTER `collection_btn_text`,
	ADD COLUMN `parent_id` INT(10) NOT NULL DEFAULT '0' AFTER `collection_btn_url`,
	CHANGE COLUMN `meta_keyword` `meta_keyword` VARCHAR(255) NULL AFTER `meta_title`,
	CHANGE COLUMN `meta_desc` `meta_desc` TEXT NULL AFTER `meta_keyword`,
	ADD COLUMN `sort_order` INT(3) NOT NULL DEFAULT '0' AFTER `meta_desc`,
	ADD COLUMN `created_by` VARCHAR(30) NOT NULL COMMENT 'IP address of the person who add' AFTER `created_at`,
	ADD COLUMN `updated_by` VARCHAR(30) NULL DEFAULT NULL AFTER `updated_at`,
	CHANGE COLUMN `col_status` `col_status` INT(1) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive' AFTER `updated_by`;

update collection c set c.collection_slug = REPLACE( LCASE(c.collection_name) , ' ', '-' );

ALTER TABLE `collection`
	ADD UNIQUE INDEX `lang_code_collection_slug` (`lang_code`, `collection_slug`);

ALTER TABLE `collection`
	CHANGE COLUMN `updated_by` `updated_by` VARCHAR(30) NULL DEFAULT NULL COMMENT 'IP address of the person who update' AFTER `updated_at`;

ALTER TABLE `collection`
	ALTER `lang_code` DROP DEFAULT;
ALTER TABLE `collection`
	CHANGE COLUMN `lang_code` `lang_code` VARCHAR(10) NOT NULL AFTER `collection_id`;

ALTER TABLE `brands`
	ENGINE=InnoDB;

UPDATE `users` SET `created_at`='2018-04-27 07:32:32';

ALTER TABLE `users`
	CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `remember_token`,
	CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`;

DROP TABLE IF EXISTS `attribute`;
CREATE TABLE IF NOT EXISTS `attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`),
  KEY `FK_ATTRIBUTE_ATTRIBUTE_GROUP` (`attribute_group_id`),
  CONSTRAINT `FK_ATTRIBUTE_ATTRIBUTE_GROUP` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_group` (`attribute_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `attribute_description`;
CREATE TABLE IF NOT EXISTS `attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK__ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK__ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `attribute_group`;
CREATE TABLE IF NOT EXISTS `attribute_group` (
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `attribute_group_description`;
CREATE TABLE IF NOT EXISTS `attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK__ATTRIBUTE_GROUP` (`attribute_group_id`),
  CONSTRAINT `FK__ATTRIBUTE_GROUP` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_group` (`attribute_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `length_class`;
CREATE TABLE IF NOT EXISTS `length_class` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Default',
  PRIMARY KEY (`length_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='in this table it has only one default is must';

DELETE FROM `length_class`;

INSERT INTO `length_class` (`length_class_id`, `value`, `default`) VALUES
	(1, 1.00000000, 1),
	(2, 0.39370000, 2),
	(3, 10.00000000, 2);

DROP TABLE IF EXISTS `length_class_description`;
CREATE TABLE IF NOT EXISTS `length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `unit` varchar(10) NOT NULL,
  UNIQUE KEY `lang_code_title` (`lang_code`,`title`),
  KEY `FK_LENGTH_CLASS_DESCRIPTION_LENGTH_CLASS` (`length_class_id`),
  CONSTRAINT `FK_LENGTH_CLASS_DESCRIPTION_LENGTH_CLASS` FOREIGN KEY (`length_class_id`) REFERENCES `length_class` (`length_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DELETE FROM `length_class_description`;

INSERT INTO `length_class_description` (`length_class_id`, `lang_code`, `title`, `unit`) VALUES
	(1, 'en', 'Centimeter', 'cm'),
	(2, 'en', 'Inch', 'in'),
	(3, 'en', 'Millimeter', 'mm');

DROP TABLE IF EXISTS `option`;
CREATE TABLE IF NOT EXISTS `option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `option_description`;
CREATE TABLE IF NOT EXISTS `option_description` (
  `option_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK_OPTION_DESCRIPTION_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_OPTION_DESCRIPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `option_value`;
CREATE TABLE IF NOT EXISTS `option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `sort_order` int(6) NOT NULL,
  PRIMARY KEY (`option_value_id`),
  KEY `FK_OPTION_VALUE_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_OPTION_VALUE_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `option_value_description`;
CREATE TABLE IF NOT EXISTS `option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK_OPTION_VALUE_DESCRIPTION_OPTION_VALUE_ID` (`option_value_id`),
  KEY `FK_OPTION_VALUE_DESCRIPTION_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_OPTION_VALUE_DESCRIPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`),
  CONSTRAINT `FK_OPTION_VALUE_DESCRIPTION_OPTION_VALUE_ID` FOREIGN KEY (`option_value_id`) REFERENCES `option_value` (`option_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(225) DEFAULT NULL,
  `brand_id` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `date_available` date NOT NULL,
  `weight_class_id` int(11) DEFAULT NULL,
  `weight` decimal(15,8) DEFAULT NULL,
  `length_class_id` int(11) DEFAULT NULL,
  `length` decimal(15,8) DEFAULT NULL,
  `width` decimal(15,8) DEFAULT NULL,
  `height` decimal(15,8) DEFAULT NULL,
  `subtract` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Subtract Stock, 2-Dont Subtract Stock',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive',
  `viewed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `FK_PRODUCT_BRAND` (`brand_id`),
  KEY `FK_PRODUCT_STOCK_STATUS` (`stock_status_id`),
  KEY `FK_PRODUCT_WEIGHT_CLASS` (`weight_class_id`),
  KEY `FK_PRODUCT_LENGTH_CLASS` (`length_class_id`),
  CONSTRAINT `FK_PRODUCT_BRAND` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
  CONSTRAINT `FK_PRODUCT_LENGTH_CLASS` FOREIGN KEY (`length_class_id`) REFERENCES `length_class` (`length_class_id`),
  CONSTRAINT `FK_PRODUCT_STOCK_STATUS` FOREIGN KEY (`stock_status_id`) REFERENCES `stock_status` (`stock_status_id`),
  CONSTRAINT `FK_PRODUCT_WEIGHT_CLASS` FOREIGN KEY (`weight_class_id`) REFERENCES `weight_class` (`weight_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_attibute`;
CREATE TABLE IF NOT EXISTS `product_attibute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `text` mediumtext NOT NULL,
  KEY `FK_PRODUCT_ATTRIBUTE_PRODUCT_ID` (`product_id`),
  KEY `FK_PRODUCT_ATTIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_PRODUCT_ATTIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`attribute_id`),
  CONSTRAINT `FK_PRODUCT_ATTRIBUTE_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_description`;
CREATE TABLE IF NOT EXISTS `product_description` (
  `product_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `product_slug` varchar(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `desc` mediumtext NOT NULL,
  `meta_title` varchar(225) DEFAULT NULL,
  `meta_desc` varchar(225) DEFAULT NULL,
  `meta_keyword` varchar(225) DEFAULT NULL,
  `tag` mediumtext,
  UNIQUE KEY `lang_code_product_slug` (`lang_code`,`product_slug`),
  KEY `FK_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_discount`;
CREATE TABLE IF NOT EXISTS `product_discount` (
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '1',
  `priority` int(3) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '1.0000',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`product_discount_id`),
  KEY `FK_PRODUCT_DISCOUNT_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_DISCOUNT_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_image`;
CREATE TABLE IF NOT EXISTS `product_image` (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(225) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  PRIMARY KEY (`product_image_id`),
  KEY `FK__product` (`product_id`),
  CONSTRAINT `FK__product` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_option`;
CREATE TABLE IF NOT EXISTS `product_option` (
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value` mediumtext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Required, 2-Not Required',
  PRIMARY KEY (`product_option_id`),
  KEY `FK_PRODUCT_OPTION_PRODUCT_ID` (`product_id`),
  KEY `FK_PRODUCT_OPTION_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_PRODUCT_OPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`),
  CONSTRAINT `FK_PRODUCT_OPTION_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_related`;
CREATE TABLE IF NOT EXISTS `product_related` (
  `product_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  UNIQUE KEY `product_id_related_id` (`product_id`,`related_id`),
  KEY `FK_PRODUCT_RELATED_PRODUCT_RELATED_ID` (`related_id`),
  CONSTRAINT `FK_PRODUCT_RELATED_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `FK_PRODUCT_RELATED_PRODUCT_RELATED_ID` FOREIGN KEY (`related_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Related Products';

DROP TABLE IF EXISTS `product_review`;
CREATE TABLE IF NOT EXISTS `product_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `text` mediumtext NOT NULL,
  `rating` int(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive',
  PRIMARY KEY (`review_id`),
  KEY `FK_PRODUCT_REVIEW_PRODUCT_ID` (`product_id`),
  KEY `FK_PRODUCT_REVIEW_USER_ID` (`user_id`),
  CONSTRAINT `FK_PRODUCT_REVIEW_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `FK_PRODUCT_REVIEW_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_special`;
CREATE TABLE IF NOT EXISTS `product_special` (
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`product_special_id`),
  KEY `FK_PRODUCT_SPECIAL_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_SPECIAL_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `product_to_collection`;
CREATE TABLE IF NOT EXISTS `product_to_collection` (
  `product_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  UNIQUE KEY `product_id_collection_id` (`product_id`,`collection_id`),
  KEY `FK_PRODUCT_TO_COLLECTION_COLLECTION` (`collection_id`),
  CONSTRAINT `FK_PRODUCT_TO_COLLECTION_COLLECTION` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`collection_id`),
  CONSTRAINT `FK_PRODUCT_TO_COLLECTION_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `stock_status`;
CREATE TABLE IF NOT EXISTS `stock_status` (
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`stock_status_id`),
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `weight_class`;
CREATE TABLE IF NOT EXISTS `weight_class` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Default',
  PRIMARY KEY (`weight_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='in this table it has only one default is must';

DELETE FROM `weight_class`;

INSERT INTO `weight_class` (`weight_class_id`, `value`, `default`) VALUES
	(1, 1.00000000, 1),
	(2, 1000.00000000, 2),
	(3, 35.27400000, 2),
	(4, 2.20460000, 2);

DROP TABLE IF EXISTS `weight_class_description`;
CREATE TABLE IF NOT EXISTS `weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `unit` varchar(10) NOT NULL,
  UNIQUE KEY `lang_code_title` (`lang_code`,`title`),
  KEY `FK_WEIGHT_CLASS` (`weight_class_id`),
  CONSTRAINT `FK__WEIGHT_CLASS` FOREIGN KEY (`weight_class_id`) REFERENCES `weight_class` (`weight_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;	

DELETE FROM `weight_class_description`;

INSERT INTO `weight_class_description` (`weight_class_id`, `lang_code`, `title`, `unit`) VALUES
	(2, 'en', 'Gram', 'g'),
	(1, 'en', 'Kilogram', 'kg'),
	(3, 'en', 'Ounce', 'oz'),
	(4, 'en', 'Pound', 'lb');