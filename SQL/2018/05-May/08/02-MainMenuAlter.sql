Update menu_main set button_text = 0;

ALTER TABLE `menu_main`
	ALTER `menu_link` DROP DEFAULT;
ALTER TABLE `menu_main`
	ADD COLUMN `menu_type` TINYINT NOT NULL DEFAULT '0' COMMENT '1-Category, 2-Collection, 3-Pages, 4-Custom Menu' AFTER `menu_id`,
	CHANGE COLUMN `menu_link` `menu_link` TEXT NULL AFTER `menu_name`,
	CHANGE COLUMN `menu_position` `menu_position` CHAR(1) NULL DEFAULT NULL COMMENT 'M-Main Menu, U-Utility Menu' AFTER `menu_status`,
	CHANGE COLUMN `button_text` `button_text` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'To enable button style in menu' AFTER `menu_position`,
	DROP COLUMN `page_name`;

ALTER TABLE `menu_main`
	CHANGE COLUMN `menu_type` `menu_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0-Default Function, 1-Category, 2-Collection, 3-Pages, 4-Custom Menu' AFTER `menu_id`,
	CHANGE COLUMN `menu_position` `menu_position` CHAR(1) NULL DEFAULT NULL COMMENT 'M-Main Menu, U-Utility Menu, F-Footer Menu' AFTER `menu_status`;
