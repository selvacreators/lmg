ALTER TABLE `categories`
	ENGINE=InnoDB;

ALTER TABLE `categories_description`
	ENGINE=InnoDB,
	ADD UNIQUE INDEX `lang_code_category_slug` (`lang_code`, `category_slug`),
	ADD CONSTRAINT `FK_categories_description_categories` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`categories_id`);

DROP TABLE IF EXISTS `product_to_category`;
CREATE TABLE IF NOT EXISTS `product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  UNIQUE KEY `product_id_category_id` (`product_id`,`category_id`),
  KEY `FK_PRODUCT_TO_CATEGORY_CATEGORIES` (`category_id`),
  CONSTRAINT `FK_PRODUCT_TO_CATEGORY_CATEGORIES` FOREIGN KEY (`category_id`) REFERENCES `categories` (`categories_id`),
  CONSTRAINT `FK_PRODUCT_TO_CATEGORY_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;