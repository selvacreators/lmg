-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lmg_world.blog
DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `allow_comment` int(1) NOT NULL DEFAULT '1' COMMENT '1-Allow, 2-Disallow',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `count_read` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1-Active, 2-Deactive',
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table lmg_world.blog_category
DROP TABLE IF EXISTS `blog_category`;
CREATE TABLE IF NOT EXISTS `blog_category` (
  `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`blog_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table lmg_world.blog_category_description
DROP TABLE IF EXISTS `blog_category_description`;
CREATE TABLE IF NOT EXISTS `blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `lang_code` varchar(10) COLLATE utf8_bin NOT NULL,
  `blog_category_slug` varchar(225) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'consider this as Meta Title',
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `lang_code_blog_category_slug` (`lang_code`,`blog_category_slug`),
  KEY `FK_BLOG_CATEGORY_DESCRIPTION_BLOG_CATEGORY` (`blog_category_id`),
  CONSTRAINT `FK_BLOG_CATEGORY_DESCRIPTION_BLOG_CATEGORY` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_category` (`blog_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table lmg_world.blog_comment
DROP TABLE IF EXISTS `blog_comment`;
CREATE TABLE IF NOT EXISTS `blog_comment` (
  `blog_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`blog_comment_id`),
  KEY `FK_BLOG_COMMENT_BLOG` (`blog_id`),
  CONSTRAINT `FK_BLOG_COMMENT_BLOG` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table lmg_world.blog_description
DROP TABLE IF EXISTS `blog_description`;
CREATE TABLE IF NOT EXISTS `blog_description` (
  `blog_id` int(11) NOT NULL,
  `lang_code` varchar(10) COLLATE utf8_bin NOT NULL,
  `blog_slug` varchar(225) COLLATE utf8_bin NOT NULL,
  `title` varchar(225) COLLATE utf8_bin NOT NULL,
  `short_description` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `tags` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Consider this as a Meta Title',
  `meta_keyword` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  UNIQUE KEY `lang_code_blog_slug` (`lang_code`,`blog_slug`),
  KEY `FK_BLOG_DESCRIPTION_BLOG` (`blog_id`),
  CONSTRAINT `FK_blog_description_blog` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table lmg_world.blog_related
DROP TABLE IF EXISTS `blog_related`;
CREATE TABLE IF NOT EXISTS `blog_related` (
  `parent_blog_id` int(11) NOT NULL,
  `child_blog_id` int(11) NOT NULL,
  UNIQUE KEY `parent_blog_id_child_blog_id` (`parent_blog_id`,`child_blog_id`),
  KEY `FK_BLOG_RELATED_BLOG_CHILD` (`child_blog_id`),
  CONSTRAINT `FK_BLOG_RELATED_BLOG_CHILD` FOREIGN KEY (`child_blog_id`) REFERENCES `blog` (`blog_id`),
  CONSTRAINT `FK_BLOG_RELATED_BLOG_PARENT` FOREIGN KEY (`parent_blog_id`) REFERENCES `blog` (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table lmg_world.blog_to_category
DROP TABLE IF EXISTS `blog_to_category`;
CREATE TABLE IF NOT EXISTS `blog_to_category` (
  `blog_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  UNIQUE KEY `blog_id_blog_category_id` (`blog_id`,`blog_category_id`),
  KEY `FK_BLOG_TO_CATEGORY_BLOG_CATEGORY_ID` (`blog_category_id`),
  CONSTRAINT `FK_BLOG_TO_CATEGORY_BLOG_CATEGORY_ID` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_category` (`blog_category_id`),
  CONSTRAINT `FK_BLOG_TO_CATEGORY_BLOG_ID` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
