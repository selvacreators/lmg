DROP TABLE IF EXISTS `product_question`;
CREATE TABLE IF NOT EXISTS `product_question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `emailid` varchar(120) NOT NULL,
  `question` mediumtext NOT NULL,
  `answer` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '3' COMMENT '1-Active, 2-Deactive, 3-Pending',
  PRIMARY KEY (`question_id`),
  KEY `FK_PRODUCT_QUESTION_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_QUESTION_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;