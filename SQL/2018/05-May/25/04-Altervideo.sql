ALTER TABLE `video` DROP `updated_at`;
ALTER TABLE `video` ADD `lang_code` VARCHAR(20) NULL AFTER `video_id`, ADD `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `lang_code`;
ALTER TABLE `video` CHANGE `home_show_status` `home_show_status` TINYINT(4) NOT NULL DEFAULT '2';