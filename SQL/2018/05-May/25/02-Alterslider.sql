ALTER TABLE `slider` CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `slider` DROP `updated_at`;
ALTER TABLE `slider` ADD `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`;
ALTER TABLE `slider` ADD `lang_code` VARCHAR(20) NULL AFTER `slider_id`;