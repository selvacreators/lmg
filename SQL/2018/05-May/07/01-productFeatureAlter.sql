ALTER TABLE `product_features` CHANGE `name` `name` VARCHAR(165) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `product_features` CHANGE `prod_des` `prod_des` MEDIUMTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `product_features` CHANGE `prod_image` `prod_image` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `product_features` CHANGE `prod_video` `prod_video` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;