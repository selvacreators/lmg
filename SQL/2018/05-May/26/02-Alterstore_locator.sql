ALTER TABLE `store_locator` ADD `lang_code` VARCHAR(20) NULL AFTER `store_id`;
ALTER TABLE `brands_setting` ADD `lang_code` VARCHAR(20) NULL AFTER `brands_setting_id`;
ALTER TABLE `brands` ADD `lang_code` VARCHAR(20) NULL AFTER `brand_id`;