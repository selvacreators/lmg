ALTER TABLE `common_setting` CHANGE `home_logo` `store_logo` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `common_setting` CHANGE `logo` `logo` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `common_setting` CHANGE `mobile_no` `phone` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `common_setting` CHANGE `email_id` `email` VARCHAR(65) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `common_setting` CHANGE `status` `status` TINYINT(4) NOT NULL DEFAULT '0';
ALTER TABLE `common_setting` ADD `lang_code` VARCHAR(10) NULL AFTER `id`, ADD `store_name` VARCHAR(50) NULL AFTER `lang_code`, ADD `address` TEXT NULL AFTER `store_name`, ADD `open_time` VARCHAR(40) NULL AFTER `address`, ADD `meta_title` VARCHAR(50) NULL AFTER `open_time`, ADD `meta_desc` TEXT NULL AFTER `meta_title`, ADD `meta_keyw` TEXT NULL AFTER `meta_desc`;
ALTER TABLE `common_setting` ADD `fav_icon` VARCHAR(255) NULL AFTER `store_logo`;
ALTER TABLE `common_setting` ADD `updated_at` TIMESTAMP NULL DEFAULT NULL AFTER `created_at`;
ALTER TABLE `common_setting` CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;