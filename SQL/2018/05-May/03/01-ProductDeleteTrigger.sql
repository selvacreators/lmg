DROP TRIGGER IF EXISTS `product_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `product_before_delete` BEFORE DELETE ON `product` FOR EACH ROW BEGIN
	DELETE FROM product_description WHERE product_id = old.product_id;
	DELETE FROM product_attibute WHERE product_id = old.product_id;
	DELETE FROM product_discount WHERE product_id = old.product_id;
	DELETE FROM product_image WHERE product_id = old.product_id;
	DELETE FROM product_option WHERE product_id = old.product_id;
	DELETE FROM product_related WHERE product_id = old.product_id;
	DELETE FROM product_review WHERE product_id = old.product_id;
	DELETE FROM product_special WHERE product_id = old.product_id;
	DELETE FROM product_to_collection WHERE product_id = old.product_id;
	DELETE FROM product_features where product_id = old.product_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
