ALTER TABLE `product_review`
	CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL DEFAULT '0' AFTER `product_id`,
	CHANGE COLUMN `status` `status` TINYINT(3) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive, 3-Pending' AFTER `updated_by`,
	DROP INDEX `FK_PRODUCT_REVIEW_USER_ID`,
	DROP FOREIGN KEY `FK_PRODUCT_REVIEW_USER_ID`;

ALTER TABLE `product_review`
	CHANGE COLUMN `status` `status` TINYINT(1) NOT NULL DEFAULT '3' COMMENT '1-Active, 2-Deactive, 3-Pending' AFTER `updated_by`;
