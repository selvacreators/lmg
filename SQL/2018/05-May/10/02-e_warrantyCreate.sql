-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 10, 2018 at 03:00 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lmg_world`
--

-- --------------------------------------------------------

--
-- Table structure for table `e_warranty`
--

DROP TABLE IF EXISTS `e_warranty`;
CREATE TABLE IF NOT EXISTS `e_warranty` (
  `ewarranty_id` int(11) NOT NULL AUTO_INCREMENT,
  `salutation` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `dob` varchar(30) DEFAULT NULL,
  `address` text,
  `postal_code` varchar(20) DEFAULT NULL,
  `purchase_invoice_num` varchar(100) DEFAULT NULL,
  `place_of_purchase` varchar(50) DEFAULT NULL,
  `date_of_purchase` varchar(30) DEFAULT NULL,
  `date_of_delivery` varchar(30) DEFAULT NULL,
  `no_mattress_purchase` varchar(30) DEFAULT NULL,
  `serial_number` varchar(100) DEFAULT NULL,
  `product_type` varchar(50) DEFAULT NULL,
  `product_size` varchar(30) DEFAULT NULL,
  `purchase_experience` varchar(30) DEFAULT NULL,
  `purchase_delivery` varchar(30) DEFAULT NULL,
  `message` text,
  `lang_code` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_ip` varchar(50) DEFAULT NULL,
  `status` tinyint(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ewarranty_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
