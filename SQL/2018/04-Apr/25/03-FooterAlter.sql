ALTER TABLE `footer` ADD `language` VARCHAR(255) NOT NULL AFTER `footer_copyright`;
ALTER TABLE `footer` ADD `flag` VARCHAR(255) NOT NULL AFTER `language`;
ALTER TABLE `footer` CHANGE `flag` `code` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;