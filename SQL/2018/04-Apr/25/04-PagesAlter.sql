ALTER TABLE `pages`
	ADD COLUMN `lang` VARCHAR(10) NOT NULL DEFAULT 'en-US' AFTER `page_id`,
	DROP INDEX `slug`,
	ADD UNIQUE INDEX `lang_slug` (`lang`, `slug`);
