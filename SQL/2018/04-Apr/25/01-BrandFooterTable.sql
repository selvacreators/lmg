DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(65) NOT NULL,
  `brand_description` text NOT NULL,
  `brand_logo` varchar(255) NOT NULL,
  `brand_featureimg` varchar(255) NOT NULL,
  `imge_position` varchar(65) NOT NULL,
  `btn_text` varchar(65) NOT NULL,
  `btn_url` varchar(255) NOT NULL,
  `brand_status` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`brand_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `brand_description`, `brand_logo`, `brand_featureimg`, `imge_position`, `btn_text`, `btn_url`, `brand_status`, `created_at`, `updated_at`) VALUES
(1, 'test', 'brand test', '/NAME.jpg', '/unnamed (1).jpg', 'left', 'button', 'demo', 1, '2018-04-25 08:07:00', '2018-04-25 13:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

DROP TABLE IF EXISTS `footer`;
CREATE TABLE IF NOT EXISTS `footer` (
  `footer_id` int(11) NOT NULL AUTO_INCREMENT,
  `footer_link` varchar(120) NOT NULL,
  `footer_content` text NOT NULL,
  `footer_copyright` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`footer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`footer_id`, `footer_link`, `footer_content`, `footer_copyright`, `status`, `created_at`, `updated_at`) VALUES
(1, '/', '<li class=\"vertical-separator\"><a href=\"#\" class=\"hide-on-mobile\">About Us</a></li>\r\n                            <li class=\"vertical-separator\"> <a href=\"#\" class=\"hide-on-mobile\">Privacy &amp; Term</a></li>\r\n                            <li class=\"vertical-separator\"><a href=\"#\" class=\"hide-on-mobile\">Contact</a></li>', 'LMG World © 2018', 1, '2018-04-25 06:13:28', '2018-04-25 11:43:28');
COMMIT;
