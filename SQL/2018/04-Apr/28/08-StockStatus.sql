INSERT INTO `stock_status` (`stock_status_id`, `lang_code`, `name`) VALUES
	(3, 'en', '2-3 Days'),
	(1, 'en', 'In Stock'),
	(2, 'en', 'Out Of Stock');