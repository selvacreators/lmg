ALTER TABLE `product`
	ENGINE=InnoDB;

ALTER TABLE `product_description`
	ENGINE=InnoDB;

ALTER TABLE `brands`
	ENGINE=InnoDB;

ALTER TABLE `collection`
	ENGINE=InnoDB;
