-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lmg_world.attribute
DROP TABLE IF EXISTS `attribute`;
CREATE TABLE IF NOT EXISTS `attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`),
  KEY `FK_ATTRIBUTE_ATTRIBUTE_GROUP` (`attribute_group_id`),
  CONSTRAINT `FK_ATTRIBUTE_ATTRIBUTE_GROUP` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_group` (`attribute_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.attribute: ~0 rows (approximately)
DELETE FROM `attribute`;
/*!40000 ALTER TABLE `attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute` ENABLE KEYS */;

-- Dumping structure for table lmg_world.attribute_description
DROP TABLE IF EXISTS `attribute_description`;
CREATE TABLE IF NOT EXISTS `attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK__ATTRIBUTE` (`attribute_id`),
  CONSTRAINT `FK__ATTRIBUTE` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.attribute_description: ~0 rows (approximately)
DELETE FROM `attribute_description`;
/*!40000 ALTER TABLE `attribute_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_description` ENABLE KEYS */;

-- Dumping structure for table lmg_world.attribute_group
DROP TABLE IF EXISTS `attribute_group`;
CREATE TABLE IF NOT EXISTS `attribute_group` (
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`attribute_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.attribute_group: ~0 rows (approximately)
DELETE FROM `attribute_group`;
/*!40000 ALTER TABLE `attribute_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_group` ENABLE KEYS */;

-- Dumping structure for table lmg_world.attribute_group_description
DROP TABLE IF EXISTS `attribute_group_description`;
CREATE TABLE IF NOT EXISTS `attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK__ATTRIBUTE_GROUP` (`attribute_group_id`),
  CONSTRAINT `FK__ATTRIBUTE_GROUP` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_group` (`attribute_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.attribute_group_description: ~0 rows (approximately)
DELETE FROM `attribute_group_description`;
/*!40000 ALTER TABLE `attribute_group_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_group_description` ENABLE KEYS */;

-- Dumping structure for table lmg_world.length_class
DROP TABLE IF EXISTS `length_class`;
CREATE TABLE IF NOT EXISTS `length_class` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Default',
  PRIMARY KEY (`length_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='in this table it has only one default is must';

-- Dumping data for table lmg_world.length_class: ~3 rows (approximately)
DELETE FROM `length_class`;
/*!40000 ALTER TABLE `length_class` DISABLE KEYS */;
INSERT INTO `length_class` (`length_class_id`, `value`, `default`) VALUES
	(1, 1.00000000, 1),
	(2, 0.39370000, 2),
	(3, 10.00000000, 2);
/*!40000 ALTER TABLE `length_class` ENABLE KEYS */;

-- Dumping structure for table lmg_world.length_class_description
DROP TABLE IF EXISTS `length_class_description`;
CREATE TABLE IF NOT EXISTS `length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `unit` varchar(10) NOT NULL,
  UNIQUE KEY `lang_code_title` (`lang_code`,`title`),
  KEY `FK_LENGTH_CLASS_DESCRIPTION_LENGTH_CLASS` (`length_class_id`),
  CONSTRAINT `FK_LENGTH_CLASS_DESCRIPTION_LENGTH_CLASS` FOREIGN KEY (`length_class_id`) REFERENCES `length_class` (`length_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.length_class_description: ~3 rows (approximately)
DELETE FROM `length_class_description`;
/*!40000 ALTER TABLE `length_class_description` DISABLE KEYS */;
INSERT INTO `length_class_description` (`length_class_id`, `lang_code`, `title`, `unit`) VALUES
	(1, 'en', 'Centimeter', 'cm'),
	(2, 'en', 'Inch', 'in'),
	(3, 'en', 'Millimeter', 'mm');
/*!40000 ALTER TABLE `length_class_description` ENABLE KEYS */;

-- Dumping structure for table lmg_world.option
DROP TABLE IF EXISTS `option`;
CREATE TABLE IF NOT EXISTS `option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.option: ~0 rows (approximately)
DELETE FROM `option`;
/*!40000 ALTER TABLE `option` DISABLE KEYS */;
/*!40000 ALTER TABLE `option` ENABLE KEYS */;

-- Dumping structure for table lmg_world.option_description
DROP TABLE IF EXISTS `option_description`;
CREATE TABLE IF NOT EXISTS `option_description` (
  `option_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK_OPTION_DESCRIPTION_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_OPTION_DESCRIPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.option_description: ~0 rows (approximately)
DELETE FROM `option_description`;
/*!40000 ALTER TABLE `option_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_description` ENABLE KEYS */;

-- Dumping structure for table lmg_world.option_value
DROP TABLE IF EXISTS `option_value`;
CREATE TABLE IF NOT EXISTS `option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `sort_order` int(6) NOT NULL,
  PRIMARY KEY (`option_value_id`),
  KEY `FK_OPTION_VALUE_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_OPTION_VALUE_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.option_value: ~0 rows (approximately)
DELETE FROM `option_value`;
/*!40000 ALTER TABLE `option_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_value` ENABLE KEYS */;

-- Dumping structure for table lmg_world.option_value_description
DROP TABLE IF EXISTS `option_value_description`;
CREATE TABLE IF NOT EXISTS `option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`),
  KEY `FK_OPTION_VALUE_DESCRIPTION_OPTION_VALUE_ID` (`option_value_id`),
  KEY `FK_OPTION_VALUE_DESCRIPTION_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_OPTION_VALUE_DESCRIPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`),
  CONSTRAINT `FK_OPTION_VALUE_DESCRIPTION_OPTION_VALUE_ID` FOREIGN KEY (`option_value_id`) REFERENCES `option_value` (`option_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.option_value_description: ~0 rows (approximately)
DELETE FROM `option_value_description`;
/*!40000 ALTER TABLE `option_value_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_value_description` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(225) DEFAULT NULL,
  `brand_id` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `date_available` date NOT NULL,
  `weight_class_id` int(11) DEFAULT NULL,
  `weight` decimal(15,8) DEFAULT NULL,
  `length_class_id` int(11) DEFAULT NULL,
  `length` decimal(15,8) DEFAULT NULL,
  `width` decimal(15,8) DEFAULT NULL,
  `height` decimal(15,8) DEFAULT NULL,
  `subtract` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-Subtract Stock, 2-Dont Subtract Stock',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive',
  `viewed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `FK_PRODUCT_BRAND` (`brand_id`),
  KEY `FK_PRODUCT_STOCK_STATUS` (`stock_status_id`),
  KEY `FK_PRODUCT_WEIGHT_CLASS` (`weight_class_id`),
  KEY `FK_PRODUCT_LENGTH_CLASS` (`length_class_id`),
  CONSTRAINT `FK_PRODUCT_BRAND` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
  CONSTRAINT `FK_PRODUCT_LENGTH_CLASS` FOREIGN KEY (`length_class_id`) REFERENCES `length_class` (`length_class_id`),
  CONSTRAINT `FK_PRODUCT_STOCK_STATUS` FOREIGN KEY (`stock_status_id`) REFERENCES `stock_status` (`stock_status_id`),
  CONSTRAINT `FK_PRODUCT_WEIGHT_CLASS` FOREIGN KEY (`weight_class_id`) REFERENCES `weight_class` (`weight_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product: ~0 rows (approximately)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_attibute
DROP TABLE IF EXISTS `product_attibute`;
CREATE TABLE IF NOT EXISTS `product_attibute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `text` mediumtext NOT NULL,
  KEY `FK_PRODUCT_ATTRIBUTE_PRODUCT_ID` (`product_id`),
  KEY `FK_PRODUCT_ATTIBUTE_ATTRIBUTE_ID` (`attribute_id`),
  CONSTRAINT `FK_PRODUCT_ATTIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`attribute_id`),
  CONSTRAINT `FK_PRODUCT_ATTRIBUTE_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_attibute: ~0 rows (approximately)
DELETE FROM `product_attibute`;
/*!40000 ALTER TABLE `product_attibute` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attibute` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_description
DROP TABLE IF EXISTS `product_description`;
CREATE TABLE IF NOT EXISTS `product_description` (
  `product_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `product_slug` varchar(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `desc` mediumtext NOT NULL,
  `meta_title` varchar(225) DEFAULT NULL,
  `meta_desc` varchar(225) DEFAULT NULL,
  `meta_keyword` varchar(225) DEFAULT NULL,
  `tag` mediumtext,
  UNIQUE KEY `lang_code_product_slug` (`lang_code`,`product_slug`),
  KEY `FK_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_description: ~0 rows (approximately)
DELETE FROM `product_description`;
/*!40000 ALTER TABLE `product_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_description` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_discount
DROP TABLE IF EXISTS `product_discount`;
CREATE TABLE IF NOT EXISTS `product_discount` (
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '1',
  `priority` int(3) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '1.0000',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`product_discount_id`),
  KEY `FK_PRODUCT_DISCOUNT_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_DISCOUNT_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_discount: ~0 rows (approximately)
DELETE FROM `product_discount`;
/*!40000 ALTER TABLE `product_discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_discount` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_image
DROP TABLE IF EXISTS `product_image`;
CREATE TABLE IF NOT EXISTS `product_image` (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(225) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  PRIMARY KEY (`product_image_id`),
  KEY `FK__product` (`product_id`),
  CONSTRAINT `FK__product` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_image: ~0 rows (approximately)
DELETE FROM `product_image`;
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_option
DROP TABLE IF EXISTS `product_option`;
CREATE TABLE IF NOT EXISTS `product_option` (
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value` mediumtext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Required, 2-Not Required',
  PRIMARY KEY (`product_option_id`),
  KEY `FK_PRODUCT_OPTION_PRODUCT_ID` (`product_id`),
  KEY `FK_PRODUCT_OPTION_OPTION_ID` (`option_id`),
  CONSTRAINT `FK_PRODUCT_OPTION_OPTION_ID` FOREIGN KEY (`option_id`) REFERENCES `option` (`option_id`),
  CONSTRAINT `FK_PRODUCT_OPTION_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_option: ~0 rows (approximately)
DELETE FROM `product_option`;
/*!40000 ALTER TABLE `product_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_option` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_related
DROP TABLE IF EXISTS `product_related`;
CREATE TABLE IF NOT EXISTS `product_related` (
  `product_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  UNIQUE KEY `product_id_related_id` (`product_id`,`related_id`),
  KEY `FK_PRODUCT_RELATED_PRODUCT_RELATED_ID` (`related_id`),
  CONSTRAINT `FK_PRODUCT_RELATED_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `FK_PRODUCT_RELATED_PRODUCT_RELATED_ID` FOREIGN KEY (`related_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Related Products';

-- Dumping data for table lmg_world.product_related: ~0 rows (approximately)
DELETE FROM `product_related`;
/*!40000 ALTER TABLE `product_related` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_related` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_review
DROP TABLE IF EXISTS `product_review`;
CREATE TABLE IF NOT EXISTS `product_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `text` mediumtext NOT NULL,
  `rating` int(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive',
  PRIMARY KEY (`review_id`),
  KEY `FK_PRODUCT_REVIEW_PRODUCT_ID` (`product_id`),
  KEY `FK_PRODUCT_REVIEW_USER_ID` (`user_id`),
  CONSTRAINT `FK_PRODUCT_REVIEW_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `FK_PRODUCT_REVIEW_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_review: ~0 rows (approximately)
DELETE FROM `product_review`;
/*!40000 ALTER TABLE `product_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_review` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_special
DROP TABLE IF EXISTS `product_special`;
CREATE TABLE IF NOT EXISTS `product_special` (
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`product_special_id`),
  KEY `FK_PRODUCT_SPECIAL_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PRODUCT_SPECIAL_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_special: ~0 rows (approximately)
DELETE FROM `product_special`;
/*!40000 ALTER TABLE `product_special` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_special` ENABLE KEYS */;

-- Dumping structure for table lmg_world.product_to_collection
DROP TABLE IF EXISTS `product_to_collection`;
CREATE TABLE IF NOT EXISTS `product_to_collection` (
  `product_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  UNIQUE KEY `product_id_collection_id` (`product_id`,`collection_id`),
  KEY `FK_PRODUCT_TO_COLLECTION_COLLECTION` (`collection_id`),
  CONSTRAINT `FK_PRODUCT_TO_COLLECTION_COLLECTION` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`collection_id`),
  CONSTRAINT `FK_PRODUCT_TO_COLLECTION_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.product_to_collection: ~0 rows (approximately)
DELETE FROM `product_to_collection`;
/*!40000 ALTER TABLE `product_to_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_to_collection` ENABLE KEYS */;

-- Dumping structure for table lmg_world.stock_status
DROP TABLE IF EXISTS `stock_status`;
CREATE TABLE IF NOT EXISTS `stock_status` (
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`stock_status_id`),
  UNIQUE KEY `lang_code_name` (`lang_code`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.stock_status: ~0 rows (approximately)
DELETE FROM `stock_status`;
/*!40000 ALTER TABLE `stock_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_status` ENABLE KEYS */;

-- Dumping structure for table lmg_world.weight_class
DROP TABLE IF EXISTS `weight_class`;
CREATE TABLE IF NOT EXISTS `weight_class` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1-Default',
  PRIMARY KEY (`weight_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='in this table it has only one default is must';

-- Dumping data for table lmg_world.weight_class: ~4 rows (approximately)
DELETE FROM `weight_class`;
/*!40000 ALTER TABLE `weight_class` DISABLE KEYS */;
INSERT INTO `weight_class` (`weight_class_id`, `value`, `default`) VALUES
	(1, 1.00000000, 1),
	(2, 1000.00000000, 2),
	(3, 35.27400000, 2),
	(4, 2.20460000, 2);
/*!40000 ALTER TABLE `weight_class` ENABLE KEYS */;

-- Dumping structure for table lmg_world.weight_class_description
DROP TABLE IF EXISTS `weight_class_description`;
CREATE TABLE IF NOT EXISTS `weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `unit` varchar(10) NOT NULL,
  UNIQUE KEY `lang_code_title` (`lang_code`,`title`),
  KEY `FK_WEIGHT_CLASS` (`weight_class_id`),
  CONSTRAINT `FK__WEIGHT_CLASS` FOREIGN KEY (`weight_class_id`) REFERENCES `weight_class` (`weight_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table lmg_world.weight_class_description: ~4 rows (approximately)
DELETE FROM `weight_class_description`;
/*!40000 ALTER TABLE `weight_class_description` DISABLE KEYS */;
INSERT INTO `weight_class_description` (`weight_class_id`, `lang_code`, `title`, `unit`) VALUES
	(2, 'en', 'Gram', 'g'),
	(1, 'en', 'Kilogram', 'kg'),
	(3, 'en', 'Ounce', 'oz'),
	(4, 'en', 'Pound', 'lb');
/*!40000 ALTER TABLE `weight_class_description` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
