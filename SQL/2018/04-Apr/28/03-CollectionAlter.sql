ALTER TABLE `collection`
	ALTER `main_image` DROP DEFAULT,
	ALTER `feature_image` DROP DEFAULT,
	ALTER `collection_banner` DROP DEFAULT,
	ALTER `collection_btn_text` DROP DEFAULT,
	ALTER `collection_btn_url` DROP DEFAULT,
	ALTER `meta_keyword` DROP DEFAULT;
ALTER TABLE `collection`
	COMMENT='Collection Master which handle like a category in ecommerce',
	ADD COLUMN `lang_code` VARCHAR(10) NOT NULL DEFAULT 'en' AFTER `collection_id`,
	ADD COLUMN `collection_slug` VARCHAR(255) NOT NULL AFTER `collection_name`,
	CHANGE COLUMN `main_image` `main_image` VARCHAR(255) NULL AFTER `brand_id`,
	CHANGE COLUMN `feature_image` `feature_image` VARCHAR(255) NULL AFTER `main_image`,
	CHANGE COLUMN `collection_banner` `collection_banner` VARCHAR(255) NULL AFTER `feature_image`,
	CHANGE COLUMN `collection_btn_text` `collection_btn_text` VARCHAR(65) NULL AFTER `position`,
	CHANGE COLUMN `collection_btn_url` `collection_btn_url` VARCHAR(225) NULL AFTER `collection_btn_text`,
	ADD COLUMN `parent_id` INT(10) NOT NULL DEFAULT '0' AFTER `collection_btn_url`,
	CHANGE COLUMN `meta_keyword` `meta_keyword` VARCHAR(255) NULL AFTER `meta_title`,
	CHANGE COLUMN `meta_desc` `meta_desc` TEXT NULL AFTER `meta_keyword`,
	ADD COLUMN `sort_order` INT(3) NOT NULL DEFAULT '0' AFTER `meta_desc`,
	ADD COLUMN `created_by` VARCHAR(30) NOT NULL COMMENT 'IP address of the person who add' AFTER `created_at`,
	ADD COLUMN `updated_by` VARCHAR(30) NULL DEFAULT NULL AFTER `updated_at`,
	CHANGE COLUMN `col_status` `col_status` INT(1) NOT NULL DEFAULT '2' COMMENT '1-Active, 2-Deactive' AFTER `updated_by`;

update collection c set c.collection_slug = REPLACE( LCASE(c.collection_name) , ' ', '-' );

ALTER TABLE `collection`
	ADD UNIQUE INDEX `lang_code_collection_slug` (`lang_code`, `collection_slug`);

ALTER TABLE `collection`
	CHANGE COLUMN `updated_by` `updated_by` VARCHAR(30) NULL DEFAULT NULL COMMENT 'IP address of the person who update' AFTER `updated_at`;

ALTER TABLE `collection`
	ALTER `lang_code` DROP DEFAULT;
ALTER TABLE `collection`
	CHANGE COLUMN `lang_code` `lang_code` VARCHAR(10) NOT NULL AFTER `collection_id`;
