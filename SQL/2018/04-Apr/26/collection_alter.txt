ALTER TABLE `collection` ADD `meta_keyword` VARCHAR(255) NOT NULL AFTER `btn_url`, ADD `meta_desc` TEXT NOT NULL AFTER `meta_keyword`;

ALTER TABLE `collection` ADD `meta_title` VARCHAR(255) NOT NULL AFTER `btn_url`;
